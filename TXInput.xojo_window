#tag Window
Begin Window TXInput
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   8
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   164
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Transponder Input"
   Visible         =   True
   Width           =   454
   Begin TextField RacerNumber
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   75
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "####"
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   22
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   80
   End
   Begin TextField TXCode
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   75
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "&&&&&&&&"
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   56
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   80
   End
   Begin Label lbl_Name
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   177
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   23
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   265
   End
   Begin Label lbl_Representing
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   177
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   57
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   265
   End
   Begin Label lbl_Number
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   14
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Number:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   22
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   55
   End
   Begin Label lbl_TX
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   14
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "TX Code:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   56
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   55
   End
   Begin PushButton pbOK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "OK"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   365
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   124
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PushButton pbCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   277
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   124
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin CheckBox cbReturned
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Returned"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   75
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      State           =   0
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   123
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label stName
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   14
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Name:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   90
      Transparent     =   False
      Underline       =   False
      Visible         =   False
      Width           =   55
   End
   Begin PopupMenu cbParticipantName
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   75
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   89
      Underline       =   False
      Visible         =   True
      Width           =   367
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Close()
		  WarningDisplayed=true
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  dim recsRS, rsParticipants, rsTeamMembers as RecordSet
		  dim i, TeamMemberIdx as integer
		  dim SQLStatement as String
		  TXIDStg=wnd_List.TXIDStg
		  
		  cbParticipantName.AddRow "Same TX for ALL team members or NOT a team"
		  cbParticipantName.RowTag(0)=0
		  cbParticipantName.ListIndex=0
		  
		  SQLStatement="Select TX_Code, Racer_Number, TeamMemberID, Returned FROM transponders WHERE rowid="+TXIDStg
		  
		  recsRS=app.theDB.DBSQLSelect(SQLStatement)
		  if not recsRS.EOF then  'not a new record
		    NewTransponder=false
		    RacerNumber.text=recsRS.Field("Racer_Number").StringValue
		    OldRacerNumber=RacerNumber.Text
		    TXCode.Text=recsRS.Field("TX_Code").StringValue
		    
		    if recsRS.Field("Returned").StringValue<>"" and recsRS.Field("Returned").StringValue="0-00-00 00:00:00" and recsRS.Field("Returned").StringValue="0000-00-00 00:00:00" then
		      cbReturned.Value=true
		    end if
		    
		    rsParticipants=app.theDB.DBSQLSelect("SELECT rowid, Participant_Name, First_Name, Representing FROM participants WHERE Racer_Number="+recsRS.Field("Racer_Number").StringValue)
		    if not(rsParticipants.EOF) then
		      ParticipantName=rsParticipants.field("Participant_Name").StringValue
		      FirstName=rsParticipants.Field("First_Name").StringValue
		      lbl_Name.Text=FirstName+" "+ParticipantName
		      lbl_Representing.Text=rsParticipants.Field("Representing").StringValue
		      
		      rsTeamMembers=app.theDB.DBSQLSelect("SELECT rowid, MemberName FROM teamMembers WHERE ParticipantID ="+rsParticipants.Field("rowid").StringValue+" ORDER BY IntervalNumber")
		      if not rsTeamMembers.EOF then
		        stName.Visible=true
		        cbParticipantName.Visible=true
		        for i=1 to rsTeamMembers.RecordCount
		          cbParticipantName.AddRow rsTeamMembers.Field("MemberName").StringValue
		          cbParticipantName.RowTag(i)=rsTeamMembers.Field("rowid").IntegerValue
		          if recsRS.Field("TeamMemberID").IntegerValue = rsTeamMembers.Field("rowid").IntegerValue then
		            TeamMemberIdx=i
		          end if
		          rsTeamMembers.MoveNext
		        next
		      end if
		      rsTeamMembers.Close
		    else
		      ParticipantName=""
		      FirstName=""
		      lbl_Name.Text=""
		      lbl_Representing.Text=""
		    end if
		    
		    
		    
		    
		    cbParticipantName.ListIndex=TeamMemberIdx
		    
		  else
		    NewTransponder=true
		    RacerNumber.text="0"
		    cbReturned.Value=false
		  end if
		  OldTXReturned=cbReturned.Value
		  RacerNumber.SelStart=0
		  RacerNumber.SelLength=len(RacerNumber.Text)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Sub InsertTransponder()
		  dim TodaysDateTime as date
		  dim SQLStatement as string
		  dim TXID as string
		  dim RowCount as Integer
		  
		  TodaysDateTime=new Date
		  
		  'Build SQL Statement
		  SQLStatement="INSERT INTO transponders (RaceID, Racer_Number, TX_Code, Issued, TeamMemberID) VALUES ("
		  SQLStatement=SQLStatement+"1, " 'RaceID
		  SQLStatement=SQLStatement+RacerNumber.Text+", " 'RacerNumber
		  SQLStatement=SQLStatement+"'"+TXCode.Text+"', " 'Transponder Code
		  SQLStatement = SQLStatement+"'"+TodaysDateTime.SQLDateTime+"', "
		  SQLStatement = SQLStatement+"'"+cbParticipantName.RowTag(cbParticipantName.ListIndex)+"')"
		  
		  'Execute the SQL
		  App.theDB.DBSQLExecute(SQLStatement)
		  
		  'Update the wndlist
		  
		  wnd_List.DataList_Transponders.LockDrawing=true
		  wnd_List.DataList_Transponders.AppendRow(RacerNumber.text)
		  RowCount=wnd_List.DataList_Transponders.Rows
		  wnd_List.DataList_Transponders.CellText(2,RowCount)=TXCode.Text
		  wnd_List.DataList_Transponders.CellText(3,RowCount)=FirstName+" "+ParticipantName
		  if cbParticipantName.RowTag(cbParticipantName.ListIndex)=0 then
		    wnd_List.DataList_Transponders.CellText(4,RowCount)=""
		  else
		    wnd_List.DataList_Transponders.CellText(4,RowCount)=cbParticipantName.Text
		  end if
		  wnd_List.DataList_Transponders.CellText(5,RowCount)=TodaysDateTime.SQLDateTime
		  wnd_List.DataList_Transponders.CellText(6,RowCount)="0"
		  wnd_List.DataList_Transponders.CellText(7,RowCount)="0000-00-00 00:00:00"
		  wnd_List.DataList_Transponders.Row(RowCount).ItemData=str(app.theDB.DBLastRowID)
		  
		  wnd_List.DataList_Transponders.VScrollValue=RowCount
		  wnd_List.DataList_Transponders.LockDrawing=false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub UpdateTransponder()
		  dim TodaysDateTime as date
		  dim SQLStatement as string
		  dim SelectedRow as Integer
		  
		  TodaysDateTime=new Date
		  
		  SQLStatement = "UPDATE transponders SET TX_Code='"+TXCode.text+"'"
		  SQLStatement = SQLStatement + ", Racer_Number="+RacerNumber.text
		  SQLStatement = SQLStatement + ", TX_Number=''"
		  SQLStatement = SQLStatement + ", TeamMemberID='"+cbParticipantName.RowTag(cbParticipantName.ListIndex)+"'"
		  if OldTXReturned = false and cbReturned.Value then
		    SQLStatement = SQLStatement + ", Returned='"+TodaysDateTime.SQLDateTime+"'"
		  end if
		  SQLStatement = SQLStatement + " WHERE rowid="+TXIDStg
		  
		  App.theDB.DBSQLExecute(SQLStatement)
		  
		  SelectedRow=wnd_List.DataList_Transponders.VScrollValue
		  wnd_List.DataList_Transponders.CellText(1,wnd_List.RowSelected)=RacerNumber.Text
		  wnd_List.DataList_Transponders.CellText(2,wnd_List.RowSelected)=TxCode.Text
		  wnd_List.DataList_Transponders.CellText(3,wnd_List.RowSelected)=lbl_Name.Text
		  wnd_List.DataList_Transponders.CellText(4,wnd_List.RowSelected)=cbParticipantName.Text
		  if OldTXReturned = false and cbReturned.Value then
		    wnd_List.DataList_Transponders.CellText(7,wnd_List.RowSelected)=TodaysDateTime.SQLDateTime
		  end if
		  
		  wnd_List.DataList_Transponders.Refresh
		  wnd_List.DataList_Transponders.Selection.Clear
		  wnd_List.DataList_Transponders.VScrollValue=SelectedRow
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected FirstName As string
	#tag EndProperty

	#tag Property, Flags = &h0
		NewTransponder As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		OldRacerNumber As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected OldTXReturned As boolean
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ParticipantName As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected TXIDStg As string
	#tag EndProperty

	#tag Property, Flags = &h0
		WarningDisplayed As boolean
	#tag EndProperty


#tag EndWindowCode

#tag Events RacerNumber
	#tag Event
		Sub GotFocus()
		  WarningDisplayed=false
		End Sub
	#tag EndEvent
	#tag Event
		Sub LostFocus()
		  dim rs, rsTeamMembers as RecordSet
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  dim i as Integer   
		  
		  if OldRacerNumber<>RacerNumber.Text then
		     rs=app.theDB.DBSQLSelect("SELECT rowid, Participant_Name, First_Name, Representing FROM participants WHERE RaceID=1 AND Racer_Number ="+RacerNumber.text)
		    if not rs.EOF then
		      ParticipantName=rs.field("Participant_Name").StringValue
		      FirstName=rs.Field("First_Name").StringValue
		      lbl_Name.Text=FirstName+" "+ParticipantName
		      lbl_Representing.Text=rs.Field("Representing").StringValue
		      
		      rsTeamMembers=app.theDB.DBSQLSelect("SELECT rowid, MemberName FROM teamMembers WHERE ParticipantID ="+rs.Field("rowid").StringValue+" ORDER BY IntervalNumber")
		      if not rsTeamMembers.EOF then
		        stName.Visible=true
		        cbParticipantName.Visible=true
		        for i=0 to rsTeamMembers.RecordCount-1
		          cbParticipantName.AddRow rsTeamMembers.Field("MemberName").StringValue
		          cbParticipantName.RowTag(i)= rsTeamMembers.Field("rowid").IntegerValue
		          rsTeamMembers.MoveNext
		        next
		      end if
		      rsTeamMembers.Close
		    end if
		    
		    if not WarningDisplayed then
		      rs=app.theDB.DBSQLSelect("Select Racer_Number, TX_Code FROM transponders WHERE Racer_Number ="+RacerNumber.text)
		      if not rs.EOF then
		        d.Message=rs.Field("Racer_Number").StringValue+" already has the following transponder(s) assigned:"
		        for i = 1 to rs.FieldCount
		          d.Explanation=d.Explanation+rs.Field("TX_Code").StringValue+" "
		          rs.MoveNext
		        next
		        b=d.ShowModal  
		      end if
		      WarningDisplayed=true
		    end if
		    rs.Close
		  end if
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TXCode
	#tag Event
		Sub TextChange()
		  TXCode.text=Uppercase(TXCode.text)
		  if len(TXCode.text)>=8 then
		    RacerNumber.SelStart=0
		    RacerNumber.SelLength=len(RacerNumber.Text)
		    RacerNumber.SetFocus
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub LostFocus()
		  dim rs as RecordSet
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  dim i as Integer
		  if not WarningDisplayed then
		    rs=app.theDB.DBSQLSelect("Select Racer_Number, TX_Code FROM transponders WHERE TX_Code ='"+TXCode.text+"'")
		    if not rs.EOF then
		      d.Message=rs.Field("TX_Code").StringValue+" already assigned."
		      d.Explanation="A transponder cannot be assigned to more than one racer number."
		      b=d.ShowModal  
		      TXCode.SetFocus
		    end if
		    WarningDisplayed=true
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub GotFocus()
		  WarningDisplayed=false
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbOK
	#tag Event
		Sub Action()
		  dim DataEntryError as Boolean
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  dim MessageStg, DetailsStg as string
		  
		  Dim rg as New RegEx
		  Dim myMatch as RegExMatch
		  
		  WarningDisplayed=true
		  
		  DataEntryError=false
		  if RacerNumber.Text="" then
		    DataEntryError=true
		    MessageStg="Racer Number is missing."
		    DetailsStg="A racer number is required before a transponder can be assigned."
		  end if
		  
		  if not(DataEntryError) then
		    if TXCode.text="" then
		      DataEntryError=true
		      MessageStg="TX Code is missing."
		      DetailsStg="A transponder code is required before a transponder can be assigned."
		    end if
		  end if
		  
		  'if not(DataEntryError) then
		  'select case len(TXCode.Text)
		  'case 8
		  'rg.SearchPattern="[A-Z][A-Z][-]\d\d\d\d\d"
		  'myMatch=rg.search(TXCode.text)
		  'if  myMatch=nil then
		  'DataEntryError=true
		  'MessageStg="The TX Code is invalid."
		  'DetailsStg="Activ transponders are eight characters long and have a format of XX-12345. Elite tranponders are 7 characters long and are all numbers."
		  'end if
		  '
		  'case 7
		  'rg.SearchPattern="\d\d\d\d\d\d\d"
		  'myMatch=rg.search(TXCode.text)
		  'if  myMatch=nil then
		  'DataEntryError=true
		  'MessageStg="The TX Code is invalid."
		  'DetailsStg="Elite tranponders are 7 characters long and are all numbers. Activ transponders are eight characters long and have a format of XX-12345."
		  'end if
		  '
		  'else
		  'DataEntryError=true
		  'MessageStg="The TX Code is invalid."
		  'DetailsStg="The length of the TX code is 7 for and Activ transponder and 8 for an Elite transponder."
		  'end Select
		  'end if
		  
		  if DataEntryError then
		    d.Message=MessageStg
		    d.Explanation=DetailsStg
		    b=d.ShowModal  
		  end if
		  
		  if not(DataEntryError) then
		    if NewTransponder then
		      InsertTransponder
		    else
		      UpdateTransponder
		    end if
		    TXInput.close
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbCancel
	#tag Event
		Sub Action()
		  WarningDisplayed=true
		  TXInput.close
		  wnd_List.InputComplete=true
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="NewTransponder"
		Group="Behavior"
		InitialValue="0"
		Type="boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="OldRacerNumber"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="WarningDisplayed"
		Group="Behavior"
		InitialValue="0"
		Type="boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
