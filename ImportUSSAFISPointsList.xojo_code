#tag Class
Protected Class ImportUSSAFISPointsList
	#tag Method, Flags = &h0
		Function CheckPointsList(NGB as String, NGB_License as String, First_Name as String, Last_Name as String, YOB as Integer) As RecordSet
		  Dim rsPointsList as RecordSet
		  Dim SQL, ReturnData() as String
		  
		  if NGB_License<>"" Then
		    SQL="SELECT * from ngbpoints WHERE ngb='"+NGB+"' AND license='"+NGB_License+"'"
		    rsPointsList=app.theDB.DBSQLSelect(SQL)
		  End if
		  
		  If NGB_License="" or rsPointsList.EOF Then ' see if we can get a name match
		    SQL="SELECT * from ngbpoints WHERE ngb='"+NGB+"' AND first_name LIKE '"+First_Name+"' AND last_name LIKE '"+Last_Name+"' AND YOB LIKE '"+Str(YOB)+"'"
		    rsPointsList=app.theDB.DBSQLSelect(SQL)
		  End If
		  
		  if rsPointsList.EOF Then 'check and see if there are any with the same last name and birth year
		    SQL="SELECT * from ngbpoints WHERE ngb='"+NGB+"' AND last_name LIKE '"+Last_Name+"' AND YOB LIKE '"+Str(YOB)+"'"
		    rsPointsList=app.theDB.DBSQLSelect(SQL)
		  End If
		  
		  Return rsPointsList
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Import()
		  Dim f As FolderItem
		  Dim fileStream As TextOutputStream
		  dim ImportStream as TextInputStream
		  Dim IncomingLine(), HeaderList(), FieldList(), BD(), RacerSuppliedData() as string
		  Dim IncomingBirthDate, IncomingData, ParticipantIDStg, SQLStatement, TAB, USSAMensPath, USSAWomensPath, FISPath, USSALicenseNumber as String
		  Dim i, LineNumber, PointsListIndex as Integer
		  Dim LastNameIdx, FirstNameIdx, DivisionIdx, USSALicenseIdx,GenderIdx,YOBIdx,SprintPointsIdx,DistancePointsIdx,OverallPointsIdx,FISLicenseIdx,NationIdx as Integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  dim rs As RecordSet
		  Dim BirthDate, CurrentDate as date
		  
		  Tab=Chr(9)
		  
		  CurrentDate=new Date
		  
		  ReDim RacerSuppliedData(8)
		  
		  if LocateUSSAImportFolder.stUSSAMensPointsFilePath.text<>"" and LocateUSSAImportFolder.stUSSAWomensPointsFilePath.Text<>"" and LocateUSSAImportFolder.stFISPointsFilePath.text<>"" then
		    USSAMensPath=LocateUSSAImportFolder.stUSSAMensPointsFilePath.text
		    USSAWomensPath=LocateUSSAImportFolder.stUSSAWomensPointsFilePath.Text
		    FISPath=LocateUSSAImportFolder.stFISPointsFilePath.text
		    LogWarnings=LocateUSSAImportFolder.cbLogWarnings.Value
		    LocateUSSAImportFolder.Close
		    
		    SQLStatement="DELETE FROM ngbpoints"
		    App.theDB.DBSQLExecute(SQLStatement)
		    
		    Progress.Show
		    Progress.Initialize("Progress","Loading Points List Files...",4,0)
		    
		    'load the USSA Mens Points list
		    LastNameIdx=-1
		    FirstNameIdx=-1
		    DivisionIdx=-1
		    USSALicenseIdx=-1
		    GenderIdx=-1
		    YOBIdx=-1
		    SprintPointsIdx=-1
		    DistancePointsIdx=-1
		    FISLicenseIdx=-1
		    
		    
		    f=GetFolderItem(USSAMensPath)
		    If f<>Nil then
		      ImportStream=f.OpenAsTextFile
		      IncomingData=ImportStream.ReadAll
		      IncomingData=Uppercase(IncomingData)
		      IncomingLine=IncomingData.Split(chr(13))
		      ImportStream.Close
		      'Check to see if the header line is there and the minimum required fields are there
		      
		      HeaderList=IncomingLine(0).Split(",")
		      
		      LastNameIdx=HeaderList.IndexOf("LAST_NAME")
		      FirstNameIdx=HeaderList.IndexOf("FIRST_NAME")
		      DivisionIdx=HeaderList.IndexOf("DIVISION")
		      USSALicenseIdx=HeaderList.IndexOf("USSA_ID")
		      GenderIdx=HeaderList.IndexOf("GENDER")
		      YOBIdx=HeaderList.IndexOf("YOB")
		      SprintPointsIdx=HeaderList.IndexOf("SPRINT")
		      DistancePointsIdx=HeaderList.IndexOf("DISTANCE")
		      OverallPointsIdx=HeaderList.IndexOf("OVERALL")
		      FISLicenseIdx=HeaderList.IndexOf("FIS_ID")
		      
		      If (LastNameIdx>=0 and FirstNameIdx>=0 and DivisionIdx>=0 and USSALicenseIdx>=0 and GenderIdx>=0 and YOBIdx>=0 and SprintPointsIdx>=0 and DistancePointsIdx>=0 and OverallPointsIdx>=0) then
		        
		        'Loop through the file and import the data
		        'read the data
		        
		        Progress.Initialize("Progress","Loading the USSA Men's Point List...",4,Ubound(IncomingLine))
		        Progress.UpdateProg1(1)
		        For LineNumber = 1 to UBound(IncomingLine)
		          if Len(IncomingLine(LineNumber))>0 then
		            Progress.UpdateProg2(LineNumber)
		            FieldList=IncomingLine(LineNumber).Split(",")
		            for i = 0 to UBound(FieldList)
		              FieldList(i)=Replace(FieldList(i),chr(34),"")
		            next
		            
		            SQLStatement="INSERT INTO ngbpoints (ngb, license, last_name, first_name, country, YOB, gender, sprint, distance, overall) "
		            SQLStatement=SQLStatement+"VALUES ('USSA', '"+Trim(FieldList(USSALicenseIdx))+"', '"+Trim(FieldList(LastNameIdx))+"', '"+Trim(FieldList(FirstNameIdx))+"', '"
		            SQLStatement=SQLStatement+Trim(FieldList(DivisionIdx))+"', '"+Trim(FieldList(YOBIdx))+"', '"+Trim(FieldList(GenderIdx))+"', '"
		            SQLStatement=SQLStatement+Trim(FieldList(SprintPointsIdx))+"', '"+Trim(FieldList(DistancePointsIdx))+"', '"+Trim(FieldList(OverallPointsIdx))+"')"
		            
		            App.theDB.DBSQLExecute(SQLStatement)
		            App.theDB.DBCommit
		            
		          end if
		        Next
		        'end of file
		        
		        
		        'load the USSA Womens Points list
		        LastNameIdx=-1
		        FirstNameIdx=-1
		        DivisionIdx=-1
		        USSALicenseIdx=-1
		        GenderIdx=-1
		        YOBIdx=-1
		        SprintPointsIdx=-1
		        DistancePointsIdx=-1
		        FISLicenseIdx=-1
		        
		        redim IncomingLine(0)
		        redim FieldList(0)
		        redim HeaderList(0)
		        
		        f=GetFolderItem(USSAWomensPath)
		        If f<>Nil then
		          ImportStream=f.OpenAsTextFile
		          IncomingData=ImportStream.ReadAll
		          IncomingData=Uppercase(IncomingData)
		          IncomingLine=IncomingData.Split(chr(13))
		          ImportStream.Close
		          'Check to see if the header line is there and the minimum required fields are there
		          
		          HeaderList=IncomingLine(0).Split(",")
		          
		          LastNameIdx=HeaderList.IndexOf("LAST_NAME")
		          FirstNameIdx=HeaderList.IndexOf("FIRST_NAME")
		          DivisionIdx=HeaderList.IndexOf("DIVISION")
		          USSALicenseIdx=HeaderList.IndexOf("USSA_ID")
		          GenderIdx=HeaderList.IndexOf("GENDER")
		          YOBIdx=HeaderList.IndexOf("YOB")
		          SprintPointsIdx=HeaderList.IndexOf("SPRINT")
		          DistancePointsIdx=HeaderList.IndexOf("DISTANCE")
		          OverallPointsIdx=HeaderList.IndexOf("OVERALL")
		          FISLicenseIdx=HeaderList.IndexOf("FIS_ID")
		          
		          If (LastNameIdx>=0 and FirstNameIdx>=0 and DivisionIdx>=0 and USSALicenseIdx>=0 and GenderIdx>=0 and YOBIdx>=0 and SprintPointsIdx>=0 and DistancePointsIdx>=0 and OverallPointsIdx>=0) then
		            
		            'Loop through the file and import the data
		            'read the data
		            
		            Progress.Initialize("Progress","Loading the USSA Womens's Point List...",4,Ubound(IncomingLine))
		            Progress.UpdateProg1(2)
		            For LineNumber = 1 to UBound(IncomingLine)
		              if Len(IncomingLine(LineNumber))>0 then
		                Progress.UpdateProg2(LineNumber)
		                
		                FieldList=IncomingLine(LineNumber).Split(",")
		                for i = 0 to UBound(FieldList)
		                  FieldList(i)=Replace(FieldList(i),chr(34),"")
		                next
		                
		                SQLStatement="INSERT INTO ngbpoints (ngb, license, last_name, first_name, country, YOB, gender, sprint, distance, overall) "
		                SQLStatement=SQLStatement+"VALUES ('USSA', '"+Trim(FieldList(USSALicenseIdx))+"', '"+Trim(FieldList(LastNameIdx))+"', '"+Trim(FieldList(FirstNameIdx))+"', '"
		                SQLStatement=SQLStatement+Trim(FieldList(DivisionIdx))+"', '"+Trim(FieldList(YOBIdx))+"', '"+Trim(FieldList(GenderIdx))+"', '"
		                SQLStatement=SQLStatement+Trim(FieldList(SprintPointsIdx))+"', '"+Trim(FieldList(DistancePointsIdx))+"', '"+Trim(FieldList(OverallPointsIdx))+"')"
		                
		                App.theDB.DBSQLExecute(SQLStatement)
		                App.theDB.DBCommit
		                
		              end if
		            Next
		            'end of file
		            
		            'load the FIS Points list
		            GenderIdx=-1
		            FISLicenseIdx=-1
		            LastNameIdx=-1
		            FirstNameIdx=-1
		            NationIdx=-1
		            YOBIdx=-1
		            DistancePointsIdx=-1
		            SprintPointsIdx=-1
		            
		            redim IncomingLine(0)
		            redim FieldList(0)
		            redim HeaderList(0)
		            
		            f=GetFolderItem(FISPath)
		            If f<>Nil then
		              ImportStream=f.OpenAsTextFile
		              IncomingData=ImportStream.ReadAll
		              IncomingData=Uppercase(IncomingData)
		              IncomingLine=IncomingData.Split(chr(13))
		              ImportStream.Close
		              'Check to see if the header line is there and the minimum required fields are there
		              
		              HeaderList=IncomingLine(0).Split(Tab)
		              
		              GenderIdx=HeaderList.IndexOf("GENDER")
		              FISLicenseIdx=HeaderList.IndexOf("FISCODE")
		              if FISLicenseIdx<0 then
		                FISLicenseIdx=HeaderList.IndexOf("FIS CODE")
		              end if
		              LastNameIdx=HeaderList.IndexOf("LASTNAME")
		              FirstNameIdx=HeaderList.IndexOf("FIRSTNAME")
		              NationIdx=HeaderList.IndexOf("NATION")
		              YOBIdx=HeaderList.IndexOf("YOB")
		              DistancePointsIdx=HeaderList.IndexOf("DI Pts")
		              SprintPointsIdx=HeaderList.IndexOf("SP Pts")
		              
		              If (GenderIdx>=0 and FISLicenseIdx>=0 and LastNameIdx>=0 and FirstNameIdx>=0 and NationIdx>=0 and YOBIdx>=0 and DistancePointsIdx>=0 and SprintPointsIdx>=0) then
		                
		                'Loop through the file and import the data
		                'read the data
		                
		                Progress.Initialize("Progress","Loading the FIS Point List...",4,Ubound(IncomingLine))
		                Progress.UpdateProg1(3)
		                For LineNumber = 1 to UBound(IncomingLine)
		                  Progress.UpdateProg2(LineNumber)
		                  
		                  FieldList=IncomingLine(LineNumber).Split(Tab)
		                  for i = 0 to UBound(FieldList)
		                    FieldList(i)=Replace(FieldList(i),chr(34),"")
		                  next
		                  
		                  if Trim(FieldList(GenderIdx))="L" then
		                    FieldList(GenderIdx)="F"
		                  else
		                    FieldList(GenderIdx)="M"
		                  end if
		                  
		                  
		                  SQLStatement="INSERT INTO ngbpoints (ngb, license, last_name, first_name, country, YOB, gender, sprint, distance, overall) "
		                  SQLStatement=SQLStatement+"VALUES ('FIS', '"+Trim(FieldList(FISLicenseIdx))+"', '"+Trim(FieldList(LastNameIdx))+"', '"+Trim(FieldList(FirstNameIdx))+"', '"
		                  SQLStatement=SQLStatement+Trim(FieldList(NationIdx))+"', '"+Trim(FieldList(YOBIdx))+"', '"+Trim(FieldList(GenderIdx))+"', '"
		                  SQLStatement=SQLStatement+Trim(FieldList(SprintPointsIdx))+"', '"+Trim(FieldList(DistancePointsIdx))+"', '"+Trim(FieldList(OverallPointsIdx))+"')"
		                  
		                  App.theDB.DBSQLExecute(SQLStatement)
		                  App.theDB.DBCommit
		                  
		                Next
		                'end of file
		                
		              else
		                'dialog tell them missing data
		                
		                
		                d.Icon=2 'stop icon
		                d.Message="The FIS Points List does not appear to be in the proper format."
		                d.Explanation="The file needs to be tab seperated columns and containing SEX, FISCODE, LASTNAME, FIRSTNAME, NAT, BIRTHDATE, DIPOINTS, SPPOINTS column headers."
		                b=d.ShowModal
		                
		              end if
		              
		            End if
		            
		          else
		            'dialog tell them missing data
		            
		            
		            d.Icon=2 'stop icon
		            d.Message="The USSA Women's Points List does not appear to be in the proper format."
		            d.Explanation="The file needs to be tab seperated columns and containing LAST_NAME, FIRST_NAME, DIVISION, USSA_ID, GENDER, YOB, SPRINT, DISTANCE, OVERALL, FIS_ID column headers."
		            b=d.ShowModal
		            
		          end if
		          
		        End if
		        
		        
		      else
		        'dialog tell them missing data
		        
		        
		        d.Icon=2 'stop icon
		        d.Message="The USSA Men's Points List does not appear to be in the proper format."
		        d.Explanation="The file needs to be tab seperated columns and containing LAST_NAME, FIRST_NAME, DIVISION, USSA_ID, GENDER, YOB, SPRINT, DISTANCE, OVERALL, FIS_ID column headers."
		        b=d.ShowModal
		        
		      end if
		      
		      Progress.Close
		    End if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ValidateAllRacers()
		  Dim rsParticipant as RecordSet
		  Dim i, PointsUpdated, WarningsCreated as Integer
		  Dim Note, Result(), SQL, Warning as String
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  
		  'Find Participant row
		  SQL="SELECT rowid, NGB_License_Number, NGB2_License_Number, Note FROM participants"
		  rsParticipant=app.theDB.DBSQLSelect(SQL)
		  
		  Progress.Show
		  Progress.Initialize("Progress","Verifying NGB License and Points...",rsParticipant.RecordCount,-1)
		  
		  For i = 1 to rsParticipant.RecordCount
		    Progress.UpdateProg1(i)
		    
		    
		    'USSA Points
		    
		    Warning=ValidatePoints(rsParticipant.Field("rowid").IntegerValue, "USSA", rsParticipant.Field("NGB_License_Number").StringValue)
		    
		    if (InStr(Warning,"Warning:")=0) then
		      Result=Warning.Split("|")
		      if ubound(Result)>=0 then
		        
		        SQL="UPDATE participants SET NGB_License_Number='"+Result(0)+"', NGB1_Points='"+Result(1)+"' WHERE rowid="+rsParticipant.Field("rowid").StringValue
		        
		        PointsUpdated=PointsUpdated+1
		      end if
		      
		    else
		      
		      if  rsParticipant.Field("Note").StringValue<>"" Then
		        Note=Warning+" "+rsParticipant.Field("Note").StringValue
		      Else
		        Note=Warning
		      End If
		      SQL="UPDATE participants SET Note='"+Note+"' WHERE rowid="+rsParticipant.Field("rowid").StringValue
		      
		      WarningsCreated=WarningsCreated+1
		      
		    end if
		    
		    
		    'FIS points
		    
		    app.theDB.DBSQLExecute(SQL)
		    
		    Warning=ValidatePoints(rsParticipant.Field("rowid").IntegerValue, "FIS", rsParticipant.Field("NGB2_License_Number").StringValue)
		    
		    if (InStr(Warning,"Warning:")=0) then
		      Result=Warning.Split("|")
		      if ubound(Result)>=0 then
		        
		        SQL="UPDATE participants SET NGB2_License_Number='"+Result(0)+"', NGB2_Points='"+Result(1)+"' WHERE rowid="+rsParticipant.Field("rowid").StringValue
		        
		        PointsUpdated=PointsUpdated+1
		      end if
		      
		    else
		      
		      if  rsParticipant.Field("Note").StringValue<>"" Then
		        Note=Warning+" "+rsParticipant.Field("Note").StringValue
		      Else
		        Note=Warning
		      End If
		      SQL="UPDATE participants SET Note='"+Note+"' WHERE rowid="+rsParticipant.Field("rowid").StringValue
		      
		      WarningsCreated=WarningsCreated+1
		      
		    end if
		    
		    app.theDB.DBSQLExecute(SQL)
		    
		    rsParticipant.MoveNext
		    
		  Next
		  
		  app.theDB.DBCommit
		  
		  Progress.Close
		  
		  d.Icon=1 'caution icon
		  d.Message="Verifying NGB License Points compete"
		  d.Explanation=str(PointsUpdated)+" Points Updated"+chr(13)+str(WarningsCreated)+" Warnings Posted"
		  b=d.ShowModal
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ValidatePoints(ParticipantID as Integer, NGB as String, NGBLicense as String) As String
		  Dim rsParticipant, rsPointsList as RecordSet
		  Dim Warning, SQL as String
		  
		  'Find Participant row
		  SQL="SELECT Participant_Name, First_Name, Birth_Date FROM participants WHERE participants.rowid="+str(ParticipantID)
		  rsParticipant=app.theDB.DBSQLSelect(SQL)
		  
		  if not(rsParticipant.EOF) then
		    
		    'If has a number lookup number
		    rsPointsList=CheckPointsList(NGB,NGBLicense,rsParticipant.Field("First_Name").StringValue, _
		    rsParticipant.Field("Participant_Name").StringValue, rsParticipant.Field("Birth_Date").DateValue.Year)
		    
		    Select Case rsPointsList.RecordCount
		      
		    Case 0
		      if NGBLicense<>"" then
		        Warning=NGBLicense+"|inact"
		      else
		        Warning="|"
		      end if
		      
		    Case 1
		      'If YOB, Gender, First and Last Name match update USSA points ELSE post an Warning/warning
		      If rsParticipant.Field("Participant_Name").StringValue = rsPointsList.Field("last_name").StringValue and _
		        Str(rsParticipant.Field("Birth_Date").DateValue.Year)=rsPointsList.Field("YOB").StringValue Then
		        
		        Select case wnd_List.pmUP.Text
		        Case "Sprint"
		          Warning=rsPointsList.Field("license").StringValue
		          If NGB ="FIS" and rsPointsList.Field("sprint").IntegerValue=0 Then
		            Warning=Warning+"|176"
		          ElseIf NGB ="USSA" and rsPointsList.Field("sprint").IntegerValue=0 Then
		            Warning=Warning+"|990"
		          Else
		            Warning=Warning+"|"+rsPointsList.Field("sprint").StringValue
		          End If
		          
		        Case "Distance"
		          Warning=rsPointsList.Field("license").StringValue
		          If NGB ="FIS" and rsPointsList.Field("distance").IntegerValue=0 Then
		            Warning=Warning+"|176"
		          ElseIf NGB ="USSA" and rsPointsList.Field("distance").IntegerValue=0 Then
		            Warning=Warning+"|990"
		          Else
		            Warning=Warning+"|"+rsPointsList.Field("distance").StringValue
		          End If
		          
		          
		        Case "Overall/Distance"
		          
		          Warning=rsPointsList.Field("license").StringValue
		          If NGB ="FIS" and rsPointsList.Field("distance").IntegerValue=0 Then
		            Warning=Warning+"|176"
		          ElseIf NGB ="USSA" and rsPointsList.Field("overall").IntegerValue=0 Then
		            Warning=Warning+"|990"
		          Else
		            If NGB="USSA" Then
		              Warning=Warning+"|"+rsPointsList.Field("overall").StringValue
		            Else
		              Warning=Warning+"|"+rsPointsList.Field("distance").StringValue
		            End If
		          End If
		          
		        End Select
		        
		      Else
		        Warning="Warning: Registered information and "+NGB+" points list information do not match: "+ _
		        chr(13)+"Entered Last Name: "+rsParticipant.Field("Participant_Name").StringValue + " Points List Last Name: "+ rsPointsList.Field("last_name").StringValue + _
		        chr(13)+"Entered YOB: "+Str(rsParticipant.Field("Birth_Date").DateValue.Year) + " Points List YOB: "+ rsPointsList.Field("YOB").StringValue 
		      End If
		    Else
		      Warning="Warning: Multiple people on "+NGB+" points list matching this racer: "
		      
		      Do
		        Warning=Warning+chr(13)+rsPointsList.Field("license").StringValue + " "+rsPointsList.Field("first_name").StringValue + " "+ rsPointsList.Field("last_name").StringValue+ " "+ rsPointsList.Field("YOB").StringValue
		        rsPointsList.MoveNext
		      Loop Until rsPointsList.EOF
		      
		      
		    End Select
		    
		    
		    
		  End If
		  
		  Return Warning
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		ErrorOutput As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Errors As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		LogWarnings As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		PtsBirthDate() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PtsDistancePoints() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PtsDivision() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PtsFirstName() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PtsFISLicense() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PtsGender() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PtsLastName() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PtsNation() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PtsOverallPoints() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PtsSprintPoints() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PtsUSSALicense() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		PtsYOB() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Warnings As Integer = 0
	#tag EndProperty


	#tag Constant, Name = rsdBirthDate, Type = Double, Dynamic = False, Default = \"7", Scope = Public
	#tag EndConstant

	#tag Constant, Name = rsdDivisionName, Type = Double, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant

	#tag Constant, Name = rsdFISLicense, Type = Double, Dynamic = False, Default = \"3", Scope = Public
	#tag EndConstant

	#tag Constant, Name = rsdGender, Type = Double, Dynamic = False, Default = \"6", Scope = Public
	#tag EndConstant

	#tag Constant, Name = rsdRaceDistanceName, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = rsdRacerName, Type = Double, Dynamic = False, Default = \"5", Scope = Public
	#tag EndConstant

	#tag Constant, Name = rsdRacerNumber, Type = Double, Dynamic = False, Default = \"4", Scope = Public
	#tag EndConstant

	#tag Constant, Name = rsdRowid, Type = Double, Dynamic = False, Default = \"8", Scope = Public
	#tag EndConstant

	#tag Constant, Name = rsdUSSALicense, Type = Double, Dynamic = False, Default = \"2", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="ErrorOutput"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Errors"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LogWarnings"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Warnings"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
