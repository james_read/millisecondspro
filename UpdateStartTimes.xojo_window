#tag Window
Begin Window UpdateStartTimes
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   8
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   595
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   595
   MaximizeButton  =   True
   MaxWidth        =   480
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   595
   MinimizeButton  =   True
   MinWidth        =   480
   Placement       =   0
   Resizeable      =   True
   Title           =   "Update Start Times...."
   Visible         =   True
   Width           =   740
   Begin PushButton pbOK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Update"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   380
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   533
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PushButton pbCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "Done"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   290
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   533
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin DateTimeEditField ActTimeFrom
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   175
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   143
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   False
      Width           =   196
   End
   Begin DateTimeEditField ActTimeTo
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   175
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   181
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   False
      Width           =   196
   End
   Begin ListBox DataList
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   3
      ColumnsResizable=   False
      ColumnWidths    =   "0,30,220"
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   300
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   198
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   "0"
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   71
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   250
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin DateTimeEditField StartTime
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   134
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   418
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   196
   End
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   22
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "New Start Time:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   418
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin CheckBox cbAssignedStart
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Assigned Start"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   149
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      State           =   1
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   453
      Underline       =   False
      Value           =   True
      Visible         =   True
      Width           =   137
   End
   Begin CheckBox cbActualStart
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Actual Start"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   149
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      State           =   0
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   485
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   137
   End
   Begin CheckBox cbFemale
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Female"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   False
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   42
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      State           =   0
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   183
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   80
   End
   Begin BevelButton bbSelectAll
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Deselect All"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   281
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   376
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   100
   End
   Begin CheckBox cbMixed
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Mixed"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   False
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   42
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      State           =   0
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   224
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   80
   End
   Begin BevelButton bbCaptureTime
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Capture Time"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   342
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   418
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   106
   End
   Begin PopupMenu UpdateBy
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   62
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   15
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   23
      Underline       =   False
      Visible         =   True
      Width           =   200
   End
   Begin ListBox FoundStartTimesList
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   1
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   True
      EnableDragReorder=   False
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   300
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   492
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   "0"
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   16
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   71
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   False
      Width           =   201
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin Label CategoryTitle
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   259
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   17
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Categories"
      TextAlign       =   1
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   49
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   112
   End
   Begin Label FoundStartTimes
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   530
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   18
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Found Start Times"
      TextAlign       =   1
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   49
      Transparent     =   False
      Underline       =   False
      Visible         =   False
      Width           =   120
   End
   Begin DateTimeEditField StartTime11
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   116
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   19
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   -132
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   196
   End
   Begin Label stActFrom
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   24
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   20
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Actual Start Time From:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   143
      Transparent     =   False
      Underline       =   False
      Visible         =   False
      Width           =   146
      Begin CheckBox cbMale
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Male"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   False
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "stActFrom"
         Italic          =   False
         Left            =   42
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         State           =   0
         TabIndex        =   9
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   143
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   80
      End
   End
   Begin Label stActFrom1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   28
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   21
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Actual Start Time From:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   -132
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   146
   End
   Begin Label stActTo
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   24
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   22
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Actual Start Time To:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   181
      Transparent     =   False
      Underline       =   False
      Visible         =   False
      Width           =   146
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  SynchronizedMicroseconds=Microseconds
		  
		  DataList.ColumnType(1)=2 'Make column one an CheckBox
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Sub ExecuteQuery(Selection as integer)
		  dim i as integer
		  dim SelectStatement as String
		  dim recCount as integer
		  dim res as boolean
		  dim rsQueryResults as RecordSet
		  
		  DataList.DeleteAllRows
		  
		  select case Selection
		  case 0 'update by class
		    
		    CategoryTitle.Text="Categories"
		    SelectStatement="SELECT Division_Name, rowid FROM divisions WHERE RaceID =1 ORDER BY List_Order ASC"
		    
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		    while not rsQueryResults.EOF
		      DataList.AddRow ""
		      DataList.Cell(DataList.lastindex,0)=rsQueryResults.Field("rowid").stringValue
		      DataList.CellCheck(DataList.lastindex,1)=True
		      DataList.Cell(DataList.lastindex,2)=rsQueryResults.Field("Division_Name").stringValue
		      rsQueryResults.moveNext
		    wend
		    rsQueryResults.Close
		    
		  case 1, 2 'update by Race Distance and Gender
		    CategoryTitle.Text="Categories"
		    
		    SelectStatement="SELECT RaceDistance_Name, rowid FROM racedistances WHERE RaceID =1"
		    
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		    while not rsQueryResults.EOF
		      DataList.AddRow ""
		      DataList.Cell(DataList.lastindex,0)=rsQueryResults.Field("rowid").stringValue
		      DataList.CellCheck(DataList.lastindex,1)=True
		      DataList.Cell(DataList.lastindex,2)=rsQueryResults.Field("RaceDistance_Name").stringValue
		      rsQueryResults.moveNext
		    wend
		    rsQueryResults.Close
		    
		  case 4
		    CategoryTitle.Text="Participants"
		    SelectStatement="SELECT Racer_Number, Participant_Name, First_Name, rowid FROM participants WHERE RaceID =1 ORDER BY Racer_Number ASC"
		    
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		    while not rsQueryResults.EOF
		      DataList.AddRow ""
		      DataList.Cell(DataList.lastindex,0)=rsQueryResults.Field("rowid").stringValue
		      DataList.CellCheck(DataList.lastindex,1)=False
		      DataList.Cell(DataList.lastindex,2)=rsQueryResults.Field("Racer_Number").stringValue+" "+rsQueryResults.Field("Participant_Name").stringValue+", "+rsQueryResults.Field("First_Name").stringValue
		      rsQueryResults.moveNext
		    wend
		    rsQueryResults.Close
		    
		  case 5 'update by wave
		    
		    CategoryTitle.Text="Wave"
		    SelectStatement="SELECT DISTINCT Wave FROM participants ORDER BY Wave"
		    
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		    while not rsQueryResults.EOF
		      DataList.AddRow ""
		      DataList.Cell(DataList.lastindex,0)=rsQueryResults.Field("Wave").stringValue
		      DataList.CellCheck(DataList.lastindex,1)=True
		      DataList.Cell(DataList.lastindex,2)=rsQueryResults.Field("Wave").stringValue
		      rsQueryResults.moveNext
		    wend
		    rsQueryResults.Close
		    
		  end select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub UpdateRacerDataStartTimes(DivisionID as integer)
		  dim i,recCount as integer
		  dim newStartTime,SQLQuery,SQLUpdate as string
		  dim rsQueryResults,rsRacerData as RecordSet
		  dim res as Boolean
		  dim temp as string
		  dim Difference as Double
		  
		  SQLQuery="Select Start_Time from divisions WHERE RaceID=1 AND rowid="+str(DivisionId)
		  rsQueryResults=app.theDB.DBSQLSelect(SQLQuery)
		  Difference=app.ConvertTimeToSeconds(StartTime.Text)-app.ConvertTimeToSeconds(rsQueryResults.field("Start_Time").StringValue)-app.AMBOffset
		  SQLQuery="Select TeamID, Racer_Number, Assigned_Start, rowid FROM participants WHERE RaceID=1 AND DivisionID="+str(DivisionId)
		  rsRacerData=app.theDB.DBSQLSelect(SQLQuery)
		  
		  For i=1 to rsRacerData.RecordCount
		    'newStartTime=app.CalculateStartTime(rsRacerData.field("Racer_Number").IntegerValue,rsQueryResults.field("Start_Time").StringValue)
		    if app.RacersPerStart<1 or app.RacersPerStart>4 then
		      newStartTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(rsQueryResults.field("Start_Time").StringValue)+Difference,true)
		    else
		      newStartTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(rsQueryResults.field("Start_Time").StringValue)+app.ConvertTimeToSeconds(right(rsRacerData.field("Assigned_Start").StringValue,12))+Difference,true)
		    end if
		    SQLUpdate="UPDATE participants SET "
		    if cbAssignedStart.Value then
		      SQLUpdate=SQLUpdate+" Assigned_Start='"+newStartTime+"' "
		    end if
		    if cbActualStart.Value then
		      if SQLUpdate<>"" then
		        SQLUpdate=SQLUpdate+","
		      end if
		      SQLUpdate=SQLUpdate+" Actual_Start='"+newStartTime+"' "
		    end if
		    SQLUpdate=SQLUpdate+" WHERE RaceID=1 AND rowid="+rsRacerData.Field("rowid").StringValue
		    App.theDB.DBSQLExecute(SQLUpdate)
		    
		    if (wnd_List.ResultType.Text="Cross Country Running") then
		      SQLUpdate="UPDATE ccTeamIntervals SET Total_Points=0, Number_Scorers=0 WHERE TeamID="+rsRacerData.Field("TeamID").StringValue
		      App.theDB.DBSQLExecute(SQLUpdate)
		    end if
		    rsRacerData.MoveNext
		  next
		  App.theDB.DBSQLExecute("Update divisions SET Actual_Start_Time='"+StartTime.Text+"' WHERE RaceID=1 AND DivisionID="+str(DivisionId))
		  rsRacerData.Close
		  rsQueryResults.Close
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub UpdateRacerDataStartTimesActStart()
		  dim i,recCount as integer
		  dim newStartTime,SQLQuery,SQLUpdate as string
		  dim rsQueryResults,rsRacerData as RecordSet
		  dim res as Boolean
		  dim temp as string
		  
		  SQLQuery="Select Racer_Number, Assigned_Start, rowid FROM participants WHERE Actual_Start>='"+ActTimeFrom.Text+"' AND Actual_Start<='"+ActTimeTo.Text+"'"
		  rsRacerData=app.theDB.DBSQLSelect(SQLQuery)
		  
		  For i=1 to rsRacerData.RecordCount
		    SQLUpdate="UPDATE participants SET "
		    if cbAssignedStart.Value then
		      SQLUpdate=SQLUpdate+" Assigned_Start='"+StartTime.text+"'"
		    end if
		    if cbActualStart.Value then
		      if SQLUpdate<>"" then
		        SQLUpdate=SQLUpdate+","
		      end if
		      SQLUpdate=SQLUpdate+" Actual_Start='"+StartTime.text+"'"
		    end if
		    SQLUpdate=SQLUpdate+" WHERE RaceID=1 AND rowid="+rsRacerData.Field("rowid").StringValue
		    App.theDB.DBSQLExecute(SQLUpdate)
		    rsRacerData.MoveNext
		  next
		  rsRacerData.Close
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateRacerDataStartTimesParticipants(ParticipantID as string)
		  dim i,recCount as integer
		  dim newStartTime,SQLQuery,SQLUpdate as string
		  dim rsRacerData as RecordSet
		  dim res as Boolean
		  dim temp as string
		  dim Difference as Double
		  
		  
		  SQLUpdate="UPDATE participants SET "
		  if cbAssignedStart.Value then
		    SQLUpdate=SQLUpdate+" Assigned_Start='"+StartTime.text+"'"
		  end if
		  if cbActualStart.Value then
		    if SQLUpdate<>"" then
		      SQLUpdate=SQLUpdate+","
		    end if
		    SQLUpdate=SQLUpdate+" Actual_Start='"+StartTime.text+"'"
		  end if
		  SQLUpdate=SQLUpdate+" WHERE rowid="+ParticipantID
		  App.theDB.DBSQLExecute(SQLUpdate)
		  App.theDB.DBCommit
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateRacerDataStartTimesWave(Wave as string)
		  dim SQLUpdate as string
		  
		  SQLUpdate="UPDATE participants SET "
		  if cbAssignedStart.Value then
		    SQLUpdate=SQLUpdate+" Assigned_Start='"+StartTime.text+"'"
		  end if
		  if cbActualStart.Value then
		    if SQLUpdate<>"" then
		      SQLUpdate=SQLUpdate+","
		    end if
		    SQLUpdate=SQLUpdate+" Actual_Start='"+StartTime.text+"'"
		  end if
		  SQLUpdate=SQLUpdate+" WHERE Wave='"+Wave+"'"
		  App.theDB.DBSQLExecute(SQLUpdate)
		  App.theDB.DBCommit
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected SynchronizedMicroseconds As double
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected SynchronizedTime As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected TotalSecondsPlusOne As double
	#tag EndProperty


#tag EndWindowCode

#tag Events pbOK
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog 
		  dim b as MessageDialogButton
		  dim SQLQuery as string
		  dim rsResults as RecordSet
		  dim i,recCount as integer
		  dim res as Boolean
		  
		  
		  if (cbActualStart.Value) or (cbAssignedStart.value) then
		    
		    Progress.Show
		    Progress.Initialize("Updating Start Times","Updating Start Times...",DataList.ListCount,-1)
		    
		    for i=0 to DataList.ListCount-1
		      Progress.UpdateProg1(i+1)
		      if DataList.CellCheck(i,1) then
		        
		        select case UpdateBy.ListIndex
		        case 0   'update by Division
		          UpdateRacerDataStartTimes(val(DataList.Cell(i,0)))
		          
		        case 1  'update by race distance
		          SQLQuery="Select rowid from divisions WHERE RaceID=1 AND RaceDistanceID="+DataList.Cell(i,0)
		          rsResults=app.theDB.DBSQLSelect(SQLQuery)
		          while not(rsResults.eof)
		            UpdateRacerDataStartTimes(rsResults.Field("rowid").IntegerValue)
		            rsResults.MoveNext
		          wend
		          
		        case 2 'update by gender
		          if cbMale.value then
		            SQLQuery="Select rowid from divisions WHERE RaceID=1 AND RaceDistanceID="+DataList.Cell(i,0)+" AND Gender='M'"
		            rsResults=app.theDB.DBSQLSelect(SQLQuery)
		            while not(rsResults.eof)
		              UpdateRacerDataStartTimes(rsResults.Field("rowid").IntegerValue)
		              rsResults.MoveNext
		            wend
		          end if
		          if cbFemale.value then
		            SQLQuery="Select rowid from divisions WHERE RaceID=1 AND RaceDistanceID="+DataList.Cell(i,0)+" AND Gender='F'"
		            rsResults=app.theDB.DBSQLSelect(SQLQuery)
		            while not(rsResults.eof)
		              UpdateRacerDataStartTimes(rsResults.Field("rowid").IntegerValue)
		              rsResults.MoveNext
		            wend
		          end if
		          if cbMixed.value then
		            SQLQuery="Select rowid from divisions WHERE RaceID=1 AND RaceDistanceID="+DataList.Cell(i,0)+" AND Gender='X'"
		            rsResults=app.theDB.DBSQLSelect(SQLQuery)
		            
		            while not(rsResults.eof)
		              UpdateRacerDataStartTimes(rsResults.Field("rowid").IntegerValue)
		              rsResults.MoveNext
		            wend
		          end if
		          
		        case 4  'update by participants
		          UpdateRacerDataStartTimesParticipants(DataList.Cell(i,0))
		          
		        case 5 'update by wave
		          UpdateRacerDataStartTimesWave(DataList.Cell(i,0))
		          
		        end select
		        
		      end if
		    next
		    
		    if UpdateBy.ListIndex =3 then 'update by Actual Start Time
		      UpdateRacerDataStartTimesActStart
		    end if
		    
		    app.theDB.DBCommit
		    Progress.Close
		  else
		    d.ActionButton.Caption="OK"
		    d.Message="Please select a start time to update."
		    b=d.ShowModal 
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbCancel
	#tag Event
		Sub Action()
		  UpdateStartTimes.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ActTimeFrom
	#tag Event
		Sub Open()
		  me.text=app.RaceDate.SQLDate+" 00:00:00.000"
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ActTimeTo
	#tag Event
		Sub Open()
		  me.text=app.RaceDate.SQLDate+" 00:00:00.000"
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events StartTime
	#tag Event
		Sub Open()
		  me.text=app.RaceDate.SQLDate+" 00:00:00.000"
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbActualStart
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog 
		  Dim b as MessageDialogButton 
		  
		  if cbActualStart.Value then
		    d.icon=1  
		    d.CancelButton.Visible=True    
		    d.Message="Update Actual Start times should only be checked if there are no electronic times available."
		    d.Explanation="Updating Actual Start times may override the electronic start times." 
		    
		    b=d.ShowModal    
		    Select Case b 
		    Case d.ActionButton
		      //user pressed OK
		    Case d.CancelButton 
		      cbActualStart.Value=false
		    End select
		  end if
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbSelectAll
	#tag Event
		Sub Action()
		  dim i as integer
		  
		  for i=0 to DataList.ListCount-1
		    DataList.CellCheck(i,1)=false
		  next
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbCaptureTime
	#tag Event
		Sub Action()
		  dim Tenths as Double
		  dim ElapsedMicroseconds as Double
		  dim ElapsedTime as new date
		  
		  ElapsedMicroseconds=Microseconds-SynchronizedMicroseconds
		  ElapsedTime.TotalSeconds=ElapsedTime.TotalSeconds-app.AMBOffset
		  Tenths=val("0."+left(Format(ElapsedMicroseconds,"###############"),1))
		  StartTime.text=app.ConvertSecondsToTime((ElapsedTime.TotalSeconds+Tenths),true)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events UpdateBy
	#tag Event
		Sub Open()
		  UpdateBy.AddRow "Update by Division"
		  UpdateBy.AddRow "Update by Distance"
		  UpdateBy.AddRow "Update by Gender"
		  UpdateBy.AddRow "Update by Actual Start Time"
		  UpdateBy.AddRow "Update by Participant"
		  UpdateBy.AddRow "Update by Wave"
		  UpdateBy.ListIndex=0
		  ExecuteQuery(Me.ListIndex)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Change()
		  
		  stActFrom.Visible=false
		  stActTo.Visible=false
		  ActTimeFrom.Visible=false
		  ActTimeTo.Visible=false
		  cbMale.Visible=true
		  cbFemale.Visible=true
		  cbMixed.Visible=true
		  DataList.Visible=true
		  bbSelectAll.Visible=true
		  CategoryTitle.Visible=true
		  
		  if me.ListIndex=2 then
		    cbMale.Enabled=true
		    cbFemale.Enabled=true
		    cbMixed.Enabled=true
		    cbMale.Value=true
		    cbFemale.Value=true
		    cbMixed.Value=true
		  elseif me.ListIndex=3 then
		    stActFrom.Visible=true
		    stActTo.Visible=True
		    ActTimeFrom.Visible=true
		    ActTimeTo.Visible=true
		    cbMale.Visible=false
		    cbFemale.Visible=False
		    cbMixed.Visible=False
		    DataList.Visible=false
		    bbSelectAll.Visible=false
		    CategoryTitle.Visible=false
		    
		  else
		    cbMale.Enabled=False
		    cbFemale.Enabled=false
		    cbMixed.Enabled=false
		    cbMale.Value=false
		    cbFemale.Value=false
		    cbMixed.Value=false
		  end if
		  ExecuteQuery(Me.ListIndex)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events FoundStartTimesList
	#tag Event
		Sub Open()
		  dim SelectStatement as string
		  dim rsQueryResults as RecordSet
		  dim recCount as integer
		  dim res as Boolean
		  
		  SelectStatement="SELECT Actual_Time FROM times WHERE RaceID =1"
		  SelectStatement=SelectStatement+" AND (TX_Code='9991' OR TX_Code='9992') ORDER BY Actual_Time ASC"
		  rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		  
		  if not rsQueryResults.EOF then
		    FoundStartTimes.Visible=true
		    FoundStartTimesList.Visible=true
		    self.Width=740
		    pbCancel.Left=540
		    pbOK.Left=630
		    while not rsQueryResults.EOF
		      FoundStartTimesList.AddRow rsQueryResults.Field("Actual_Time").stringValue
		      rsQueryResults.moveNext
		    wend
		    rsQueryResults.Close
		  else
		    self.Width=480
		    pbCancel.Left=290
		    pbOK.Left=380
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Function DragRow(drag As DragItem, row As Integer) As Boolean
		  Drag.Text=me.list(Row)
		  return true
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events StartTime11
	#tag Event
		Sub Open()
		  me.text=app.RaceDate.SQLDate+" 00:00:00.000"
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
