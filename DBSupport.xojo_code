#tag Class
Protected Class DBSupport
	#tag Method, Flags = &h0
		Sub DBClose()
		  select case DatabaseType
		    
		  case "MySQLDatabase"
		    if MySQLDB<>nil then
		      if MySQLDB.DatabaseName<>"" then
		        MySQLDB.Close
		      end if
		    end if
		    
		  case "REALSQLDatabase"
		    if RealSQLDB<>nil then
		      if RealSQLDB.DatabaseFile<>nil then
		        RealSQLDB.Close
		      end if
		    end if
		    
		  end Select
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DBCommit()
		  select case DatabaseType
		    
		  case "MySQLDatabase"
		    MySQLDB.Commit
		    
		  case "REALSQLDatabase"
		    RealSQLDB.Commit
		    
		  end Select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DBLastRowID() As Integer
		  Dim LastRowIDNumber as Integer
		  
		  select case DatabaseType
		    
		  case "MySQLDatabase"
		    LastRowIDNumber=MySQLDB.GetInsertID
		    
		  case "REALSQLDatabase"
		    LastRowIDNumber=RealSQLDB.LastRowID
		    
		  end Select
		  
		  return LastRowIDNumber
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DBSQLExecute(SQLStatement as string)
		  select case DatabaseType
		    
		  case "MySQLDatabase"
		    MySQLDB.SQLExecute(SQLStatement)
		    
		  case "REALSQLDatabase"
		    RealSQLDB.SQLExecute(SQLStatement)
		    
		  end Select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function DBSQLSelect(SQLStatement as string) As RecordSet
		  Dim rsRecs as RecordSet
		  
		  select case DatabaseType
		    
		  case "MySQLDatabase"
		    SQLStatement=Replace(SQLStatement,"COUNT()","COUNT(*)")
		    rsRecs=MySQLDB.SQLSelect(SQLStatement)
		    
		  case "REALSQLDatabase"
		    rsRecs=RealSQLDB.SQLSelect(SQLStatement)
		    
		  end Select
		  
		  return rsRecs
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MySQLDatabase_New()
		  
		  'delete the old tables
		  MySQLDB.SQLExecute("DROP TABLE divisions")
		  MySQLDB.SQLExecute("DROP TABLE intervals")
		  MySQLDB.SQLExecute("DROP TABLE participants")
		  MySQLDB.SQLExecute("DROP TABLE racedistances")
		  MySQLDB.SQLExecute("DROP TABLE races")
		  MySQLDB.SQLExecute("DROP TABLE times")
		  MySQLDB.SQLExecute("DROP TABLE transponders")
		  
		  
		  'create new tables
		  MySQLDB.SQLExecute(mySQL_SetForeignKeyChecks0)
		  MySQLDB.SQLExecute(mySQLSchema_Divisions)
		  MySQLDB.SQLExecute(mySQLSchema_Intervals)
		  MySQLDB.SQLExecute(mySQLSchema_NGBPoints)
		  MySQLDB.SQLExecute(mySQLSchema_Participants)
		  MySQLDB.SQLExecute(mySQLSchema_RaceDistances)
		  MySQLDB.SQLExecute(mySQLSchema_Races)
		  MySQLDB.SQLExecute(mySQLSchema_Times)
		  MySQLDB.SQLExecute(mySQLSchema_Transponders)
		  MySQLDB.SQLExecute(mySQLSchema_CCTeams)
		  MySQLDB.SQLExecute(mySQLSchema_CCTeamIntervals)
		  MySQLDB.SQLExecute(mySQL_SetForeignKeyChecks1)
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MySQL_Connect()
		  ConnectToServer.Show
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MySQL_Disconnect()
		  MySQLDB.Close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub MySQL_MultiTreadingOff()
		  'MySQLDB.MultiThreaded=false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RealSQLDatabase_New()
		  Dim dbFile as FolderItem
		  
		  dbFile=GetSaveFolderItem("","New Race.rdb")
		  if dbFile <> nil then
		    RealSQLDB=New REALSQLDatabase
		    RealSQLDB.databaseFile=dbFile
		    If RealSQLDB.CreateDatabaseFile then
		      //proceed with database operations...
		      RealSQLDB.SQLExecute("DROP TABLE Movies")
		      RealSQLDB.SQLExecute("DROP TABLE Actors")
		      RealSQLDB.SQLExecute(RBSchema_Divisions)
		      RealSQLDB.SQLExecute(RBSchema_Intervals)
		      RealSQLDB.SQLExecute(RBSchema_NGBPoints)
		      RealSQLDB.SQLExecute(RBSchema_Participants)
		      RealSQLDB.SQLExecute(RBSchema_RaceDistances)
		      RealSQLDB.SQLExecute(RBSchema_Races)
		      RealSQLDB.SQLExecute(RBSchema_TeamMembers)
		      RealSQLDB.SQLExecute(RBSchema_Times)
		      RealSQLDB.SQLExecute(RBSchema_Transponders)
		      RealSQLDB.SQLExecute(RBCreateParticipantsRacerNumberIndex)
		      RealSQLDB.SQLExecute(RBSchema_CCTeams)
		      RealSQLDB.SQLExecute(RBSchema_CCTeamIntervals)
		      
		      DatabaseType="REALSQLDatabase"
		      
		      'set the race defaults
		    else
		      beep
		      MsgBox "The race data file could not be created"
		    end if
		    
		    'display the race window
		    wnd_List.Show
		    wnd_List.SetupListBox
		    wnd_List.LoadRaceTab
		    app.ChangeRace
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RealSQLDatabase_Open(RacePath as string)
		  Dim dbFile as FolderItem
		  
		  RealSQLDB=New REALSQLDatabase
		  if RacePath="" then
		    dbFile = GetOpenFolderItem(RacePath)
		  else
		    dbFile = GetFolderItem(RacePath)
		  end if
		  if dbFile <> nil then
		    RealSQLDB.DatabaseFile=dbFile
		    If RealSQLDB.Connect() then
		      
		      DatabaseType="REALSQLDatabase"
		      
		      app.ChangeRace
		      wnd_List.Show
		      wnd_List.SetupListBox
		      wnd_List.LoadRaceTab
		      
		    else
		      Beep
		      MsgBox "The race datafile couldn't be opened or is missing."
		    end if
		  end if
		  
		Exception err
		  If err IsA NilObjectException then
		    Beep
		    MsgBox "The race datafile is missing."
		    
		  End if
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		DatabaseType As String
	#tag EndProperty

	#tag Property, Flags = &h0
		MySQLDB As MySQLCommunityServer
	#tag EndProperty

	#tag Property, Flags = &h0
		MySQLDBHost As String
	#tag EndProperty

	#tag Property, Flags = &h0
		MySQLDBPassword As String
	#tag EndProperty

	#tag Property, Flags = &h0
		MySQLDBUserName As String
	#tag EndProperty

	#tag Property, Flags = &h0
		RealSQLDB As RealSQLDatabase
	#tag EndProperty


	#tag Constant, Name = mySQLCreateParticipantsRacerNumberIndex, Type = String, Dynamic = False, Default = \"ALTER TABLE `participants` ADD INDEX ( `Racer_Number` )  ", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQLSchema_CCTeamIntervals, Type = String, Dynamic = False, Default = \"CREATE TABLE `ccTeamIntervals` (\r  `rowid` int(11) NOT NULL auto_increment\x2C\r  `TeamID` int(11) default NULL\x2C\r  `Interval_Number` int(11) default NULL\x2C\r  `Interval_Name` char(255) default NULL\x2C\r  `Total_Points` int(11) default NULL\x2C\r  `Number_Scorers` int(11) default NULL\x2C\r  `UpdateFlag` char(1) default \'N\'\x2C\r  PRIMARY KEY  (`rowid`)\r) ENGINE\x3DMyISAM DEFAULT CHARSET\x3Dlatin1;", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQLSchema_CCTeams, Type = String, Dynamic = False, Default = \"CREATE TABLE `ccTeams` (\n  `rowid` int(11) NOT NULL auto_increment\x2C\n  `DivisionID` int(11) NOT NULL\x2C\n  `Team_Name` char(255) default NULL\x2C\n  `Gender` char(1) default NULL\x2C\nPRIMARY KEY  (`rowid`)\n) ENGINE\x3DMyISAM DEFAULT CHARSET\x3Dlatin1;", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQLSchema_Divisions, Type = String, Dynamic = False, Default = \"CREATE TABLE `divisions` (\r  `rowid` int(11) NOT NULL auto_increment\x2C\r  `RaceID` int(11) default NULL\x2C\r  `Division_Name` char(255) default NULL\x2C\r  `RaceDistanceID` int(11) default NULL\x2C\r  `Import_Name` char(255) default NULL\x2C\r  `Low_Age` int(11) default NULL\x2C\r  `High_Age` int(11) default NULL\x2C\r  `Gender` char(1) default NULL\x2C\r  `Start_Time` char(23) default NULL\x2C\r  `Actual_Start_Time` char(23) default NULL\x2C\r  `List_Order` int(11) default NULL\x2C\r  `Stage_Cutoff` int(11) default NULL\x2C\r  `Has_Team_Members` char(1) default NULL\x2C\r  PRIMARY KEY  (`rowid`)\r) ENGINE\x3DMyISAM DEFAULT CHARSET\x3Dlatin1;", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQLSchema_Intervals, Type = String, Dynamic = False, Default = \"CREATE TABLE `intervals` (\r  `rowid` int(11) NOT NULL auto_increment\x2C\r  `RaceID` int(11) default NULL\x2C\r  `DivisionID` int(11) default NULL\x2C\r  `Number` int(11) default NULL\x2C\r  `Interval_Name` char(255) default NULL\x2C\r  `Pace_Type` char(255) default NULL\x2C\r  `Actual_Distance` double default NULL\x2C\r  PRIMARY KEY  (`rowid`)\r) ENGINE\x3DMyISAM DEFAULT CHARSET\x3Dlatin1;", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQLSchema_NGBPoints, Type = String, Dynamic = False, Default = \"CREATE TABLE `ngbpoints` (\r  `rowid` int(11) NOT NULL auto_increment\x2C\r  `ngb` char(255) default NULL\x2C\r  `last_name` char(255) default NULL\x2C\r  `first_name` char(255) default NULL\x2C\r  `country` char(255) default NULL\x2C\r  `YOB` int(11) default NULL\x2C\r  `gender` char(255) default NULL\x2C\r  `sprint` double default NULL\x2C\r  `distance` double default NULL\x2C\r  `overall` double default NULL\x2C\r  PRIMARY KEY  (`rowid`)\r) ENGINE\x3DMyISAM DEFAULT CHARSET\x3Dlatin1;", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQLSchema_Participants, Type = String, Dynamic = False, Default = \"CREATE TABLE `participants` (\n  `rowid` int(11) NOT NULL auto_increment\x2C\n  `RaceID` int(11) default NULL\x2C\n  `TeamID` int(11) default NULL\x2C\n  `RaceDistanceID` int(11) default NULL\x2C\n  `DivisionID` int(11) default NULL\x2C\n  `Racer_Number` int(11) default NULL\x2C\n  `Participant_Name` char(255) default NULL\x2C\n  `First_Name` char(255) default NULL\x2C\n  `Street` char(255) default NULL\x2C\n  `City` char(255) default NULL\x2C\n  `State` char(2) default NULL\x2C\n  `Postal_Code` char(255) default NULL\x2C\n  `Country` char(3) default NULL\x2C\n  `Gender` char(1) default NULL\x2C\n  `Phone` char(20) default NULL\x2C\n  `EMail` char(255) default NULL\x2C\n  `SMS_Address` char(255) default NULL\x2C\n  `Birth_Date` date default NULL\x2C\n  `Age` int(11) default NULL\x2C\n  `Representing` char(255) default NULL\x2C\n  `Assigned_Start` char(23) default NULL\x2C\n  `Actual_Start` char(23) default NULL\x2C\n  `Actual_Stop` char(23) default NULL\x2C\n  `Total_Adjustment` char(23) default NULL\x2C\n  `Total_Time` char(23) default NULL\x2C\n  `Net_Time` char(23) default NULL\x2C\n  `Time_Source` char(2) NOT NULL default \'TT\'\x2C\n  `Age_Grade` char(6) default NULL\x2C\n  `DNS` char(1) default NULL\x2C\n  `DNF` char(1) default NULL\x2C\n  `DQ` char(1) default NULL\x2C\n  `Prevent_External_Updates` char(1) default NULL\x2C\n  `Laps_Completed` int(11) default NULL\x2C\n  `Adjustment_Applied` char(1) default NULL\x2C\n  `Adjustment_Reason` text\x2C\n  `NGB_License_Number` char(80) default NULL\x2C\n  `NGB2_License_Number` char(80) default NULL\x2C\n  `NGB1_Points` char(10) default NULL\x2C\n  `NGB2_Points` char(10) default NULL\x2C\n  `Seed_Group` char(10) default NULL\x2C\n  `Results_Printed` char(1) default NULL\x2C\n  `Query_Flag` char(1) default NULL\x2C\n  `UpdateFlag` char(1) default \'N\'\x2C\n  `Wave` text\x2C\n  `Note` text\x2C\n  `DateTimeInserted` datetime\x2C\n  PRIMARY KEY  (`rowid`)\x2C\n  KEY `RacerNumber` (`Racer_Number`)\n) ENGINE\x3DMyISAM DEFAULT CHARSET\x3Dlatin1;", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQLSchema_RaceDistances, Type = String, Dynamic = False, Default = \"CREATE TABLE `racedistances` (\r  `rowid` int(11) NOT NULL auto_increment\x2C\r  `RaceID` int(11) default NULL\x2C\r  `RaceDistance_Name` text\x2C\r  `Pace_Type` char(30) default NULL\x2C\r  `Actual_Distance` double default NULL\x2C\r  `Age_Grade_Distance` double default NULL\x2C\r  `Min_CC_Scorers` int(11) default NULL\x2C\r  `Max_CC_Scorers` int(11) default NULL\x2C\r  PRIMARY KEY  (`rowid`)\r) ENGINE\x3DMyISAM DEFAULT CHARSET\x3Dlatin1;", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQLSchema_Races, Type = String, Dynamic = False, Default = \"CREATE TABLE `races` (\r  `rowid` int(11) NOT NULL auto_increment\x2C\r  `DataVersion` char(10) default NULL\x2C\r  `RaceID` int(11) default NULL\x2C\r  `Race_Name` char(255) default NULL\x2C\r  `Race_Date` date default NULL\x2C\r  `Start_Time` char(23) default NULL\x2C\r  `Publish` char(1) default NULL\x2C\r  `Year` int(11) default NULL\x2C\r  `Display_Type` char(255) default NULL\x2C\r  `Note` text\x2C\r  `Allow_Early_Start` char(1) default NULL\x2C\r  `Calculate_Time_From` char(20) default NULL\x2C\r  `nthTime` char(1) default NULL\x2C\r  `Display_Lookup_List` char(1) default NULL\x2C\r  `Race_Age_Date` date default NULL\x2C\r  `Show_Back_Time` char(1) default NULL\x2C\r  `Show_Net_Time` char(1) default NULL\x2C\r  `Show_Adjustment` char(1) default NULL\x2C\r  `Places_To_Truncate` int(11) default NULL\x2C\r  `Minimum_Time` char(23) default NULL\x2C\r  `NGB1` char(255) default NULL\x2C\r  `NGB2` char(255) default NULL\x2C\r  `Racers_Per_Start` int(11) default NULL\x2C\r  `Start_Interval` char(23) default NULL\x2C\r  `Starting_Racer_Number` int(11) default NULL\x2C\r  `Logo1` char(255) default NULL\x2C\r  `Logo2` char(255) default NULL\x2C\r  `Logo3` char(255) default NULL\x2C\r  `Logo4` char(255) default NULL\x2C\r  `Jam_Session` char(1) default NULL\x2C\r  `UpdateFlag` char(1) default \'N\'\x2C\r  `SMSCount` int(11) default \'0\'\x2C\r  `TD_Number` char(10) default NULL\x2C\r  `TD_Name` char(30) default NULL\x2C\r  `TD_Div` char(3) default NULL\x2C\r  `ATD_Number` char(10) default NULL\x2C\r  `ATD_Name` char(30) default NULL\x2C\r  `ATD_Div` char(3) default NULL\x2C\r  `CoC_Name` char(30) default NULL\x2C\r  `CoC_Div` char(3) default NULL\x2C\r  `Jury_Member_4_Name` char(30) default NULL\x2C\r  `Jury_Member_4_Div` char(3) default NULL\x2C\r  `Jury_Member_5_Name` char(30) default NULL\x2C\r  `Jury_Member_5_Div` char(3) default NULL\x2C\r  `Lap_Length` int(11) default NULL\x2C\r  `HD` int(11) default NULL\x2C\r  `MC` int(11) default NULL\x2C\r  `TC` int(11) default NULL\x2C\r  `WX` char(255) default NULL\x2C\r  `FValue` char(30) default NULL\x2C\r  `MinPointsEventType` char(30) default NULL\x2C\r  `AthletePathRaceID` char(30) default NULL\x2C\r  PRIMARY KEY  (`rowid`)\r) ENGINE\x3DMyISAM DEFAULT CHARSET\x3Dlatin1;", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQLSchema_TeamMembers, Type = String, Dynamic = False, Default = \"CREATE TABLE `teammembers` (\r  `rowid` int(11) NOT NULL AUTO_INCREMENT\x2C\r  `ParticipantID` int(11) DEFAULT NULL\x2C\r  `IntervalNumber` int(11) DEFAULT NULL\x2C\r  `MemberName` char(255) DEFAULT NULL\x2C\r  PRIMARY KEY (`rowid`)\r) ENGINE\x3DMyISAM DEFAULT CHARSET\x3Dlatin1;\r", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQLSchema_Times, Type = String, Dynamic = False, Default = \"CREATE TABLE `times` (\r  `rowid` int(11) NOT NULL auto_increment\x2C\r  `RaceID` int(11) default NULL\x2C\r  `ParticipantID` int(11) default NULL\x2C\r  `TeamMemberID` int(11) default 0\x2C\r  `TX_Code` char(40) default NULL\x2C\r  `Use_This_Passing` char(1) default NULL\x2C\r  `Type` char(40) default NULL\x2C\r  `Interval_Number` int(11) default NULL\x2C\r  `Timing_Point_No` int(11) default NULL\x2C\r  `Actual_Time` char(23) default NULL\x2C\r  `Interval_Time` char(23) default NULL\x2C\r  `Total_Time` char(23) default NULL\x2C\r  `UpdateFlag` char(1) default \'N\'\x2C\r  PRIMARY KEY  (`rowid`)\r) ENGINE\x3DMyISAM DEFAULT CHARSET\x3Dlatin1;", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQLSchema_Transponders, Type = String, Dynamic = False, Default = \"CREATE TABLE `transponders` (\r  `rowid` int(11) NOT NULL auto_increment\x2C\r  `TeamMemberID` int(11) default 0\x2C\r  `RaceID` int(11) default NULL\x2C\r  `Racer_Number` int(11) default NULL\x2C\r  `TX_Number` int(11) default NULL\x2C\r  `TX_Code` char(40) default NULL\x2C\r  `Issued` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP\x2C\r  `Returned` timestamp NOT NULL default \'0000-00-00 00:00:00\'\x2C\r  `Hit_Count` int(11) default NULL\x2C\r  PRIMARY KEY  (`rowid`)\r) ENGINE\x3DMyISAM DEFAULT CHARSET\x3Dlatin1;", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQL_SetForeignKeyChecks0, Type = String, Dynamic = False, Default = \"SET FOREIGN_KEY_CHECKS \x3D 0;", Scope = Public
	#tag EndConstant

	#tag Constant, Name = mySQL_SetForeignKeyChecks1, Type = String, Dynamic = False, Default = \"SET FOREIGN_KEY_CHECKS \x3D 1;", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RBCreateParticipantsRacerNumberIndex, Type = String, Dynamic = False, Default = \"CREATE INDEX RacerNumber ON participants (Racer_Number)", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RBSchema_CCTeamIntervals, Type = String, Dynamic = False, Default = \"CREATE TABLE ccTeamIntervals (\r  TeamID INTEGER\x2C\r  Interval_Number INTEGER\x2C\r  Interval_Name VARCHAR\x2C\r  Total_Points INTEGER\x2C\r  Number_Scorers INTEGER\x2C\r  UpdateFlag VARCHAR DEFAULT N)", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RBSchema_CCTeams, Type = String, Dynamic = False, Default = \"CREATE TABLE CCTeams (DivisionID INTEGER\x2C Team_Name varchar\x2C Gender varchar)", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RBSchema_Divisions, Type = String, Dynamic = False, Default = \"CREATE TABLE divisions (\r  RaceID Integer\x2C\r  Division_Name VARCHAR\x2C\r  RaceDistanceID Integer\x2C\r  Import_Name VARCHAR\x2C\r  Low_Age Integer\x2C\r  High_Age Integer\x2C\r  Gender VARCHAR\x2C\r  Start_Time VARCHAR\x2C\r  Actual_Start_Time VARCHAR\x2C\r  List_Order Integer\x2C\r  Stage_Cutoff Integer\x2C\r  Has_Team_Members VARCHAR)", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RBSchema_Intervals, Type = String, Dynamic = False, Default = \"CREATE TABLE intervals (\r  RaceID INTEGER\x2C\r  DivisionID INTEGER\x2C\r  Number INTEGER\x2C\r  Interval_Name VARCHAR\x2C\r  Pace_Type VARCHAR\x2C\r  Actual_Distance DOUBLE)", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RBSchema_NGBPoints, Type = String, Dynamic = False, Default = \"CREATE TABLE ngbpoints (\r  ngb VARCHAR\x2C\r  license VARCHAR\x2C\r  last_name VARCHAR\x2C\r  first_name VARCHAR\x2C\r  country VARCHAR\x2C\r  YOB SMALLINT\x2C\r  gender VARCHAR\x2C\r  sprint DOUBLE\x2C\r  distance DOUBLE\x2C\r  overall DOUBLE)", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RBSchema_Participants, Type = String, Dynamic = False, Default = \"CREATE TABLE participants (\n  RaceID INTEGER\x2C\n  TeamID INTEGER\x2C\n  RaceDistanceID INTEGER\x2C\n  DivisionID INTEGER\x2C\n  Racer_Number INTEGER\x2C\n  Participant_Name VARCHAR\x2C\n  First_Name VARCHAR\x2C\n  Street VARCHAR\x2C\n  City VARCHAR\x2C\n  State VARCHAR\x2C\n  Postal_Code VARCHAR\x2C\n  Country VARCHAR\x2C\n  Gender VARCHAR\x2C\n  Phone VARCHAR\x2C\n  EMail VARCHAR\x2C\n  SMS_Address VARCHAR\x2C\n  Birth_Date DATE\x2C\n  Age INTEGER\x2C\n  Representing VARCHAR\x2C\n  Assigned_Start VARCHAR\x2C\n  Actual_Start VARCHAR\x2C\n  Actual_Stop VARCHAR\x2C\n  Total_Adjustment VARCHAR\x2C\n  Total_Time VARCHAR\x2C\n  Net_Time VARCHAR\x2C\n  Time_Source VARCHAR DEFAULT TT\x2C\n  Age_Grade VARCHAR\x2C\n  DNS VARCHAR DEFAULT N\x2C\n  DNF VARCHAR DEFAULT N\x2C\n  DQ VARCHAR DEFAULT N\x2C\n  Prevent_External_Updates VARCHAR\x2C\n  Laps_Completed INTEGER\x2C\n  Adjustment_Applied VARCHAR\x2C\n  Adjustment_Reason VARCHAR\x2C\n  NGB_License_Number VARCHAR\x2C\n  NGB2_License_Number VARCHAR\x2C\n  NGB1_Points VARCHAR\x2C\n  NGB2_Points VARCHAR\x2C\n  Seed_Group VARCHAR\x2C \n  Results_Printed VARCHAR\x2C\n  Query_Flag VARCHAR\x2C\n  UpdateFlag VARCHAR DEFAULT N\x2C\n  Wave VARCHAR\x2C\n  Note VARCHAR\x2C\n  DateTimeInserted TIMESTAMP)", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RBSchema_RaceDistances, Type = String, Dynamic = False, Default = \"CREATE TABLE racedistances (\r  RaceID INTEGER\x2C\r  RaceDistance_Name VARCHAR\x2C\r  Pace_Type VARCHAR\x2C\r  Actual_Distance DOUBLE\x2C\r  Age_Grade_Distance DOUBLE\x2C\r  Min_CC_Scorers INTEGER\x2C\r  Max_CC_Scorers INTEGER)", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RBSchema_Races, Type = String, Dynamic = False, Default = \"CREATE TABLE races (\r  DataVersion VARCHAR\x2C\r  RaceID INTEGER\x2C\r  Race_Name VARCHAR\x2C\r  Race_Date DATE\x2C\r  Start_Time VARCHAR\x2C\r  Publish VARCHAR\x2C\r  Year INTEGER\x2C\r  Display_Type VARCHAR\x2C\r  Note VARCHAR\x2C\r  Allow_Early_Start VARCHAR\x2C\r  Calculate_Time_From VARCHAR\x2C\r  Display_Lookup_List VARCHAR\x2C\r  Race_Age_Date DATE\x2C\r  Show_Back_Time VARCHAR\x2C\r  Show_Net_Time VARCHAR\x2C\r  Show_Adjustment VARCHAR\x2C\r  Places_To_Truncate INTEGER\x2C\r  Minimum_Time VARCHAR\x2C\r  NGB1 VARCHAR\x2C\r  NGB2 VARCHAR\x2C\r  Racers_Per_Start INTEGER\x2C\r  Start_Interval VARCHAR\x2C\r  Starting_Racer_Number INTEGER\x2C\r  Logo1 VARCHAR\x2C\r  Logo2 VARCHAR\x2C\r  Logo3 VARCHAR\x2C\r  Logo4 VARCHAR\x2C\r  Jam_Session VARCHAR\x2C\r  nthTime INTEGER\x2C\r  UpdateFlag VARCHAR\x2C\r  SMSCount INTEGER DEFAULT 0\x2C\r  TD_Number VARCHAR\x2C\r  TD_Name VARCHAR\x2C\r  TD_Div VARCHAR\x2C\r  ATD_Number VARCHAR\x2C\r  ATD_Name VARCHAR\x2C\r  ATD_Div VARCHAR\x2C\r  CoC_Name VARCHAR\x2C\r  CoC_Div VARCHAR\x2C\r  Jury_Member_4_Name VARCHAR\x2C\r  Jury_Member_4_Div VARCHAR\x2C\r  Jury_Member_5_Name VARCHAR\x2C\r  Jury_Member_5_Div VARCHAR\x2C\r  Lap_Length INTEGER\x2C\r  HD INTEGER\x2C\r  MC INTEGER\x2C\r  TC INTEGER\x2C\r  WX VARCHAR\x2C\r  FValue VARCHAR\x2C\r  MinPointsEventType VARCHAR\x2C\r  AthletePathRaceID VARCHAR)", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RBSchema_TeamMembers, Type = String, Dynamic = False, Default = \"CREATE TABLE teammembers (ParticipantID integer\x2C IntervalNumber integer\x2C MemberName varchar)", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RBSchema_Times, Type = String, Dynamic = False, Default = \"CREATE TABLE times (\r  RaceID INTEGER\x2C\r  ParticipantID INTEGER\x2C\r  TeamMemberID INTEGER\x2C\r  TX_Code VARCHAR\x2C\r  Use_This_Passing VARCHAR\x2C\r  Type VARCHAR\x2C\r  Interval_Number INTEGER\x2C\r  Timing_Point_No INTEGER\x2C\r  Actual_Time VARCHAR\x2C\r  Interval_Time VARCHAR\x2C\r  Total_Time VARCHAR\x2C\r  UpdateFlag VARCHAR DEFAULT N)", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RBSchema_Transponders, Type = String, Dynamic = False, Default = \"CREATE TABLE transponders (\r  RaceID INTEGER\x2C\r  TeamMemberID INTEGER\x2C\r  Racer_Number INTEGER\x2C\r  TX_Number INTEGER\x2C\r  TX_Code VARCHAR\x2C\r  Issued TIMESTAMP\x2C\r  Returned TIMESTAMP\x2C\r  Hit_Count INTEGER)", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="DatabaseType"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MySQLDBHost"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MySQLDBPassword"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MySQLDBUserName"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
