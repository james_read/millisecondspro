#tag Window
Begin Window wnd_List
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   638
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   0
   MaximizeButton  =   True
   MaxWidth        =   0
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   638
   MinimizeButton  =   True
   MinWidth        =   1003
   Placement       =   0
   Resizeable      =   True
   Title           =   "Race Name"
   Visible         =   True
   Width           =   1003
   Begin TabPanel RaceTab
      AutoDeactivate  =   True
      Bold            =   False
      Enabled         =   True
      Height          =   591
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   13
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Panels          =   ""
      Scope           =   0
      SmallTabs       =   False
      TabDefinition   =   "Race\rDivisions\rTeams\rParticipants\rTimes\rTransponders"
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   39
      Underline       =   False
      Value           =   3
      Visible         =   True
      Width           =   963
      Begin GroupBox GroupBox2
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Race Distances"
         Enabled         =   True
         Height          =   152
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   530
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   418
         Underline       =   False
         Visible         =   True
         Width           =   414
         Begin ListBox RaceTab_DistancesListBox
            AutoDeactivate  =   True
            AutoHideScrollbars=   True
            Bold            =   False
            Border          =   True
            ColumnCount     =   2
            ColumnsResizable=   False
            ColumnWidths    =   ""
            DataField       =   ""
            DataSource      =   ""
            DefaultRowHeight=   -1
            Enabled         =   True
            EnableDrag      =   False
            EnableDragReorder=   False
            GridLinesHorizontal=   0
            GridLinesVertical=   0
            HasHeading      =   True
            HeadingIndex    =   -1
            Height          =   112
            HelpTag         =   ""
            Hierarchical    =   False
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            InitialValue    =   ""
            Italic          =   False
            Left            =   543
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            RequiresSelection=   False
            Scope           =   0
            ScrollbarHorizontal=   False
            ScrollBarVertical=   True
            SelectionType   =   0
            TabIndex        =   0
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   446
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   326
            _ScrollOffset   =   0
            _ScrollWidth    =   -1
         End
         Begin BevelButton bbDeleteRaceDistance
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Delete"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Click to delete a distance from the race."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   877
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   1
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   481
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   60
         End
         Begin BevelButton bbNewRaceDistance
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "New"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Click to add a new distance event for the race."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "GroupBox2"
            Italic          =   False
            Left            =   877
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   1
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   446
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   60
         End
      End
      Begin GroupBox GroupBox1
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Race Info"
         Enabled         =   True
         Height          =   87
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   42
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   75
         Underline       =   False
         Visible         =   True
         Width           =   902
         Begin TextField RaceName
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   "Enter the name of the race."
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   131
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   104
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   385
         End
         Begin Label StaticText1
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   47
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   0
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Race Name:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   105
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   81
         End
         Begin Label StaticText2
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   529
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   2
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Date:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   103
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   37
         End
         Begin DateEditField RaceDateInput
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   "Enter the date of the race."
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   573
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   3
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   103
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   90
         End
         Begin DateEditField RaceAgeDate
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   "Enter the date from which the racers age is calculated."
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   844
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   105
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   90
         End
         Begin Label StaticText3
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   738
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   4
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Race Age Date:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   105
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   100
         End
         Begin DateTimeEditField RaceTime
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   "Enter the start time, including date, of the race."
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   131
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   7
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   131
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   175
         End
         Begin Label StaticText4
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   48
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   6
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Start Time:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   132
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   80
         End
         Begin PopupMenu puStartType
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select the start type for this race. Mass - all the participants for a distance start together. Wave - participants for a particular distance start at different times."
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            InitialValue    =   ""
            Italic          =   False
            Left            =   386
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   9
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   130
            Underline       =   False
            Visible         =   True
            Width           =   124
         End
         Begin Label StaticText6
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   310
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   8
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Race Type:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   131
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   64
         End
         Begin ComboBox cbTTStartInterval
            AutoComplete    =   False
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select the start interval for the race."
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            InitialValue    =   "Select...\r10 sec\r15 sec\r20 sec\r30 sec\r45 sec\r60 sec\r90 sec\r120 sec"
            Italic          =   False
            Left            =   606
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   11
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   131
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   False
            Width           =   97
         End
         Begin Label lblTTStartInterval
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   519
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   10
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Start Interval:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   131
            Transparent     =   False
            Underline       =   False
            Visible         =   False
            Width           =   80
         End
         Begin Label StaticText11
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   709
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   12
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "AtheletePath Race ID:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   132
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   132
         End
         Begin TextField APRaceID
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   "Enter the AthletePath Race ID. Required for live timing."
            Index           =   -2147483648
            InitialParent   =   "GroupBox1"
            Italic          =   False
            Left            =   844
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   13
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   131
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   90
         End
      End
      Begin BevelButton bbApplyChanges
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Apply Changes"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   ""
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   844
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   5
         TabPanelIndex   =   1
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   592
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
      Begin GroupBox gb_query
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Query"
         Enabled         =   True
         Height          =   122
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   49
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   False
         LockRight       =   False
         LockTop         =   False
         Scope           =   0
         TabIndex        =   6
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   119
         Underline       =   False
         Visible         =   True
         Width           =   855
         Begin PopupMenu DataBaseFields_Divisions
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select a field in the table to query on."
            Index           =   -2147483648
            InitialParent   =   "gb_query"
            InitialValue    =   ""
            Italic          =   False
            Left            =   135
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   2
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   177
         End
         Begin PopupMenu Relationship_Divisions
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Selecte a quey operation."
            Index           =   -2147483648
            InitialParent   =   "gb_query"
            InitialValue    =   ""
            Italic          =   False
            Left            =   328
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   2
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   70
         End
         Begin TextField ef_query_Divisions
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   "Enter the value to query on. % is the widlcard character."
            Index           =   -2147483648
            InitialParent   =   "gb_query"
            Italic          =   False
            Left            =   413
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   2
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   154
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   300
         End
         Begin PushButton pb_query_Divisions
            AutoDeactivate  =   True
            Bold            =   False
            ButtonStyle     =   "0"
            Cancel          =   False
            Caption         =   "Query"
            Default         =   True
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Click or press enter to execute the query."
            Index           =   -2147483648
            InitialParent   =   "gb_query"
            Italic          =   False
            Left            =   734
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   3
            TabPanelIndex   =   2
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   90
         End
         Begin BevelButton bbShowAllDiviions
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Show All"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Shows all the records."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "gb_query"
            Italic          =   False
            Left            =   312
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   4
            TabPanelIndex   =   2
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
         Begin BevelButton bbShowSubset
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Show Highlighted"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Shows just the highlighted records."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "gb_query"
            Italic          =   False
            Left            =   444
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   2
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
         Begin BevelButton bbOmitSubset
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Omit Highlighted"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Omits the highlighted records from the list."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "gb_query"
            Italic          =   False
            Left            =   576
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   6
            TabPanelIndex   =   2
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
      End
      Begin StyleGrid DataList_Divisions
         AreaSelection   =   True
         AutoDeactivate  =   True
         CellEvenColor   =   &cEBEBEB00
         CellOddColor    =   &cFFFFFF00
         Cols            =   4
         CornerBtnEnabled=   True
         DefaultColumnWidth=   0
         DefaultRowHeight=   16
         Enabled         =   True
         EvenBackColor   =   &cEBEBEB00
         FreezeCols      =   0
         GetFocusOnClick =   True
         GridLineColor   =   &cB4B4B400
         HasBackColor    =   True
         HasCellGridLineColor=   False
         HasHeader       =   True
         HasInactiveSelectionColor=   False
         HasOutOfBoundsGridLineColor=   False
         HasSelectedTextColor=   True
         HasSelectionColor=   True
         Height          =   341
         HelpTag         =   ""
         HorizontalGrid  =   False
         HorizontalScrollbar=   False
         HotTrack        =   False
         InactiveSelectionColor=   &cCCDCF200
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Left            =   49
         LiveScroll      =   False
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         OddBackColor    =   &cFFFFFF00
         OutOfBoundsGridLineColor=   &cB4B4B400
         PopupMenuCellFullSensitivity=   False
         RenderSortOrder =   True
         Rows            =   0
         Scope           =   0
         SelectedTextColor=   &c00000000
         SelectEntireRow =   True
         SelectionColor  =   &c3575D400
         ShadeSelection  =   False
         ShowResizeGlyph =   False
         StartupCocoaFont=   ""
         StartupLinuxFont=   "Geneva"
         StartupLinuxFontSize=   9
         StartupMacFont  =   "Geneva"
         StartupMacFontSize=   10
         StartupWin32Font=   "MS Sans Serif"
         StartupWin32FontSize=   9
         TabIndex        =   8
         TabPanelIndex   =   2
         TabStop         =   True
         Top             =   275
         UseFocusRing    =   True
         VerticalGrid    =   False
         VerticalScrollbar=   True
         Visible         =   True
         Width           =   901
      End
      Begin BevelButton bbNewDivision
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "New"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to add a new record."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   65
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   2
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbEditDivision
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Edit"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to edit the selected record."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   137
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   2
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbDeleteDivsion
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Delete"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to delete the selected record. Option click to delete all the records."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   209
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   2
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbDivisionImport
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Import"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to import records."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   284
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   2
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbDuplicateIntervals
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Copy Intervals"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to copy the intervals from the first record to the rest of the displayed records."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   427
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   5
         TabPanelIndex   =   2
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   126
      End
      Begin Line Line1
         BorderWidth     =   1
         Height          =   27
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Left            =   277
         LineColor       =   &c00000000
         LockedInPosition=   False
         Scope           =   0
         TabIndex        =   10
         TabPanelIndex   =   2
         TabStop         =   True
         Top             =   80
         Visible         =   True
         Width           =   0
         X1              =   277
         X2              =   277
         Y1              =   80
         Y2              =   107
      End
      Begin GroupBox GroupBox4
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Query"
         Enabled         =   True
         Height          =   122
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   49
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   False
         LockRight       =   False
         LockTop         =   False
         Scope           =   0
         TabIndex        =   7
         TabPanelIndex   =   4
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   119
         Underline       =   False
         Visible         =   True
         Width           =   855
         Begin PopupMenu Relationship_Participants
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select a relationship for the query."
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            InitialValue    =   ""
            Italic          =   False
            Left            =   328
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   4
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   70
         End
         Begin PushButton pb_Query_Participants
            AutoDeactivate  =   True
            Bold            =   False
            ButtonStyle     =   "0"
            Cancel          =   False
            Caption         =   "Query"
            Default         =   True
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Click or press enter to execute the query"
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            Italic          =   False
            Left            =   734
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   4
            TabPanelIndex   =   4
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   90
         End
         Begin BevelButton bbShowAllParticipants
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Show All"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   ""
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            Italic          =   False
            Left            =   312
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   6
            TabPanelIndex   =   4
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
         Begin BevelButton bbShowHightlightedParticipants
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Show Highlighted"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Show just the highlighted records"
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            Italic          =   False
            Left            =   444
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   7
            TabPanelIndex   =   4
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
         Begin BevelButton bbOmitHighlightedParticipants
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Omit Highlighted"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Omits the highlighted records from the list"
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            Italic          =   False
            Left            =   576
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   8
            TabPanelIndex   =   4
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
         Begin PopupMenu DataBaseFields_Participants
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select a field to query."
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            InitialValue    =   ""
            Italic          =   False
            Left            =   139
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   4
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   177
         End
         Begin TextField ef_query_Participants
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   "Enter a value to query with."
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            Italic          =   False
            Left            =   422
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   3
            TabPanelIndex   =   4
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   154
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   300
         End
         Begin PopupMenu TimeSource2
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select the source of the times to receive."
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            InitialValue    =   "2nd Point..."
            Italic          =   False
            Left            =   427
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   4
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   167
            Underline       =   False
            Visible         =   True
            Width           =   160
         End
         Begin PopupMenu TimeSource1
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select the source of the times to receive."
            Index           =   -2147483648
            InitialParent   =   "GroupBox4"
            InitialValue    =   "1st Point..."
            Italic          =   False
            Left            =   427
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   4
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   143
            Underline       =   False
            Visible         =   True
            Width           =   160
         End
      End
      Begin BevelButton bbNewParticipant
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "New"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to create a new record"
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   65
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   4
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbEditParticipant
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Edit"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to edit the selected record"
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   137
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   4
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbDeleteParticipant
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Delete"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to delete the selected record. Option click to delete all the records."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   209
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   4
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin Line Line2
         BorderWidth     =   1
         Height          =   27
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Left            =   277
         LineColor       =   &c00000000
         LockedInPosition=   False
         Scope           =   0
         TabIndex        =   15
         TabPanelIndex   =   4
         TabStop         =   True
         Top             =   80
         Visible         =   True
         Width           =   0
         X1              =   277
         X2              =   277
         Y1              =   80
         Y2              =   107
      End
      Begin StyleGrid DataList_Participants
         AreaSelection   =   True
         AutoDeactivate  =   True
         CellEvenColor   =   &cEBEBEB00
         CellOddColor    =   &cFFFFFF00
         Cols            =   4
         CornerBtnEnabled=   True
         DefaultColumnWidth=   0
         DefaultRowHeight=   16
         Enabled         =   True
         EvenBackColor   =   &cEBEBEB00
         FreezeCols      =   0
         GetFocusOnClick =   True
         GridLineColor   =   &cB4B4B400
         HasBackColor    =   True
         HasCellGridLineColor=   False
         HasHeader       =   True
         HasInactiveSelectionColor=   False
         HasOutOfBoundsGridLineColor=   False
         HasSelectedTextColor=   True
         HasSelectionColor=   True
         Height          =   341
         HelpTag         =   ""
         HorizontalGrid  =   False
         HorizontalScrollbar=   True
         HotTrack        =   False
         InactiveSelectionColor=   &cCCDCF200
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Left            =   49
         LiveScroll      =   True
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         OddBackColor    =   &cFFFFFF00
         OutOfBoundsGridLineColor=   &cB4B4B400
         PopupMenuCellFullSensitivity=   False
         RenderSortOrder =   True
         Rows            =   0
         Scope           =   0
         SelectedTextColor=   &c00000000
         SelectEntireRow =   True
         SelectionColor  =   &c3575D400
         ShadeSelection  =   False
         ShowResizeGlyph =   False
         StartupCocoaFont=   ""
         StartupLinuxFont=   "Geneva"
         StartupLinuxFontSize=   9
         StartupMacFont  =   "Geneva"
         StartupMacFontSize=   10
         StartupWin32Font=   "MS Sans Serif"
         StartupWin32FontSize=   9
         TabIndex        =   9
         TabPanelIndex   =   4
         TabStop         =   False
         Top             =   275
         UseFocusRing    =   True
         VerticalGrid    =   False
         VerticalScrollbar=   True
         Visible         =   True
         Width           =   901
      End
      Begin StyleGrid DataList_Times
         AreaSelection   =   True
         AutoDeactivate  =   True
         CellEvenColor   =   &cEBEBEB00
         CellOddColor    =   &cFFFFFF00
         Cols            =   4
         CornerBtnEnabled=   True
         DefaultColumnWidth=   0
         DefaultRowHeight=   16
         Enabled         =   True
         EvenBackColor   =   &cEBEBEB00
         FreezeCols      =   0
         GetFocusOnClick =   True
         GridLineColor   =   &cB4B4B400
         HasBackColor    =   True
         HasCellGridLineColor=   False
         HasHeader       =   True
         HasInactiveSelectionColor=   False
         HasOutOfBoundsGridLineColor=   False
         HasSelectedTextColor=   True
         HasSelectionColor=   True
         Height          =   341
         HelpTag         =   ""
         HorizontalGrid  =   False
         HorizontalScrollbar=   False
         HotTrack        =   False
         InactiveSelectionColor=   &cCCDCF200
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Left            =   49
         LiveScroll      =   False
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         OddBackColor    =   &cFFFFFF00
         OutOfBoundsGridLineColor=   &cB4B4B400
         PopupMenuCellFullSensitivity=   False
         RenderSortOrder =   True
         Rows            =   0
         Scope           =   0
         SelectedTextColor=   &c00000000
         SelectEntireRow =   True
         SelectionColor  =   &c3575D400
         ShadeSelection  =   False
         ShowResizeGlyph =   False
         StartupCocoaFont=   ""
         StartupLinuxFont=   "Geneva"
         StartupLinuxFontSize=   9
         StartupMacFont  =   "Geneva"
         StartupMacFontSize=   10
         StartupWin32Font=   "MS Sans Serif"
         StartupWin32FontSize=   9
         TabIndex        =   8
         TabPanelIndex   =   5
         TabStop         =   True
         Top             =   275
         UseFocusRing    =   True
         VerticalGrid    =   False
         VerticalScrollbar=   True
         Visible         =   True
         Width           =   901
      End
      Begin BevelButton bbNewTime
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "New"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to add a new record."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   65
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   5
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbEditTime
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Edit"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to edit the selected record."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   137
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   5
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbDeleteTime
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Delete"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to delete the selected record. Option click to delete all the records."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   209
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   5
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbImportTimes
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Import"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   2
         Height          =   22
         HelpTag         =   "Click to import time records."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   284
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   5
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin Line Line3
         BorderWidth     =   1
         Height          =   28
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Left            =   277
         LineColor       =   &c00000000
         LockedInPosition=   False
         Scope           =   0
         TabIndex        =   22
         TabPanelIndex   =   5
         TabStop         =   True
         Top             =   79
         Visible         =   True
         Width           =   0
         X1              =   277
         X2              =   277
         Y1              =   79
         Y2              =   107
      End
      Begin GroupBox GroupBox5
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Query"
         Enabled         =   True
         Height          =   122
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   49
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   False
         LockRight       =   False
         LockTop         =   False
         Scope           =   0
         TabIndex        =   6
         TabPanelIndex   =   5
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   119
         Underline       =   False
         Visible         =   True
         Width           =   855
         Begin PopupMenu DataBaseFields_Times
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select a field on which to query."
            Index           =   -2147483648
            InitialParent   =   "GroupBox5"
            InitialValue    =   ""
            Italic          =   False
            Left            =   135
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   5
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   177
         End
         Begin PopupMenu Relationship_Times
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select a relatonship for the query."
            Index           =   -2147483648
            InitialParent   =   "GroupBox5"
            InitialValue    =   ""
            Italic          =   False
            Left            =   328
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   5
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   70
         End
         Begin TextField ef_query_times
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox5"
            Italic          =   False
            Left            =   413
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   5
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   154
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   300
         End
         Begin BevelButton bbShowAllTimes
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Show All"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Click to show all the records in the list."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "GroupBox5"
            Italic          =   False
            Left            =   312
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   4
            TabPanelIndex   =   5
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
         Begin BevelButton bbShowHighlightedTimes
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Show Highlighted"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Click to show just the highlighted records in the list."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "GroupBox5"
            Italic          =   False
            Left            =   444
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   5
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
         Begin BevelButton bbOmitHighlightedTimes
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Omit Highlighted"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Click to omit the highlighted records from the list."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "GroupBox5"
            Italic          =   False
            Left            =   576
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   6
            TabPanelIndex   =   5
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
         Begin PushButton pb_query_Times
            AutoDeactivate  =   True
            Bold            =   False
            ButtonStyle     =   "0"
            Cancel          =   False
            Caption         =   "Query"
            Default         =   True
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Click or press enter to execute the query."
            Index           =   -2147483648
            InitialParent   =   "GroupBox5"
            Italic          =   False
            Left            =   734
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   3
            TabPanelIndex   =   5
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   90
         End
      End
      Begin GroupBox GroupBox6
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Query"
         Enabled         =   True
         Height          =   122
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   49
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   False
         LockRight       =   False
         LockTop         =   False
         Scope           =   0
         TabIndex        =   6
         TabPanelIndex   =   6
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   119
         Underline       =   False
         Visible         =   True
         Width           =   855
         Begin PopupMenu DataBaseFields_Transponders
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select a field on which to query."
            Index           =   -2147483648
            InitialParent   =   "GroupBox6"
            InitialValue    =   ""
            Italic          =   False
            Left            =   135
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   6
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   177
         End
         Begin PopupMenu Relationship_Transponders
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select a relationship for the query."
            Index           =   -2147483648
            InitialParent   =   "GroupBox6"
            InitialValue    =   ""
            Italic          =   False
            Left            =   328
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   6
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   70
         End
         Begin TextField ef_query_Transponders
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   "Enter a value for the query."
            Index           =   -2147483648
            InitialParent   =   "GroupBox6"
            Italic          =   False
            Left            =   413
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   6
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   154
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   300
         End
         Begin PushButton pn_query_Transponders
            AutoDeactivate  =   True
            Bold            =   False
            ButtonStyle     =   "0"
            Cancel          =   False
            Caption         =   "Query"
            Default         =   True
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Click or press enter to execute the query."
            Index           =   -2147483648
            InitialParent   =   "GroupBox6"
            Italic          =   False
            Left            =   734
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   3
            TabPanelIndex   =   6
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   90
         End
         Begin BevelButton bbShowAllTransponders
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Show All"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Click to show all the records in the list."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "GroupBox6"
            Italic          =   False
            Left            =   312
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   4
            TabPanelIndex   =   6
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
         Begin BevelButton bbShowHighlightedTX
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Show Highlighted"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Click to show the highlighted records in the list."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "GroupBox6"
            Italic          =   False
            Left            =   444
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   6
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
         Begin BevelButton bbOmitHighlightedTX
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Omit Highlighted"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Click to omit the highlighted records from the lilst."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "GroupBox6"
            Italic          =   False
            Left            =   576
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   6
            TabPanelIndex   =   6
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
      End
      Begin BevelButton bbTXNew
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "New"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to add a new record."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   65
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   6
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bBEditTX
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Edit"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to edit the selected record."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   137
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   6
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbDeleteTX
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Delete"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to delete the selected record. Option click to delete all the records."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   209
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   6
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbImportTX
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Import"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to import transponder data."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   284
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   6
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin Line Line4
         BorderWidth     =   1
         Height          =   27
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Left            =   277
         LineColor       =   &c00000000
         LockedInPosition=   False
         Scope           =   0
         TabIndex        =   29
         TabPanelIndex   =   6
         TabStop         =   True
         Top             =   80
         Visible         =   True
         Width           =   0
         X1              =   277
         X2              =   277
         Y1              =   80
         Y2              =   107
      End
      Begin Label DivisionsFound
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   850
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   False
         LockRight       =   True
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   7
         TabPanelIndex   =   2
         TabStop         =   True
         Text            =   "0 found"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   253
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label ParticipantsFound
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   850
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   False
         LockRight       =   True
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   8
         TabPanelIndex   =   4
         TabStop         =   True
         Text            =   "0 found"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   253
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label TimesFound
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   850
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   False
         LockRight       =   True
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   7
         TabPanelIndex   =   5
         TabStop         =   True
         Text            =   "0 found"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   253
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label TXFound
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   850
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   False
         LockRight       =   True
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   7
         TabPanelIndex   =   6
         TabStop         =   True
         Text            =   "0 found"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   253
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin BevelButton bbAdjustParticipantTimes
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Adjust"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to adjust the times of the listed participants."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   427
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   5
         TabPanelIndex   =   4
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbExport
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Export"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   2
         Height          =   22
         HelpTag         =   "Click to export participant data"
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   355
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   4
         TabPanelIndex   =   4
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbExportTransponders
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Export"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to export participant data"
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   353
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   4
         TabPanelIndex   =   6
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbAdjustTimeTimes
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Adjust"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to adjust the times of the listed participants."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   427
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   5
         TabPanelIndex   =   5
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbExportTransponders1
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Export"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to export participant data"
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   355
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   4
         TabPanelIndex   =   2
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin StyleGrid DataList_Transponders
         AreaSelection   =   True
         AutoDeactivate  =   True
         CellEvenColor   =   &cEBEBEB00
         CellOddColor    =   &cFFFFFF00
         Cols            =   6
         CornerBtnEnabled=   True
         DefaultColumnWidth=   0
         DefaultRowHeight=   16
         Enabled         =   True
         EvenBackColor   =   &cEBEBEB00
         FreezeCols      =   0
         GetFocusOnClick =   True
         GridLineColor   =   &cB4B4B400
         HasBackColor    =   True
         HasCellGridLineColor=   False
         HasHeader       =   True
         HasInactiveSelectionColor=   False
         HasOutOfBoundsGridLineColor=   False
         HasSelectedTextColor=   True
         HasSelectionColor=   True
         Height          =   341
         HelpTag         =   ""
         HorizontalGrid  =   False
         HorizontalScrollbar=   False
         HotTrack        =   False
         InactiveSelectionColor=   &cCCDCF200
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Left            =   50
         LiveScroll      =   False
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         OddBackColor    =   &cFFFFFF00
         OutOfBoundsGridLineColor=   &cB4B4B400
         PopupMenuCellFullSensitivity=   False
         RenderSortOrder =   True
         Rows            =   0
         Scope           =   0
         SelectedTextColor=   &c00000000
         SelectEntireRow =   True
         SelectionColor  =   &c3575D400
         ShadeSelection  =   False
         ShowResizeGlyph =   False
         StartupCocoaFont=   ""
         StartupLinuxFont=   "Geneva"
         StartupLinuxFontSize=   9
         StartupMacFont  =   "Geneva"
         StartupMacFontSize=   10
         StartupWin32Font=   "MS Sans Serif"
         StartupWin32FontSize=   9
         TabIndex        =   8
         TabPanelIndex   =   6
         TabStop         =   True
         Top             =   275
         UseFocusRing    =   True
         VerticalGrid    =   False
         VerticalScrollbar=   True
         Visible         =   True
         Width           =   901
      End
      Begin GroupBox gb_query1
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Query"
         Enabled         =   True
         Height          =   122
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   49
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   False
         LockRight       =   False
         LockTop         =   False
         Scope           =   0
         TabIndex        =   6
         TabPanelIndex   =   3
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   119
         Underline       =   False
         Visible         =   True
         Width           =   855
         Begin PopupMenu DataBaseFields_Teams
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select a field in the table to query on."
            Index           =   -2147483648
            InitialParent   =   "gb_query1"
            InitialValue    =   ""
            Italic          =   False
            Left            =   135
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   3
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   177
         End
         Begin PopupMenu Relationship_Teams
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Selecte a quey operation."
            Index           =   -2147483648
            InitialParent   =   "gb_query1"
            InitialValue    =   ""
            Italic          =   False
            Left            =   328
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   3
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   70
         End
         Begin TextField ef_query_Teams
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   "Enter the value to query on. % is the widlcard character."
            Index           =   -2147483648
            InitialParent   =   "gb_query1"
            Italic          =   False
            Left            =   413
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   3
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   154
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   300
         End
         Begin PushButton pb_query_Teams
            AutoDeactivate  =   True
            Bold            =   False
            ButtonStyle     =   "0"
            Cancel          =   False
            Caption         =   "Query"
            Default         =   True
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Click or press enter to execute the query."
            Index           =   -2147483648
            InitialParent   =   "gb_query1"
            Italic          =   False
            Left            =   734
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            Scope           =   0
            TabIndex        =   3
            TabPanelIndex   =   3
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   156
            Underline       =   False
            Visible         =   True
            Width           =   90
         End
         Begin BevelButton bbShowAllTeams
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Show All"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Shows all the records."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "gb_query1"
            Italic          =   False
            Left            =   312
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   4
            TabPanelIndex   =   3
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
         Begin BevelButton bbShowSubsetTeams
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Show Highlighted"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Shows just the highlighted records."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "gb_query1"
            Italic          =   False
            Left            =   444
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   3
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
         Begin BevelButton bbOmitSubsetTeams
            AcceptFocus     =   False
            AutoDeactivate  =   True
            BackColor       =   &c00000000
            Bevel           =   0
            Bold            =   False
            ButtonType      =   0
            Caption         =   "Omit Highlighted"
            CaptionAlign    =   3
            CaptionDelta    =   0
            CaptionPlacement=   1
            Enabled         =   True
            HasBackColor    =   False
            HasMenu         =   0
            Height          =   22
            HelpTag         =   "Omits the highlighted records from the list."
            Icon            =   0
            IconAlign       =   0
            IconDX          =   0
            IconDY          =   0
            Index           =   -2147483648
            InitialParent   =   "gb_query1"
            Italic          =   False
            Left            =   576
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   False
            LockRight       =   False
            LockTop         =   False
            MenuValue       =   0
            Scope           =   0
            TabIndex        =   6
            TabPanelIndex   =   3
            TabStop         =   True
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   120
         End
      End
      Begin StyleGrid DataList_Teams
         AreaSelection   =   True
         AutoDeactivate  =   True
         CellEvenColor   =   &cEBEBEB00
         CellOddColor    =   &cFFFFFF00
         Cols            =   4
         CornerBtnEnabled=   True
         DefaultColumnWidth=   0
         DefaultRowHeight=   16
         Enabled         =   True
         EvenBackColor   =   &cEBEBEB00
         FreezeCols      =   0
         GetFocusOnClick =   True
         GridLineColor   =   &cB4B4B400
         HasBackColor    =   True
         HasCellGridLineColor=   False
         HasHeader       =   True
         HasInactiveSelectionColor=   False
         HasOutOfBoundsGridLineColor=   False
         HasSelectedTextColor=   True
         HasSelectionColor=   True
         Height          =   341
         HelpTag         =   ""
         HorizontalGrid  =   False
         HorizontalScrollbar=   False
         HotTrack        =   False
         InactiveSelectionColor=   &cCCDCF200
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Left            =   49
         LiveScroll      =   False
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         OddBackColor    =   &cFFFFFF00
         OutOfBoundsGridLineColor=   &cB4B4B400
         PopupMenuCellFullSensitivity=   False
         RenderSortOrder =   True
         Rows            =   0
         Scope           =   0
         SelectedTextColor=   &c00000000
         SelectEntireRow =   True
         SelectionColor  =   &c3575D400
         ShadeSelection  =   False
         ShowResizeGlyph =   False
         StartupCocoaFont=   ""
         StartupLinuxFont=   "Geneva"
         StartupLinuxFontSize=   9
         StartupMacFont  =   "Geneva"
         StartupMacFontSize=   10
         StartupWin32Font=   "MS Sans Serif"
         StartupWin32FontSize=   9
         TabIndex        =   8
         TabPanelIndex   =   3
         TabStop         =   True
         Top             =   275
         UseFocusRing    =   True
         VerticalGrid    =   False
         VerticalScrollbar=   True
         Visible         =   True
         Width           =   901
      End
      Begin BevelButton bbNewTeam
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "New"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to add a new record."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   65
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   3
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbEditTeam
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Edit"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to edit the selected record."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   137
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   3
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbDeleteTeam
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Delete"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to delete the selected record. Option click to delete all the records."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   209
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   3
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbDivisionTeam
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Import"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to import records."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   284
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   3
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin Line Line5
         BorderWidth     =   1
         Height          =   27
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Left            =   277
         LineColor       =   &c00000000
         LockedInPosition=   False
         Scope           =   0
         TabIndex        =   46
         TabPanelIndex   =   3
         TabStop         =   True
         Top             =   80
         Visible         =   True
         Width           =   0
         X1              =   277
         X2              =   277
         Y1              =   80
         Y2              =   107
      End
      Begin Label TeamsFound
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   850
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   False
         LockRight       =   True
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   7
         TabPanelIndex   =   3
         TabStop         =   True
         Text            =   "0 found"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   253
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin BevelButton bbExportTeams
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Export"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to export participant data"
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   355
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   4
         TabPanelIndex   =   3
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbDuplicateTeamIntervals
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Copy Intervals"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to copy the intervals from the first record to the rest of the displayed records."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   427
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   5
         TabPanelIndex   =   3
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   126
      End
      Begin BevelButton bbExportTimes
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Export"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to export time data"
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   355
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   4
         TabPanelIndex   =   5
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin BevelButton bbImportParticipants
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Import"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   2
         Height          =   22
         HelpTag         =   "Click to import time records."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   285
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   4
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin GroupBox GroupBox3
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Time Calculation"
         Enabled         =   True
         Height          =   114
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   42
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   418
         Underline       =   False
         Visible         =   True
         Width           =   473
         Begin RadioButton AssignedStart
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Assigned Start"
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select if the race will be scored based on the racers assigned start time."
            Index           =   -2147483648
            InitialParent   =   "GroupBox3"
            Italic          =   False
            Left            =   62
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   446
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   121
         End
         Begin RadioButton ActualStart
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Actual Start"
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select if the race will be scored on net or ""chip"" time."
            Index           =   -2147483648
            InitialParent   =   "GroupBox3"
            Italic          =   False
            Left            =   62
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   3
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   474
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   110
         End
         Begin CheckBox AllowEarlyStarts
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Allow Early Starts"
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Check if the participant's is calculated from actual start and are allowed to start before thier assigned start time."
            Index           =   -2147483648
            InitialParent   =   "GroupBox3"
            Italic          =   False
            Left            =   62
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            State           =   0
            TabIndex        =   6
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   504
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   124
         End
         Begin TimeEditField MinTime
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   "Enter the amount of time the must occur between intervals."
            Index           =   -2147483648
            InitialParent   =   "GroupBox3"
            Italic          =   False
            Left            =   359
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   446
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   91
         End
         Begin PopupMenu Rounding
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select the format of the results."
            Index           =   -2147483648
            InitialParent   =   "GroupBox3"
            InitialValue    =   ""
            Italic          =   False
            Left            =   359
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   473
            Underline       =   False
            Visible         =   True
            Width           =   132
         End
         Begin Label StaticText7
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox3"
            Italic          =   False
            Left            =   205
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   1
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Minimum Interval Time:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   447
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   145
         End
         Begin Label StaticText8
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox3"
            Italic          =   False
            Left            =   240
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   4
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Time Truncation:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   474
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   110
         End
         Begin Label StaticText13
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox3"
            Italic          =   False
            Left            =   198
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   7
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Use this time as the time:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   503
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   152
         End
         Begin ComboBox cbnthTime
            AutoComplete    =   False
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select the final time that will be counted as the official time for the athlete or team."
            Index           =   -2147483648
            InitialParent   =   "GroupBox3"
            InitialValue    =   ""
            Italic          =   False
            Left            =   359
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   8
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   503
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   124
         End
      End
      Begin GroupBox GroupBox7
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Results and Scoring"
         Enabled         =   True
         Height          =   233
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   42
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   174
         Underline       =   False
         Visible         =   True
         Width           =   902
         Begin GroupBox gbUSSA
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "USSA/FIS Info"
            Enabled         =   True
            Height          =   159
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox7"
            Italic          =   False
            Left            =   46
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   4
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   238
            Underline       =   False
            Visible         =   False
            Width           =   479
            Begin GroupBox GroupBox12
               AutoDeactivate  =   True
               Bold            =   False
               Caption         =   "Jury"
               Enabled         =   True
               Height          =   127
               HelpTag         =   ""
               Index           =   -2147483648
               InitialParent   =   "gbUSSA"
               Italic          =   False
               Left            =   51
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Scope           =   0
               TabIndex        =   0
               TabPanelIndex   =   1
               TabStop         =   True
               TextFont        =   "System"
               TextSize        =   10.0
               TextUnit        =   0
               Top             =   262
               Underline       =   False
               Visible         =   True
               Width           =   228
               Begin TextField tfJuryMember4
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the name of the 4th jury member."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   135
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   9
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   342
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   101
               End
               Begin TextField tfChiefOfCourse
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the name of the Chief of Course."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   135
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   6
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   322
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   101
               End
               Begin TextField tfATDName
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the name of the assistant technical delegate."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   135
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   4
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   301
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   101
               End
               Begin TextField tfTDName
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the name of the technical delegate."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   135
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   1
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   281
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   101
               End
               Begin TextField tfTDNumber
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the technical delegate's USSA/FIS number."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   86
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   0
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   281
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   46
               End
               Begin TextField tfATDNumber
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the assistant technical delegate's USSA/FIS nunmber."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   86
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   3
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   301
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   46
               End
               Begin Label stHD1111
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   69
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Multiline       =   False
                  Scope           =   0
                  Selectable      =   False
                  TabIndex        =   8
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   "Member 4:"
                  TextAlign       =   2
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   341
                  Transparent     =   False
                  Underline       =   False
                  Visible         =   True
                  Width           =   63
               End
               Begin Label stHD11111
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   62
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Multiline       =   False
                  Scope           =   0
                  Selectable      =   False
                  TabIndex        =   11
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   "Member 5:"
                  TextAlign       =   2
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   361
                  Transparent     =   False
                  Underline       =   False
                  Visible         =   True
                  Width           =   70
               End
               Begin TextField tfTDDiv
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the division or country of the TD."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   239
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   2
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   281
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   35
               End
               Begin TextField tfATDDiv
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the division or country of the assistant TD."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   239
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   5
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   301
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   35
               End
               Begin TextField tfChiefOfCourseDiv
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the division or country of the Chief of Course."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   239
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   7
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   322
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   35
               End
               Begin TextField tfJuryMember4Div
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the division or countruy of the 4th jury member."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   239
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   10
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   342
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   35
               End
               Begin TextField tfJuryMember5Div
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the division or country of the 5th jury member."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   239
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   13
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   362
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   35
               End
               Begin TextField tfJuryMember5
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the name of the 4th jury member."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox12"
                  Italic          =   False
                  Left            =   135
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   12
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   362
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   101
               End
            End
            Begin Label stHD111
               AutoDeactivate  =   True
               Bold            =   False
               DataField       =   ""
               DataSource      =   ""
               Enabled         =   True
               Height          =   20
               HelpTag         =   ""
               Index           =   -2147483648
               InitialParent   =   "gbUSSA"
               Italic          =   False
               Left            =   47
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Multiline       =   False
               Scope           =   0
               Selectable      =   False
               TabIndex        =   5
               TabPanelIndex   =   1
               TabStop         =   True
               Text            =   "Chief of Course:"
               TextAlign       =   2
               TextColor       =   &c00000000
               TextFont        =   "System"
               TextSize        =   10.0
               TextUnit        =   0
               Top             =   321
               Transparent     =   False
               Underline       =   False
               Visible         =   True
               Width           =   86
            End
            Begin Label stHD11
               AutoDeactivate  =   True
               Bold            =   False
               DataField       =   ""
               DataSource      =   ""
               Enabled         =   True
               Height          =   20
               HelpTag         =   ""
               Index           =   -2147483648
               InitialParent   =   "gbUSSA"
               Italic          =   False
               Left            =   44
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Multiline       =   False
               Scope           =   0
               Selectable      =   False
               TabIndex        =   4
               TabPanelIndex   =   1
               TabStop         =   True
               Text            =   "ATD:"
               TextAlign       =   2
               TextColor       =   &c00000000
               TextFont        =   "System"
               TextSize        =   10.0
               TextUnit        =   0
               Top             =   300
               Transparent     =   False
               Underline       =   False
               Visible         =   True
               Width           =   38
            End
            Begin Label stHD1
               AutoDeactivate  =   True
               Bold            =   False
               DataField       =   ""
               DataSource      =   ""
               Enabled         =   True
               Height          =   20
               HelpTag         =   ""
               Index           =   -2147483648
               InitialParent   =   "gbUSSA"
               Italic          =   False
               Left            =   44
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Multiline       =   False
               Scope           =   0
               Selectable      =   False
               TabIndex        =   3
               TabPanelIndex   =   1
               TabStop         =   True
               Text            =   "TD:"
               TextAlign       =   2
               TextColor       =   &c00000000
               TextFont        =   "System"
               TextSize        =   10.0
               TextUnit        =   0
               Top             =   281
               Transparent     =   False
               Underline       =   False
               Visible         =   True
               Width           =   38
            End
            Begin GroupBox GroupBox13
               AutoDeactivate  =   True
               Bold            =   False
               Caption         =   "Default Course"
               Enabled         =   True
               Height          =   127
               HelpTag         =   ""
               Index           =   -2147483648
               InitialParent   =   "gbUSSA"
               Italic          =   False
               Left            =   283
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Scope           =   0
               TabIndex        =   1
               TabPanelIndex   =   1
               TabStop         =   True
               TextFont        =   "System"
               TextSize        =   10.0
               TextUnit        =   0
               Top             =   262
               Underline       =   False
               Visible         =   True
               Width           =   137
               Begin TextField tfWX
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the weather condotions for the race."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox13"
                  Italic          =   False
                  Left            =   332
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   9
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   363
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   81
               End
               Begin TextField tfTC
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   "#####"
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the total climb (TC), in meters, for the race."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox13"
                  Italic          =   False
                  Left            =   332
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   ""
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   7
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   342
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   46
               End
               Begin TextField tfMC
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the maximum climb (MC), in meters, for the course."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox13"
                  Italic          =   False
                  Left            =   332
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   "#####"
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   5
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   322
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   46
               End
               Begin TextField tfHD
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the height difference (HD), in meters for the course."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox13"
                  Italic          =   False
                  Left            =   332
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   "#####"
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   3
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   301
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   46
               End
               Begin TextField tfLapLength
                  AcceptTabs      =   False
                  Alignment       =   0
                  AutoDeactivate  =   True
                  AutomaticallyCheckSpelling=   False
                  BackColor       =   &cFFFFFF00
                  Bold            =   False
                  Border          =   True
                  CueText         =   ""
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Format          =   ""
                  Height          =   18
                  HelpTag         =   "Enter the lenth, in meters, of the lap for the course."
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox13"
                  Italic          =   False
                  Left            =   332
                  LimitText       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Mask            =   "#####"
                  Password        =   False
                  ReadOnly        =   False
                  Scope           =   0
                  TabIndex        =   1
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   ""
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   281
                  Underline       =   False
                  UseFocusRing    =   True
                  Visible         =   True
                  Width           =   46
               End
               Begin Label stLength
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox13"
                  Italic          =   False
                  Left            =   289
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Multiline       =   False
                  Scope           =   0
                  Selectable      =   False
                  TabIndex        =   0
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   "Lap Len:"
                  TextAlign       =   2
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   281
                  Transparent     =   False
                  Underline       =   False
                  Visible         =   True
                  Width           =   41
               End
               Begin Label stHD
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox13"
                  Italic          =   False
                  Left            =   289
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Multiline       =   False
                  Scope           =   0
                  Selectable      =   False
                  TabIndex        =   2
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   "HD:"
                  TextAlign       =   2
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   301
                  Transparent     =   False
                  Underline       =   False
                  Visible         =   True
                  Width           =   41
               End
               Begin Label stMC
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox13"
                  Italic          =   False
                  Left            =   289
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Multiline       =   False
                  Scope           =   0
                  Selectable      =   False
                  TabIndex        =   4
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   "MC:"
                  TextAlign       =   2
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   322
                  Transparent     =   False
                  Underline       =   False
                  Visible         =   True
                  Width           =   41
               End
               Begin Label stTC
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox13"
                  Italic          =   False
                  Left            =   289
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Multiline       =   False
                  Scope           =   0
                  Selectable      =   False
                  TabIndex        =   6
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   "TC:"
                  TextAlign       =   2
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   342
                  Transparent     =   False
                  Underline       =   False
                  Visible         =   True
                  Width           =   41
               End
               Begin Label stWX
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox13"
                  Italic          =   False
                  Left            =   289
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Multiline       =   False
                  Scope           =   0
                  Selectable      =   False
                  TabIndex        =   8
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   "WX:"
                  TextAlign       =   2
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   363
                  Transparent     =   False
                  Underline       =   False
                  Visible         =   True
                  Width           =   41
               End
            End
            Begin GroupBox GroupBox15
               AutoDeactivate  =   True
               Bold            =   False
               Caption         =   "Point Calculation"
               Enabled         =   True
               Height          =   124
               HelpTag         =   ""
               Index           =   -2147483648
               InitialParent   =   "gbUSSA"
               Italic          =   False
               Left            =   425
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Scope           =   0
               TabIndex        =   2
               TabPanelIndex   =   1
               TabStop         =   True
               TextFont        =   "System"
               TextSize        =   10.0
               TextUnit        =   0
               Top             =   262
               Underline       =   False
               Visible         =   True
               Width           =   95
               Begin PopupMenu pmFValue
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox15"
                  InitialValue    =   ""
                  Italic          =   False
                  Left            =   435
                  ListIndex       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Scope           =   0
                  TabIndex        =   3
                  TabPanelIndex   =   1
                  TabStop         =   True
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   328
                  Underline       =   False
                  Visible         =   True
                  Width           =   80
               End
               Begin Label stFValue
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox15"
                  Italic          =   False
                  Left            =   435
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Multiline       =   False
                  Scope           =   0
                  Selectable      =   False
                  TabIndex        =   2
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   "F-Value:"
                  TextAlign       =   0
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   310
                  Transparent     =   False
                  Underline       =   False
                  Visible         =   True
                  Width           =   41
               End
               Begin PopupMenu pmMinPoints
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox15"
                  InitialValue    =   ""
                  Italic          =   False
                  Left            =   434
                  ListIndex       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Scope           =   0
                  TabIndex        =   1
                  TabPanelIndex   =   1
                  TabStop         =   True
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   296
                  Underline       =   False
                  Visible         =   True
                  Width           =   80
               End
               Begin Label stMinPoints
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox15"
                  Italic          =   False
                  Left            =   434
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Multiline       =   False
                  Scope           =   0
                  Selectable      =   False
                  TabIndex        =   0
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   "Min Points:"
                  TextAlign       =   0
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   279
                  Transparent     =   False
                  Underline       =   False
                  Visible         =   True
                  Width           =   59
               End
               Begin Label stFValue1
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox15"
                  Italic          =   False
                  Left            =   435
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Multiline       =   False
                  Scope           =   0
                  Selectable      =   False
                  TabIndex        =   4
                  TabPanelIndex   =   1
                  TabStop         =   True
                  Text            =   "Use Points:"
                  TextAlign       =   0
                  TextColor       =   &c00000000
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   342
                  Transparent     =   False
                  Underline       =   False
                  Visible         =   True
                  Width           =   56
               End
               Begin PopupMenu pmUP
                  AutoDeactivate  =   True
                  Bold            =   False
                  DataField       =   ""
                  DataSource      =   ""
                  Enabled         =   True
                  Height          =   20
                  HelpTag         =   ""
                  Index           =   -2147483648
                  InitialParent   =   "GroupBox15"
                  InitialValue    =   ""
                  Italic          =   False
                  Left            =   435
                  ListIndex       =   0
                  LockBottom      =   False
                  LockedInPosition=   False
                  LockLeft        =   True
                  LockRight       =   False
                  LockTop         =   True
                  Scope           =   0
                  TabIndex        =   5
                  TabPanelIndex   =   1
                  TabStop         =   True
                  TextFont        =   "System"
                  TextSize        =   10.0
                  TextUnit        =   0
                  Top             =   360
                  Underline       =   False
                  Visible         =   True
                  Width           =   80
               End
            End
         End
         Begin GroupBox GroupBox8
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Logos"
            Enabled         =   True
            Height          =   131
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox7"
            Italic          =   False
            Left            =   527
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   0
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   198
            Underline       =   False
            Visible         =   True
            Width           =   410
            Begin ImageWell iwLogo4
               AutoDeactivate  =   True
               Enabled         =   True
               Height          =   100
               HelpTag         =   ""
               Image           =   0
               Index           =   -2147483648
               InitialParent   =   "GroupBox8"
               Left            =   834
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Scope           =   0
               TabIndex        =   3
               TabPanelIndex   =   1
               TabStop         =   True
               Top             =   222
               Visible         =   True
               Width           =   100
            End
            Begin ImageWell iwLogo3
               AutoDeactivate  =   True
               Enabled         =   True
               Height          =   100
               HelpTag         =   ""
               Image           =   0
               Index           =   -2147483648
               InitialParent   =   "GroupBox8"
               Left            =   733
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Scope           =   0
               TabIndex        =   2
               TabPanelIndex   =   1
               TabStop         =   True
               Top             =   222
               Visible         =   True
               Width           =   100
            End
            Begin ImageWell iwLogo2
               AutoDeactivate  =   True
               Enabled         =   True
               Height          =   100
               HelpTag         =   ""
               Image           =   0
               Index           =   -2147483648
               InitialParent   =   "GroupBox8"
               Left            =   632
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Scope           =   0
               TabIndex        =   1
               TabPanelIndex   =   1
               TabStop         =   True
               Top             =   222
               Visible         =   True
               Width           =   100
            End
            Begin ImageWell iwLogo1
               AutoDeactivate  =   True
               Enabled         =   True
               Height          =   100
               HelpTag         =   ""
               Image           =   0
               Index           =   -2147483648
               InitialParent   =   "GroupBox8"
               Left            =   531
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Scope           =   0
               TabIndex        =   0
               TabPanelIndex   =   1
               TabStop         =   True
               Top             =   222
               Visible         =   True
               Width           =   100
            End
         End
         Begin PopupMenu ResultType
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   "Select the format of the results."
            Index           =   -2147483648
            InitialParent   =   "GroupBox7"
            InitialValue    =   ""
            Italic          =   False
            Left            =   160
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   2
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   204
            Underline       =   False
            Visible         =   True
            Width           =   170
         End
         Begin Label StaticText5
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox7"
            Italic          =   False
            Left            =   51
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   1
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Scoring Defaults:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   206
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   106
         End
         Begin GroupBox GroupBox14
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "NGB License Information"
            Enabled         =   True
            Height          =   62
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox7"
            Italic          =   False
            Left            =   527
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            TabIndex        =   5
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   335
            Underline       =   False
            Visible         =   True
            Width           =   410
            Begin Label StaticText15
               AutoDeactivate  =   True
               Bold            =   False
               DataField       =   ""
               DataSource      =   ""
               Enabled         =   True
               Height          =   20
               HelpTag         =   ""
               Index           =   -2147483648
               InitialParent   =   "GroupBox14"
               Italic          =   False
               Left            =   544
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Multiline       =   False
               Scope           =   0
               Selectable      =   False
               TabIndex        =   0
               TabPanelIndex   =   1
               TabStop         =   True
               Text            =   "NGB 1:"
               TextAlign       =   2
               TextColor       =   &c00000000
               TextFont        =   "System"
               TextSize        =   12.0
               TextUnit        =   0
               Top             =   365
               Transparent     =   False
               Underline       =   False
               Visible         =   True
               Width           =   54
            End
            Begin TextField tfNGB1
               AcceptTabs      =   False
               Alignment       =   0
               AutoDeactivate  =   True
               AutomaticallyCheckSpelling=   False
               BackColor       =   &cFFFFFF00
               Bold            =   False
               Border          =   True
               CueText         =   ""
               DataField       =   ""
               DataSource      =   ""
               Enabled         =   True
               Format          =   ""
               Height          =   22
               HelpTag         =   "Enter the name of the National Governing Body for License #1."
               Index           =   -2147483648
               InitialParent   =   "GroupBox14"
               Italic          =   False
               Left            =   601
               LimitText       =   0
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Mask            =   ""
               Password        =   False
               ReadOnly        =   False
               Scope           =   0
               TabIndex        =   1
               TabPanelIndex   =   1
               TabStop         =   True
               Text            =   ""
               TextColor       =   &c00000000
               TextFont        =   "System"
               TextSize        =   12.0
               TextUnit        =   0
               Top             =   364
               Underline       =   False
               UseFocusRing    =   True
               Visible         =   True
               Width           =   104
            End
            Begin TextField tfNGB2
               AcceptTabs      =   False
               Alignment       =   0
               AutoDeactivate  =   True
               AutomaticallyCheckSpelling=   False
               BackColor       =   &cFFFFFF00
               Bold            =   False
               Border          =   True
               CueText         =   ""
               DataField       =   ""
               DataSource      =   ""
               Enabled         =   True
               Format          =   ""
               Height          =   22
               HelpTag         =   "Enter the name of the National Governing Body for License #2."
               Index           =   -2147483648
               InitialParent   =   "GroupBox14"
               Italic          =   False
               Left            =   796
               LimitText       =   0
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Mask            =   ""
               Password        =   False
               ReadOnly        =   False
               Scope           =   0
               TabIndex        =   3
               TabPanelIndex   =   1
               TabStop         =   True
               Text            =   ""
               TextColor       =   &c00000000
               TextFont        =   "System"
               TextSize        =   12.0
               TextUnit        =   0
               Top             =   364
               Underline       =   False
               UseFocusRing    =   True
               Visible         =   True
               Width           =   104
            End
            Begin Label StaticText16
               AutoDeactivate  =   True
               Bold            =   False
               DataField       =   ""
               DataSource      =   ""
               Enabled         =   True
               Height          =   20
               HelpTag         =   ""
               Index           =   -2147483648
               InitialParent   =   "GroupBox14"
               Italic          =   False
               Left            =   739
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Multiline       =   False
               Scope           =   0
               Selectable      =   False
               TabIndex        =   2
               TabPanelIndex   =   1
               TabStop         =   True
               Text            =   "NGB 2:"
               TextAlign       =   2
               TextColor       =   &c00000000
               TextFont        =   "System"
               TextSize        =   12.0
               TextUnit        =   0
               Top             =   365
               Transparent     =   False
               Underline       =   False
               Visible         =   True
               Width           =   54
            End
         End
         Begin CheckBox cbJam
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Jam Session"
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "GroupBox7"
            Italic          =   False
            Left            =   383
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   0
            State           =   0
            TabIndex        =   3
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   205
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   100
         End
      End
      Begin BevelButton bbGenBibTag
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "BibTags"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to Gernate BibTag Records"
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   425
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   5
         TabPanelIndex   =   6
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   60
      End
      Begin GroupBox gbPhotoCell
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Photocell Time Control"
         Enabled         =   True
         Height          =   70
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   42
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   4
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   544
         Underline       =   False
         Visible         =   True
         Width           =   473
         Begin Label stFinishBracket
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "gbPhotoCell"
            Italic          =   False
            Left            =   259
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   3
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Finish Bracket:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   576
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   87
         End
         Begin Label lblFinishDelay
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   17
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "gbPhotoCell"
            Italic          =   False
            Left            =   440
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   5
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "+/- 0.25 sec"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   9.0
            TextUnit        =   0
            Top             =   576
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   69
         End
         Begin Slider slFinish
            AutoDeactivate  =   True
            Enabled         =   True
            Height          =   23
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "gbPhotoCell"
            Left            =   355
            LineStep        =   1
            LiveScroll      =   False
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Maximum         =   100
            Minimum         =   0
            PageStep        =   20
            Scope           =   0
            TabIndex        =   4
            TabPanelIndex   =   1
            TabStop         =   True
            TickStyle       =   "0"
            Top             =   576
            Value           =   25
            Visible         =   True
            Width           =   75
         End
         Begin Label lblStartDelay
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   17
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "gbPhotoCell"
            Italic          =   False
            Left            =   217
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   2
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "6 sec"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   9.0
            TextUnit        =   0
            Top             =   576
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   39
         End
         Begin Slider slStart
            AutoDeactivate  =   True
            Enabled         =   True
            Height          =   23
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "gbPhotoCell"
            Left            =   135
            LineStep        =   1
            LiveScroll      =   False
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Maximum         =   10
            Minimum         =   0
            PageStep        =   20
            Scope           =   0
            TabIndex        =   1
            TabPanelIndex   =   1
            TabStop         =   True
            TickStyle       =   "0"
            Top             =   576
            Value           =   6
            Visible         =   True
            Width           =   75
         End
         Begin Label stStartDelay
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "gbPhotoCell"
            Italic          =   False
            Left            =   50
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   0
            Selectable      =   False
            TabIndex        =   0
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Start Delay:"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   576
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   71
         End
      End
      Begin BevelButton bbVerifyNGBPoints
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Verify NGB Points"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   "Click to adjust the times of the listed participants."
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "RaceTab"
         Italic          =   False
         Left            =   499
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   6
         TabPanelIndex   =   4
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   85
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   120
      End
   End
   Begin Timer DemoCheckTimer
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   848
      LockedInPosition=   False
      Mode            =   2
      Period          =   1800000
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   4
      Width           =   32
   End
   Begin Timer AutoPrint_PrintResultsTimer
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   804
      LockedInPosition=   False
      Mode            =   0
      Period          =   60000
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   4
      Width           =   32
   End
   Begin BevelButton AutoPrint_Stop
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   ""
      CaptionAlign    =   1
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   15
      HelpTag         =   ""
      Icon            =   2075919446
      IconAlign       =   1
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   961
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   4
      Underline       =   False
      Value           =   False
      Visible         =   False
      Width           =   15
   End
   Begin Timer AutoDiscoveryTimer
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   760
      LockedInPosition=   False
      Mode            =   2
      Period          =   1000
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   3
      Width           =   32
   End
   Begin Timer UpdateNonTXTimesTimer
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   716
      LockedInPosition=   False
      Mode            =   0
      Period          =   1000
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   4
      Width           =   32
   End
   Begin Timer UpdateSMSMessageCount
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   673
      LockedInPosition=   False
      Mode            =   0
      Period          =   10000
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   4
      Width           =   32
   End
   Begin BevelButton bbSMSMessaging
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Start SMS Messaging"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   346
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   9
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   154
   End
   Begin BevelButton bbLiveTiming
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Start LiveTiming"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   18
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   9
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   154
   End
   Begin Label SMSMessagesSent
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   512
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "SMS Sent: 0"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   11
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   123
   End
   Begin BevelButton bbTweet
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Start Tweeting"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "Starts or stops Tweeting of winners."
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   180
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   9
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   154
   End
   Begin oAuth praemusCore
      accessTokenURL  =   "http://api.twitter.com/oauth/access_token"
      Address         =   ""
      appAuthURL      =   ""
      authenticationSecret=   ""
      authenticationToken=   ""
      authorizeTokenURL=   "http://api.twitter.com/oauth/authorize"
      BytesAvailable  =   0
      BytesLeftToSend =   0
      consumerKey     =   "JrCOOmD883oU20qcCRlkQ"
      consumerSecret  =   "jQweRgkxtaTezdwYQ6hJFHIp2wiFY6SwJLOh9cZo0Ss"
      errorMessage    =   ""
      Handle          =   0
      Height          =   32
      httpProxyAddress=   ""
      httpProxyPort   =   0
      Index           =   -2147483648
      InitialParent   =   ""
      IsConnected     =   False
      LastErrorCode   =   0
      Left            =   629
      LocalAddress    =   ""
      LockedInPosition=   False
      payload         =   ""
      Port            =   0
      ready           =   False
      RemoteAddress   =   ""
      requestTokenURL =   "http://api.twitter.com/oauth/request_token"
      Scope           =   0
      screenName      =   ""
      secret          =   ""
      TabIndex        =   11
      TabPanelIndex   =   "0"
      TabStop         =   True
      token           =   ""
      Top             =   4
      userId          =   0
      Width           =   32
      yield           =   False
   End
   Begin TCPSocket OrbitsSocket
      Address         =   ""
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   592
      LockedInPosition=   False
      Port            =   50000
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   4
      Width           =   32
   End
   Begin ProgressWheel TimesLoaded
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   16
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   981
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   3
      Visible         =   False
      Width           =   16
   End
   Begin Timer TimesLoadedTimer
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   893
      LockedInPosition=   False
      Mode            =   2
      Period          =   1000
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   4
      Width           =   32
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Activate()
		  HighlightQueryValue
		End Sub
	#tag EndEvent

	#tag Event
		Sub Close()
		  
		  
		  if theDB<>nil then
		    ApplyRaceDataChanges
		    
		  end if
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  app.UTCOffset=UTCOffsetInMinutes*60
		  
		  ReDim RangeLow(1)
		  ReDim RangeHigh(1)
		  
		  me.top=(screen(0).height-me.height)/2
		  me.left=(screen(0).width-me.width)/2
		  if app.Registered then
		    DemoCheckTimer.Enabled=false
		  else
		    DemoCheckTimer.Enabled=true
		  end if
		  
		  wnd_List.praemusCore.token=app.TwitterParts(0)
		  wnd_List.praemusCore.secret=app.TwitterParts(1)
		  wnd_List.praemusCore.userId=val(app.TwitterParts(2))
		  wnd_List.praemusCore.screenName=app.TwitterParts(3)
		  
		  OrbitsSocket.Listen
		  
		  HideTimeTrialUI
		End Sub
	#tag EndEvent


	#tag MenuHandler
		Function FilePageSetup() As Boolean Handles FilePageSetup.Action
			Dim PageSetup as PrinterSetup
			PageSetup= New PrinterSetup
			if app.PrinterSettings<> "" then
			PageSetup.SetupString=app.PrinterSettings
			end if
			If PageSetup.PageSetupDialog Then
			app.PrinterSettings=PageSetup.SetupString
			End If
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FilePrint() As Boolean Handles FilePrint.Action
			PrintSetup.Show
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FilePrintFeedbackResults() As Boolean Handles FilePrintFeedbackResults.Action
			PrintFeedBackResults.Show
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function ImportChipXTimeData() As Boolean Handles ImportChipXTimeData.Action
			LocateImportFolder.Show
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function SpecialAcceptTimes() As Boolean Handles SpecialAcceptTimes.Action
			'dim w as ReceiveTimes
			'dim i as integer
			'dim t as string
			'
			'w = new ReceiveTimes
			'
			''OpenWindows.append w
			''
			''i = Ubound(OpenWindows)
			''w.myIndex = i
			'
			't = "Connections"
			'w.title = t
			
			ReceiveTimes.show
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function SpecialCorrectSwitchTransponders() As Boolean Handles SpecialCorrectSwitchTransponders.Action
			CorrectSwitchTransponders.show
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function SpecialPostResultstoWeb() As Boolean Handles SpecialPostResultstoWeb.Action
			PostResultstoWeb
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function SpecialPostTransponderstoWeb() As Boolean Handles SpecialPostTransponderstoWeb.Action
			PostTXDatatoWeb
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function SpecialPreferences() As Boolean Handles SpecialPreferences.Action
			Preferences.Show
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function SpecialReassignWrongDistances() As Boolean Handles SpecialReassignWrongDistances.Action
			SwitchDistances.Show
			
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function SpecialRecalculateTimes() As Boolean Handles SpecialRecalculateTimes.Action
			RecalcTimes
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function SpecialResetRace() As Boolean Handles SpecialResetRace.Action
			ResetRace
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function SpecialResultsPreview() As Boolean Handles SpecialResultsPreview.Action
			ResultsPreview.Show
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function SpecialTransponderAssignment() As Boolean Handles SpecialTransponderAssignment.Action
			TXAssignment.Show
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function SpecialTransponderCheckin() As Boolean Handles SpecialTransponderCheckin.Action
			TXCheckIn.Show
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function SpecialUpdateStartTimes() As Boolean Handles SpecialUpdateStartTimes.Action
			UpdateStartTimes.Show
		End Function
	#tag EndMenuHandler


	#tag Method, Flags = &h1
		Protected Sub ApplyRaceDataChanges()
		  Dim SQLStatement as string
		  Dim Proceed as boolean
		  Dim rsQueryResults as RecordSet
		  
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  Proceed = true
		  
		  if RaceName.text = "" then
		    msgBox("A Race Name is required.")
		    Proceed=False
		  end if
		  
		  if (Proceed) and ((RaceDateinput.text="") or (RaceDateInput.text="0000-00-00")) then
		    msgBox("A Race Date is required.")
		    Proceed=False
		  end if
		  
		  if (Proceed) and ((RaceTime.text="") or (RaceTime.text="00:00:00")) then
		    msgBox("A Race Time is required.")
		    Proceed=False
		  end if
		  
		  if (Proceed) and ((RaceAgeDate.text="") or (RaceAgeDate.text="0000-00-00")) then
		    msgBox("A Race Age Date is required.")
		    Proceed=False
		  end if
		  
		  if (Proceed) and (Rounding.rowTag(Rounding.ListIndex)<0) then
		    msgBox("Time Truncation is required.")
		    Proceed=False
		  end if
		  
		  if (Proceed) and (ResultType.text="Select...") then
		    msgBox("Result Type is required.")
		    Proceed=False
		  end if
		  
		  if Proceed then
		    
		    SQLStatement="SELECT * FROM races WHERE RaceID=1"
		    rsQueryResults=app.theDB.DBSQLSelect(SQLStatement)
		    
		    if rsQueryResults.EOF then
		      SQLStatement="INSERT INTO races (DataVersion,RaceID,Race_Name, Race_Date,Start_Time,Publish,Year,Display_Type,Note,Allow_Early_Start,Calculate_Time_From,"
		      SQLStatement=SQLStatement+"Display_Lookup_List,Race_Age_Date,nthTime,Places_To_Truncate,"
		      SQLStatement=SQLStatement +"Minimum_Time,Racers_Per_Start,Start_Interval,Starting_Racer_Number, Logo1, Logo2, Logo3, Logo4, Jam_Session,TD_Number, TD_Name, TD_Div,"
		      SQLStatement=SQLStatement +"ATD_Number,ATD_Name,ATD_Div,CoC_Name,CoC_Div,Jury_Member_4_Name,Jury_Member_4_Div,Jury_Member_5_Name,Jury_Member_5_Div,"
		      SQLStatement = SQLStatement + "Lap_Length,HD,MC,TC,WX,NGB1,NGB2,AthletePathRaceID) "
		      SQLStatement=SQLStatement+" VALUES ("
		      SQLStatement=SQLStatement+"'"+app.ShortVersion+"',"
		      SQLStatement=SQLStatement+format(RaceID,"##########")+","
		      SQLStatement=SQLStatement+"'"+RaceName.text+"',"
		      SQLStatement=SQLStatement+"'"+RaceDateInput.text+"',"
		      SQLStatement=SQLStatement+"'"+RaceTime.text+"','N'," //Start Time and Publish
		      SQLStatement=SQLStatement+"'"+Left(RaceDateInput.text,4)+"',"
		      SQLStatement=SQLStatement+"'"+ResultType.text+"',''," //Result Type and Note
		      
		      if AllowEarlyStarts.value then
		        SQLStatement = SQLStatement + "'Y',"
		      else
		        SQLStatement = SQLStatement + "'N',"
		      end if
		      
		      if AssignedStart.value then
		        SQLStatement = SQLStatement + "'Assigned',"
		      elseif ActualStart.value then
		        SQLStatement = SQLStatement + "'Actual',"
		      end if
		      
		      SQLStatement = SQLStatement + "'Y'," //Display Look Up Lists
		      SQLStatement = SQLStatement + "'"+RaceAgeDate.text+"'," 'Race_Age_Date
		      
		      SQLStatement = SQLStatement + "'"+Str(CDbl(cbnthTime.Text))+"'," 'nthTime
		      
		      SQLStatement = SQLStatement + str(Rounding.rowTag(Rounding.ListIndex))+"," 'Places_To_Truncate
		      SQLStatement = SQLStatement + "'"+MinTime.text+"'," 'Minimum_Time
		      SQLStatement = SQLStatement + "'"+str(puStartType.rowTag(puStartType.ListIndex))+"'," ''Racers_Per_Start
		      SQLStatement = SQLStatement + "'"+str(val(cbTTStartInterval.text))+"'," 'Start_Interval
		      SQLStatement = SQLStatement + "'0',"
		      
		      if(iwLogo1.Image<>nil) then
		        SQLStatement = SQLStatement + "'"+app.PictureToString(iwLogo1.Image)+"'," ''Logo1
		      else
		        SQLStatement = SQLStatement + "'',"
		      end if
		      
		      if(iwLogo2.Image<>nil) then
		        SQLStatement = SQLStatement + "'"+app.PictureToString(iwLogo2.Image)+"'," ''Logo2
		      else
		        SQLStatement = SQLStatement + "'',"
		      end if
		      
		      if(iwLogo3.Image<>nil) then
		        SQLStatement = SQLStatement + "'"+app.PictureToString(iwLogo3.Image)+"'," ''Logo3
		      else
		        SQLStatement = SQLStatement + "'',"
		      end if
		      
		      if(iwLogo4.Image<>nil) then
		        SQLStatement = SQLStatement + "'"+app.PictureToString(iwLogo4.Image)+"'," ''Logo1
		      else
		        SQLStatement = SQLStatement + "'',"
		      end if
		      
		      if cbJam.value then
		        SQLStatement = SQLStatement + "'Y'," 'Jam Session Y
		      else
		        SQLStatement = SQLStatement + "'N'," 'Jam Session N
		      end if
		      
		      SQLStatement = SQLStatement + "'" + tfTDNumber.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfTDName.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfTDDiv.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfATDNumber.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfATDName.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfATDDiv.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfChiefOfCourse.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfChiefOfCourseDiv.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfJuryMember4.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfJuryMember4Div.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfJuryMember5.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfJuryMember5Div.Text + "', "
		      
		      SQLStatement = SQLStatement + "'" + tfLapLength.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfHD.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfMC.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfTC.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfWX.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfNGB1.Text + "', "
		      SQLStatement = SQLStatement + "'" + tfNGB2.Text + "', "
		      SQLStatement = SQLStatement + "'" + APRaceID.Text + "') "
		      
		    else
		      SQLStatement = "UPDATE races SET Race_Name='"+RaceName.text+"'"
		      SQLStatement = SQLStatement + ", Race_Date='"+RaceDateInput.text+"'"
		      SQLStatement = SQLStatement + ", Race_Age_Date='"+RaceAgeDate.text+"'"
		      SQLStatement = SQLStatement + ", Start_Time='"+RaceTime.text+"'"
		      SQLStatement = SQLStatement + ", Display_Type='"+ResultType.text+"'"
		      SQLStatement = SQLStatement + ", Places_To_Truncate="+str(Rounding.rowTag(Rounding.ListIndex))
		      SQLStatement = SQLStatement + ", Minimum_Time='"+MinTime.text+"'"
		      SQLStatement = SQLStatement + ", Racers_Per_Start="+str(puStartType.rowTag(puStartType.ListIndex))
		      SQLStatement = SQLStatement + ", Start_Interval='"+str(val(cbTTStartInterval.text))+"'"
		      SQLStatement = SQLStatement + ", Year='"+Left(RaceDateInput.text,4)+"'"
		      SQLStatement = SQLStatement + ", nthTime='"+str(CDbl(cbnthTime.Text))+"'"
		      
		      if AssignedStart.value then
		        SQLStatement = SQLStatement + ", Calculate_Time_From='Assigned'"
		      elseif ActualStart.value then
		        SQLStatement = SQLStatement + ", Calculate_Time_From='Actual'"
		      end if
		      
		      if AllowEarlyStarts.value then
		        SQLStatement = SQLStatement + ", Allow_Early_Start='Y'"
		      else
		        SQLStatement = SQLStatement + ", Allow_Early_Start='N'"
		      end if
		      
		      if(iwLogo1.Image<>nil) then
		        SQLStatement = SQLStatement + ", Logo1='"+Logo1+"'" ''Logo1
		      else
		        SQLStatement = SQLStatement + ", Logo1=''"
		      end if
		      
		      if(iwLogo2.Image<>nil) then
		        SQLStatement = SQLStatement + ", Logo2='"+Logo2+"'" ''Logo2
		      else
		        SQLStatement = SQLStatement + ", Logo2=''"
		      end if
		      
		      if(iwLogo3.Image<>nil) then
		        SQLStatement = SQLStatement + ", Logo3='"+Logo3+"'" ''Logo3
		      else
		        SQLStatement = SQLStatement + ", Logo3=''"
		      end if
		      
		      if(iwLogo4.Image<>nil) then
		        SQLStatement = SQLStatement + ", Logo4='"+Logo4+"'" ''Logo4
		      else
		        SQLStatement = SQLStatement + ", Logo4=''"
		      end if
		      
		      if cbJam.value then
		        SQLStatement = SQLStatement + ", Jam_Session='Y'"
		      else
		        SQLStatement = SQLStatement + ", Jam_Session='N'"
		      end if
		      SQLStatement = SQLStatement + ", Start_Interval='"+str(val(cbTTStartInterval.text))+"'"
		      
		      SQLStatement = SQLStatement + ", TD_Number='" + tfTDNumber.Text + "' "
		      SQLStatement = SQLStatement + ", TD_Name='" + tfTDName.Text + "' "
		      SQLStatement = SQLStatement + ", TD_Div='" + tfTDDiv.Text + "' "
		      SQLStatement = SQLStatement + ", ATD_Number='" + tfATDNumber.Text + "' "
		      SQLStatement = SQLStatement + ", ATD_Name='" + tfATDName.Text + "' "
		      SQLStatement = SQLStatement + ", ATD_Div='" + tfATDDiv.Text + "' "
		      SQLStatement = SQLStatement + ", CoC_Name='" + tfChiefOfCourse.Text + "' "
		      SQLStatement = SQLStatement + ", CoC_Div='" + tfChiefOfCourseDiv.Text + "' "
		      SQLStatement = SQLStatement + ", Jury_Member_4_Name='" + tfJuryMember4.Text + "' "
		      SQLStatement = SQLStatement + ", Jury_Member_4_Div='" + tfJuryMember4Div.Text + "' "
		      SQLStatement = SQLStatement + ", Jury_Member_5_Name='" + tfJuryMember5.Text + "' "
		      SQLStatement = SQLStatement + ", Jury_Member_5_Div='" + tfJuryMember5Div.Text + "' "
		      
		      SQLStatement = SQLStatement + ", Lap_Length='" + tfLapLength.Text + "' "
		      SQLStatement = SQLStatement + ", HD='" + tfHD.Text + "' "
		      SQLStatement = SQLStatement + ", MC='" + tfMC.Text + "' "
		      SQLStatement = SQLStatement + ", TC='" + tfTC.Text + "' "
		      SQLStatement = SQLStatement + ", WX='" + tfWX.Text + "'"
		      SQLStatement = SQLStatement + ", NGB1='" + tfNGB1.Text + "' "
		      SQLStatement = SQLStatement + ", NGB2='" + tfNGB2.Text + "' "
		      SQLStatement = SQLStatement + ", AthletePathRaceID='" + APRaceID.Text + "' "
		      
		      SQLStatement = SQLStatement + " WHERE RaceID=" +Format(RaceID,"##########")
		    end if
		    
		    App.theDB.DBSQLExecute(SQLStatement)
		    app.theDB.DBCommit
		    app.ChangeRace
		    
		    if app.RaceAgeDate.SQLDate<>"0000-00-00" and OldRaceAgeDate<>RaceAgeDate.Text then
		      d.icon=MessageDialog.GraphicQuestion   //display warning icon
		      d.ActionButton.Caption="Recalc Age"
		      d.CancelButton.Visible= True     //show the Cancel button
		      d.Message="Do you want to recalculate participant ages and reassign divisions?"
		      d.Explanation="This action will recalculate all the participants' ages based on the race age date and, if necessary, assign them to a new age division. There is no undo for this operation."
		      
		      b=d.ShowModal     //display the dialog
		      Select Case b //determine which button was pressed.
		      Case d.ActionButton
		        RecalcAge
		      Case d.CancelButton
		        'nop
		      End select
		      OldRaceAgeDate=app.RaceAgeDate.SQLDate
		    end if
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub CleanUpErrors()
		  dim SQLStatement as string
		  dim rsParticipants, rsIntervals as RecordSet
		  dim deleteID as integer
		  
		  SQLStatement="SELECT rowid FROM participants WHERE RaceID =1 AND RaceDistanceID=4"
		  rsParticipants=App.theDB.DBSQLSelect(SQLStatement)
		  
		  
		  while not rsParticipants.EOF
		    SQLStatement="SELECT rowid FROM times WHERE ParticipantID="+rsParticipants.Field("rowid").StringValue+" AND Interval_Number=3 ORDER BY Total_Time"
		    rsIntervals=App.theDB.DBSQLSelect(SQLStatement)
		    if rsIntervals.RecordCount>1 then
		      SQLStatement="DELETE FROM times WHERE RaceID=1 AND rowid="+rsIntervals.Field("rowid").StringValue
		      App.theDB.DBSQLExecute(SQLStatement)
		    else
		      SQLStatement="UPDATE times SET Use_This_Passing = 'Y' WHERE RaceID=1 AND rowid="+rsIntervals.Field("rowid").StringValue
		      App.theDB.DBSQLExecute(SQLStatement)
		    end if
		    rsParticipants.MoveNext
		  wend
		  
		  beep
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ConnectToResultsDB()
		  theResultsDB.host=app.WebServer
		  theResultsDB.port=3306
		  'theResultsDB.port=8889 'used for local testing
		  theResultsDB.databaseName="results_v2"
		  theResultsDB.userName="milliseconds"
		  theResultsDB.Password="milli2nds"
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub DuplicateIntervals()
		  dim i,j as integer
		  dim row as integer
		  dim recsRS as RecordSet
		  dim recCount as integer
		  dim res as boolean
		  dim SQLStatement as string
		  
		  dim IntervalNumber() as integer
		  dim IntervalName() as string
		  dim IntervalPaceType() as string
		  dim IntervalAcutalDistance() as double
		  
		  row=DataList_Divisions.Selection.FirstSelectedRow
		  
		  if row > 0 then
		    
		    Progress.Show
		    Progress.Initialize("Progress","Copying Intervals...",DataList_Divisions.Rows,-1)
		    
		    'Load the selected division intervals into arrays
		    '  Query for the selected intevals
		    SQLStatement="SELECT Number,Interval_Name,Actual_Distance,Pace_Type FROM intervals WHERE RaceID =1 AND DivisionID = "+DataList_Divisions.Row(row).ItemData
		    recsRS=app.theDB.DBSQLSelect(SQLStatement)
		    
		    '  resize the arrays
		    
		    redim IntervalNumber(0)
		    redim IntervalName(0)
		    redim IntervalPaceType(0)
		    redim IntervalAcutalDistance(0)
		    
		    '  populate the arrays
		    while not recsRS.eof
		      
		      IntervalNumber.append recsRS.field("Number").integerValue
		      IntervalName.append recsRS.field("Interval_Name").stringValue
		      IntervalPaceType.append recsRS.field("Pace_Type").stringValue
		      IntervalAcutalDistance.append recsRS.field("Actual_Distance").doubleValue
		      recsRS.moveNext
		    wend
		    recsRS.close
		    
		    
		    for i=1 to DataList_Divisions.Rows
		      'Delete any existing intervals
		      SQLStatement="DELETE FROM intervals WHERE RaceID=1 AND DivisionID = "+DataList_Divisions.Row(i).ItemData
		      App.theDB.DBSQLExecute(SQLStatement)
		      
		      for j=1 to Ubound(IntervalNumber)
		        'add the new intervals
		        SQLStatement="INSERT INTO intervals (RaceID,DivisionID,Number,Interval_Name,Pace_Type,Actual_Distance) "
		        SQLStatement=SQLStatement+" VALUES ("
		        SQLStatement=SQLStatement+"1,"
		        SQLStatement=SQLStatement+DataList_Divisions.Row(i).ItemData+","
		        SQLStatement=SQLStatement+str(IntervalNumber(j))+","
		        SQLStatement=SQLStatement+"'"+IntervalName(j)+"',"
		        SQLStatement=SQLStatement+"'"+IntervalPaceType(j)+"',"
		        SQLStatement=SQLStatement+"'"+str(IntervalAcutalDistance(j))+"')"
		        app.theDB.DBSQLExecute(SQLStatement)
		        App.theDB.DBCommit
		      next
		      Progress.UpdateProg1(i)
		    next
		    
		    Progress.Close
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub DuplicateTeamIntervals()
		  dim i,j as integer
		  dim row as integer
		  dim recsRS as RecordSet
		  dim recCount as integer
		  dim res as boolean
		  dim SQLStatement as string
		  
		  dim IntervalNumber() as string
		  
		  row=DataList_Teams.Selection.FirstSelectedRow
		  
		  if row > 0 then
		    
		    Progress.Show
		    Progress.Initialize("Progress","Copying Intervals...",DataList_Teams.Rows,-1)
		    
		    'Load the selected division intervals into arrays
		    '  Query for the selected intevals
		    SQLStatement="SELECT Interval_Number FROM ccTeamIntervals WHERE TeamID = "+DataList_Teams.Row(row).ItemData
		    recsRS=app.theDB.DBSQLSelect(SQLStatement)
		    
		    '  resize the arrays
		    redim IntervalNumber(0)
		    
		    '  populate the arrays
		    while not recsRS.eof
		      IntervalNumber.append recsRS.field("Interval_Number").stringValue
		      recsRS.moveNext
		    wend
		    recsRS.close
		    
		    
		    for i=1 to DataList_Teams.Rows
		      'Delete any existing intervals
		      SQLStatement="DELETE FROM ccTeamIntervals WHERE TeamID = "+DataList_Teams.Row(i).ItemData
		      App.theDB.DBSQLExecute(SQLStatement)
		      
		      for j=1 to Ubound(IntervalNumber)
		        'add the new intervals
		        SQLStatement="INSERT INTO ccTeamIntervals (TeamID,Interval_Number) "
		        SQLStatement=SQLStatement+" VALUES ("
		        SQLStatement=SQLStatement+DataList_Teams.Row(i).ItemData+","
		        SQLStatement=SQLStatement+"'"+IntervalNumber(j)+"')" 
		        app.theDB.DBSQLExecute(SQLStatement)
		        App.theDB.DBCommit
		      next
		      Progress.UpdateProg1(i)
		    next
		    
		    Progress.Close
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ExecuteQuery(Table as string,stExtraQuery as string, SortString as string)
		  dim i as integer
		  dim dateFieldData as date
		  dim currentDate as new date
		  dim SelectStatement as String
		  dim recCount as integer
		  dim res as boolean
		  dim dataString as string
		  dim rsQueryResults, rsParticipants, rsIntervals, rsTeamMembers as RecordSet
		  Dim Tab,CR as String
		  Tab=Chr(9)  //returns a tab
		  CR=Chr(13) //returns carriage return
		  i=0
		  
		  select case Table
		  case "Divisions"
		    ExtraQuery_Divisions=stExtraQuery
		    DataList_Divisions.Rows=0
		    SelectStatement="SELECT Division_Name, Low_Age, High_Age, Gender, Start_Time, List_Order, divisions.rowid, Import_Name, RaceDistance_Name FROM divisions INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid WHERE divisions.RaceID = 1"
		    if ExtraQuery_Divisions<>"" then
		      SelectStatement=SelectStatement+" AND "+ExtraQuery_Divisions
		    end if
		    DivisionQueryStatement=SelectStatement+" ORDER BY "+SortString
		    rsQueryResults=app.theDB.DBSQLSelect(DivisionQueryStatement)
		    DivisionsFound.Text=format(rsQueryResults.RecordCount,"###,##0") + " found"
		    DataList_Divisions.Rows=rsQueryResults.RecordCount
		    
		    DataList_Divisions.LockDrawing = true
		    for i=1 to rsQueryResults.RecordCount
		      
		      DataList_Divisions.CellText(1,i)=rsQueryResults.Field("Division_Name").stringValue
		      DataList_Divisions.CellText(2,i)=rsQueryResults.Field("Low_Age").stringValue
		      DataList_Divisions.CellText(3,i)=rsQueryResults.Field("High_Age").stringValue
		      DataList_Divisions.CellText(4,i)=rsQueryResults.Field("Gender").StringValue
		      DataList_Divisions.CellText(5,i)=rsQueryResults.Field("RaceDistance_Name").StringValue
		      DataList_Divisions.CellText(6,i)=rsQueryResults.Field("Import_Name").StringValue
		      DataList_Divisions.CellText(7,i)=rsQueryResults.Field("Start_Time").StringValue
		      DataList_Divisions.CellText(8,i)=rsQueryResults.Field("List_Order").StringValue
		      DataList_Divisions.Row(i).ItemData=rsQueryResults.Field("rowid").StringValue
		      
		      rsQueryResults.MoveNext
		    next
		    
		    DataList_Divisions.LockDrawing=false
		    DataList_Divisions.Refresh
		    rsQueryResults.Close
		    
		  case "Participants"
		    ExecuteQuery_Participants(Table,stExtraQuery,SortString)
		    
		    
		  case "CCTeams"
		    ExtraQuery_Teams=stExtraQuery
		    DataList_Divisions.Rows=0
		    SelectStatement="SELECT Team_Name, ccTeams.Gender, Division_Name, ccTeams.rowid FROM ccTeams INNER JOIN divisions ON ccTeams.DivisionID = divisions.rowid "
		    if ExtraQuery_Teams<>"" then
		      SelectStatement=SelectStatement+" WHERE "+ExtraQuery_Teams
		    end if
		    TeamQueryStatement=SelectStatement+" ORDER BY "+SortString
		    rsQueryResults=app.theDB.DBSQLSelect(TeamQueryStatement)
		    TeamsFound.Text=format(rsQueryResults.RecordCount,"###,##0") + " found"
		    DataList_Teams.Rows=rsQueryResults.RecordCount
		    
		    DataList_Teams.LockDrawing = true
		    for i=1 to rsQueryResults.RecordCount
		      
		      DataList_Teams.CellText(1,i)=rsQueryResults.Field("Team_Name").stringValue
		      
		      select case rsQueryResults.Field("Gender").stringValue
		      case "M"
		        DataList_Teams.CellText(2,i)="Male"
		      case "F"
		        DataList_Teams.CellText(2,i)="Female"
		      case "X"
		        DataList_Teams.CellText(2,i)="Mixed"
		      end select
		      
		      DataList_Teams.CellText(3,i)=rsQueryResults.Field("Division_Name").stringValue
		      
		      DataList_Teams.Row(i).ItemData=rsQueryResults.Field("rowid").StringValue
		      
		      rsQueryResults.MoveNext
		    next
		    
		    DataList_Teams.LockDrawing=false
		    DataList_Teams.Refresh
		    rsQueryResults.Close
		    
		  case "Times"
		    ExtraQuery_Times=stExtraQuery
		    DataList_Times.Rows=0
		    SelectStatement="SELECT participants.Racer_Number, participants.Participant_Name, participants.First_Name, times.Total_Time, times.Actual_Time,"
		    SelectStatement=SelectStatement+" times.Interval_Time, participants.DivisionID, times.Interval_Number, times.Use_This_Passing, times.TX_Code, times.TeamMemberID, "
		    SelectStatement=SelectStatement+" times.rowid FROM times LEFT OUTER JOIN participants ON times.ParticipantID = participants.rowid "
		    SelectStatement=SelectStatement+" WHERE times.RaceID =1"
		    if ExtraQuery_Times<>"" then
		      if InStr(ExtraQuery_Times,"Racer_Number")>0 then
		        SelectStatement=SelectStatement+" AND "+ExtraQuery_Times.Replace("times","participants")
		      elseif InStr(ExtraQuery_Times,"Unassigned")>0 then
		        SelectStatement=SelectStatement+" AND times.ParticipantID=0"
		      else
		        SelectStatement=SelectStatement+" AND "+ExtraQuery_Times
		      end if
		    end if
		    TimeQueryStatement=SelectStatement+" ORDER BY "+SortString
		    
		    rsQueryResults=app.theDB.DBSQLSelect(TimeQueryStatement)
		    TimesFound.Text=format(rsQueryResults.RecordCount,"###,##0") + " found"
		    DataList_Times.Rows=rsQueryResults.RecordCount
		    
		    DataList_Times.LockDrawing = true
		    for i=1 to rsQueryResults.RecordCount
		      
		      DataList_Times.CellText(1,i)=rsQueryResults.field("Racer_Number").stringValue
		      DataList_Times.CellText(2,i)=rsQueryResults.field("First_Name").StringValue+" "+rsQueryResults.Field("Participant_Name").StringValue
		      
		      if rsQueryResults.Field("TeamMemberID").StringValue <> "" then
		        rsTeamMembers=app.theDB.DBSQLSelect("SELECT MemberName FROM teamMembers WHERE rowid = "+rsQueryResults.Field("TeamMemberID").StringValue)
		        if rsTeamMembers.EOF then
		          DataList_Times.CellText(3,i)=""
		        else
		          DataList_Times.CellText(3,i)=rsTeamMembers.field("MemberName").stringValue
		        end if
		        rsTeamMembers.Close
		      else
		        DataList_Times.CellText(3,i)=""
		      end if
		      
		      select case rsQueryResults.field("Interval_Number").IntegerValue
		        
		      case 0
		        DataList_Times.CellText(4,i)="Start"
		        
		      case 9999
		        DataList_Times.CellText(4,i)="Stop"
		        
		      else
		        if rsQueryResults.Field("Interval_Number").StringValue<>"" and rsQueryResults.Field("DivisionID").StringValue<>"" then
		          SelectStatement="SELECT Interval_Name FROM intervals "
		          SelectStatement=SelectStatement+"WHERE DivisionID = "+rsQueryResults.Field("DivisionID").StringValue
		          SelectStatement=SelectStatement+" AND Number = "+rsQueryResults.Field("Interval_Number").StringValue
		          rsIntervals=app.theDB.DBSQLSelect(SelectStatement)
		          if rsIntervals.RecordCount>0 then
		            DataList_Times.CellText(4,i)=rsIntervals.Field("Interval_Name").StringValue
		          else
		            DataList_Times.CellText(4,i)=rsQueryResults.Field("Interval_Number").StringValue
		          end if
		        else
		          DataList_Times.CellText(4,i)=rsQueryResults.Field("Interval_Number").StringValue
		        end if
		      end select
		      
		      DataList_Times.CellText(5,i)=rsQueryResults.field("Interval_Time").stringValue
		      DataList_Times.CellText(6,i)=rsQueryResults.field("Total_Time").stringValue
		      DataList_Times.CellText(7,i)=rsQueryResults.field("Actual_Time").stringValue
		      if rsQueryResults.field("Use_This_Passing").stringValue="N" then
		        DataList_Times.CellText(8,i)="No"
		      else
		        DataList_Times.CellText(8,i)="Yes"
		      end if
		      DataList_Times.CellText(9,i)=rsQueryResults.field("TX_Code").stringValue
		      
		      DataList_Times.Row(i).ItemData=rsQueryResults.Field("rowid").StringValue
		      
		      rsQueryResults.MoveNext
		    next
		    
		    DataList_Times.LockDrawing=false
		    DataList_Times.Refresh
		    rsQueryResults.Close
		    
		    
		  case "Transponders"
		    ExtraQuery_TX=stExtraQuery
		    DataList_Transponders.Rows=0
		    SelectStatement="SELECT transponders.Racer_Number, transponders.TX_Code, transponders.Issued, transponders.Returned, transponders.rowid, teamMembers.MemberName, "
		    SelectStatement=SelectStatement+ "participants.First_Name, participants.Participant_Name, transponders.Hit_Count "
		    SelectStatement=SelectStatement+ "FROM transponders "
		    SelectStatement=SelectStatement+ "LEFT OUTER JOIN participants ON transponders.Racer_Number = participants.Racer_Number "
		    SelectStatement=SelectStatement+ "LEFT OUTER JOIN teamMembers ON teamMembers.rowid = transponders.TeamMemberID "
		    SelectStatement=SelectStatement+ "WHERE transponders.RaceID = 1"
		    if ExtraQuery_TX<>"" then
		      SelectStatement=SelectStatement+" AND "+ExtraQuery_TX
		    end if
		    
		    TXQueryStatement=SelectStatement+" ORDER BY "+SortString
		    
		    rsQueryResults=app.theDB.DBSQLSelect(TXQueryStatement)
		    TXFound.Text=format(rsQueryResults.RecordCount,"###,##0") + " found"
		    DataList_Transponders.Rows=rsQueryResults.RecordCount
		    
		    DataList_Transponders.LockDrawing = true
		    for i=1 to rsQueryResults.RecordCount
		      
		      DataList_Transponders.CellText(1,i)=rsQueryResults.Field("Racer_Number").stringValue
		      DataList_Transponders.CellText(2,i)=rsQueryResults.Field("TX_Code").stringValue
		      DataList_Transponders.CellText(3,i)=rsQueryResults.Field("First_Name").stringValue+" "+rsQueryResults.Field("Participant_Name").stringValue
		      DataList_Transponders.CellText(4,i)=rsQueryResults.Field("MemberName").stringValue
		      DataList_Transponders.CellText(5,i)=rsQueryResults.Field("Issued").StringValue
		      DataList_Transponders.CellText(6,i)=rsQueryResults.Field("Hit_Count").StringValue
		      DataList_Transponders.CellText(7,i)=rsQueryResults.Field("Returned").StringValue
		      DataList_Transponders.Row(i).ItemData=rsQueryResults.Field("rowid").StringValue
		      
		      rsQueryResults.MoveNext
		    next
		    
		    DataList_Transponders.LockDrawing=false
		    DataList_Transponders.Refresh
		    rsQueryResults.close
		    
		  end select
		  
		  HighlightQueryValue
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ExecuteQuery_Participants(Table as string, stExtraQuery as string, SortString as string)
		  dim i, ii as integer
		  dim TimeDifference, RequestedDifference as double
		  dim SelectStatement, QueryFlag, Temp, TempSelect as String
		  dim res as boolean
		  dim rsQueryResults,rsQueryResults2, rsParticipants, rsTimes as RecordSet
		  Dim row as StyleGridRow
		  
		  
		  ExtraQuery_Participants=stExtraQuery
		  DataList_Participants.Rows=0
		  SelectStatement="SELECT participants.Racer_Number, participants.Participant_Name, First_Name, participants.representing, divisions.Division_Name,"
		  SelectStatement=SelectStatement+" participants.Laps_Completed, participants.Total_Time, participants.DQ, participants.DNS, participants.DNF, participants.rowid, participants.Time_Source"
		  SelectStatement=SelectStatement+" FROM participants LEFT OUTER JOIN divisions ON participants.DivisionID = divisions.rowid"
		  SelectStatement=SelectStatement+" WHERE participants.RaceID =1"
		  
		  
		  select case DataBaseFields_Participants.Text
		    
		  case "Assigned/Actual Start Difference"
		    App.theDB.DBSQLExecute("UPDATE participants SET Query_Flag='N'")
		    App.theDB.DBCommit
		    
		    RequestedDifference=val(ef_query_Participants.text)
		    rsParticipants=app.theDB.DBSQLSelect("SELECT Assigned_Start, Actual_Start, rowid FROM participants WHERE DNS='N'")
		    for i=1 to rsParticipants.RecordCount
		      TimeDifference=abs(app.ConvertTimeToSeconds(rsParticipants.Field("Assigned_Start").StringValue)-app.ConvertTimeToSeconds(rsParticipants.Field("Actual_Start").StringValue))
		      select case Relationship_Participants.text
		        
		      case "=", "LIKE"
		        if TimeDifference=RequestedDifference then
		          QueryFlag="Y"
		        else
		          QueryFlag="N"
		        end if
		        
		      case ">="
		        if TimeDifference>=RequestedDifference then
		          QueryFlag="Y"
		        else
		          QueryFlag="N"
		        end if
		      case "<="
		        if TimeDifference<=RequestedDifference then
		          QueryFlag="Y"
		        else
		          QueryFlag="N"
		        end if
		      case "<>"
		        if TimeDifference<>RequestedDifference then
		          QueryFlag="Y"
		        else
		          QueryFlag="N"
		        end if
		      end select
		      
		      App.theDB.DBSQLExecute("UPDATE participants SET Query_Flag='"+QueryFlag+"' WHERE rowid="+rsParticipants.Field("rowid").StringValue)
		      App.theDB.DBCommit
		      
		      rsParticipants.MoveNext
		      
		    next
		    
		    SelectStatement=SelectStatement+" AND Query_Flag='Y'"
		    
		  case "Has Passed Between These Points"
		    App.theDB.DBSQLExecute("UPDATE participants SET Query_Flag='N'")
		    App.theDB.DBCommit
		    
		    rsParticipants=app.theDB.DBSQLSelect("SELECT rowid FROM participants ")
		    for i=1 to rsParticipants.RecordCount
		      TempSelect="SELECT rowid FROM times WHERE ParticipantID="+rsParticipants.Field("rowid").StringValue
		      TempSelect=TempSelect+" AND Interval_Number="+TimeSource1.RowTag(TimeSource1.ListIndex)
		      rsQueryResults=app.theDB.DBSQLSelect(TempSelect)
		      
		      TempSelect="SELECT rowid FROM times WHERE ParticipantID="+rsParticipants.Field("rowid").StringValue
		      TempSelect=TempSelect+" AND Interval_Number="+TimeSource2.RowTag(TimeSource2.ListIndex)
		      rsQueryResults2=app.theDB.DBSQLSelect(TempSelect)
		      
		      if rsQueryResults.RecordCount>=1 and rsQueryResults2.RecordCount>=1 then
		        App.theDB.DBSQLExecute("UPDATE participants SET Query_Flag='Y' WHERE rowid="+rsParticipants.Field("rowid").StringValue)
		        App.theDB.DBCommit
		      end if
		      
		      rsParticipants.MoveNext
		    next
		    
		    SelectStatement=SelectStatement+" AND Query_Flag='Y'"
		    
		  case "Has NOT Passed Between These Points"
		    App.theDB.DBSQLExecute("UPDATE participants SET Query_Flag='N'")
		    App.theDB.DBCommit
		    
		    rsParticipants=app.theDB.DBSQLSelect("SELECT rowid FROM participants ")
		    for i=1 to rsParticipants.RecordCount
		      TempSelect="SELECT rowid FROM times WHERE ParticipantID="+rsParticipants.Field("rowid").StringValue
		      TempSelect=TempSelect+" AND Interval_Number="+TimeSource1.RowTag(TimeSource1.ListIndex)
		      rsQueryResults=app.theDB.DBSQLSelect(TempSelect)
		      
		      TempSelect="SELECT rowid FROM times WHERE ParticipantID="+rsParticipants.Field("rowid").StringValue
		      TempSelect=TempSelect+" AND Interval_Number="+TimeSource2.RowTag(TimeSource2.ListIndex)
		      rsQueryResults2=app.theDB.DBSQLSelect(TempSelect)
		      
		      if rsQueryResults.RecordCount>=1 and rsQueryResults2.RecordCount=0 then
		        App.theDB.DBSQLExecute("UPDATE participants SET Query_Flag='Y' WHERE rowid="+rsParticipants.Field("rowid").StringValue)
		        App.theDB.DBCommit
		      end if
		      
		      rsParticipants.MoveNext
		    next
		    SelectStatement=SelectStatement+" AND Query_Flag='Y'"
		    
		  case "Has a NGB License and Inactive Points"
		    SelectStatement=SelectStatement+" AND NGB_License_Number<>'' AND NGB1_Points='inact' "
		    
		  case "Has a NGB2 License and Inactive Points"
		    SelectStatement=SelectStatement+" AND NGB2_License_Number<>'' AND NGB2_Points='inact' "
		    
		  else
		    
		    if ExtraQuery_Participants<>"" then
		      if InStr(ExtraQuery_Participants,"Division")>0 then
		        SelectStatement=SelectStatement+" AND "+ExtraQuery_Participants.Replace("participants","divisions")
		      else
		        SelectStatement=SelectStatement+" AND "+ExtraQuery_Participants
		      end if
		    end if
		    
		  end select
		  
		  if SortString="" then
		    SortString="Racer_Number"
		  end if
		  ParticipantQueryStatement=SelectStatement+" ORDER BY "+SortString
		  
		  rsQueryResults=app.theDB.DBSQLSelect(ParticipantQueryStatement)
		  ParticipantsFound.Text=format(rsQueryResults.RecordCount,"###,##0") + " found"
		  DataList_Participants.Rows=rsQueryResults.RecordCount
		  DataList_Participants.LockDrawing = true
		  
		  for i=1 to rsQueryResults.RecordCount
		    DataList_Participants.CellText(1,i)=rsQueryResults.Field("Racer_Number").stringValue
		    
		    if rsQueryResults.Field("First_Name").StringValue<>"" then
		      DataList_Participants.CellText(2,i)=rsQueryResults.Field("First_Name").stringValue+" "+rsQueryResults.Field("Participant_Name").stringValue
		    else
		      DataList_Participants.CellText(2,i)=rsQueryResults.Field("Participant_Name").stringValue
		    end if
		    DataList_Participants.CellText(3,i)=rsQueryResults.Field("Representing").stringValue
		    DataList_Participants.CellText(4,i)=rsQueryResults.Field("Division_Name").stringValue
		    DataList_Participants.CellText(5,i)=rsQueryResults.Field("Time_Source").stringValue
		    DataList_Participants.CellText(6,i)=rsQueryResults.Field("Laps_Completed").stringValue
		    if rsQueryResults.Field("DQ").StringValue = "Y" then
		      DataList_Participants.CellText(7,i)="DQ"
		      
		    elseif rsQueryResults.Field("DNS").StringValue = "Y" then
		      DataList_Participants.CellText(7,i)="DNS"
		      
		    elseif rsQueryResults.Field("DNF").StringValue = "Y" then
		      DataList_Participants.CellText(7,i)="DNF"
		      
		    else
		      DataList_Participants.CellText(7,i)=app.StripLeadingZeros(rsQueryResults.Field("Total_Time").StringValue)
		    end if
		    
		    DataList_Participants.Row(i).ItemData=rsQueryResults.Field("rowid").StringValue
		    
		    SelectStatement="SELECT Interval_Time, Interval_Number FROM times WHERE ParticipantID="+rsQueryResults.Field("rowid").StringValue+" AND Use_This_Passing='Y' AND Interval_Number>0 AND Interval_Number<9900"
		    rsTimes=app.theDB.DBSQLSelect(SelectStatement)
		    for ii = 1 to rsTimes.RecordCount
		      DataList_Participants.CellText(Participant_Columns+rsTimes.Field("Interval_Number").IntegerValue,i)=app.StripLeadingZeros(rsTimes.Field("Interval_Time").StringValue)
		      rsTimes.MoveNext
		    next
		    rsTimes.Close
		    
		    rsQueryResults.MoveNext
		  next
		  
		  
		  DataList_Participants.LockDrawing=false
		  DataList_Participants.Refresh
		  rsQueryResults.close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ExportDivisions()
		  Dim file As FolderItem
		  Dim fileStream As TextOutputStream
		  Dim Delimiter, FileName, OutputData, SelectStatement as string
		  Dim rsDistance as RecordSet
		  Dim i, ii as Integer
		  
		  if TargetMacOS then
		    Delimiter=chr(9)
		    FileName="Division Export.txt"
		  else
		    Delimiter=","
		    FileName="Division Export.csv"
		  end if
		  
		  file= GetSaveFolderItem("application/text",FileName)
		  if file<>nil then
		    
		    SelectStatement="SELECT Division_Name, Import_Name, Low_Age, High_Age, Gender, Start_Time, List_Order,"
		    SelectStatement=SelectStatement+" RaceDistance_Name FROM divisions LEFT JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid WHERE divisions.RaceID =1"
		    rsDistance=app.theDB.DBSQLSelect(SelectStatement)
		    
		    if rsDistance.RecordCount> 0 then
		      Progress.Show
		      Progress.Initialize("Progress","Exporting Division Data...",rsDistance.RecordCount,-1)
		      fileStream=file.CreateTextFile
		      
		      OutputData= "Division Name"+Delimiter
		      OutputData=OutputData + "Category"+Delimiter
		      OutputData=OutputData + "Low Age"+Delimiter
		      OutputData=OutputData + "High Age"+Delimiter
		      OutputData=OutputData + "Gender"+Delimiter
		      OutputData=OutputData + "Start Time"+Delimiter
		      OutputData=OutputData + "Order"+Delimiter
		      OutputData=OutputData + "Race Distance"+Delimiter
		      fileStream.WriteLine OutputData
		      
		      for i=1 to rsDistance.RecordCount
		        Progress.UpdateProg1(i)
		        OutputData=""
		        for ii=1 to rsDistance.FieldCount
		          OutputData=OutputData+rsDistance.IdxField(ii).StringValue+Delimiter
		        next
		        OutputData=Left(OutputData,len(OutputData)-1) 'remove that last Delimiter
		        OutputData=OutputData
		        fileStream.WriteLine OutputData
		        rsDistance.MoveNext
		      next
		      fileStream.Close
		      Progress.Close
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ExportNewspaperResults()
		  Dim file As FolderItem
		  Dim fileStream As TextOutputStream
		  Dim OldDivision, Delimiter, FileName, OutputData, SelectStatement as string
		  Dim rsParticipants, rsTimes as RecordSet
		  Dim i, ii as Integer
		  
		  Delimiter=";;"
		  
		  file= GetSaveFolderItem("application/text",wnd_list.RaceName.Text+".txt")
		  if file<>nil then
		    
		    SelectStatement="SELECT participants.First_Name, participants.Participant_Name, participants.Representing, participants.Gender, participants.Laps_Completed, "
		    SelectStatement=SelectStatement+"divisions.Division_Name, participants.Total_Time, divisions.RaceDistanceID, divisions.rowid FROM participants "
		    SelectStatement=SelectStatement+"LEFT JOIN divisions ON participants.DivisionID = divisions.rowid "
		    SelectStatement=SelectStatement+"WHERE DNF='N' ORDER BY divisions.Division_Name ASC, participants.Total_Time ASC"
		    rsParticipants=app.theDB.DBSQLSelect(SelectStatement)
		    
		    if rsParticipants.RecordCount> 0 then
		      Progress.Show
		      Progress.Initialize("Progress","Exporting Newspaper Data...",rsParticipants.RecordCount,-1)
		      fileStream=file.CreateTextFile
		      
		      for i=1 to rsParticipants.RecordCount
		        Progress.UpdateProg1(i)
		        
		        if OldDivision<>rsParticipants.Field("Division_Name").StringValue then
		          OldDivision=rsParticipants.Field("Division_Name").StringValue
		          fileStream.WriteLine
		          fileStream.WriteLine
		          fileStream.WriteLine OldDivision
		          fileStream.WriteLine "Div Pl;;Ovrall Pl;;Name;;Representing;;Time"
		          fileStream.WriteLine
		        end if
		        
		        OutputData=""
		        OutputData=OutputData + app.GetPlace("Division", rsParticipants.Field("RaceDistanceID").StringValue, rsParticipants.Field("rowid").StringValue, rsParticipants.Field("Gender").StringValue, rsParticipants.Field("Total_Time").StringValue, false, rsParticipants.Field("Laps_Completed").StringValue)+Delimiter
		        OutputData=OutputData + app.GetPlace("Overall", rsParticipants.Field("RaceDistanceID").StringValue, rsParticipants.Field("rowid").StringValue, rsParticipants.Field("Gender").StringValue, rsParticipants.Field("Total_Time").StringValue, false, rsParticipants.Field("Laps_Completed").StringValue)+Delimiter
		        OutputData=OutputData + rsParticipants.Field("First_Name").StringValue+" "+rsParticipants.Field("Participant_Name").StringValue+Delimiter
		        OutputData=OutputData + rsParticipants.Field("Representing").StringValue+Delimiter
		        OutputData=OutputData + rsParticipants.Field("Total_Time").StringValue+Delimiter
		        
		        fileStream.WriteLine OutputData
		        rsParticipants.MoveNext
		      next
		      fileStream.Close
		      Progress.Close
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ExportParticipantData()
		  Dim file As FolderItem
		  Dim fileStream As TextOutputStream
		  Dim Temp, Delimiter, FileName, OutputData, SelectStatement, TXOut,TXReturnedOUT as string
		  Dim rsParticipants, rsTimes, rsTX, rsTeam as RecordSet
		  Dim i, ii as Integer
		  Dim TempDate as date
		  dim AssignedStart, ActualStop, Adjustment, TotalTime as double
		  
		  TempDate=new date
		  
		  if TargetMacOS then
		    Delimiter=chr(9)
		    FileName="Participant Export.txt"
		  else
		    Delimiter=","
		    FileName="Participant Export.csv"
		  end if
		  
		  file= GetSaveFolderItem("application/text",FileName)
		  if file<>nil then
		    
		    SelectStatement="SELECT participants.rowid, participants.TeamID, participants.Racer_Number, participants.First_Name, participants.Participant_Name, participants.Gender, participants.Age, participants.Birth_Date, "
		    SelectStatement=SelectStatement+"divisions.RaceDistanceID, participants.DivisionID, participants.Laps_Completed, divisions.Import_Name, participants.Representing, "
		    SelectStatement=SelectStatement+"participants.Assigned_Start, participants.NGB_License_Number, participants.NGB1_Points, participants.NGB2_License_Number, participants.NGB2_Points, participants.Seed_Group, "
		    SelectStatement=SelectStatement+"divisions.Division_Name, participants.Laps_Completed, participants.Total_Time, participants.Net_Time, participants.Assigned_Start, participants.Actual_Start, "
		    SelectStatement=SelectStatement+"participants.Actual_Stop, participants.Total_Adjustment, participants.DNS, participants.DNF, participants.DQ, "
		    SelectStatement=SelectStatement+"participants.Street, participants.City, participants.State, participants.Postal_Code, participants.Country, participants.EMail, participants.Phone, participants.DateTimeInserted, Note "
		    SelectStatement=SelectStatement+"FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		    SelectStatement=SelectStatement+"WHERE participants.RaceID =1 ORDER BY participants.Racer_Number"
		    rsParticipants=app.theDB.DBSQLSelect(SelectStatement)
		    
		    if rsParticipants.RecordCount> 0 then
		      Progress.Show
		      Progress.Initialize("Progress","Exporting Participant Data...",rsParticipants.RecordCount,-1)
		      fileStream=file.CreateTextFile
		      
		      OutputData= "Racer Number"+Delimiter
		      OutputData=OutputData + "First Name"+Delimiter
		      OutputData=OutputData + "Name"+Delimiter
		      OutputData=OutputData + "Gender"+Delimiter
		      OutputData=OutputData + "Age"+Delimiter
		      OutputData=OutputData + "Birth Date"+Delimiter
		      OutputData=OutputData + "Category"+Delimiter
		      OutputData=OutputData + "Representing"+Delimiter
		      OutputData=OutputData + "Declared Team"+Delimiter
		      OutputData=OutputData + "Start Date"+Delimiter
		      OutputData=OutputData + "Start Time"+Delimiter
		      OutputData=OutputData + "NGB License"+Delimiter
		      OutputData=OutputData + "NGB Points"+Delimiter
		      OutputData=OutputData + "NGB2 License"+Delimiter
		      OutputData=OutputData + "NGB2 Points"+Delimiter
		      OutputData=OutputData + "Seed Group" +Delimiter
		      OutputData=OutputData + "Overall Place"+Delimiter
		      OutputData=OutputData + "Overall Back"+Delimiter
		      OutputData=OutputData + "Gender Place"+Delimiter
		      OutputData=OutputData + "Gender Back"+Delimiter
		      OutputData=OutputData + "Division Place"+Delimiter
		      OutputData=OutputData + "Division Back"+Delimiter
		      OutputData=OutputData + "Division Name"+Delimiter
		      OutputData=OutputData + "Laps Completed" +Delimiter
		      OutputData=OutputData + "Total Time"+Delimiter
		      OutputData=OutputData + "Chip Time"+Delimiter
		      OutputData=OutputData + "Gun Time"+Delimiter
		      OutputData=OutputData + "Assigned Start"+Delimiter
		      OutputData=OutputData + "Actual Start"+Delimiter
		      OutputData=OutputData + "Actual Stop"+Delimiter
		      OutputData=OutputData + "TX Code"+Delimiter
		      OutputData=OutputData + "TX Issued"+Delimiter
		      OutputData=OutputData + "TX Returned"+Delimiter
		      OutputData=OutputData + "Street"+Delimiter
		      OutputData=OutputData + "City"+Delimiter
		      OutputData=OutputData + "State"+Delimiter
		      OutputData=OutputData + "Postal Code"+Delimiter
		      OutputData=OutputData + "Country"+Delimiter
		      OutputData=OutputData + "EMail"+Delimiter
		      OutputData=OutputData + "Phone"+Delimiter
		      OutputData=OutputData + "Entered"+Delimiter
		      OutputData=OutputData + "Extra"+Delimiter
		      fileStream.WriteLine OutputData
		      
		      for i=1 to rsParticipants.RecordCount
		        Progress.UpdateProg1(i)
		        OutputData=""
		        OutputData=OutputData +rsParticipants.Field("Racer_Number").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("First_Name").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Participant_Name").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Gender").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Age").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Birth_Date").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Import_Name").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Representing").StringValue+Delimiter
		        
		        SelectStatement="SELECT Team_Name FROM CCTeams WHERE rowid="+rsParticipants.Field("TeamID").StringValue
		        rsTeam=app.theDB.DBSQLSelect(SelectStatement)
		        if rsTeam<>Nil then
		          if rsTeam.RecordCount>0 then
		            OutputData=OutputData +rsTeam.Field("Team_Name").StringValue+Delimiter
		          else
		            OutputData=OutputData +"Non-Scoring Participant"+Delimiter
		          end if
		          
		          
		        else
		          OutputData=OutputData +"Non-Scoring Participant"+Delimiter
		        end if
		        
		        
		        TempDate.SQLDateTime=rsParticipants.Field("Assigned_Start").StringValue
		        
		        OutputData=OutputData +format(TempDate.Month,"00")+"/"+format(TempDate.Day,"00")+"/"+format(TempDate.Year,"00")+Delimiter
		        OutputData=OutputData +format(TempDate.Hour,"00")+":"+format(TempDate.Minute,"00")+":"+format(TempDate.Second,"00")+Delimiter
		        
		        OutputData=OutputData +rsParticipants.Field("NGB_License_Number").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("NGB1_Points").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("NGB2_License_Number").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("NGB2_Points").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Seed_Group").StringValue+Delimiter
		        if rsParticipants.Field("DQ").StringValue<>"Y" and rsParticipants.Field("DNS").StringValue<>"Y" and rsParticipants.Field("DNF").StringValue<>"Y" then
		          OutputData=OutputData +app.GetPlace("Overall",rsParticipants.Field("RaceDistanceID").StringValue,rsParticipants.Field("DivisionID").StringValue, rsParticipants.Field("Gender").StringValue, _
		          rsParticipants.Field("Total_Time").StringValue,false,rsParticipants.Field("Laps_Completed").StringValue)+Delimiter
		          OutputData=OutputData + app.GetBack("Overall",rsParticipants.Field("RaceDistanceID").StringValue,rsParticipants.Field("DivisionID").StringValue,rsParticipants.Field("Gender").StringValue,rsParticipants.Field("Total_Time").StringValue)+Delimiter
		          
		          OutputData=OutputData +app.GetPlace("Gender",rsParticipants.Field("RaceDistanceID").StringValue,rsParticipants.Field("DivisionID").StringValue, rsParticipants.Field("Gender").StringValue, _
		          rsParticipants.Field("Total_Time").StringValue,false,rsParticipants.Field("Laps_Completed").StringValue)+Delimiter
		          OutputData=OutputData + app.GetBack("Gender",rsParticipants.Field("RaceDistanceID").StringValue,rsParticipants.Field("DivisionID").StringValue,rsParticipants.Field("Gender").StringValue,rsParticipants.Field("Total_Time").StringValue)+Delimiter
		          
		          OutputData=OutputData +app.GetPlace("Division",rsParticipants.Field("RaceDistanceID").StringValue,rsParticipants.Field("DivisionID").StringValue, rsParticipants.Field("Gender").StringValue, _
		          rsParticipants.Field("Total_Time").StringValue,false,rsParticipants.Field("Laps_Completed").StringValue)+Delimiter
		          OutputData=OutputData + app.GetBack("Division",rsParticipants.Field("RaceDistanceID").StringValue,rsParticipants.Field("DivisionID").StringValue,rsParticipants.Field("Gender").StringValue,rsParticipants.Field("Total_Time").StringValue)+Delimiter
		        else
		          OutputData=""+OutputData+""+Delimiter+""+Delimiter+""+Delimiter+""+Delimiter+""+Delimiter+""+Delimiter
		        end if
		        OutputData=OutputData +rsParticipants.Field("Division_Name").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Laps_Completed").StringValue+Delimiter
		        
		        if rsParticipants.Field("DQ").StringValue="Y" then
		          OutputData=OutputData +"DQ"+Delimiter
		          OutputData=OutputData +""+Delimiter
		          OutputData=OutputData +""+Delimiter
		          OutputData=OutputData +rsParticipants.Field("Assigned_Start").StringValue+Delimiter
		          OutputData=OutputData +rsParticipants.Field("Actual_Start").StringValue+Delimiter
		          OutputData=OutputData +rsParticipants.Field("Actual_Stop").StringValue+Delimiter
		        elseif rsParticipants.Field("DNS").StringValue="Y" then
		          OutputData=OutputData +"DNS"+Delimiter
		          OutputData=OutputData +""+Delimiter
		          OutputData=OutputData +""+Delimiter
		          OutputData=OutputData +rsParticipants.Field("Assigned_Start").StringValue+Delimiter
		          OutputData=OutputData +""+Delimiter
		          OutputData=OutputData +""+Delimiter
		        elseif rsParticipants.Field("DNF").StringValue="Y" then
		          OutputData=OutputData +"DNF"+Delimiter
		          OutputData=OutputData +""+Delimiter
		          OutputData=OutputData +""+Delimiter
		          OutputData=OutputData +rsParticipants.Field("Assigned_Start").StringValue+Delimiter
		          OutputData=OutputData +rsParticipants.Field("Actual_Start").StringValue+Delimiter
		          OutputData=OutputData +""+Delimiter
		        else
		          OutputData=OutputData +rsParticipants.Field("Total_Time").StringValue+Delimiter
		          OutputData=OutputData +rsParticipants.Field("Net_Time").StringValue+Delimiter
		          
		          'gun time
		          AssignedStart=app.ConvertTimeToSeconds(rsParticipants.Field("Assigned_Start").stringValue)
		          ActualStop=app.ConvertTimeToSeconds(rsParticipants.Field("Actual_Stop").StringValue)
		          Adjustment=app.ConvertTimeToSeconds(rsParticipants.Field("Total_Adjustment").stringValue)
		          TotalTime=round(((ActualStop-AssignedStart)+Adjustment)*1000)/1000
		          
		          OutputData=OutputData +app.TruncateTime(app.ConvertSecondsToTime(TotalTime,false))+Delimiter
		          
		          
		          OutputData=OutputData +rsParticipants.Field("Assigned_Start").StringValue+Delimiter
		          OutputData=OutputData +rsParticipants.Field("Actual_Start").StringValue+Delimiter
		          OutputData=OutputData +rsParticipants.Field("Actual_Stop").StringValue+Delimiter
		        end if
		        
		        SelectStatement="SELECT TX_Code, Issued, Returned FROM transponders WHERE Racer_Number="+rsParticipants.Field("Racer_Number").StringValue
		        rsTX=app.theDB.DBSQLSelect(SelectStatement)
		        if rsTX.RecordCount>0 then
		          TXOut=""
		          TXReturnedOUT=""
		          for ii=1 to rsTX.RecordCount
		            TXOut=TXOut+rsTX.Field("TX_Code").StringValue+" "
		            TXReturnedOUT=TXReturnedOUT+rsTX.Field("Issued").StringValue+Delimiter+rsTX.Field("Returned").StringValue+" "
		            rsTX.MoveNext
		          next
		          OutputData=OutputData +TXOut+Delimiter
		          OutputData=OutputData +TXReturnedOUT+Delimiter
		        else
		          OutputData=OutputData +"No Transponder Assigned"+Delimiter
		          OutputData=OutputData +""+Delimiter+""+Delimiter
		        end if
		        OutputData=OutputData +rsParticipants.Field("Street").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("City").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("State").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Postal_Code").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Country").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("EMail").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Phone").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("DateTimeInserted").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Note").StringValue+Delimiter
		        
		        SelectStatement="SELECT Interval_Time, Total_Time FROM times WHERE ParticipantID="+rsParticipants.Field("rowid").StringValue+" AND Use_This_Passing='Y' "
		        SelectStatement=SelectStatement+"AND Interval_Number > 0 AND Interval_Number< 9900 ORDER BY Interval_Number ASC"
		        rsTimes=app.theDB.DBSQLSelect(SelectStatement)
		        for ii=1 to rsTimes.RecordCount
		          OutputData=OutputData+rsTimes.Field("Interval_Time").StringValue+Delimiter
		          OutputData=OutputData+rsTimes.Field("Total_Time").StringValue+Delimiter
		          rsTimes.MoveNext
		        next
		        
		        OutputData=OutputData
		        fileStream.WriteLine OutputData
		        rsParticipants.MoveNext
		      next
		      fileStream.Close
		      Progress.Close
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ExportParticipantOBRAData()
		  Dim file As FolderItem
		  Dim fileStream As TextOutputStream
		  Dim Temp, Delimiter, FileName, OutputData, SelectStatement, OldDivisionName as string
		  Dim rsParticipants, rsTimes, rsTX as RecordSet
		  Dim i, ii as Integer
		  Dim TempDate as date
		  
		  TempDate=new date
		  
		  if TargetMacOS then
		    Delimiter=chr(9)
		    FileName="OBRA CX Export.txt"
		  else
		    Delimiter=","
		    FileName="OBRA CX Export.csv"
		  end if
		  
		  file= GetSaveFolderItem("application/text",FileName)
		  if file<>nil then
		    
		    SelectStatement="SELECT participants.rowid, participants.Racer_Number, participants.First_Name, participants.Participant_Name, participants.Gender, participants.Age, "
		    SelectStatement=SelectStatement+"divisions.RaceDistanceID, participants.DivisionID, participants.Laps_Completed, participants.Total_Time, participants.Representing, "
		    SelectStatement=SelectStatement+"participants.NGB_License_Number, participants.Seed_Group, "
		    SelectStatement=SelectStatement+"divisions.Division_Name,  "
		    SelectStatement=SelectStatement+"participants.DNS, participants.DNF, participants.DQ, "
		    SelectStatement=SelectStatement+"participants.City "
		    SelectStatement=SelectStatement+"FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		    SelectStatement=SelectStatement+"WHERE participants.RaceID =1 ORDER BY divisions.Division_Name, participants.DNF, participants.DNS, participants.DQ, participants.Laps_Completed DESC, participants.Total_Time"
		    rsParticipants=app.theDB.DBSQLSelect(SelectStatement)
		    
		    if rsParticipants.RecordCount> 0 then
		      OldDivisionName=""
		      Progress.Show
		      Progress.Initialize("Progress","Exporting Participant Data...",rsParticipants.RecordCount,-1)
		      fileStream=file.CreateTextFile
		      
		      OutputData= "Place"+Delimiter
		      OutputData=OutputData + "Laps"+Delimiter
		      OutputData=OutputData + "Number"+Delimiter
		      OutputData=OutputData + "OBRA #"+Delimiter
		      OutputData=OutputData + "First Name"+Delimiter
		      OutputData=OutputData + "Last Name"+Delimiter
		      OutputData=OutputData + "Gender"+Delimiter
		      OutputData=OutputData + "Age"+Delimiter
		      OutputData=OutputData + "Category"+Delimiter
		      OutputData=OutputData + "Team"+Delimiter
		      OutputData=OutputData + "City"
		      fileStream.WriteLine OutputData
		      
		      for i=1 to rsParticipants.RecordCount
		        Progress.UpdateProg1(i)
		        OutputData=""
		        
		        if OldDivisionName<>rsParticipants.Field("Division_Name").StringValue then
		          OutputData=rsParticipants.Field("Division_Name").StringValue
		          OldDivisionName=rsParticipants.Field("Division_Name").StringValue
		          fileStream.WriteLine OutputData
		          OutputData=""
		        end if
		        
		        if rsParticipants.Field("DQ").StringValue="Y" then
		          OutputData=OutputData +"DQ"+Delimiter
		          OutputData=OutputData +""+Delimiter
		        elseif rsParticipants.Field("DNS").StringValue="Y" then
		          OutputData=OutputData +"DNS"+Delimiter
		          OutputData=OutputData +""+Delimiter
		        elseif rsParticipants.Field("DNF").StringValue="Y" then
		          OutputData=OutputData +"DNF"+Delimiter
		          OutputData=OutputData +""+Delimiter
		        else
		          OutputData=OutputData +app.GetPlace("Division",rsParticipants.Field("RaceDistanceID").StringValue,rsParticipants.Field("DivisionID").StringValue, rsParticipants.Field("Gender").StringValue, _
		          rsParticipants.Field("Total_Time").StringValue,false,rsParticipants.Field("Laps_Completed").StringValue)+Delimiter
		          OutputData=OutputData +rsParticipants.Field("Laps_Completed").StringValue+Delimiter
		        end if
		        
		        OutputData=OutputData +rsParticipants.Field("Racer_Number").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("NGB_License_Number").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("First_Name").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Participant_Name").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Gender").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Age").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Division_Name").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Representing").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("City").StringValue
		        
		        fileStream.WriteLine OutputData
		        rsParticipants.MoveNext
		      next
		      fileStream.Close
		      Progress.Close
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ExportParticipantUSATData()
		  Dim dlg as New SelectFolderDialog
		  Dim f As FolderItem
		  Dim fileStream As TextOutputStream
		  Dim Temp, Delimiter, Path, FileName, OutputData, HeaderData, SelectStatement, OldRaceDistance, NewFileName, TotalTime as string
		  Dim rsParticipants as RecordSet
		  Dim i, ii, PeriodPosition as Integer
		  
		  dlg.ActionButtonCaption="Select"
		  dlg.Title="Race Folder"
		  dlg.PromptText="Select a folder for the USAT files."
		  
		  
		  Delimiter=","
		  FileName="USAT Export.csv"'
		  
		  OldRaceDistance=""
		  
		  HeaderData=  "USAT #"+Delimiter
		  HeaderData=HeaderData + "Last Name"+Delimiter
		  HeaderData=HeaderData + "First Name"+Delimiter
		  HeaderData=HeaderData + "Sex"+Delimiter
		  HeaderData=HeaderData + "DOB"+Delimiter
		  HeaderData=HeaderData + "Email"+Delimiter
		  HeaderData=HeaderData + "Street Address"+Delimiter
		  HeaderData=HeaderData + "City"+Delimiter
		  HeaderData=HeaderData + "State" +Delimiter
		  HeaderData=HeaderData + "Zip Code"+Delimiter
		  HeaderData=HeaderData + "Country"+Delimiter
		  HeaderData=HeaderData + "Category"+Delimiter
		  HeaderData=HeaderData + "Final Time"+Delimiter
		  
		  f=dlg.ShowModal()
		  
		  if f<>nil then
		    Path=f.AbsolutePath
		    
		    
		    
		    SelectStatement="SELECT participants.Racer_Number, participants.Birth_Date, participants.NGB_License_Number, participants.First_Name, participants.Participant_Name, participants.Gender, divisions.Division_Name, "
		    SelectStatement=SelectStatement+"racedistances.RaceDistance_Name, participants.Street, participants.City, participants.State, participants.Postal_Code, participants.Country, participants.Phone, "
		    SelectStatement=SelectStatement+"participants.Email, participants.Total_Time, participants.DQ, participants.DNF, participants.DNS "
		    SelectStatement=SelectStatement+"FROM participants LEFT JOIN divisions ON participants.DivisionID = divisions.rowid LEFT JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		    SelectStatement=SelectStatement+"WHERE participants.RaceID =1 ORDER BY RaceDistance_Name, Participant_Name, First_Name"
		    rsParticipants=app.theDB.DBSQLSelect(SelectStatement)
		    
		    if rsParticipants.RecordCount> 0 then
		      Progress.Show
		      Progress.Initialize("Progress","Exporting Participant Data...",rsParticipants.RecordCount,-1)
		      
		      for i=1 to rsParticipants.RecordCount
		        
		        Progress.UpdateProg1(i)
		        
		        if OldRaceDistance<>rsParticipants.Field("RaceDistance_Name").StringValue then
		          NewFileName=Path+RaceName.Text+" "+RaceDateInput.Text+" "+rsParticipants.Field("RaceDistance_Name").StringValue+" "+FileName
		          f=GetFolderItem(NewFileName)
		          if OldRaceDistance<>"" then
		            fileStream.Close
		          end if
		          fileStream=f.CreateTextFile
		          fileStream.WriteLine HeaderData
		          OldRaceDistance=rsParticipants.Field("RaceDistance_Name").StringValue
		        end if
		        
		        OutputData=""
		        OutputData=OutputData +rsParticipants.Field("NGB_License_Number").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Participant_Name").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("First_Name").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Gender").StringValue+Delimiter
		        OutputData=OutputData +mid(rsParticipants.Field("Birth_Date").StringValue,6,2)+"/"+mid(rsParticipants.Field("Birth_Date").StringValue,9,2)+"/"+left(rsParticipants.Field("Birth_Date").StringValue,4)+Delimiter
		        OutputData=OutputData +rsParticipants.Field("EMail").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Street").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("City").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("State").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Postal_Code").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Country").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("RaceDistance_Name").StringValue+Delimiter
		        if rsParticipants.Field("DQ").StringValue="Y" then
		          OutputData=OutputData +"DQ"+Delimiter
		        elseif rsParticipants.Field("DNS").StringValue="Y" then
		          OutputData=OutputData +"DNS"+Delimiter
		        elseif rsParticipants.Field("DNF").StringValue="Y" then
		          OutputData=OutputData +"DNF"+Delimiter
		        else
		          TotalTime=rsParticipants.Field("Total_Time").StringValue
		          PeriodPosition=instr(TotalTime,".")
		          if (PeriodPosition+2=(len(TotalTime))) or (PeriodPosition+3=(len(TotalTime))) then
		            TotalTime=left(TotalTime,PeriodPosition+1)
		          end if
		          OutputData=OutputData +TotalTime+Delimiter
		        end if
		        
		        OutputData=OutputData
		        fileStream.WriteLine OutputData
		        rsParticipants.MoveNext
		      next
		      fileStream.Close
		      Progress.Close
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ExportParticipantUSSAData()
		  Dim dlg as New SelectFolderDialog
		  Dim f As FolderItem
		  Dim fileStream As TextOutputStream
		  Dim Temp, Delimiter, Path, FileName, OutputData, HeaderData, SelectStatement, OldRaceDistance, NewFileName, TotalTime as string
		  Dim rsParticipants as RecordSet
		  Dim i, ii, PeriodPosition as Integer
		  
		  dlg.ActionButtonCaption="Select"
		  dlg.Title="Race Folder"
		  dlg.PromptText="Select a folder for the USAT files."
		  
		  
		  Delimiter=","
		  FileName="USSA Points Export.csv"'
		  
		  OldRaceDistance=""
		  
		  HeaderData= "Rank"+Delimiter
		  HeaderData=HeaderData + "Bib"+Delimiter
		  HeaderData=HeaderData + "First Name"+Delimiter
		  HeaderData=HeaderData + "Last Name"+Delimiter
		  HeaderData=HeaderData + "Sex"+Delimiter
		  HeaderData=HeaderData + "Birth Date"+Delimiter
		  HeaderData=HeaderData + "Birth Year"+Delimiter
		  HeaderData=HeaderData + "Class"+Delimiter
		  HeaderData=HeaderData + "Affiliation"+Delimiter
		  HeaderData=HeaderData + "Nation"+Delimiter
		  HeaderData=HeaderData + "FIS Status"+Delimiter
		  HeaderData=HeaderData + "FIS Number"+Delimiter
		  HeaderData=HeaderData + "USSA Number"+Delimiter
		  HeaderData=HeaderData + "FIS Points From List"+Delimiter
		  HeaderData=HeaderData + "USSA Points From List"+Delimiter
		  HeaderData=HeaderData + "Time"+Delimiter
		  HeaderData=HeaderData + "Race Points"+Delimiter
		  HeaderData=HeaderData + "User Comments"+Delimiter
		  
		  f=dlg.ShowModal()
		  
		  if f<>nil then
		    Path=f.AbsolutePath
		    
		    SelectStatement="SELECT participants.Racer_Number, participants.First_Name, participants.Participant_Name, participants.Gender, participants.Birth_Date, "
		    SelectStatement=SelectStatement+"divisions.RaceDistanceID, participants.DivisionID, participants.Laps_Completed, participants.Total_Time, participants.Representing, "
		    SelectStatement=SelectStatement+"participants.NGB_License_Number, participants.NGB2_License_Number, participants.NGB1_Points, participants.NGB2_Points, participants.Country, "
		    SelectStatement=SelectStatement+"participants.DNS, participants.DNF, participants.DQ, divisions.Division_Name, RaceDistance_Name, Adjustment_Reason "
		    SelectStatement=SelectStatement+"FROM participants LEFT JOIN divisions ON participants.DivisionID = divisions.rowid LEFT JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		    SelectStatement=SelectStatement+"WHERE participants.RaceID =1 ORDER BY RaceDistance_Name, participants.DNF, participants.DNS, participants.DQ, participants.Total_Time"
		    rsParticipants=app.theDB.DBSQLSelect(SelectStatement)
		    
		    if rsParticipants.RecordCount> 0 then
		      Progress.Show
		      Progress.Initialize("Progress","Exporting Participant Data...",rsParticipants.RecordCount,-1)
		      
		      for i=1 to rsParticipants.RecordCount
		        
		        Progress.UpdateProg1(i)
		        
		        if OldRaceDistance<>rsParticipants.Field("RaceDistance_Name").StringValue then
		          NewFileName=Path+RaceName.Text+" "+RaceDateInput.Text+" "+rsParticipants.Field("RaceDistance_Name").StringValue+" "+FileName
		          f=GetFolderItem(NewFileName)
		          if OldRaceDistance<>"" then
		            fileStream.Close
		          end if
		          fileStream=f.CreateTextFile
		          fileStream.WriteLine HeaderData
		          OldRaceDistance=rsParticipants.Field("RaceDistance_Name").StringValue
		        end if
		        
		        OutputData=""
		        
		        OutputData=OutputData +app.GetPlace("Gender",rsParticipants.Field("RaceDistanceID").StringValue,rsParticipants.Field("DivisionID").StringValue,rsParticipants.Field("Gender").StringValue, _
		        rsParticipants.Field("Total_Time").StringValue,false,rsParticipants.Field("Laps_Completed").StringValue)+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Racer_Number").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("First_Name").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Participant_Name").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Gender").StringValue+Delimiter
		        OutputData=OutputData +left(rsParticipants.Field("Birth_Date").StringValue,10)+Delimiter
		        OutputData=OutputData +left(rsParticipants.Field("Birth_Date").StringValue,4)+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Division_Name").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Representing").StringValue+Delimiter
		        OutputData=OutputData +rsParticipants.Field("Country").StringValue+Delimiter
		        
		        if rsParticipants.Field("NGB2_License_Number").StringValue <>"" then
		          select case rsParticipants.Field("NGB2_Points").StringValue
		          case ""
		            OutputData=OutputData +""+Delimiter
		          case "inact"
		            OutputData=OutputData +"E"+Delimiter
		          case "active"
		            OutputData=OutputData +"A"+Delimiter
		          else
		            OutputData=OutputData +"A"+Delimiter
		          end select
		        else
		          OutputData=OutputData +""+Delimiter
		        end if
		        
		        OutputData=OutputData +rsParticipants.Field("NGB2_License_Number").StringValue+Delimiter
		        if rsParticipants.Field("NGB_License_Number").StringValue<>"" then
		          if asc(left(rsParticipants.Field("NGB_License_Number").StringValue,1))>58 then
		            OutputData=OutputData + right(rsParticipants.Field("NGB_License_Number").StringValue,7)+Delimiter
		          else
		            OutputData=OutputData + rsParticipants.Field("NGB_License_Number").StringValue+Delimiter
		          end if
		        else
		          OutputData=OutputData +""+Delimiter
		        end if
		        
		        OutputData=OutputData +left(rsParticipants.Field("NGB2_Points").StringValue,9)+Delimiter
		        OutputData=OutputData +rsParticipants.Field("NGB1_Points").StringValue+Delimiter
		        
		        if rsParticipants.Field("DQ").StringValue="Y" then
		          OutputData=OutputData +"DQ"+Delimiter
		          OutputData=OutputData +""+Delimiter 'we'll let the template calculate the race points
		          OutputData=OutputData +rsParticipants.Field("Adjustment_Reason").StringValue
		        elseif rsParticipants.Field("DNS").StringValue="Y" then
		          OutputData=OutputData +"DNS"+Delimiter
		          OutputData=OutputData +""+Delimiter 'we'll let the template calculate the race points
		          OutputData=OutputData +""
		        elseif rsParticipants.Field("DNF").StringValue="Y" then
		          OutputData=OutputData +"DNF"+Delimiter
		          OutputData=OutputData +""+Delimiter 'we'll let the template calculate the race points
		          OutputData=OutputData +""
		        else
		          OutputData=OutputData +rsParticipants.Field("Total_Time").StringValue+Delimiter
		          OutputData=OutputData +""+Delimiter 'we'll let the template calculate the race points
		          OutputData=OutputData +""
		        end if
		        
		        fileStream.WriteLine OutputData
		        rsParticipants.MoveNext
		      next
		      fileStream.Close
		      Progress.Close
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ExportTeams()
		  Dim file As FolderItem
		  Dim fileStream As TextOutputStream
		  Dim Delimiter, FileName, OutputData, SelectStatement as string
		  Dim rsTeam as RecordSet
		  Dim i, ii as Integer
		  
		  if TargetMacOS then
		    Delimiter=chr(9)
		    FileName="Team Export.txt"
		  else
		    Delimiter=","
		    FileName="Team Export.csv"
		  end if
		  
		  file= GetSaveFolderItem("application/text",FileName)
		  if file<>nil then
		    
		    SelectStatement="SELECT ccTeams.Team_Name, ccTeams.Gender, Division_Name FROM ccTeams INNER JOIN divisions ON ccTeams.DivisionID = divisions.rowid"
		    rsTeam=app.theDB.DBSQLSelect(SelectStatement)
		    
		    if rsTeam.RecordCount> 0 then
		      Progress.Show
		      Progress.Initialize("Progress","Exporting Team Data...",rsTeam.RecordCount,-1)
		      fileStream=file.CreateTextFile
		      
		      OutputData= "Team Name"+Delimiter
		      OutputData=OutputData + "Gender"+Delimiter
		      OutputData=OutputData + "Division"+Delimiter
		      fileStream.WriteLine OutputData
		      
		      for i=1 to rsTeam.RecordCount
		        Progress.UpdateProg1(i)
		        OutputData=""
		        for ii=1 to rsTeam.FieldCount
		          OutputData=OutputData+rsTeam.IdxField(ii).StringValue+Delimiter
		        next
		        OutputData=Left(OutputData,len(OutputData)-1) 'remove that last Delimiter
		        OutputData=OutputData
		        fileStream.WriteLine OutputData
		        rsTeam.MoveNext
		      next
		      fileStream.Close
		      Progress.Close
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ExportTimes()
		  Dim file As FolderItem
		  Dim fileStream As TextOutputStream
		  Dim Delimiter, FileName, OutputData, SelectStatement as string
		  Dim rsTimes as RecordSet
		  Dim i, ii as Integer
		  
		  
		  if TargetMacOS then
		    Delimiter=chr(9)
		    FileName="Times Export.txt"
		  else
		    Delimiter=","
		    FileName="Times Export.csv"
		  end if
		  
		  
		  file= GetSaveFolderItem("application/text",FileName)
		  if file<>nil then
		    
		    SelectStatement="SELECT participants.Racer_Number, participants.First_Name, participants.Participant_Name, participants.Representing, times.TX_Code, times.Use_This_Passing, times.Type, "
		    SelectStatement=SelectStatement+"times.Interval_Number, times.Timing_Point_No, "
		    SelectStatement=SelectStatement+"times.Actual_Time, times.Interval_Time, times.Total_Time "
		    SelectStatement=SelectStatement+"FROM times LEFT OUTER JOIN participants ON times.ParticipantID = participants.rowid "
		    SelectStatement=SelectStatement+"WHERE times.RaceID = 1 ORDER BY Actual_Time"
		    rsTimes=app.theDB.DBSQLSelect(SelectStatement)
		    
		    if rsTimes.RecordCount> 0 then
		      Progress.Show
		      Progress.Initialize("Progress","Exporting Times Data...",rsTimes.RecordCount,-1)
		      fileStream=file.CreateTextFile
		      
		      OutputData= "Racer Number"+Delimiter
		      OutputData=OutputData + "First Name"+Delimiter
		      OutputData=OutputData + "Last/Team Name"+Delimiter
		      OutputData=OutputData + "Representing"+Delimiter
		      OutputData=OutputData + "TX Code"+Delimiter
		      OutputData=OutputData + "Use This Passing"+Delimiter
		      OutputData=OutputData + "Type"+Delimiter
		      OutputData=OutputData + "Interval Number"+Delimiter
		      OutputData=OutputData + "Timing Point Number"+Delimiter
		      OutputData=OutputData + "Actual Time"+Delimiter
		      OutputData=OutputData + "Interval Time"+Delimiter
		      OutputData=OutputData + "Total Time"+Delimiter
		      fileStream.WriteLine OutputData
		      
		      for i=1 to rsTimes.RecordCount
		        Progress.UpdateProg1(i)
		        OutputData=""
		        for ii=1 to rsTimes.FieldCount
		          OutputData=OutputData+rsTimes.IdxField(ii).StringValue+Delimiter
		        next
		        OutputData=Left(OutputData,len(OutputData)-1) 'remove that last Delimiter
		        OutputData=OutputData
		        fileStream.WriteLine OutputData
		        rsTimes.MoveNext
		      next
		      fileStream.Close
		      Progress.Close
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ExportTransponders()
		  Dim file As FolderItem
		  Dim fileStream As TextOutputStream
		  Dim Delimiter, FileName, OutputData, SelectStatement as string
		  Dim rsTX as RecordSet
		  Dim i, ii as Integer
		  
		  if TargetMacOS then
		    Delimiter=chr(9)
		    FileName="TX Export.txt"
		  else
		    Delimiter=","
		    FileName="TX Export.csv"
		  end if
		  
		  file= GetSaveFolderItem("application/text",FileName)
		  if file<>nil then
		    
		    SelectStatement="SELECT Racer_Number, TX_Code, Issued, TeamMemberID, Hit_Count, Returned FROM transponders WHERE RaceID =1"
		    rsTX=app.theDB.DBSQLSelect(SelectStatement)
		    
		    if rsTX.RecordCount> 0 then
		      Progress.Show
		      Progress.Initialize("Progress","Exporting Transponder Data...",rsTX.RecordCount,-1)
		      fileStream=file.CreateTextFile
		      
		      OutputData= "Racer Number"+Delimiter
		      OutputData=OutputData + "TX Code"+Delimiter
		      OutputData=OutputData + "Issued"+Delimiter
		      OutputData=OutputData + "TeamMemberID"+Delimiter
		      OutputData=OutputData + "Hit Count"+Delimiter
		      OutputData=OutputData + "Returned"+Delimiter
		      fileStream.WriteLine OutputData
		      
		      for i=1 to rsTX.RecordCount
		        Progress.UpdateProg1(i)
		        OutputData=""
		        for ii=1 to rsTX.FieldCount
		          OutputData=OutputData+rsTX.IdxField(ii).StringValue+Delimiter
		        next
		        OutputData=Left(OutputData,len(OutputData)-1) 'remove that last Delimiter
		        OutputData=OutputData
		        fileStream.WriteLine OutputData
		        rsTX.MoveNext
		      next
		      fileStream.Close
		      Progress.Close
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function FormatLen(number As Integer) As String
		  Dim formattedValue As String
		  
		  formattedValue = format(number, "###############")
		  return formattedValue
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetInternetType(f As FolderItem) As String
		  Dim t As String
		  Dim rtException As RuntimeException
		  
		  if f = nil then
		    rtException = new RuntimeException
		    rtException.Message = "Attempt to get Internet Type of nil FolderItem."
		    raise rtException
		  end if
		  
		  t = f.type ' get file type from FolderItem
		  // we have more than one text type, so we must
		  // check for a special case
		  if t = "text" then t = "text/plain"
		  
		  return t
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub HideTimeTrialUI()
		  lblTTStartInterval.Visible=False
		  cbTTStartInterval.Visible=False
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub HighlightQueryValue()
		  select case RaceTab.value
		    
		  case 1
		    ef_query_Divisions.SelStart=0
		    ef_query_Divisions.SelLength=99
		    ef_query_Divisions.SetFocus
		    
		  case 2
		    ef_query_Teams.SelStart=0
		    ef_query_Teams.SelLength=99
		    ef_query_Teams.SetFocus
		    
		  case 3
		    ef_query_Participants.SelStart=0
		    ef_query_Participants.SelLength=99
		    ef_query_Participants.SetFocus
		    
		  case 4
		    ef_query_times.SelStart=0
		    ef_query_times.SelLength=99
		    ef_query_times.SetFocus
		    
		  case 5
		    ef_query_Transponders.SelStart=0
		    ef_query_Transponders.SelLength=99
		    ef_query_Transponders.SetFocus
		    
		  end Select
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ImportAdjustmentData()
		  Dim f As FolderItem
		  dim ImportStream as TextInputStream
		  Dim IncomingLine(), HeaderList(), FieldList() as string
		  Dim IncomingData, SQLStatement, DistanceIDStg, DivisionIDStg, NetTimeStg, TotalTimeStg, TotalAdjustedTimeStg as String
		  Dim TotalAdjustedTime as Double
		  Dim i, FieldCountRemainder, LineNumber as Integer
		  Dim RacerNumberIdx, ReasonIdx, AdjustmentIdx as Integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  Dim CurrentDate As Date
		  Dim rsParticipant as RecordSet
		  
		  CurrentDate = new Date
		  
		  if ImportFIlePath<>"" then
		    f=GetFolderItem(ImportFIlePath)
		    If f<>Nil then
		      ImportStream=f.OpenAsTextFile
		      IncomingData=ReplaceAll(ImportStream.ReadAll,chr(34),"")
		      IncomingData=ReplaceAll(IncomingData,chr(10),chr(13)) 'replace linefeeds with cr
		      IncomingData=ReplaceAll(IncomingData,chr(13)+chr(13),chr(13)) 'replace double cr with single cr
		      IncomingLine=IncomingData.Split(chr(13))
		      ImportStream.Close
		      'Check to see if the header line is there and the minimum required fields are there
		      IncomingLine(0)=Uppercase(IncomingLine(0))
		      HeaderList=IncomingLine(0).Split(Seperator)
		      for i=0 to UBound(HeaderList)
		        HeaderList(i)=Trim(HeaderList(i))
		      next
		      
		      RacerNumberIdx=HeaderList.IndexOf("RACER NUMBER")
		      ReasonIdx=HeaderList.IndexOf("REASON")
		      AdjustmentIdx=headerlist.IndexOf("ADJUSTMENT")
		      
		      If (RacerNumberIdx>=0 and ReasonIdx>=0 and  AdjustmentIdx>=0) then
		        Progress.Show
		        Progress.Initialize("Progress","Loading Adjustment Import File...",0,-1)
		        'Loop through the file and import the data
		        'read the data
		        
		        Progress.Initialize("Progress","Adding Adjustment Records...",UBound(IncomingLine),-1)
		        For LineNumber = 1 to UBound(IncomingLine)
		          Progress.UpdateProg1(LineNumber)
		          FieldList=IncomingLine(LineNumber).Split(Seperator)
		          if FieldList(RacerNumberIdx)<>"" and FieldList(AdjustmentIdx)<>"" and FieldList(ReasonIdx)<>"" then
		            
		            SQLStatement="SELECT Assigned_Start, Actual_Start, Actual_Stop, Net_Time, Total_Adjustment, Adjustment_Reason FROM Participants WHERE Racer_Number="+FieldList(RacerNumberIdx)
		            rsParticipant=app.theDB.DBSQLSelect(SQLStatement)
		            
		            TotalAdjustedTime=app.ConvertTimeToSeconds(FieldList(AdjustmentIdx))+app.ConvertTimeToSeconds(rsParticipant.Field("Total_Adjustment").StringValue)
		            if TotalAdjustedTime>0 then
		              TotalAdjustedTimeStg="+"+app.ConvertSecondsToTime(TotalAdjustedTime,false)
		            else
		              TotalAdjustedTimeStg="-"+app.ConvertSecondsToTime(TotalAdjustedTime,false)
		            end if
		            
		            TotalTimeStg=app.CalculateTotalTime(rsParticipant.Field("Assigned_Start").StringValue,rsParticipant.Field("Actual_Start").StringValue, _
		            rsParticipant.Field("Actual_Stop").StringValue,TotalAdjustedTimeStg,NetTimeStg)
		            
		            SQLStatement="UPDATE participants SET Total_Adjustment = '"+TotalAdjustedTimeStg+"', Adjustment_Reason = '"+rsParticipant.Field("Actual_Stop").StringValue+chr(13)+FieldList(ReasonIdx)+"', "
		            SQLStatement=SQLStatement+"Total_Time='"+TotalTimeStg+"', Net_Time='"+NetTimeStg+"' "
		            SQLStatement=SQLStatement+"WHERE Racer_Number="+FieldList(RacerNumberIdx)
		            App.theDB.DBSQLExecute(SQLStatement)
		            rsParticipant.Close
		            
		          end if
		        Next
		        'end of file
		        d.Icon=0 'note icon
		        app.theDB.DBCommit
		        Progress.Close
		        d.Message="Import Complete."
		        b=d.ShowModal
		        
		      else
		        'dialog tell them missing data
		        d.Icon=2 'stop icon
		        d.Message="The file does not appear to be in the proper format."
		        d.Explanation="The file needs to be tab or comma seperated columns and containing Racer Number,  Adjustment and Reason."
		        b=d.ShowModal
		        
		      end if
		      
		    End if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ImportChipXTimeData()
		  Dim f As FolderItem
		  dim ImportStream as TextInputStream
		  Dim IncomingLine(), FieldList() as string
		  Dim IncomingData, Time as String
		  Dim i, TXCodeIDX, TODIDX, DateIDX, LineNumber as Integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  dim rs As RecordSet
		  dim DateTime as new date
		  dim TimeINSeconds as Double
		  
		  dim recCount as integer
		  dim res as boolean
		  
		  TXCodeIDX=1
		  DateIDX=3
		  TODIdx=4
		  Seperator=chr(9)
		  
		  if ImportFIlePath<>"" then
		    f=GetFolderItem(ImportFIlePath)
		    If f<>Nil then
		      Progress.Show
		      Progress.Initialize("Progress","Loading ChipX® Time Import File...",0,-1)
		      ImportStream=f.OpenAsTextFile
		      IncomingData=ReplaceAll(ImportStream.ReadAll,chr(1),"")
		      IncomingData=Uppercase(IncomingData)
		      IncomingLine=IncomingData.Split(chr(10))
		      ImportStream.Close
		      'Check to see if the header line is there and the minimum required fields are there
		      FieldList=IncomingLine(0).Split(Seperator)
		      If (FieldList(0)="0" and UBound(FieldList)>=6) then
		        
		        'Loop through the file and import the data
		        'read the data
		        
		        // Loop over a string, 5 characters at a time.
		        Progress.Initialize("Progress","Adding Time Records...",UBound(IncomingLine),-1)
		        For LineNumber = 0 to UBound(IncomingLine)
		          Progress.UpdateProg1(LineNumber)
		          
		          FieldList=IncomingLine(LineNumber).Split(Seperator)
		          if UBound(FieldList)>=6 then
		            
		            Time=FieldList(DateIDX)+" "+FieldList(TODIDX)
		            TimeINSeconds=app.ConvertTimeToSeconds(FieldList(DateIDX)+" "+FieldList(TODIDX))
		            if LocateImportFolder.pmOperation.Text="Add" then
		              TimeINSeconds=TimeINSeconds+app.UTCOffset-app.AMBOffset+app.ConvertTimeToSeconds(LocateImportFolder.tefAdjustmentTime.Text)
		            else
		              TimeINSeconds=TimeINSeconds+app.UTCOffset-app.AMBOffset-app.ConvertTimeToSeconds(LocateImportFolder.tefAdjustmentTime.Text)
		            end if
		            Time=app.ConvertSecondsToTime(TimeINSeconds,true)
		            
		            App.TimeSource.Append TimeSource
		            App.TimeType.Append "Both"
		            App.TimeTXCode.Append FieldList(TXCodeIDX)
		            App.TimeTime.Append Time
		            App.TimeTimingPointID.Append TimingPointID
		          end if
		          
		        Next
		        'end of file
		        
		      else
		        'dialog tell them missing data
		        
		        
		        d.Icon=2 'stop icon
		        d.Message="The file does not appear to be in the proper format."
		        d.Explanation="The file needs to be file generated by the ChipX® decoder."
		        b=d.ShowModal
		        
		      end if
		      
		      Progress.Close
		    End if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ImportDivisionData()
		  Dim f As FolderItem
		  dim ValidDate as Boolean
		  dim ImportStream as TextInputStream
		  Dim IncomingLine(), HeaderList(), FieldList(), ST() as string
		  Dim IncomingData, SQLStatement, DistanceIDStg, DivisionIDStg as String
		  Dim FieldCount, FieldCountRemainder, i, Intervals, LineNumber as Integer
		  Dim NameIdx, LowAgeIdx, HighAgeIdx, GenderIdx as Integer
		  Dim StartDateIdx, StartTimeIdx, OrderIdx, ImportNameIdx, RaceDistIdx as Integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  Dim StartDate as date
		  
		  dim recCount as integer
		  dim rsRaceDistances as recordSet
		  dim res as boolean
		  
		  NameIdx=-1
		  LowAgeIdx=-1
		  HighAgeIdx=-1
		  GenderIdx=-1
		  StartTimeIdx=-1
		  OrderIdx=-1
		  ImportNameIdx=-1
		  RaceDistIdx=-1
		  
		  if ImportFIlePath<>"" then
		    f=GetFolderItem(ImportFIlePath)
		    If f<>Nil then
		      ImportStream=f.OpenAsTextFile
		      IncomingData=ReplaceAll(ImportStream.ReadAll,chr(34),"") 'replace double quotes
		      IncomingData=ReplaceAll(IncomingData,chr(10),chr(13)) 'replace linefeeds with cr
		      IncomingData=ReplaceAll(IncomingData,chr(13)+chr(13),chr(13)) 'replace double cr with single cr
		      IncomingLine=IncomingData.Split(chr(13))
		      ImportStream.Close
		      'Check to see if the header line is there and the minimum required fields are there
		      IncomingLine(0)=Uppercase(IncomingLine(0))
		      HeaderList=IncomingLine(0).Split(Seperator)
		      for i=0 to UBound(HeaderList)
		        HeaderList(i)=Trim(HeaderList(i))
		      next
		      
		      NameIdx=HeaderList.IndexOf("DIVISION NAME")
		      LowAgeIdx=HeaderList.IndexOf("LOW AGE")
		      HighAgeIdx=headerlist.IndexOf("HIGH AGE")
		      GenderIdx=HeaderList.IndexOf("GENDER")
		      StartDateIdx=HeaderList.IndexOf("START DATE")
		      StartTimeIdx=HeaderList.IndexOf("START TIME")
		      OrderIdx=HeaderList.IndexOf("ORDER")
		      ImportNameIdx=HeaderList.IndexOf("CATEGORY")
		      RaceDistIdx=HeaderList.IndexOf("RACE DISTANCE")
		      
		      If (NameIdx>=0 and LowAgeIdx>=0 and HighAgeIdx>=0 and GenderIdx>=0 and RaceDistIdx>=0 ) then
		        Progress.Show
		        Progress.Initialize("Progress","Loading Division Import File...",0,-1)
		        
		        FieldCount=4
		        
		        if StartTimeIdx>0 then
		          FieldCount=FieldCount+1
		        end if
		        
		        if ImportNameIdx>0 then
		          FieldCount=FieldCount+1
		        end if
		        
		        if StartTimeIdx>0 then
		          FieldCount=FieldCount+1
		        end if
		        
		        if RaceDistIdx>0 then
		          FieldCount=FieldCount+1
		        end if
		        
		        'Loop through the file and import the data
		        'read the data
		        
		        // Loop over a string, 5 characters at a time.
		        Progress.Initialize("Progress","Adding Division Records...",UBound(IncomingLine),-1)
		        For LineNumber = 1 to UBound(IncomingLine)
		          Progress.UpdateProg1(LineNumber)
		          if IncomingLine(LineNumber)<>"" then
		            FieldList=IncomingLine(LineNumber).Split(Seperator)
		            if UBound(FieldList)>0 then
		              'Find which Race Distance
		              if RaceDistIdx>0 then
		                SQLStatement="Select rowid FROM racedistances WHERE RaceDistance_Name='"+FieldList(RaceDistIdx)+"'"
		                rsRaceDistances=app.theDB.DBSQLSelect(SQLStatement)
		                if not rsRaceDistances.EOF then
		                  DistanceIDStg=rsRaceDistances.Field("rowid").StringValue
		                else
		                  SQLStatement="Select rowid FROM racedistances"
		                  rsRaceDistances=app.theDB.DBSQLSelect(SQLStatement)
		                  if not rsRaceDistances.EOF then
		                    DistanceIDStg=rsRaceDistances.Field("rowid").StringValue
		                  else
		                    DistanceIDStg="0"
		                  end if
		                end if
		                rsRaceDistances.Close
		              else
		                DistanceIDStg="0"
		              end if
		              
		              SQLStatement="INSERT INTO divisions (RaceID, Division_Name, RaceDistanceID, Import_Name, Low_Age, High_Age, "
		              SQLStatement=SQLStatement+"Gender, Start_Time, Actual_Start_Time, List_Order) "
		              SQLStatement=SQLStatement+" VALUES ("
		              SQLStatement=SQLStatement+"1," 'RaceID
		              SQLStatement=SQLStatement+"'"+FieldList(NameIdx)+"'," 'Division_Name
		              SQLStatement=SQLStatement+distanceIDStg+"," 'RaceDistanceID
		              
		              if ImportNameIdx>=0 then 'Import_Name
		                SQLStatement=SQLStatement+"'"+FieldList(ImportNameIdx)+"',"
		              else
		                SQLStatement=SQLStatement+"'',"
		              end if
		              
		              SQLStatement=SQLStatement+"'"+FieldList(LowAgeIdx)+"'," 'Low_Age
		              SQLStatement=SQLStatement+"'"+FieldList(HighAgeIdx)+"'," 'High_Age
		              SQLStatement=SQLStatement+"'"+FieldList(GenderIdx)+"'," 'Gender
		              
		              if StartDateIdx>=0 and StartDateIdx<=UBound(FieldList) then 'Start Date
		                if FieldList(StartDateIdx)<>"" then
		                  StartDate=new date
		                  ValidDate=ParseDate(FieldList(StartDateIdx),StartDate)
		                  if Not(ValidDate) then
		                    StartDate=app.RaceDate
		                  end if
		                else
		                  StartDate=app.RaceDate
		                end if
		              else
		                StartDate=app.RaceDate
		              end if
		              
		              if StartTimeIdx>=0 and StartTimeIdx<=UBound(FieldList) then 'Start Time
		                if FieldList(StartTimeIdx)<>"" then
		                  ST=FieldList(StartTimeIdx).Split(":")
		                  StartDate.Hour=Val(ST(0))
		                  StartDate.Minute=Val(ST(1))
		                  if UBound(ST)=2 then
		                    StartDate.Second=Val(ST(2))
		                  end if
		                  SQLStatement=SQLStatement+"'"+StartDate.SQLDateTime+".000',"
		                else
		                  SQLStatement=SQLStatement+"'"+app.RaceStartTime+"',"
		                end if
		              else
		                SQLStatement=SQLStatement+"'"+app.RaceStartTime+"',"
		              end if
		              
		              SQLStatement=SQLStatement+"'0000-00-00 00:00:00.000'," 'Actual_Start_Time
		              
		              if OrderIdx>=0 and OrderIdx<=UBound(FieldList) then 'List_Order
		                SQLStatement=SQLStatement+"'"+FieldList(OrderIdx)+"'"
		              else
		                SQLStatement=SQLStatement+"''"
		              end if
		              SQLStatement = SQLStatement + ")"
		              
		              ' insert the record
		              App.theDB.DBSQLExecute(SQLStatement)
		              DivisionIDStg=format(App.theDB.DBLastRowID,"##########")
		              
		              
		              FieldCountRemainder=(UBound(FieldList)+1)-FieldCount
		              
		              if ((FieldCountRemainder mod 4)=0) then
		                for i=FieldCount to UBound(FieldList) Step 4
		                  if FieldList(i) <> "" then
		                    SQLStatement="INSERT INTO intervals (RaceID, DivisionID, Number, Interval_Name, Actual_Distance, Pace_Type) "
		                    SQLStatement=SQLStatement+"VALUES ("
		                    SQLStatement=SQLStatement+"1," 'RaceID
		                    SQLStatement=SQLStatement+DivisionIDStg+"," 'Division_Name
		                    SQLStatement=SQLStatement+FieldList(i)+"," 'Number
		                    SQLStatement=SQLStatement+"'"+FieldList(i+1)+"'," 'Interval_Name
		                    SQLStatement=SQLStatement+"'"+FieldList(i+2)+"'," 'Actual_Distance
		                    SQLStatement=SQLStatement+"'"+FieldList(i+3)+"')" 'Pace_Type
		                    App.theDB.DBSQLExecute(SQLStatement)
		                  end if
		                next
		              end if
		            end if
		          end if
		        Next
		        'end of file
		        app.theDB.DBCommit
		        d.Icon=0 'note icon
		        Progress.Close
		        d.Message="Import Complete."
		        b=d.ShowModal
		        ExtraQuery_Divisions=""
		        ExecuteQuery("Divisions",ExtraQuery_Divisions,Division_Sort)
		        
		      else
		        'dialog tell them missing data
		        d.Icon=2 'stop icon
		        d.Message="The file does not appear to be in the proper format."
		        d.Explanation="The file needs to be tab or comma seperated columns and containing Division Name, Low Age, High Age, Gender and Race Distance."
		        b=d.ShowModal
		        
		      end if
		      
		    End if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ImportiPicoTimeData()
		  Dim f As FolderItem
		  dim ImportStream as TextInputStream
		  Dim IncomingLine()as string
		  Dim IncomingData, Time as String
		  Dim i, LineNumber as Integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  dim rs As RecordSet
		  
		  dim recCount as integer
		  dim res as boolean
		  dim HexTable(257) as string
		  dim HexValue as string
		  
		  for i = 1 to 255
		    HexValue=Hex(i)
		    if len(HexValue)=1 then
		      HEXTable(i)="0"+HexValue
		    else
		      HEXTable(i)=HexValue
		    end if
		  next
		  
		  
		  if ImportFIlePath<>"" then
		    f=GetFolderItem(ImportFIlePath)
		    If f<>Nil then
		      Progress.Show
		      Progress.Initialize("Progress","Loading Timer Data...",0,-1)
		      ImportStream=f.OpenAsTextFile
		      IncomingData=ReplaceAll(ImportStream.ReadAll,chr(34),"")
		      IncomingData=ReplaceAll(IncomingData,chr(13),"")
		      IncomingLine=IncomingData.Split(chr(10))
		      ImportStream.Close
		      'Check to see if the header line is there and the minimum required fields are there
		      If (left(IncomingLine(0),8)="$TUNERPT") or (left(IncomingLine(0),2)="aa") or  (left(IncomingLine(0),2)="ab") then
		        
		        'Loop through the file and import the data
		        'read the data
		        
		        // Loop over a string, 5 characters at a time.
		        Progress.Initialize("Progress","Adding Time Records...",UBound(IncomingLine),-1)
		        For LineNumber = 0 to UBound(IncomingLine)
		          Progress.UpdateProg1(LineNumber)
		          
		          if (left(IncomingLine(LineNumber),2)="aa") then
		            
		            if (((TimeSource=1) and (right(IncomingLine(LineNumber),2)="LS")) or ((TimeSource<>1) and (right(IncomingLine(LineNumber),2)="FS"))) then
		              
		              Time="20"+mid(IncomingLine(LineNumber),21,2)+"-"+mid(IncomingLine(LineNumber),23,2)+"-"+mid(IncomingLine(LineNumber),25,2)+" "+mid(IncomingLine(LineNumber),27,2)+":"+mid(IncomingLine(LineNumber), 29,2)+":"+mid(IncomingLine(LineNumber), 31,2)+"."+format(HEXTable.IndexOf(mid(IncomingLine(LineNumber),31,2),1),"00")+"0"
		              
		              'if app.PlacesToTruncate=0 then
		              'Time=Left(Time,len(Time)-4)
		              'Time=Time+".000"
		              'else
		              'Time=Left(Time,len(Time)-(3-app.PlacesToTruncate))
		              'for i = 1 to 3-app.PlacesToTruncate
		              'Time=Time+"0"
		              'next
		              'end if
		              
		              App.TimeSource.Append TimeSource
		              App.TimeType.Append "Both"
		              App.TimeTXCode.Append mid(IncomingLine(LineNumber),5,12)
		              App.TimeTime.Append Time
		              App.TimeTimingPointID.Append TimingPointID
		            end if
		          end if
		          
		        Next
		        'end of file
		        
		      else
		        'dialog tell them missing data
		        d.Icon=2 'stop icon
		        d.Message="The file does not appear to be in the proper format."
		        d.Explanation="The file needs to be file generated by the iPico® decoder. Or the first line of the file is blank and it shouldn't be blank."
		        b=d.ShowModal
		        
		      end if
		      
		      Progress.Close
		    End if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ImportMillisecondsProTimeData()
		  Dim f As FolderItem
		  dim ImportStream as TextInputStream
		  Dim IncomingLine(), FieldList(), HeaderList() as string
		  Dim IncomingData, Time as String
		  Dim i, TXCodeIDX, ActualTimeIdx, LineNumber as Integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  dim rs As RecordSet
		  dim DateTime as new date
		  dim TimeINSeconds as Double
		  
		  dim recCount as integer
		  dim res as boolean
		  
		  TXCodeIDX=-1
		  ActualTimeIdx=-1
		  
		  if ImportFIlePath<>"" then
		    f=GetFolderItem(ImportFIlePath)
		    If f<>Nil then
		      Progress.Show
		      Progress.Initialize("Progress","Loading Milliseconds Pro® Time Import File...",0,-1)
		      ImportStream=f.OpenAsTextFile
		      IncomingData=ReplaceAll(ImportStream.ReadAll,chr(34),"")
		      IncomingData=Uppercase(IncomingData)
		      IncomingLine=IncomingData.Split(chr(13))
		      ImportStream.Close
		      'Check to see if the header line is there and the minimum required fields are there
		      IncomingLine(0)=Uppercase(IncomingLine(0))
		      HeaderList=IncomingLine(0).Split(Seperator)
		      
		      for i=0 to UBound(HeaderList)
		        HeaderList(i)=Trim(HeaderList(i))
		      next
		      TXCodeIdx=HeaderList.IndexOf("TX CODE")
		      ActualTimeIdx=headerlist.IndexOf("ACTUAL TIME")
		      
		      If (TXCodeIdx>=0 and ActualTimeIdx>=0) then
		        
		        'Loop through the file and import the data
		        'read the data
		        
		        Progress.Initialize("Progress","Adding Time Records...",UBound(IncomingLine),-1)
		        For LineNumber = 1 to UBound(IncomingLine)
		          Progress.UpdateProg1(LineNumber)
		          
		          FieldList=IncomingLine(LineNumber).Split(Seperator)
		          if UBound(FieldList)>=6 then
		            
		            Time=FieldList(ActualTimeIdx)
		            
		            'if app.PlacesToTruncate=0 then
		            'Time=Left(Time,len(Time)-4)
		            'Time=Time+".000"
		            'else
		            'Time=Left(Time,len(Time)-(3-app.PlacesToTruncate))
		            'for i = 1 to 3-app.PlacesToTruncate
		            'Time=Time+"0"
		            'next
		            'end if
		            
		            App.TimeSource.Append TimeSource
		            App.TimeType.Append "Both"
		            App.TimeTXCode.Append FieldList(TXCodeIDX)
		            App.TimeTime.Append Time
		            App.TimeTimingPointID.Append TimingPointID
		          end if
		          
		        Next
		        'end of file
		        
		      else
		        'dialog tell them missing data
		        
		        
		        d.Icon=2 'stop icon
		        d.Message="The file does not appear to be in the proper format."
		        d.Explanation="The file needs to be file generated by Milliseconds Pro."
		        b=d.ShowModal
		        
		      end if
		      
		      Progress.Close
		    End if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ImportParticipantData()
		  Dim f As FolderItem
		  Dim ValidDate as Boolean
		  dim ImportStream as TextInputStream
		  Dim IncomingLine(), HeaderList(), FieldList(), BD(), ST() as string
		  Dim IncomingData, SQLStatement, Representing, SeedGroup, StartTime, DivisionIDStg, DistanceIDStg, NGBLicense, NGB2License, NGBPoints, NGB2points, TeamMember, TeamID as String
		  Dim i, Age, LineNumber, Duplicates, Added, ParticipantID, PeriodPos as Integer
		  Dim BibNumberIdx, FirstNameIdx, LastNameIdx, GenderIdx, BirthDateIdx, AgeIdx, CategoryIdx, SeedGroupIdx, StartTimeIdx, NGBLicIdx, NGB2LicIdx,  NGBPointsIdx, NGB2PointsIdx, TeamMemberIdx as Integer
		  Dim StartDateIdx, StreetIdx, CityIdx, StateIdx, PostalCodeIdx, CountryIdx, EMailIdx, PhoneIdx, SMSAddressIdx, DeclaredIdx, WaveIdx, NoteIdx as Integer
		  Dim Street, City, State, PostalCode, Country, EMail, Phone, SMSAddress, Wave, Note as String
		  Dim RepresentingIdx as Integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  Dim BirthDate, CurrentDate, StartDate as date
		  Dim rsTeam as RecordSet
		  
		  dim recCount as integer
		  dim rsDivisions, rsParticipants as recordSet
		  dim res as boolean
		  
		  CurrentDate=new date
		  
		  Duplicates=0
		  Added=0
		  
		  if ImportFIlePath<>"" then
		    f=GetFolderItem(ImportFIlePath)
		    If f<>Nil then
		      ImportStream=f.OpenAsTextFile
		      IncomingData=ReplaceAll(ImportStream.ReadAll,chr(34),"") 'replace double quotes
		      IncomingData=ReplaceAll(IncomingData,"'","") 'replace single quotes
		      IncomingData=ReplaceAll(IncomingData,chr(10),chr(13)) 'replace linefeeds with cr
		      IncomingData=ReplaceAll(IncomingData,chr(13)+chr(13),chr(13)) 'replace double cr with single cr
		      IncomingLine=IncomingData.Split(chr(13))
		      ImportStream.Close
		      'Check to see if the header line is there and the minimum required fields are there
		      IncomingLine(0)=Uppercase(IncomingLine(0))
		      HeaderList=IncomingLine(0).Split(Seperator)
		      for i=0 to UBound(HeaderList)
		        HeaderList(i)=Trim(HeaderList(i))
		      next
		      
		      BibNumberIdx=HeaderList.IndexOf("RACER NUMBER")
		      FirstNameIdx=HeaderList.IndexOf("FIRST NAME")
		      LastNameIdx=headerlist.IndexOf("NAME")
		      StreetIdx=headerlist.IndexOf("STREET")
		      CityIdx=headerlist.IndexOf("CITY")
		      StateIdx=headerlist.IndexOf("STATE")
		      PostalCodeIdx=headerlist.IndexOf("POSTAL CODE")
		      CountryIdx=headerlist.IndexOf("COUNTRY")
		      EMailIdx=headerlist.IndexOf("EMAIL")
		      PhoneIdx=headerlist.IndexOf("PHONE")
		      GenderIdx=HeaderList.IndexOf("GENDER")
		      BirthDateIdx=HeaderList.IndexOf("BIRTH DATE")
		      AgeIdx=HeaderList.IndexOf("AGE")
		      CategoryIdx=HeaderList.IndexOf("CATEGORY")
		      if CategoryIdx<0 then
		        CategoryIdx=HeaderList.IndexOf("DIVISION")
		      end if
		      RepresentingIdx=HeaderList.IndexOf("REPRESENTING")
		      SeedGroupIdx=HeaderList.IndexOf("SEED GROUP")
		      StartDateIdx=HeaderList.IndexOf("START DATE")
		      StartTimeIdx=HeaderList.IndexOf("START TIME")
		      NGBLicIdx=HeaderList.IndexOf("NGB LICENSE")
		      NGB2LicIdx=HeaderList.IndexOf("NGB2 LICENSE")
		      NGBPointsIdx=HeaderList.IndexOf("NGB POINTS")
		      NGB2PointsIdx=HeaderList.IndexOf("NGB2 POINTS")
		      TeamMemberIdx=HeaderList.IndexOf("RELAY TEAM")
		      SMSAddressIdx=HeaderList.IndexOf("SMS ADDRESS")
		      DeclaredIdx=HeaderList.IndexOf("DECLARED")
		      WaveIdx=HeaderList.IndexOf("WAVE")
		      NoteIdx=HeaderList.IndexOf("NOTE")
		      
		      If (BibNumberIdx>=0 and FirstNameIdx>=0 and LastNameIdx>=0 and GenderIdx>=0 and (BirthDateIdx>=0 or AgeIdx>=0)) then
		        Progress.Show
		        Progress.Initialize("Progress","Loading Participant Import File...",0,-1)
		        'Loop through the file and import the data
		        'read the data
		        
		        // Loop over a string, 5 characters at a time.
		        Progress.Initialize("Progress","Adding Registration Records...",UBound(IncomingLine),-1)
		        For LineNumber = 1 to UBound(IncomingLine)
		          Progress.UpdateProg1(LineNumber)
		          if IncomingLine(LineNumber)<>"" then
		            FieldList=IncomingLine(LineNumber).Split(Seperator)
		            
		            if TeamMemberIdx>=0 and TeamMemberIdx<=UBound(FieldList)then
		              If FieldList(TeamMemberIdx) <> "" then
		                TeamMember=FieldList(TeamMemberIdx)
		              else
		                TeamMember="N"
		              end if
		            else
		              TeamMember="N"
		            end if
		            
		            SQLStatement="SELECT rowid, Representing FROM participants WHERE Racer_Number="+FieldList(BibNumberIdx)
		            rsParticipants=app.theDB.DBSQLSelect(SQLStatement)
		            
		            if rsParticipants.EOF then
		              
		              
		              if  StreetIdx>=0 then
		                Street=Uppercase(FieldList(StreetIdx))
		              else
		                Street=""
		              end if
		              
		              if CityIdx>=0 then
		                City=Uppercase(FieldList(CityIdx))
		              else
		                City=""
		              end if
		              
		              if StateIdx>=0 then
		                State=Uppercase(FieldList(StateIdx))
		              else
		                State=""
		              end if
		              
		              if PostalCodeIdx>=0 then
		                PostalCode=FieldList(PostalCodeIdx)
		              else
		                PostalCode=""
		              end if
		              
		              if CountryIdx>=0 then
		                Country=Uppercase(FieldList(CountryIdx))
		              else
		                Country=""
		              end if
		              
		              if EMailIdx>=0 then
		                EMail=FieldList(EMailIdx)
		              else
		                EMail=""
		              end if
		              
		              if PhoneIdx>=0 then
		                Phone=FieldList(PhoneIdx)
		              else
		                Phone=""
		              end if
		              
		              if ((BirthDateIdx>=0) and FieldList(BirthDateIdx)<>"") then
		                if FieldList(BirthDateIdx)<>"00/00/00" and FieldList(BirthDateIdx)<>"0000-00-00" then
		                  BD=FieldList(BirthDateIdx).Split("/")
		                  BirthDate=new date
		                  if val(BD(2))>1900 then
		                    BirthDate.Year=val(BD(2))
		                  else
		                    BirthDate.year=1900+val(BD(2))
		                  end if
		                  if (CurrentDate.Year-BirthDate.year)>97 then '97 year old or older probably is not participating
		                    BirthDate.year=2000+val(BD(2))
		                  end if
		                  BirthDate.Day=val(BD(1))
		                  BirthDate.Month=val(BD(0))
		                  
		                  Age=app.CalculateAge(BirthDate,app.RaceAgeDate)
		                else
		                  Age=0
		                end if
		              else
		                BirthDate=new date
		                BirthDate.SQLDate="0000-00-00"
		                Age=val(FieldList(AgeIdx))
		              end if
		              
		              'Find which class
		              SQLStatement="Select Start_Time, RaceDistanceID, rowid FROM divisions WHERE RaceID=1"
		              SQLStatement=SQLStatement+" AND Low_Age<="+str(Age)+" AND High_Age>="+str(Age)+" AND Gender='"+FieldList(GenderIdx)+"'"
		              if CategoryIdx>=0 then
		                SQLStatement=SQLStatement+" AND Import_Name LIKE '"+FieldList(CategoryIdx)+"'"
		              end if
		              rsDivisions=app.theDB.DBSQLSelect(SQLStatement)
		              if not rsDivisions.EOF then
		                StartTime=rsDivisions.Field("Start_Time").StringValue
		                DistanceIDStg=rsDivisions.Field("RaceDistanceID").StringValue
		                DivisionIDStg=rsDivisions.Field("rowid").StringValue
		              else
		                StartTime=app.RaceStartTime
		                DistanceIDStg="0"
		                DivisionIDStg="0"
		              end if
		              rsDivisions.Close
		              
		              if StartDateIdx>=0 and StartDateIdx<=UBound(FieldList) then 'Start Date
		                if FieldList(StartDateIdx)<>"" then
		                  StartDate=new date
		                  ValidDate=ParseDate(FieldList(StartDateIdx),StartDate)
		                  if Not(ValidDate) then
		                    StartDate=app.RaceDate
		                  end if
		                  StartTime=StartDate.SQLDateTime+".000"
		                else
		                  StartDate=app.RaceDate
		                end if
		              else
		                StartDate=app.RaceDate
		              end if
		              
		              if StartTimeIdx>=0 and StartTimeIdx<=UBound(FieldList) then 'Start Time
		                if FieldList(StartTimeIdx)<>"" then
		                  PeriodPos=InStr(FieldList(StartTimeIdx),".")
		                  if PeriodPos>0 then
		                    FieldList(StartTimeIdx)=Left(FieldList(StartTimeIdx),PeriodPos-1)
		                  end if
		                  ST=FieldList(StartTimeIdx).Split(":")
		                  StartDate.Hour=Val(ST(0))
		                  StartDate.Minute=Val(ST(1))
		                  if UBound(ST)=2 then
		                    StartDate.Second=Val(ST(2))
		                  end if
		                  StartTime=StartDate.SQLDateTime+".000"
		                end if
		              end if
		              
		              if (RepresentingIdx>=0) then
		                If FieldList(RepresentingIdx) <> "" then
		                  Representing=FieldList(RepresentingIdx)
		                else
		                  Representing=""
		                  if CityIdx>=0 then
		                    Representing=FieldList(CityIdx)
		                  end if
		                  if StateIdx>=0 then
		                    Representing=Representing+" "+FieldList(StateIdx)
		                  end if
		                  if CountryIdx>=0 then
		                    Representing=Representing+ " "+ FieldList(CountryIdx)
		                  end if
		                end if
		                Representing=Uppercase(LTrim(Representing))
		              else
		                Representing=""
		                if CityIdx>=0 then
		                  Representing=FieldList(CityIdx)
		                end if
		                if StateIdx>=0 then
		                  Representing=Representing+" "+FieldList(StateIdx)
		                end if
		                if CountryIdx>=0 then
		                  Representing=Representing+ " "+ FieldList(CountryIdx)
		                end if
		                Representing=Uppercase(LTrim(Representing))
		              end if
		              
		              if DeclaredIdx>0 then
		                if FieldList(DeclaredIdx) <> "" and DivisionIDStg<> "0" then
		                  SQLStatement="SELECT rowid FROM CCTeams WHERE Team_Name LIKE '"+RTrim(LTrim(FieldList(DeclaredIdx)))+"' "
		                  SQLStatement=SQLStatement+"AND Gender='"+Uppercase(RTrim(LTrim(FieldList(GenderIdx))))+"' "
		                  SQLStatement=SQLStatement+"AND DivisionID="+DivisionIDStg
		                  rsTeam=app.theDB.DBSQLSelect(SQLStatement)
		                  if rsTeam.RecordCount>0 then
		                    TeamID=rsTeam.Field("rowid").StringValue
		                  else
		                    SQLStatement="INSERT INTO CCTeams (Team_Name, Gender, DivisionID) VALUES ('"
		                    SQLStatement=SQLStatement+RTrim(LTrim(FieldList(DeclaredIdx)))+"', "
		                    SQLStatement=SQLStatement+" '"+Uppercase(RTrim(LTrim(FieldList(GenderIdx))))+"', "
		                    SQLStatement=SQLStatement+" '"+DivisionIDStg+"')"
		                    App.theDB.DBSQLExecute(SQLStatement)
		                    TeamID=Str(App.theDB.DBLastRowID)
		                  end if
		                else
		                  TeamID="0"
		                end if
		              else
		                TeamID="0"
		              end if
		              
		              if WaveIdx>=0 and WaveIdx<=UBound(FieldList) then
		                If FieldList(WaveIdx) <> "" then
		                  Wave=FieldList(WaveIdx)
		                else
		                  Wave=""
		                end if
		              else
		                Wave=""
		              end if
		              
		              if NoteIdx>=0 and NoteIdx<=UBound(FieldList) then
		                If FieldList(NoteIdx) <> "" then
		                  Note=FieldList(NoteIdx)
		                else
		                  Note=""
		                end if
		              else
		                Note=""
		              end if
		              
		              if SeedGroupIdx>=0 and SeedGroupIdx<=UBound(FieldList) then
		                If FieldList(SeedGroupIdx) <> "" then
		                  SeedGroup=FieldList(SeedGroupIdx)
		                else
		                  SeedGroup=""
		                end if
		              else
		                SeedGroup=""
		              end if
		              
		              if NGBLicIdx>=0 and NGBLicIdx<=UBound(FieldList) then
		                If FieldList(NGBLicIdx) <> "" then
		                  NGBLicense=FieldList(NGBLicIdx)
		                else
		                  NGBLicense=""
		                end if
		              else
		                NGBLicense=""
		              end if
		              
		              if NGB2LicIdx>=0 and NGB2LicIdx<=UBound(FieldList) then
		                If FieldList(NGB2LicIdx) <> "" then
		                  NGB2License=FieldList(NGB2LicIdx)
		                else
		                  NGB2License=""
		                end if
		              else
		                NGB2License=""
		              end if
		              
		              if NGBPointsIdx>=0 and NGBPointsIdx<=UBound(FieldList) then
		                If FieldList(NGBPointsIdx) <> "" then
		                  NGBPoints=FieldList(NGBPointsIdx)
		                else
		                  NGBPoints=""
		                end if
		              else
		                NGBPoints=""
		              end if
		              
		              if NGB2PointsIdx>=0 and NGB2PointsIdx<=UBound(FieldList) then
		                If FieldList(NGB2PointsIdx) <> "" then
		                  NGB2Points=FieldList(NGB2PointsIdx)
		                else
		                  NGB2Points=""
		                end if
		              else
		                NGB2Points=""
		              end if
		              
		              if SMSAddressIdx>=0 then
		                SMSAddress=FieldList(SMSAddressIdx)
		                SMSAddress=ReplaceAll( SMSAddress, "-", "" )
		                SMSAddress=ReplaceAll( SMSAddress, "(", "" )
		                SMSAddress=ReplaceAll( SMSAddress, ")", "" )
		                SMSAddress=ReplaceAll( SMSAddress, " ", "" )
		              else
		                SMSAddress=""
		              end if
		              
		              SQLStatement="INSERT INTO participants (RaceID,DivisionID,Racer_Number,Participant_Name,First_Name, "
		              SQLStatement=SQLStatement+"Street, City, State, Postal_Code, Country, Phone, EMail, Birth_Date, "
		              SQLStatement=SQLStatement+"Gender,Age,Representing,NGB_License_Number,NGB2_License_Number,NGB1_Points,NGB2_Points,"
		              SQLStatement=SQLStatement+"Assigned_Start,Actual_Start,Actual_Stop,Total_Time,Net_Time,"
		              SQLStatement=SQLStatement+"DNS,DNF,DQ,Laps_Completed,"
		              SQLStatement=SQLStatement+"Total_Adjustment,Adjustment_Applied,Adjustment_Reason,Seed_Group,SMS_Address,DateTimeInserted,Wave,Note"
		              
		              if wnd_List.ResultType.Text="Cross Country Running" then
		                SQLStatement=SQLStatement+",TeamID"
		              end if
		              
		              SQLStatement=SQLStatement+") VALUES ("
		              SQLStatement=SQLStatement+"1," 'RaceID
		              SQLStatement=SQLStatement+divisionIDStg+"," 'DivisionID
		              SQLStatement=SQLStatement+FieldList(BibNumberIdx)+","
		              if TeamMember="N" then
		                SQLStatement=SQLStatement+"'"+Uppercase(FieldList(LastNameIdx))+"',"
		                SQLStatement=SQLStatement+"'"+Titlecase(FieldList(FirstNameIdx))+"',"
		              else
		                SQLStatement=SQLStatement+"'"+Representing+"',"
		                SQLStatement=SQLStatement+"'',"
		              end if
		              SQLStatement=SQLStatement+"'"+Street+"',"
		              SQLStatement=SQLStatement+"'"+City+"',"
		              SQLStatement=SQLStatement+"'"+State+"',"
		              SQLStatement=SQLStatement+"'"+PostalCode+"',"
		              SQLStatement=SQLStatement+"'"+Country+"',"
		              SQLStatement=SQLStatement+"'"+Phone+"',"
		              SQLStatement=SQLStatement+"'"+Lowercase(EMail)+"',"
		              if BirthDate.SQLDate<>"0002-11-30" then
		                SQLStatement=SQLStatement+"'"+BirthDate.SQLDate+"',"
		              else
		                SQLStatement=SQLStatement+"'0000-00-00',"
		              end if
		              SQLStatement=SQLStatement+"'"+Uppercase(FieldList(GenderIdx))+"',"
		              SQLStatement=SQLStatement+str(Age)+","
		              if TeamMember="N" then
		                SQLStatement=SQLStatement+"'"+Representing+"',"
		              else
		                'SQLStatement=SQLStatement+"'"+Titlecase(FieldList(FirstNameIdx))+" "+Titlecase(FieldList(LastNameIdx))+"',"
		                SQLStatement=SQLStatement+"'"+FieldList(FirstNameIdx)+" "+FieldList(LastNameIdx)+"',"
		              end if
		              SQLStatement=SQLStatement+"'"+NGBLicense+"',"
		              SQLStatement=SQLStatement+"'"+NGB2License+"',"
		              SQLStatement=SQLStatement+"'"+NGBPoints+"',"
		              SQLStatement=SQLStatement+"'"+NGB2Points+"',"
		              SQLStatement=SQLStatement+"'"+StartTime+"'," ' Assigned_Start
		              SQLStatement=SQLStatement+"'"+app.RaceDate.SQLDate+" 00:00:00.000'," 'Actual_Start
		              SQLStatement=SQLStatement+"'"+left(StartTime,10)+" 00:00:00.000'," 'Actual_Time
		              SQLStatement=SQLStatement+"'00:00:00.000'," 'Total_Time
		              SQLStatement=SQLStatement+"'00:00:00.000'," 'Net_Time
		              SQLStatement=SQLStatement+"'Y'," 'DNS
		              SQLStatement=SQLStatement+"'Y'," 'DNF
		              SQLStatement=SQLStatement+"'N'," 'DQ
		              SQLStatement=SQLStatement+"0," 'Interval
		              SQLStatement=SQLStatement+"'+00:00:00.000'," 'Adjustment
		              SQLStatement=SQLStatement+"''," 'Adjustment Applied
		              SQLStatement=SQLStatement+"''," 'Adjustment Reason
		              SQLStatement=SQLStatement+"'"+SeedGroup+"'," 'Seed Group
		              SQLStatement=SQLStatement+"'"+SMSAddress+"'," 'SMS Address 
		              SQLStatement=SQLStatement+"'"+CurrentDate.SQLDateTime+"'," 
		              SQLStatement=SQLStatement+"'"+Wave+"'," 
		              SQLStatement=SQLStatement+"'"+Note+"'" 'don't forget the comma if more fields are added
		              if wnd_List.ResultType.Text="Cross Country Running" then
		                SQLStatement=SQLStatement+","+TeamID
		              end if
		              SQLStatement=SQLStatement+")"
		              ' insert the record
		              App.theDB.DBSQLExecute(SQLStatement)
		              Added=Added+1
		              
		              'insert the team member record
		              if TeamMember>="1" and TeamMember<="9" then
		                SQLStatement="INSERT INTO teammembers (ParticipantID,IntervalNumber,MemberName) VALUES ("
		                SQLStatement=SQLStatement+Format(App.theDB.DBLastRowID,"########")+","
		                SQLStatement=SQLStatement+TeamMember+","
		                SQLStatement=SQLStatement+"'"+Titlecase(FieldList(FirstNameIdx))+" "+Uppercase(FieldList(LastNameIdx))+"')"
		                App.theDB.DBSQLExecute(SQLStatement)
		              end if
		              
		            else
		              if TeamMember<>"N" then
		                'update the record
		                if (InStr(rsParticipants.Field("Representing").StringValue, Titlecase(FieldList(FirstNameIdx))+" "+Titlecase(FieldList(LastNameIdx)))=0) then
		                  SQLStatement="UPDATE participants SET Representing='"+rsParticipants.Field("Representing").StringValue+", "+Titlecase(FieldList(FirstNameIdx))+" "+Titlecase(FieldList(LastNameIdx))
		                  SQLStatement=SQLStatement+"' WHERE rowid="+rsParticipants.Field("rowid").StringValue
		                  App.theDB.DBSQLExecute(SQLStatement)
		                end if
		                
		                if TeamMember>="1" and TeamMember<="9" then
		                  'insert the team member record
		                  SQLStatement="INSERT INTO teammembers (ParticipantID,IntervalNumber,MemberName) VALUES ("
		                  SQLStatement=SQLStatement+rsParticipants.Field("rowid").StringValue+","
		                  SQLStatement=SQLStatement+TeamMember+","
		                  SQLStatement=SQLStatement+"'"+Titlecase(FieldList(FirstNameIdx))+" "+Uppercase(FieldList(LastNameIdx))+"')"
		                  App.theDB.DBSQLExecute(SQLStatement)
		                end if
		                
		              else
		                Duplicates=Duplicates+1
		              end if
		            end if
		          end if
		        Next
		        'end of file
		        d.Icon=0 'note icon
		        app.theDB.DBCommit
		        Progress.Close
		        
		        ExecuteQuery("Participants",ExtraQuery_Participants,Participant_Sort)
		        
		        d.Message="Import Complete. Added: "+str(Added)
		        if Duplicates>0 then
		          d.Message=d.Message+" Duplicate Racer Numbers: "+str(Duplicates)
		        end if
		        b=d.ShowModal
		        
		      else
		        'dialog tell them missing data
		        
		        
		        d.Icon=2 'stop icon
		        d.Message="The file does not appear to be in the proper format."
		        d.Explanation="The file needs to have tab or comma seperated columns and containing at least First Name, Last Name, Gender, and Birth Date or Age."
		        b=d.ShowModal
		        
		      end if
		      
		    End if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ImportPocketTimerTimeData()
		  Dim f As FolderItem
		  dim ImportStream as TextInputStream
		  Dim IncomingLine(), HeaderList(), FieldList(), BD(), TODParts() as string
		  Dim IncomingData, ParticipantIDStg, SQLStatement as String
		  Dim BibNumberIdx, TODIdx, IntervalNumberIdx, LineNumber as Integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  dim rs As RecordSet
		  dim TOD as New Date
		  
		  dim recCount as integer
		  dim res as boolean
		  
		  TOD.SQLDateTime=wnd_List.RaceTime.Text
		  
		  BibNumberIdx=-1
		  IntervalNumberIdx=-1
		  TODIdx=-1
		  
		  if ImportFIlePath<>"" then
		    f=GetFolderItem(ImportFIlePath)
		    If f<>Nil then
		      Progress.Show
		      Progress.Initialize("Progress","Loading Time Import File...",0,-1)
		      ImportStream=f.OpenAsTextFile
		      IncomingData=ReplaceAll(ImportStream.ReadAll,chr(34),"")
		      IncomingData=Uppercase(IncomingData)
		      IncomingLine=IncomingData.Split(chr(13))
		      ImportStream.Close
		      'Check to see if the header line is there and the minimum required fields are there
		      
		      HeaderList=IncomingLine(0).Split(Seperator)
		      
		      BibNumberIdx=HeaderList.IndexOf("RACER NUMBER")
		      IntervalNumberIdx=HeaderList.IndexOf("INTERVAL NUMBER")
		      TODIdx=HeaderList.IndexOf("TOD")
		      
		      If (BibNumberIdx>=0 and TODIdx>=0 and IntervalNumberIdx>=0) then
		        
		        'Loop through the file and import the data
		        'read the data
		        
		        // Loop over a string, 5 characters at a time.
		        Progress.Initialize("Progress","Adding Time Records...",UBound(IncomingLine),-1)
		        For LineNumber = 1 to UBound(IncomingLine)
		          Progress.UpdateProg1(LineNumber)
		          
		          FieldList=IncomingLine(LineNumber).Split(Seperator)
		          if FieldList(BibNumberIdx)<>"?" then
		            SQLStatement="SELECT rowid FROM participants WHERE Racer_Number="+FieldList(BibNumberIdx)
		            rs=app.theDB.DBSQLSelect(SQLStatement)
		            
		            if not(rs.eof) then
		              ParticipantIDStg=rs.Field("rowid").StringValue
		              
		              TODParts=Split(FieldList(TODIdx),":")
		              
		              TOD.Hour=val(TODParts(0))
		              TOD.Minute=val(TODParts(1))
		              TOD.Second=val(TODParts(2))
		              
		              SQLStatement="INSERT INTO times (RaceID,ParticipantID,Use_This_Passing,Interval_Number,Actual_Time)"
		              SQLStatement=SQLStatement+" VALUES ("
		              SQLStatement=SQLStatement+"1," 'RaceID
		              SQLStatement=SQLStatement+participantIDStg+"," 'participant ID
		              SQLStatement=SQLStatement+"'Y'," 'Use_This_Passing
		              SQLStatement=SQLStatement+FieldList(IntervalNumberIdx)+"," 'Interval Number
		              SQLStatement=SQLStatement+"'"+TOD.SQLDateTime+".000'" 'time of day
		              SQLStatement = SQLStatement + ")"
		              ' insert the record
		              App.theDB.DBSQLExecute(SQLStatement)
		              
		              'special intervals
		              Select Case FieldList(IntervalNumberIdx)
		              case "0"
		                SQLStatement="UPDATE participants SET DNS='N', Actual_Start = '"+TOD.SQLDateTime+".000'', DNS='N' WHERE rowid="+ParticipantIDStg
		                App.theDB.DBSQLExecute(SQLStatement)
		                
		              case "9999"
		                SQLStatement="UPDATE participants SET DNS='N', DNF='N', Actual_Stop = '"+TOD.SQLDateTime+".000', DNF='N' WHERE rowid="+ParticipantIDStg
		                App.theDB.DBSQLExecute(SQLStatement)
		                
		              end Select
		            end if
		          end if
		        Next
		        'end of file
		        
		        RecalcTimes
		        
		      else
		        'dialog tell them missing data
		        
		        
		        d.Icon=2 'stop icon
		        d.Message="The file does not appear to be in the proper format."
		        d.Explanation="The file needs to be tab or comma seperated columns and containing Racer Number, Interval Number and TOD."
		        b=d.ShowModal
		        
		      end if
		      
		    End if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ImportRaceData()
		  Dim f As FolderItem
		  dim ImportStream as TextInputStream
		  Dim IncomingLine(), HeaderList(), FieldList(), BD() as string
		  Dim IncomingData, SQLStatement, Representing, StartTime, DivisionIDStg, DistanceIDStg as String
		  Dim Age, LineNumber as Integer
		  Dim BibNumberIdx, FirstNameIdx, LastNameIdx, GenderIdx, BirthDateIdx, AgeIdx, CategoryIdx as Integer
		  Dim HomeTownIdx, HTStateIdx, HTCountryIdx, TeamIdx as Integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  Dim BirthDate as date
		  
		  dim recCount as integer
		  dim rsDivisions as recordSet
		  dim res as boolean
		  
		  if ImportFIlePath<>"" then
		    f=GetFolderItem(ImportFIlePath)
		    If f<>Nil then
		      ImportStream=f.OpenAsTextFile
		      IncomingData=ReplaceAll(ImportStream.ReadAll,chr(34),"")
		      IncomingLine=IncomingData.Split(chr(13))
		      ImportStream.Close
		      
		      FieldList=IncomingLine(LineNumber).Split(Seperator)
		      if UBound(FieldList)=9 then
		        Progress.Initialize("Progress","Adding Race Data...",UBound(IncomingLine),-1)
		        
		        'remove any existing data
		        SQLStatement="DELETE FROM races WHERE RaceID=1"
		        App.theDB.DBSQLExecute(SQLStatement)
		        SQLStatement="DELETE FROM racedistances WHERE RaceID=1"
		        App.theDB.DBSQLExecute(SQLStatement)
		        
		        'Add the race record
		        SQLStatement="INSERT INTO races (RaceID, Race_Name, Date, Race_Age_Date, Start_Time, "
		        SQLStatement=SQLStatement+"Places_To_Truncate, Minimum_Time, Year, Display_Type, Calculate_Time_From, NGB1, "
		        SQLStatement=SQLStatement+"Note, Display_Lookup_List, Allow_Early_Start, "
		        SQLStatement=SQLStatement+"Show_Back_Time, Show_Net_Time, Show_Adjustment, "
		        SQLStatement=SQLStatement+"Publish, NGB2, Racers_Per_Start, "
		        SQLStatement=SQLStatement+"Start_Interval, Starting_Racer_Number) VALUES ("
		        
		        SQLStatement=SQLStatement+"1," 'RaceID
		        SQLStatement=SQLStatement+"'"+FieldList(0)+"', " 'Race Name
		        SQLStatement=SQLStatement+"'"+FieldList(1)+"', " 'Date
		        SQLStatement=SQLStatement+"'"+FieldList(2)+"', " 'Race_Age_Date
		        SQLStatement=SQLStatement+"'"+FieldList(3)+"', " 'Start_Time
		        SQLStatement=SQLStatement+"'"+FieldList(4)+"', " 'Places_To_Truncate
		        SQLStatement=SQLStatement+"'"+FieldList(5)+"', " 'Minimum_Time
		        SQLStatement=SQLStatement+"'"+FieldList(6)+"', " 'Year
		        SQLStatement=SQLStatement+"'"+FieldList(7)+"', " 'Display_Type
		        SQLStatement=SQLStatement+"'"+FieldList(8)+"', " 'Calculate_Time_From
		        SQLStatement=SQLStatement+"'"+FieldList(9)+"', " 'NGB1
		        SQLStatement=SQLStatement+"'', " 'Note
		        SQLStatement=SQLStatement+"'N', " 'Display_Lookup_List
		        SQLStatement=SQLStatement+"'N', " 'Allow_Early_Start
		        SQLStatement=SQLStatement+"'Y', " 'Show_Back_Time
		        SQLStatement=SQLStatement+"'Y', " 'Show_Net_Time
		        SQLStatement=SQLStatement+"'Y', " 'Show_Adjustment
		        SQLStatement=SQLStatement+"'N', " 'Publish
		        SQLStatement=SQLStatement+"'', " 'NGB2
		        SQLStatement=SQLStatement+"0, " 'Racers_Per_Start
		        SQLStatement=SQLStatement+"'00:00:00', " 'Start_Interval
		        SQLStatement=SQLStatement+"0)" 'Starting_Racer_Number
		        App.theDB.DBSQLExecute(SQLStatement)
		        
		        'Add the race distance records
		        For LineNumber = 1 to UBound(IncomingLine)
		          Progress.UpdateProg1(LineNumber)
		          
		          FieldList=IncomingLine(LineNumber).Split(Seperator)
		          if UBound(FieldList)=2 then
		            SQLStatement="INSERT INTO racedistances (RaceID, RaceDistance_Name, Actual_Distance, Pace_Type, "
		            SQLStatement=SQLStatement+"OverallPlace, FastestTime, CurrentPlace_Male, FastestTime_Male, "
		            SQLStatement=SQLStatement+"CurrentPlace_Female, FastestTime_Female, CurrentPlace_Mixed, "
		            SQLStatement=SQLStatement+"FastestTime_Mixed) VALUES ("
		            SQLStatement=SQLStatement+"1,"
		            SQLStatement=SQLStatement+"'"+FieldList(0)+"', "
		            SQLStatement=SQLStatement+"'"+FieldList(1)+"', "
		            SQLStatement=SQLStatement+"'"+FieldList(2)+"',"
		            SQLStatement=SQLStatement+"0,"
		            SQLStatement=SQLStatement+"'00:00:00.000',"
		            SQLStatement=SQLStatement+"0,"
		            SQLStatement=SQLStatement+"'00:00:00.000',"
		            SQLStatement=SQLStatement+"0,"
		            SQLStatement=SQLStatement+"'00:00:00.000',"
		            SQLStatement=SQLStatement+"0,"
		            SQLStatement=SQLStatement+"'00:00:00.000')"
		            App.theDB.DBSQLExecute(SQLStatement)
		          end if
		        Next
		        'end of file
		        app.ChangeRace
		        LoadRaceTab
		        d.Icon=0 'note icon
		        app.theDB.DBCommit
		        Progress.Close
		        d.Message="Import Complete."
		        b=d.ShowModal
		      else
		        d.Icon=2 'stop icon
		        d.Message="The race import file does not appear to be in the proper format."
		        d.Explanation="See documentation for the proper format."
		        b=d.ShowModal
		      end if
		    End if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ImportTeamData()
		  Dim f As FolderItem
		  dim ImportStream as TextInputStream
		  Dim IncomingLine(), HeaderList(), FieldList() as string
		  Dim IncomingData, SQLStatement, DistanceIDStg, DivisionIDStg as String
		  Dim i, FieldCountRemainder, LineNumber as Integer
		  Dim RepresentingIdx, GenderIdx as Integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  Dim CurrentDate As Date
		  Dim rsTX as RecordSet
		  
		  CurrentDate = new Date
		  
		  if ImportFIlePath<>"" then
		    f=GetFolderItem(ImportFIlePath)
		    If f<>Nil then
		      ImportStream=f.OpenAsTextFile
		      IncomingData=ReplaceAll(ImportStream.ReadAll,chr(34),"")
		      IncomingData=ReplaceAll(IncomingData,chr(10),chr(13)) 'replace linefeeds with cr
		      IncomingData=ReplaceAll(IncomingData,chr(13)+chr(13),chr(13)) 'replace double cr with single cr
		      IncomingLine=IncomingData.Split(chr(13))
		      ImportStream.Close
		      'Check to see if the header line is there and the minimum required fields are there
		      IncomingLine(0)=Uppercase(IncomingLine(0))
		      HeaderList=IncomingLine(0).Split(Seperator)
		      for i=0 to UBound(HeaderList)
		        HeaderList(i)=Trim(HeaderList(i))
		      next
		      
		      RepresentingIdx=HeaderList.IndexOf("REPRESENTING")
		      GenderIdx=HeaderList.IndexOf("GENDER")
		      
		      If (RepresentingIdx>=0) then
		        Progress.Show
		        Progress.Initialize("Progress","Loading Team Import File...",0,-1)
		        'Loop through the file and import the data
		        'read the data
		        
		        Progress.Initialize("Progress","Adding Team Records...",UBound(IncomingLine),-1)
		        For LineNumber = 1 to UBound(IncomingLine)
		          Progress.UpdateProg1(LineNumber)
		          FieldList=IncomingLine(LineNumber).Split(Seperator)
		          if IncomingLine(LineNumber)<>"" then
		            
		            SQLStatement="INSERT INTO CCTeams (Team_Name, Gender) VALUES ("
		            SQLStatement=SQLStatement+"'"+LTrim(RTrim(FieldList(RepresentingIdx)))+"', " 'Representing
		            SQLStatement=SQLStatement+"'"+Uppercase(LTrim(RTrim(FieldList(GenderIdx))))+"')" 'Gender
		            ' insert the record
		            App.theDB.DBSQLExecute(SQLStatement)
		            
		          end if
		        Next
		        'end of file
		        d.Icon=0 'note icon
		        app.theDB.DBCommit
		        Progress.Close
		        d.Message="Import Complete."
		        b=d.ShowModal
		        
		      else
		        'dialog tell them missing data
		        d.Icon=2 'stop icon
		        d.Message="The file does not appear to be in the proper format."
		        d.Explanation="The file needs to be tab or comma seperated columns and containing Representing."
		        b=d.ShowModal
		        
		      end if
		      
		    End if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ImportToolKitTimeData()
		  Dim f As FolderItem
		  dim ImportStream as TextInputStream
		  Dim IncomingLine(), FieldList() as string
		  Dim IncomingData, Time as String
		  Dim i, TXCodeIDX, TODIDX, DateIDX, LineNumber as Integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  dim rs As RecordSet
		  dim DateTime as new date
		  dim TimeINSeconds as Double
		  
		  dim recCount as integer
		  dim res as boolean
		  
		  TXCodeIDX=1
		  TODIdx=2
		  Seperator=chr(9)
		  
		  if ImportFIlePath<>"" then
		    f=GetFolderItem(ImportFIlePath)
		    If f<>Nil then
		      Progress.Show
		      Progress.Initialize("Progress","Loading ToolKit® Time Import File...",0,-1)
		      ImportStream=f.OpenAsTextFile
		      IncomingData=ReplaceAll(ImportStream.ReadAll,chr(1),"")
		      IncomingData=Uppercase(IncomingData)
		      IncomingLine=IncomingData.Split(chr(10))
		      ImportStream.Close
		      'Check to see if the header line is there and the minimum required fields are there
		      FieldList=IncomingLine(0).Split(Seperator)
		      If (FieldList(0)="BibTag" or FieldList(0)="LF" or FieldList(0)="UH") then
		        
		        'Loop through the file and import the data
		        'read the data
		        
		        // Loop over a string, 5 characters at a time.
		        Progress.Initialize("Progress","Adding Time Records...",UBound(IncomingLine),-1)
		        For LineNumber = 0 to UBound(IncomingLine)
		          Progress.UpdateProg1(LineNumber)
		          
		          FieldList=IncomingLine(LineNumber).Split(Seperator)
		          if UBound(FieldList)>3 then
		            
		            Time=FieldList(DateIDX)+" "+FieldList(TODIDX)
		            TimeINSeconds=app.ConvertTimeToSeconds(FieldList(TODIDX))
		            TimeINSeconds=TimeINSeconds-app.AMBOffset
		            'TimeINSeconds=TimeINSeconds-app.AMBOffset
		            Time=app.ConvertSecondsToTime(TimeINSeconds,true)
		            
		            'if app.PlacesToTruncate=0 then
		            'Time=Left(Time,len(Time)-4)
		            'Time=Time+".000"
		            'else
		            'Time=Left(Time,len(Time)-(3-app.PlacesToTruncate))
		            'for i = 1 to 3-app.PlacesToTruncate
		            'Time=Time+"0"
		            'next
		            'end if
		            
		            App.TimeSource.Append TimeSource
		            App.TimeType.Append "Both"
		            App.TimeTXCode.Append FieldList(TXCodeIDX)
		            App.TimeTime.Append Time
		            App.TimeTimingPointID.Append TimingPointID
		          end if
		          
		        Next
		        'end of file
		        
		      else
		        'dialog tell them missing data
		        
		        
		        d.Icon=2 'stop icon
		        d.Message="The file does not appear to be in the proper format."
		        d.Explanation="The file needs to be file generated by ToolKit® using the Milliseconds Pro Format in the user defined exporter."
		        b=d.ShowModal
		        
		      end if
		      
		      Progress.Close
		    End if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ImportTXData()
		  Dim f As FolderItem
		  dim ImportStream as TextInputStream
		  Dim IncomingLine(), HeaderList(), FieldList() as string
		  Dim IncomingData, SQLStatement, DistanceIDStg, DivisionIDStg as String
		  Dim i, FieldCountRemainder, LineNumber as Integer
		  Dim RacerNumberIdx, TXCodeIdx, TXNumberIdx, IssuedIdx, HitsIdx, TeamMemberIDIdx, ReturnedIdx as Integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  Dim CurrentDate As Date
		  Dim rsTX as RecordSet
		  
		  CurrentDate = new Date
		  
		  if ImportFIlePath<>"" then
		    f=GetFolderItem(ImportFIlePath)
		    If f<>Nil then
		      ImportStream=f.OpenAsTextFile
		      IncomingData=ReplaceAll(ImportStream.ReadAll,chr(34),"")
		      IncomingData=ReplaceAll(IncomingData,chr(10),chr(13)) 'replace linefeeds with cr
		      IncomingData=ReplaceAll(IncomingData,chr(13)+chr(13),chr(13)) 'replace double cr with single cr
		      IncomingLine=IncomingData.Split(chr(13))
		      ImportStream.Close
		      'Check to see if the header line is there and the minimum required fields are there
		      IncomingLine(0)=Uppercase(IncomingLine(0))
		      HeaderList=IncomingLine(0).Split(Seperator)
		      for i=0 to UBound(HeaderList)
		        HeaderList(i)=Trim(HeaderList(i))
		      next
		      
		      RacerNumberIdx=HeaderList.IndexOf("RACER NUMBER")
		      TXCodeIdx=HeaderList.IndexOf("TX CODE")
		      TXNumberIdx=headerlist.IndexOf("TX NUMBER")
		      IssuedIdx=HeaderList.IndexOf("ISSUED")
		      HitsIdx=HeaderList.IndexOf("HIT COUNT")
		      TeamMemberIDIdx=HeaderList.IndexOf("TEAMMEMBERID")
		      ReturnedIdx=HeaderList.IndexOf("RETURNED")
		      
		      If (RacerNumberIdx>=0 and TXCodeIdx>=0) then
		        Progress.Show
		        Progress.Initialize("Progress","Loading Transponder Import File...",0,-1)
		        'Loop through the file and import the data
		        'read the data
		        
		        Progress.Initialize("Progress","Adding Transponder Records...",UBound(IncomingLine),-1)
		        For LineNumber = 1 to UBound(IncomingLine)
		          Progress.UpdateProg1(LineNumber)
		          FieldList=IncomingLine(LineNumber).Split(Seperator)
		          if IncomingLine(LineNumber)<>"" then
		            
		            if ((instr(FieldList(TXCodeIdx),"-") = 0) and (asc(left(FieldList(TXCodeIdx),1))>=65) and (len(FieldList(TXCodeIdx)) = 7)) Then
		              FieldList(TXCodeIdx) = left(FieldList(TXCodeIdx),2)+"-"+right(FieldList(TXCodeIdx),5)
		            end if
		            
		            SQLStatement="SELECT rowid FROM transponders WHERE Racer_Number="+FieldList(RacerNumberIdx)+" AND TX_Code='"+LTrim(RTrim(FieldList(TXCodeIdx)))+"'"
		            rsTX=app.theDB.DBSQLSelect(SQLStatement)
		            if rsTX.EOF then
		              
		              SQLStatement="INSERT INTO transponders (RaceID, Racer_Number, TX_Code, TX_Number, Issued, TeamMemberID, Hit_Count, Returned) "
		              SQLStatement=SQLStatement+" VALUES ("
		              SQLStatement=SQLStatement+"1," 'RaceID
		              SQLStatement=SQLStatement+FieldList(RacerNumberIdx)+"," 'Racer_Number
		              SQLStatement=SQLStatement+"'"+LTrim(RTrim(FieldList(TXCodeIdx)))+"'," 'TX_Code
		              
		              if TXNumberIdx>0 and TXNumberIdx<=UBound(FieldList) then 'TX_Number
		                SQLStatement=SQLStatement+"'"+LTrim(RTrim(FieldList(TXNumberIdx)))+"',"
		              else
		                SQLStatement=SQLStatement+"'',"
		              end if
		              
		              if IssuedIdx>0 and IssuedIdx<=UBound(FieldList) then 'Issued
		                SQLStatement=SQLStatement+"'"+FieldList(IssuedIdx)+"',"
		              else
		                SQLStatement=SQLStatement+"'"+CurrentDate.SQLDateTime+"'," 
		              end if
		              
		              if TeamMemberIDIdx>0 and TeamMemberIDIdx<=UBound(FieldList) then 'Participant Name
		                SQLStatement=SQLStatement+"'"+FieldList(TeamMemberIDIdx)+"',"
		              else
		                SQLStatement=SQLStatement+"''," 
		              end if
		              
		              if HitsIdx>0 and HitsIdx<=UBound(FieldList) then 'Hit Count
		                SQLStatement=SQLStatement+"'"+FieldList(HitsIdx)+"',"
		              else
		                SQLStatement=SQLStatement+"'0',"
		              end if
		              
		              if ReturnedIdx>0 and ReturnedIdx<=UBound(FieldList) then 'Issued
		                SQLStatement=SQLStatement+"'"+FieldList(ReturnedIdx)+"')"
		              else
		                SQLStatement=SQLStatement+"'0000-00-00 00:00:00')"
		              end if
		              
		              ' insert the record
		              App.theDB.DBSQLExecute(SQLStatement)
		            end if
		            rsTX.Close
		            
		          end if
		        Next
		        'end of file
		        d.Icon=0 'note icon
		        app.theDB.DBCommit
		        Progress.Close
		        d.Message="Import Complete."
		        b=d.ShowModal
		        
		      else
		        'dialog tell them missing data
		        d.Icon=2 'stop icon
		        d.Message="The file does not appear to be in the proper format."
		        d.Explanation="The file needs to be tab or comma seperated columns and containing Racer Number and TX Code."
		        b=d.ShowModal
		        
		      end if
		      
		    End if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadRaceTab()
		  dim res as boolean
		  dim recsRS as recordSet
		  dim recCount as Integer
		  dim i as integer
		  Dim f as FolderItem
		  
		  puStartType.addrow "Wave"
		  puStartType.rowTag(0)=-1
		  puStartType.addrow "Mass"
		  puStartType.rowTag(1)=0
		  puStartType.HelpTag="Select the start type for this race."+chr(13)+chr(13)
		  puStartType.HelpTag=puStartType.HelpTag+" Mass - all the participants for a distance start together."+chr(13)+chr(13)
		  puStartType.HelpTag=puStartType.HelpTag+" Wave - participants for a particular distance start at different times."+chr(13)+chr(13)
		  if app.CircuitRaceAvailable then
		    puStartType.addrow "Bicycle Circuit or Road Race"
		    puStartType.rowTag(puStartType.ListCount-1)=9999
		    puStartType.HelpTag=puStartType.HelpTag+" Bicycle Circuit or Road Race - use for all road bike events except criteriums and time trials."+chr(13)+chr(13)
		  end if
		  if app.CriteriumAvailable then
		    puStartType.addrow "24 hr, Criterium or Cyclocross"
		    puStartType.rowTag(puStartType.ListCount-1)=9998
		    puStartType.HelpTag=puStartType.HelpTag+" 24 hr, Criterium, or Cyclocross - use for all events where participants are scored on the number of laps completed in a fixed amount of time."+chr(13)+chr(13)
		  end if
		  if app.TimeTrialAvailable then
		    puStartType.addrow "Time Trial 1/start"
		    puStartType.rowTag(puStartType.ListCount-1)=1
		    puStartType.addrow "Time Trial 2/start"
		    puStartType.rowTag(puStartType.ListCount-1)=2
		    puStartType.addrow "Time Trial 3/start"
		    puStartType.rowTag(puStartType.ListCount-1)=3
		    puStartType.addrow "Time Trial 4/start"
		    puStartType.rowTag(puStartType.ListCount-1)=4
		    puStartType.HelpTag=puStartType.HelpTag+" Time Trial #/start - use for all time trial style events."
		    
		    lblTTStartInterval.Visible=true
		    cbTTStartInterval.Visible=true
		  end if
		  
		  
		  ResultType.addrow "Select..."
		  ResultType.addrow "Normal"
		  ResultType.addrow "Normal with Intervals"
		  if app.TriathlonAvailable then
		    ResultType.AddRow "Triathlon"
		  end if
		  if app.CrossCountryRunAvailable then
		    ResultType.addrow constCrossCountryResultType
		  end if
		  ResultType.addrow wnd_List.constFISUSSAResultType
		  'ResultType.addrow "Sea Otter"
		  
		  Rounding.addrow "Select..."
		  Rounding.rowTag(0)=-1
		  Rounding.addrow "00:00:00"
		  Rounding.rowTag(1)=0
		  Rounding.addrow "00:00:00.0"
		  Rounding.rowTag(2)=1
		  Rounding.addrow "00:00:00.00"
		  Rounding.rowTag(3)=2
		  Rounding.addrow "00:00:00.000"
		  Rounding.rowTag(4)=3
		  
		  cbnthTime.addrow "1st"
		  cbnthTime.rowTag(0)=1
		  cbnthTime.addrow "2nd"
		  cbnthTime.rowTag(1)=2
		  cbnthTime.addrow "3rd"
		  cbnthTime.rowTag(2)=3
		  cbnthTime.addrow "4th"
		  cbnthTime.rowTag(3)=4
		  cbnthTime.addrow "5th"
		  cbnthTime.rowTag(4)=5
		  cbnthTime.addrow "6th"
		  cbnthTime.rowTag(4)=6
		  cbnthTime.addrow "7th"
		  cbnthTime.rowTag(4)=7
		  cbnthTime.addrow "8th"
		  cbnthTime.rowTag(4)=8
		  cbnthTime.addrow "9th"
		  cbnthTime.rowTag(4)=9
		  cbnthTime.addrow "10th"
		  cbnthTime.rowTag(4)=10
		  
		  ResultType.listindex=0
		  Rounding.listindex=0
		  puStartType.ListIndex=0
		  cbnthTime.ListIndex=0
		  AssignedStart.value=true
		  
		  recsRS=App.theDB.DBSQLSelect("Select * from races where RaceID="+Format(app.RaceID,"##########"))
		  if not(recsRS.eof) then
		    RaceID=app.RaceID
		    RaceName.text=recsRS.Field("Race_Name").stringValue
		    RaceDateInput.text=left(recsRS.Field("Race_Date").StringValue,10)
		    if RaceDateInput.text="0-00-00" then RaceDateInput.text="0000-00-00"
		    RaceTime.text=recsRS.Field("Start_Time").stringValue
		    RaceAgeDate.text=left(recsRS.Field("Race_Age_Date").stringValue,10)
		    if RaceAgeDate.text="0-00-00" then RaceAgeDate.text="0000-00-00"
		    MinTime.text = recsRS.Field("Minimum_Time").stringValue
		    
		    SMSMessagesSent.Text="Messages sent: "+recsRS.Field("SMSCount").StringValue
		    
		    for i=0 to cbTTStartInterval.ListCount-1
		      cbTTStartInterval.ListIndex=i
		      if cbTTStartInterval.Text = recsRS.Field("Start_Interval").StringValue+" sec" then
		        exit
		      end if
		    next
		    if i=cbTTStartInterval.ListCount then
		      cbTTStartInterval.ListIndex=0
		    end if
		    
		    for i=0 to puStartType.ListCount-1
		      puStartType.ListIndex=i
		      if puStartType.rowTag(i) = recsRS.Field("Racers_Per_Start").IntegerValue then
		        exit
		      end if
		    next
		    if i=puStartType.ListCount then
		      puStartType.ListIndex=0
		    end if
		    
		    for i=0 to ResultType.ListCount-1
		      ResultType.ListIndex=i
		      if ResultType.text = recsRS.Field("Display_Type").stringValue then
		        exit
		      end if
		    next
		    if i=ResultType.ListCount then
		      ResultType.ListIndex=0
		    end if
		    if ResultType.Text=wnd_List.constFISUSSAResultType then
		      bbImportParticipants.AddRow("USSA and FIS Points")
		      bbExport.AddRow("USSA Points Export")
		    end if
		    
		    for i=0 to Rounding.ListCount-1
		      Rounding.ListIndex=i
		      if str(Rounding.rowTag(i)) = recsRS.Field("Places_To_Truncate").stringValue then
		        exit
		      end if
		    next
		    if i=Rounding.ListCount then
		      Rounding.ListIndex=0
		    end if
		    
		    for i=0 to cbnthTime.ListCount-1
		      cbnthTime.ListIndex=i
		      if cbnthTime.RowTag(i) = recsRS.Field("nthTime").StringValue then
		        exit
		      end if
		    next
		    if i=cbnthTime.ListCount then
		      cbnthTime.ListIndex=0
		    end if
		    
		    select case recsRS.Field("Calculate_Time_From").stringValue
		    case "Assigned"
		      AssignedStart.Value=true
		      ActualStart.value=false
		    case "Actual"
		      AssignedStart.Value=false
		      ActualStart.value=true
		      
		    case "Race"
		      AssignedStart.Value=false
		      ActualStart.value=false
		      
		    else
		      AssignedStart.Value=true
		      ActualStart.value=false
		    end select
		    
		    if recsRS.Field("Allow_Early_Start").stringValue ="Y" then
		      AllowEarlyStarts.value=true
		    else
		      AllowEarlyStarts.value=false
		    end if
		    
		    if recsRS.Field("Jam_Session").stringValue ="Y" then
		      cbJam.value=true
		    else
		      cbJam.value=false
		    end if
		    
		    if recsRS.Field("Logo1").StringValue<>"" then
		      f=GetFolderItem(recsRS.Field("Logo1").StringValue,FolderItem.PathTypeAbsolute)
		      if f <> nil and f.Exists then
		        Logo1=recsRS.Field("Logo1").StringValue
		        iwLogo1.Image=ScalePicture(f.OpenAsPicture,100,100)
		      else
		        Logo1=""
		        MsgBox("Logo 1 is no longer available. Please locate Logo 1 if you want it to print on reports.")
		      end if
		    end if
		    
		    if recsRS.Field("Logo2").StringValue<>"" then
		      f=GetFolderItem(recsRS.Field("Logo2").StringValue,FolderItem.PathTypeAbsolute)
		      if f <> nil and f.Exists then
		        Logo2=recsRS.Field("Logo2").StringValue
		        iwLogo2.Image=ScalePicture(f.OpenAsPicture,100,100)
		      else
		        Logo2=""
		        MsgBox("Logo 2 is no longer available. Please locate Logo 2 if you want it to print on reports.")
		      end if
		    end if
		    
		    if recsRS.Field("Logo3").StringValue<>"" then
		      f=GetFolderItem(recsRS.Field("Logo3").StringValue,FolderItem.PathTypeAbsolute)
		      if f <> nil and f.Exists then
		        Logo3=recsRS.Field("Logo3").StringValue
		        iwLogo3.Image=ScalePicture(f.OpenAsPicture,100,100)
		      else
		        Logo3=""
		        MsgBox("Logo 3 is no longer available. Please locate Logo 3 if you want it to print on reports.")
		      end if
		    end if
		    
		    if recsRS.Field("Logo4").StringValue<>"" then
		      f=GetFolderItem(recsRS.Field("Logo4").StringValue,FolderItem.PathTypeAbsolute)
		      if f <> nil and f.Exists then
		        Logo4=recsRS.Field("Logo4").StringValue
		        iwLogo4.Image=ScalePicture(f.OpenAsPicture,100,100)
		      else
		        Logo4=""
		        MsgBox("Logo 4 is no longer available. Please locate Logo 4 if you want it to print on reports.")
		      end if
		    end if
		    newRace=false
		    
		    tfTDNumber.Text=recsRS.Field("TD_Number").StringValue
		    tfTDName.Text=recsRS.Field("TD_Name").StringValue
		    tfTDDiv.Text=recsRS.Field("TD_Div").StringValue
		    tfATDNumber.Text=recsRS.Field("ATD_Number").StringValue
		    tfATDName.Text=recsRS.Field("ATD_Name").StringValue
		    tfATDDiv.Text=recsRS.Field("ATD_Div").StringValue
		    tfChiefOfCourse.Text=recsRS.Field("CoC_Name").StringValue
		    tfChiefOfCourseDiv.Text=recsRS.Field("CoC_Div").StringValue
		    tfJuryMember4.Text=recsRS.Field("Jury_Member_4_Name").StringValue
		    tfJuryMember4Div.Text=recsRS.Field("Jury_Member_4_Div").StringValue
		    tfJuryMember5.Text=recsRS.Field("Jury_Member_5_Name").StringValue
		    tfJuryMember5Div.Text=recsRS.Field("Jury_Member_5_Div").StringValue
		    
		    tfLapLength.Text=recsRS.Field("Lap_Length").StringValue
		    tfHD.Text=recsRS.Field("HD").StringValue
		    tfMC.Text=recsRS.Field("MC").StringValue
		    tfTC.Text=recsRS.Field("TC").StringValue
		    tfWX.Text=recsRS.Field("WX").StringValue
		    
		    tfNGB1.Text=recsRS.Field("NGB1").StringValue
		    tfNGB2.Text=recsRS.Field("NGB2").StringValue
		    
		    APRaceID.Text=recsRS.Field("AthletePathRaceID").StringValue
		    
		    pmFValue.AddRow "Select F-Value...   "
		    pmFValue.RowTag(0)=0
		    pmFValue.AddRow "Mass Start (1400)"
		    pmFValue.RowTag(1)=1400
		    pmFValue.AddRow "Pursuit with no break (1400)"
		    pmFValue.RowTag(2)=1400
		    pmFValue.AddRow "Old Pursuit with break (1200)"
		    pmFValue.RowTag(3)=1200
		    pmFValue.AddRow "Sprint"
		    pmFValue.RowTag(4)=1200
		    pmFValue.AddRow "Interval Start"
		    pmFValue.RowTag(5)=800
		    
		    pmMinPoints.AddRow "Select an Event Category..."
		    pmMinPoints.AddRow "WC, Olympics, WC"
		    pmMinPoints.AddRow "CC, NCAA Champs, AB, SuperTour"
		    pmMinPoints.AddRow "US Champs"
		    pmMinPoints.AddRow "U23 WC"
		    pmMinPoints.AddRow "World Jr Champs"
		    pmMinPoints.AddRow "JOs"
		    pmMinPoints.AddRow "Regional, NCAA Regional"
		    
		    pmUP.AddRow "Select a Points Distance..."
		    pmUP.AddRow "Sprint"
		    pmUP.AddRow "Distance"
		    pmUP.AddRow "Overall/Distance"
		    
		    for i=0 to pmFValue.ListCount-1
		      pmFValue.ListIndex=i
		      if pmFValue.text = recsRS.Field("FValue").stringValue then
		        exit
		      end if
		    next
		    if i=pmFValue.ListCount then
		      pmFValue.ListIndex=0
		    end if
		    
		  else
		    RaceID=1
		    RaceName.text=""
		    RaceDateInput.text="0000-00-00"
		    RaceTime.text="0000-00-00 00:00:00.000"
		    RaceAgeDate.text="0000-00-00"
		    MinTime.text = "00:00:00.000"
		    cbJam.Value=false
		    newRace=true
		  end if
		  
		  recsRS=app.theDB.DBSQLSelect("SELECT RaceDistance_Name,Actual_Distance,Pace_Type,rowid FROM racedistances WHERE RaceID = 1")
		  RaceTab_DistancesListBox.DeleteAllRows
		  while not recsRS.EOF
		    RaceTab_DistancesListBox.AddRow ""
		    RaceTab_DistancesListBox.Cell(RaceTab_DistancesListBox.lastindex,0)=recsRS.Field("rowid").stringValue
		    RaceTab_DistancesListBox.Cell(RaceTab_DistancesListBox.lastindex,1)=recsRS.Field("RaceDistance_Name").stringValue
		    RaceTab_DistancesListBox.Cell(RaceTab_DistancesListBox.lastindex,2)=str(recsRS.Field("Actual_Distance").DoubleValue)
		    RaceTab_DistancesListBox.Cell(RaceTab_DistancesListBox.lastindex,3)=recsRS.Field("Pace_Type").stringValue
		    recsRS.moveNext
		  wend
		  recsRS.Close
		  RaceTab_DistancesListBox.columncount=4
		  RaceTab_DistancesListBox.columnwidths="0,33%,33%,33%"
		  RaceTab_DistancesListBox.heading(1)="Name"
		  RaceTab_DistancesListBox.heading(2)="Actual Distance"
		  RaceTab_DistancesListBox.heading(3)="Pace Type"
		  RaceTab_DistancesListBox.ColumnAlignment(1)=ListBox.AlignLeft
		  RaceTab_DistancesListBox.ColumnAlignment(2)=ListBox.AlignLeft
		  RaceTab_DistancesListBox.ColumnAlignment(2)=ListBox.Alignleft
		  
		  UpdateSpecialMenu
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PostResultstoWeb()
		  dim rsResults, rsIntervals, rsTX, rsDivisions, rsParticipants, rsRaceDistances, rsTimes as RecordSet
		  dim IntervalIndex, DivisionIndex, ParticipantsIndex, RaceDistanceIndex, TimeIndex as Integer
		  dim StageID, ResultsRaceDistanceID, ResultsDivisionID, ResultsIntervalID, ResultsParticipantID as String
		  dim IntervalName, IntervalNumber, SearchTime, IntervalPace, TwitterUpdate as String
		  dim AssignedStart, ActualStop, Adjustment, TotalTime as double
		  
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  Dim Proceed, Update as Boolean
		  
		  dim i As integer
		  Dim LocalSQLStatement, ResultsRaceID, SQLStatement as String
		  
		  Update=true
		  
		  'Connect to mySQL database
		  theResultsDB=New MySQLCommunityServer
		  ConnectToResultsDB
		  If theResultsDB.Connect then
		    'Find race based on date and name
		    SQLStatement="SELECT id, name, race_date FROM races WHERE Name = '"+RaceName.text+"' AND race_date = '"+RaceDateInput.text+"'"
		    rsResults=theResultsDB.SQLSelect(SQLStatement)
		    
		    if rsResults.EOF then
		      ' create race
		      d.icon=3
		      d.Message="Create and upload results for: "+RaceName.text
		      d.Explanation=RaceName.Text+" did on exist on the server. Click OK to create and upload the race."
		      d.CancelButton.Visible=True
		      b=d.ShowModal
		      if b=d.ActionButton then
		        Progress.Show
		        'Add the race record
		        SQLStatement = "INSERT INTO stages (timer_id) VALUES ('1')"
		        theResultsDB.SQLExecute(SQLStatement)
		        StageID=str(theResultsDB.GetInsertID)
		        
		        SQLStatement = "INSERT INTO races (stage_id,name,race_date,publish,race_year,display_type,email) VALUES ("
		        SQLStatement = SQLStatement + "'" + StageID+"', "
		        SQLStatement = SQLStatement + "'" + RaceName.Text+"', "
		        SQLStatement = SQLStatement + "'" + RaceDateInput.Text+"', "
		        SQLStatement = SQLStatement + "'Y', "
		        SQLStatement = SQLStatement + "'" + left(RaceDateInput.text,4)+"', "
		        SQLStatement = SQLStatement + "'" + app.ResultsDisplayType+"', "
		        SQLStatement = SQLStatement + "'" + app.TimerEmail+"')"
		        theResultsDB.SQLExecute(SQLStatement)
		        ResultsRaceID=str(theResultsDB.GetInsertID)
		        
		        Proceed=true
		        Update=false 'not an update, but a new race 
		      else
		        Proceed=false
		      end if
		    else
		      
		      ' Confirm upload to the race found
		      d.icon=3
		      d.Message="Upload Results for: "+rsResults.Field("name").StringValue+" ("+rsResults.Field("race_date").StringValue+")"
		      d.Explanation="This action will replace all data on the server with the data from this race."
		      d.CancelButton.Visible=true
		      b=d.ShowModal
		      if b=d.ActionButton then
		        ResultsRaceID=rsResults.Field("id").StringValue
		        Progress.Show
		        
		        '  Delete existing race data
		        SQLStatement="SELECT id, name FROM race_distances WHERE race_id="+rsResults.Field("id").StringValue
		        rsRaceDistances=theResultsDB.SQLSelect(SQLStatement)
		        for RaceDistanceIndex= 1 to rsRaceDistances.RecordCount
		          
		          SQLStatement="SELECT id FROM divisions WHERE race_distance_id="+rsRaceDistances.Field("id").StringValue
		          rsDivisions=theResultsDB.SQLSelect(SQLStatement)
		          
		          for DivisionIndex= 1 to rsDivisions.RecordCount
		            
		            SQLStatement="SELECT id FROM participants WHERE division_id="+rsDivisions.Field("id").StringValue
		            rsParticipants=theResultsDB.SQLSelect(SQLStatement)
		            
		            Progress.Initialize("Progress","Clearing records for "+rsRaceDistances.Field("name").StringValue+" race distance...",rsDivisions.RecordCount,rsParticipants.RecordCount)
		            Progress.UpdateProg1(DivisionIndex)
		            
		            for ParticipantsIndex= 1 to rsParticipants.RecordCount
		              Progress.UpdateProg2(ParticipantsIndex)
		              
		              SQLStatement = "DELETE FROM times WHERE participant_id="+rsParticipants.Field("id").StringValue
		              theResultsDB.SQLExecute(SQLStatement)
		              
		              SQLStatement = "DELETE FROM participants WHERE id="+rsParticipants.Field("id").StringValue
		              theResultsDB.SQLExecute(SQLStatement)
		              rsParticipants.MoveNext
		            next
		            
		            SQLStatement="SELECT id FROM intervals WHERE division_id="+rsDivisions.Field("id").StringValue
		            rsIntervals=theResultsDB.SQLSelect(SQLStatement)
		            for IntervalIndex= 1 to rsIntervals.RecordCount
		              SQLStatement = "DELETE FROM intervals WHERE id="+rsIntervals.Field("id").StringValue
		              theResultsDB.SQLExecute(SQLStatement)
		              rsIntervals.MoveNext
		            next
		            
		            SQLStatement = "DELETE FROM divisions WHERE id="+rsDivisions.Field("id").StringValue
		            theResultsDB.SQLExecute(SQLStatement)
		            rsDivisions.MoveNext
		          next
		          SQLStatement = "DELETE FROM race_distances WHERE id="+rsRaceDistances.Field("id").StringValue
		          theResultsDB.SQLExecute(SQLStatement)
		          
		          
		          rsRaceDistances.MoveNext
		        next
		        
		        Proceed=true
		      else
		        Proceed=false
		      end if
		    end if
		    
		    if Proceed then
		      'add the race distance records
		      
		      LocalSQLStatement="SELECT rowid, RaceDistance_Name FROM raceDistances"
		      rsRaceDistances=app.theDB.DBSQLSelect(LocalSQLStatement)
		      for RaceDistanceIndex = 1 to rsRaceDistances.RecordCount
		        SQLStatement="INSERT INTO race_distances (race_id, name) VALUES ('"+ResultsRaceID+"', '"+rsRaceDistances.Field("RaceDistance_Name").StringValue+"')"
		        theResultsDB.SQLExecute(SQLStatement)
		        ResultsRaceDistanceID=str(theResultsDB.GetInsertID)
		        
		        LocalSQLStatement="SELECT rowid, Division_Name FROM divisions WHERE RaceDistanceID="+rsRaceDistances.Field("rowid").StringValue
		        rsDivisions=app.theDB.DBSQLSelect(LocalSQLStatement)
		        for DivisionIndex = 1 to rsDivisions.RecordCount
		          SQLStatement="INSERT INTO divisions (race_distance_id, name) VALUES ('"+ResultsRaceDistanceID+"', '"+rsDivisions.Field("Division_Name").StringValue+"')"
		          theResultsDB.SQLExecute(SQLStatement)
		          ResultsDivisionID=str(theResultsDB.GetInsertID)
		          
		          LocalSQLStatement="SELECT rowid, Interval_Name, Number FROM intervals WHERE DivisionID="+rsDivisions.Field("rowid").StringValue
		          rsIntervals=app.theDB.DBSQLSelect(LocalSQLStatement)
		          for IntervalIndex = 1 to rsIntervals.RecordCount
		            SQLStatement="INSERT INTO intervals (division_id, name, interval_number) VALUES ('"+ResultsDivisionID+"', '"
		            SQLStatement=SQLStatement+rsIntervals.Field("Interval_Name").StringValue+"', '"+rsIntervals.Field("Number").StringValue+"')"
		            theResultsDB.SQLExecute(SQLStatement)
		            rsIntervals.MoveNext
		          next
		          
		          LocalSQLStatement="SELECT participants.rowid, participants.DivisionID, participants.Racer_Number, "
		          LocalSQLStatement=LocalSQLStatement+"participants.Participant_Name, participants.First_Name, "
		          LocalSQLStatement=LocalSQLStatement+"participants.Gender, participants.Age, participants.Representing, "
		          LocalSQLStatement=LocalSQLStatement+"participants.DNS, participants.DNF, participants.DQ, "
		          LocalSQLStatement=LocalSQLStatement+"participants.Assigned_Start, participants.Actual_Start, "
		          LocalSQLStatement=LocalSQLStatement+"participants.Actual_Stop, participants.Total_Time, "
		          LocalSQLStatement=LocalSQLStatement+"participants.Net_Time, participants.Laps_Completed, "
		          LocalSQLStatement=LocalSQLStatement+"participants.Total_Adjustment, participants.Adjustment_Reason "
		          LocalSQLStatement=LocalSQLStatement+"FROM participants WHERE DivisionID="+rsDivisions.Field("rowid").StringValue
		          rsParticipants=app.theDB.DBSQLSelect(LocalSQLStatement)
		          
		          
		          Progress.Initialize("Progress","Uploading records for "+rsRaceDistances.Field("RaceDistance_Name").StringValue+" race distance...",rsDivisions.RecordCount,rsParticipants.RecordCount)
		          Progress.UpdateProg1(DivisionIndex)
		          
		          
		          for ParticipantsIndex = 1 to rsParticipants.RecordCount
		            Progress.UpdateProg2(ParticipantsIndex)
		            
		            SQLStatement="INSERT INTO participants (division_id, racer_number, last_name, "
		            SQLStatement=SQLStatement+"first_name, gender, age, representing, race_start, assigned_start, actual_start, stop_time, net_time, total_time, final_time, laps_completed, "
		            SQLStatement=SQLStatement+"overall_place, overall_back, gender_place, gender_back, division_place, division_back, pace, total_adjustment, adjustment_reason) "
		            SQLStatement=SQLStatement+"VALUES "
		            SQLStatement=SQLStatement+"("+ResultsDivisionID+", "
		            SQLStatement=SQLStatement+rsParticipants.Field("Racer_Number").StringValue+", "
		            SQLStatement=SQLStatement+"'" + rsParticipants.Field("Participant_Name").StringValue+"', "
		            SQLStatement=SQLStatement+"'" + rsParticipants.Field("First_Name").StringValue+"', "
		            SQLStatement=SQLStatement+"'" + rsParticipants.Field("Gender").StringValue+"', "
		            SQLStatement=SQLStatement+rsParticipants.Field("Age").StringValue+", "
		            SQLStatement=SQLStatement+"'" + rsParticipants.Field("Representing").StringValue+"', "
		            SQLStatement=SQLStatement+"'" + right(app.RaceStartTime, 12) +"', "
		            SQLStatement=SQLStatement+"'" + right(rsParticipants.Field("Assigned_Start").StringValue,12)+"', "
		            
		            if rsParticipants.Field("DQ").StringValue="Y" or rsParticipants.Field("DNS").StringValue="Y" or rsParticipants.Field("DNF").StringValue="Y" then
		              SQLStatement=SQLStatement+"'" + right(rsParticipants.Field("Actual_Start").StringValue,12)+"', "
		              SQLStatement=SQLStatement+"'" + right(rsParticipants.Field("Actual_Stop").StringValue,12)+"', "
		              if rsParticipants.Field("DQ").StringValue="Y"  then
		                SQLStatement=SQLStatement+"'DQ', " 'net time
		                SQLStatement=SQLStatement+"'DQ', " 'total time
		                SQLStatement=SQLStatement+"'DQ', " 'final time
		              elseif rsParticipants.Field("DNS").StringValue="Y"  then
		                SQLStatement=SQLStatement+"'DNS', " 'net time
		                SQLStatement=SQLStatement+"'DNS', " 'total time
		                SQLStatement=SQLStatement+"'DNS', " 'final time
		              elseif rsParticipants.Field("DNF").StringValue="Y" then
		                SQLStatement=SQLStatement+"'DNF', " 'net time
		                SQLStatement=SQLStatement+"'DNF', " 'total time
		                SQLStatement=SQLStatement+"'DNF', " 'final time
		              end if
		              SQLStatement=SQLStatement+"'0', " 'laps completed
		              
		              SQLStatement=SQLStatement+"'0', " 'overall
		              SQLStatement=SQLStatement+"'0', "
		              
		              SQLStatement=SQLStatement+"'0', " 'gender
		              SQLStatement=SQLStatement+"'0', "
		              
		              SQLStatement=SQLStatement+"'0', " 'division
		              SQLStatement=SQLStatement+"'0', "
		              
		              SQLStatement=SQLStatement+"'0:00.0', " 'pace
		              
		              SQLStatement=SQLStatement+"'+00:00:00.000', " 'adjustment
		              SQLStatement=SQLStatement+"'" + rsParticipants.Field("Adjustment_Reason").StringValue+"')"
		            else
		              SQLStatement=SQLStatement+"'" + right(rsParticipants.Field("Actual_Start").StringValue,12)+"', "
		              SQLStatement=SQLStatement+"'" + right(rsParticipants.Field("Actual_Stop").StringValue,12)+"', "
		              SQLStatement=SQLStatement+"'" + rsParticipants.Field("Net_Time").StringValue+"', "  ' chip time
		              
		              'gun time
		              AssignedStart=app.ConvertTimeToSeconds(rsParticipants.Field("Assigned_Start").stringValue)
		              ActualStop=app.ConvertTimeToSeconds(rsParticipants.Field("Actual_Stop").StringValue)
		              Adjustment=app.ConvertTimeToSeconds(rsParticipants.Field("Total_Adjustment").stringValue)
		              TotalTime=round(((ActualStop-AssignedStart)+Adjustment)*1000)/1000
		              SQLStatement=SQLStatement+"'" + app.TruncateTime(app.ConvertSecondsToTime(TotalTime,false))+"', "
		              
		              SQLStatement=SQLStatement+"'" + rsParticipants.Field("Total_Time").StringValue+"', " 'total time
		              
		              if instr(puStartType.text,"Criterium")>0 then
		                SQLStatement=SQLStatement+"'" + rsParticipants.Field("Laps_Completed").StringValue+"', "
		              else
		                SQLStatement=SQLStatement+"'0',"
		              end if
		              
		              SQLStatement=SQLStatement+"'" + app.GetPlace("Overall",rsRaceDistances.Field("rowid").StringValue,rsDivisions.Field("rowid").StringValue, rsParticipants.Field("Gender").StringValue,rsParticipants.Field("Total_Time").StringValue,false,rsParticipants.Field("Laps_Completed").StringValue)+"', "
		              SQLStatement=SQLStatement+"'" + app.GetBack("Overall",rsRaceDistances.Field("rowid").StringValue,rsDivisions.Field("rowid").StringValue, rsParticipants.Field("Gender").StringValue,rsParticipants.Field("Total_Time").StringValue)+"', "
		              
		              SQLStatement=SQLStatement+"'" + app.GetPlace("Gender",rsRaceDistances.Field("rowid").StringValue,rsDivisions.Field("rowid").StringValue, rsParticipants.Field("Gender").StringValue,rsParticipants.Field("Total_Time").StringValue,false,rsParticipants.Field("Laps_Completed").StringValue)+"', "
		              SQLStatement=SQLStatement+"'" +app.GetBack("Gender",rsRaceDistances.Field("rowid").StringValue,rsDivisions.Field("rowid").StringValue, rsParticipants.Field("Gender").StringValue,rsParticipants.Field("Total_Time").StringValue)+"', "
		              
		              SQLStatement=SQLStatement+"'" + app.GetPlace("Division",rsRaceDistances.Field("rowid").StringValue,rsDivisions.Field("rowid").StringValue, rsParticipants.Field("Gender").StringValue,rsParticipants.Field("Total_Time").StringValue,false,rsParticipants.Field("Laps_Completed").StringValue)+"', "
		              SQLStatement=SQLStatement+"'" + app.GetBack("Division",rsRaceDistances.Field("rowid").StringValue,rsDivisions.Field("rowid").StringValue, rsParticipants.Field("Gender").StringValue,rsParticipants.Field("Total_Time").StringValue)+"', "
		              
		              SQLStatement=SQLStatement+"'" + app.CalculatePace(rsRaceDistances.Field("rowid").StringValue,rsParticipants.Field("Total_Time").StringValue)+"', "
		              SQLStatement=SQLStatement+"'" + rsParticipants.Field("Total_Adjustment").StringValue+"', "
		              SQLStatement=SQLStatement+"'" + rsParticipants.Field("Adjustment_Reason").StringValue+"')"
		            end if
		            
		            theResultsDB.SQLExecute(SQLStatement)
		            ResultsParticipantID=str(theResultsDB.GetInsertID)
		            
		            LocalSQLStatement="SELECT times.ParticipantID, times.TX_Code, times.Interval_Number, "
		            LocalSQLStatement=LocalSQLStatement+"times.Interval_Time, times.Total_Time, teammembers.MemberName "
		            LocalSQLStatement=LocalSQLStatement+"FROM times LEFT OUTER JOIN teammembers ON times.TeamMemberID = teammembers.rowid "
		            LocalSQLStatement=LocalSQLStatement+"WHERE times.ParticipantID = "+rsParticipants.Field("rowid").StringValue+" AND "
		            LocalSQLStatement=LocalSQLStatement+"times.Use_This_Passing='Y' AND (times.Interval_Number > 0 AND times.Interval_Number < 9990 OR times.Interval_Number = 9999) "
		            LocalSQLStatement=LocalSQLStatement+"ORDER BY times.Interval_Number ASC"
		            rsTimes=app.theDB.DBSQLSelect(LocalSQLStatement)
		            for IntervalIndex = 1 to rsTimes.RecordCount
		              LocalSQLStatement="SELECT Interval_Name, Pace_Type, Actual_Distance FROM intervals WHERE DivisionID="+rsParticipants.Field("DivisionID").StringValue+" AND Number="+rsTimes.Field("Interval_Number").StringValue+" ORDER BY Number ASC"
		              rsIntervals=app.theDB.DBSQLSelect(LocalSQLStatement)
		              if not(rsIntervals.EOF) then
		                IntervalPace=app.CalculateIntPace(rsIntervals.Field("Pace_Type").StringValue,rsIntervals.Field("Actual_Distance").DoubleValue, rsTimes.Field("Interval_Time").StringValue)
		                IntervalName=rsIntervals.Field("Interval_Name").StringValue
		                if rsTimes.Field("MemberName").StringValue <> "" then
		                  IntervalName=IntervalName+": "+rsTimes.Field("MemberName").StringValue
		                end if
		              else
		                IntervalPace=""
		                IntervalName=""
		              end if
		              
		              SQLStatement="INSERT INTO times (participant_id, interval_number,"
		              SQLStatement=SQLStatement+"overall_place, overall_back, gender_place, gender_back, division_place, division_back, "
		              SQLStatement=SQLStatement+"interval_time, total_time, pace, team_member_name) VALUES "
		              
		              SQLStatement=SQLStatement+"("+ResultsParticipantID+", "
		              SQLStatement=SQLStatement+"'" + rsTimes.Field("Interval_Number").StringValue+"', "
		              
		              SQLStatement=SQLStatement+"'" + app.GetIntervalPlace("Overall",rsRaceDistances.Field("rowid").StringValue,rsDivisions.Field("rowid").StringValue, _
		              rsParticipants.Field("Gender").StringValue,rsTimes.Field("Interval_Number").StringValue,rsTimes.Field("Interval_Time").StringValue,"Interval Time",true)+"', "
		              SQLStatement=SQLStatement+"'" + app.GetIntervalBack("Overall",rsRaceDistances.Field("rowid").StringValue,rsDivisions.Field("rowid").StringValue, _
		              rsParticipants.Field("Gender").StringValue,rsTimes.Field("Interval_Number").StringValue,rsTimes.Field("Interval_Time").StringValue,false)+"', "
		              
		              SQLStatement=SQLStatement+"'" + app.GetIntervalPlace("Gender",rsRaceDistances.Field("rowid").StringValue,rsDivisions.Field("rowid").StringValue, _
		              rsParticipants.Field("Gender").StringValue,rsTimes.Field("Interval_Number").StringValue,rsTimes.Field("Interval_Time").StringValue,"Interval Time",true)+"', "
		              SQLStatement=SQLStatement+"'" + app.GetIntervalBack("Gender",rsRaceDistances.Field("rowid").StringValue,rsDivisions.Field("rowid").StringValue, _
		              rsParticipants.Field("Gender").StringValue,rsTimes.Field("Interval_Number").StringValue,rsTimes.Field("Interval_Time").StringValue,false)+"', "
		              
		              SQLStatement=SQLStatement+"'" + app.GetIntervalPlace("Division",rsRaceDistances.Field("rowid").StringValue,rsDivisions.Field("rowid").StringValue, _
		              rsParticipants.Field("Gender").StringValue,rsTimes.Field("Interval_Number").StringValue,rsTimes.Field("Interval_Time").StringValue,"Interval Time",true)+"', "
		              SQLStatement=SQLStatement+"'" + app.GetIntervalBack("Division",rsRaceDistances.Field("rowid").StringValue,rsDivisions.Field("rowid").StringValue, _
		              rsParticipants.Field("Gender").StringValue,rsTimes.Field("Interval_Number").StringValue,rsTimes.Field("Interval_Time").StringValue,false)+"', "
		              SQLStatement=SQLStatement+"'" + rsTimes.Field("Interval_Time").StringValue+"', "
		              SQLStatement=SQLStatement+"'" + rsTimes.Field("Total_Time").StringValue+"', "
		              SQLStatement=SQLStatement+"'" + IntervalPace +"', "
		              SQLStatement=SQLStatement+"'" + IntervalName+"') "
		              theResultsDB.SQLExecute(SQLStatement)
		              rsTimes.MoveNext
		            next
		            rsParticipants.MoveNext
		          next
		          rsDivisions.MoveNext
		        next
		        rsRaceDistances.MoveNext
		      next
		      theResultsDB.close
		      
		      TwitterUpdate="The results for the "+RaceName.Text+" have been "
		      if(Update) then
		        TwitterUpdate=TwitterUpdate+"updated"
		      else
		        TwitterUpdate=TwitterUpdate+"posted"
		      end if
		      TwitterUpdate=TwitterUpdate+". Check them out at http://www.milliseconds.com."
		      praemusCore.SendTweet(TwitterUpdate )
		      
		      Progress.Close
		    end if
		    
		  else
		    d.icon=1
		    d.Message="Connection to the results database failed."
		    d.Explanation="Please establish a connection to the Internet and try again."
		    b=d.ShowModal
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PostTXDatatoWeb()
		  dim theRentalDB as MySQLCommunityServer
		  dim rsRentals, rsLocal, rsParticipants, rsTX as RecordSet
		  dim IntervalName, ReservationID as String
		  Dim LocalSQLStatement, SQLStatement, InsertSQL as String
		  
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  Dim Proceed as Boolean
		  
		  InsertSQL="INSERT INTO transponders (ReservationID, TXCode, CheckOut, CheckIn, "
		  InsertSQL=InsertSQL+"RacerNumber, FirstName, LastName, Street, City, State, PostalCode, Country, Phone, EMail) "
		  
		  dim i As integer
		  
		  'Connect to mySQL database
		  theRentalDB=New MySQLCommunityServer
		  theRentalDB.host=app.WebServer
		  'theRentalDb.Host="127.0.0.1"
		  theRentalDB.port=3306
		  theRentalDB.databaseName="rentals"
		  theRentalDB.userName="milliseconds"
		  theRentalDB.Password="milli2nds"
		  If theRentalDB.Connect then
		    'Find race based on date and name
		    SQLStatement="SELECT ID, CustomerID, EventName, EventDate FROM tbl_reservations "
		    SQLStatement=SQLStatement+"WHERE (CustomerID = 12 OR CustomerID = 39 OR CustomerID = 40 OR CustomerID = 51 OR CustomerID = 45) AND "
		    SQLStatement=SQLStatement+"EventDate='"+app.RaceDate.SQLDate+"' AND EventName='"+RaceName.Text+"'"
		    rsRentals=theRentalDB.SQLSelect(SQLStatement)
		    
		    if rsRentals.EOF then
		      ' create race
		      d.icon=3
		      d.Message="Create and upload Rental Check-Out records for: "+RaceName.text
		      d.Explanation=RaceName.Text+" did on exist on the server. Click OK to create and upload the Rental Check-Out records."
		      d.CancelButton.Visible=True
		      b=d.ShowModal
		      if b=d.ActionButton then
		        Progress.Show
		        Progress.Initialize("Uploading check-out records","Race Name...",4,0)
		        Progress.UpdateProg1(1)
		        Progress.setMax2(4)
		        
		        'Add the race record
		        SQLStatement = "INSERT INTO tbl_reservations (CustomerID,Complete,EventName,EventDate,RequiredDate,NumTXRequired) VALUES ("
		        SQLStatement = SQLStatement + "'12', "
		        SQLStatement = SQLStatement + "'', "
		        SQLStatement = SQLStatement + "'" + RaceName.Text+"', "
		        SQLStatement = SQLStatement + "'" + RaceDateInput.Text+"', "
		        SQLStatement = SQLStatement + "'" + RaceDateInput.Text+"', "
		        SQLStatement = SQLStatement + "')"
		        theRentalDB.SQLExecute(SQLStatement)
		        ReservationID=str(theRentalDB.GetInsertID)
		        Progress.UpdateProg2(4)
		        
		        Proceed=true
		      else
		        Proceed=false
		      end if
		    else
		      
		      ' Confirm upload to the race found
		      d.icon=3
		      d.Message="Upload Check-Out Records for: "+rsRentals.Field("EventName").StringValue+" ("+rsRentals.Field("EventDate").StringValue+")"
		      d.Explanation="This action will update the check-out records on the server with the data from this race."
		      d.CancelButton.Visible=true
		      b=d.ShowModal
		      if b=d.ActionButton then
		        ReservationID=rsRentals.Field("ID").StringValue
		        Progress.Show
		        Progress.Initialize("Uploading Check-Out Records","Race Name...",4,0)
		        Progress.UpdateProg1(1)
		        Progress.setMax2(4)
		        
		        Proceed=true
		      else
		        Proceed=false
		      end if
		    end if
		    
		    if Proceed then
		      
		      'add the participant records
		      LocalSQLStatement="SELECT Racer_Number, TX_Code, Issued, Returned FROM transponders ORDER BY TX_Code"
		      rsLocal=app.theDB.DBSQLSelect(LocalSQLStatement)
		      
		      Progress.UpdateProg1(3)
		      Progress.UpdateMessage("Check-Out records...")
		      Progress.setMax2(rslocal.RecordCount)
		      for i=1 to rsLocal.RecordCount
		        if rsLocal.Field("TX_Code").StringValue<>"" then
		          
		          SQLStatement="SELECT ID, CheckIn FROM tbl_transponders WHERE ReservationID = "+ReservationID+" AND TXCode='"+rsLocal.Field("TX_Code").StringValue+"'"
		          rsTX=theRentalDB.SQLSelect(SQLStatement)
		          
		          if rsTX.EOF then
		            InsertSQL="INSERT INTO tbl_transponders (ReservationID, TXCode, CheckOut, CheckIn, RacerNumber, FirstName, LastName, Street, City, State, PostalCode, Country, Phone, EMail) VALUES ("
		            InsertSQL=InsertSQL+ReservationID+", "
		            InsertSQL=InsertSQL+"'"+rsLocal.Field("TX_Code").StringValue+"', "
		            InsertSQL=InsertSQL+"'"+rsLocal.Field("Issued").StringValue+"', "
		            if rsLocal.Field("Returned").StringValue<>"0-00-00 00:00:00" AND rsLocal.Field("Returned").StringValue<>"0000-00-00 00:00:00" then
		              InsertSQL=InsertSQL+"'"+rsLocal.Field("Returned").StringValue+"', "
		            else
		              'Check to see if it has been checked out again after it was checked out for this race
		              SQLStatement="SELECT CheckOut FROM tbl_transponders WHERE CheckOut >= '"+rsLocal.Field("Issued").StringValue+"' AND TXCode='"+rsLocal.Field("TX_Code").StringValue+"'"
		              rsTX=theRentalDB.SQLSelect(SQLStatement)
		              if not(rsTX.EOF) then
		                InsertSQL=InsertSQL+"'"+rsTX.Field("CheckOut").StringValue+"', "
		              else
		                InsertSQL=InsertSQL+"'0000-00-00 00:00:00', "
		              end if
		            end if
		            
		            SQLStatement="SELECT Racer_Number, First_Name, Participant_Name, Street, City, State, Postal_Code, Country, Phone, EMail FROM participants WHERE Racer_Number="+rsLocal.Field("Racer_Number").StringValue
		            rsParticipants=app.theDB.DBSQLSelect(SQLStatement)
		            
		            InsertSQL=InsertSQL+"'"+rsLocal.Field("Racer_Number").StringValue+"', "
		            InsertSQL=InsertSQL+"'"+rsParticipants.Field("First_Name").StringValue+"', "
		            InsertSQL=InsertSQL+"'"+rsParticipants.Field("Participant_Name").StringValue+"', "
		            InsertSQL=InsertSQL+"'"+rsParticipants.Field("Street").StringValue+"', "
		            InsertSQL=InsertSQL+"'"+rsParticipants.Field("City").StringValue+"', "
		            InsertSQL=InsertSQL+"'"+rsParticipants.Field("State").StringValue+"', "
		            InsertSQL=InsertSQL+"'"+rsParticipants.Field("Postal_Code").StringValue+"', "
		            InsertSQL=InsertSQL+"'"+rsParticipants.Field("Country").StringValue+"', "
		            InsertSQL=InsertSQL+"'"+rsParticipants.Field("Phone").StringValue+"', "
		            InsertSQL=InsertSQL+"'"+rsParticipants.Field("EMail").StringValue+"')"
		            SQLStatement=InsertSQL
		            
		          else
		            
		            SQLStatement="SELECT Racer_Number, First_Name, Participant_Name, Street, City, State, Postal_Code, Country, Phone, EMail FROM participants WHERE Racer_Number="+rsLocal.Field("Racer_Number").StringValue
		            rsParticipants=app.theDB.DBSQLSelect(SQLStatement)
		            
		            SQLStatement="UPDATE tbl_transponders SET "
		            SQLStatement=SQLStatement+"CheckOut='"+rsLocal.Field("Issued").StringValue+"'"
		            if right(rsTX.Field("CheckIn").StringValue,8)="00:00:00" AND rsLocal.Field("Returned").StringValue<>"0-00-00 00:00:00" AND rsLocal.Field("Returned").StringValue<>"0000-00-00 00:00:00" then
		              SQLStatement=SQLStatement+", CheckIn='"+rsLocal.Field("Returned").StringValue+"'"
		            end if
		            
		            if rsParticipants.RecordCount>0 then
		              SQLStatement=SQLStatement+", RacerNumber='"+rsParticipants.Field("Racer_Number").StringValue+"', "
		              SQLStatement=SQLStatement+"FirstName='"+rsParticipants.Field("First_Name").StringValue+"', "
		              SQLStatement=SQLStatement+"LastName='"+rsParticipants.Field("Participant_Name").StringValue+"', "
		              SQLStatement=SQLStatement+"Street='"+rsParticipants.Field("Street").StringValue+"', "
		              SQLStatement=SQLStatement+"City='"+rsParticipants.Field("City").StringValue+"', "
		              SQLStatement=SQLStatement+"State='"+rsParticipants.Field("State").StringValue+"', "
		              SQLStatement=SQLStatement+"PostalCode='"+rsParticipants.Field("Postal_Code").StringValue+"', "
		              SQLStatement=SQLStatement+"Country='"+rsParticipants.Field("Country").StringValue+"', "
		              SQLStatement=SQLStatement+"Phone='"+rsParticipants.Field("Phone").StringValue+"', "
		              SQLStatement=SQLStatement+"Email='"+rsParticipants.Field("EMail").StringValue+"'"
		            end if
		            SQLStatement=SQLStatement+" WHERE ID="+rsTX.Field("ID").StringValue
		          end if
		          theRentalDB.SQLExecute(SQLStatement)
		        end if
		        rsLocal.MoveNext
		        Progress.UpdateProg2(i)
		      next
		      theRentalDB.close
		      Progress.Close
		    end if
		    
		  else
		    d.icon=1
		    d.Message="Connection to the rental database failed."
		    d.Explanation="Please establish a connection to the Internet and try again."
		    b=d.ShowModal
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RecalcAge()
		  Dim BirthDate as Date
		  Dim Age, i as Integer
		  Dim rsParticipants, rsDivisions as RecordSet
		  Dim DistanceIDStg, DivisionIDStg,SQLStatement as string
		  
		  
		  
		  SQLStatement="SELECT participants.rowid, divisions.Import_Name, participants.Birth_Date, participants.Gender, participants.Age FROM participants LEFT OUTER JOIN divisions ON participants.DivisionID = divisions.rowid"
		  SQLStatement=SQLStatement+" WHERE participants.RaceID =1"
		  rsParticipants=App.theDB.DBSQLSelect(SQLStatement)
		  
		  Progress.Show
		  Progress.Initialize("Progress","Recalculating Ages...",rsParticipants.RecordCount,-1)
		  
		  while not rsParticipants.EOF
		    i=i+1
		    
		    if rsParticipants.Field("Birth_Date").StringValue<>"0000-00-00" then
		      BirthDate=new date
		      BirthDate.SQLDate=rsParticipants.Field("Birth_Date").StringValue
		      
		      Age=app.CalculateAge(BirthDate,app.RaceAgeDate)
		    else
		      Age=rsParticipants.Field("Age").IntegerValue
		    end if 
		    
		    'Find which division
		    SQLStatement="Select RaceDistanceID, rowid FROM divisions WHERE Low_Age<="+str(Age)+" AND High_Age>="+str(Age)
		    SQLStatement=SQLStatement+" AND Gender='"+rsParticipants.Field("Gender").StringValue+"' AND Import_Name = '"+rsParticipants.Field("Import_Name").StringValue+"'"
		    rsDivisions=app.theDB.DBSQLSelect(SQLStatement)
		    if not rsDivisions.EOF then
		      DistanceIDStg=rsDivisions.Field("RaceDistanceID").StringValue
		      DivisionIDStg=rsDivisions.Field("rowid").StringValue
		    else
		      DistanceIDStg="0"
		      DivisionIDStg="0"
		    end if
		    rsDivisions.Close
		    
		    SQLStatement="UPDATE participants SET RaceDistanceID="+DistanceIDStg+", DivisionID="+DivisionIDStg+", Age="+str(Age)+" WHERE rowid="+rsParticipants.Field("rowid").StringValue
		    App.theDB.DBSQLExecute(SQLStatement)
		    
		    rsParticipants.moveNext
		    Progress.UpdateProg1(i)
		  wend
		  rsParticipants.close
		  app.theDB.DBCommit
		  Progress.Close
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RecalcTimes()
		  
		  dim res, UpdateIntervalNumbers, Proceed as Boolean
		  dim i, NewIntervalNumber, IntervalCount as integer
		  dim rsParticipants, rsIntervals, rsTimes, rsDistance as RecordSet
		  dim IntervalTime, NewTotalTime, NewNetTime, PreviousTime, SQLStatement, NewAgeGrade as string
		  dim TempActualTime, TempActualStart as String
		  
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  Proceed=true
		  
		  d.icon=MessageDialog.GraphicQuestion   //display warning icon
		  d.ActionButton.Caption="Recalc Only"
		  d.CancelButton.Visible= True     //show the Cancel button
		  d.AlternateActionButton.Visible= True   //show the "Don't Save" button
		  d.AlternateActionButton.Caption="Recalc and Update"
		  d.Message="Do you want to recalculate times and update interval numbers?"
		  d.Explanation="Recalc and Update will attempt to sort the intervals and put them in thier proper order. May cause confusion if there are interval times missing or there are too many interval times."
		  
		  b=d.ShowModal     //display the dialog
		  Select Case b //determine which button was pressed.
		  Case d.ActionButton
		    UpdateIntervalNumbers=false
		  Case d.AlternateActionButton
		    UpdateIntervalNumbers=true
		  Case d.CancelButton
		    Proceed=false
		  End select
		  
		  if Proceed then
		    
		    i=0
		    SQLStatement="SELECT rowid, Actual_Start, Actual_Stop, DNF, DNS, Racer_Number "
		    SQLStatement=SQLStatement+"FROM participants WHERE RaceID =1 AND (Prevent_External_Updates <> 'Y' OR Prevent_External_Updates is null)"
		    rsParticipants=App.theDB.DBSQLSelect(SQLStatement)
		    
		    Progress.Initialize("Progress","Checking for Unassigned Start and Stop Times...",2,rsParticipants.RecordCount)
		    Progress.UpdateProg1(1)
		    Progress.UpdateProg2(0)
		    for i = 1 to rsParticipants.RecordCount
		      
		      if instr(rsParticipants.Field("Actual_Start").StringValue,"00:00:00.000")>0 then
		        SQLStatement="SELECT times.Actual_Time, times.rowid FROM transponders INNER JOIN times ON transponders.TX_Code = times.TX_Code"
		        SQLStatement=SQLStatement+" WHERE times.Interval_Number=0 AND transponders.Racer_Number="+rsParticipants.Field("Racer_Number").StringValue
		        SQLStatement=SQLStatement+" ORDER BY times.Actual_Time DESC"
		        rsTimes=App.theDB.DBSQLSelect(SQLStatement)
		        
		        if rsTimes.RecordCount>0 then
		          SQLStatement="UPDATE participants SET DNS='N', Actual_Start='"+rsTimes.Field("Actual_Time").StringValue
		          SQLStatement=SQLStatement+"' WHERE RaceID=1"
		          SQLStatement=SQLStatement+" AND rowid="+rsParticipants.Field("rowid").StringValue
		          App.theDB.DBSQLExecute(SQLStatement)
		          
		          SQLStatement="UPDATE times SET Use_This_Passing='Y', ParticipantID='"+rsParticipants.Field("rowid").StringValue
		          SQLStatement=SQLStatement+"' WHERE RaceID=1"
		          SQLStatement=SQLStatement+" AND rowid="+rsTimes.Field("rowid").StringValue
		          App.theDB.DBSQLExecute(SQLStatement)
		        end if
		      end if
		      
		      if instr(rsParticipants.Field("Actual_Stop").StringValue,"00:00:00.000")>0 then
		        SQLStatement="SELECT times.Actual_Time, times.rowid FROM transponders INNER JOIN times ON transponders.TX_Code = times.TX_Code"
		        SQLStatement=SQLStatement+" WHERE times.Interval_Number=9999 AND transponders.Racer_Number="+rsParticipants.Field("Racer_Number").StringValue
		        SQLStatement=SQLStatement+" ORDER BY times.Actual_Time ASC LIMIT "+str(app.nthTime-1)+",1"
		        rsTimes=App.theDB.DBSQLSelect(SQLStatement)
		        
		        if rsTimes.RecordCount>0 then
		          SQLStatement="UPDATE participants SET DNF='N', DNS='N', Actual_Stop='"+rsTimes.Field("Actual_Time").StringValue
		          SQLStatement=SQLStatement+"' WHERE RaceID=1"
		          SQLStatement=SQLStatement+" AND rowid="+rsParticipants.Field("rowid").StringValue
		          App.theDB.DBSQLExecute(SQLStatement)
		          
		          SQLStatement="UPDATE times SET Use_This_Passing='Y', ParticipantID='"+rsParticipants.Field("rowid").StringValue
		          SQLStatement=SQLStatement+"' WHERE RaceID=1"
		          SQLStatement=SQLStatement+" AND rowid="+rsTimes.Field("rowid").StringValue
		          App.theDB.DBSQLExecute(SQLStatement)
		        end if
		      end if
		      
		      rsParticipants.MoveNext
		      Progress.UpdateProg2(i)
		    next
		    
		    Progress.setMax2(rsParticipants.RecordCount)
		    Progress.UpdateMessage("Recalculating Times...")
		    Progress.UpdateProg1(2)
		    Progress.UpdateProg2(0)
		    
		    i=0
		    SQLStatement="SELECT rowid, Gender, Age, Assigned_Start, Actual_Start, Total_Adjustment, "
		    SQLStatement=SQLStatement+"Actual_Stop "
		    SQLStatement=SQLStatement+"FROM participants WHERE RaceID =1 AND (Prevent_External_Updates <> 'Y' or Prevent_External_Updates is null)"
		    rsParticipants=App.theDB.DBSQLSelect(SQLStatement)
		    
		    Progress.Show
		    Progress.Initialize("Progress","Recalculating Times...",rsParticipants.RecordCount,-1)
		    
		    while not rsParticipants.EOF
		      i=i+1
		      NewTotalTime=app.CalculateTotalTime(rsParticipants.Field("Assigned_Start").StringValue,rsParticipants.Field("Actual_Start").StringValue,rsParticipants.Field("Actual_Stop").StringValue,rsParticipants.Field("Total_Adjustment").StringValue,NewNetTime)
		      
		      SQLStatement="SELECT Age_Grade_Distance FROM divisions INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid INNER JOIN participants ON participants.DivisionID = divisions.rowid "
		      SQLStatement = SQLStatement + "WHERE participants.rowid ="+rsParticipants.Field("rowid").StringValue
		      rsDistance=app.theDB.DBSQLSelect(SQLStatement)
		      NewAgeGrade=app.CalculateAgeGrade(rsParticipants.Field("Gender").StringValue,rsParticipants.Field("Age").IntegerValue,rsDistance.Field("Age_Grade_Distance").StringValue,NewTotalTime)
		      
		      SQLStatement="UPDATE Participants SET"
		      SQLStatement=SQLStatement+" Total_Time='"+NewTotalTime+"',"
		      SQLStatement=SQLStatement+" Net_Time='"+NewNetTime+"',"
		      SQLStatement=SQLStatement+" Age_Grade='"+NewAgeGrade+"' "
		      SQLStatement=SQLStatement+" WHERE RaceID=1"
		      SQLStatement=SQLStatement+" AND rowid="+rsParticipants.Field("rowid").StringValue
		      App.theDB.DBSQLExecute(SQLStatement)
		      
		      if UpdateIntervalNumbers then
		        'Update the Interval Numbers
		        SQLStatement="SELECT Actual_Time, Interval_Number, rowid FROM times WHERE RaceID=1"
		        SQLStatement=SQLStatement+" AND ParticipantID="+rsParticipants.Field("rowid").StringValue
		        if app.RacersPerStart<>9998 then
		          SQLStatement=SQLStatement+" AND Interval_Number>0 AND Interval_Number<9999 "
		        else
		          SQLStatement=SQLStatement+" AND Interval_Number>0 "
		        end if
		        SQLStatement=SQLStatement+" ORDER BY Actual_Time ASC"
		        rsIntervals=App.theDB.DBSQLSelect(SQLStatement)
		        NewIntervalNumber=0
		        PreviousTime="0000-00-00 00:00:00.000"
		        IntervalCount=0
		        while not rsIntervals.EOF
		          IntervalCount=IntervalCount+1
		          if ((rsIntervals.Field("Actual_Time").StringValue > rsParticipants.Field("Assigned_Start").StringValue and app.CalculateTimesFrom="Assigned") _
		            or (rsIntervals.Field("Actual_Time").StringValue > rsParticipants.Field("Actual_Start").StringValue and app.CalculateTimesFrom="Actual")) then
		            if ((PreviousTime=rsIntervals.Field("Actual_Time").StringValue) or ((app.ConvertTimeToSeconds(rsIntervals.Field("Actual_Time").StringValue)-app.ConvertTimeToSeconds(PreviousTime))<app.MinimumLapTime)) then
		              if app.RacersPerStart=9998 and IntervalCount=rsIntervals.RecordCount and PreviousTime=rsIntervals.Field("Actual_Time").StringValue then
		                SQLStatement="UPDATE times SET Use_This_Passing='Y', Interval_Number="+str(NewIntervalNumber)
		              else
		                SQLStatement="UPDATE times SET Use_This_Passing='N'"
		              end if
		              SQLStatement=SQLStatement+" WHERE rowid="+rsIntervals.Field("rowid").StringValue
		              App.theDB.DBSQLExecute(SQLStatement)
		            else
		              NewIntervalNumber=NewIntervalNumber+1
		              SQLStatement="UPDATE times SET Use_This_Passing='Y', Interval_Number="+str(NewIntervalNumber)
		              SQLStatement=SQLStatement+" WHERE rowid="+rsIntervals.Field("rowid").StringValue
		              App.theDB.DBSQLExecute(SQLStatement)
		            end if
		            PreviousTime=rsIntervals.Field("Actual_Time").StringValue
		          else
		            SQLStatement="UPDATE times SET Use_This_Passing='N'"
		            SQLStatement=SQLStatement+" WHERE rowid="+rsIntervals.Field("rowid").StringValue
		            App.theDB.DBSQLExecute(SQLStatement)
		          end if
		          rsIntervals.MoveNext
		        wend
		        rsIntervals.Close
		        
		        SQLStatement="UPDATE Participants SET Laps_Completed="+Str(NewIntervalNumber)
		        SQLStatement=SQLStatement+" WHERE RaceID=1"
		        SQLStatement=SQLStatement+" AND rowid="+rsParticipants.Field("rowid").StringValue
		        App.theDB.DBSQLExecute(SQLStatement)
		        
		        if app.RacersPerStart=9998 then
		          SQLStatement="SELECT rowid FROM times WHERE Interval_Number="+Str(NewIntervalNumber)+" AND ParticipantID="+rsParticipants.Field("rowid").StringValue+" ORDER BY rowid DESC"
		          rsTimes=app.theDB.DBSQLSelect(SQLStatement)
		          
		          SQLStatement="UPDATE times SET Interval_Number=9999 WHERE rowid="+rsTimes.Field("rowid").StringValue
		          App.theDB.DBSQLExecute(SQLStatement)
		          rsTimes.Close
		        end if
		        
		        
		      end if
		      
		      'Update the times
		      RecalcTimes_UpdateIntervals(rsParticipants.Field("rowid").StringValue,rsParticipants.Field("Assigned_Start").StringValue,rsParticipants.Field("Actual_Start").StringValue)
		      
		      rsParticipants.moveNext
		      
		      Progress.UpdateProg2(i)
		    wend
		    
		    rsParticipants.close
		    app.theDB.DBCommit
		    Progress.Close
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RecalcTimes_UpdateIntervals(ParticipantID as String, AssignedStart as String, ActualStart as string)
		  dim rsParticipants, rsIntervals, rsTimes as RecordSet
		  dim IntervalTime, NewTotalTime, NewNetTime, PreviousTime, SQLStatement as string
		  dim OldActualTime as String
		  
		  if ParticipantID>"0" then
		    
		    SQLStatement="SELECT rowid, Actual_Time, Interval_Number, ParticipantID FROM times WHERE RaceID=1"
		    SQLStatement=SQLStatement+" AND ParticipantID="+ParticipantID
		    SQLStatement=SQLStatement+" AND Use_This_Passing='Y'"
		    SQLStatement=SQLStatement+" ORDER BY Interval_Number ASC"
		    rsIntervals=App.theDB.DBSQLSelect(SQLStatement)
		    PreviousTime="00:00:00.000"
		    OldActualTime=""
		    while not rsIntervals.EOF
		      if OldActualTime<rsIntervals.Field("Actual_Time").StringValue then
		        
		        if app.CalculateTimesFrom="Assigned" then
		          NewTotalTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(rsIntervals.Field("Actual_Time").StringValue)-App.ConvertTimeToSeconds(AssignedStart),false)
		        else
		          NewTotalTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(rsIntervals.Field("Actual_Time").StringValue)-App.ConvertTimeToSeconds(ActualStart),false)
		        end if
		        
		        select case rsIntervals.Field("Interval_Number").IntegerValue
		        case 0
		          NewTotalTime="00:00:00.000"
		          IntervalTime=NewTotalTime
		        case 1
		          IntervalTime=NewTotalTime
		        else
		          IntervalTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(NewTotalTime)-App.ConvertTimeToSeconds(PreviousTime),false)
		        end select
		        PreviousTime=NewTotalTime
		        
		        if rsIntervals.Field("Interval_Number").StringValue ="5" then
		          SQLStatement=""
		        end if
		        
		        SQLStatement="UPDATE times SET"
		        SQLStatement=SQLStatement+" Total_Time='"+NewTotalTime+"',"
		        SQLStatement=SQLStatement+" Interval_Time='"+IntervalTime+"'"
		        SQLStatement=SQLStatement+" WHERE rowid="+rsIntervals.Field("rowid").StringValue
		      else
		        SQLStatement="UPDATE times SET"
		        SQLStatement=SQLStatement+" Total_Time='00:00:00.000',"
		        SQLStatement=SQLStatement+" Interval_Time='00:00:00.000',"
		        SQLStatement=SQLStatement+" Pace='00:00:00.000',"
		        SQLStatement=SQLStatement+" Use_This_Passing='N'"
		        SQLStatement=SQLStatement+" WHERE rowid="+rsIntervals.Field("rowid").StringValue
		      end if
		      App.theDB.DBSQLExecute(SQLStatement)
		      
		      OldActualTime=rsIntervals.Field("Actual_Time").StringValue
		      
		      rsIntervals.MoveNext
		    wend
		    rsIntervals.Close
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ResetRace()
		  dim SQLStatement as string
		  dim rsDivsions as RecordSet
		  dim res as Boolean
		  dim recCount as integer
		  
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  d.icon=1
		  d.CancelButton.Visible=true
		  
		  d.CancelButton.Caption="OK"
		  d.ActionButton.Caption="Cancel"
		  d.Message="Reset Race"
		  d.Explanation="There is NO UNDO for this action."
		  b=d.ShowModal
		  
		  if  b=d.CancelButton then
		    Progress.Show
		    Progress.Initialize("Progress","Resetting Race...",0,-1)
		    
		    'update the placing information
		    SQLStatement="UPDATE racedistances SET OverallPlace=0, FastestTime='00:00:00.000',"
		    SQLStatement=SQLStatement+" CurrentPlace_Male=0,"
		    SQLStatement=SQLStatement+" FastestTime_Male='00:00:00.000',"
		    SQLStatement=SQLStatement+" CurrentPlace_Female=0,"
		    SQLStatement=SQLStatement+" FastestTime_Female='00:00:00.000',"
		    SQLStatement=SQLStatement+" CurrentPlace_Mixed=0,"
		    SQLStatement=SQLStatement+" FastestTime_Mixed='00:00:00.000'"
		    SQLStatement=SQLStatement+" WHERE RaceID=1"
		    App.theDB.DBSQLExecute(SQLStatement)
		    
		    SQLStatement="UPDATE intervals SET CurrentPlace=0, CurrentFastestTime='00:00:00.000'"
		    SQLStatement=SQLStatement+" WHERE RaceID=1"
		    App.theDB.DBSQLExecute(SQLStatement)
		    
		    SQLStatement="UPDATE ccTeamIntervals SET Total_Points='', Number_Scorers=''"
		    App.theDB.DBSQLExecute(SQLStatement)
		    
		    SQLStatement="UPDATE divisions SET Actual_Start_Time='"+app.RaceDate.SQLDate+" 00:00:00.000', Current_Place=0, FastestTime='00:00:00.000' WHERE RaceID=1"
		    App.theDB.DBSQLExecute(SQLStatement)
		    'Reset the start time and clear the times
		    SQLStatement="SELECT rowid, Start_Time FROM divisions WHERE RaceID = 1"
		    rsDivsions=app.theDB.DBSQLSelect(SQLStatement)
		    while not rsDivsions.eof
		      SQLStatement="UPDATE participants SET Assigned_Start='"+rsDivsions.Field("Start_Time").StringValue+"',"
		      SQLStatement=SQLStatement+" Actual_Start='"+app.RaceDate.SQLDate+" 00:00:00.000',"
		      SQLStatement=SQLStatement+" Actual_Stop='"+app.RaceDate.SQLDate+" 00:00:00.000',"
		      SQLStatement=SQLStatement+" Total_Adjustment='+00:00:00.000',"
		      SQLStatement=SQLStatement+" Total_Time='00:00:00.000',"
		      SQLStatement=SQLStatement+" Net_Time='00:00:00.000',"
		      SQLStatement=SQLStatement+" Time_Source='TT',"
		      SQLStatement=SQLStatement+" DNS='Y',"
		      SQLStatement=SQLStatement+" DNF='Y',"
		      SQLStatement=SQLStatement+" DQ='N',"
		      SQLStatement=SQLStatement+" Laps_Completed=0,"
		      SQLStatement=SQLStatement+" Adjustment_Reason=''"
		      SQLStatement = SQLStatement + " WHERE RaceID=1 AND DivisionID="+rsDivsions.Field("rowid").StringValue
		      
		      App.theDB.DBSQLExecute(SQLStatement)
		      rsDivsions.moveNext
		    wend
		    rsDivsions.Close
		    
		    'delete the times
		    SQLStatement="DELETE FROM times WHERE RaceID=1"
		    App.theDB.DBSQLExecute(SQLStatement)
		    app.theDB.DBCommit
		    
		    ExecuteQuery("Participants","participants."+replaceall(DataBaseFields_Participants.text," ","_")+" "+Relationship_Participants.text+" '"+ef_query_Participants.text+"'",Participant_Sort)
		    wnd_List.DataList_Participants.Refresh
		    
		    wnd_List.DataList_Times.RemoveRow(1,DataList_Times.Rows)
		    wnd_List.DataList_Times.Refresh
		    
		    Progress.Close
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ScalePicture(PictureToScale as picture, Availablewidth as integer, Availableheight as integer) As picture
		  // Now Scale the image
		  dim d as double
		  dim w,h,neww,newh as integer
		  dim p2 as Picture
		  
		  //check that there really is a picture first
		  if PictureToScale.graphics<>nil then
		    w=PictureToScale.width
		    h=PictureToScale.height
		    d=min(Availablewidth/w,Availableheight/h) // Calculate the factor with which to scale
		    neww=w*d // the new width of the picture
		    newh=h*d // the new height of the picture
		    
		    
		    p2= new Picture(Availablewidth,Availableheight, PictureToScale.Depth)
		    p2.Graphics.DrawPicture PictureToScale, (Availablewidth-neww)/2, (Availableheight-newh)/2,neww,newh,0,0,w,h
		    return p2
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetupListbox()
		  dim colsRS as recordSet
		  dim SelectStatement as string
		  dim i as integer
		  
		  'Divisions
		  
		  DataList_Divisions.Cols=8
		  
		  DataList_Divisions.Column(1).Header.Text="Name"
		  DataList_Divisions.Column(1).Width=150
		  DataList_Divisions.Column(1).Alignment=ColumnAlignment.Left
		  DataList_Divisions.Column(1).Header.Alignment=ColumnAlignment.Center
		  
		  DataList_Divisions.Column(2).Header.Text="Low Age"
		  DataList_Divisions.Column(2).Width=65
		  DataList_Divisions.Column(2).Alignment=ColumnAlignment.Center
		  DataList_Divisions.Column(2).Header.Alignment=1
		  
		  DataList_Divisions.Column(3).Header.Text="High Age"
		  DataList_Divisions.Column(3).Width=65
		  DataList_Divisions.Column(3).Alignment=ColumnAlignment.Center
		  DataList_Divisions.Column(3).Header.Alignment=ColumnAlignment.Center
		  
		  DataList_Divisions.Column(4).Header.Text="Gender"
		  DataList_Divisions.Column(4).Width=65
		  DataList_Divisions.Column(4).Alignment=ColumnAlignment.Center
		  
		  DataList_Divisions.Column(5).Header.Text="Distance"
		  DataList_Divisions.Column(5).Width=150
		  DataList_Divisions.Column(5).Alignment=ColumnAlignment.Left
		  DataList_Divisions.Column(5).Header.Alignment=ColumnAlignment.Center
		  
		  DataList_Divisions.Column(6).Header.Text="Category"
		  DataList_Divisions.Column(6).Width=150
		  DataList_Divisions.Column(6).Alignment=ColumnAlignment.Left
		  DataList_Divisions.Column(6).Header.Alignment=ColumnAlignment.Center
		  
		  DataList_Divisions.Column(7).Header.Text="Start Time"
		  DataList_Divisions.Column(7).Width=175
		  DataList_Divisions.Column(7).Alignment=ColumnAlignment.Center
		  
		  DataList_Divisions.Column(8).Header.Text="Order"
		  DataList_Divisions.Column(8).Width=65
		  DataList_Divisions.Column(8).Alignment=ColumnAlignment.Center
		  DataList_Divisions.SortIndicatorColumn=1
		  
		  SelectStatement="SELECT * FROM divisions"
		  colsRS=app.theDB.DBSQLSelect(SelectStatement)
		  for i=1 to colsRS.fieldcount
		    if instr(colsRS.idxfield(i).name,"id")=0 then
		      DataBaseFields_Divisions.addRow replaceall(colsRS.idxfield(i).name,"_"," ")
		    end if
		  next
		  DataBaseFields_Divisions.ListIndex=0
		  
		  Division_Sort="List_Order ASC"
		  ExtraQuery_Divisions=""
		  ExecuteQuery("Divisions",ExtraQuery_Divisions,Division_Sort)
		  
		  'Teams
		  
		  DataList_Teams.Cols=3
		  
		  DataList_Teams.Column(1).Header.Text="Name"
		  DataList_Teams.Column(1).Width=300
		  DataList_Teams.Column(1).Alignment=ColumnAlignment.Left
		  DataList_Teams.Column(1).Header.Alignment=ColumnAlignment.Left
		  
		  DataList_Teams.Column(2).Header.Text="Gender"
		  DataList_Teams.Column(2).Width=300
		  DataList_Teams.Column(2).Alignment=ColumnAlignment.Left
		  DataList_Teams.Column(2).Header.Alignment=ColumnAlignment.Left
		  
		  DataList_Teams.Column(3).Header.Text="Division"
		  DataList_Teams.Column(3).Width=301
		  DataList_Teams.Column(3).Alignment=ColumnAlignment.Left
		  DataList_Teams.Column(3).Header.Alignment=ColumnAlignment.Left
		  
		  SelectStatement="SELECT * FROM CCTeams"
		  colsRS=app.theDB.DBSQLSelect(SelectStatement)
		  for i=1 to colsRS.fieldcount
		    if instr(colsRS.idxfield(i).name,"id")=0 then
		      DataBaseFields_Teams.addRow replaceall(colsRS.idxfield(i).name,"_"," ")
		    end if
		  next
		  DataBaseFields_Divisions.ListIndex=0
		  
		  Team_Sort="Team_Name ASC"
		  ExtraQuery_Teams=""
		  ExecuteQuery("CCTeams",ExtraQuery_Teams,Team_Sort)
		  
		  'Participants
		  
		  DataList_Participants.Cols=Participant_Columns
		  
		  DataList_Participants.Column(1).Header.Text="No"
		  DataList_Participants.Column(1).Width=40
		  DataList_Participants.Column(1).Alignment=ColumnAlignment.Center
		  
		  DataList_Participants.Column(2).Header.Text="  Name"
		  DataList_Participants.Column(2).Width=140
		  DataList_Participants.Column(2).Alignment=ColumnAlignment.Left
		  DataList_Participants.Column(2).Header.Alignment=ColumnAlignment.Left
		  
		  DataList_Participants.Column(3).Header.Text="  Representing"
		  DataList_Participants.Column(3).Width=200
		  DataList_Participants.Column(3).Alignment=ColumnAlignment.Left
		  DataList_Participants.Column(3).Header.Alignment=ColumnAlignment.Left
		  
		  DataList_Participants.Column(4).Header.Text="  Division"
		  DataList_Participants.Column(4).Width=180
		  DataList_Participants.Column(4).Alignment=0
		  DataList_Participants.Column(4).Header.Alignment=0
		  
		  DataList_Participants.Column(5).Header.Text="Src"
		  DataList_Participants.Column(5).Width=30
		  DataList_Participants.Column(5).Alignment=ColumnAlignment.Center
		  
		  DataList_Participants.Column(6).Header.Text="Int"
		  DataList_Participants.Column(6).Width=30
		  DataList_Participants.Column(6).Alignment=ColumnAlignment.Center
		  
		  DataList_Participants.Column(7).Header.Text="Total Time"
		  DataList_Participants.Column(7).Width=80
		  DataList_Participants.Column(7).Alignment=ColumnAlignment.Center
		  
		  SelectStatement="SELECT DISTINCT Interval_Name, Number FROM intervals ORDER BY Number"
		  colsRS=app.theDB.DBSQLSelect(SelectStatement)
		  DataList_Participants.Cols=7+ colsRS.RecordCount
		  For i = 1 to colsRS.RecordCount
		    if i = colsRS.Field("Number").IntegerValue then
		      DataList_Participants.Column(Participant_Columns+i).Header.Text=colsRS.Field("Interval_Name").StringValue
		      DataList_Participants.Column(Participant_Columns+i).Width=80
		      DataList_Participants.Column(Participant_Columns+i).Alignment=ColumnAlignment.Center
		    end if
		    colsRS.MoveNext
		  Next
		  
		  DataList_Participants.SortIndicatorColumn=1
		  
		  SelectStatement="SELECT * FROM participants"
		  colsRS=app.theDB.DBSQLSelect(SelectStatement)
		  for i=1 to colsRS.fieldcount
		    if instr(colsRS.idxfield(i).name,"id")=0 then
		      DataBaseFields_Participants.addRow replaceall(colsRS.idxfield(i).name,"_"," ")
		    end if
		  next
		  DataBaseFields_Participants.InsertRow(3, "Division Name")
		  DataBaseFields_Participants.InsertRow(4, "Assigned/Actual Start Difference")
		  DataBaseFields_Participants.InsertRow(5, "Has Passed Between These Points")
		  DataBaseFields_Participants.InsertRow(6, "Has NOT Passed Between These Points")
		  
		  i=0
		  Do
		    i=i+1
		    DataBaseFields_Participants.ListIndex=i
		  Loop Until (DataBaseFields_Participants.Text="NGB License Number") or (i>=DataBaseFields_Participants.ListCount)
		  
		  DataBaseFields_Participants.InsertRow(i+4, "Has a NGB License and Inactive Points")
		  
		  Do
		    i=i+1
		    DataBaseFields_Participants.ListIndex=i
		  Loop Until (DataBaseFields_Participants.Text="NGB2 License Number") or (i>=DataBaseFields_Participants.ListCount)
		  i=i+1
		  DataBaseFields_Participants.InsertRow(i+3, "Has a NGB2 License and Inactive Points")
		  
		  DataBaseFields_Participants.ListIndex=0
		  
		  Participant_Sort="Racer_Number ASC"
		  ExtraQuery_Participants=""
		  ExecuteQuery("Participants",ExtraQuery_Participants,Participant_Sort)
		  
		  'Times
		  
		  DataList_Times.Cols=9
		  
		  DataList_Times.Column(1).Header.Text="Number"
		  DataList_Times.Column(1).Width=75
		  DataList_Times.Column(1).Alignment=ColumnAlignment.Center
		  
		  DataList_Times.Column(2).Header.Text="Name"
		  DataList_Times.Column(2).Width=150
		  DataList_Times.Column(2).Alignment=ColumnAlignment.Left
		  
		  DataList_Times.Column(3).Header.Text="Team Member"
		  DataList_Times.Column(3).Width=150
		  DataList_Times.Column(3).Alignment=ColumnAlignment.Left
		  
		  DataList_Times.Column(4).Header.Text="Interval"
		  DataList_Times.Column(4).Width=60
		  DataList_Times.Column(4).Alignment=ColumnAlignment.Center
		  
		  DataList_Times.Column(5).Header.Text="Int Time"
		  DataList_Times.Column(5).Width=90
		  DataList_Times.Column(5).Alignment=ColumnAlignment.Center
		  
		  DataList_Times.Column(6).Header.Text="Total Time"
		  DataList_Times.Column(6).Width=90
		  DataList_Times.Column(6).Alignment=ColumnAlignment.Center
		  
		  DataList_Times.Column(7).Header.Text="Time of Day"
		  DataList_Times.Column(7).Width=160
		  DataList_Times.Column(7).Alignment=ColumnAlignment.Center
		  
		  DataList_Times.Column(8).Header.Text="Passing"
		  DataList_Times.Column(8).Width=45
		  DataList_Times.Column(8).Alignment=ColumnAlignment.Center
		  
		  DataList_Times.Column(9).Header.Text="TX Code"
		  DataList_Times.Column(9).Width=70
		  DataList_Times.Column(9).Alignment=ColumnAlignment.Center
		  
		  DataList_Times.SortIndicatorColumn=1
		  
		  DataBaseFields_Times.AddRow "Racer Number"
		  DataBaseFields_Times.AddRow "Unassigned"
		  
		  SelectStatement="SELECT * FROM times"
		  colsRS=app.theDB.DBSQLSelect(SelectStatement)
		  for i=1 to colsRS.fieldcount
		    if instr(colsRS.idxfield(i).name,"id")=0 then
		      DataBaseFields_Times.addRow replaceall(colsRS.idxfield(i).name,"_"," ")
		    end if
		  next
		  DataBaseFields_Times.ListIndex=0
		  
		  'Transponders
		  
		  DataList_Transponders.Cols=7
		  
		  DataList_Transponders.Column(1).Header.Text="Number"
		  DataList_Transponders.Column(1).Width=90
		  DataList_Transponders.Column(1).Alignment=ColumnAlignment.Center
		  
		  DataList_Transponders.Column(2).Header.Text="Transponder"
		  DataList_Transponders.Column(2).Width=90
		  DataList_Transponders.Column(2).Alignment=ColumnAlignment.Center
		  
		  DataList_Transponders.Column(3).Header.Text="Name"
		  DataList_Transponders.Column(3).Width=150
		  DataList_Transponders.Column(3).Alignment=ColumnAlignment.Left
		  
		  DataList_Transponders.Column(4).Header.Text="Team Member Name"
		  DataList_Transponders.Column(4).Width=150
		  DataList_Transponders.Column(4).Alignment=ColumnAlignment.Left
		  
		  DataList_Transponders.Column(5).Header.Text="Issued"
		  DataList_Transponders.Column(5).Width=175
		  DataList_Transponders.Column(5).Alignment=ColumnAlignment.Center
		  
		  DataList_Transponders.Column(6).Header.Text="Hits"
		  DataList_Transponders.Column(6).Width=50
		  DataList_Transponders.Column(6).Alignment=ColumnAlignment.Center
		  
		  DataList_Transponders.Column(7).Header.Text="Returned"
		  DataList_Transponders.Column(7).Width=175
		  DataList_Transponders.Column(7).Alignment=ColumnAlignment.Center
		  
		  DataList_Transponders.SortIndicatorColumn=1
		  
		  SelectStatement="SELECT * FROM transponders LIMIT 1,1"
		  colsRS=app.theDB.DBSQLSelect(SelectStatement)
		  for i=1 to colsRS.fieldcount
		    if instr(colsRS.idxfield(i).name,"id")=0 then
		      DataBaseFields_Transponders.addRow replaceall(colsRS.idxfield(i).name,"_"," ")
		    end if
		  next
		  DataBaseFields_Transponders.ListIndex=0
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowRecords(OmitSubset as boolean)
		  dim i as integer
		  dim selectString as string
		  dim IDFieldName as string
		  dim SelectionArray() as integer
		  dim UseThese() as integer
		  
		  
		  'if OmitSubset then
		  'for i=myData.SelectedRow to myData.SelectedRowEnd
		  'SelectionArray.Append myData.row(i).ItemData
		  'next
		  '
		  'for i=1 to myData.Rows
		  'if SelectionArray.IndexOf(myData.row(i).ItemData)<0 then
		  'UseThese.Append myData.row(i).ItemData
		  'end if
		  'next
		  '
		  'else
		  '
		  'for i=myData.SelectedRow to myData.SelectedRowEnd
		  'UseThese.Append myData.row(i).ItemData
		  'next
		  'end if
		  '
		  ' for i=0 to Ubound(UseThese)-1
		  'if selectString <> "" then
		  'selectString=selectString+" OR "+IDFieldName+"="+format(UseThese(i),"########")
		  'else
		  'selectString=IDFieldName+"="+format(UseThese(i),"########")
		  'end if
		  'next
		  'ExecuteQuery(selectString)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ShowTimeTrialUI()
		  lblTTStartInterval.Visible=true
		  cbTTStartInterval.Visible=true
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateNonTXTimes()
		  dim i,ii as Integer
		  dim rsNonTXTimes, rsTXTimes, rsParticipant as RecordSet
		  dim SQL, LowTime, HighTime, NewTotalTimeStr, IntervalNumber(), TimeSrc as String
		  dim ActualTime, ActualTimeAdjusted, StartTime, NewTotalTime as double
		  
		  IntervalNumber.Append "0"
		  IntervalNumber.Append "9999"
		  RangeLow(0)=0
		  RangeHigh(0)=slStart.Value
		  RangeLow(1)=slFinish.Value/100
		  RangeHigh(1)=slFinish.Value/100
		  
		  for i=0 to 1
		    
		    SQL="SELECT rowid, Actual_Time FROM times WHERE TX_Code='Channel A' AND Use_This_Passing='N' AND Interval_Number="+IntervalNumber(i)+" ORDER BY Actual_Time"
		    
		    rsNonTXTimes=app.theDB.DBSQLSelect(SQL)
		    
		    for ii = 1 to rsNonTXTimes.RecordCount 
		      ActualTime=app.ConvertTimeToSeconds(rsNonTXTimes.Field("Actual_Time").StringValue)
		      ActualTimeAdjusted=ActualTime-app.AMBOffset
		      LowTime=app.ConvertSecondsToTime((ActualTimeAdjusted-RangeLow(i)),true)
		      HighTime=app.ConvertSecondsToTime((ActualTimeAdjusted+RangeHigh(i)),true)
		      SQL="SELECT rowid, ParticipantID, Actual_Time FROM times WHERE ParticipantID <>0 AND TX_Code <> 'Channel A' AND TX_Code <> 'Channel B' AND TX_Code <> 'Chan A' AND TX_Code <> 'Chan B' "
		      SQL=SQL+"AND Interval_Number="+IntervalNumber(i)+" "
		      SQL=SQL+"AND Actual_Time>'"+LowTime+"' AND Actual_Time<'"+HighTime+"' ORDER BY Actual_Time"
		      rsTXTimes=app.theDB.DBSQLSelect(SQL)
		      
		      if rsTXTimes.RecordCount>0 then
		        'update the transponders records to not use this passing
		        SQL="UPDATE times SET Use_This_Passing='N' WHERE ParticipantID="+rsTXTimes.Field("ParticipantID").StringValue+" AND Interval_Number="+IntervalNumber(i)
		        app.theDB.DBSQLExecute(SQL)
		        
		        if i = 0 then 'update start times
		          'update the non transponders time records to to use the passing
		          SQL="UPDATE times SET Use_This_Passing='Y', ParticipantID="+rsTXTimes.Field("ParticipantID").StringValue+" WHERE rowid="+rsNonTXTimes.Field("rowid").StringValue
		          app.theDB.DBSQLExecute(SQL)
		          
		          'update the participant record with the new total time
		          SQL="UPDATE participants SET Actual_Start='"+rsNonTXTimes.Field("Actual_Time").StringValue+"', Time_Source='AT' WHERE rowid="+rsTXTimes.Field("ParticipantID").StringValue
		          app.theDB.DBSQLExecute(SQL)
		          
		          UpdateParticipantListTime(rsTXTimes.Field("ParticipantID").StringValue,0,"","AT")
		          
		        else
		          'update stop times
		          'update the non transponders time records to to use the passing and calculate the total time
		          SQL="SELECT Actual_Start, Time_Source, Laps_Completed FROM participants WHERE rowid="+rsTXTimes.Field("ParticipantID").StringValue
		          rsParticipant=app.theDB.DBSQLSelect(SQL)
		          
		          if rsParticipant.Field("Time_Source").StringValue.Right(1) = "T" then
		            StartTime=app.ConvertTimeToSeconds(rsParticipant.Field("Actual_Start").StringValue)
		            NewTotalTime=ActualTime-StartTime
		            NewTotalTimeStr= app.ConvertSecondsToTime(NewTotalTime,false)
		            
		            if app.PlacesToTruncate=0 then
		              NewTotalTimeStr=Left(NewTotalTimeStr,len(NewTotalTimeStr)-4)
		            else
		              NewTotalTimeStr=Left(NewTotalTimeStr,len(NewTotalTimeStr)-(3-app.PlacesToTruncate))
		            end if
		            SQL="UPDATE times SET Use_This_Passing='Y', Total_Time='"+NewTotalTimeStr+"', ParticipantID="+rsTXTimes.Field("ParticipantID").StringValue+" WHERE rowid="+rsNonTXTimes.Field("rowid").StringValue
		            app.theDB.DBSQLExecute(SQL)
		            
		            'update the participant record with the new total time
		            TimeSrc=rsParticipant.Field("Time_Source").StringValue.Left(1)+"A"
		            SQL="UPDATE participants SET DNF='N',  DNS='N', Actual_Stop='"+rsNonTXTimes.Field("Actual_Time").StringValue+"', Total_Time='"+NewTotalTimeStr+"', Time_Source='"+TimeSrc+"' WHERE rowid="+rsTXTimes.Field("ParticipantID").StringValue
		            app.theDB.DBSQLExecute(SQL)
		            rsParticipant.Close
		            
		            UpdateParticipantListTime(rsTXTimes.Field("ParticipantID").StringValue,9999,NewTotalTimeStr,TimeSrc)
		            
		          else
		            SQL="UPDATE times SET TX_Code='Chan A' WHERE rowid="+rsNonTXTimes.Field("rowid").StringValue
		            app.theDB.DBSQLExecute(SQL)
		            
		          end if
		        end if
		        
		      end if
		      rsTXTimes.Close
		      rsNonTXTimes.MoveNext
		    next
		    rsNonTXTimes.Close
		  next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateParticipantListTime(ParticipantID as string, Interval as Integer, Time as string, TimeSource as string)
		  dim i as integer
		  dim RowFound as Boolean
		  dim rsParticipant as RecordSet
		  dim SQL as String
		  
		  'see if the participant is listed in the data list.
		  if DataList_Participants.Rows>0 then
		    i=0
		    RowFound=false
		    do
		      i=i+1
		      if DataList_Participants.Row(i).ItemData=ParticipantID then
		        RowFound=true
		      end if
		    loop until ((i>=DataList_Participants.Rows) or (RowFound))
		    
		    'if found then update the time
		    if RowFound then
		      
		      SQL="SELECT Laps_Completed FROM participants WHERE rowid="+ParticipantID
		      rsParticipant=app.theDB.DBSQLSelect(SQL)
		      
		      Select case Interval
		      case 0
		        DataList_Participants.CellText(5,i)=TimeSource
		        DataList_Participants.CellText(6,i)=rsParticipant.Field("Laps_Completed").StringValue
		        if DataList_Participants.CellText(7,i)="DNS" then
		          DataList_Participants.CellText(7,i)="DNF"
		        end if
		        
		      case 9999 
		        if TimeSource<>"" then
		          DataList_Participants.CellText(5,i)=TimeSource
		        end if
		        DataList_Participants.CellText(6,i)=rsParticipant.Field("Laps_Completed").StringValue
		        if Time<>"" then
		          DataList_Participants.CellText(7,i)=app.StripLeadingZeros(Time)
		        end if
		        
		        DataList_Participants.Refresh
		      end select
		      
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub UpdateSpecialMenu()
		  dim m As SpecialMenuItem
		  dim i,SpecialMenuItemCount as integer
		  dim temp as MenuItem
		  dim RemoveLine as boolean
		  
		  SpecialMenuItemCount = MenuBar1.Child( "SpecialMenu" ).Count - 1
		  
		  for i = SpecialMenuItemCount downto 0
		    ' Get a menu item
		    'm = SpecialMenuItem(MenuBar1.Child( "SpecialMenu" ).Item( i ) )
		    ' If the item has the window we want to close
		    if MenuBar1.Child( "SpecialMenu" ).Item( i ).Text="Place Racers" or MenuBar1.Child( "SpecialMenu" ).Item( i ).Text="Place Racers - Automated" or _
		      MenuBar1.Child( "SpecialMenu" ).Item( i ).Text="Place Racers - By Hand..." or MenuBar1.Child( "SpecialMenu" ).Item( i ).Text="Post Results to Web" or _
		      MenuBar1.Child( "SpecialMenu" ).Item( i ).Text="Start List - Automated" or MenuBar1.Child( "SpecialMenu" ).Item( i ).Text="Start List - By Hand" or _ 
		      MenuBar1.Child( "SpecialMenu" ).Item( i ).Text="Post Transponders to Web" then
		      ' Then remove that menu item
		      if MenuBar1.Child( "SpecialMenu" ).Item( i ).Text="Post Transponders to Web" or MenuBar1.Child( "SpecialMenu" ).Item( i ).Text="Start List - By Hand" then
		        RemoveLine=true
		      else
		        RemoveLine=false
		      end if
		      MenuBar1.Child( "SpecialMenu" ).Remove( i )
		      if RemoveLine then
		        MenuBar1.Child( "SpecialMenu" ).Remove( i )
		      end if
		    end
		  next
		  
		  SpecialMenuItemCount=0
		  
		  if instr(puStartType.Text,"Time Trial")>0 then
		    'Add TT Start List items
		    m = new SpecialMenuItem("AutomatedStartList")
		    m.Text="Start List - Automated"
		    MenuBar1.Child("SpecialMenu").Insert(3+SpecialMenuItemCount,m)
		    SpecialMenuItemCount=SpecialMenuItemCount+1
		    
		    m = new SpecialMenuItem("ManualStartList")
		    m.Text="Start List - by Hand"
		    MenuBar1.Child("SpecialMenu").Insert(3+SpecialMenuItemCount,m)
		    SpecialMenuItemCount=SpecialMenuItemCount+1
		    
		    Dim StartListSeperatorMenuItem as New MenuItem("-")
		    MenuBar1.Child("SpecialMenu").Insert(3+SpecialMenuItemCount,StartListSeperatorMenuItem)
		    SpecialMenuItemCount=SpecialMenuItemCount+1
		  end if
		  
		  if app.WebServer<>"" then
		    bbSMSMessaging.Visible=true
		    
		    m = new SpecialMenuItem("PostResults")
		    m.Text="Post Results to Web"
		    MenuBar1.Child("SpecialMenu").Insert(8+SpecialMenuItemCount,m)
		    SpecialMenuItemCount=SpecialMenuItemCount+1
		    
		    m = new SpecialMenuItem("PostTX")
		    m.Text="Post Transponders to Web"
		    MenuBar1.Child("SpecialMenu").Insert(8+SpecialMenuItemCount,m)
		    SpecialMenuItemCount=SpecialMenuItemCount+1
		    
		    Dim PostResultsSeperatorMenuItem as New MenuItem("-")
		    MenuBar1.Child("SpecialMenu").Insert(8+SpecialMenuItemCount,PostResultsSeperatorMenuItem)
		    SpecialMenuItemCount=SpecialMenuItemCount+1
		    
		  end if
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		AutoPrint_DisplayFormat As String
	#tag EndProperty

	#tag Property, Flags = &h0
		AutoPrint_FinalResults As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		AutoPrint_FirstPagePrinted As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		AutoPrint_ListDNF As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		AutoPrint_ListIntervals As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		AutoPrint_Selection() As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		AutoPrint_SelectionID() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		AutoPrint_SelectionText() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Connected As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		dataToUpload As String
	#tag EndProperty

	#tag Property, Flags = &h0
		dataToUploadType As String
	#tag EndProperty

	#tag Property, Flags = &h0
		dimRaceDistanceRowSelected As integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private DivisionBack() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		DivisionID As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private DivisionIDArray() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		DivisionPlace() As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected DivisionQueryStatement As string
	#tag EndProperty

	#tag Property, Flags = &h0
		Division_Sort As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ExtraQuery_Divisions As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ExtraQuery_Participants As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ExtraQuery_Teams As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ExtraQuery_Times As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ExtraQuery_TX As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private First_Name() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Gender() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private GenderBack() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		GenderPlace() As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		ImportFilePath As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private IndexArray() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		InputComplete As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Laps() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Logo1 As string
	#tag EndProperty

	#tag Property, Flags = &h0
		Logo2 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Logo3 As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Logo4 As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Name() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected NetTime() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private NewRace As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private NilArray() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		OldPanel As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected OldRaceAgeDate As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private OverallBack() As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private OverallPlace() As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		ParticipantID As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private ParticipantIDArray() As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ParticipantQueryStatement As string
	#tag EndProperty

	#tag Property, Flags = &h0
		Participant_Sort As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private RaceDistanceID() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		RaceDistanceIDStg As String
	#tag EndProperty

	#tag Property, Flags = &h0
		RaceDistanceRowSelected As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private RaceID As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		RangeHigh() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		RangeLow() As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		ResultsRaceID As string
	#tag EndProperty

	#tag Property, Flags = &h0
		RowSelected As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Seperator As String
	#tag EndProperty

	#tag Property, Flags = &h0
		SMTPSecureSocket1 As SMTPSecureSocket
	#tag EndProperty

	#tag Property, Flags = &h21
		Private SortArray() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		TeamID As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		TeamQueryStatement As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Team_Sort As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected theDB As MySQLCommunityServer
	#tag EndProperty

	#tag Property, Flags = &h0
		theResultsDb As MySQLCommunityServer
	#tag EndProperty

	#tag Property, Flags = &h0
		TimeID As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected TimeQueryStatement As string
	#tag EndProperty

	#tag Property, Flags = &h0
		TimeSource As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Time_Sort As String
	#tag EndProperty

	#tag Property, Flags = &h0
		TimingPointID As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private TotalTime() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		Transponder_Sort As String
	#tag EndProperty

	#tag Property, Flags = &h0
		TXIDStg As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected TXQueryStatement As string
	#tag EndProperty


	#tag Constant, Name = constCrossCountryResultType, Type = String, Dynamic = False, Default = \"Cross Country Running", Scope = Public
	#tag EndConstant

	#tag Constant, Name = constFISUSSAResultType, Type = String, Dynamic = False, Default = \"USSA/FIS X-C Results", Scope = Public
	#tag EndConstant

	#tag Constant, Name = Participant_Columns, Type = Double, Dynamic = False, Default = \"7", Scope = Public
	#tag EndConstant


#tag EndWindowCode

#tag Events RaceTab
	#tag Event
		Sub Change()
		  if OldPanel=0 then
		    ApplyRaceDataChanges
		  end if
		  OldPanel=RaceTab.PanelIndex
		  
		  HighlightQueryValue
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  OldPanel=0
		  RaceTab.PanelIndex=0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events RaceTab_DistancesListBox
	#tag Event
		Sub DoubleClick()
		  
		  RaceDistanceRowSelected=RaceTab_DistancesListBox.ListIndex
		  RaceDistanceIDStg =RaceTab_DistancesListBox.Cell(RaceDistanceRowSelected,0)
		  RaceDistance.show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbDeleteRaceDistance
	#tag Event
		Sub Action()
		  Dim SQLStatement as string
		  dim res as boolean
		  Dim Result as integer
		  dim row as integer
		  
		  Result=MsgBox("Are you sure you want to delete this Race Distance?",1+48)
		  if Result = 1 then
		    row=RaceTab_DistancesListBox.ListIndex
		    SQLStatement="DELETE FROM racedistances WHERE RaceID = 1 AND rowid="+RaceTab_DistancesListBox.Cell(row,0)
		    App.theDB.DBSQLExecute(SQLStatement)
		    RaceTab_DistancesListBox.RemoveRow row
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbNewRaceDistance
	#tag Event
		Sub Action()
		  if RaceDateInput.text = "" or RaceDateInput.text = "0000-00-00" then
		    MsgBox("A Race Date is required before you can add a race distance.")
		    RaceDateInput.SetFocus
		  else
		    RaceDistanceIDStg="0"
		    RaceDistance.show
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events RaceDateInput
	#tag Event
		Sub LostFocus()
		  if RaceAgeDate.text = "" or RaceAgeDate.text = "0000-00-00" then
		    RaceAgeDate.text = RaceDateInput.text
		  end if
		  RaceTime.text=RaceDateInput.text+" 00:00:00.000"
		  app.RaceDate=App.SQLDateToDate(RaceDateInput.text)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events RaceAgeDate
	#tag Event
		Sub Open()
		  OldRaceAgeDate=app.RaceAgeDate.SQLDate
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events puStartType
	#tag Event
		Sub Change()
		  Select case me.Text
		    
		  case "Time Trial 1/start"
		    ShowTimeTrialUI
		    
		  case "Time Trial 2/start"
		    ShowTimeTrialUI
		    
		  case "Time Trial 3/start"
		    ShowTimeTrialUI
		    
		  case "Time Trial 4/start"
		    ShowTimeTrialUI
		    
		  else
		    HideTimeTrialUI
		    
		  end Select
		  
		  
		  UpdateSpecialMenu 
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbApplyChanges
	#tag Event
		Sub Action()
		  ApplyRaceDataChanges
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Relationship_Divisions
	#tag Event
		Sub Open()
		  Relationship_Divisions.addRow "="
		  Relationship_Divisions.addRow ">="
		  Relationship_Divisions.addRow "<="
		  Relationship_Divisions.addRow "<>"
		  Relationship_Divisions.addRow "LIKE"
		  Relationship_Divisions.ListIndex = 0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pb_query_Divisions
	#tag Event
		Sub Action()
		  ExecuteQuery("Divisions",ReplaceAll(DataBaseFields_Divisions.text," ","_")+" "+Relationship_Divisions.text+" '"+ef_query_Divisions.text+"'",Division_Sort)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbShowAllDiviions
	#tag Event
		Sub Action()
		  ExtraQuery_Divisions=""
		  ExecuteQuery("Divisions",ExtraQuery_Divisions,Division_Sort)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbShowSubset
	#tag Event
		Sub Action()
		  ShowRecords(false)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbOmitSubset
	#tag Event
		Sub Action()
		  ShowRecords(True)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DataList_Divisions
	#tag Event
		Sub CellDblClick(CellX as Integer, CellY as Integer,x as Integer,y as Integer)
		  RowSelected=CellY
		  
		  DivisionID=val(DataList_Divisions.Row(CellY).ItemData)
		  DivisionInput.show
		End Sub
	#tag EndEvent
	#tag Event
		Sub ColumnHeaderClick(Column as Integer)
		  select case Column
		    
		  case 1
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Division_Sort="Division_Name ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Division_Sort="Division_Name DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 2
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Division_Sort="Low_Age ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Division_Sort="Low_Age DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 3
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Division_Sort="High_Age ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Division_Sort="High_Age DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 4
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Division_Sort="Gender ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Division_Sort="Gender DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 5
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Division_Sort="RaceDistance_Name ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Division_Sort="RaceDistance_Name DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 6
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Division_Sort="Import_Name ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Division_Sort="Import_Name DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 7
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Division_Sort="Start_Time ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Division_Sort="Start_Time DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 8
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Division_Sort="List_Order ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Division_Sort="List_Order DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  end Select
		  
		  me.SortIndicatorColumn=Column
		  ExecuteQuery("Divisions",ExtraQuery_Divisions,Division_Sort)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.SortIndicatorColumn=6
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbNewDivision
	#tag Event
		Sub Action()
		  DivisionID=0
		  DivisionInput.show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbEditDivision
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if DataList_Divisions.Selection.FirstSelectedRow=0 then
		    d.Icon=1
		    d.ActionButton.Caption="OK"
		    d.Message="Please select a division record to edit."
		    b=d.ShowModal
		  else
		    DivisionID=DataList_Divisions.Row(DataList_Divisions.Selection.FirstSelectedRow).ItemData
		    DivisionInput.show
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbDeleteDivsion
	#tag Event
		Sub Action()
		  dim Result As Integer
		  dim row as integer
		  dim SQLStatement as string
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if Keyboard.OptionKey then
		    d.Icon=1
		    d.CancelButton.Visible=true
		    d.CancelButton.Caption="Delete"
		    d.ActionButton.Caption="Cancel"
		    d.Message="Delete ALL the division data?"
		    d.Explanation="Are you sure? There is no undo for this operation and all division data will be lost."
		    b=d.ShowModal
		    if b=d.CancelButton then
		      SQLStatement="DELETE FROM divisions WHERE RaceID=1 "
		      App.theDB.DBSQLExecute(SQLStatement)
		      app.theDB.DBCommit
		      DataList_Divisions.RemoveRow(1,DataList_Divisions.Rows)
		      DataList_Divisions.Selection.Clear
		      DataList_Divisions.Refresh
		    end if
		  else
		    if DataList_Divisions.Selection.FirstSelectedRow=0 then
		      d.Icon=1
		      d.ActionButton.Caption="OK"
		      d.Message="Please make a selection of division records."
		      b=d.ShowModal
		    else
		      d.Icon=1
		      d.CancelButton.Visible=true
		      d.CancelButton.Caption="Delete"
		      d.ActionButton.Caption="Cancel"
		      d.Message="Are you sure you want to delete the selected Division(s)?"
		      d.Explanation="There is no undo for this operation."
		      b=d.ShowModal
		      if b=d.CancelButton then
		        for row=DataList_Divisions.Selection.FirstSelectedRow to DataList_Divisions.Selection.LastSelectedRow
		          SQLStatement="DELETE FROM divisions WHERE RaceID=1 AND rowid="+str(DataList_Divisions.Row(row).ItemData)
		          App.theDB.DBSQLExecute(SQLStatement)
		        next
		        app.theDB.DBCommit
		        DataList_Divisions.RemoveRow(DataList_Divisions.Selection.FirstSelectedRow,DataList_Divisions.Selection.LastSelectedRow-DataList_Divisions.Selection.FirstSelectedRow+1)
		        DataList_Divisions.Selection.Clear
		        DataList_Divisions.Refresh
		      end if
		    end if
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbDivisionImport
	#tag Event
		Sub Action()
		  LocateImportFolder.Show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbDuplicateIntervals
	#tag Event
		Sub Action()
		  DuplicateIntervals
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Relationship_Participants
	#tag Event
		Sub Open()
		  Relationship_Participants.addRow "="
		  Relationship_Participants.addRow ">="
		  Relationship_Participants.addRow "<="
		  Relationship_Participants.addRow "<>"
		  Relationship_Participants.addRow "LIKE"
		  Relationship_Participants.ListIndex = 0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pb_Query_Participants
	#tag Event
		Sub Action()
		  ExecuteQuery("Participants","participants."+replaceall(DataBaseFields_Participants.text," ","_")+" "+Relationship_Participants.text+" '"+ef_query_Participants.text+"'",Participant_Sort)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbShowAllParticipants
	#tag Event
		Sub Action()
		  DataBaseFields_Participants.ListIndex=1
		  ExtraQuery_Participants=""
		  ExecuteQuery("Participants",ExtraQuery_Participants,Participant_Sort)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbShowHightlightedParticipants
	#tag Event
		Sub Action()
		  ShowRecords(false)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbOmitHighlightedParticipants
	#tag Event
		Sub Action()
		  ShowRecords(false)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DataBaseFields_Participants
	#tag Event
		Sub Change()
		  Select Case me.text
		    
		  Case "Has Passed Between These Points", "Has NOT Passed Between These Points"
		    TimeSource1.Visible=true
		    TimeSource2.Visible=true
		    Relationship_Participants.Visible=false
		    ef_query_Participants.Visible=false
		    
		  Case "Has a NGB License and Inactive Points", "Has a NGB2 License and Inactive Points"
		    TimeSource1.Visible=false
		    TimeSource2.Visible=false
		    Relationship_Participants.Visible=false
		    ef_query_Participants.Visible=false
		    
		  else
		    TimeSource1.Visible=false
		    TimeSource2.Visible=false
		    Relationship_Participants.Visible=true
		    ef_query_Participants.Visible=true
		  end Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TimeSource2
	#tag Event
		Sub Open()
		  dim SQLStatement as string
		  dim rsDistinctIntervals as RecordSet
		  dim res As Boolean
		  dim distinctRecCount as Integer
		  
		  me.RowTag(0)=-1
		  
		  me.AddRow "Start"
		  me.RowTag(me.ListCount-1)=0
		  
		  SQLStatement="SELECT DISTINCT Interval_Name, Number FROM intervals WHERE RaceID =1 ORDER BY Number ASC"
		  rsDistinctIntervals=app.theDB.DBSQLSelect(SQLStatement)
		  
		  while not rsDistinctIntervals.eof
		    me.AddRow rsDistinctIntervals.Field("Interval_Name").StringValue
		    me.RowTag(me.ListCount-1)=rsDistinctIntervals.Field("Number").IntegerValue
		    rsDistinctIntervals.MoveNext
		  Wend
		  
		  me.AddRow "Finish"
		  me.RowTag(me.ListCount-1)=9999
		  me.ListIndex=0
		  
		  me.Visible=false
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TimeSource1
	#tag Event
		Sub Open()
		  dim SQLStatement as string
		  dim rsDistinctIntervals as RecordSet
		  dim res As Boolean
		  dim distinctRecCount as Integer
		  
		  me.RowTag(0)=-1
		  
		  me.AddRow "Start"
		  me.RowTag(me.ListCount-1)=0
		  
		  SQLStatement="SELECT DISTINCT Interval_Name, Number FROM intervals WHERE RaceID =1 ORDER BY Number ASC"
		  rsDistinctIntervals=app.theDB.DBSQLSelect(SQLStatement)
		  
		  while not rsDistinctIntervals.eof
		    me.AddRow rsDistinctIntervals.Field("Interval_Name").StringValue
		    me.RowTag(me.ListCount-1)=rsDistinctIntervals.Field("Number").IntegerValue
		    rsDistinctIntervals.MoveNext
		  Wend
		  
		  me.AddRow "Finish"
		  me.RowTag(me.ListCount-1)=9999
		  me.ListIndex=0
		  
		  me.Visible=false
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbNewParticipant
	#tag Event
		Sub Action()
		  ParticipantID=0
		  InputComplete=false
		  Do 
		    ParticipantInput.ShowModal
		  Loop until InputComplete
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbEditParticipant
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if DataList_Participants.Selection.FirstSelectedRow=0 then
		    d.Icon=1
		    d.ActionButton.Caption="OK"
		    d.Message="Please select a participant record to edit."
		    b=d.ShowModal
		  else
		    ParticipantID=val(DataList_Participants.Row(DataList_Participants.Selection.FirstSelectedRow).ItemData)
		    ParticipantInput.show
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbDeleteParticipant
	#tag Event
		Sub Action()
		  dim Result As Integer
		  dim row as integer
		  dim SQLStatement as string
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if Keyboard.OptionKey then
		    d.Icon=1
		    d.CancelButton.Visible=true
		    d.CancelButton.Caption="Delete"
		    d.ActionButton.Caption="Cancel"
		    d.Message="Delete ALL the participant data?"
		    d.Explanation="Are you sure? There is no undo for this operation and all participant data will be lost."
		    b=d.ShowModal
		    if b=d.CancelButton then
		      SQLStatement="DELETE FROM participants WHERE RaceID=1 "
		      App.theDB.DBSQLExecute(SQLStatement)
		      
		      SQLStatement="DELETE FROM teammembers WHERE rowid>=0 "
		      App.theDB.DBSQLExecute(SQLStatement)
		      
		      app.theDB.DBCommit
		      DataList_Participants.RemoveRow(1,DataList_Participants.Rows)
		      DataList_Participants.Selection.Clear
		      DataList_Participants.Refresh
		    end if
		  else
		    if DataList_Participants.Selection.FirstSelectedRow=0 then
		      d.Icon=1
		      d.ActionButton.Caption="OK"
		      d.Message="Please make a selection of participant records."
		      b=d.ShowModal
		    else
		      d.Icon=1
		      d.CancelButton.Visible=true
		      d.CancelButton.Caption="Delete"
		      d.ActionButton.Caption="Cancel"
		      d.Message="Are you sure you want to delete the selected Participant(s)?"
		      d.Explanation="There is no undo for this operation."
		      b=d.ShowModal
		      if b=d.CancelButton then
		        for row=DataList_Participants.Selection.FirstSelectedRow to DataList_Participants.Selection.LastSelectedRow
		          SQLStatement="DELETE FROM participants WHERE RaceID=1 AND rowid="+str(DataList_Participants.Row(row).ItemData)
		          App.theDB.DBSQLExecute(SQLStatement)
		          
		          SQLStatement="DELETE FROM teammembers WHERE ParticipantID="+str(DataList_Participants.Row(row).ItemData)
		          App.theDB.DBSQLExecute(SQLStatement)
		        next
		        app.theDB.DBCommit
		        DataList_Participants.RemoveRow(DataList_Participants.Selection.FirstSelectedRow,DataList_Participants.Selection.LastSelectedRow-DataList_Participants.Selection.FirstSelectedRow+1)
		        DataList_Participants.Selection.Clear
		        DataList_Participants.Refresh
		      end if
		    end if
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DataList_Participants
	#tag Event
		Sub CellDblClick(CellX as Integer, CellY as Integer,x as Integer,y as Integer)
		  RowSelected=CellY
		  
		  
		  ParticipantID=val(DataList_Participants.Row(CellY).ItemData)
		  ParticipantInput.show
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub ColumnHeaderClick(Column as Integer)
		  select case Column
		    
		  case 1
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Participant_Sort="Racer_Number ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Participant_Sort="Racer_Number DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 2
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Participant_Sort="Participant_Name ASC, First_Name ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Participant_Sort="Participant_Name DESC, First_Name ASC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 3
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Participant_Sort="Representing ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Participant_Sort="Representing DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 4
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Participant_Sort="Division_Name ASC, Participant_Name ASC, First_Name ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Participant_Sort="Division_Name DESC, Participant_Name ASC, First_Name ASC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 5
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Participant_Sort="Time_Source ASC, Racer_Number ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Participant_Sort="Time_Source DESC, Racer_Number ASC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 6
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Participant_Sort="Laps_Completed ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Participant_Sort="Laps_Completed DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 7
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Participant_Sort="DNS ASC, DNF ASC, DQ ASC, Total_Time ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Participant_Sort="DNS ASC, DNF ASC, DQ ASC, Total_Time DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  end Select
		  
		  me.SortIndicatorColumn=Column
		  ExecuteQuery("Participants",ExtraQuery_Participants,Participant_Sort)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DataList_Times
	#tag Event
		Sub CellDblClick(CellX as Integer, CellY as Integer,x as Integer,y as Integer)
		  RowSelected=CellY
		  
		  
		  TimeID=str(DataList_Times.Row(CellY).ItemData)
		  TimeInput.show
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub ColumnHeaderClick(Column as Integer)
		  select case Column
		    
		  case 1
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Time_Sort="Racer_Number ASC, Use_This_Passing DESC, Interval_Number ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Time_Sort="Racer_Number DESC, Use_This_Passing DESC, Interval_Number ASC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 2
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Time_Sort="Participant_Name ASC, First_Name ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Time_Sort="Participant_Name DESC, First_Name ASC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 4
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Time_Sort="Interval_Number ASC, Racer_Number ASC, Use_This_Passing DESC"
		      me.SortedBackwardsFlag=false
		    else
		      Time_Sort="Interval_Number DESC, Racer_Number ASC, Use_This_Passing DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 5
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Time_Sort="Interval_Time ASC, Racer_Number ASC, Use_This_Passing DESC, Interval_Number ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Time_Sort="Interval_Time DESC, Racer_Number ASC, Use_This_Passing DESC, Interval_Number ASC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 6
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Time_Sort="times.Total_Time ASC, Racer_Number ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Time_Sort="times.Total_Time DESC, Racer_Number ASC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 7
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Time_Sort="Actual_Time ASC, Racer_Number ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Time_Sort="Actual_Time DESC, Racer_Number ASC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 8
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Time_Sort="Use_This_Passing ASC, Racer_Number ASC, Interval_Number ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Time_Sort="Use_This_Passing DESC, Racer_Number ASC, Interval_Number ASC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 9
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Time_Sort="TX_Code ASC, Racer_Number ASC, Use_This_Passing DESC, Interval_Number ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Time_Sort="Tx_Code DESC, Racer_Number ASC, Use_This_Passing DESC, Interval_Number ASC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  end Select
		  
		  me.SortIndicatorColumn=Column
		  ExecuteQuery("Times",ExtraQuery_Times,Time_Sort)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  Time_Sort="Racer_Number ASC, Use_This_Passing DESC, Interval_Number ASC"
		  me.SortIndicatorColumn=1
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbNewTime
	#tag Event
		Sub Action()
		  TimeID="0"
		  TimeInput.show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbEditTime
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if DataList_Times.Selection.FirstSelectedRow=0 then
		    d.Icon=1
		    d.ActionButton.Caption="OK"
		    d.Message="Please select a time record to edit."
		    b=d.ShowModal
		  else
		    
		    TimeID=str(DataList_Times.Row(DataList_Times.Selection.FirstSelectedRow).ItemData)
		    TimeInput.show
		    
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbDeleteTime
	#tag Event
		Sub Action()
		  dim rs as RecordSet
		  dim Result As Integer
		  dim row, FirstRow, LastRow as integer
		  dim SQLStatement as string
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if Keyboard.OptionKey then
		    d.Icon=1
		    d.CancelButton.Visible=true
		    d.CancelButton.Caption="Delete"
		    d.ActionButton.Caption="Cancel"
		    d.Message="Delete ALL the time data?"
		    d.Explanation="Are you sure? There is no undo for this operation and all time will be lost."
		    b=d.ShowModal
		    if b=d.CancelButton then
		      SQLStatement="DELETE FROM times WHERE RaceID=1 "
		      App.theDB.DBSQLExecute(SQLStatement)
		      app.theDB.DBCommit
		      DataList_Times.RemoveRow(1,DataList_Times.Rows)
		      DataList_Times.Refresh
		    end if
		  else
		    if DataList_Times.Selection.FirstSelectedRow=0 then
		      d.Icon=1
		      d.ActionButton.Caption="OK"
		      d.Message="Please make a selection of time records."
		      b=d.ShowModal
		    else
		      FirstRow=DataList_Times.Selection.FirstSelectedRow 
		      LastRow=DataList_Times.Selection.LastSelectedRow
		      d.Icon=1
		      d.CancelButton.Visible=true
		      d.CancelButton.Caption="Delete"
		      d.ActionButton.Caption="Cancel"
		      d.Message="Are you sure you want to delete the selected "+Chr(13)+"time(s)?"
		      d.Explanation="There is no undo for this operation."
		      b=d.ShowModal
		      if b=d.CancelButton then
		        
		        for row=FirstRow to LastRow
		          
		          if DataList_Times.CellText(4,row) = "Stop" then
		            SQLStatement="SELECT ParticipantID FROM times WHERE rowid="+DataList_Times.Row(row).ItemData
		            rs=app.theDB.DBSQLSelect(SQLStatement)
		            SQLStatement="UPDATE participants SET DNF='Y', Actual_Stop='"+app.RaceDate.SQLDate+" 00:00:00.000', Total_Time='00:00:00.000', Net_Time='00:00:00.000' WHERE rowid="+rs.Field("ParticipantID").StringValue
		            App.theDB.DBSQLExecute(SQLStatement)
		            rs.Close
		          end if
		          
		          SQLStatement="DELETE FROM times WHERE RaceID=1 AND rowid="+str(DataList_Times.Row(row).ItemData)
		          App.theDB.DBSQLExecute(SQLStatement)
		        next
		        app.theDB.DBCommit
		        DataList_Times.RemoveRow(DataList_Times.Selection.FirstSelectedRow,DataList_Times.Selection.LastSelectedRow-DataList_Times.Selection.FirstSelectedRow+1)
		        DataList_Times.Selection.Clear
		        DataList_Times.Refresh
		      end if
		    end if
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbImportTimes
	#tag Event
		Sub Action()
		  LocateImportFolder.Show
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.AddRow("ChipX® Import")
		  me.AddRow("ToolKit® Import")
		  me.AddRow("iPico® Import")
		  me.AddRow("PocketTimer® Import")
		  me.AddRow("Milliseconds Pro® Import")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Relationship_Times
	#tag Event
		Sub Open()
		  Relationship_Times.addRow "="
		  Relationship_Times.addRow ">="
		  Relationship_Times.addRow "<="
		  Relationship_Times.addRow "<>"
		  Relationship_Times.addRow "LIKE"
		  Relationship_Times.ListIndex = 0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbShowAllTimes
	#tag Event
		Sub Action()
		  ExtraQuery_Times=""
		  ExecuteQuery("Times",ExtraQuery_Times,Time_Sort)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbShowHighlightedTimes
	#tag Event
		Sub Action()
		  ShowRecords(false)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbOmitHighlightedTimes
	#tag Event
		Sub Action()
		  ShowRecords(True)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pb_query_Times
	#tag Event
		Sub Action()
		  ExecuteQuery("Times","times."+replaceall(DataBaseFields_Times.text," ","_")+" "+Relationship_Times.text+" '"+ef_query_Times.text+"'",Time_Sort)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Relationship_Transponders
	#tag Event
		Sub Open()
		  Relationship_Transponders.addRow "="
		  Relationship_Transponders.addRow ">="
		  Relationship_Transponders.addRow "<="
		  Relationship_Transponders.addRow "<>"
		  Relationship_Transponders.addRow "LIKE"
		  Relationship_Transponders.ListIndex = 0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pn_query_Transponders
	#tag Event
		Sub Action()
		  ExecuteQuery("Transponders","transponders."+replaceall(DataBaseFields_Transponders.text," ","_")+" "+Relationship_Transponders.text+" '"+ef_query_Transponders.text+"'",Transponder_Sort)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbShowAllTransponders
	#tag Event
		Sub Action()
		  ExtraQuery_TX=""
		  ExecuteQuery("Transponders",ExtraQuery_TX,Transponder_Sort)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbShowHighlightedTX
	#tag Event
		Sub Action()
		  ShowRecords(false)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbOmitHighlightedTX
	#tag Event
		Sub Action()
		  ShowRecords(True)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbTXNew
	#tag Event
		Sub Action()
		  InputComplete=false
		  Do
		    TXIDStg="0"
		    TXInput.ShowModal
		  Loop until InputComplete
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bBEditTX
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if DataList_Transponders.Selection.FirstSelectedRow=0 then
		    d.Icon=1
		    d.ActionButton.Caption="OK"
		    d.Message="Please select a transponder record to edit."
		    b=d.ShowModal
		  else
		    TXIDStg=str(DataList_Transponders.Row(DataList_Transponders.Selection.FirstSelectedRow).ItemData)
		    TXInput.show
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbDeleteTX
	#tag Event
		Sub Action()
		  dim Result As Integer
		  dim row as integer
		  dim SQLStatement as string
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if Keyboard.OptionKey then
		    d.Icon=1
		    d.CancelButton.Visible=true
		    d.CancelButton.Caption="Delete"
		    d.ActionButton.Caption="Cancel"
		    d.Message="Delete ALL the transponder data?"
		    d.Explanation="Are you sure? There is no undo for this operation and all transponder will be lost."
		    b=d.ShowModal
		    if b=d.CancelButton then
		      SQLStatement="DELETE FROM transponders WHERE RaceID=1 "
		      App.theDB.DBSQLExecute(SQLStatement)
		      app.theDB.DBCommit
		      DataList_Transponders.RemoveRow(1,DataList_Transponders.Rows)
		      DataList_Transponders.Refresh
		    end if
		  else
		    if DataList_Transponders.Selection.FirstSelectedRow=0 then
		      d.Icon=1
		      d.ActionButton.Caption="OK"
		      d.Message="Please make a selection of transponder records."
		      b=d.ShowModal
		    else
		      d.Icon=1
		      d.CancelButton.Visible=true
		      d.CancelButton.Caption="Delete"
		      d.ActionButton.Caption="Cancel"
		      d.Message="Are you sure you want to delete the selected Transponders(s)?"
		      d.Explanation="There is no undo for this operation."
		      b=d.ShowModal
		      if b=d.CancelButton then
		        for row=DataList_Transponders.Selection.FirstSelectedRow to DataList_Transponders.Selection.LastSelectedRow
		          SQLStatement="DELETE FROM transponders WHERE RaceID=1 AND rowid="+str(DataList_Transponders.Row(row).ItemData)
		          App.theDB.DBSQLExecute(SQLStatement)
		        next
		        app.theDB.DBCommit
		        DataList_Transponders.RemoveRow(DataList_Transponders.Selection.FirstSelectedRow,DataList_Transponders.Selection.LastSelectedRow-DataList_Transponders.Selection.FirstSelectedRow+1)
		        DataList_Transponders.Selection.Clear
		        DataList_Transponders.Refresh
		      end if
		    end if
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbImportTX
	#tag Event
		Sub Action()
		  LocateImportFolder.Show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbAdjustParticipantTimes
	#tag Event
		Sub Action()
		  AdjustParticipantTimes.Show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbExport
	#tag Event
		Sub Action()
		  select case me.List(me.MenuValue)
		  case "Participant Export"
		    ExportParticipantData
		  case "USA Triathlon Results Export"
		    ExportParticipantUSATData
		  case "Newspaper Results Export"
		    ExportNewspaperResults
		  case "OBRA CX Results Template"
		    ExportParticipantOBRAData
		  case "USSA Points Export"
		    ExportParticipantUSSAData
		  end select
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.AddRow("Participant Export")
		  if app.TriathlonAvailable then
		    me.AddRow("USA Triathlon Results Export")
		  end if
		  me.AddRow("Newspaper Results Export")
		  if app.CriteriumAvailable then
		    me.AddRow("OBRA CX Results Template")
		  end if
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbExportTransponders
	#tag Event
		Sub Action()
		  ExportTransponders
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbAdjustTimeTimes
	#tag Event
		Sub Action()
		  AdjustTimeTimes.Show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbExportTransponders1
	#tag Event
		Sub Action()
		  ExportDivisions
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DataList_Transponders
	#tag Event
		Sub Open()
		  ExtraQuery_TX=""
		  Transponder_Sort="transponders.Racer_Number ASC"
		  me.SortIndicatorColumn=1
		End Sub
	#tag EndEvent
	#tag Event
		Sub ColumnHeaderClick(Column as Integer)
		  select case Column
		    
		  case 1
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Transponder_Sort="transponders.Racer_Number ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Transponder_Sort="transponders.Racer_Number DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 2
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Transponder_Sort="TX_Code ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Transponder_Sort="TX_Code DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 3
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Transponder_Sort="participants.Participant_Name ASC, participants.First_Name ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Transponder_Sort="participants.Participant_Name DESC, participants.First_Name ASC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 4
		    beep
		    
		  case 5
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Transponder_Sort="Issued ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Transponder_Sort="Issued DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 6
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Transponder_Sort="Hit_Count ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Transponder_Sort="Hit_Count DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  case 7
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Transponder_Sort="Returned ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Transponder_Sort="Returned DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  end Select
		  
		  me.SortIndicatorColumn=Column
		  ExecuteQuery("Transponders",ExtraQuery_TX,Transponder_Sort)
		End Sub
	#tag EndEvent
	#tag Event
		Sub CellDblClick(CellX as Integer, CellY as Integer,x as Integer,y as Integer)
		  RowSelected=CellY
		  
		  TXIDStg=str(DataList_Transponders.Row(CellY).ItemData)
		  TXInput.show
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Relationship_Teams
	#tag Event
		Sub Open()
		  Relationship_Teams.addRow "="
		  Relationship_Teams.addRow ">="
		  Relationship_Teams.addRow "<="
		  Relationship_Teams.addRow "<>"
		  Relationship_Teams.addRow "LIKE"
		  Relationship_Teams.ListIndex = 0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pb_query_Teams
	#tag Event
		Sub Action()
		  ExecuteQuery("CCTeams",ReplaceAll(DataBaseFields_Teams.text," ","_")+" "+Relationship_Teams.text+" '"+ef_query_Teams.text+"'",Team_Sort)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbShowAllTeams
	#tag Event
		Sub Action()
		  ExtraQuery_Teams=""
		  ExecuteQuery("CCTeams",ExtraQuery_Teams,Team_Sort)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbShowSubsetTeams
	#tag Event
		Sub Action()
		  ShowRecords(false)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbOmitSubsetTeams
	#tag Event
		Sub Action()
		  ShowRecords(True)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DataList_Teams
	#tag Event
		Sub CellDblClick(CellX as Integer, CellY as Integer,x as Integer,y as Integer)
		  RowSelected=CellY
		  
		  TeamID=val(DataList_Teams.Row(CellY).ItemData)
		  TeamInput.show
		End Sub
	#tag EndEvent
	#tag Event
		Sub ColumnHeaderClick(Column as Integer)
		  select case Column
		    
		  case 1
		    if (me.SortIndicatorColumn<>Column) or (me.SortedBackwardsFlag and me.SortIndicatorColumn=Column) then
		      Team_Sort="Team_Name ASC"
		      me.SortedBackwardsFlag=false
		    else
		      Team_Sort="Team_Name DESC"
		      me.SortedBackwardsFlag=true
		    end if
		    
		  end Select
		  
		  me.SortIndicatorColumn=Column
		  ExecuteQuery("Teams",ExtraQuery_Teams,Team_Sort)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.SortIndicatorColumn=6
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbNewTeam
	#tag Event
		Sub Action()
		  TeamID=0
		  TeamInput.show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbEditTeam
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if DataList_Teams.Selection.FirstSelectedRow=0 then
		    d.Icon=1
		    d.ActionButton.Caption="OK"
		    d.Message="Please select a team record to edit."
		    b=d.ShowModal
		  else
		    DivisionID=DataList_Teams.Row(DataList_Teams.Selection.FirstSelectedRow).ItemData
		    DivisionInput.show
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbDeleteTeam
	#tag Event
		Sub Action()
		  dim Result As Integer
		  dim row as integer
		  dim SQLStatement as string
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if Keyboard.OptionKey then
		    d.Icon=1
		    d.CancelButton.Visible=true
		    d.CancelButton.Caption="Delete"
		    d.ActionButton.Caption="Cancel"
		    d.Message="Delete ALL the team data?"
		    d.Explanation="Are you sure? There is no undo for this operation and all team data will be lost."
		    b=d.ShowModal
		    if b=d.CancelButton then
		      SQLStatement="DELETE FROM CCTeams WHERE Team_Name<>'' "
		      App.theDB.DBSQLExecute(SQLStatement)
		      app.theDB.DBCommit
		      DataList_Teams.RemoveRow(1,DataList_Teams.Rows)
		      DataList_Teams.Selection.Clear
		      DataList_Teams.Refresh
		    end if
		  else
		    if DataList_Teams.Selection.FirstSelectedRow=0 then
		      d.Icon=1
		      d.ActionButton.Caption="OK"
		      d.Message="Please make a selection of team records."
		      b=d.ShowModal
		    else
		      d.Icon=1
		      d.CancelButton.Visible=true
		      d.CancelButton.Caption="Delete"
		      d.ActionButton.Caption="Cancel"
		      d.Message="Are you sure you want to delete the selected Teams(s)?"
		      d.Explanation="There is no undo for this operation."
		      b=d.ShowModal
		      if b=d.CancelButton then
		        for row=DataList_Teams.Selection.FirstSelectedRow to DataList_Teams.Selection.LastSelectedRow
		          SQLStatement="DELETE FROM CCTeams WHERE rowid="+str(DataList_Teams.Row(row).ItemData)
		          App.theDB.DBSQLExecute(SQLStatement)
		        next
		        app.theDB.DBCommit
		        DataList_Teams.RemoveRow(DataList_Teams.Selection.FirstSelectedRow,DataList_Teams.Selection.LastSelectedRow-DataList_Teams.Selection.FirstSelectedRow+1)
		        DataList_Teams.Selection.Clear
		        DataList_Teams.Refresh
		      end if
		    end if
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbDivisionTeam
	#tag Event
		Sub Action()
		  LocateImportFolder.Show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbExportTeams
	#tag Event
		Sub Action()
		  ExportTeams
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbDuplicateTeamIntervals
	#tag Event
		Sub Action()
		  DuplicateTeamIntervals
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbExportTimes
	#tag Event
		Sub Action()
		  ExportTimes
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbImportParticipants
	#tag Event
		Sub Action()
		  if me.List(me.MenuValue) <> "USSA and FIS Points" then
		    LocateImportFolder.Show
		  else
		    LocateUSSAImportFolder.Show
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.AddRow("Participant import")
		  me.AddRow("Adjustment Import")
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events AllowEarlyStarts
	#tag Event
		Sub Action()
		  app.AllowEarlyStart=me.Value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events iwLogo4
	#tag Event
		Function MouseDown(X As Integer, Y As Integer) As Boolean
		  Dim d as DragItem
		  d=New DragItem(self, Me.left, Me.top, Me.width, Me.height)
		  d.picture= Me.Image
		  d.Drag //Allow the drag
		End Function
	#tag EndEvent
	#tag Event
		Sub Open()
		  
		  dim LStFilTyp as new FileType
		  
		  LStFilTyp.Name = "image/jpeg"
		  LStFilTyp.MacType = "JPEG"
		  LStFilTyp.Extensions = "jpg;jpeg"
		  
		  Me.AcceptPictureDrop
		  Me.AcceptFileDrop(LStFilTyp.MacType)
		End Sub
	#tag EndEvent
	#tag Event
		Sub DropObject(obj As DragItem, action As Integer)
		  if Obj.FolderItemAvailable then
		    Me.Image=Nil
		    Me.Image=ScalePicture(Obj.FolderItem.OpenAsPicture,100,100)
		    Logo4=Obj.FolderItem.AbsolutePath
		  End if
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events iwLogo3
	#tag Event
		Function MouseDown(X As Integer, Y As Integer) As Boolean
		  Dim d as DragItem
		  d=New DragItem(self,Me.left, Me.top, Me.width, Me.height)
		  d.picture= Me.Image
		  d.Drag //Allow the drag
		End Function
	#tag EndEvent
	#tag Event
		Sub Open()
		  
		  dim LStFilTyp as new FileType
		  
		  LStFilTyp.Name = "image/jpeg"
		  LStFilTyp.MacType = "JPEG"
		  LStFilTyp.Extensions = "jpg;jpeg"
		  
		  Me.AcceptPictureDrop
		  Me.AcceptFileDrop(LStFilTyp.MacType)
		End Sub
	#tag EndEvent
	#tag Event
		Sub DropObject(obj As DragItem, action As Integer)
		  if Obj.FolderItemAvailable then
		    Me.Image=Nil
		    Me.Image=ScalePicture(Obj.FolderItem.OpenAsPicture,100,100)
		    Logo3=Obj.FolderItem.AbsolutePath
		  End if
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events iwLogo2
	#tag Event
		Function MouseDown(X As Integer, Y As Integer) As Boolean
		  Dim d as DragItem
		  d=New DragItem(self, Me.left, Me.top, Me.width, Me.height)
		  d.picture= Me.Image
		  d.Drag //Allow the drag
		End Function
	#tag EndEvent
	#tag Event
		Sub Open()
		  
		  dim LStFilTyp as new FileType
		  
		  LStFilTyp.Name = "image/jpeg"
		  LStFilTyp.MacType = "JPEG"
		  LStFilTyp.Extensions = "jpg;jpeg"
		  
		  Me.AcceptPictureDrop
		  Me.AcceptFileDrop(LStFilTyp.MacType)
		End Sub
	#tag EndEvent
	#tag Event
		Sub DropObject(obj As DragItem, action As Integer)
		  if Obj.FolderItemAvailable then
		    Me.Image=Nil
		    Me.Image=ScalePicture(Obj.FolderItem.OpenAsPicture,100,100)
		    Logo2=Obj.FolderItem.AbsolutePath
		  End if
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events iwLogo1
	#tag Event
		Function MouseDown(X As Integer, Y As Integer) As Boolean
		  Dim d as DragItem
		  d=New DragItem(self, Me.left, Me.top, Me.width, Me.height)
		  d.picture= Me.Image
		  d.Drag //Allow the drag
		End Function
	#tag EndEvent
	#tag Event
		Sub DropObject(obj As DragItem, action As Integer)
		  if Obj.FolderItemAvailable then
		    Me.Image=Nil
		    Me.Image=ScalePicture(Obj.FolderItem.OpenAsPicture,100,100)
		    Logo1=Obj.FolderItem.AbsolutePath
		  End if
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  
		  dim LStFilTyp as new FileType
		  
		  LStFilTyp.Name = "image/jpeg"
		  LStFilTyp.MacType = "JPEG"
		  LStFilTyp.Extensions = "jpg;jpeg"
		  
		  Me.AcceptPictureDrop
		  Me.AcceptFileDrop(LStFilTyp.MacType)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ResultType
	#tag Event
		Sub Change()
		  if me.Text=wnd_List.constFISUSSAResultType then
		    gbUSSA.Visible=true
		    
		    if tfNGB1.Text="" then
		      tfNGB1.Text="USSA"
		    end if
		    
		    if tfNGB2.Text="" then
		      tfNGB2.Text="FIS"
		    end if
		    
		    tfNGB1.Enabled=false
		    tfNGB2.Enabled=false
		    
		  else
		    gbUSSA.Visible=false
		    tfNGB1.Enabled=true
		    tfNGB2.Enabled=true
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbGenBibTag
	#tag Event
		Sub Action()
		  GetBibTagRange.show
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events slFinish
	#tag Event
		Sub ValueChanged()
		  lblFinishDelay.Text="+/- "+format(me.Value/100,"0.00")+" sec"
		  
		  RangeLow(1)=me.Value/100
		  RangeHigh(1)=me.Value/100
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events slStart
	#tag Event
		Sub ValueChanged()
		  lblStartDelay.Text=str(me.Value)+" sec"
		  RangeLow(0)=0
		  RangeHigh(0)=me.Value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbVerifyNGBPoints
	#tag Event
		Sub Action()
		  dim PointsList as new ImportUSSAFISPointsList
		  
		  PointsList.ValidateAllRacers
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DemoCheckTimer
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  d.ActionButton.Caption="OK"
		  d.Message="The demonstration period has expired"
		  d.Explanation="Thank you for trying Milliseconds Pro. The application will now close."
		  b=d.ShowModal
		  Quit
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events AutoPrint_PrintResultsTimer
	#tag Event
		Sub Action()
		  Dim Success, TimeToPrint, Selection(1) as Boolean
		  Dim rsQueryResults as RecordSet
		  Dim i,RecCount as integer
		  Dim SelectStatement, SelectionID(1), SelectionText(1) as string
		  
		  for i = 0 to ubound(AutoPrint_SelectionID)-1
		    TimeToPrint=false
		    
		    SelectStatement="SELECT Results_Printed FROM participants WHERE Results_Printed IS NULL AND DNF='N' "
		    
		    select case AutoPrint_DisplayFormat
		    case "Overall By Distance"
		      SelectStatement=SelectStatement+"RaceDistanceID = "+AutoPrint_SelectionID(i)
		      
		    case "Overall By Gender"
		      if (InStr(SelectionText(i),"Female")>0) then 'Gots to check for Female first because the male is part of Female and Male
		        SelectStatement=SelectStatement+"RaceDistanceID = "+AutoPrint_SelectionID(i)+" AND A.Gender='F' "
		        
		      elseif (InStr(SelectionText(i),"Male")>0) then
		        SelectStatement=SelectStatement+"RaceDistanceID = "+AutoPrint_SelectionID(i)+" AND A.Gender='M' "
		        
		      else
		        SelectStatement=SelectStatement+"RaceDistanceID = "+AutoPrint_SelectionID(i)+" AND A.Gender='X' "
		        
		      end if
		      
		    else
		      SelectStatement=SelectStatement+"DivisionID = "+AutoPrint_SelectionID(i)
		      
		    end select
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		    
		    RecCount=rsQueryResults.RecordCount
		    
		    if rsQueryResults.RecordCount>0 then
		      if AutoPrint_FirstPagePrinted then
		        if (rsQueryResults.RecordCount\64)>0 then
		          TimeToPrint=true
		        end if
		      else
		        if rsQueryResults.RecordCount>61 and rsQueryResults.RecordCount<125 then
		          if (rsQueryResults.RecordCount\61)>0 then
		            TimeToPrint=true
		          end if
		        elseif ((rsQueryResults.RecordCount-61)\64)>0 then
		          TimeToPrint=true
		        end if
		      end if
		    end if
		    
		    rsQueryResults.Close
		    
		    if TimeToPrint then
		      Selection(0)=AutoPrint_Selection(i)
		      SelectionID(0)=AutoPrint_SelectionID(i)
		      SelectionText(0)=AutoPrint_SelectionText(i)
		      Success=PrintSetup.PrintResults(AutoPrint_FinalResults,SelectionID,Selection,SelectionText,AutoPrint_ListIntervals,AutoPrint_ListDNF,AutoPrint_DisplayFormat,True,True)
		      PrintSetup.Close
		    end if
		  next
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events AutoPrint_Stop
	#tag Event
		Sub Action()
		  AutoPrint_PrintResultsTimer.Mode=Timer.ModeOff
		  me.Visible=false
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events AutoDiscoveryTimer
	#tag Event
		Sub Action()
		  if WindowCount=1 then
		    if app.IAmServer then
		      app.ServerAutoDiscovery=new AutoDiscovery
		      app.ServerAutoDiscovery.Bind(9090)
		      app.ServerAutoDiscovery.Register("MillisecondsProServer")
		    end if
		    AutoDiscoveryTimer.Enabled=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events UpdateNonTXTimesTimer
	#tag Event
		Sub Action()
		  UpdateNonTXTimes
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events UpdateSMSMessageCount
	#tag Event
		Sub Action()
		  dim rs as RecordSet
		  rs=app.theDB.DBSQLSelect("SELECT SMSCount FROM races")
		  SMSMessagesSent.Text="SMS sent: "+rs.Field("SMSCount").StringValue
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbSMSMessaging
	#tag Event
		Sub Action()
		  if app.SendSMS then
		    app.SendSMS=false
		    me.Caption="Start SMS Messaging"
		    UpdateSMSMessageCount.Mode=0
		  else
		    app.SendSMS=true
		    me.Caption="Stop SMS Messaging"
		    UpdateSMSMessageCount.Mode=2
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbLiveTiming
	#tag Event
		Sub Action()
		  dim n as integer
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  
		  if self.APRaceID.Text<>"" then
		    if me.Caption="Stop Live Timing" then
		      app.LiveTimingUpdateThread.Done=true
		      me.Caption="Start Live Timing"
		    else
		      
		      d.icon=MessageDialog.GraphicQuestion         //display warning icon
		      d.ActionButton.Caption="Intervals and Final Times"
		      d.CancelButton.Visible=True                 //show the Cancel button
		      d.AlternateActionButton.Visible=True        //show the "Don't Save" button
		      d.AlternateActionButton.Caption="Final Times Only"
		      d.Message="What times would you like to push?"
		      d.Explanation="You can push final times only or interval and final times."
		      
		      b=d.ShowModal                              //display the dialog
		      Select Case b                              //determine which button was pressed.
		      Case d.ActionButton
		        //user pressed Interval and Final Times
		        app.PushIntervalAndFinalTimes=True
		        app.LiveTimingUpdateThread = new LiveTimingUpdate
		        app.LiveTimingUpdateThread.Run
		        me.Caption="Stop Live Timing"
		      Case d.AlternateActionButton
		        //user pressed Final Times only
		        app.PushIntervalAndFinalTimes=True
		        app.LiveTimingUpdateThread = new LiveTimingUpdate
		        app.LiveTimingUpdateThread.Run
		        me.Caption="Stop Live Timing"
		      Case d.CancelButton
		        //user pressed Cancel
		      End select
		      
		    end if
		  else
		    n=MsgBox("An AthletePath Race ID is required before turning on Live Timing.",0+16)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbTweet
	#tag Event
		Sub Action()
		  if app.SendTweet then
		    app.SendTweet=false
		    me.Caption="Start Tweeting"
		  else
		    SelectTweetTarget.show
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OrbitsSocket
	#tag Event
		Sub Error()
		  if me.LastErrorCode=102 then
		    me.Listen
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TimesLoadedTimer
	#tag Event
		Sub Action()
		  if Ubound(App.TimeTime)>=0 then
		    TimesLoaded.Visible=true
		  else
		    TimesLoaded.Visible=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="AutoPrint_DisplayFormat"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoPrint_FinalResults"
		Group="Behavior"
		InitialValue="0"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoPrint_FirstPagePrinted"
		Group="Behavior"
		InitialValue="0"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoPrint_ListDNF"
		Group="Behavior"
		InitialValue="0"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoPrint_ListIntervals"
		Group="Behavior"
		InitialValue="0"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="dataToUpload"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="dataToUploadType"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="dimRaceDistanceRowSelected"
		Group="Behavior"
		InitialValue="0"
		Type="integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="DivisionID"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Division_Sort"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ExtraQuery_Divisions"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ExtraQuery_Participants"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ExtraQuery_Teams"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ExtraQuery_Times"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ExtraQuery_TX"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImportFilePath"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InputComplete"
		Group="Behavior"
		InitialValue="0"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Logo1"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Logo2"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Logo3"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Logo4"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="OldPanel"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ParticipantID"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Participant_Sort"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="RaceDistanceIDStg"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="RaceDistanceRowSelected"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ResultsRaceID"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="RowSelected"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Seperator"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TeamID"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TeamQueryStatement"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Team_Sort"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TimeID"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TimeSource"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Time_Sort"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TimingPointID"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transponder_Sort"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TXIDStg"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
