#tag Window
Begin Window ResultsPreview
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   676
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Results Preview"
   Visible         =   True
   Width           =   1285
   Begin ListBox lbRacerData
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   9
      ColumnsResizable=   True
      ColumnWidths    =   "65,65,255,255,60,125,125,130,0"
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   True
      EnableDragReorder=   True
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   True
      HeadingIndex    =   -1
      Height          =   587
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "Place	No	Name	Representing	Laps	Net Time	Total Time	Back	 "
      Italic          =   False
      Left            =   73
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   "0"
      ScrollbarHorizontal=   True
      ScrollBarVertical=   True
      SelectionType   =   1
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   28
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   1100
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin PushButton pbUpdate
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Save"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1173
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   636
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton pbCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Done"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1081
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   636
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin BevelButton bbSameTime
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   1
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Same Time"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   21
      HelpTag         =   "Assigns the same time to the selected participants. Does net effect Net Time."
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1185
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   62
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   80
   End
   Begin PopupMenu pmDivisions
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   102
      ListIndex       =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   634
      Underline       =   False
      Visible         =   True
      Width           =   200
   End
   Begin BevelButton bbRestTimes
      AcceptFocus     =   False
      AutoDeactivate  =   False
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Reset Times"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "Resets the total times to match the net times."
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1185
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   108
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   80
   End
   Begin TextField efCutoffPercent
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1187
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "99%"
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "05"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   198
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   80
   End
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1185
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Cutoff %:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   176
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin BevelButton bbSetCutoff
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Set Cutoff"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "Sets the cutoff from the selected racer down."
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1185
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   232
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   80
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   34
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1185
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   True
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Pairing Interval:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   287
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   68
   End
   Begin TextField efPairingInterval
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1185
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "9.99"
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "0.10"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   327
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   80
   End
   Begin BevelButton bbSetCutoff1
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Refresh"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "Sets the cutoff from the selected racer down."
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1187
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   361
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   80
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  redim ValidCharacters(12)
		  ValidCharacters(0)="."
		  ValidCharacters(1)="1"
		  ValidCharacters(2)="2"
		  ValidCharacters(3)="3"
		  ValidCharacters(4)="4"
		  ValidCharacters(5)="5"
		  ValidCharacters(6)="6"
		  ValidCharacters(7)="7"
		  ValidCharacters(8)="8"
		  ValidCharacters(9)="9"
		  ValidCharacters(10)="0"
		  ValidCharacters(11)=":"
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub CalculateCutoff(CutoffType as string)
		  dim i, ii as Integer
		  dim WinningTime as Double
		  dim CutOffTimeStg as String
		  dim CutOffTime as Double
		  dim LineSet as Boolean
		  
		  
		  StageCutoff=0
		  LineSet=false
		  WinningTime=app.ConvertTimeToSeconds(lbRacerData.Cell(0,5))
		  
		  if CutoffType="Percentage" then
		    CutOffTimeStg=app.ConvertSecondsToTime(WinningTime+(WinningTime*(val(efCutoffPercent.text)/100)),false)
		    for i=0 to lbRacerData.ListCount-1
		      
		      if lbRacerData.Cell(i,7)>=CutOffTimeStg and not(LineSet) then
		        LineSet=true
		        StageCutoff=val(lbRacerData.Cell(i,0))
		        for ii=0 to 6 
		          lbRacerData.CellBorderTop(i,ii)=listbox.BorderThickSolid
		        next
		      else
		        for ii=0 to 7
		          lbRacerData.CellBorderTop(i,ii)=listbox.BorderNone
		        next
		      end if
		      
		    next
		    
		  else
		    
		    for i=0 to lbRacerData.ListCount-1
		      
		      if lbRacerData.Selected(i) then
		        lbRacerData.Selected(i)=false
		        StageCutoff=val(lbRacerData.Cell(i,0))
		        for ii=0 to 7
		          lbRacerData.CellBorderTop(i,ii)=listbox.BorderThickSolid
		        next
		        if lbRacerData.Cell(i,6)<>"DNF" and lbRacerData.Cell(i,6)<>"DNS" and lbRacerData.Cell(i,6)<>"DQ" then
		          CutOffTime=app.ConvertTimeToSeconds(lbRacerData.Cell(i,6))
		        else
		          CutOffTime=0
		        end if
		      else
		        for ii=0 to 7
		          lbRacerData.CellBorderTop(i,ii)=listbox.BorderNone
		        next
		      end if
		      
		    next
		    if CutOffTime>0 then
		      efCutoffPercent.text=str(Round(((CutOffTime/WinningTime)-1)*100))+"%"
		    else
		      efCutoffPercent.text="0%"
		    end if
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub LoadListBox(DivisionID as string)
		  dim i,TempStageCutoff as Integer
		  dim rsQueryResults as RecordSet
		  dim SelectStatement as String
		  
		  if DivisionID="" then
		    DivisionID=str(-1)
		  end if
		  
		  lbRacerData.DeleteAllRows
		  SelectStatement="SELECT Racer_Number, Participant_Name, First_Name, Representing, participants.Gender, Laps_Completed, Total_Time, Net_Time, DNS, DNF, DQ, participants.rowid, divisions.Stage_Cutoff, divisions.RaceDistanceID "
		  SelectStatement=SelectStatement+"FROM participants JOIN divisions ON DivisionID=divisions.rowid WHERE DivisionID = '"+DivisionID+"' AND participants.RaceID =1 "
		  SelectStatement=SelectStatement+"ORDER BY DNF, Total_Time"
		  rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		  
		  if rsQueryResults.RecordCount>0 then
		    
		    Progress.Show
		    Progress.Initialize("Progress","Loading Participants...",rsQueryResults.RecordCount,-1)
		    
		    for i=1 to rsQueryResults.RecordCount
		      lbRacerData.AddRow ""
		      lbRacerData.Cell(lbRacerData.lastindex,0)=app.GetPlace("Division",rsQueryResults.Field("RaceDistanceID").stringValue,DivisionID,rsQueryResults.Field("Gender").StringValue,rsQueryResults.Field("Total_Time").stringValue,false)
		      lbRacerData.Cell(lbRacerData.lastindex,1)=rsQueryResults.Field("Racer_Number").StringValue
		      if rsQueryResults.Field("First_Name").StringValue<>"" then
		        lbRacerData.Cell(lbRacerData.lastindex,2)=rsQueryResults.Field("Participant_Name").stringValue+", "+rsQueryResults.Field("First_Name").StringValue
		      else
		        lbRacerData.Cell(lbRacerData.lastindex,2)=rsQueryResults.Field("Participant_Name").stringValue
		      end if
		      lbRacerData.Cell(lbRacerData.lastindex,3)=rsQueryResults.Field("Representing").stringValue
		      lbRacerData.Cell(lbRacerData.lastindex,4)=rsQueryResults.Field("Laps_Completed").stringValue
		      
		      lbRacerData.Cell(lbRacerData.lastindex,5)=rsQueryResults.Field("Net_Time").stringValue
		      lbRacerData.Cell(lbRacerData.lastindex,6)=rsQueryResults.Field("Total_Time").stringValue
		      
		      if rsQueryResults.Field("DQ").stringValue = "Y" then
		        lbRacerData.Cell(lbRacerData.lastindex,5)="DQ"
		        lbRacerData.Cell(lbRacerData.lastindex,6)="DQ"
		        lbRacerData.Cell(lbRacerData.lastindex,7)=""
		      elseif rsQueryResults.Field("DNS").stringValue = "Y" then
		        lbRacerData.Cell(lbRacerData.lastindex,5)="DNS"
		        lbRacerData.Cell(lbRacerData.lastindex,6)="DNS"
		        lbRacerData.Cell(lbRacerData.lastindex,7)="" 
		      elseif rsQueryResults.Field("DNF").stringValue = "Y" then
		        lbRacerData.Cell(lbRacerData.lastindex,5)="DNF"
		        lbRacerData.Cell(lbRacerData.lastindex,6)="DNF"
		        lbRacerData.Cell(lbRacerData.lastindex,7)=""
		      end if
		      
		      lbRacerData.Cell(lbRacerData.lastindex,8)=rsQueryResults.Field("rowid").stringValue
		      
		      TempStageCutoff=rsQueryResults.Field("Stage_Cutoff").IntegerValue
		      
		      rsQueryResults.moveNext
		      
		      Progress.UpdateProg1(i)
		    next
		    
		    if TempStageCutoff>0 then
		      lbRacerData.Selected(TempStageCutoff-1)=true
		      CalculateCutoff("Line")
		    else
		      CalculateCutoff("Percentage")
		    end if
		    rsQueryResults.close
		    
		    
		    Progress.Close
		    
		  end if
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private BackSpace As Boolean
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ColumnSelected As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private DeleteKey As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private DeletePosition As Integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private OldValue As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private StageCutoff As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ValidCharacters() As String
	#tag EndProperty


#tag EndWindowCode

#tag Events lbRacerData
	#tag Event
		Sub DropObject(obj As DragItem, action As Integer)
		  dim i as Integer
		  dim BaseTime, CurrentRowTime, NextRowTime, PreviousRowTime, NewTime as Double
		  
		  BaseTime=app.ConvertTimeToSeconds(lbRacerData.Cell(0,6))
		  for i=0 to lbRacerData.ListCount-1
		    if lbRacerData.Cell(i,6) <> "DNF" and lbRacerData.Cell(i,6) <> "DNS" and lbRacerData.Cell(i,6) <> "DQ" then
		      lbRacerData.Cell(i,0)=Str(i+1)
		      lbRacerData.Cell(i,7)="+"+app.ConvertSecondsToTime((app.ConvertTimeToSeconds(lbRacerData.Cell(i,5))-BaseTime),false)
		      
		      if me.Selected(i) then
		        
		        ' if the following row's time is less than the current row's time, make the current row's time 1 fraction less than following row
		        if i < me.ListCount then
		          CurrentRowTime=app.ConvertTimeToSeconds(me.Cell(i,6))
		          NextRowTime=app.ConvertTimeToSeconds(me.cell(i+1,6))
		          if NextRowTime<=CurrentRowTime then
		            select case app.PlacesToTruncate
		            case 0
		              NewTime=NextRowTime-1
		            case 1
		              NewTime=NextRowTime-0.1
		            case 2
		              NewTime=NextRowTime-0.01
		            case 3
		              NewTime=NextRowTime-0.001
		            end select
		            me.Cell(i,6)=app.ConvertSecondsToTime(NewTime,false)
		            me.Cell(i,6)=Left(me.Cell(i,6),len(me.Cell(i,6))-(3-app.PlacesToTruncate))
		          end if
		        end if
		      end if
		      
		      if i >= 1 then
		        'if the previous row's time is greater than this rows time, make this row's time 1 fraction greater than the previous row.
		        CurrentRowTime=app.ConvertTimeToSeconds(me.Cell(i,6))
		        PreviousRowTime=app.ConvertTimeToSeconds(me.cell(i-1,6))
		        if PreviousRowTime>=CurrentRowTime then
		          select case app.PlacesToTruncate
		          case 0
		            NewTime=PreviousRowTime+1
		          case 1
		            NewTime=PreviousRowTime+0.1
		          case 2
		            NewTime=PreviousRowTime+0.01
		          case 3
		            NewTime=PreviousRowTime+0.001
		          end select
		          me.Cell(i,6)=app.ConvertSecondsToTime(NewTime,false)
		          me.Cell(i,6)=Left(me.Cell(i,6),len(me.Cell(i,6))-(3-app.PlacesToTruncate))
		        end if
		      end if
		      
		      
		    end if
		  next
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  lbRacerData.ColumnAlignment(0)=Listbox.AlignCenter
		  lbRacerData.ColumnAlignment(1)=Listbox.AlignCenter
		  lbRacerData.ColumnAlignment(4)=Listbox.AlignCenter
		  lbRacerData.ColumnAlignment(5)=Listbox.AlignCenter
		  lbRacerData.ColumnAlignment(6)=Listbox.AlignCenter
		  
		End Sub
	#tag EndEvent
	#tag Event
		Function CellBackgroundPaint(g As Graphics, row As Integer, column As Integer) As Boolean
		  dim difference, FirstTime, SecondTime as Double
		  dim FirstTimeStg, SecondTimeStg as string
		  
		  if me.ListCount>1 then
		    if row>0 AND row<me.ListCount then
		      FirstTimeStg=me.cell(row-1,6)
		      SecondTimeStg=me.cell(row,6)
		      if instr(FirstTimeStg,"D")=0 and instr(SecondTimeStg, "D")=0 then
		        FirstTime=app.ConvertTimeToSeconds(FirstTimeStg)
		        SecondTime=app.ConvertTimeToSeconds(SecondTimeStg)
		        difference=abs(SecondTime-FirstTime)
		        if difference<=val(efPairingInterval.text) then
		          g.foreColor= CMY(0,0,.25)
		          g.FillRect 0,0,g.width,g.height
		        end if
		      end if
		    end if
		    
		    if row<me.ListCount-1 then
		      FirstTimeStg=me.cell(row,6)
		      SecondTimeStg=me.cell(row+1,6)
		      if instr(FirstTimeStg,"D")=0 and instr(SecondTimeStg, "D")=0 then
		        FirstTime=app.ConvertTimeToSeconds(FirstTimeStg)
		        SecondTime=app.ConvertTimeToSeconds(SecondTimeStg)
		        difference=abs(FirstTime-SecondTime)
		        if difference<=val(efPairingInterval.text) then
		          g.foreColor= CMY(0,0,.25)
		          g.FillRect 0,0,g.width,g.height
		        end if
		      end if
		    end if
		    
		  end if
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events pbUpdate
	#tag Event
		Sub Action()
		  dim i as Integer
		  dim SQLStatement as string
		  
		  for i=0 to lbRacerData.ListCount-1
		    SQLStatement="UPDATE participants SET Total_Time='"+lbRacerData.Cell(i,6)+"'"
		    if (lbRacerData.Cell(i,5)="DQ" or lbRacerData.Cell(i,5)="DNS" or lbRacerData.Cell(i,5)="DNF") and lbRacerData.Cell(i,6)<>"DQ" and lbRacerData.Cell(i,6)<>"DNS" and lbRacerData.Cell(i,6)<>"DNF" then
		      SQLStatement=SQLStatement+", DQ='N', DNS='N', DNF='N'"
		    end if
		    SQLStatement=SQLStatement+" WHERE rowid="+lbRacerData.Cell(i,8)
		    App.theDB.DBSQLExecute(SQLStatement)
		  next
		  
		  SQLStatement="UPDATE divisions SET Stage_Cutoff="+str(StageCutoff)+" WHERE rowid="+pmDivisions.RowTag(pmDivisions.ListIndex)
		  App.theDB.DBSQLExecute(SQLStatement)
		  
		  app.theDB.DBCommit
		  
		  wnd_List.ExecuteQuery("Participants",wnd_List.ExtraQuery_Participants,wnd_List.Participant_Sort)
		  
		  
		  lbRacerData.DeleteAllRows
		  
		  pmDivisions.ListIndex=0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbCancel
	#tag Event
		Sub Action()
		  ResultsPreview.close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbSameTime
	#tag Event
		Sub Action()
		  dim i as Integer
		  dim TimeString, TimeBack as string
		  
		  TimeString=""
		  TimeBack=""
		  
		  for i=0 to lbRacerData.ListCount-1
		    if lbRacerData.Selected(i) then
		      if TimeString="" then
		        TimeString=lbRacerData.Cell(i,5)
		        TimeBack=lbRacerData.Cell(i,6)
		      else
		        lbRacerData.Cell(i,5)=TimeString
		        lbRacerData.Cell(i,6)=TimeBack
		      end if
		      lbRacerData.Cell(i,0)=Str(i+1)
		    end if
		  next
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmDivisions
	#tag Event
		Sub Change()
		  efCutoffPercent.text="5"
		  LoadListBox(pmDivisions.RowTag(pmDivisions.ListIndex))
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  dim recCount as integer
		  dim rsDivisions as recordSet
		  dim res as boolean
		  
		  pmDivisions.addRow "Select Division..."
		  rsDivisions=App.theDB.DBSQLSelect("Select Division_Name, rowid FROM divisions WHERE RaceID=1 ORDER BY List_Order")
		  while not rsDivisions.EOF
		    pmDivisions.addRow rsDivisions.Field("Division_Name").stringValue
		    pmDivisions.rowTag(pmDivisions.ListCount-1)=rsDivisions.Field("rowid").integerValue
		    rsDivisions.moveNext
		  Wend
		  rsDivisions.Close
		  
		  pmDivisions.ListIndex=0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbRestTimes
	#tag Event
		Sub Action()
		  dim i as Integer
		  
		  for i=0 to lbRacerData.ListCount-1
		    if lbRacerData.Cell(i,5)<>"" then
		      lbRacerData.Cell(i,5)=lbRacerData.Cell(i,4)
		    end if
		  next
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events efCutoffPercent
	#tag Event
		Sub LostFocus()
		  CalculateCutoff("Percentage")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbSetCutoff
	#tag Event
		Sub Action()
		  CalculateCutoff("Line")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbSetCutoff1
	#tag Event
		Sub Action()
		  LoadListBox(pmDivisions.RowTag(pmDivisions.ListIndex))
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
