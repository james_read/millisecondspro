#tag Window
Begin Window AdjustParticipantTimes
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   8
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   358
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   False
   Title           =   "Untitled"
   Visible         =   True
   Width           =   482
   Begin GroupBox GroupBox1
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Start Times"
      ControlOrder    =   "0"
      Enabled         =   True
      Height          =   79
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   289
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   31
      Underline       =   False
      Visible         =   True
      Width           =   150
      Begin CheckBox cbStartAssigned
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Assigned"
         ControlOrder    =   "2"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   310
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         State           =   0
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   56
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
      Begin CheckBox cbStartActual
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Actual"
         ControlOrder    =   "3"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   310
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         State           =   0
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   81
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
   End
   Begin GroupBox GroupBox2
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Stop Times"
      ControlOrder    =   "1"
      Enabled         =   True
      Height          =   80
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   289
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   125
      Underline       =   False
      Visible         =   True
      Width           =   150
      Begin CheckBox cbStopActual
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Actual"
         ControlOrder    =   "4"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   310
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         State           =   0
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   150
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
      Begin CheckBox cbStopTotal
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Total"
         ControlOrder    =   "5"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   310
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         State           =   0
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   175
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
   End
   Begin TimeEditField tefAdjustmentTime
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      BackSpace       =   "0"
      Bold            =   False
      Border          =   True
      ControlOrder    =   "6"
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      DeleteKey       =   "0"
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   128
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Multiline       =   ""
      Password        =   False
      ReadOnly        =   False
      ScrollbarHorizontal=   ""
      ScrollbarVertical=   "True"
      Styled          =   ""
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   115
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   100
   End
   Begin Label stOPDesc
      AutoDeactivate  =   True
      Bold            =   False
      ControlOrder    =   "7"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Selectable      =   False
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "to"
      TextAlign       =   1
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   115
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   47
   End
   Begin PopupMenu pmOperation
      AutoDeactivate  =   True
      Bold            =   False
      ControlOrder    =   "8"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "Add\rSubtract"
      Italic          =   False
      Left            =   20
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   115
      Underline       =   False
      Visible         =   True
      Width           =   96
   End
   Begin Label stAdjustMessage
      AutoDeactivate  =   True
      Bold            =   False
      ControlOrder    =   "9"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   78
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   29
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   True
      Selectable      =   False
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Adjust Times"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   20.0
      TextUnit        =   0
      Top             =   23
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   248
   End
   Begin TextField efReason
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      ControlOrder    =   "10"
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   False
      Format          =   ""
      Height          =   70
      HelpTag         =   "Not used for start time adjustments. Required for Stop Time adjustments."
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   29
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Multiline       =   ""
      Password        =   False
      ReadOnly        =   False
      ScrollbarHorizontal=   ""
      ScrollbarVertical=   "True"
      Styled          =   ""
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   225
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   410
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      ControlOrder    =   "11"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   29
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Selectable      =   False
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Reason:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   193
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin PushButton pbOK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "OK"
      ControlOrder    =   "12"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   382
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   318
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton pbCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Cancel"
      ControlOrder    =   "13"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   289
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   318
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h0
		Function CalcAdjustedTime(TimeToBeAdjustedStg as string, AdjustmentTime as string, AddOrSubtract as string, DateIncluded as boolean) As string
		  Dim HasSign as Boolean
		  Dim AdjustedTime, Adjustment, TimeToBeAdjusted as Double
		  Dim FirstChar as String
		  
		  FirstChar=left(TimeToBeAdjustedStg,1)
		  if FirstChar="+" or FirstChar="-" then
		    TimeToBeAdjustedStg=Right(TimeToBeAdjustedStg,len(TimeToBeAdjustedStg)-1)
		    if FirstChar="-" then
		      TimeToBeAdjusted=app.ConvertTimeToSeconds(TimeToBeAdjustedStg)*(-1)
		    else
		      TimeToBeAdjusted=app.ConvertTimeToSeconds(TimeToBeAdjustedStg)
		    end if
		  else
		    FirstChar=""
		    TimeToBeAdjusted=app.ConvertTimeToSeconds(TimeToBeAdjustedStg)
		  end if
		  
		  Adjustment=app.ConvertTimeToSeconds(tefAdjustmentTime.text)
		  
		  if DateIncluded then
		    TimeToBeAdjusted=TimeToBeAdjusted-app.AMBOffset
		  end if
		  
		  if AddOrSubtract="Add" then
		    AdjustedTime=round((TimeToBeAdjusted+Adjustment)*1000)/1000 'make sure we get the proper fractions
		  else
		    AdjustedTime=round((TimeToBeAdjusted-Adjustment)*1000)/1000 'make sure we get the proper fractions
		  end if
		  
		  Return FirstChar+app.ConvertSecondsToTime(AdjustedTime,DateIncluded)
		End Function
	#tag EndMethod


#tag EndWindowCode

#tag Events cbStopTotal
	#tag Event
		Sub Action()
		  if (me.Value) then
		    efReason.Enabled=true
		    efReason.SetFocus
		  else
		    efReason.Text=""
		    efReason.Enabled=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events tefAdjustmentTime
	#tag Event
		Sub Open()
		  tefAdjustmentTime.text="00:00:00.000"
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmOperation
	#tag Event
		Sub Change()
		  if me.text="Add" then
		    stOPDesc.text="to"
		  else
		    stOPDesc.text="from"
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events stAdjustMessage
	#tag Event
		Sub Open()
		  me.text="Adjust times for "+wnd_List.ParticipantsFound.text+" records"
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbOK
	#tag Event
		Sub Action()
		  Dim DataEntryError as boolean
		  Dim row as integer
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  Dim recsRS as RecordSet
		  Dim AdjustedTime, CR, SQLStatement as string
		  
		  CR=chr(13)
		  
		  'Error Checking
		  If tefAdjustmentTime.text="00:00:00.000" then
		    app.DisplayDataEntryError("Adjustment time misssing.","Please enter an adjustment time.",tefAdjustmentTime)
		    DataEntryError=true
		    
		  elseif not(cbStartActual.value) and not(cbStartAssigned.value)  and not(cbStopActual.value) and not(cbStopTotal.value) then
		    d.icon=2   
		    cbStartActual.setFocus
		    d.Message="Time type to update is required"
		    d.Explanation="Please select a time type to update."
		    b=d.ShowModal
		    DataEntryError=true
		    
		  elseif cbStopTotal.Value and efReason.Text="" then
		    app.DisplayDataEntryError("Adjustment Reason is required for total time adjustments.","Please enter an adjustment reason.",efReason)
		    DataEntryError=true
		    
		  else
		    DataEntryError=false
		  end if
		  
		  if not(DataEntryError) then
		    'Confirm
		    d.icon=1   //display warning icon
		    d.ActionButton.Caption="OK"
		    d.CancelButton.Visible=True     //show the Cancel button
		    d.CancelButton.Caption="Cancel"
		    d.Message="Really adjust times for "+wnd_List.ParticipantsFound.Text+" records. There is no UNDO for this action."
		    d.Explanation=pmOperation.text+" "+stOPDesc.text+" the following times:"
		    
		    if cbStartActual.Value then
		      d.Explanation=d.Explanation+CR+"    Actual Start Times"
		    end if
		    
		    if cbStartAssigned.Value then
		      d.Explanation=d.Explanation+CR+"    Assigned Start Times"
		    end if
		    
		    if cbStopActual.Value then
		      d.Explanation=d.Explanation+CR+"    Actual Stop Times"
		    End If
		    
		    if cbStopTotal.Value then
		      d.Explanation=d.Explanation+CR+"    Total Times"
		    End If
		    b=d.ShowModal     //display the dialog
		    Select Case b //determine which button was pressed.
		    Case d.ActionButton
		      DataEntryError=false
		    Case d.CancelButton
		      DataEntryError=true
		    End select
		    
		    if not(DataEntryError) then
		      'Process the changes
		      Progress.Show
		      Progress.Initialize("Adjusting Times","",wnd_List.DataList_Participants.Rows,-1)
		      for row=1 to wnd_List.DataList_Participants.Rows
		        Progress.UpdateProg1(row)
		        
		        SQLStatement="Select Actual_Start, Assigned_Start, Actual_Stop, Total_Time, Total_Adjustment, Adjustment_Reason FROM participants WHERE RaceID=1 AND rowid="+ _
		        str(wnd_List.DataList_Participants.Row(row).ItemData)
		        recsRS=app.theDB.DBSQLSelect(SQLStatement)
		        if recsRS<>Nil then
		          SQLStatement="UPDATE participants SET "
		          if cbStartActual.Value then
		            AdjustedTime=CalcAdjustedTime(recsRS.Field("Actual_Start").stringValue,tefAdjustmentTime.text,pmOperation.text,true)
		            SQLStatement=SQLStatement+"Actual_Start='"+AdjustedTime+"', "
		          end if
		          
		          if cbStartAssigned.Value then
		            AdjustedTime=CalcAdjustedTime(recsRS.Field("Assigned_Start").stringValue,tefAdjustmentTime.text,pmOperation.text,true)
		            SQLStatement=SQLStatement+"Assigned_Start='"+AdjustedTime+"', "
		          end if
		          
		          if cbStopActual.Value then
		            AdjustedTime=CalcAdjustedTime(recsRS.Field("Actual_Stop").stringValue,tefAdjustmentTime.text,pmOperation.text,true)
		            SQLStatement=SQLStatement+"Actual_Stop='"+AdjustedTime+"', "
		          End If
		          
		          if cbStopTotal.Value then
		            AdjustedTime=CalcAdjustedTime(recsRS.Field("Total_Time").stringValue,tefAdjustmentTime.text,pmOperation.text,false)
		            SQLStatement=SQLStatement+"Total_Time='"+AdjustedTime+"', Adjustment_Applied='Y', Adjustment_Reason='"+recsRS.Field("Adjustment_Reason").stringValue+" "+efReason.text+"', Total_Adjustment='"
		            AdjustedTime=CalcAdjustedTime(recsRS.Field("Total_Adjustment").stringValue,tefAdjustmentTime.text,pmOperation.text,false)
		            SQLStatement=SQLStatement+AdjustedTime+"', "
		          End If
		          
		          SQLStatement=SQLStatement+"RaceID=1 WHERE RaceID=1 AND rowid="+str(wnd_List.DataList_Participants.Row(row).ItemData) 'RaceID assignment there so I don't have to strip the trailing comma 
		          App.theDB.DBSQLExecute(SQLStatement)
		        end if
		      next
		      app.theDB.DBCommit
		      
		      wnd_List.ExecuteQuery("Participants",wnd_List.ExtraQuery_Participants,wnd_List.Participant_Sort)
		      
		      Progress.Close
		      
		      beep
		      d.Message="Update complete."
		      d.Explanation="Don't forget to recalculate times and/or place racers, if necessary."
		      d.CancelButton.Visible=false
		      b=d.ShowModal
		      
		      self.Close
		      
		    end if
		    
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbCancel
	#tag Event
		Sub Action()
		  self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
