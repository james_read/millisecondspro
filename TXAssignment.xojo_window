#tag Window
Begin Window TXAssignment
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   554
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Transponder Assignment"
   Visible         =   True
   Width           =   772
   Begin Label RacerName
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   65
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   213
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Racer NAME"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   48.0
      TextUnit        =   0
      Top             =   100
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   539
   End
   Begin TextField RacerNumber
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   False
      Format          =   ""
      Height          =   65
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   36
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "######"
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "0"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   48.0
      TextUnit        =   0
      Top             =   24
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   164
   End
   Begin Label TXCode
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   60
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   229
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "TX-12345"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   48.0
      TextUnit        =   0
      Top             =   25
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   444
   End
   Begin Label Representing
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   188
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   36
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   True
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Representing"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   36.0
      TextUnit        =   0
      Top             =   238
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   728
   End
   Begin Label AgeDivision
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   65
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   36
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Age Division"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   48.0
      TextUnit        =   0
      Top             =   169
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   728
   End
   Begin BevelButton btnConnect
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Connect"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   False
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   46
      HelpTag         =   "Click to establish a connection using the selected connection method."
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   627
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   486
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   125
   End
   Begin BevelButton btnDeleteLastAssigned
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Delete This Assignment"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   False
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "Click to delete the transponder assignment displayed."
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   157
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   512
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   172
   End
   Begin BevelButton btnAssignment
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Assignment"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   False
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   48
      HelpTag         =   "Click to change to Transponder Verification.\rOption+Click to change to auto-increment Racer Number."
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   50
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   486
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   95
   End
   Begin Serial Serial1
      Baud            =   8
      Bits            =   3
      CTS             =   False
      DTR             =   False
      Height          =   "32"
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   6
      LockedInPosition=   False
      Parity          =   0
      Scope           =   "0"
      Stop            =   0
      TabIndex        =   "8"
      TabPanelIndex   =   "0"
      TabStop         =   True
      Top             =   509
      Width           =   "32"
      XON             =   False
   End
   Begin TCPSocket TCPSocket1
      Address         =   ""
      Height          =   "32"
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   6
      LockedInPosition=   False
      Port            =   5100
      Scope           =   "0"
      TabIndex        =   "9"
      TabPanelIndex   =   "0"
      TabStop         =   True
      Top             =   479
      Width           =   "32"
   End
   Begin Label DisplayRacerNumber
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   65
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   36
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "99999"
      TextAlign       =   1
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   48.0
      TextUnit        =   0
      Top             =   100
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   165
   End
   Begin ComboBox ConnectMethod
      AutoComplete    =   False
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   "Select the desired connection method or enter an IP Address of the timing point."
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "Select a Method..."
      Italic          =   False
      Left            =   438
      ListIndex       =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   512
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   157
   End
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   352
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Connect via:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   512
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   74
   End
   Begin TextField HitTHreshold
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   403
      LimitText       =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "20"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   488
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   34
   End
   Begin Label Hits
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   455
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   14
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Hits"
      TextAlign       =   1
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   490
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   49
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   341
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   15
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Hit Th."
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   490
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   50
   End
   Begin BevelButton btnNameAssignment
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Team Member Assignment"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   False
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "Click to change to Transponder Verification.\rOption+Click to change to auto-increment Racer Number."
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   157
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   16
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   486
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   172
   End
   Begin Label SMSMessageStatus
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   42
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   36
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   17
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "SMS Message Status"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   36.0
      TextUnit        =   0
      Top             =   438
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   728
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  ClearFields
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Sub ClearFields()
		  TXCode.text=""
		  DisplayRacerNumber.text=""
		  RacerName.text=""
		  AgeDivision.text=""
		  Representing.Text=""
		  SMSMessageStatus.Text=""
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ProcessData()
		  dim i, CRLFPos, DelimiterLength as integer
		  dim DataRecord as string
		  dim result as Boolean
		  dim TXNumber as String
		  dim Reader as new ReaderSupport
		  
		  Select Case IncomingDataSource
		  Case "Serial1"
		    IncomingData=IncomingData+Serial1.ReadAll //read the buffer
		  case "TCPSocket1"
		    IncomingData=IncomingData+TCPSocket1.ReadAll //read the buffer
		  end select
		  do
		    CRLFPos=InStr(IncomingData,Chr(13)+Chr(10)) // see if we are at the end of the record
		    DelimiterLength=2
		    if(CRLFPos=0) then
		      CRLFPos=InStr(IncomingData,Chr(13))
		      DelimiterLength=1
		    end if
		    if(CRLFPos=0) then
		      CRLFPos=InStr(IncomingData,Chr(10))
		      DelimiterLength=1
		    end if
		    if(CRLFPos>0) then // why yes we are
		      DataRecord=Left(IncomingData,CRLFPos) //pull out the data
		      IncomingData=Mid(IncomingData,CRLFPos+DelimiterLength) // save any extra that came in
		      DataRecord=LTrim(RTrim(DataRecord))
		      if Reader.ValidCRC(DataRecord) then
		        if Reader.ParseRecord(DataRecord) then
		          if Reader.PassingRecord and Reader.LastSeen = false then // it's a passing record  
		            if Reader.Type<>"ToolKit" then
		              
		              if (RacerNumber.text <> "0") or (btnAssignment.Caption="Verify") then // process the incoming record
		                TXCode.Text=Reader.TXCode
		                
		                if VerifyAndAdd(Val(RacerNumber.text),Reader.TXNumber,Reader.TXCode,Reader.Hits,Reader.LowBattery) then
		                  beep
		                else
		                  ClearFields
		                  if btnAssignment.Caption="Assignment" then
		                    TXCode.text="Failed"
		                  end if
		                end if
		              else
		                MsgBox "The Racer Number cannot be zero."
		              end if
		              
		            else
		              for i = 0 to UBound(Reader.TK_TXCode)
		                
		                if (RacerNumber.text <> "0") or (btnAssignment.Caption="Verify") then // process the incoming record
		                  TXCode.Text=Reader.TXCode
		                  
		                  if VerifyAndAdd(Val(RacerNumber.text),"",Reader.TK_TXCode(i),254,Reader.LowBattery) then
		                    beep
		                  else
		                    ClearFields
		                    if btnAssignment.Caption="Assignment" then
		                      TXCode.text="Failed"
		                    end if
		                  end if
		                else
		                  MsgBox "The Racer Number cannot be zero."
		                end if
		                
		                
		              next
		              redim Reader.TK_TXCode(0)
		              redim Reader.TK_Time(0)
		              TCPSocket1.write Reader.TK_AckStore(Reader.TK_Source,Reader.TK_MessageNumber)
		              
		            end if
		            
		          else
		            
		          end if
		        end if
		      end if
		    end if
		  Loop Until ((IncomingData="") or (CRLFPos=0))
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub TXAdd(ParticipantID as string, TXNumber as string, TXCode as string, HitCount as string)
		  dim TodaysDateTime as date
		  dim SQLStatement as string
		  
		  TodaysDateTime=new Date
		  SQLStatement = "INSERT INTO Transponders (RaceID, Racer_Number,TX_Number,TX_Code, Issued, Hit_Count) VALUES ("
		  SQLStatement = SQLStatement+"'1',"
		  SQLStatement = SQLStatement+"'"+ParticipantID+"',"
		  SQLStatement = SQLStatement+"'"+TXNumber+"',"
		  SQLStatement = SQLStatement+"'"+TXCode+"',"
		  SQLStatement = SQLStatement+"'"+TodaysDateTime.SQLDateTime+"',"
		  SQLStatement = SQLStatement+"'"+HitCount+"')"
		  App.theDB.DBSQLExecute(SQLStatement)
		  app.theDB.DBCommit
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function VerifyAndAdd(RacerNumber as integer, TXNumber As string, TXCode as string, HitCount as integer, LowBattery as Boolean) As Boolean
		  dim Success as boolean
		  dim n as integer
		  dim rsParticipant, rsTX, rsTeamMembers as RecordSet
		  dim res as Boolean
		  dim SQLStatement, fldRepresenting, Participants() as string
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  d.icon=1
		  
		  if btnAssignment.Caption="Assignment" then
		    
		    if (ReaderType="AMB") And LowBattery then
		      Beep
		      d.Message="Transponder "+TXCode+" has a low battery."
		      d.Explanation="Click OK to assign this transponder."
		      d.CancelButton.Visible=TRUE
		      d.CancelButton.Caption="OK"
		      d.ActionButton.Caption="Cancel"
		      b=d.ShowModal
		      if b=d.CancelButton then
		        Success=true
		      else
		        Success=false
		      end If
		    else
		      Success=true
		    End if
		    
		    if Success then
		      if (ReaderType="AMB") and (HitCount< val(TXAssignment.HitThreshold.Text)) then
		        Beep
		        d.Message="Transponder "+TXCode+" has a low hit count: "+str(HitCount)
		        d.Explanation="Click OK to assign this transponder."
		        d.CancelButton.Visible=TRUE
		        d.CancelButton.Caption="OK"
		        d.ActionButton.Caption="Cancel"
		        b=d.ShowModal
		        if b=d.CancelButton then
		          Success=true
		        else
		          Success=false
		        end If
		      else
		        Success=true
		      End if
		    End If
		    
		    if Success then
		      rsTX=App.theDB.DBSQLSelect("Select Racer_Number FROM transponders WHERE RaceID=1 AND Racer_Number ="+str(RacerNumber))
		      
		      if not rsTX.eof then
		        d.Message="The Racer Number "+str(RacerNumber)+" already has a transponder assigned."
		        d.Explanation="Click OK to assign this transponder too."
		        d.CancelButton.Visible=TRUE
		        d.CancelButton.Caption="OK"
		        d.ActionButton.Caption="Cancel"
		        b=d.ShowModal
		        if b=d.CancelButton then
		          Success=true
		        else
		          Success=false
		        end If
		      else
		        Success=true
		      end if
		      
		      rsTX=app.theDB.DBSQLSelect("Select Racer_Number FROM transponders WHERE RaceID=1 AND TX_Code ='"+TXCode+"'")
		      
		      if Success then
		        if rsTX.eof then
		          
		          TXAdd(str(RacerNumber),TXNumber,TXCode, str(HitCount))
		          
		          if NextRacerNumber=0 then
		            TXAssignment.RacerNumber.text="0"
		          else
		            if RacerNumber=NextRacerNumber then
		              NextRacerNumber=NextRacerNumber+1
		            else
		              NextRacerNumber=RacerNumber+1
		            end if
		            TXAssignment.RacerNumber.text=str(NextRacerNumber)
		          end if
		          TXAssignment.RacerNumber.SelStart=0
		          TXAssignment.RacerNumber.SelLength=10
		          TXAssignment.RacerNumber.SetFocus
		          Success=True
		        else
		          d.Message="The transponder "+TXCode+" is already assigned to Racer Number: "+rsTX.Field("Racer_Number").stringValue
		          d.Explanation="You cannot assign the same tranponder to two race participants." 
		          b=d.ShowModal
		          Success=false
		        end if
		      end if
		    end if
		  Else
		    Success=True
		  end if
		  
		  if Success then  
		    if btnAssignment.caption="Assignment" then
		      SQLStatement="Select participants.rowid, participants.Racer_Number, participants.Participant_Name, participants.First_Name, divisions.Division_Name, divisions.Has_Team_Members, participants.Representing, SMS_Address FROM participants "
		      SQLStatement = SQLStatement+"LEFT JOIN divisions ON participants.DivisionID = divisions.rowid "
		      SQLStatement = SQLStatement+"WHERE participants.RaceID=1 AND Racer_Number ="+str(RacerNumber)
		    else
		      SQLStatement="Select participants.rowid, transponders.Racer_Number, participants.Participant_Name, participants.First_Name, divisions.Division_Name, participants.Representing, SMS_Address "
		      SQLStatement = SQLStatement+"FROM transponders LEFT JOIN participants ON participants.Racer_Number = transponders.Racer_Number LEFT JOIN divisions on participants.DivisionID = divisions.rowid "
		      SQLStatement = SQLStatement+"WHERE transponders.TX_Code ='"+TXCode+"'"
		    end if
		    
		    rsParticipant=app.theDB.DBSQLSelect(SQLStatement)
		    if not(rsParticipant.eof) then
		      RacerNumber=rsParticipant.Field("Racer_Number").IntegerValue
		      DisplayRacerNumber.text=str(RacerNumber)
		      RacerName.Text=rsParticipant.Field("First_Name").StringValue+" "+rsParticipant.Field("Participant_Name").StringValue
		      AgeDivision.text=rsParticipant.Field("Division_Name").StringValue
		      Representing.Text=rsParticipant.Field("Representing").StringValue
		      
		      if (app.SendSMS) then
		        if rsParticipant.Field("SMS_Address").StringValue<> "" then
		          SMSMessageStatus.Text="Text Msg OK: "
		          if (instr(rsParticipant.Field("SMS_Address").StringValue,"@")=0) then
		            SMSMessageStatus.Text=SMSMessageStatus.Text+replace(format(val(rsParticipant.Field("SMS_Address").StringValue),"(###)\ ###\-####"),"|"," & ")
		          else
		            SMSMessageStatus.Text=replace(rsParticipant.Field("SMS_Address").StringValue,"|"," & ")
		          end if 
		        else
		          SMSMessageStatus.Text="Text Messaging not setup"
		        end if
		      else
		        SMSMessageStatus.Text=""
		      end if
		      
		      if rsParticipant.Field("rowid").StringValue <>"" then
		        SQLStatement="SELECT rowid FROM teamMembers WHERE ParticipantID="+rsParticipant.Field("rowid").StringValue
		        rsTeamMembers=app.theDB.DBSQLSelect(SQLStatement)
		        if not rsTeamMembers.EOF then
		          btnNameAssignment.Enabled=True
		          if btnAssignment.caption="Assignment" then
		            TXParticipantNameRequest.Show
		          end if
		        else
		          btnNameAssignment.Enabled=False
		        end if
		        rsTeamMembers.Close
		      end if
		      
		    else
		      if btnAssignment.caption="Assignment" then
		        DisplayRacerNumber.text=str(RacerNumber)
		        RacerName.Text="Transponder Assigned"
		        AgeDivision.text=""
		      else
		        DisplayRacerNumber.text=""
		        RacerName.Text=""
		        AgeDivision.text="Transponder NOT Assigned"
		      end if
		      Representing.Text=""
		    end if
		    Hits.Text=str(HitCount)
		  end if
		  
		  
		  return Success
		End Function
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected IncomingData As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected IncomingDataSource As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected NextRacerNumber As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ProcessingData As boolean
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ReaderType As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected TCPIPConnectionEstablished As boolean
	#tag EndProperty


#tag EndWindowCode

#tag Events btnConnect
	#tag Event
		Sub Action()
		  Dim i, ii, TickCount, AsciiCode as Integer
		  Dim SerialPortNameString as string
		  
		  
		  if btnConnect.Caption="Connect" then
		    if left(ConnectMethod.text,7)="Serial:" then
		      ProcessingData=True
		      IncomingDataSource="Serial1"
		      for i=0 to (System.SerialPortCount-1)
		        if InStr(ConnectMethod.text,System.SerialPort(i).name) > 0 then
		          if TargetWin32 then
		            SerialPortNameString=""
		            for ii=1 to len(System.SerialPort(i).name)
		              AsciiCode= asc(mid(System.SerialPort(i).name,ii,1))
		              if(AsciiCode>=48 and AsciiCode<=57) then
		                SerialPortNameString=SerialPortNameString+mid(System.SerialPort(i).name,ii,1)
		              end if
		            next
		            Serial1.Port=val(SerialPortNameString)-1
		          else
		            Serial1.Port = i
		          end if
		        end if
		      next
		      
		      If Serial1.Open then
		        btnConnect.Caption="Done"
		        btnConnect.HelpTag="Click when done assigning transponders."
		        RacerNumber.Enabled=true
		        RacerNumber.SelStart=0
		        RacerNumber.SelLength=5
		        RacerNumber.SetFocus
		        ReaderType="AMB"
		      Else
		        MsgBox "The serial port could not be opened."
		      End if
		    else
		      IncomingDataSource="TCPSocket1"
		      
		      if ConnectMethod.Text<>"ToolKit" then
		        
		        ProcessingData=True
		        TCPSocket1.Address=ConnectMethod.text
		        TCPsocket1.Port=5100 ' Try an AMB connection first
		        
		        TickCount=Ticks
		        TCPsocket1.Connect
		        
		        tcpIPConnectionEstablished=false
		        Do 
		          app.DoEvents
		        Loop Until TCPIPConnectionEstablished or Ticks >= TickCount+240
		        
		      else
		        
		        TickCount=Ticks
		        TCPSocket1.Port=3097
		        TCPSocket1.Listen
		        Do
		          app.DoEvents
		          if TCPSocket1.IsConnected then
		            TCPIPConnectionEstablished=true
		            App.ReaderType="ToolKit"
		            TCPSocket1.Write "@Ping@$"
		          end if
		        Loop Until TCPIPConnectionEstablished or Ticks >= TickCount+240
		        
		      end if
		      
		      
		      if TCPIPConnectionEstablished then
		        btnConnect.Caption="Done"
		        RacerNumber.Enabled=true
		        RacerNumber.SelStart=0
		        RacerNumber.SelLength=5
		        RacerNumber.SetFocus
		        ReaderType="AMB"
		      else
		        TCPsocket1.Port=10200'try iPico
		        
		        TickCount=Ticks
		        TCPsocket1.Connect
		        
		        tcpIPConnectionEstablished=false
		        Do
		          app.DoEvents
		        Loop Until TCPIPConnectionEstablished or Ticks >= TickCount+240
		        
		        if TCPIPConnectionEstablished then
		          btnConnect.Caption="Done"
		          RacerNumber.Enabled=true
		          RacerNumber.SelStart=0
		          RacerNumber.SelLength=5
		          RacerNumber.SetFocus
		          ReaderType="iPico"
		        else
		          MsgBox "Unable to establish a network connection to "+ConnectMethod.text
		        end if
		      end if
		    end if
		  else
		    
		    Select Case IncomingDataSource
		    Case "Serial1"
		      Serial1.close
		    case "TCPSocket1"
		      TCPSocket1.close
		    end select
		    TXAssignment.Close
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnDeleteLastAssigned
	#tag Event
		Sub Action()
		  dim SQLStatement as String
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  d.icon=1  
		  d.CancelButton.Visible=true
		  
		  d.ActionButton.Caption="Cancel"
		  d.CancelButton.Caption="Delete"
		  
		  d.Message="Delete Transponder Assignment for: "+TXCode.Text
		  d.Explanation="There is no UNDO for this action."
		  b=d.ShowModal  
		  
		  if b=d.CancelButton then
		    SQLStatement="DELETE FROM transponders WHERE TX_Code='"+TXCode.text+"'"
		    App.theDB.DBSQLExecute(SQLStatement)
		    
		    ClearFields
		    
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnAssignment
	#tag Event
		Sub Action()
		  dim rsTX as RecordSet
		  dim res as Boolean
		  dim n as Integer
		  dim SQLStatement as String
		  
		  if Keyboard.OptionKey then
		    if NextRacerNumber=0 then
		      SQLStatement="Select Racer_Number FROM transponders WHERE RaceID=1 ORDER BY Racer_Number DESC LIMIT 0,1"
		      rsTX=app.theDB.DBSQLSelect(SQLStatement)
		      if not rsTX.eof then
		        NextRacerNumber=rsTX.Field("Racer_Number").IntegerValue+1
		      else
		        NextRacerNumber=1
		      end if
		    else
		      NextRacerNumber=0
		    end if
		    RacerNumber.text=str(NextRacerNumber)
		    RacerNumber.SelStart=0
		    RacerNumber.SelLength=5
		    RacerNumber.SetFocus
		  else
		    NextRacerNumber=0
		    if btnAssignment.Caption="Assignment" then
		      RacerNumber.Visible=false
		      btnAssignment.Caption="Verify"
		    else
		      RacerNumber.Visible=true
		      btnAssignment.Caption="Assignment"
		      RacerNumber.SelStart=0
		      RacerNumber.SelLength=5
		      RacerNumber.SetFocus
		    end if
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Serial1
	#tag Event
		Sub DataAvailable()
		  ProcessData
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TCPSocket1
	#tag Event
		Sub Connected()
		  beep
		  TCPIPConnectionEstablished = true
		End Sub
	#tag EndEvent
	#tag Event
		Sub DataAvailable()
		  ProcessData
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ConnectMethod
	#tag Event
		Sub Change()
		  if me.text<>"Select a Method..." then
		    btnConnect.Enabled=true
		    btnDeleteLastAssigned.enabled=true
		    btnAssignment.enabled=True
		  else
		    btnConnect.Enabled=false
		    btnDeleteLastAssigned.enabled=false
		    btnAssignment.enabled=false
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  dim i as integer
		  dim stream as TextInputStream
		  
		  for i=0 to (System.SerialPortCount-1)
		    me.AddRow "Serial: "+System.SerialPort(i).Name
		  next 
		  me.AddRow "ToolKit"
		  
		  me.ListIndex=0
		End Sub
	#tag EndEvent
	#tag Event
		Sub TextChanged()
		  dim DataSource(-1) as String
		  
		  DataSource=split(me.Text,".")
		  
		  if Ubound(DataSource)=3 then
		    btnConnect.Enabled=true
		    btnDeleteLastAssigned.enabled=true
		    btnAssignment.enabled=True
		  else
		    btnConnect.Enabled=false
		    btnDeleteLastAssigned.enabled=false
		    btnAssignment.enabled=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnNameAssignment
	#tag Event
		Sub Action()
		  dim SQLStatement as String
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  d.icon=1
		  
		  if TXCode.Text<>"" then
		    TXParticipantNameRequest.Show
		  else
		    d.Message="Please make an assignment first."
		    d.Explanation="A transponder assignment must be made before assigning a team member to a transponder."
		    b=d.ShowModal
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
