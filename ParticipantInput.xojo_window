#tag Window
Begin Window ParticipantInput
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   8
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   770
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   675
   MaximizeButton  =   True
   MaxWidth        =   750
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   675
   MinimizeButton  =   True
   MinWidth        =   750
   Placement       =   0
   Resizeable      =   True
   Title           =   "Participant"
   Visible         =   True
   Width           =   856
   Begin GroupBox GroupBox2
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Time Detail"
      Enabled         =   True
      Height          =   269
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   336
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   224
      Underline       =   False
      Visible         =   True
      Width           =   500
      Begin CheckBox cbExternalUpdates
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Prevent External Updates"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   "When checked, prevents updates to times from external devices."
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   348
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   460
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   180
      End
      Begin ListBox lbIntervals
         AutoDeactivate  =   True
         AutoHideScrollbars=   True
         Bold            =   False
         Border          =   True
         ColumnCount     =   7
         ColumnsResizable=   False
         ColumnWidths    =   "0,0,50,100,175,100,50"
         DataField       =   ""
         DataSource      =   ""
         DefaultRowHeight=   -1
         Enabled         =   True
         EnableDrag      =   False
         EnableDragReorder=   False
         GridLinesHorizontal=   0
         GridLinesVertical=   0
         HasHeading      =   True
         HeadingIndex    =   -1
         Height          =   162
         HelpTag         =   ""
         Hierarchical    =   False
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         InitialValue    =   " 		N°	Name	Actual Time	Interval Time	Use"
         Italic          =   False
         Left            =   349
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         RequiresSelection=   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollBarVertical=   True
         SelectionType   =   0
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   290
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   474
         _ScrollOffset   =   0
         _ScrollWidth    =   -1
      End
      Begin Label StaticText39
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   347
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   2
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Laps Completed:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   260
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   114
      End
      Begin TextField efLapsCompleted
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   461
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   "###"
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   259
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   41
      End
      Begin BevelButton BevelButton1
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Load Times"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   ""
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   728
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   4
         TabPanelIndex   =   0
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   261
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   91
      End
      Begin CheckBox cbHidePassings
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Hide Do Not Use Passings"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   526
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   5
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   260
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   190
      End
      Begin Label StaticText52
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   550
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   6
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Average Interval Time:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   461
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   140
      End
      Begin Label stAveIntTime
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   694
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   7
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "00:00:00.000"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   461
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   94
      End
   End
   Begin GroupBox GroupBox1
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Performance"
      Enabled         =   True
      Height          =   323
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   224
      Underline       =   False
      Visible         =   True
      Width           =   306
      Begin DateTimeEditField AssignedStartA
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   140
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   255
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   175
      End
      Begin DateTimeEditField ActualStartA
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   140
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   281
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   175
      End
      Begin DateTimeEditField ActualStopA
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   140
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   307
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   175
      End
      Begin AdjustmentTimeEditField Adjustment
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   140
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   3
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   334
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   110
      End
      Begin TimeEditField TotalTime
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   140
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   4
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   360
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   110
      End
      Begin TimeEditField NetTime
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   140
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   5
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   386
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   110
      End
      Begin Label StaticText20
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   35
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   9
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Assigned Start:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   255
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   94
      End
      Begin Label StaticText21
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   35
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   10
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Actual Start:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   281
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   94
      End
      Begin Label StaticText23
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   35
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   11
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Actual Stop:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   307
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   94
      End
      Begin Label StaticText24
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   35
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   12
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Adjustment:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   334
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   94
      End
      Begin Label StaticText25
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   35
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   13
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Total Time:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   361
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   94
      End
      Begin Label StaticText26
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   31
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   14
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Net Time:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   386
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   94
      End
      Begin Label StaticText28
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   35
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   16
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Gender Place:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   493
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   94
      End
      Begin Label StaticText29
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   35
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   17
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Division Place:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   514
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   94
      End
      Begin Label OverallPlace
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   136
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   20
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "0"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   472
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   50
      End
      Begin Label GenderPlace
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   136
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   22
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "0"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   493
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   50
      End
      Begin Label DivisionPlace
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   136
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   24
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "0"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   513
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   50
      End
      Begin Label stAgeGrade
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   265
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   28
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "00.00%"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   472
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   50
      End
      Begin Label StaticText49
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   164
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   27
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Age Grade:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   472
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   94
      End
      Begin Label StaticText27
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   35
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   15
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Overall Place:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   472
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   94
      End
      Begin CheckBox cbDNS
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "DNS"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   45
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   6
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   446
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   80
      End
      Begin CheckBox cbDQ
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "DQ"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   128
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   8
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   445
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   50
      End
      Begin CheckBox cbDNF
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "DNF"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   203
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         State           =   0
         TabIndex        =   7
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   445
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   80
      End
      Begin PopupMenu pmTimeSource
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         InitialValue    =   "TX Start, TX Stop\rTX Start, A Stop\rA Start, A Stop\rA Start, TX Stop\rA Start, B Stop\rTX Start, B Stop\rB Start, B Stop\rB Start, TX Stop\rB Start, A Stop"
         Italic          =   False
         Left            =   140
         ListIndex       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   0
         TabIndex        =   29
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   413
         Underline       =   False
         Visible         =   True
         Width           =   175
      End
      Begin Label StaticText51
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   34
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   30
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Time Source:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   415
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   94
      End
   End
   Begin ComboBox cbSeedGroup
      AutoComplete    =   False
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   408
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   197
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   False
      Width           =   53
   End
   Begin TextField RacerNumber
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   125
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "#####"
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   15
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   62
   End
   Begin TextField Name
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   306
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   15
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   199
   End
   Begin TextField FirstName
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   601
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   15
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   235
   End
   Begin TextField Street
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   125
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   47
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   413
   End
   Begin TextField PostalCode
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   468
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   80
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   71
   End
   Begin TextField City
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   126
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   79
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   148
   End
   Begin TextField State
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   336
      LimitText       =   2
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   81
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   38
   End
   Begin TextField Country
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   126
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   110
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   61
   End
   Begin TextField Phone
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   634
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "###-###-####"
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   48
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   202
   End
   Begin TextField EMail
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   634
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   78
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   202
   End
   Begin DateEditField DOB
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   "Enter the date of the race."
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   126
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   144
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   90
   End
   Begin TextField Age
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   259
      LimitText       =   3
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "###"
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   14
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   145
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   38
   End
   Begin TextField Gender
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   366
      LimitText       =   1
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   15
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   145
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   35
   End
   Begin ComboBox Division
      AutoComplete    =   False
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   474
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   16
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   144
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   362
   End
   Begin TextField efNGB1License
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   610
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   18
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   173
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   85
   End
   Begin PushButton Cancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   675
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   22
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   730
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PushButton OK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "OK"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   767
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   23
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   730
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   188
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   24
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Last/Team Name:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   16
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   111
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   521
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   25
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "First Name:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   16
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   76
   End
   Begin Label StaticText3
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   224
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   26
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Age:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   145
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   30
   End
   Begin Label StaticText4
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   308
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   27
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Gender:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   145
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   50
   End
   Begin Label StaticText5
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   241
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   28
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Representing:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   112
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   90
   End
   Begin Label StaticText6
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   407
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   29
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Division:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   144
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   58
   End
   Begin Label StaticText19
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   23
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   30
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Racer Number:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   15
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   99
   End
   Begin Label stNGB1Label
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   512
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   31
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "NGB 1 Lic/Pts:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   174
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   94
   End
   Begin Label StaticText40
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   32
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Birth Date:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   145
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   88
   End
   Begin Label StaticText41
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   11
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   33
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Street:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   48
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   111
   End
   Begin Label StaticText42
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   87
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   34
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "City:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   80
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   35
   End
   Begin Label StaticText43
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   296
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   35
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "State:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   81
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   35
   End
   Begin Label StaticText44
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   385
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   36
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Postal Code:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   81
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   77
   End
   Begin Label StaticText45
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   67
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   37
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Country:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   111
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   55
   End
   Begin Label StaticText46
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   565
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   38
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Phone:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   49
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   64
   End
   Begin Label StaticText47
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   573
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   39
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "EMail:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   79
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   56
   End
   Begin TextField SMS
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   634
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   40
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   111
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   115
   End
   Begin Label StaticText48
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   572
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   41
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "SMS:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   112
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   56
   End
   Begin TextArea AdjustmentReason
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   True
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   47
      HelpTag         =   ""
      HideSelection   =   True
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LimitText       =   0
      LineHeight      =   0.0
      LineSpacing     =   1.0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Multiline       =   True
      ReadOnly        =   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollbarVertical=   True
      Styled          =   False
      TabIndex        =   42
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   574
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   306
   End
   Begin Label StaticText32
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   17
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   True
      Scope           =   0
      Selectable      =   False
      TabIndex        =   43
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Adjustment/DQ Reason:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   554
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   160
   End
   Begin GroupBox GroupBox3
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Team Members"
      Enabled         =   True
      Height          =   118
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   336
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   44
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   503
      Underline       =   False
      Visible         =   True
      Width           =   416
      Begin ListBox MemberList
         AutoDeactivate  =   True
         AutoHideScrollbars=   True
         Bold            =   False
         Border          =   True
         ColumnCount     =   2
         ColumnsResizable=   False
         ColumnWidths    =   ""
         DataField       =   ""
         DataSource      =   ""
         DefaultRowHeight=   -1
         Enabled         =   True
         EnableDrag      =   False
         EnableDragReorder=   False
         GridLinesHorizontal=   0
         GridLinesVertical=   0
         HasHeading      =   True
         HeadingIndex    =   -1
         Height          =   85
         HelpTag         =   ""
         Hierarchical    =   False
         Index           =   -2147483648
         InitialParent   =   "GroupBox3"
         InitialValue    =   ""
         Italic          =   False
         Left            =   344
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         RequiresSelection=   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollBarVertical=   True
         SelectionType   =   0
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   526
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   401
         _ScrollOffset   =   0
         _ScrollWidth    =   -1
      End
   End
   Begin BevelButton NewTeamMember
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "New"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   776
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   45
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   522
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   60
   End
   Begin BevelButton DeleteTeamMember
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Delete"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   776
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   46
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   560
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   60
   End
   Begin BevelButton BevelButton2
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Test SMS"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   24
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   761
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   47
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   109
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   75
   End
   Begin PopupMenu cbRepresenting
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   124
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   48
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   177
      Underline       =   False
      Visible         =   True
      Width           =   211
   End
   Begin TextField Representing
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   336
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   49
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   111
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   202
   End
   Begin Label StaticText50
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   35
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   50
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Scoring for:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   177
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   79
   End
   Begin Label stSeedGrouplbl
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   329
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   51
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Seed Group:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   198
      Transparent     =   False
      Underline       =   False
      Visible         =   False
      Width           =   75
   End
   Begin TextField efNGB2License
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   610
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   20
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   199
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   85
   End
   Begin Label stNGB2Label
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   516
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   52
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "NGB 2 Lic/Pts:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   200
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   91
   End
   Begin TextField efNGB1Points
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   698
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   19
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   173
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   50
   End
   Begin TextField efNGB2Points
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   698
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   21
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   199
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   50
   End
   Begin TextArea Note
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   True
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   65
      HelpTag         =   ""
      HideSelection   =   True
      Index           =   -2147483648
      Italic          =   False
      Left            =   20
      LimitText       =   0
      LineHeight      =   0.0
      LineSpacing     =   1.0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Multiline       =   True
      ReadOnly        =   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollbarVertical=   True
      Styled          =   False
      TabIndex        =   53
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   653
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   816
   End
   Begin Label StaticText321
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   17
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   True
      Scope           =   0
      Selectable      =   False
      TabIndex        =   54
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Note:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   633
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   160
   End
   Begin BevelButton bbVerifyUSSAPts
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Lookup Pts"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   757
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   55
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   173
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   79
   End
   Begin BevelButton bbVerifyFISPts
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Lookup Pts"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   757
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   56
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   198
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   79
   End
   Begin TextField efWave
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   408
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   17
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   174
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   85
   End
   Begin Label stNGB1Label1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   370
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   57
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Wave:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   175
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   34
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  dim recsRS as recordSet
		  dim rsDivisions as recordSet
		  dim recCount as integer
		  dim i as integer
		  dim SelectStatement as string
		  dim res as boolean
		  
		  OpeningWindow=true
		  ParticipantIDStg=Format(wnd_list.ParticipantID,"##########")
		  
		  'SelectStatement="Select *, divisions.RaceDistanceID FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid WHERE participants.RaceID=1 AND participants.rowid="+ParticipantIDStg
		  SelectStatement="Select * FROM participants WHERE participants.RaceID=1 AND participants.rowid="+ParticipantIDStg
		  
		  recsRS=app.theDB.DBSQLSelect(SelectStatement)
		  if not recsRS.EOF then  'not a new record
		    PopulateRepresentingComboBox(recsRS.Field("Gender").StringValue,recsRS.Field("DivisionID").StringValue)
		    NewParticipant=false
		    NewTeamMember.Enabled=true
		    DeleteTeamMember.Enabled=true
		    Name.text=recsRS.Field("Participant_Name").stringValue
		    FirstName.text=recsRS.Field("First_Name").stringValue
		    Street.Text=recsRS.Field("Street").StringValue
		    City.Text=recsRS.Field("City").StringValue
		    State.Text=recsRS.Field("State").StringValue
		    PostalCode.Text=recsRS.Field("Postal_Code").StringValue
		    Country.Text=recsRS.Field("Country").StringValue
		    EMail.Text=recsRS.Field("EMail").StringValue
		    SMS.Text=recsRS.Field("SMS_Address").StringValue
		    Phone.Text=recsRS.Field("Phone").StringValue
		    Representing.text=recsRS.Field("Representing").stringValue
		    if recsRS.Field("Birth_Date").StringValue<>"" then
		      DOB.Text=recsRS.Field("Birth_Date").StringValue
		    else
		      DOB.Text="0000-00-00"
		    end if
		    Age.text=recsRS.Field("Age").stringValue
		    RacerNumber.text=Format(recsRS.Field("Racer_Number").integerValue,"######")
		    Gender.text=recsRS.Field("Gender").stringValue
		    efNGB1License.text=recsRS.Field("NGB_License_Number").StringValue
		    efNGB2License.text=recsRS.Field("NGB2_License_Number").StringValue
		    efNGB1Points.text=recsRS.Field("NGB1_Points").StringValue
		    efNGB2Points.text=recsRS.Field("NGB2_Points").StringValue
		    
		    
		    'Select the right Representing
		    for i=0 to cbRepresenting.ListCount-1
		      cbRepresenting.ListIndex=i
		      if cbRepresenting.rowTag(i) = recsRS.Field("TeamID").integerValue then
		        RepresentingID=recsRS.Field("TeamID").StringValue
		        exit
		      end if
		    next
		    if i=cbRepresenting.ListCount then
		      cbRepresenting.ListIndex=0
		      RepresentingID="0"
		    end if
		    
		    
		    PopulateDivisionComboBox
		    
		    for i=0 to Division.ListCount-1
		      Division.ListIndex=i
		      if Division.rowTag(i) = recsRS.Field("DivisionID").integerValue then
		        exit
		      end if
		    next
		    if i=Division.ListCount then
		      Division.ListIndex=0
		    end if
		    
		    DivOld=Division.Text
		    DivOldIDStg=str(Division.RowTag(Division.ListIndex))
		    
		    efLapsCompleted.text=recsRS.field("Laps_Completed").StringValue
		    AssignedStartA.text=recsRS.Field("Assigned_Start").stringValue
		    ActualStartA.text=recsRS.Field("Actual_Start").stringValue
		    ActualStopA.text=recsRS.Field("Actual_Stop").stringValue
		    OldActualStop=ActualStopA.Text
		    Adjustment.text=recsRS.Field("Total_Adjustment").stringValue
		    TotalTime.text=recsRS.Field("Total_Time").stringValue
		    OldTotalTime=TotalTime.Text
		    NetTime.text=recsRS.Field("Net_Time").stringValue
		    
		    stAgeGrade.Text=recsRS.Field("Age_Grade").StringValue
		    
		    if recsRS.Field("DNS").stringValue="Y" then
		      cbDNS.value=true
		    else
		      cbDNS.value=false
		    end if
		    if recsRS.Field("DNF").stringValue="Y" then
		      cbDNF.value=true
		    else
		      cbDNF.value=false
		    end if
		    if recsRS.Field("DQ").stringValue="Y" then
		      cbDQ.value=true
		    else
		      cbDQ.value=false
		    end if
		    
		    if recsRS.Field("Prevent_External_Updates").stringValue="Y" then
		      cbExternalUpdates.value=true
		    else
		      cbExternalUpdates.value=false
		    end if
		    
		    AdjustmentReason.text=recsRS.Field("Adjustment_Reason").stringValue
		    
		    divisionIDStg=format(recsRS.field("DivisionID").IntegerValue,"#########")
		    
		    rsDivisions=app.theDB.DBSQLSelect("SELECT RaceDistanceID FROM divisions WHERE rowid="+divisionIDStg)
		    distanceIDStg=format(rsDivisions.field("RaceDistanceID").IntegerValue,"#########")
		    
		    efWave.Text=recsRS.Field("Wave").StringValue
		    
		    Note.Text=recsRS.Field("Note").StringValue
		    
		    SetPlaces
		    
		    select case recsRS.field("Time_Source").StringValue
		    case "TX", ""
		      pmTimeSource.ListIndex=0
		    case "A"
		      pmTimeSource.ListIndex=1
		    case "B"
		      pmTimeSource.ListIndex=2
		    end Select
		    
		  else
		    PopulateRepresentingComboBox("","")
		    NewParticipant=true
		    NewTeamMember.Enabled=false
		    DeleteTeamMember.Enabled=false
		    DOB.Text="0000-00-00"
		    Age.text="0"
		    AssignedStartA.text=app.RaceDate.SQLDate+" 00:00:00.000"
		    ActualStartA.text=app.RaceDate.SQLDate+" 00:00:00.000"
		    ActualStopA.text=app.RaceDate.SQLDate+" 00:00:00.000"
		    Adjustment.text="+00:00:00.000"
		    TotalTime.text="00:00:00.000"
		    NetTime.text="00:00:00.000"
		    cbDNS.value=true
		    cbDNF.value=true
		    cbDQ.value=false
		    cbExternalUpdates.Value=false
		    OverallPlace.text="0"
		    GenderPlace.text="0"
		    DivisionPlace.text="0"
		    efLapsCompleted.text="0"
		    
		    DivOld= "Select Division..."
		    DivOldIDStg="0"
		    
		    ParticipantIDStg="0"
		    RepresentingID="0"
		    
		    pmTimeSource.ListIndex=0
		    
		  end if
		  PopulateIntervalTimes
		  
		  MemberList.columncount=3
		  MemberList.columnwidths="0,25%,75%"
		  
		  MemberList.heading(1)="Interval"
		  MemberList.heading(2)="Name"
		  MemberList.ColumnAlignment(1)=ListBox.AlignCenter
		  MemberList.ColumnAlignment(2)=ListBox.AlignLeft
		  LoadTeamMembers
		  
		  if wnd_List.tfNGB1.Text<>"" then
		    stNGB1Label.Text=wnd_List.tfNGB1.Text+" Lic/Pts:"
		  end if
		  
		  if wnd_List.tfNGB2.Text<>"" then
		    stNGB2Label.Text=wnd_List.tfNGB2.Text+" Lic/Pts:"
		  end if
		  
		  OpeningWindow=false
		  RacerNumber.SetFocus
		  RacerNumber.SelStart=0
		  RacerNumber.SelLength=99
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Sub InsertParticipant()
		  dim SQLStatement as string
		  dim RowCount, LastRowID as integer
		  dim CurrentTime as Date
		  
		  CurrentTime=New Date
		  
		  'Build SQL Statement
		  SQLStatement="INSERT INTO participants (RaceID,DivisionID,Racer_Number,Participant_Name,First_Name, "
		  SQLStatement=SQLStatement+"Street, City, State, Postal_Code, Country, Phone, EMail, Birth_Date, "
		  SQLStatement=SQLStatement+"Gender,Age,Representing,TeamID,NGB_License_Number,NGB2_License_Number,"
		  SQLStatement=SQLStatement+"Assigned_Start,Actual_Start,Actual_Stop,Total_Time,Net_Time,"
		  SQLStatement=SQLStatement+"DNS,DNF,DQ,Prevent_External_Updates,Laps_Completed,"
		  SQLStatement=SQLStatement+"Total_Adjustment,Adjustment_Applied,Adjustment_Reason,SMS_Address,Wave,Note,DateTimeInserted)"
		  SQLStatement=SQLStatement+" VALUES ("
		  SQLStatement=SQLStatement+"1," 'RaceID
		  SQLStatement=SQLStatement+divisionIDStg+"," 'DivisionID
		  SQLStatement=SQLStatement+RacerNumber.text+","
		  SQLStatement=SQLStatement+"'"+Replace(Name.text,"'","")+"',"
		  SQLStatement=SQLStatement+"'"+Replace(FirstName.text,"'","")+"',"
		  SQLStatement=SQLStatement+"'"+Street.text+"',"
		  SQLStatement=SQLStatement+"'"+City.text+"',"
		  SQLStatement=SQLStatement+"'"+State.text+"',"
		  SQLStatement=SQLStatement+"'"+PostalCode.text+"',"
		  SQLStatement=SQLStatement+"'"+Country.text+"',"
		  SQLStatement=SQLStatement+"'"+Phone.text+"',"
		  SQLStatement=SQLStatement+"'"+EMail.text+"',"
		  SQLStatement=SQLStatement+"'"+DOB.text+"',"
		  SQLStatement=SQLStatement+"'"+Gender.text+"',"
		  SQLStatement=SQLStatement+Age.text+","
		  SQLStatement=SQLStatement+"'"+Replace(Representing.text,"'","")+"',"
		  
		  if wnd_List.ResultType.Text<>wnd_List.constCrossCountryResultType then
		    SQLStatement=SQLStatement+"0,"
		  else
		    SQLStatement=SQLStatement+RepresentingID+","
		  end if
		  
		  SQLStatement=SQLStatement+"'"+efNGB1License.text+"',"
		  SQLStatement=SQLStatement+"'"+efNGB2License.text+"',"
		  SQLStatement=SQLStatement+"'"+AssignedStartA.text+"',"
		  SQLStatement=SQLStatement+"'"+ActualStartA.text+"',"
		  SQLStatement=SQLStatement+"'"+ActualStopA.text+"',"
		  SQLStatement=SQLStatement+"'"+TotalTime.text+"',"
		  SQLStatement=SQLStatement+"'"+NetTime.text+"',"
		  
		  
		  if cbDNS.value then 
		    SQLStatement = SQLStatement + "'Y',"
		  else
		    SQLStatement = SQLStatement + "'N',"
		  end if
		  
		  if cbDNF.value then
		    SQLStatement = SQLStatement + "'Y',"
		  else
		    SQLStatement = SQLStatement + "'N',"
		  end if
		  
		  if cbDQ.value then
		    SQLStatement = SQLStatement + "'Y',"
		  else
		    SQLStatement = SQLStatement + "'N',"
		  end if
		  
		  if cbExternalUpdates.value then
		    SQLStatement = SQLStatement + "'Y',"
		  else
		    SQLStatement = SQLStatement + "'N',"
		  end if
		  
		  SQLStatement=SQLStatement+efLapsCompleted.Text+","
		  SQLStatement=SQLStatement+"'"+Adjustment.text+"',"
		  if (AdjustmentReason.Text<>"") then
		    SQLStatement=SQLStatement+"'*',"
		  else
		    SQLStatement=SQLStatement+"'',"
		  end if
		  SQLStatement=SQLStatement+"'"+AdjustmentReason.text+"',"
		  SQLStatement=SQLStatement+"'"+SMS.text+"',"
		  SQLStatement=SQLStatement+"'"+efWave.text+"',"
		  SQLStatement=SQLStatement+"'"+Note.text+"',"
		  SQLStatement=SQLStatement+"'"+CurrentTime.SQLDateTime+"'"
		  SQLStatement = SQLStatement + ")"
		  
		  App.theDB.DBSQLExecute(SQLStatement)
		  LastRowID=App.theDB.DBLastRowID
		  App.theDB.DBCommit
		  
		  
		  wnd_List.DataList_Participants.LockDrawing = true
		  wnd_List.DataList_Participants.AppendRow(RacerNumber.text)
		  RowCount=wnd_List.DataList_Participants.Rows
		  
		  if FirstName.Text<>"" then
		    wnd_List.DataList_Participants.CellText(2,RowCount)=FirstName.Text+" "+Name.Text
		  else
		    wnd_List.DataList_Participants.CellText(2,RowCount)=Name.Text
		  end if
		  
		  wnd_List.DataList_Participants.CellText(3,RowCount)=Representing.Text
		  
		  wnd_List.DataList_Participants.CellText(4,RowCount)=Division.Text
		  
		  select case pmTimeSource.Text
		  case "TX Start, TX Stop"
		    wnd_List.DataList_Participants.CellText(5,RowCount)="TT"
		  case "TX Start, A Stop"
		    wnd_List.DataList_Participants.CellText(5,RowCount)="TA"
		  case "A Start, A Stop"
		    wnd_List.DataList_Participants.CellText(5,RowCount)="AA"
		  case "A Start, TX Stop"
		    wnd_List.DataList_Participants.CellText(5,RowCount)="AT"
		  case "A Start, B Stop"
		    wnd_List.DataList_Participants.CellText(5,RowCount)="AB"
		  case "TX Start, B Stop"
		    wnd_List.DataList_Participants.CellText(5,RowCount)="TB"
		  case "B Start, B Stop"
		    wnd_List.DataList_Participants.CellText(5,RowCount)="BB"
		  case "B Start, TX Stop"
		    wnd_List.DataList_Participants.CellText(5,RowCount)="BT"
		  case "B Start, A Stop"
		    wnd_List.DataList_Participants.CellText(5,RowCount)="BA"
		  end Select
		  
		  if cbDQ.Value then
		    wnd_List.DataList_Participants.CellText(7,RowCount)="DQ"
		    
		  elseif cbDNS.Value then
		    wnd_List.DataList_Participants.CellText(7,RowCount)="DNS"
		    
		  elseif cbDNF.Value then
		    wnd_List.DataList_Participants.CellText(7,RowCount)="DNF"
		    
		  else
		    wnd_List.DataList_Participants.CellText(7,RowCount)=TotalTime.Text
		  end if
		  
		  wnd_List.DataList_Participants.Row(RowCount).ItemData=format(LastRowID,"#########")
		  
		  wnd_List.DataList_Participants.VScrollValue=RowCount
		  wnd_List.DataList_Participants.LockDrawing = false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadTeamMembers()
		  Dim recsRS as RecordSet
		  
		  MemberList.DeleteAllRows
		  
		  recsRS=App.theDB.DBSQLSelect("SELECT IntervalNumber, MemberName, rowid FROM teamMembers WHERE ParticipantID = "+ParticipantIDStg+" ORDER BY IntervalNumber")
		  while not recsRS.EOF
		    MemberList.AddRow ""
		    MemberList.Cell(MemberList.lastindex,0)=recsRS.Field("rowid").stringValue
		    MemberList.Cell(MemberList.lastindex,1)=recsRS.Field("IntervalNumber").stringValue
		    MemberList.Cell(MemberList.lastindex,2)=recsRS.Field("MemberName").stringValue
		    recsRS.moveNext
		  wend
		  recsRS.close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PopulateDivisionComboBox()
		  //Load the DivisionDistance popupmenu
		  dim recCount as integer
		  dim rsDivisions as recordSet
		  dim res as boolean
		  
		  redim DivisionList(0)
		  redim DivisionListID(0)
		  redim DivisionListDistanceID(0)
		  redim DivisionListStartTime(0)
		  
		  Division.deleteAllRows
		  
		  Division.addRow "Select Division..."
		  rsDivisions=App.theDB.DBSQLSelect("Select Division_Name, RaceDistanceID, Start_Time, rowid FROM divisions WHERE RaceID=1 AND Low_Age<="+Age.text+" AND High_Age>="+Age.text+" AND Gender='"+Gender.text+"' ORDER BY List_Order")
		  while not rsDivisions.EOF
		    DivisionList.Append rsDivisions.Field("Division_Name").stringValue
		    DivisionListID.Append rsDivisions.Field("rowid").StringValue
		    DivisionListDistanceID.Append rsDivisions.Field("RaceDistanceID").StringValue
		    DivisionListStartTime.Append rsDivisions.Field("Start_Time").StringValue
		    
		    Division.addRow rsDivisions.Field("Division_Name").stringValue
		    Division.rowTag(Division.ListCount-1)=rsDivisions.Field("rowid").integerValue
		    rsDivisions.moveNext
		  Wend
		  
		  if rsDivisions.RecordCount=1 then
		    Division.ListIndex=1
		  else
		    Division.ListIndex=0
		  end if
		  
		  rsDivisions.Close
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PopulateIntervalTimes()
		  dim SelectStatement as string
		  dim res as Boolean
		  dim recsRS,rsIntervals as RecordSet
		  dim recCount as Integer
		  dim AveTotal as Double
		  
		  AveTotal=0
		  recCount=0
		  
		  lbIntervals.DeleteAllRows
		  
		  SelectStatement="Select times.Interval_Number, participants.DivisionID, times.Actual_Time, times.Interval_Time, times.Use_This_Passing, times.rowid "
		  SelectStatement=SelectStatement+"FROM participants INNER JOIN times ON participants.rowid = times.ParticipantID "
		  SelectStatement=SelectStatement+"WHERE participants.RaceID=1 AND participants.rowid="+ParticipantIDStg
		  
		  if cbHidePassings.Value then
		    SelectStatement=SelectStatement+" AND times.Use_This_Passing='Y'"
		  end if
		  
		  SelectStatement=SelectStatement+" ORDER BY times.Use_This_Passing DESC,  times.Interval_Number ASC, times.Actual_Time ASC"
		  recsRS=app.theDB.DBSQLSelect(SelectStatement)
		  while not recsRS.EOF
		    lbIntervals.AddRow ""
		    lbIntervals.Cell(lbIntervals.lastindex,0)=recsRS.Field("rowid").stringValue 'IntervalID
		    
		    lbIntervals.Cell(lbIntervals.lastindex,1)=format(recsRS.Field("Interval_Number").IntegerValue,"000000") 'Interval Number hidden
		    select case recsRS.Field("Interval_Number").IntegerValue
		    case 0
		      lbIntervals.Cell(lbIntervals.lastindex,2)="Start"
		      
		    case 9999
		      lbIntervals.Cell(lbIntervals.lastindex,2)="Stop"
		      
		    else
		      lbIntervals.Cell(lbIntervals.lastindex,2)=recsRS.Field("Interval_Number").stringValue 'Interval number again
		      
		    end select
		    
		    SelectStatement="SELECT Interval_Name FROM intervals "
		    SelectStatement=SelectStatement+"WHERE DivisionID = "+recsRS.Field("DivisionID").StringValue
		    SelectStatement=SelectStatement+" AND Number = "+recsRS.Field("Interval_Number").StringValue
		    rsIntervals=app.theDB.DBSQLSelect(SelectStatement)
		    
		    lbIntervals.Cell(lbIntervals.lastindex,3)=rsIntervals.Field("Interval_Name").stringValue 'Interval Name
		    lbIntervals.Cell(lbIntervals.lastindex,4)=recsRS.Field("Actual_Time").stringValue 'Actual Time
		    lbIntervals.Cell(lbIntervals.lastindex,5)=recsRS.Field("Interval_Time").stringValue 'Total Time
		    if recsRS.Field("Use_This_Passing").stringValue="Y" then
		      lbIntervals.CellCheck(lbIntervals.lastindex,6)=True 'Use checkbox
		      if recsRS.Field("Interval_Number").IntegerValue>0 and recsRS.Field("Interval_Number").IntegerValue<9000 then
		        recCount=recCount+1
		        AveTotal=AveTotal+app.ConvertTimeToSeconds(recsRS.Field("Interval_Time").stringValue)
		      end if
		    else
		      lbIntervals.CellCheck(lbIntervals.lastindex,6)=False 'Use checkbox
		    end if
		    recsRS.moveNext
		  wend
		  
		  if recCount>0 then
		    stAveIntTime.Text=app.ConvertSecondsToTime((AveTotal/recCount),false)
		  else
		    stAveIntTime.Text="00:00:00.000"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PopulateRepresentingComboBox(TeamGender As string, DivisionID as String)
		  //Load the DivisionDistance popupmenu
		  dim recCount as integer
		  dim rsRecs as recordSet
		  dim res as boolean
		  
		  cbRepresenting.deleteAllRows
		  
		  if TeamGender<>"" then
		    
		    cbRepresenting.addRow "Select Team..."
		    cbRepresenting.rowTag(0)=-1
		    cbRepresenting.addRow "Non-Scoring Participant"
		    cbRepresenting.rowTag(1)=0
		    rsRecs=App.theDB.DBSQLSelect("Select Team_Name, rowid FROM ccTeams WHERE Gender = '"+TeamGender _
		    +"' AND DivisionID="+DivisionID+" ORDER BY Team_Name")
		    while not rsRecs.EOF
		      cbRepresenting.addRow rsRecs.Field("Team_Name").stringValue
		      cbRepresenting.rowTag(cbRepresenting.ListCount-1)=rsRecs.Field("rowid").integerValue
		      rsRecs.moveNext
		    Wend
		    
		    if rsRecs.RecordCount=1 then
		      cbRepresenting.ListIndex=1
		    else
		      cbRepresenting.ListIndex=0
		    end if
		    
		    rsRecs.Close
		    
		  else
		    cbRepresenting.addRow "Enter a gender first..."
		    cbRepresenting.ListIndex=0
		    
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PopulateTimes()
		  dim rsTX, rsTimes, rsParticipants, rsIntervals as RecordSet
		  dim IntervalTime, NewTotalTime, NewNetTime as string
		  dim NetTimeStg, PreviousTime, SQLStatement, TempActualStart, TempActualTime as string
		  dim NewIntervalNumber as integer
		  
		  if(NewParticipant) then
		    InsertParticipant
		    ParticipantIDStg=format(app.theDB.DBLastRowID,"#########")
		    NewParticipant=False
		  end if
		  
		  'update the ParticipantID in the times table
		  SQLStatement="SELECT TX_Code FROM transponders WHERE Racer_Number="+RacerNumber.Text
		  rsTX=App.theDB.DBSQLSelect(SQLStatement)
		  while not rsTX.EOF
		    SQLStatement="UPDATE times SET ParticipantID="+ParticipantIDStg+" WHERE TX_Code='"+rsTX.Field("TX_Code").StringValue+"'"
		    App.theDB.DBSQLExecute(SQLStatement)
		    rsTX.MoveNext
		  wend
		  
		  'update actual start time if it exists
		  SQLStatement="SELECT Actual_Time, rowid FROM times WHERE Interval_Number=0 AND ParticipantID="+ParticipantIDStg
		  SQLStatement=SQLStatement+" AND Actual_Time > '"+AssignedStartA.Text+"' ORDER BY Actual_Time DESC"
		  rsTimes=App.theDB.DBSQLSelect(SQLStatement)
		  if not rsTimes.EOF then
		    ActualStartA.Text=rsTimes.Field("Actual_Time").StringValue
		    cbDNS.Value=false
		    SQLStatement="UPDATE times SET Use_This_Passing='N' WHERE Interval_Number=0 AND ParticipantID="+ParticipantIDStg
		    App.theDB.DBSQLExecute(SQLStatement)
		    SQLStatement="UPDATE times SET Use_This_Passing='Y' WHERE rowid="+rsTimes.Field("rowid").StringValue
		    App.theDB.DBSQLExecute(SQLStatement)
		  end if
		  
		  'calculate total times
		  NetTimeStg=NetTime.text
		  TotalTime.text=app.CalculateTotalTime(AssignedStartA.text,ActualStartA.text,ActualStopA.text,Adjustment.text,NetTimeStg)
		  NetTime.text=NetTimeStg
		  
		  'update interval times
		  
		  'Update the Interval Numbers
		  SQLStatement="SELECT Actual_Time, Interval_Number, rowid FROM times WHERE RaceID=1"
		  SQLStatement=SQLStatement+" AND ParticipantID="+ParticipantIDStg
		  if app.RacersPerStart<>9998 then
		    SQLStatement=SQLStatement+" AND Interval_Number>0 AND Interval_Number<9999 "
		  else
		    SQLStatement=SQLStatement+" AND Interval_Number>0 "  'All the times for the athlete will be 9999 for crit type races
		  end if
		  SQLStatement=SQLStatement+" ORDER BY Actual_Time ASC"
		  rsIntervals=App.theDB.DBSQLSelect(SQLStatement)
		  NewIntervalNumber=0
		  PreviousTime="0000-00-00 00:00:00.000"
		  while not rsIntervals.EOF
		    if ((rsIntervals.Field("Actual_Time").StringValue > AssignedStartA.Text and app.CalculateTimesFrom="Assigned") _
		      or (rsIntervals.Field("Actual_Time").StringValue > ActualStartA.Text and app.CalculateTimesFrom="Actual")) then
		      if PreviousTime=rsIntervals.Field("Actual_Time").StringValue then
		        SQLStatement="UPDATE times SET Use_This_Passing='N'"
		        SQLStatement=SQLStatement+" WHERE rowid="+rsIntervals.Field("rowid").StringValue
		        App.theDB.DBSQLExecute(SQLStatement)
		      else
		        NewIntervalNumber=NewIntervalNumber+1
		        SQLStatement="UPDATE times SET Use_This_Passing='Y', Interval_Number="+str(NewIntervalNumber)
		        SQLStatement=SQLStatement+" WHERE rowid="+rsIntervals.Field("rowid").StringValue
		        App.theDB.DBSQLExecute(SQLStatement)
		      end if
		      PreviousTime=rsIntervals.Field("Actual_Time").StringValue
		    else
		      SQLStatement="UPDATE times SET Use_This_Passing='N'"
		      SQLStatement=SQLStatement+" WHERE rowid="+rsIntervals.Field("rowid").StringValue
		      App.theDB.DBSQLExecute(SQLStatement)
		    end if
		    rsIntervals.MoveNext
		  wend
		  rsIntervals.Close
		  
		  'Update the times
		  SQLStatement="SELECT rowid, Actual_Time, Interval_Number, ParticipantID FROM times WHERE RaceID=1"
		  SQLStatement=SQLStatement+" AND ParticipantID="+ParticipantIDStg
		  SQLStatement=SQLStatement+" AND Use_This_Passing='Y'"
		  SQLStatement=SQLStatement+" ORDER BY Interval_Number ASC"
		  rsIntervals=App.theDB.DBSQLSelect(SQLStatement)
		  while not rsIntervals.EOF
		    TempActualStart=ActualStartA.Text
		    TempActualTime=rsIntervals.Field("Actual_Time").StringValue
		    if app.CalculateTimesFrom="Assigned" then
		      NewTotalTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(rsIntervals.Field("Actual_Time").StringValue)-App.ConvertTimeToSeconds(AssignedStartA.Text),false)
		    else
		      NewTotalTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(rsIntervals.Field("Actual_Time").StringValue)-App.ConvertTimeToSeconds(ActualStartA.Text),false)
		    end if
		    
		    select case rsIntervals.Field("Interval_Number").IntegerValue
		    case 0
		      NewTotalTime="00:00:00.000"
		      IntervalTime=NewTotalTime
		    case 1
		      IntervalTime=NewTotalTime
		    else
		      if PreviousTime<>"0000-00-00 00:00:00.000" then
		        IntervalTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(NewTotalTime)-App.ConvertTimeToSeconds(PreviousTime),false)
		      else
		        IntervalTime=NewTotalTime
		      end if
		    end select
		    PreviousTime=NewTotalTime
		    
		    SQLStatement="UPDATE times SET"
		    SQLStatement=SQLStatement+" Total_Time='"+NewTotalTime+"',"
		    SQLStatement=SQLStatement+" Interval_Time='"+IntervalTime+"'"
		    SQLStatement=SQLStatement+" WHERE rowid="+rsIntervals.Field("rowid").StringValue
		    
		    App.theDB.DBSQLExecute(SQLStatement)
		    
		    rsIntervals.MoveNext
		  wend
		  rsIntervals.Close
		  
		  if app.RacersPerStart=9998 then
		    SQLStatement="UPDATE times SET Interval_Number=9999 WHERE RaceID=1 AND Interval_Number="+Str(NewIntervalNumber)+" AND ParticipantID="+ParticipantIDStg
		    App.theDB.DBSQLExecute(SQLStatement)
		    efLapsCompleted.Text=Str(NewIntervalNumber)
		  end if
		  
		  'update actual stop time if it exists
		  SQLStatement="SELECT Actual_Time, rowid FROM times WHERE Interval_Number=9999 AND ParticipantID="+ParticipantIDStg
		  SQLStatement=SQLStatement+" AND Actual_Time > '"+AssignedStartA.Text+"' ORDER BY Actual_Time ASC LIMIT "+str(app.nthTime-1)+",1"
		  rsTimes=App.theDB.DBSQLSelect(SQLStatement)
		  if not rsTimes.EOF then
		    ActualStopA.Text=rsTimes.Field("Actual_Time").StringValue
		    cbDNF.Value=false
		    cbDNS.Value=false
		    efLapsCompleted.Text="1"
		    SQLStatement="UPDATE times SET Use_This_Passing='N' WHERE Interval_Number=9999 AND ParticipantID="+ParticipantIDStg
		    App.theDB.DBSQLExecute(SQLStatement)
		    SQLStatement="UPDATE times SET Use_This_Passing='Y' WHERE rowid="+rsTimes.Field("rowid").StringValue
		    App.theDB.DBSQLExecute(SQLStatement)
		    TotalTime.text=app.CalculateTotalTime(AssignedStartA.text,ActualStartA.text,ActualStopA.text,Adjustment.text,NetTimeStg)
		  end if
		  
		  lbIntervals.DeleteAllRows
		  PopulateIntervalTimes
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SetPlaces()
		  if TotalTime.Text<>"00:00:00" AND TotalTime.Text<>"00:00:00.0" AND  TotalTime.Text<>"00:00:00.00" AND TotalTime.Text<>"00:00:00.000" then
		    OverallPlace.text=app.GetPlace("Overall",distanceIDStg,divisionIDStg,Gender.text,TotalTime.text,false,efLapsCompleted.text,efNGB2Points.text)
		    GenderPlace.text=app.GetPlace("Gender",distanceIDStg,divisionIDStg,Gender.text,TotalTime.text,false,efLapsCompleted.text,efNGB2Points.text)
		    DivisionPlace.text=app.GetPlace("Division",distanceIDStg,divisionIDStg,Gender.text,TotalTime.text,false,efLapsCompleted.text,efNGB2Points.text)
		  else
		    OverallPlace.text="0"
		    GenderPlace.text="0"
		    DivisionPlace.text="0"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateIntervalTimes(ParticipantID as String, DivisionID as String, Participant_AssignedStart as String, Participant_ActualStart as String)
		  dim i, PreviousIntervalNumber as Integer
		  dim IntervalTime, NetTimeStg, PreviousActualTime, SQLStatement, TotalTime, TEMP as string
		  dim rsIntervals as RecordSet
		  
		  NetTimeStg="00:00:00.000"
		  PreviousIntervalNumber=0
		  
		  SQLStatement="Select rowid, Interval_Number, Actual_Time FROM times WHERE ParticipantID="+ParticipantID+" ORDER BY Interval_Number"
		  rsIntervals=App.theDB.DBSQLSelect(SQLStatement)
		  
		  if not(rsIntervals.EOF) then
		    if app.CalculateTimesFrom="Assigned" then
		      PreviousActualTime=Participant_AssignedStart
		    else
		      PreviousActualTime=Participant_ActualStart
		    end if
		    
		    for i=1 to rsIntervals.RecordCount
		      if rsIntervals.Field("Interval_Number").IntegerValue > 0 and rsIntervals.Field("Interval_Number").IntegerValue <9990 then
		        TotalTime=app.CalculateTotalTime(Participant_AssignedStart,Participant_ActualStart,rsIntervals.Field("Actual_Time").StringValue,"+00:00:00.000",NetTimeStg)
		        IntervalTime=app.ConvertSecondsToTime((app.ConvertTimeToSeconds(rsIntervals.Field("Actual_Time").StringValue)-app.ConvertTimeToSeconds(PreviousActualTime)),false)
		      else
		        TotalTime="00:00:00.000"
		        IntervalTime="00:00:00.000"
		      end if
		      
		      SQLStatement = "UPDATE times SET Interval_Time='"+IntervalTime+"'"
		      SQLStatement = SQLStatement + ", Total_Time='"+TotalTime+"'"
		      SQLStatement = SQLStatement + " WHERE rowid="+rsIntervals.Field("rowid").StringValue
		      
		      App.theDB.DBSQLExecute(SQLStatement)
		      
		      PreviousActualTime=rsIntervals.Field("Actual_Time").StringValue
		      PreviousIntervalNumber=rsIntervals.Field("Interval_Number").IntegerValue
		      rsIntervals.MoveNext
		    next
		    lbIntervals.DeleteAllRows
		    PopulateIntervalTimes
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub UpdateParticipant()
		  dim SelectedRow as Integer
		  dim SQLStatement as String
		  
		  SQLStatement = "UPDATE participants SET DivisionID="+divisionIDStg
		  SQLStatement = SQLStatement + ", Racer_Number="+RacerNumber.text
		  SQLStatement = SQLStatement + ", Participant_Name='"+Replace(Name.text,"'","")+"'"
		  SQLStatement = SQLStatement + ", First_Name='"+Replace(FirstName.text,"'","")+"'"
		  SQLStatement = SQLStatement + ", Street='"+Replace(Street.text,"'","")+"'"
		  SQLStatement = SQLStatement + ", City='"+Replace(City.text,"'","")+"'"
		  SQLStatement = SQLStatement + ", State='"+Replace(State.text,"'","")+"'"
		  SQLStatement = SQLStatement + ", Postal_Code='"+Replace(PostalCode.text,"'","")+"'"
		  SQLStatement = SQLStatement + ", Country='"+Replace(Country.text,"'","")+"'"
		  SQLStatement = SQLStatement + ", Phone='"+Phone.text+"'"
		  SQLStatement = SQLStatement + ", EMail='"+EMail.text+"'"
		  SQLStatement = SQLStatement + ", Birth_Date='"+DOB.text+"'"
		  SQLStatement = SQLStatement + ", Gender='"+Gender.text+"'"
		  SQLStatement = SQLStatement + ", Age="+Age.text
		  SQLStatement = SQLStatement + ", Representing='"+Replace(Representing.text,"'","")+"'"
		  
		  if wnd_List.ResultType.Text<>wnd_List.constCrossCountryResultType then
		    SQLStatement = SQLStatement + ", TeamID=0"
		  else
		    SQLStatement = SQLStatement + ", TeamID="+RepresentingID
		  end if
		  
		  SQLStatement = SQLStatement + ", NGB_License_Number='"+efNGB1License.text+"'"
		  SQLStatement = SQLStatement + ", NGB2_License_Number='"+efNGB2License.text+"'"
		  SQLStatement = SQLStatement + ", NGB1_Points='"+efNGB1Points.text+"'"
		  SQLStatement = SQLStatement + ", NGB2_Points='"+efNGB2Points.text+"'"
		  SQLStatement = SQLStatement + ", Assigned_Start='"+AssignedStartA.text+"'"
		  SQLStatement = SQLStatement + ", Actual_Start='"+ActualStartA.text+"'"
		  SQLStatement = SQLStatement + ", Actual_Stop='"+ActualStopA.Text+"'"
		  SQLStatement = SQLStatement + ", Total_Adjustment='"+Adjustment.text+"'"
		  SQLStatement = SQLStatement + ", Total_Time='"+TotalTime.text+"'"
		  SQLStatement = SQLStatement + ", Net_Time='"+NetTime.text+"'"
		  SQLStatement = SQLStatement + ", Age_Grade='"+stAgeGrade.text+"'"
		  
		  if cbDNS.value then
		    SQLStatement = SQLStatement + ", DNS='Y'"
		  else
		    SQLStatement = SQLStatement + ", DNS='N'"
		  end if
		  
		  if cbDNF.value then 
		    SQLStatement = SQLStatement + ", DNF='Y'"
		  else
		    SQLStatement = SQLStatement + ", DNF='N'"
		  end if
		  
		  if cbDQ.value then 
		    SQLStatement = SQLStatement + ", DQ='Y'"
		  else
		    SQLStatement = SQLStatement + ", DQ='N'"
		  end if
		  
		  if cbExternalUpdates.value then
		    SQLStatement = SQLStatement + ", Prevent_External_Updates='Y'"
		  else
		    SQLStatement = SQLStatement + ", Prevent_External_Updates='N'"
		  end if
		  
		  if (AdjustmentReason.Text<>"") then
		    SQLStatement=SQLStatement+", Adjustment_Applied='*'"
		  else
		    SQLStatement=SQLStatement+", Adjustment_Applied=''"
		  end if
		  
		  select case pmTimeSource.Text
		  case "TX Start, TX Stop"
		    SQLStatement = SQLStatement + ", Time_Source='TT'"
		  case "TX Start, A Stop"
		    SQLStatement = SQLStatement + ", Time_Source='TA'"
		  case "A Start, A Stop"
		    SQLStatement = SQLStatement + ", Time_Source='AA'"
		  case "A Start, TX Stop"
		    SQLStatement = SQLStatement + ", Time_Source='AT'"
		  case "A Start, B Stop"
		    SQLStatement = SQLStatement + ", Time_Source='AB'"
		  case "TX Start, B Stop"
		    SQLStatement = SQLStatement + ", Time_Source='TB'"
		  case "B Start, TX Stop"
		    SQLStatement = SQLStatement + ", Time_Source='BT'"
		  case "B Start, A Stop"
		    SQLStatement = SQLStatement + ", Time_Source='BA'"
		  end Select
		  
		  SQLStatement = SQLStatement + ", Adjustment_Reason='"+AdjustmentReason.text+"'"
		  SQLStatement = SQLStatement + ", Laps_Completed="+efLapsCompleted.Text
		  SQLStatement = SQLStatement + ", SMS_Address='"+SMS.Text+"'"
		  SQLStatement = SQLStatement + ", UpdateFlag='Y'"
		  SQLStatement = SQLStatement + ", Wave='"+efWave.Text+"'"
		  SQLStatement = SQLStatement + ", Note='"+Note.Text+"'"
		  
		  SQLStatement = SQLStatement + " WHERE rowid="+ParticipantIDStg
		  
		  App.theDB.DBSQLExecute(SQLStatement)
		  App.theDB.DBCommit
		  
		  SelectedRow=wnd_List.DataList_Participants.VScrollValue
		  
		  wnd_List.DataList_Participants.LockDrawing = true
		  
		  wnd_List.DataList_Participants.CellText(1,wnd_List.RowSelected)=RacerNumber.Text
		  
		  if FirstName.Text<>"" then
		    wnd_List.DataList_Participants.CellText(2,wnd_List.RowSelected)=FirstName.Text+" "+Name.Text
		  else
		    wnd_List.DataList_Participants.CellText(2,wnd_List.RowSelected)=Name.Text
		  end if
		  
		  wnd_List.DataList_Participants.CellText(3,wnd_List.RowSelected)=Representing.Text
		  
		  wnd_List.DataList_Participants.CellText(4,wnd_List.RowSelected)=Division.Text
		  
		  select case pmTimeSource.Text
		  case "TX Start, TX Stop"
		    wnd_List.DataList_Participants.CellText(5,wnd_List.RowSelected)="TT"
		  case "TX Start, A Stop"
		    wnd_List.DataList_Participants.CellText(5,wnd_List.RowSelected)="TA"
		  case "A Start, A Stop"
		    wnd_List.DataList_Participants.CellText(5,wnd_List.RowSelected)="AA"
		  case "A Start, TX Stop"
		    wnd_List.DataList_Participants.CellText(5,wnd_List.RowSelected)="AT"
		  case "A Start, B Stop"
		    wnd_List.DataList_Participants.CellText(5,wnd_List.RowSelected)="AB"
		  case "TX Start, B Stop"
		    wnd_List.DataList_Participants.CellText(5,wnd_List.RowSelected)="TB"
		  case "B Start, B Stop"
		    wnd_List.DataList_Participants.CellText(5,wnd_List.RowSelected)="BB"
		  case "B Start, TX Stop"
		    wnd_List.DataList_Participants.CellText(5,wnd_List.RowSelected)="BT"
		  case "B Start, A Stop"
		    wnd_List.DataList_Participants.CellText(5,wnd_List.RowSelected)="BA"
		  end Select
		  
		  if cbDQ.Value then
		    wnd_List.DataList_Participants.CellText(7,wnd_List.RowSelected)="DQ"
		    
		  elseif cbDNS.Value then
		    wnd_List.DataList_Participants.CellText(7,wnd_List.RowSelected)="DNS"
		    
		  elseif cbDNF.Value then
		    wnd_List.DataList_Participants.CellText(7,wnd_List.RowSelected)="DNF"
		    
		  else
		    wnd_List.DataList_Participants.CellText(7,wnd_List.RowSelected)=app.StripLeadingZeros(TotalTime.Text)
		  end if
		  
		  wnd_List.DataList_Participants.LockDrawing=false
		  wnd_List.DataList_Participants.Selection.Clear
		  wnd_List.DataList_Participants.VScrollValue=SelectedRow
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		#tag Note
			  dim res,Update as Boolean
			  dim recCount as Integer
			  dim NetTimeStg as string
			  Dim d as New MessageDialog  //declare the MessageDialog object
			  Dim b as MessageDialogButton //for handling the result
			  
			  if not(OpeningWindow) then
			    
			    divisionIDStg=format(me.rowTag(me.ListIndex),"#########")
			    
			    recsRS=app.theDB.DBSQLSelect("Select RaceDistanceID, Actual_Start_Time FROM divisions WHERE RaceID=1 AND rowid="+divisionIDStg)
			    if not(recsRS.eof) then
			      distanceIDStg=recsRS.field("RaceDistanceID").StringValue
			      if (AssignedStartA.text<>recsRS.Field("Actual_Start_Time").StringValue) and (AssignedStartA.text<>app.RaceDate.SQLDate+" 00:00:00.000") and (recsRS.Field("Actual_Start_Time").StringValue<>app.RaceDate.SQLDate+" 00:00:00.000") Then
			        d.icon=1   //display Caution icon
			        d.CancelButton.Visible=true
			        d.ActionButton.Caption="Yes"
			        d.CancelButton.Caption="No"
			        d.Message="Update the Participant's Assigned Start times?"
			        d.Explanation="Click the 'Yes' button to update the participants assigned start time with the acutal start time ("+recsRS.Field("Actual_Start_Time").StringValue+" for this division."
			        b=d.ShowModal
			        if b=d.ActionButton then
			          Update=true
			        else
			          Update=false
			        end if
			      else
			        Update=true
			      end if
			      
			      if Update then
			        if instr(ActualStopA.text,"00:00:00.000")=0 then
			          AssignedStartA.text=recsRS.Field("Actual_Start_Time").StringValue
			          NetTimeStg=NetTime.text
			          TotalTime.text=app.CalculateTotalTime(AssignedStartA.text,ActualStartA.text,ActualStopA.text,Adjustment.text,NetTimeStg)
			        end if
			      end if
			    end if
			    recsRS.Close
			  end if
		#tag EndNote
		dimrecsRS As RecordSet
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected distanceIDStg As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected divisionIDStg As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected DivisionList() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected DivisionListDistanceID() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected DivisionListID() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected DivisionListStartTime() As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected DivOld As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected DivOldIDStg As String
	#tag EndProperty

	#tag Property, Flags = &h0
		MemberID As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		MemberRow As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected NewParticipant As boolean
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected notDisplayed As boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private OldActualStop As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private OldTotalTime As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected oldValue As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected OpeningWindow As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		ParticipantIDStg As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected RepresentingID As string
	#tag EndProperty


#tag EndWindowCode

#tag Events lbIntervals
	#tag Event
		Sub Open()
		  lbIntervals.ColumnAlignment(2)=ListBox.AlignCenter
		  lbIntervals.ColumnType(6)=2 'Make column 6 an CheckBox
		End Sub
	#tag EndEvent
	#tag Event
		Sub CellAction(row As Integer, column As Integer)
		  dim SQLStatement as string
		  
		  if column=6 then
		    if lbIntervals.CellCheck(row,6) then
		      SQLStatement="UPDATE times SET Use_This_Passing='Y' WHERE rowid="+lbIntervals.Cell(row,0)
		    else
		      SQLStatement="UPDATE times SET Use_This_Passing='N' WHERE rowid="+lbIntervals.Cell(row,0)
		    end if
		    App.theDB.DBSQLExecute(SQLStatement)
		    UpdateIntervalTimes(ParticipantIDStg,divisionIDStg,AssignedStartA.text,ActualStartA.Text)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events efLapsCompleted
	#tag Event
		Sub LostFocus()
		  if oldValue<>FirstName.text then
		    FirstName.text=titlecase(FirstName.text)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BevelButton1
	#tag Event
		Sub Action()
		  PopulateTimes
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbHidePassings
	#tag Event
		Sub Action()
		  PopulateIntervalTimes
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events AssignedStartA
	#tag Event
		Sub LostFocus()
		  dim NetTimeStg, SQLStatement as string
		  dim rsDistance as RecordSet
		  NetTimeStg=NetTime.text
		  TotalTime.text=app.CalculateTotalTime(AssignedStartA.text,ActualStartA.text,ActualStopA.text,Adjustment.text,NetTimeStg)
		  NetTime.text=NetTimeStg
		  
		  SQLStatement="SELECT Age_Grade_Distance FROM divisions INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		  SQLStatement = SQLStatement + "WHERE divisions.rowid ="+divisionIDStg
		  rsDistance=app.theDB.DBSQLSelect(SQLStatement)
		  stAgeGrade.Text=App.CalculateAgeGrade(Gender.text,val(Age.Text),rsDistance.Field("Age_Grade_Distance").StringValue,TotalTime.Text)
		  rsDistance.Close
		  
		  UpdateIntervalTimes(ParticipantIDStg,divisionIDStg,AssignedStartA.text,ActualStartA.Text)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ActualStartA
	#tag Event
		Sub LostFocus()
		  dim NetTimeStg, SQLStatement as string
		  dim rsDistance as RecordSet
		  NetTimeStg=NetTime.text
		  TotalTime.text=app.CalculateTotalTime(AssignedStartA.text,ActualStartA.text,ActualStopA.text,Adjustment.text,NetTimeStg)
		  NetTime.text=NetTimeStg
		  
		  SQLStatement="SELECT Age_Grade_Distance FROM divisions INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		  SQLStatement = SQLStatement + "WHERE divisions.rowid ="+divisionIDStg
		  rsDistance=app.theDB.DBSQLSelect(SQLStatement)
		  stAgeGrade.Text=App.CalculateAgeGrade(Gender.text,val(Age.Text),rsDistance.Field("Age_Grade_Distance").StringValue,TotalTime.Text)
		  rsDistance.Close
		  
		  UpdateIntervalTimes(ParticipantIDStg,divisionIDStg,AssignedStartA.text,ActualStartA.Text)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ActualStopA
	#tag Event
		Sub LostFocus()
		  dim NetTimeStg, SQLStatement as string
		  dim rsDistance as RecordSet
		  NetTimeStg=NetTime.text
		  TotalTime.text=app.CalculateTotalTime(AssignedStartA.text,ActualStartA.text,ActualStopA.text,Adjustment.text,NetTimeStg)
		  NetTime.text=NetTimeStg
		  
		  SQLStatement="SELECT Age_Grade_Distance FROM divisions INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		  SQLStatement = SQLStatement + "WHERE divisions.rowid ="+divisionIDStg
		  rsDistance=app.theDB.DBSQLSelect(SQLStatement)
		  stAgeGrade.Text=App.CalculateAgeGrade(Gender.text,val(Age.Text),rsDistance.Field("Age_Grade_Distance").StringValue,TotalTime.Text)
		  rsDistance.Close
		  
		  if InStr(OldActualStop,"00:00:00")>0 then
		    cbDNS.value=false
		    cbDNF.value=false
		    efLapsCompleted.Text="1"
		    OldActualStop=ActualStopA.text
		    OldTotalTime=TotalTime.Text
		  end if
		  
		  SetPlaces
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Adjustment
	#tag Event
		Sub LostFocus()
		  dim NetTimeStg, SQLStatement as string
		  dim rsDistance as RecordSet
		  NetTimeStg=NetTime.text
		  TotalTime.text=app.CalculateTotalTime(AssignedStartA.text,ActualStartA.text,ActualStopA.text,Adjustment.text,NetTimeStg)
		  NetTime.text=NetTimeStg
		  
		  SQLStatement="SELECT Age_Grade_Distance FROM divisions INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		  SQLStatement = SQLStatement + "WHERE divisions.rowid ="+divisionIDStg
		  rsDistance=app.theDB.DBSQLSelect(SQLStatement)
		  stAgeGrade.Text=App.CalculateAgeGrade(Gender.text,val(Age.Text),rsDistance.Field("Age_Grade_Distance").StringValue,TotalTime.Text)
		  rsDistance.Close
		  
		  SetPlaces
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TotalTime
	#tag Event
		Sub LostFocus()
		  dim NetTimeStg, SQLStatement as string
		  dim rsDistance as RecordSet
		  
		  if InStr(TotalTime.Text,"00:00:00")=0 then 'only want the caclulation to occur is there is a total time
		    
		    if InStr(OldTotalTime,"00:00:00")>0 then
		      cbDNS.value=false
		      cbDNF.value=false
		      efLapsCompleted.Text="1"
		      OldTotalTime=TotalTime.Text
		      
		      
		    end if
		    
		    SQLStatement="SELECT Age_Grade_Distance FROM divisions INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		    SQLStatement = SQLStatement + "WHERE divisions.rowid ="+divisionIDStg
		    rsDistance=app.theDB.DBSQLSelect(SQLStatement)
		    stAgeGrade.Text=App.CalculateAgeGrade(Gender.text,val(Age.Text),rsDistance.Field("Age_Grade_Distance").StringValue,TotalTime.Text)
		    rsDistance.Close
		    
		    SetPlaces
		    
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbDNS
	#tag Event
		Sub Action()
		  if distanceIDStg<>"" then
		    if me.Value then
		      TotalTime.text="00:00:00.000"
		      NetTime.text="00:00:00.000"
		    end if
		    SetPlaces
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbDQ
	#tag Event
		Sub Action()
		  if distanceIDStg<>"" then
		    if me.Value then
		      TotalTime.text="00:00:00.000"
		      NetTime.text="00:00:00.000"
		    end if
		    SetPlaces
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbDNF
	#tag Event
		Sub Action()
		  if distanceIDStg<>"" then
		    if me.Value then
		      TotalTime.text="00:00:00.000"
		      NetTime.text="00:00:00.000"
		    end if
		    SetPlaces
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbSeedGroup
	#tag Event
		Sub Open()
		  
		  if app.RacersPerStart>=1 and app.RacersPerStart <=4 then 'time trial
		    me.Visible=true
		    stSeedGrouplbl.Visible=true
		  end if
		  
		  me.addrow "I"
		  me.rowTag(0)=1
		  me.addrow "II"
		  me.rowTag(1)=2
		  me.addrow "III"
		  me.rowTag(2)=3
		  me.addrow "IV"
		  me.rowTag(3)=4
		  me.ListIndex=0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events RacerNumber
	#tag Event
		Sub LostFocus()
		  dim recCount as integer
		  dim rs as recordSet
		  dim res as boolean
		  dim Result as Integer
		  
		  if RacerNumber.Text="" then
		    RacerNumber.Text="0"
		  end if
		  
		  if oldValue<>RacerNumber.text AND RacerNumber.text<>"0" then
		    'need to search for existing racer number
		    rs=app.theDB.DBSQLSelect("Select Racer_Number FROM participants WHERE RaceID=1 AND Racer_Number ="+RacerNumber.text)
		    if not rs.EOF then
		      Result=MsgBox("Racer Number "+RacerNumber.text+" has already been assigned to this race.",0+16)
		      RacerNumber.SelStart=0
		      RacerNumber.SelLength=10
		      Name.SetFocus
		      RacerNumber.SetFocus
		    end if
		    rs.Close
		    
		  end if
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub GotFocus()
		  oldValue=RacerNumber.text
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Name
	#tag Event
		Sub LostFocus()
		  if oldValue<>me.text then
		    me.text=uppercase(me.text)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub GotFocus()
		  oldValue=me.text
		  Me.text=Replace(Me.text,"'","")
		  me.SelStart=0
		  me.SelLength=32767
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events FirstName
	#tag Event
		Sub LostFocus()
		  if oldValue<>FirstName.text then
		    FirstName.text=titlecase(FirstName.text)
		  end if
		  Me.text=Replace(Me.text,"'","")
		End Sub
	#tag EndEvent
	#tag Event
		Sub GotFocus()
		  oldValue=me.text
		  Me.text=Replace(Me.text,"'","")
		  me.SelStart=0
		  me.SelLength=32767
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Street
	#tag Event
		Sub GotFocus()
		  oldValue=me.text
		  Me.text=Replace(Me.text,"'","")
		End Sub
	#tag EndEvent
	#tag Event
		Sub LostFocus()
		  if oldValue<>me.text then
		    me.text=uppercase(me.text)
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events PostalCode
	#tag Event
		Sub LostFocus()
		  dim rsPostalCodes as RecordSet
		  
		  if me.text<>"" then
		    rsPostalCodes=app.theDB.DBSQLSelect("Select City, State, Country FROM participants WHERE Postal_Code='"+me.text+"' LIMIT 1")
		    if not(rsPostalCodes.EOF) then
		      City.Text=rsPostalCodes.Field("City").StringValue
		      State.Text=rsPostalCodes.Field("State").StringValue
		      Country.Text=rsPostalCodes.Field("Country").StringValue
		      if Representing.Text="" then
		        Representing.Text=City.Text+" "+State.Text+" "+Country.Text
		      end if
		      Phone.SetFocus
		    end if
		    rsPostalCodes.Close
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events City
	#tag Event
		Sub LostFocus()
		  if oldValue<>me.text then
		    me.text=Uppercase(me.text)
		  end if
		  Me.text=Replace(Me.text,"'","")
		End Sub
	#tag EndEvent
	#tag Event
		Sub GotFocus()
		  oldValue=me.text
		  Me.text=Replace(Me.text,"'","")
		  me.SelStart=0
		  me.SelLength=32767
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events State
	#tag Event
		Sub LostFocus()
		  if oldValue<>me.text then
		    me.text=uppercase(me.text)
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub GotFocus()
		  oldValue=me.text
		  Me.text=Replace(Me.text,"'","")
		  me.SelStart=0
		  me.SelLength=32767
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Country
	#tag Event
		Sub LostFocus()
		  if oldValue<>me.text then
		    me.text=Uppercase(me.text)
		  end if
		  Me.text=Replace(Me.text,"'","")
		  
		  if Representing.text="" then
		    Representing.Text=City.Text+" "+State.Text+" "+Country.Text
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub GotFocus()
		  oldValue=me.text
		  Me.text=Replace(Me.text,"'","")
		  me.SelStart=0
		  me.SelLength=32767
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Phone
	#tag Event
		Sub LostFocus()
		  Me.text=Replace(Me.text,"'","")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events EMail
	#tag Event
		Sub LostFocus()
		  Me.text=Replace(Me.text,"'","")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DOB
	#tag Event
		Sub LostFocus()
		  Dim theDate as New Date
		  
		  if me.Text<>"0000-00-00" then
		    theDate.SQLDate=me.Text
		    Age.text=str(app.CalculateAge(theDate,app.RaceAgeDate))
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Gender
	#tag Event
		Sub TextChange()
		  me.text=Uppercase(me.text)
		  if me.text="M" OR me.text="F" OR me.text="X" then
		    Division.SetFocus
		  else
		    beep
		    me.text=""
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub LostFocus()
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if Gender.text<>oldValue then
		    if (Gender.text="M") or (Gender.text="F") or(Gender.text="X") then
		      PopulateDivisionComboBox
		      SetPlaces
		    else
		      if notDisplayed then
		        d.icon=2   //display stop icon
		        d.ActionButton.Caption="OK"
		        d.Message="Invalid Gender"
		        d.Explanation="Please use: "+EndOfLine+EndOfLine+"'M' for Male"+EndOfLine+"'F for Female"+EndOfLine+"'X' for Mixed Team"
		        b=d.ShowModal     //display the dialog
		        notDisplayed=false
		      end if
		      Gender.selStart=0
		      Gender.selLength=2
		      Gender.setFocus
		    end if
		  end  if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Division
	#tag Event
		Sub Change()
		  if app.RacersPerStart>=1 and app.RacersPerStart <=4 then 'time trial
		    me.Width=211
		  end if
		  
		  
		  if (not(OpeningWindow) and (Division.ListIndex>=0)) then
		    if Division.ListIndex>0 then
		      divisionIDStg=str(Division.RowTag(Division.ListIndex))
		      distanceIDStg=DivisionListDistanceID(Division.ListIndex)
		      DivOld=Division.Text
		      DivOldIDStg=divisionIDStg
		      PopulateRepresentingComboBox(Gender.Text,DivOldIDStg)
		      SetPlaces
		      
		      if (InStr(AssignedStartA.Text,"00:00:00.000")>0) OR (AssignedStartA.Text="") then
		        AssignedStartA.Text=DivisionListStartTime(Division.ListIndex)
		      end if
		    end if
		    
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Cancel
	#tag Event
		Sub Action()
		  ParticipantInput.close
		  wnd_List.InputComplete=true
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OK
	#tag Event
		Sub Action()
		  dim DataEntryError as boolean
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if (Name.text="") then
		    app.DisplayDataEntryError("Last/Team Name is required","Please enter a Last/Team Name",Name)
		    DataEntryError=true
		    
		  elseif ((val(Age.text)<1) or (val(Age.text)>999)) then
		    app.DisplayDataEntryError("Invalid Age","Please enter an Age between 1 and 999",Age)
		    DataEntryError=true
		    
		  elseif Gender.text="" then
		    app.DisplayDataEntryError("Gender is required","Please enter Gender",Gender)
		    DataEntryError=true
		    
		  elseif Division.ListIndex=0 then
		    
		    d.icon=2   //display stop icon
		    Division.setFocus
		    d.Message="Age Division is required"
		    d.Explanation="Please select a division"
		    b=d.ShowModal  
		    DataEntryError=true
		    
		  elseif RacerNumber.text="" then
		    app.DisplayDataEntryError("Racer Number is required","Please enter a racer number.",RacerNumber)
		    DataEntryError=true
		    
		  elseif ((Adjustment.text<>"+00:00:00.000")  and (AdjustmentReason.text="")) then
		    d.Message="Adjustment Reason is required"
		    d.Explanation="Please enter a reason for the adjustment or remove the adjustment time."
		    b=d.ShowModal
		    AdjustmentReason.setFocus
		    DataEntryError=true
		    
		  elseif ((wnd_List.ResultType.Text="Cross Country Running") and (RepresentingID="-1")) then
		    app.DisplayDataEntryError("Scoring For is required.","Please select an item from the representing drop down menu.",Representing)
		    DataEntryError=true
		    
		  else
		    DataEntryError=false
		  end if
		  
		  if RacerNumber.text="0" then
		    d.icon=1   //display warning icon
		    d.ActionButton.Caption="OK"
		    d.CancelButton.Visible=True     //show the Cancel button
		    d.CancelButton.Caption="Fix It"
		    d.Message="Racer Number is Zero"
		    d.Explanation="The participant will not get a time until you assign a racer number"
		    
		    b=d.ShowModal     //display the dialog
		    Select Case b //determine which button was pressed.
		    Case d.ActionButton
		      DataEntryError=false
		    Case d.CancelButton 
		      RacerNumber.selStart=0
		      RacerNumber.selLength=len(RacerNumber.Text)+1
		      RacerNumber.setFocus
		      DataEntryError=true
		    End select
		    
		    
		  end if
		  
		  if not(DataEntryError) then
		    if(NewParticipant) then
		      InsertParticipant
		    else
		      UpdateParticipant
		    end if
		    
		    ParticipantInput.close
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events SMS
	#tag Event
		Sub LostFocus()
		  Me.text=Replace(Me.text,"'","")
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  if app.WebServer="" then
		    self.Visible=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events StaticText48
	#tag Event
		Sub Open()
		  if app.WebServer="" then
		    self.Visible=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events MemberList
	#tag Event
		Sub DoubleClick()
		  MemberRow=me.ListIndex
		  MemberID=val(me.Cell(MemberRow,0))
		  ParticipantTeamMemberInput.newMember=False
		  ParticipantTeamMemberInput.show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events NewTeamMember
	#tag Event
		Sub Action()
		  MemberID=0
		  ParticipantTeamMemberInput.show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DeleteTeamMember
	#tag Event
		Sub Action()
		  Dim SQLStatement as string
		  dim res as boolean
		  Dim Result as integer
		  dim row as integer
		  
		  Result=MsgBox("Are you sure you want to delete this Team Member",1+48)
		  if Result = 1 then
		    row=MemberList.ListIndex
		    SQLStatement="DELETE FROM teammembers WHERE rowid="+MemberList.Cell(row,0)
		    App.theDB.DBSQLExecute(SQLStatement)
		    MemberList.RemoveRow row
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BevelButton2
	#tag Event
		Sub Action()
		  Dim i, ntResponseCodeStart, ntResponseCodeEnd, ntResponseStringStart, ntResponseStringEnd as Integer
		  Dim form as Dictionary
		  Dim socket1 as New HTTPSecureSocket
		  Dim socket2 as New HTTPSocket
		  Dim response, PhoneNumbers(), DoubleQuote, PostContent, ntResponseCode, ntResponseString, PhoneNumbersString, command as string
		  
		  Dim SMTPSocket1 as new SMTPSecureSocket
		  Dim mail as EmailMessage
		  Dim file as EmailAttachment
		  
		  dim s as new Shell
		  
		  s.TimeOut=300000 ' want it to go for 5 min before it times out.
		  
		  
		  DoubleQuote=chr(34)
		  
		  PhoneNumbers=Split(SMS.Text,"|")
		  PhoneNumbersString=""
		  for i=0 to UBound(PhoneNumbers)
		    
		    if instr(PhoneNumbers(i),"@")>0 then
		      mail = New EmailMessage
		      mail.fromAddress="info@milliseconds.com"
		      mail.Subject=wnd_List.RaceName.Text+" Text Message Test"
		      mail.bodyPlainText = FirstName.Text+" "+Name.Text+" will be participating in the "+wnd_List.RaceName.Text+". Watch for updates as the race progresses."
		      mail.headers.appendHeader "X-Mailer","Milliseconds Pro"
		      mail.addRecipient Trim(PhoneNumbers(i))
		      
		      //send the mail
		      SMTPSocket1.Address="smtp.gmail.com"
		      SMTPSocket1.Username="mac@milliseconds.com"
		      SMTPSocket1.Password="kokopelli08"
		      SMTPSocket1.Port=465
		      SMTPSocket1.ConnectionType=3
		      SMTPSocket1.Secure=true
		      SMTPSocket1.messages.append mail //add email to list of messages
		      SMTPSocket1.SendMail //send message
		      MsgBox("Test message sent")
		      app.theDB.DBSQLExecute("UPDATE races SET SMSCount=SMSCount+1")
		      
		      
		    elseif app.SMSServer(0)="EZTexting" then 'EZTexting Support
		      PhoneNumbersString=PhoneNumbersString+"&PhoneNumbers[]="+PhoneNumbers(i)
		      // create and populate the form object
		      'form = New Dictionary
		      'form.value("user") = app.SMSServer(1)
		      'form.value("pass") = app.SMSServer(2)
		      'form.value("phonenumber") = PhoneNumbers(i)
		      'form.value("subject") = ""
		      'form.value("message") = FirstName.Text+" "+Name.Text+" will be participating in the "+wnd_List.RaceName.Text+". Watch for updates as the race progresses."
		      'form.value("express") = "0"
		      
		      // setup the socket to POST the form
		      'socket1.setFormData form
		      'response = socket1.post ("https://app.eztexting.com/api/sending",30)
		      
		      
		      
		      'select case response
		      'case "1"
		      'MsgBox("Test message sent")
		      'app.theDB.DBSQLExecute("UPDATE races SET SMSCount=SMSCount+1")
		      '
		      'case "-1"
		      'MsgBox("Sending test message failed: Invalid User Name or Password")
		      '
		      'case "-2"
		      'MsgBox("Sending test message failed: Credits exhausted.")
		      '
		      'case "-5"
		      'MsgBox("Sending test message failed: Phone number has opted out.")
		      '
		      'case "-7"
		      'MsgBox("Sending test message failed: Invalid message (message exceeds 130 characters or contains single or double quotes).")
		      '
		      'case "-10"
		      'MsgBox("Sending test message failed: Unknown error.")
		      '
		      'end select
		      
		      'Socket1.Close
		    else
		      // create and populate the PostContent
		      PostContent="<?xml version="+DoubleQuote+"1.0"+DoubleQuote+"?>"
		      PostContent=PostContent+"<methodCall>"
		      PostContent=PostContent+"<methodName>send</methodName>"
		      PostContent=PostContent+"<params>"
		      PostContent=PostContent+"<param>"
		      PostContent=PostContent+"<struct>"
		      PostContent=PostContent+"<member>"
		      PostContent=PostContent+"<name>Id</name>"
		      PostContent=PostContent+"<value>"+app.SMSServer(1)+"</value>"
		      PostContent=PostContent+"</member>"
		      PostContent=PostContent+"<member>"
		      PostContent=PostContent+"<name>Recipient</name>"
		      PostContent=PostContent+"<value>"+PhoneNumbers(i)+"</value>"
		      PostContent=PostContent+"</member>"
		      PostContent=PostContent+"<member>"
		      PostContent=PostContent+"<name>Message</name>"
		      PostContent=PostContent+"<value>"+FirstName.Text+" "+Name.Text+" will be participating in the "+wnd_List.RaceName.Text+". Watch for updates as the race progresses."+"</value>"
		      PostContent=PostContent+"</member>"
		      PostContent=PostContent+"</struct>"
		      PostContent=PostContent+"</param>"
		      PostContent=PostContent+"</params>"
		      PostContent=PostContent+"</methodCall>"
		      
		      // setup the socket to POST the message
		      socket2.SetPostContent(PostContent,"text/xml")
		      response = socket2.post ("http://gateway.networktext.com/gateway",30)
		      
		      ntResponseCodeStart=InStr(Response,"<value><int>")+12
		      ntResponseCodeEnd=InStr(Response,"</int></value>")
		      ntResponseCode=mid(Response,ntResponseCodeStart,(ntResponseCodeEnd-ntResponseCodeStart))
		      
		      if ntResponseCode="1" then
		        MsgBox("Test message sent")
		        app.theDB.DBSQLExecute("UPDATE races SET SMSCount=SMSCount+1")
		      else
		        ntResponseStringStart=InStr(Response,"<value><string>")+15
		        ntResponseStringEnd=InStr(Response,"</string></value>")
		        ntResponseString=mid(Response,ntResponseStringStart,(ntResponseStringEnd-ntResponseStringStart))
		        MsgBox("Sending test message failed: "+ntResponseString+" Error Number: "+ntResponseCode)
		      end if
		      
		      socket2.close
		    end if
		  next
		  
		  if app.SMSServer(0)="EZTexting" then 'EZTexting Support
		    command="curl -d 'User="+app.SMSServer(1)+"&Password="+app.SMSServer(2)+PhoneNumbersString+"&Message="+FirstName.Text+" "+Name.Text+" will be participating in the "+wnd_List.RaceName.Text+". Watch for updates as the race progresses.&MessageTypeID=2' https://app.eztexting.com/sending/messages?format=xml"
		    
		    s.Execute command
		    if InStr(s.Result,"<Status>Success</Status>")>0 then
		      MsgBox("Test message sent")
		      app.theDB.DBSQLExecute("UPDATE races SET SMSCount=SMSCount+1")
		    else
		      MsgBox("Test message failed")
		    end if
		    
		  end if
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbRepresenting
	#tag Event
		Sub Change()
		  
		  if (not(OpeningWindow) and (cbRepresenting.ListIndex>=0)) then
		    if cbRepresenting.ListIndex>0 then
		      RepresentingID=str(cbRepresenting.RowTag(cbRepresenting.ListIndex))
		    end if
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  if wnd_List.ResultType.Text="Cross Country Running" then
		    me.Visible=true
		  else
		    me.Visible=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Representing
	#tag Event
		Sub LostFocus()
		  Me.text=Replace(Me.text,"'","")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbVerifyUSSAPts
	#tag Event
		Sub Action()
		  dim PointsList as new ImportUSSAFISPointsList
		  dim WarningString as String
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  Dim Result() As String
		  
		  WarningString=PointsList.ValidatePoints(val(ParticipantIDStg), "USSA", efNGB1License.Text)
		  
		  if (InStr(WarningString,"Warning:")=0) then
		    Result=WarningString.Split("|")
		    efNGB1License.Text=Result(0)
		    efNGB1Points.Text=Result(1)
		  else
		    d.Icon=1 'caution icon
		    d.Message="An error occured looking up license information."
		    d.Explanation=WarningString
		    b=d.ShowModal
		    if Note.Text<>"" Then
		      Note.Text=WarningString+" "+Note.Text
		    Else
		      Note.Text=WarningString
		    End If
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbVerifyFISPts
	#tag Event
		Sub Action()
		  dim PointsList as new ImportUSSAFISPointsList
		  dim ErrorString as String
		  Dim d as new MessageDialog
		  Dim b as MessageDialogButton
		  Dim Result() As String
		  
		  ErrorString=PointsList.ValidatePoints(val(ParticipantIDStg), "FIS", efNGB2License.Text)
		  
		  if (InStr(ErrorString,"Error:")=0) then
		    Result=ErrorString.Split("|")
		    efNGB2License.Text=Result(0)
		    efNGB2Points.Text=Result(1)
		  else
		    d.Icon=1 'caution icon
		    d.Message="An error occured looking up license information."
		    d.Explanation=ErrorString
		    b=d.ShowModal
		    if Note.Text<>"" Then
		      Note.Text=ErrorString+" "+Note.Text
		    Else
		      Note.Text=ErrorString
		    End If
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MemberID"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MemberRow"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ParticipantIDStg"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
