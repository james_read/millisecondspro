#tag Class
Protected Class oAuth
Inherits HTTPSocket
	#tag Event
		Sub PageReceived(url as string, httpStatus as integer, headers as internetHeaders, content as string)
		  dim elementparts(-1) as string
		  payload=""
		  
		  
		  if postMode = "REQUEST_TOKEN" then
		    
		    postMode = ""
		    
		    if httpstatus=200 then
		      elementparts=content.Split("&")
		      appAuthURL = "http://api.twitter.com/oauth/authorize?"+elementparts(0)
		      authenticationToken = elementparts(0).replace("oauth_token=","")
		      authenticationSecret=elementparts(1).replace("oauth_token_secret=","")
		      ready=true
		    else
		      makeException("oAuth RequestToken exception "+str(HTTPStatus)+EndOfLine+content)
		    end if
		    
		  end if
		  
		  if postMode= "ACCESS_TOKEN" then
		    
		    postmode=""
		    
		    if httpstatus=200 then
		      elementparts=content.Split("&")
		      token = elementparts(0).replace("oauth_token=","")
		      secret = elementparts(1).replace("oauth_token_secret=","")
		      screenName = elementparts(3).replace("screen_name=","")
		      userid = val(elementparts(2).replace("user_id=",""))
		      ready=true
		      
		    else
		      makeException("oAuth AccessToken exception "+str(HTTPStatus)+EndOfLine+content)
		    end if
		  end if
		  
		  if postMode = "POST_TWEET" then
		    
		    postmode=""
		    
		    if httpstatus=200 then
		      ready=true
		      payload=content
		    else
		      makeException("oAuth SendTweet exception "+str(HTTPStatus)+EndOfLine+content)
		    end if
		  end if
		  
		  
		  
		  if postMode = "GET_TIMELINE" then
		    
		    postmode=""
		    
		    if httpstatus=200 then
		      ready=true
		      payload=content
		    else
		      makeException("oAuth GetTimeline exception "+str(HTTPStatus)+EndOfLine+content)
		    end if
		  end if
		  
		  if postMode = "GET_MENTIONS" then
		    dim searchcontent,id,title,name,composition,published as string
		    dim resultTweets(-1),tweetelements(-1),idprocessing(-1) as string
		    
		    resultTweets=content.Split("<entry>")
		    
		    for i as integer = 1 to resultTweets.Ubound
		      
		      composition = ""
		      title=""
		      name=""
		      id=""
		      tweetelements=resultTweets(i).split(chr(10))
		      
		      for j as integer = 0 to tweetelements.Ubound
		        
		        if tweetelements(j).InStr("<id>") > 0 then
		          idprocessing = tweetelements(j).split(":")
		          idprocessing = idprocessing(1).split("<")
		          id=idprocessing(0)
		        end if
		        'if tweetelements(j).InStr("<published>") > 0 then published=RemoveHTMLTagsMBS(tweetelements(j))
		        'if tweetelements(j).InStr("<title>") > 0 then title= RemoveHTMLTagsMBS(tweetelements(j))
		        if tweetelements(j).InStr("<name>") > 0 then
		          idprocessing= tweetelements(j).split("(")
		          idprocessing=idprocessing(0).split(">")
		          name=idprocessing(1)
		        end if
		        
		      next
		      
		      'payload = payload + DefineEncoding("@"+name+"||"+published+"||"+DecodingFromHTMLMBS(title)+EndOfLine,Encodings.UTF8)
		      
		    next
		    ready=true
		  end if
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub AuthoriseApplication(pinCode as string)
		  if pinCode = "" then 
		    makeException("NullPINcode")
		  else
		    dim timestamp as integer
		    dim sha as new sha
		    dim signature,signatureBase,signaturePart,signingKey,oAuthPayload,nonce as string
		    
		    ready = false
		    timestamp = UnixTime
		    nonce = makeNonce
		    
		    signaturePart = accessTokenURL
		    signaturePart = EncodeURLComponent(signaturePart)
		    signaturePart = "POST&" + signaturePart + "&"
		    
		    signatureBase = "oauth_consumer_key="+consumerKey
		    signatureBase = signatureBase + "&oauth_nonce="+nonce
		    signatureBase = signatureBase + "&oauth_signature_method=HMAC-SHA1"
		    signatureBase = signatureBase + "&oauth_token="+authenticationToken
		    signatureBase = signatureBase + "&oauth_timestamp="+str(timestamp)
		    signatureBase = signatureBase + "&oauth_verifier="+pinCode
		    signatureBase = signatureBase + "&oauth_version=1.0"
		    
		    signingKey = consumerSecret+"&"+pinCode
		    
		    signatureBase = signaturePart + EncodeURLComponent(signatureBase)
		    
		    signature = sha.HMAC(signingKey,signatureBase)
		    
		    signature = EncodeBase64(signature)
		    signature = EncodeURLComponent(signature)
		    
		    oAuthPayload = "OAuth "
		    oAuthPayload = oAuthPayload+"oauth_consumer_key="""+consumerKey+""", "
		    oAuthPayload = oAuthPayload+"oauth_nonce="""+nonce+""",  "
		    oAuthPayload = oAuthPayload+"oauth_signature_method=""HMAC-SHA1"", "
		    oAuthPayload = oAuthPayload+"oauth_token="""+authenticationToken+""", "
		    oAuthPayload = oAuthPayload+"oauth_timestamp="""+str(timestamp)+""", "
		    oAuthPayload = oAuthPayload+"oauth_verifier="""+pinCode+""", "
		    oAuthPayload = oAuthPayload+"oauth_version=""1.0"", "
		    oAuthPayload = oAuthPayload+"oauth_signature=""" + signature + """"
		    
		    SetRequestHeader("Authorization",oAuthPayload)
		    postmode="ACCESS_TOKEN"
		    Post(AccessTokenUrl)
		  end if
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub GetAuthorisationURL()
		  dim timestamp as integer
		  dim sha as new sha
		  dim signature,signatureBase,signaturePart,signingKey,oAuthPayload,nonce as string
		  
		  ready = false
		  timestamp = UnixTime
		  nonce = makeNonce
		  
		  signaturePart = requestTokenURL
		  signaturePart = EncodeURLComponent(signaturePart)
		  signaturePart = "POST&" + signaturePart + "&"
		  
		  signatureBase = "oauth_callback=oob"
		  signatureBase = signatureBase + "&oauth_consumer_key="+consumerKey
		  signatureBase = signatureBase + "&oauth_nonce="+nonce
		  signatureBase = signatureBase + "&oauth_signature_method=HMAC-SHA1"
		  signatureBase = signatureBase + "&oauth_timestamp="+str(timestamp)
		  signatureBase = signatureBase + "&oauth_version=1.0"
		  
		  signingKey = consumerSecret+"&"
		  
		  signatureBase = signaturePart + EncodeURLComponent(signatureBase)
		  
		  signature = sha.HMAC(signingKey,signatureBase)
		  
		  signature = EncodeBase64(signature)
		  signature = EncodeURLComponent(signature)
		  
		  oAuthPayload = "OAuth "
		  oAuthPayload = oAuthPayload+"oauth_callback=""oob"", "
		  oAuthPayload = oAuthPayload+"oauth_consumer_key="""+consumerKey+""", "
		  oAuthPayload = oAuthPayload+"oauth_nonce="""+nonce+""",  "
		  oAuthPayload = oAuthPayload+"oauth_signature_method=""HMAC-SHA1"", "
		  oAuthPayload = oAuthPayload+"oauth_timestamp="""+str(unixTime)+""", "
		  oAuthPayload = oAuthPayload+"oauth_version=""1.0"", "
		  oAuthPayload = oAuthPayload+"oauth_signature="""+signature +""""
		  
		  SetRequestHeader("Authorization",oAuthPayload)
		  postmode="REQUEST_TOKEN"
		  Post(requestTokenUrl)
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetMentions(username as string = "")
		  if username = "" then username = screenName
		  
		  if username <> "" then
		    
		    postMode="GET_MENTIONS"
		    Get("http://search.twitter.com/search.atom?q="+EncodeURLComponent(username+" -from:"+username))
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GetTimeline(lastId as string)
		  if secret = "" then makeException("NoTokenSecret")
		  if token = "" then makeException("NoToken")
		  
		  dim timestamp as integer
		  dim sha as new sha
		  dim dict as new dictionary
		  dim signature,signatureBase,signaturePart,signingKey,oAuthPayload,nonce as string
		  dim url as string
		  
		  url = "http://api.twitter.com/1/statuses/home_timeline.xml"
		  if lastid<>"" then
		    url=url + "?since_id=" + lastid + "&count=800"
		  else
		    url=url+ "?count=800"
		  end if
		  
		  ready = false
		  timestamp = UnixTime
		  nonce = makeNonce
		  
		  signaturePart = url
		  
		  signaturePart = EncodeURLComponent(signaturePart)
		  signaturePart = "GET&" + signaturePart + "&"
		  
		  signatureBase = "oauth_consumer_key="+consumerKey
		  signatureBase = signatureBase + "&oauth_nonce="+nonce
		  signatureBase = signatureBase + "&oauth_signature_method=HMAC-SHA1"
		  signatureBase = signatureBase + "&oauth_timestamp="+str(timestamp)
		  signatureBase = signatureBase + "&oauth_token="+token
		  signatureBase =signatureBase + "&oauth_version=1.0"
		  
		  signingKey = consumerSecret+"&"+secret
		  
		  signatureBase = signaturePart + EncodeURLComponent(signatureBase)
		  
		  signature = sha.HMAC(signingKey,signatureBase)
		  
		  signature = EncodeBase64(signature)
		  signature = EncodeURLComponent(signature)
		  
		  oAuthPayload = "OAuth "
		  oAuthPayload = oAuthPayload+"oauth_nonce="""+nonce+""",  "
		  oAuthPayload = oAuthPayload+"oauth_signature_method=""HMAC-SHA1"", "
		  oAuthPayload = oAuthPayload+"oauth_timestamp="""+str(timestamp)+""", "
		  oAuthPayload = oAuthPayload+"oauth_consumer_key="""+consumerKey+""", "
		  oAuthPayload = oAuthPayload+"oauth_token="""+token+""", "
		  oAuthPayload = oAuthPayload+"oauth_signature="""+signature+""", "
		  oAuthPayload = oAuthPayload+"oauth_version=""1.0"""
		  
		  SetRequestHeader("Authorization",oAuthPayload)
		  
		  postmode="GET_TIMELINE"
		  
		  Get(url)
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub makeException(exceptionMessage as string)
		  'Dim r as RuntimeException
		  'r=New RuntimeException
		  'r.ErrorNumber=-999
		  'r.Message=exceptionMessage
		  'Raise r
		  if instr(exceptionMessage,"<error>")>0 then
		    dim eme(-1) as string
		    eme=exceptionMessage.split("<error>")
		    eme=eme(1).split("</error>")
		    errorMessage=eme(0)
		  else
		    errorMessage=exceptionMessage
		  end if
		  ready=true
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function makeNonce() As String
		  return "QP70eNmVz8jvdPevU3oJD2AfF7R7odC2XJcn4XlZJqk"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SendTweet(tweet as String, optional MSTTweet as Boolean = false)
		  dim tempToken, tempSecret as String
		  
		  if secret = "" then makeException("NoTokenSecret")
		  if token = "" then makeException("NoToken")
		  if tweet = "" then makeException("NoTweetSpecified")
		  
		  tempSecret=secret
		  tempToken=token
		  
		  if MSTTweet then
		    secret = mstSecret
		    token = mstToken
		  end if
		  
		  dim timestamp as integer
		  dim sha as new sha
		  dim dict as new dictionary
		  dim signature,signatureBase,signaturePart,signingKey,oAuthPayload,nonce as string
		  
		  ready = false
		  timestamp = UnixTime
		  nonce = makeNonce
		  
		  signaturePart = "http://api.twitter.com/statuses/update.xml"
		  signaturePart = EncodeURLComponent(signaturePart)
		  signaturePart = "POST&" + signaturePart + "&"
		  
		  signatureBase = "oauth_consumer_key="+consumerKey
		  signatureBase = signatureBase + "&oauth_nonce="+nonce
		  signatureBase = signatureBase + "&oauth_signature_method=HMAC-SHA1"
		  signatureBase = signatureBase + "&oauth_timestamp="+str(timestamp)
		  signatureBase = signatureBase + "&oauth_token="+token
		  signatureBase =signatureBase + "&oauth_version=1.0"
		  signatureBase =signatureBase + "&status="+EncodeURLComponent(tweet)
		  
		  signingKey = consumerSecret+"&"+secret
		  
		  signatureBase = signaturePart + EncodeURLComponent(signatureBase)
		  
		  signature = sha.HMAC(signingKey,signatureBase)
		  
		  signature = EncodeBase64(signature)
		  signature = EncodeURLComponent(signature)
		  
		  oAuthPayload = "OAuth "
		  oAuthPayload = oAuthPayload+"oauth_nonce="""+nonce+""",  "
		  oAuthPayload = oAuthPayload+"oauth_signature_method=""HMAC-SHA1"", "
		  oAuthPayload = oAuthPayload+"oauth_timestamp="""+str(timestamp)+""", "
		  oAuthPayload = oAuthPayload+"oauth_consumer_key="""+consumerKey+""", "
		  oAuthPayload = oAuthPayload+"oauth_token="""+token+""", "
		  oAuthPayload = oAuthPayload+"oauth_signature="""+signature+""", "
		  oAuthPayload = oAuthPayload+"oauth_version=""1.0"""
		  
		  dict.Value("status")=tweet
		  
		  SetRequestHeader("Authorization",oAuthPayload)
		  SetFormData(dict)
		  
		  postmode="POST_TWEET"
		  
		  Post("http://api.twitter.com/statuses/update.xml")
		  
		  token=tempToken
		  secret=tempSecret
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub StartAuthorisation()
		  ready=false
		  GetAuthorisationURL
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function UnixTime() As integer
		  dim d as new date
		  
		  return d.TotalSeconds - 2082819600
		End Function
	#tag EndMethod


	#tag Note, Name = License
		    Praemus Alpha, oAuth interface in REALBasic
		    Copyright (C) 2010 Andy Dixon, Praemus Technologies
		
		    This program is free software: you can redistribute it and/or modify
		    it under the terms of the GNU General Public License as published by
		    the Free Software Foundation, either version 3 of the License, or
		    (at your option) any later version.
		
		    This program is distributed in the hope that it will be useful,
		    but WITHOUT ANY WARRANTY; without even the implied warranty of
		    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		    GNU General Public License for more details.
		
		    If you find this useful or modify the code, please let me know by email
		    andy.dixon@praemus.com - Its a very quick and dirty implementation.
		
		    You should have received a copy of the GNU General Public License
		    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	#tag EndNote


	#tag Property, Flags = &h0
		accessTokenURL As String = "http://api.twitter.com/oauth/access_token"
	#tag EndProperty

	#tag Property, Flags = &h0
		appAuthURL As String
	#tag EndProperty

	#tag Property, Flags = &h0
		authenticationSecret As string
	#tag EndProperty

	#tag Property, Flags = &h0
		authenticationToken As string
	#tag EndProperty

	#tag Property, Flags = &h0
		authorizeTokenURL As String = "http://api.twitter.com/oauth/authorize"
	#tag EndProperty

	#tag Property, Flags = &h0
		consumerKey As String
	#tag EndProperty

	#tag Property, Flags = &h0
		consumerSecret As String
	#tag EndProperty

	#tag Property, Flags = &h0
		errorMessage As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mstScreenName As String = "milliseconds"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mstSecret As String = "F1FhtjftJtv62fOibm0VW60J8dYyVwAMs7fzgbM"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mstToken As String = "16211853-XIyjfG91eg0tQHSEh2n3v3orb21COEIkPiNyuGuvy"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mstUserid As Integer = 16211853
	#tag EndProperty

	#tag Property, Flags = &h0
		payload As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private postMode As string
	#tag EndProperty

	#tag Property, Flags = &h0
		ready As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		requestTokenURL As String = "http://api.twitter.com/oauth/request_token"
	#tag EndProperty

	#tag Property, Flags = &h0
		screenName As string
	#tag EndProperty

	#tag Property, Flags = &h0
		secret As string
	#tag EndProperty

	#tag Property, Flags = &h0
		token As string
	#tag EndProperty

	#tag Property, Flags = &h0
		userId As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="accessTokenURL"
			Visible=true
			Group="Twitter URLs"
			InitialValue="http://api.twitter.com/oauth/access_token"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Address"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="appAuthURL"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="authenticationSecret"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="authenticationToken"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="authorizeTokenURL"
			Visible=true
			Group="Twitter URLs"
			InitialValue="http://api.twitter.com/oauth/authorize"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BytesAvailable"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BytesLeftToSend"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="consumerKey"
			Visible=true
			Group="Twitter API"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="consumerSecret"
			Visible=true
			Group="Twitter API"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="errorMessage"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Handle"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="httpProxyAddress"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="httpProxyPort"
			Group="Behavior"
			InitialValue="0"
			Type="integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsConnected"
			Group="Behavior"
			InitialValue="0"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LastErrorCode"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LocalAddress"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="payload"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Port"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ready"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RemoteAddress"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="requestTokenURL"
			Visible=true
			Group="Twitter URLs"
			InitialValue="http://api.twitter.com/oauth/request_token"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="screenName"
			Visible=true
			Group="User Information"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="secret"
			Visible=true
			Group="User token Information"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="token"
			Visible=true
			Group="User token Information"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="userId"
			Visible=true
			Group="User Information"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="yield"
			Group="Behavior"
			InitialValue="0"
			Type="boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
