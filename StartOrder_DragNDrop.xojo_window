#tag Window
Begin Window StartOrder_DragNDrop
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   624
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   620
   MinimizeButton  =   True
   MinWidth        =   1000
   Placement       =   0
   Resizeable      =   False
   Title           =   "Manual Start List Generation"
   Visible         =   True
   Width           =   1539
   Begin ListBox Participants_DataList
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   8
      ColumnsResizable=   True
      ColumnWidths    =   "0,50,100,100,100,80,80,150"
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   True
      EnableDragReorder=   True
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   True
      HeadingIndex    =   -1
      Height          =   550
      HelpTag         =   "Drag participants from registered participants area to the start list area. They will be added to the end of the start list."
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "	No	Name	Representing	Division	NGB1 Pts	NGB2 Pts	Start Time"
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   1
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   9.0
      TextUnit        =   0
      Top             =   22
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   675
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin ListBox StartList_DataList
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   8
      ColumnsResizable=   True
      ColumnWidths    =   "0,50,100,100,100,80,80,150"
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   True
      EnableDragReorder=   True
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   True
      HeadingIndex    =   -1
      Height          =   550
      HelpTag         =   "Drag participants from registered participants area to the start list area. They will be added to the end of the start list\r\rTo reorder the start list, drag the participants to the new location.\r\rTo remove participants from the start list area, drag the participants to the registered participant area."
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "	No	Name	Representing	Division	NGB1 Pts	NGB2 Pts	Start Time"
      Italic          =   False
      Left            =   707
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   1
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   9.0
      TextUnit        =   0
      Top             =   22
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   675
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin PushButton pbOK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "OK"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1439
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   584
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton pbCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1347
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   584
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin BevelButton bbInsertGhost
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Insert Ghost"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "Click to insert a ghost before the selected racer."
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1406
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   61
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   96
   End
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1406
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Start Interval:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   159
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin ComboBox cbStartInterval
      AutoComplete    =   False
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   "Select the start interval to use. You can use one from the list or enter your own."
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "10 sec\r15 sec\r20 sec\r30 sec\r45 sec\r60 sec\r90 sec\r120 sec"
      Italic          =   False
      Left            =   1406
      ListIndex       =   3
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   183
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   103
   End
   Begin Label StaticText3
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1406
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Racers Per Start:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   110
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   96
   End
   Begin PopupMenu pmRacersPerStart
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   "Select the number of racers in each start."
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "1\r2\r3\r4"
      Italic          =   False
      Left            =   1406
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   134
      Underline       =   False
      Visible         =   True
      Width           =   103
   End
   Begin Label StaticText4
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Regsitered Participants:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   0
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   164
   End
   Begin Label StaticText5
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   707
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Start List:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   0
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   247
   End
   Begin BevelButton bbDraw
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Draw"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "Click to execute a random draw for the selected racers."
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   1412
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   16
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   286
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   90
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  dim i, ii, iii, NumIntervals as Integer
		  dim rsQueryResults as RecordSet
		  dim SelectStatement as String
		  dim CurrentTime, LastTime, NextTime as string
		  dim TimeDifference as double
		  
		  LastTime="00:00:00"
		  
		  Participants_DataList.DeleteAllRows
		  SelectStatement="SELECT participants.rowid, participants.Racer_Number, participants.Participant_Name, participants.First_Name, participants.Representing, participants.Assigned_Start, divisions.Division_Name, "
		  SelectStatement=SelectStatement+"participants.NGB1_Points, participants.NGB2_Points "
		  SelectStatement=SelectStatement+"FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid WHERE participants.RaceID =1 "
		  SelectStatement=SelectStatement+"ORDER BY participants.Assigned_Start, divisions.Division_Name, participants.Racer_Number"
		  rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		  
		  if rsQueryResults.RecordCount>0 then
		    
		    Progress.Show
		    Progress.Initialize("Progress","Loading Participants...",rsQueryResults.RecordCount,-1)
		    
		    for i=1 to rsQueryResults.RecordCount
		      
		      CurrentTime=mid(rsQueryResults.Field("Assigned_Start").StringValue,12,8)
		      TimeDifference=app.ConvertTimeToSeconds(CurrentTime) - app.ConvertTimeToSeconds(LastTime)
		      if (TimeDifference > app.StartIntervalSec) then
		        NextTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(CurrentTime)-TimeDifference+app.StartIntervalSec,false)
		        NumIntervals=TimeDifference/app.StartIntervalSec/app.RacersPerStart
		        for ii = 1 to NumIntervals-1
		          for iii = 1 to app.RacersPerStart
		            Participants_DataList.AddRow ""
		            Participants_DataList.Cell(Participants_DataList.lastindex,0)="0"
		            Participants_DataList.Cell(Participants_DataList.lastindex,1)="-"
		            Participants_DataList.Cell(Participants_DataList.lastindex,2)="-"
		            Participants_DataList.Cell(Participants_DataList.lastindex,3)="-"
		            Participants_DataList.Cell(Participants_DataList.lastindex,4)="-"
		            Participants_DataList.Cell(Participants_DataList.lastindex,5)=""
		            Participants_DataList.Cell(Participants_DataList.lastindex,6)=""
		            Participants_DataList.Cell(Participants_DataList.lastindex,7)=app.RaceDate.SQLDate+" "+NextTime
		          next
		          NextTime=app.ConvertSecondsToTime((app.ConvertTimeToSeconds(NextTime)+app.StartIntervalSec),false)
		        next
		      end if
		      LastTime=CurrentTime
		      
		      Participants_DataList.AddRow ""
		      Participants_DataList.Cell(Participants_DataList.lastindex,0)=rsQueryResults.Field("rowid").stringValue
		      Participants_DataList.Cell(Participants_DataList.lastindex,1)=rsQueryResults.Field("Racer_Number").StringValue
		      if rsQueryResults.Field("First_Name").StringValue<>"" then
		        Participants_DataList.Cell(Participants_DataList.lastindex,2)=rsQueryResults.Field("Participant_Name").stringValue+", "+rsQueryResults.Field("First_Name").StringValue
		      else
		        Participants_DataList.Cell(Participants_DataList.lastindex,2)=rsQueryResults.Field("Participant_Name").stringValue
		      end if
		      Participants_DataList.Cell(Participants_DataList.lastindex,3)=rsQueryResults.Field("Representing").stringValue
		      Participants_DataList.Cell(Participants_DataList.lastindex,4)=rsQueryResults.Field("Division_Name").stringValue
		      if rsQueryResults.Field("NGB1_Points").stringValue<>"inact" then
		        Participants_DataList.Cell(Participants_DataList.lastindex,5)=rsQueryResults.Field("NGB1_Points").stringValue
		      else
		        Participants_DataList.Cell(Participants_DataList.lastindex,5)=""
		      end if
		      if rsQueryResults.Field("NGB2_Points").stringValue<>"inact" then
		        Participants_DataList.Cell(Participants_DataList.lastindex,6)=rsQueryResults.Field("NGB2_Points").stringValue
		      else
		        Participants_DataList.Cell(Participants_DataList.lastindex,6)=""
		      end if
		      Participants_DataList.Cell(Participants_DataList.lastindex,7)=rsQueryResults.Field("Assigned_Start").stringValue
		      
		      rsQueryResults.moveNext
		      
		      Progress.UpdateProg1(i)
		    next
		    rsQueryResults.close
		    
		    Progress.Close
		    
		  end if
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub UpdateStartTimes()
		  dim i,nRows as integer
		  
		  nRows=StartList_DataList.ListCount-1
		  For i=0 to nRows
		    StartList_DataList.Cell(i,7)=app.RaceDate.SQLDate+" "+app.CalculateStartTime(i+1,"00:00:00",False)
		  Next
		  DragReorderRowsOccured=false
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		DragReorderRowsOccured As Boolean = False
	#tag EndProperty


#tag EndWindowCode

#tag Events Participants_DataList
	#tag Event
		Function DragRow(drag As DragItem, row As Integer) As Boolean
		  Dim nRows, i as Integer
		  
		  'Deal with the drag
		  nRows=Me.ListCount-1
		  For i=0 to nRows
		    If Me.Selected(i) then
		      Drag.AddItem(0,0,450,4)
		      Drag.Text=Me.Cell(i,0)+"|"+Me.Cell(i,1)+"|"+Me.Cell(i,2)+"|"+Me.Cell(i,3)+"|"+Me.Cell(i,4)+"|"+Me.Cell(i,5)+"|"+Me.Cell(i,6)+"|"+Me.Cell(i,7)+"|"+Str(i)
		    End if
		  Next
		  Return True //allow the drag
		End Function
	#tag EndEvent
	#tag Event
		Sub LostFocus()
		  dim i,nRows as Integer
		  
		  nRows=me.ListCount-1
		  For i=0 to nRows
		    If me.Selected(i) then
		      me.Selected(i)=false
		    End if
		  Next
		End Sub
	#tag EndEvent
	#tag Event
		Sub GotFocus()
		  dim i,nRows as Integer
		  
		  nRows=StartList_DataList.ListCount-1
		  For i=0 to nRows
		    If StartList_DataList.Selected(i) then
		      StartList_DataList.Selected(i)=false
		    End if
		  Next
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  if app.NGB1<>"" then
		    me.Heading(5)=app.NGB1+" pts"
		  end if 
		  if app.NGB2<>"" then
		    me.Heading(6)=app.NGB2+" pts"
		  end if
		  
		  
		  me.AcceptTextDrop
		  me.SortedColumn=5
		  me.ColumnSortDirection(5)=Listbox.SortAscending
		End Sub
	#tag EndEvent
	#tag Event
		Sub DropObject(obj As DragItem, action As Integer)
		  Dim i as Integer
		  Dim ParticipantArray(-1) as String
		  Dim RemoveArray(-1) as Integer
		  
		  Do
		    If Obj.TextAvailable then
		      ParticipantArray=Split(Obj.Text,"|")
		      if ParticipantArray(0)<>"0" then
		        Me.AddRow("")
		        Me.Cell(Me.LastIndex,0)=ParticipantArray(0)
		        Me.Cell(Me.LastIndex,1)=ParticipantArray(1)
		        Me.Cell(Me.LastIndex,2)=ParticipantArray(2)
		        Me.Cell(Me.LastIndex,3)=ParticipantArray(3)
		        Me.Cell(Me.LastIndex,4)=ParticipantArray(4)
		        Me.Cell(Me.LastIndex,5)=ParticipantArray(5)
		        Me.Cell(Me.LastIndex,6)=ParticipantArray(6)
		        Me.Cell(Me.LastIndex,7)=ParticipantArray(7)
		      end if
		      RemoveArray.Append Val(ParticipantArray(8))
		    End if
		  Loop until Not obj.NextItem
		  
		  for i=UBound(RemoveArray) DownTo 0
		    StartList_DataList.RemoveRow(RemoveArray(i))
		  next
		  UpdateStartTimes
		  me.sort
		  StartList_DataList.SetFocus
		End Sub
	#tag EndEvent
	#tag Event
		Function CompareRows(row1 as Integer, row2 as Integer, column as Integer, ByRef result as Integer) As Boolean
		  dim NGB1Row1Points, NGB2Row1Points, NGB1Row2Points, NGB2Row2Points, SortValueRow1, SortValueRow2 as string
		  dim UseCustomSort as Boolean
		  
		  UseCustomSort=false
		  
		  select case column
		  case 4, 5, 6
		    if app.ResultsDisplayType=wnd_List.constFISUSSAResultType then
		      if Me.Cell(row1,5)<>"" then
		        NGB1Row1Points=Me.Cell(row1,5)
		      else
		        NGB1Row1Points="999"
		      end if
		      
		      if Me.Cell(row1,6)<>"" then
		        NGB2Row1Points=Me.Cell(row1,6)
		      else
		        NGB2Row1Points="999"
		      end if
		      
		      if Me.Cell(row2,5)<>"" then
		        NGB1Row2Points=Me.Cell(row2,5)
		      else
		        NGB1Row2Points="999"
		      end if
		      
		      if Me.Cell(row2,6)<>"" then
		        NGB2Row2Points=Me.Cell(row2,6)
		      else
		        NGB2Row2Points="999"
		      end if
		      if column = 4 or column=6 then
		        SortValueRow1=app.PadString(Me.Cell(row1,4),"L",40)+format(0-val(NGB2Row1Points),"0000.000")+format(0-val(NGB1Row1Points),"0000.000")
		        SortValueRow2=app.PadString(Me.Cell(row2,4),"L",40)+format(0-val(NGB2Row2Points),"0000.000")+format(0-val(NGB1Row2Points),"0000.000")
		      else
		        SortValueRow1=app.PadString(Me.Cell(row1,4),"L",40)+format(0-val(NGB1Row1Points),"0000.000")+format(0-val(NGB2Row1Points),"0000.000")
		        SortValueRow2=app.PadString(Me.Cell(row2,4),"L",40)+format(0-val(NGB1Row2Points),"0000.000")+format(0-val(NGB2Row2Points),"0000.000")
		      end if
		      
		    else
		      
		      SortValueRow1=Me.Cell(row1,4)
		      SortValueRow2=Me.Cell(row2,4)
		    end if
		    
		    if SortValueRow1=SortValueRow2 then
		      result=0
		    elseIf SortValueRow1>SortValueRow2 then
		      result=1
		    else
		      result=-1
		    End if
		    
		    UseCustomSort=true
		    
		  else
		    
		    if IsNumeric(Me.Cell(row1,column)) then
		      If Val(Me.Cell(row1,column))>Val(Me.Cell(row2,column)) then
		        result=1
		      else
		        result=-1
		      End if
		      UseCustomSort=true
		    else
		      UseCustomSort=false
		      
		    end if
		    
		    
		  end select
		  
		  Return UseCustomSort  
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events StartList_DataList
	#tag Event
		Sub Open()
		  if app.NGB1<>"" then
		    me.Heading(5)=app.NGB1+" pts"
		  end if
		  if app.NGB2<>"" then
		    me.Heading(6)=app.NGB2+" pts"
		  end if
		  
		  me.AcceptTextDrop
		  me.SortedColumn=5
		  me.ColumnSortDirection(5)=Listbox.SortAscending
		End Sub
	#tag EndEvent
	#tag Event
		Sub DropObject(obj As DragItem, action As Integer)
		  Dim i,nRows as Integer
		  Dim ParticipantArray(-1) as String
		  Dim RemoveArray(-1) as Integer
		  
		  if DragReorderRowsOccured then ' reordering start times
		    UpdateStartTimes
		    
		  else 'receiving something from the Participant list
		    Do
		      If Obj.TextAvailable then
		        ParticipantArray=Split(Obj.Text,"|")
		        Me.AddRow("")
		        Me.Cell(Me.LastIndex,0)=ParticipantArray(0)
		        Me.Cell(Me.LastIndex,1)=ParticipantArray(1)
		        Me.Cell(Me.LastIndex,2)=ParticipantArray(2)
		        Me.Cell(Me.LastIndex,3)=ParticipantArray(3)
		        Me.Cell(Me.LastIndex,4)=ParticipantArray(4)
		        Me.Cell(Me.LastIndex,5)=ParticipantArray(5)
		        Me.Cell(Me.LastIndex,6)=ParticipantArray(6)
		        if (ParticipantArray(7)=app.RaceStartTime) or (instr(ParticipantArray(7),app.RaceDate.SQLDate+" "+"00:00:00.000")>0) then
		          me.Cell(Me.LastIndex,7)=app.RaceDate.SQLDate+" "+app.CalculateStartTime(Me.LastIndex+1,"00:00:00",false)
		        else
		          me.Cell(Me.LastIndex,7)=ParticipantArray(7)
		        end if
		        RemoveArray.Append Val(ParticipantArray(8))
		        me.Selected(me.LastIndex)=true
		      End if
		    Loop until Not obj.NextItem
		    me.SetFocus
		    
		    for i=UBound(RemoveArray) DownTo 0
		      Participants_DataList.RemoveRow(RemoveArray(i))
		    next
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Function DragReorderRows(newPosition as Integer, parentRow as Integer) As Boolean
		  DragReorderRowsOccured=true
		  'msgbox "DragReorderRowsOccured"
		End Function
	#tag EndEvent
	#tag Event
		Function DragRow(drag As DragItem, row As Integer) As Boolean
		  Dim nRows, i as Integer
		  
		  'Deal with the drag
		  nRows=Me.ListCount-1
		  For i=0 to nRows
		    If Me.Selected(i) then
		      Drag.AddItem(0,0,450,4)
		      Drag.Text=Me.Cell(i,0)+"|"+Me.Cell(i,1)+"|"+Me.Cell(i,2)+"|"+Me.Cell(i,3)+"|"+Me.Cell(i,4)+"|"+Me.Cell(i,5)+"|"+Me.Cell(i,6)+"|"+Me.Cell(i,7)+"|"+Str(i)
		    End if
		  Next
		  Return True //allow the drag
		End Function
	#tag EndEvent
	#tag Event
		Function CellClick(row as Integer, column as Integer, x as Integer, y as Integer) As Boolean
		  if column=1 then
		    Me.CellType(row,column)=ListBox.TypeEditable
		    Me.EditCell(row,column)
		  end if
		End Function
	#tag EndEvent
	#tag Event
		Sub CellLostFocus(row as Integer, column as Integer)
		  dim i, StartingNumber as Integer
		  
		  if column=1 then
		    StartingNumber=val(me.Cell(row,column))
		    for i = row+1 To StartList_DataList.ListCount-1
		      if me.Cell(i,column) <>"-" then
		        me.Cell(i,column)=str(StartingNumber+i-row)
		      end if
		    next
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbOK
	#tag Event
		Sub Action()
		  dim i,nRows as integer
		  dim SQLStatement as string
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  d.icon=1   //display warning icon
		  d.ActionButton.Caption="OK"
		  d.CancelButton.Visible=True     //show the Cancel button
		  d.CancelButton.Caption="Cancel"
		  d.Message="Do you wish to save the start times?"
		  d.Explanation = "This will update the start times for all the racers that listed start list area. There is no UNDO for this action."
		  
		  b=d.ShowModal     //display the dialog
		  if b=d.ActionButton then
		     nRows=StartList_DataList.ListCount-1
		    
		    Progress.Show
		    Progress.Initialize("Progress","Updating Start Times...",nRows,-1)
		    
		    for i=0 to nRows
		      if StartList_DataList.Cell(i,0)<>"0" then
		        SQLStatement="UPDATE participants SET Racer_Number = "+StartList_DataList.Cell(i,1)+", Assigned_Start='"+StartList_DataList.Cell(i,7)+"' WHERE rowid="+StartList_DataList.Cell(i,0)
		        App.theDB.DBSQLExecute(SQLStatement)
		        app.theDB.DBCommit
		      end if
		      Progress.UpdateProg1(i)
		    next
		    
		    Progress.Close
		    
		    wnd_List.ExecuteQuery("Participants",wnd_List.ExtraQuery_Participants,wnd_List.Participant_Sort)
		    
		    self.Close
		    
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbCancel
	#tag Event
		Sub Action()
		  self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbInsertGhost
	#tag Event
		Sub Action()
		  dim i,nRows As Integer
		  
		  for i = 0 To StartList_DataList.ListCount
		    if StartList_DataList.Selected(i) then
		      exit
		    end if
		  next
		  
		  if i >= 0 then
		    StartList_DataList.InsertRow(i,"")
		  else
		    StartList_DataList.AddRow("")
		  end if
		  StartList_DataList.Cell(StartList_DataList.LastIndex,0)="0"
		  StartList_DataList.Cell(StartList_DataList.LastIndex,1)="-"
		  StartList_DataList.Cell(StartList_DataList.LastIndex,2)="-"
		  StartList_DataList.Cell(StartList_DataList.LastIndex,3)="-"
		  StartList_DataList.Cell(StartList_DataList.LastIndex,4)="-"
		  
		  UpdateStartTimes
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbStartInterval
	#tag Event
		Sub Open()
		  dim i as integer
		  for i=0 to me.ListCount-1
		    me.ListIndex=i
		    if me.Text = wnd_List.cbTTStartInterval.Text then
		      exit
		    end if
		  next
		  if i=me.ListCount then
		    me.ListIndex=0
		  end if
		  app.StartIntervalSec=val(me.text)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Change()
		  app.StartIntervalSec=val(me.text)
		  UpdateStartTimes
		End Sub
	#tag EndEvent
	#tag Event
		Sub TextChanged()
		  app.StartIntervalSec=val(me.text)
		  UpdateStartTimes
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmRacersPerStart
	#tag Event
		Sub Open()
		  dim i as integer
		  for i=0 to me.ListCount-1
		    me.ListIndex=i
		    if me.Text = str(app.RacersPerStart) then
		      exit
		    end if
		  next
		  if i=me.ListCount then
		    me.ListIndex=0
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Change()
		  app.RacersPerStart=val(me.text)
		  UpdateStartTimes
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events bbDraw
	#tag Event
		Sub Action()
		  dim i, ii, assignedCount, lowestRacerNumber, SelectionStart, SelectionStop As Integer
		  dim ProposedStartTime as string
		  dim ProposedStartTimeFound as Boolean
		  dim r as new Random
		  
		  assignedCount=0
		  
		  for i = 0 To StartList_DataList.ListCount
		    if StartList_DataList.Selected(i) then
		      exit
		    end if
		  next
		  SelectionStart=i
		  
		  lowestRacerNumber=val(StartList_DataList.Cell(SelectionStart,7))
		  for i = SelectionStart To StartList_DataList.ListCount
		    if not(StartList_DataList.Selected(i)) then
		      exit
		    else
		      StartList_DataList.Cell(i,7)="" 'clear out the start times so we know if there has been one assigned
		      if lowestRacerNumber>val(StartList_DataList.Cell(i,1)) then
		        lowestRacerNumber=val(StartList_DataList.Cell(i,1))
		      end if
		    end if
		  next
		  SelectionStop=i-1
		  lowestRacerNumber=lowestRacerNumber-1
		  
		  for i = SelectionStart to SelectionStop 
		    
		    Do
		      ProposedStartTime=app.RaceDate.SQLDate+" "+app.CalculateStartTime(r.InRange(SelectionStart,SelectionStop)+1,"00:00:00",False)
		      ProposedStartTimeFound=False
		      for ii = SelectionStart to SelectionStop
		        if StartList_DataList.Cell(ii,7)=ProposedStartTime then
		          ProposedStartTimeFound=true
		        end if
		      next
		    loop until ProposedStartTimeFound=false
		    
		    StartList_DataList.Cell(i,7)=ProposedStartTime
		    
		  next
		  
		  StartList_DataList.SortedColumn=7
		  StartList_DataList.ColumnSortDirection(7)=ListBox.SortAscending
		  StartList_DataList.Sort
		  
		  for i = SelectionStart to SelectionStop 
		    StartList_DataList.Cell(i,1)=str(lowestRacerNumber+i)
		  next
		  
		  DragReorderRowsOccured=false
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="DragReorderRowsOccured"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
