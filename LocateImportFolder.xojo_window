#tag Window
Begin Window LocateImportFolder
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   1
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   272
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Select Import File..."
   Visible         =   True
   Width           =   439
   Begin TextField ImportFIlePath
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   125
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   190
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   266
   End
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Import from File:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   191
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin BevelButton LocateFile
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   ""
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   391
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   190
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   22
   End
   Begin GroupBox GroupBox1
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Column Seperator"
      Enabled         =   True
      Height          =   76
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   125
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   8
      Underline       =   False
      Visible         =   True
      Width           =   150
      Begin RadioButton rbComma
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Comma"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   145
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   3
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   31
         Underline       =   False
         Value           =   True
         Visible         =   True
         Width           =   80
      End
      Begin RadioButton rbTab
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Tab"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   145
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   4
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   56
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   80
      End
   End
   Begin PushButton pbOK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "OK"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   350
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   232
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PushButton pbCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   269
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   232
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PopupMenu TimeSource
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   "Select the source of the times to receive."
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "Select Time Source..."
      Italic          =   False
      Left            =   125
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   99
      Underline       =   False
      Visible         =   True
      Width           =   200
   End
   Begin TextField tfTimingPointID
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   125
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "###"
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   130
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   37
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Timing Point ID:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   131
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin TimeEditField tefAdjustmentTime
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      Italic          =   False
      Left            =   125
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   160
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   100
   End
   Begin PopupMenu pmOperation
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "Add\rSubtract"
      Italic          =   False
      Left            =   17
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   160
      Underline       =   False
      Visible         =   True
      Width           =   96
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  wnd_List.ImportFIlePath=""
		  select Case wnd_List.RaceTab.Value
		  case 0 'Race
		    LocateImportFolder.Title="Select the Race Import File..."
		    
		  case 1 'Divisions
		    LocateImportFolder.Title="Select the Division Import File..."
		    
		  case 2 'Teams
		    LocateImportFolder.Title="Select the Team Import File..."
		    
		  case 3 'Participants
		    LocateImportFolder.Title="Select the Participant Import File..."
		    
		  case 4 'Times
		    LocateImportFolder.Title="Select the Time Import File..."
		    
		  case 5 'Transponders
		    LocateImportFolder.Title="Select the Transponder Import File..."
		    
		  end Select 
		  
		  if TargetMacOS then
		    rbComma.Value=false
		    rbTab.Value=true
		  else
		    rbComma.Value=true
		    rbTab.Value=False
		  end if
		End Sub
	#tag EndEvent


	#tag Property, Flags = &h0
		f As folderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		ImportType As string
	#tag EndProperty


	#tag Constant, Name = ColumnSepStg, Type = String, Dynamic = False, Default = \"Column Seperator", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Column Seperator"
	#tag EndConstant

	#tag Constant, Name = CommaLabel, Type = String, Dynamic = False, Default = \"Comma", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Comma"
	#tag EndConstant

	#tag Constant, Name = FileLoclabel, Type = String, Dynamic = False, Default = \"Import from File:", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Import from File:"
	#tag EndConstant

	#tag Constant, Name = TabLabel, Type = String, Dynamic = False, Default = \"Tab", Scope = Protected
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Tab"
	#tag EndConstant


#tag EndWindowCode

#tag Events LocateFile
	#tag Event
		Sub Action()
		  dim f as FolderItem
		  f=GetOpenFolderItem("")
		  if f<> nil then
		    ImportFIlePath.text=f.AbsolutePath
		    wnd_List.ImportFIlePath=ImportFIlePath.text
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbOK
	#tag Event
		Sub Action()
		  dim n as Integer
		  
		  if rbComma.Value then
		    wnd_List.Seperator=","
		  else
		    wnd_List.Seperator=chr(9)
		  end if
		  
		  select Case wnd_List.RaceTab.Value
		  case 0 'Race
		    wnd_list.ImportRaceData
		    LocateImportFolder.Close
		    
		  case 1 'Divisions
		    wnd_list.ImportDivisionData
		    LocateImportFolder.Close
		    
		  case 2 'CCTeams
		    wnd_list.ImportTeamData
		    LocateImportFolder.Close
		    
		  case 3 'Participants
		    select case wnd_List.bbImportParticipants.MenuValue
		    case 0
		      wnd_List.ImportParticipantData
		    case 1
		      wnd_List.ImportAdjustmentData
		    end select
		    LocateImportFolder.Close
		    
		  case 4 'Times
		    
		    if TimeSource.RowTag(TimeSource.ListIndex) = -1 then
		      beep
		      n=MsgBox("Please select a Time Source.",16)
		    else
		      if tfTimingPointID.Text="" then
		        wnd_List.TimingPointID=0
		      else
		        wnd_List.TimingPointID=val(tfTimingPointID.Text)
		      end if
		      select case wnd_List.bbImportTimes.MenuValue 
		      case 0
		        wnd_List.ImportChipXTimeData
		      case 1
		        wnd_List.ImportToolKitTimeData
		      case 2
		        wnd_List.ImportiPicoTimeData
		      case 3
		        wnd_list.ImportPocketTimerTimeData
		      else
		        wnd_List.ImportMillisecondsProTimeData
		      end select
		      LocateImportFolder.Close
		    end if
		    
		  case 5 'Transponders
		    wnd_list.ImportTXData
		    
		  end Select 
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbCancel
	#tag Event
		Sub Action()
		  LocateImportFolder.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TimeSource
	#tag Event
		Sub Open()
		  dim SQLStatement as string
		  dim rsDistinctIntervals as RecordSet
		  dim res As Boolean
		  dim distinctRecCount as Integer
		  
		  me.RowTag(0)=-1
		  
		  me.AddRow "Start Times"
		  me.RowTag(me.ListCount-1)=0
		  me.AddRow "Multiple Intervals Same Point"
		  me.RowTag(me.ListCount-1)=9998
		  
		  SQLStatement="SELECT DISTINCT Interval_Name, Number FROM intervals WHERE RaceID =1 ORDER BY Number ASC"
		  rsDistinctIntervals=app.theDB.DBSQLSelect(SQLStatement)
		  
		  while not rsDistinctIntervals.eof
		    me.AddRow rsDistinctIntervals.Field("Interval_Name").StringValue
		    me.RowTag(me.ListCount-1)=rsDistinctIntervals.Field("Number").IntegerValue
		    rsDistinctIntervals.MoveNext
		  Wend
		  
		  me.AddRow "Stop Times"
		  me.RowTag(me.ListCount-1)=9999
		  me.ListIndex=0
		  
		  if wnd_List.RaceTab.Value <>4 then
		    me.Visible=false
		  else
		    me.Visible=true
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Change()
		  wnd_List.TimeSource=me.RowTag(me.ListIndex)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events tfTimingPointID
	#tag Event
		Sub Open()
		  if wnd_List.RaceTab.Value <>4 then
		    me.Visible=false
		  else
		    me.Visible=true
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events StaticText2
	#tag Event
		Sub Open()
		  if wnd_List.RaceTab.Value <>4 then
		    me.Visible=false
		  else
		    me.Visible=true
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events tefAdjustmentTime
	#tag Event
		Sub Open()
		  tefAdjustmentTime.text="00:00:00.000"
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImportType"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
