#tag Window
Begin Window ConnectToServer
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   400
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   False
   Title           =   "Connect to MySQL Server"
   Visible         =   True
   Width           =   985
   Begin GroupBox GroupBox3
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Connection"
      Enabled         =   True
      Height          =   146
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   16
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   10
      Underline       =   False
      Visible         =   True
      Width           =   420
      Begin BevelButton bbConnect
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   True
         ButtonType      =   0
         Caption         =   "Connect"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   97
         HelpTag         =   ""
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "GroupBox3"
         Italic          =   False
         Left            =   304
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   6
         TabPanelIndex   =   0
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   18.0
         TextUnit        =   0
         Top             =   41
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   120
      End
      Begin TextField ServerLocation
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   22
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox3"
         Italic          =   False
         Left            =   86
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "127.0.0.1"
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   73
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   160
      End
   End
   Begin GroupBox GroupBox1
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Remote Data"
      Enabled         =   True
      Height          =   174
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   16
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   168
      Underline       =   False
      Visible         =   True
      Width           =   420
      Begin Label StaticText4
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   25
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Race Name:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   196
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   95
      End
      Begin Label RemoteRaceName
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   124
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Remote Data Not Available"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   196
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   300
      End
      Begin Label StaticText5
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   25
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   2
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Participants:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   250
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   95
      End
      Begin Label RemoteParticipants
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   125
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   3
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "0"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   250
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label StaticText6
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   25
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   4
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Times:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   277
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   95
      End
      Begin Label RemoteTimes
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   125
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   5
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "0"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   277
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label StaticText7
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   25
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   6
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Transponders:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   304
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   95
      End
      Begin Label RemoteTransponders
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   125
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   7
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "0"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   304
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label StaticText12
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   25
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   8
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Divisions:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   223
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   95
      End
      Begin Label RemoteDivisions
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   125
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   9
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "0"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   223
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin BevelButton ClearRemoteData
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Clear"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   False
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   ""
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "GroupBox1"
         Italic          =   False
         Left            =   309
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   10
         TabPanelIndex   =   0
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   262
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   105
      End
   End
   Begin GroupBox GroupBox2
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Local Data"
      Enabled         =   True
      Height          =   174
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   545
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   168
      Underline       =   False
      Visible         =   True
      Width           =   420
      Begin Label StaticText8
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   554
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Race Name:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   196
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   95
      End
      Begin Label StaticText9
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   554
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   1
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Participants:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   250
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   95
      End
      Begin Label LocalParticipants
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   654
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   2
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "0"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   250
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label StaticText10
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   554
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   3
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Times:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   277
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   95
      End
      Begin Label LocalTimes
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   654
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   4
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "0"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   277
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin Label StaticText11
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   554
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   5
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Transponders:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   304
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   95
      End
      Begin Label LocalTransponders
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   654
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   6
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "0"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   304
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin BevelButton OpenLocalDBFile
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Open"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   ""
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   841
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   7
         TabPanelIndex   =   0
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   228
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   105
      End
      Begin Label LocalRaceName
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   654
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   8
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Local Data Not Selected"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   196
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   267
      End
      Begin Label StaticText13
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   554
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   9
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "Divisions:"
         TextAlign       =   2
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   223
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   95
      End
      Begin Label LocalDivisions
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   654
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   0
         Selectable      =   False
         TabIndex        =   10
         TabPanelIndex   =   0
         TabStop         =   True
         Text            =   "0"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   223
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   100
      End
      Begin BevelButton NewLocalDBFile
         AcceptFocus     =   False
         AutoDeactivate  =   False
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "New"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   ""
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   841
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   11
         TabPanelIndex   =   0
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   262
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   105
      End
      Begin BevelButton OpenLocalDBFile1
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Close"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   ""
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "GroupBox2"
         Italic          =   False
         Left            =   841
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   0
         TabIndex        =   12
         TabPanelIndex   =   0
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   296
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   105
      End
   End
   Begin BevelButton CopyRemoteToLocal
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "-> Copy ->"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   False
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   448
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   228
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   85
   End
   Begin BevelButton CopyLocalToRemote
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "<- Copy <-"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   False
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   448
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   277
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   85
   End
   Begin PushButton PushButton1
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Done"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   885
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   360
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin Label stConnected
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   89
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   510
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Connected"
      TextAlign       =   1
      TextColor       =   &c00804000
      TextFont        =   "System"
      TextSize        =   56.0
      TextUnit        =   0
      Top             =   41
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   425
   End
   Begin Label stNotConnected
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   89
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   510
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Not Connected"
      TextAlign       =   1
      TextColor       =   &cFF000000
      TextFont        =   "System"
      TextSize        =   56.0
      TextUnit        =   0
      Top             =   40
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   425
   End
   Begin AutoDiscovery AutoDiscovery1
      BroadcastAddress=   ""
      Handle          =   0
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      IsConnected     =   False
      LastErrorCode   =   0
      Left            =   16
      LocalAddress    =   ""
      LockedInPosition=   False
      PacketsAvailable=   0
      PacketsLeftToSend=   0
      Port            =   9090
      RouterHops      =   0
      Scope           =   0
      SendToSelf      =   False
      TabIndex        =   8
      TabPanelIndex   =   "0"
      TabStop         =   True
      Top             =   360
      Width           =   32
   End
   Begin Timer Timer1
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   65
      LockedInPosition=   False
      Mode            =   2
      Period          =   1000
      Scope           =   0
      TabIndex        =   9
      TabPanelIndex   =   "0"
      TabStop         =   True
      Top             =   360
      Width           =   32
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  Dim n as NetworkInterface
		  n=System.GetNetworkInterface
		  MyIP=n.IPAddress
		  
		  AutoDiscovery1.Bind(9090)
		  AutoDiscovery1.Register( "MillisecondsProServer" )
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub UpdateLocalDBStats()
		  Dim SQLStatement as String
		  Dim rs as RecordSet
		  
		  SQLStatement="SELECT Race_Name FROM races WHERE RaceID=1"
		  rs=LocalDB.SQLSelect(SQLStatement)
		  if rs.RecordCount>0 then
		    LocalRaceName.Text=rs.Field("Race_Name").StringValue
		    rs.Close
		    
		    SQLStatement="SELECT rowid FROM divisions"
		    rs=LocalDB.SQLSelect(SQLStatement)
		    LocalDivisions.Text=format(rs.RecordCount,"#,###,###,###")
		    rs.Close
		    
		    SQLStatement="SELECT rowid FROM participants"
		    rs=LocalDB.SQLSelect(SQLStatement)
		    LocalParticipants.Text=format(rs.RecordCount,"#,###,###,###")
		    rs.Close
		    
		    SQLStatement="SELECT rowid FROM times"
		    rs=LocalDB.SQLSelect(SQLStatement)
		    LocalTimes.Text=format(rs.RecordCount,"#,###,###,###")
		    rs.Close
		    
		    SQLStatement="SELECT rowid FROM transponders"
		    rs=LocalDB.SQLSelect(SQLStatement)
		    LocalTransponders.Text=format(rs.RecordCount,"#,###,###,###")
		    rs.Close
		  else
		    rs.Close
		    LocalRaceName.Text="no race data exists"
		    LocalDivisions.Text="0"
		    LocalParticipants.Text="0"
		    LocalTimes.Text="0"
		    LocalTransponders.Text="0"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateRemoteDBStats()
		  Dim SQLStatement as String
		  Dim rs as RecordSet
		  
		  SQLStatement="SELECT Race_Name FROM races WHERE RaceID=1"
		  rs=app.theDB.DBSQLSelect(SQLStatement)
		  if rs.RecordCount>0 then
		    RemoteRaceName.Text=rs.Field("Race_Name").StringValue
		    rs.Close
		    
		    SQLStatement="SELECT rowid FROM divisions"
		    rs=app.theDB.DBSQLSelect(SQLStatement)
		    RemoteDivisions.Text=format(rs.RecordCount,"#,###,###,###")
		    rs.Close
		    
		    SQLStatement="SELECT rowid FROM participants"
		    rs=app.theDB.DBSQLSelect(SQLStatement)
		    RemoteParticipants.Text=format(rs.RecordCount,"#,###,###,###")
		    rs.Close
		    
		    SQLStatement="SELECT rowid FROM times"
		    rs=app.theDB.DBSQLSelect(SQLStatement)
		    RemoteTimes.Text=format(rs.RecordCount,"#,###,###,###")
		    rs.Close
		    
		    SQLStatement="SELECT rowid FROM transponders"
		    rs=app.theDB.DBSQLSelect(SQLStatement)
		    RemoteTransponders.Text=format(rs.RecordCount,"#,###,###,###")
		    rs.Close
		    
		  else
		    rs.Close
		    RemoteRaceName.Text="no race data exists"
		    RemoteDivisions.Text="0"
		    RemoteParticipants.Text="0"
		    RemoteTimes.Text="0"
		    RemoteTransponders.Text="0"
		  end if
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		LocalDB As RealSQLDatabase
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected MyIP As string
	#tag EndProperty


#tag EndWindowCode

#tag Events bbConnect
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  
		  if me.Caption="Connect" then
		    app.theDB.MySQLDB=New MySQLCommunityServer
		    app.theDB.MySQLDB.host=ServerLocation.Text
		    app.theDB.MySQLDB.port=3306
		    app.theDB.MySQLDB.databaseName="millisecondsPro"
		    app.theDB.MySQLDB.userName="milliseconds"
		    app.theDB.MySQLDB.Password="milli2nds"
		    If not(app.theDB.MySQLDB.Connect) then
		      d.icon=1
		      d.Message="Connection to the database failed."
		      d.Explanation="Error ("+str(app.theDB.MySQLDB.ErrorCode)+"): "+app.theDB.MySQLDB.ErrorMessage
		      d.ActionButton.Caption="OK"
		      b=d.ShowModal
		      
		      app.theDB.MySQLDBHost=""
		      app.theDB.MySQLDBUserName=""
		      app.theDB.MySQLDBPassword=""
		      
		    else
		      app.theDB.DatabaseType="MySQLDatabase"
		      
		      stConnected.Visible=true
		      stNotConnected.Visible=false
		      ClearRemoteData.Enabled=true
		      me.Caption="Disconnect"
		      
		      if LocalDB<>nil then
		        CopyLocalToRemote.Enabled=true
		        CopyRemoteToLocal.Enabled=true
		      end if
		      
		      app.theDB.MySQLDBHost=ServerLocation.Text
		      app.theDB.MySQLDBUserName="milliseconds"
		      app.theDB.MySQLDBPassword="milli2nds"
		      
		      UpdateRemoteDBStats
		      App.theDB.MySQL_MultiTreadingOff
		      
		    end if
		  else
		    me.Caption="Connect"
		    app.theDB.DatabaseType=""
		    stConnected.Visible=false
		    stNotConnected.Visible=true
		    ClearRemoteData.Enabled=false
		    app.theDB.MySQLDB.Close
		    
		    app.theDB.MySQLDBHost=""
		    app.theDB.MySQLDBUserName=""
		    app.theDB.MySQLDBPassword=""
		    
		  end if
		  
		  if ServerLocation.Text="127.0.0.1" then
		    app.IAmServer=true
		  end if
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  if app.theDB.MySQLDBHost <> "" then
		    me.Caption="Disconnect"
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ClearRemoteData
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  Dim rsFieldNames, rsTableNames, rsLocalRecords as RecordSet
		  Dim i, ii, iii, iiii as Integer
		  Dim InsertStatement, SQLStatement as String
		  
		  d.icon=MessageDialog.GraphicCaution
		  d.ActionButton.Caption="Cancel"
		  d.CancelButton.Caption="Clear"
		  d.CancelButton.Visible=true
		  d.Message="Remove ALL data from the remote server."
		  d.Explanation="Are you sure? There is no undo for this action."
		  b=d.ShowModal
		  
		  if b=d.CancelButton then
		    app.theDB.DBSQLExecute("DROP TABLE divisions")
		    app.theDB.DBSQLExecute("DROP TABLE intervals")
		    app.theDB.DBSQLExecute("DROP TABLE participants")
		    app.theDB.DBSQLExecute("DROP TABLE racedistances")
		    app.theDB.DBSQLExecute("DROP TABLE races")
		    app.theDB.DBSQLExecute("DROP TABLE times")
		    app.theDB.DBSQLExecute("DROP TABLE transponders")
		    
		    app.theDB.DBSQLExecute(app.theDB.mySQLSchema_Divisions)
		    app.theDB.DBSQLExecute(app.theDB.mySQLSchema_Intervals)
		    app.theDB.DBSQLExecute(app.theDB.mySQLSchema_Participants)
		    app.theDB.DBSQLExecute(app.theDB.mySQLSchema_RaceDistances)
		    app.theDB.DBSQLExecute(app.theDB.mySQLSchema_Races)
		    app.theDB.DBSQLExecute(app.theDB.mySQLSchema_Times)
		    app.theDB.DBSQLExecute(app.theDB.mySQLSchema_Transponders)
		    
		    UpdateRemoteDBStats
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OpenLocalDBFile
	#tag Event
		Sub Action()
		  Dim LocalDBFile as FolderItem
		  
		  LocalDBFile=GetOpenFolderItem("")
		  if LocalDBFile <> nil then
		    LocalDB=New REALSQLDatabase
		    LocalDB.DatabaseFile=LocalDBFile
		    If LocalDB.Connect() then
		      UpdateLocalDBStats
		      if bbConnect.Caption="Disconnect" then
		        CopyLocalToRemote.Enabled=true
		        CopyRemoteToLocal.Enabled=true
		      end if
		    end 
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events NewLocalDBFile
	#tag Event
		Sub Action()
		  Dim LocalDBFile as FolderItem
		  LocalDBFile=GetSaveFolderItem("","New Race.rdb")
		  if LocalDBFile <> nil then
		    LocalDB=New REALSQLDatabase
		    LocalDB.databaseFile=LocalDBFile
		    If LocalDB.CreateDatabaseFile then
		      //proceed with database operations...
		      LocalDB.SQLExecute("DROP TABLE Movies")
		      LocalDB.SQLExecute("DROP TABLE Actors")
		      LocalDB.SQLExecute(app.theDB.RBSchema_Divisions)
		      LocalDB.SQLExecute(app.theDB.RBSchema_Intervals)
		      LocalDB.SQLExecute(app.theDB.RBSchema_Participants)
		      LocalDB.SQLExecute(app.theDB.RBSchema_RaceDistances)
		      LocalDB.SQLExecute(app.theDB.RBSchema_Races)
		      LocalDB.SQLExecute(app.theDB.RBSchema_Times)
		      LocalDB.SQLExecute(app.theDB.RBSchema_Transponders)
		      LocalDB.SQLExecute(app.theDB.RBCreateParticipantsRacerNumberIndex)
		    else
		      beep
		      MsgBox "The race data file could not be created"
		    end if
		    
		    UpdateLocalDBStats
		    if bbConnect.Caption="Disconnect" then
		      CopyLocalToRemote.Enabled=true
		      CopyRemoteToLocal.Enabled=true
		    end if
		  end
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OpenLocalDBFile1
	#tag Event
		Sub Action()
		  if LocalDB<>nil then
		    LocalDB.Close
		    LocalRaceName.Text="Local Data Not Selected"
		    LocalDivisions.Text="0"
		    LocalParticipants.Text="0"
		    LocalTimes.Text="0"
		    LocalTransponders.Text="0"
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events CopyRemoteToLocal
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  Dim rsFieldNames, rsTableNames, rsRemoteRecords as RecordSet
		  Dim i, ii, iii, iiii as Integer
		  Dim InsertFirstHalf,InsertSecondHalf, SQLStatement as String
		  
		  d.icon=MessageDialog.GraphicCaution
		  d.ActionButton.Caption="Cancel"
		  d.CancelButton.Caption="Copy"
		  d.CancelButton.Visible=true
		  d.Message="Copy data from the REMOTE server to the LOCAL server."
		  d.Explanation="Are you sure? There is no undo for this action."
		  b=d.ShowModal
		  
		  if b=d.CancelButton then
		    rsTableNames=app.theDB.MySQLDB.TableSchema
		    
		    Progress.Show
		    Progress.Initialize("Copying Data from Remote Server to Local Database","Copy Data...",rsTableNames.RecordCount,0)
		    
		    for i = 1 to rsTableNames.RecordCount
		      
		      Progress.UpdateProg1(i)
		      Progress.UpdateMessage("Copying "+rsTableNames.IdxField(1).StringValue+"...")
		      
		      LocalDB.SQLExecute("DELETE FROM "+rsTableNames.IdxField(1).StringValue)
		      LocalDB.Commit
		      
		      InsertFirstHalf="INSERT INTO "+rsTableNames.IdxField(1).StringValue+" ("
		      rsFieldNames=app.theDB.MySQLDB.FieldSchema(rsTableNames.IdxField(1))
		      for ii = 1 to rsFieldNames.RecordCount
		        InsertFirstHalf=InsertFirstHalf+rsFieldNames.IdxField(1).StringValue+","
		        rsFieldNames.MoveNext
		      next
		      rsFieldNames.Close
		      InsertFirstHalf=Left(InsertFirstHalf,Len(InsertFirstHalf)-1)+") VALUES "
		      
		      SQLStatement="SELECT * FROM "+rsTableNames.IdxField(1).StringValue
		      rsRemoteRecords=app.theDB.DBSQLSelect(SQLStatement)
		      
		      Progress.setMax2(rsRemoteRecords.RecordCount)
		      Progress.UpdateProg2(0)
		      
		      for iii = 1 to rsRemoteRecords.RecordCount
		        Progress.UpdateProg2(iii)
		        InsertSecondHalf="("
		        for iiii = 1 to rsRemoteRecords.FieldCount
		          InsertSecondHalf=InsertSecondHalf+"'"+rsRemoteRecords.IdxField(iiii).StringValue+"'"
		          if iiii <> rsRemoteRecords.FieldCount then
		            InsertSecondHalf=InsertSecondHalf+","
		          else
		            InsertSecondHalf=InsertSecondHalf+"), "
		          end if
		        next
		        
		        InsertSecondHalf=left(InsertSecondHalf,len(InsertSecondHalf)-2) 'remove the last ", "
		        LocalDB.SQLExecute(InsertFirstHalf+InsertSecondHalf)
		        LocalDB.Commit
		        
		        rsRemoteRecords.MoveNext
		      next
		      
		      rsRemoteRecords.Close
		      rsTableNames.MoveNext
		    next
		    rsTableNames.Close
		    UpdateLocalDBStats
		    LocalDB.Close
		    Progress.Close
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events CopyLocalToRemote
	#tag Event
		Sub Action()
		   Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  Dim rsFieldNames, rsTableNames, rsLocalRecords as RecordSet
		  Dim i, ii, iii, iiii as Integer
		  Dim InsertFirstHalf, InsertSecondHalf, SQLStatement as String
		  
		  d.icon=MessageDialog.GraphicCaution
		  d.ActionButton.Caption="Cancel"
		  d.CancelButton.Caption="Copy"
		  d.CancelButton.Visible=true
		  d.Message="Copy data from the LOCAL data file to the REMOTE server."
		  d.Explanation="Are you sure? There is no undo for this action."
		  b=d.ShowModal
		  
		  if b=d.CancelButton then
		    rsTableNames=LocalDB.TableSchema
		    
		    Progress.Show
		    Progress.Initialize("Copying Data from Local Database to Remote Server","Copy Data...",rsTableNames.RecordCount,0)
		    
		    for i = 1 to rsTableNames.RecordCount
		      app.theDB.DBSQLExecute("DROP TABLE "+rsTableNames.IdxField(1).StringValue)
		      rsTableNames.MoveNext
		    next
		    app.theDB.DBSQLExecute("FLUSH TABLES ")
		    app.theDB.DBSQLExecute(DBSupport.mySQLSchema_Divisions)
		    app.theDB.DBSQLExecute(DBSupport.mySQLSchema_Intervals)
		    app.theDB.DBSQLExecute(DBSupport.mySQLSchema_NGBPoints)
		    app.theDB.DBSQLExecute(DBSupport.mySQLSchema_Participants)
		    app.theDB.DBSQLExecute(DBSupport.mySQLSchema_RaceDistances)
		    app.theDB.DBSQLExecute(DBSupport.mySQLSchema_Races)
		    app.theDB.DBSQLExecute(DBSupport.mySQLSchema_TeamMembers)
		    app.theDB.DBSQLExecute(DBSupport.mySQLSchema_Times)
		    app.theDB.DBSQLExecute(DBSupport.mySQLSchema_Transponders)
		    app.theDB.DBSQLExecute(DBSupport.mySQLSchema_CCTeams)
		    app.theDB.DBSQLExecute(DBSupport.mySQLSchema_CCTeamIntervals)
		    app.theDB.DBSQLExecute(DBSupport.mySQLCreateParticipantsRacerNumberIndex)
		    app.theDB.DBSQLExecute(DBSupport.mySQL_SetForeignKeyChecks0)
		    app.theDB.DBSQLExecute(DBSupport.mySQL_SetForeignKeyChecks1)
		    app.theDB.MySQLDB.Commit
		    
		    rsTableNames.close
		    rsTableNames=LocalDB.TableSchema
		    
		    for i = 1 to rsTableNames.RecordCount
		      
		      Progress.UpdateProg1(i)
		      Progress.UpdateMessage("Copying "+rsTableNames.IdxField(1).StringValue+"...")
		      
		      app.theDB.DBSQLExecute("DELETE FROM "+rsTableNames.IdxField(1).StringValue)
		      app.theDB.MySQLDB.Commit
		      
		      InsertFirstHalf="INSERT INTO "+rsTableNames.IdxField(1).StringValue+" (rowid,"
		      rsFieldNames=LocalDB.FieldSchema(rsTableNames.IdxField(1))
		      for ii = 1 to rsFieldNames.RecordCount
		        InsertFirstHalf=InsertFirstHalf+rsFieldNames.IdxField(1).StringValue+","
		        rsFieldNames.MoveNext
		      next
		      rsFieldNames.Close
		      InsertFirstHalf=Left(InsertFirstHalf,Len(InsertFirstHalf)-1)+") VALUES "
		      
		      SQLStatement="SELECT rowid,* FROM "+rsTableNames.IdxField(1).StringValue
		      rsLocalRecords=LocalDB.SQLSelect(SQLStatement)
		      
		      Progress.setMax2(rsLocalRecords.RecordCount)
		      Progress.UpdateProg2(0)
		      
		      for iii = 1 to rsLocalRecords.RecordCount
		        Progress.UpdateProg2(iii)
		        InsertSecondHalf="("
		        for iiii = 1 to rsLocalRecords.FieldCount
		          InsertSecondHalf=InsertSecondHalf+"'"+rsLocalRecords.IdxField(iiii).StringValue+"'"
		          if iiii <> rsLocalRecords.FieldCount then
		            InsertSecondHalf=InsertSecondHalf+","
		          else
		            InsertSecondHalf=InsertSecondHalf+"), "
		          end if
		        next
		        
		        InsertSecondHalf=left(InsertSecondHalf,len(InsertSecondHalf)-2) 'remove the last ", "
		        app.theDB.DBSQLExecute(InsertFirstHalf+InsertSecondHalf)
		        app.theDB.MySQLDB.Commit
		        
		        rsLocalRecords.MoveNext
		      next
		      
		      
		      rsLocalRecords.Close
		      rsTableNames.MoveNext
		    next
		    rsTableNames.Close
		    Progress.Close
		    UpdateRemoteDBStats
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events PushButton1
	#tag Event
		Sub Action()
		  if bbConnect.Caption="Disconnect" then
		    if LocalDB<>nil then
		      LocalDB.close
		    end if
		    'display the race window
		    wnd_List.Show
		    wnd_List.SetupListBox
		    wnd_List.LoadRaceTab
		    app.ChangeRace
		  else
		    wnd_List.close
		  end if
		  self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events stNotConnected
	#tag Event
		Sub Open()
		  if app.theDB.MySQLDBHost="" then
		    stConnected.Visible=false
		  else
		    me.Visible=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events AutoDiscovery1
	#tag Event
		Sub MemberJoined(ip as String)
		  if MyIP<> ip and ServerLocation.Text <> MyIP then
		    Timer1.Enabled=false
		    ServerLocation.Text = ip
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Timer1
	#tag Event
		Sub Action()
		  dim result() as String
		  
		  AutoDiscovery1.Unregister( "MillisecondsProServer" )
		  AutoDiscovery1.Register( "MillisecondsProServer" )
		  result=AutoDiscovery1.GetMemberList
		  AutoDiscovery1.UpdateMemberList
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
