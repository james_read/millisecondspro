#tag Window
Begin Window TXCheckIn
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   309
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Check In"
   Visible         =   True
   Width           =   357
   Begin BevelButton ResetCount
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "99,999"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   False
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   166
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   48.0
      TextUnit        =   0
      Top             =   57
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   317
   End
   Begin BevelButton btnConnect
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Connect"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   False
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   "Click to establish a connection using the selected connection method."
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   277
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   "0"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   267
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   60
   End
   Begin ComboBox ConnectMethod
      AutoComplete    =   False
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   "Select the desired connection method or enter an IP Address of the timing point."
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "Select a Method..."
      Italic          =   False
      Left            =   108
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   267
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   157
   End
   Begin CheckBox cbPrintReceipt
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Print Receipt"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      State           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   239
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Total Checked In:"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   21
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   106
   End
   Begin Serial Serial1
      Baud            =   8
      Bits            =   3
      CTS             =   False
      DTR             =   False
      Height          =   "32"
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   10
      LockedInPosition=   False
      Parity          =   0
      Scope           =   "0"
      Stop            =   0
      TabIndex        =   "5"
      TabPanelIndex   =   "0"
      TabStop         =   True
      Top             =   271
      Width           =   "32"
      XON             =   False
   End
   Begin TCPSocket TCPSocket1
      Address         =   ""
      Height          =   "32"
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   71
      LockedInPosition=   False
      Port            =   5100
      Scope           =   "0"
      TabIndex        =   "6"
      TabPanelIndex   =   "0"
      TabStop         =   True
      Top             =   271
      Width           =   "32"
   End
   Begin Label stTotalCheckInCount
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   127
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "0"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   21
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   76
   End
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Connect via:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   268
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   76
   End
   Begin PushButton ResendData
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Resend Data"
      Default         =   False
      Enabled         =   False
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   237
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   21
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  Reader = new ReaderSupport
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Function CheckInTX(TXCode as string) As Integer
		  dim res as boolean
		  dim CurrentDateTime as new date
		  dim retdate as date
		  dim Success, n as integer
		  dim rsTX as RecordSet
		  dim SQLStatement as String
		  
		  Success=-1
		  
		  rsTx=app.theDB.DBSQLSelect("Select Returned FROM transponders WHERE TX_Code ='"+TXCode+"'")
		  if not rsTX.eof then
		    if rsTX.Field("Returned").StringValue="0-00-00 00:00:00" or rsTX.Field("Returned").StringValue="0000-00-00 00:00:00" or rsTX.Field("Returned").StringValue="" then
		      App.theDB.DBSQLExecute("UPDATE transponders SET Returned='"+CurrentDateTime.SQLDateTime+"' WHERE TX_Code='"+TXCode+"'")
		      Success=1  
		    else
		      Success=2
		    end if
		  end if
		  rsTX.Close
		  Return Success
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ProcessData()
		  dim i, CRLFPos, DelimiterLength as integer
		  dim DataRecord as string
		  dim result as Boolean
		  dim TXNumber as String
		  dim rs as RecordSet
		  
		  CRLFPos=0
		  
		  
		  Select Case IncomingDataSource
		  Case "Serial1"
		    IncomingData=IncomingData+Serial1.ReadAll //read the buffer
		  case "TCPSocket1"
		    IncomingData=IncomingData+TCPSocket1.ReadAll //read the buffer
		  end select
		  
		  CRLFPos=InStr(IncomingData,Chr(13)+Chr(10)) // see if we are at the end of the record
		  DelimiterLength=2
		  if(CRLFPos=0) then
		    CRLFPos=InStr(IncomingData,Chr(13))
		    DelimiterLength=1
		  end if
		  if(CRLFPos=0) then
		    CRLFPos=InStr(IncomingData,Chr(10))
		    DelimiterLength=1
		  end if
		  if(CRLFPos>0) then // why yes we are
		    while CRLFPos>0
		      DataRecord=Left(IncomingData,CRLFPos) //pull out the data
		      IncomingData=Mid(IncomingData,CRLFPos+DelimiterLength) // save any extra that came in
		      DataRecord=LTrim(RTrim(DataRecord))
		      if Reader.ValidCRC(DataRecord) then
		        if Reader.ParseRecord(DataRecord) then
		          
		          if Reader.PassingRecord and Reader.LastSeen = false then // it's a passing record  
		            
		            if Reader.Type<>"ToolKit" then
		              
		              select case CheckInTX(Reader.TXCode) 
		              case 1, 3
		                CheckInCount=CheckInCount+1
		                ResetCount.Caption=Format(CheckInCount,"##,##0")
		                TotalCheckInCount=TotalCheckInCount+1
		                stTotalCheckInCount.Text=Format(TotalCheckInCount,"###,###,##0")
		                ResetCount.Refresh
		                stTotalCheckInCount.Refresh
		                
		                if cbPrintReceipt.Value then
		                  rs=app.theDB.DBSQLSelect("Select Racer_Number FROM transponders WHERE TX_Code ='"+Reader.TXCode+"'")
		                  app.PrintCheckInReceipt(rs.Field("Racer_Number").StringValue)
		                end if
		              case 2
		                'already checked in don't count it again
		                
		              end select
		              
		            else
		              
		              for i = 0 to UBound(Reader.TK_TXCode)
		                
		                select case CheckInTX(Reader.TK_TXCode(i))
		                case 1, 3
		                  CheckInCount=CheckInCount+1
		                  ResetCount.Caption=Format(CheckInCount,"##,##0")
		                  TotalCheckInCount=TotalCheckInCount+1
		                  stTotalCheckInCount.Text=Format(TotalCheckInCount,"###,###,##0")
		                  ResetCount.Refresh
		                  stTotalCheckInCount.Refresh
		                  
		                  if cbPrintReceipt.Value then
		                    rs=app.theDB.DBSQLSelect("Select Racer_Number FROM transponders WHERE TX_Code ='"+Reader.TK_TXCode(i)+"'")
		                    app.PrintCheckInReceipt(rs.Field("Racer_Number").StringValue)
		                  end if
		                case 2
		                  'already checked in don't count it again
		                  
		                end select
		                
		              next
		              redim Reader.TK_TXCode(0)
		              redim Reader.TK_Time(0)
		              TCPSocket1.write Reader.TK_AckStore(Reader.TK_Source,Reader.TK_MessageNumber)
		              
		            end if
		          end if
		        end if
		      end if
		      CRLFPos=InStr(IncomingData,Chr(13)+Chr(10)) // see if we are at the end of the record
		    wend
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ResetTotalCount()
		  dim recsRS as RecordSet
		  recsRS=app.theDB.DBSQLSelect("SELECT COUNT(*)  FROM transponders WHERE RaceID=1 AND Returned <> '0000-00-00 00:00:00'")
		  if recsRS<>nil then
		    stTotalCheckInCount.text=format(recsRS.idxField(1).integerValue,"###,###,##0")
		    recsRS.close
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SendData(DataToSend as string)
		  Select Case IncomingDataSource
		  Case "Serial1"
		    Serial1.write DataToSend
		  case "TCPSocket1"
		    TCPSocket1.Write DataToSend
		  end select
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected CheckInCount As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected FailedCount As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected IncomingData As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected IncomingDataSource As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ProcessingData As boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private Reader As ReaderSupport
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected TCPIPConnectionEstablished As boolean
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected TotalCheckInCount As Integer
	#tag EndProperty


#tag EndWindowCode

#tag Events ResetCount
	#tag Event
		Sub Open()
		  me.Caption="0"
		End Sub
	#tag EndEvent
	#tag Event
		Sub Action()
		  CheckInCount=0
		  ResetCount.Caption=Format(CheckInCount,"##,##0")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnConnect
	#tag Event
		Sub Action()
		  Dim i, ii, TickCount, AsciiCode as Integer
		  Dim SerialPortNameString as string
		  
		  if btnConnect.Caption="Connect" then
		    if left(ConnectMethod.text,7)="Serial:" then
		      ProcessingData=True
		      IncomingDataSource="Serial1"
		      for i=0 to (System.SerialPortCount-1)
		        if InStr(ConnectMethod.text,System.SerialPort(i).name) > 0 then
		          if TargetWin32 then
		            SerialPortNameString=""
		            for ii=1 to len(System.SerialPort(i).name)
		              AsciiCode= asc(mid(System.SerialPort(i).name,ii,1))
		              if(AsciiCode>=48 and AsciiCode<=57) then
		                SerialPortNameString=SerialPortNameString+mid(System.SerialPort(i).name,ii,1)
		              end if
		            next
		            Serial1.Port=val(SerialPortNameString)-1
		          else
		            Serial1.Port = i
		          end if
		        end if
		      next
		      
		      If Serial1.Open then
		        btnConnect.Caption="Done"
		        btnConnect.HelpTag="Click when done checking in transponders."
		        ResetCount.Enabled=true
		        ResendData.Enabled=true
		      Else
		        MsgBox "The serial port could not be opened."
		      End if
		    else
		      IncomingDataSource="TCPSocket1"
		      ProcessingData=True
		      
		      if ConnectMethod.Text<>"ToolKit" then
		        
		        TCPSocket1.Address=ConnectMethod.text
		        TCPsocket1.Port=5100 ' Try an AMB connection first
		        
		        TickCount=Ticks
		        TCPsocket1.Connect
		        
		        tcpIPConnectionEstablished=false
		        Do
		          app.DoEvents
		        Loop Until TCPIPConnectionEstablished or Ticks >= TickCount+240
		        
		      else
		        TickCount=Ticks
		        TCPSocket1.Port=3097
		        TCPSocket1.Listen
		        Do
		          app.DoEvents
		          if TCPSocket1.IsConnected then
		            TCPIPConnectionEstablished=true
		            App.ReaderType="ToolKit"
		            TCPSocket1.Write "@Ping@$"
		          end if
		        Loop Until TCPIPConnectionEstablished or Ticks >= TickCount+240
		        
		      end if
		      
		      if TCPIPConnectionEstablished then
		        btnConnect.Caption="Done"
		        btnConnect.HelpTag="Click when done checking in transponders."
		        ResetCount.Enabled=true
		        ResendData.Enabled=true
		      else
		        TCPsocket1.Port=10200'try iPico
		        
		        TickCount=Ticks
		        TCPsocket1.Connect
		        
		        tcpIPConnectionEstablished=false
		        Do
		          app.DoEvents
		        Loop Until TCPIPConnectionEstablished or Ticks >= TickCount+240
		        
		        if TCPIPConnectionEstablished then
		          btnConnect.Caption="Done"
		          btnConnect.HelpTag="Click when done checking in transponders."
		          ResetCount.Enabled=true
		          ResendData.Enabled=true
		        else
		          MsgBox "Unable to establish a network connection to "+ConnectMethod.text
		        end if
		      end if
		    end if
		  else
		    
		    Select Case IncomingDataSource
		    Case "Serial1"
		      Serial1.close
		    case "TCPSocket1"
		      TCPSocket1.close
		    end select
		    TXCheckIn.Close
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ConnectMethod
	#tag Event
		Sub Change()
		  if me.text<>"Select a Method..." then
		    btnConnect.Enabled=true
		  else
		    btnConnect.Enabled=false
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  dim i as integer
		  dim stream as TextInputStream
		  
		  for i=0 to (System.SerialPortCount-1)
		    me.AddRow "Serial: "+System.SerialPort(i).Name
		  next 
		  
		  me.AddRow "ToolKit"
		  
		  me.ListIndex=0
		End Sub
	#tag EndEvent
	#tag Event
		Sub TextChanged()
		  dim DataSource(-1) as String
		  
		  DataSource=split(me.Text,".")
		  
		  if Ubound(DataSource)=3 then
		    btnConnect.Enabled=true
		  else
		    btnConnect.Enabled=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Serial1
	#tag Event
		Sub DataAvailable()
		  ProcessData
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TCPSocket1
	#tag Event
		Sub Connected()
		  beep
		  TCPIPConnectionEstablished = true
		End Sub
	#tag EndEvent
	#tag Event
		Sub DataAvailable()
		  ProcessData
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events stTotalCheckInCount
	#tag Event
		Sub Open()
		  ResetTotalCount
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ResendData
	#tag Event
		Sub Action()
		  ResetTotalCount
		  CheckInCount=0
		  ResetCount.Caption=Format(CheckInCount,"##,##0")
		  SendData(Reader.ResendData(Reader.DecoderID))
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
