#tag Module
Protected Module UTCHelper
	#tag Method, Flags = &h0
		Function UTCOffsetInMinutes() As Integer
		  // Returns the offset of the current time to UTC (GMT) in minutes.
		  // supports Mac OS, Windows and Linux
		  //
		  // Note that the offset is not always an even multiple of 60,  but
		  // there are also half hour offsets, even one 5:45h offset
		  
		  // This 5th version by Thomas Tempelmann (rb@tempel.org) on 21 Apr 2006
		  //
		  // Using code from various authors found on the RB NUG mailing list
		  //
		  // Latest version can be found here: http://www.tempel.org/rb/#gmt
		  
		  dim offset as integer
		  
		  #if TargetLinux or TargetMachO then
		    #if TargetMachO then
		      declare function localtime_r lib "/usr/lib/libc.dylib" (time_in as Ptr, time_out as Ptr) as Ptr
		      declare function time lib "/usr/lib/libc.dylib" (tloc_ptr as Integer) as Integer
		    #else
		      declare function localtime_r lib "/usr/lib/libc.so" (time_in as Ptr, time_out as Ptr) as Ptr
		      declare function time lib "/usr/lib/libc.so" (tloc_ptr as Integer) as Integer
		    #endif
		    dim time_in, time_out as MemoryBlock
		    time_in =  new MemoryBlock(4)
		    time_out =  new MemoryBlock(44) // we assume that "int" is 4 byte on any supported Linux system
		    time_in.Long(0) = time (0) // Bugfix TT 21Apr06: need to pass current local time to get summer time offsets correctly
		    call localtime_r (time_in, time_out)
		    offset = time_out.Long (36) \ 60
		  #elseif TargetMacOS then
		    #if TargetMachO
		      Declare Sub ReadLocation lib "Carbon" (location As ptr)
		    #else
		      #if TargetCarbon
		        Declare Sub ReadLocation lib "CarbonLib" (location As ptr)
		      #else
		        Declare Sub ReadLocation lib "InterfaceLib" (location As ptr) Inline68k("205F203C000C00E4A051")
		      #endif
		    #endif
		    dim info as memoryBlock
		    dim byteOffset as integer
		    info = NewMemoryBlock(12)
		    ReadLocation info
		    if info.LittleEndian then
		      byteOffset = 8
		    else
		      byteOffset = 11
		    end
		    offset = info.short(9) * 256 + info.byte(byteOffset)
		    offset = offset \ 60
		  #elseif TargetWin32 then
		    Declare Function GetTimeZoneInformation Lib "Kernel32" ( tzInfoPointer as Ptr ) as Integer
		    // returns one of
		    // TIME_ZONE_ID_UNKNOWN 0
		    //      -- Note: e.g. New Delhi (GMT+5:30) and Newfoundland (-3:30) return this value 0
		    // TIME_ZONE_ID_STANDARD 1
		    // TIME_ZONE_ID_DAYLIGHT 2
		    dim info as memoryBlock
		    dim result, bias, dayLightbias as integer
		    info = new MemoryBlock(172)
		    result = GetTimeZoneInformation(info)
		    bias = info.Long(0)
		    // note about bias: the original code I found in the NUG archives used Long(84) and switched to Long(0)
		    // only for result=1 and result=2, but my tests found that Long(0) is also the right value for result=0
		    if result = 2 then
		      daylightBias = info.long(168)
		    end if
		    offset = - (bias + dayLightbias)
		  #else
		    this target is not supported yet - see if there's a new version at <www.tempel.org/rb/>
		  #endif
		  
		  return offset
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
