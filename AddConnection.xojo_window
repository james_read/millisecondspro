#tag Window
Begin Window AddConnection
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   8
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   177
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   False
   Title           =   "Untitled"
   Visible         =   True
   Width           =   264
   Begin TabPanel tpConnectionMethod
      AutoDeactivate  =   True
      Bold            =   False
      Enabled         =   True
      Height          =   111
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Panels          =   ""
      Scope           =   "0"
      SmallTabs       =   False
      TabDefinition   =   "Serial\rNetwork\rToolKit"
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   14
      Underline       =   False
      Value           =   1
      Visible         =   True
      Width           =   224
      Begin PopupMenu ConnectMethod
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   "Select the desired connection method."
         Index           =   -2147483648
         InitialParent   =   "tpConnectionMethod"
         InitialValue    =   "Select a Method..."
         Italic          =   False
         Left            =   54
         ListIndex       =   0
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   False
         Scope           =   "0"
         TabIndex        =   0
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   54
         Underline       =   False
         Visible         =   True
         Width           =   157
      End
      Begin TextField efNetworkAddress
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   23
         HelpTag         =   "Enter the IP or Web address of the timing point you which to connect to."
         Index           =   -2147483648
         InitialParent   =   "tpConnectionMethod"
         Italic          =   False
         Left            =   42
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   "0"
         TabIndex        =   1
         TabPanelIndex   =   2
         TabStop         =   True
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   65
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   180
      End
      Begin PopupMenu pmConnectionSpeed
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   22
         HelpTag         =   "Select the speed to connect to the timing point."
         Index           =   -2147483648
         InitialParent   =   "tpConnectionMethod"
         InitialValue    =   "2400 baud\r9600 baud"
         Italic          =   False
         Left            =   54
         ListIndex       =   2
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   1
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   88
         Underline       =   False
         Visible         =   True
         Width           =   157
      End
      Begin TextField efToolKitPort
         AcceptTabs      =   False
         Alignment       =   0
         AutoDeactivate  =   True
         AutomaticallyCheckSpelling=   False
         BackColor       =   &cFFFFFF00
         Bold            =   False
         Border          =   True
         CueText         =   ""
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Format          =   ""
         Height          =   23
         HelpTag         =   "Enter the IP or Web address of the timing point you which to connect to."
         Index           =   -2147483648
         InitialParent   =   "tpConnectionMethod"
         Italic          =   False
         Left            =   128
         LimitText       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Mask            =   ""
         Password        =   False
         ReadOnly        =   False
         Scope           =   "0"
         TabIndex        =   0
         TabPanelIndex   =   3
         TabStop         =   True
         Text            =   "3097"
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   65
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   87
      End
      Begin Label StaticText1
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   26
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "tpConnectionMethod"
         Italic          =   False
         Left            =   41
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   "0"
         Selectable      =   False
         TabIndex        =   1
         TabPanelIndex   =   3
         TabStop         =   True
         Text            =   "ToolKit Port:"
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   63
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   92
      End
   End
   Begin PushButton pbConnect
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Connect"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   164
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   137
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton pbCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   72
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   137
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin ProgressWheel ConnectionProgress
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   16
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   141
      Visible         =   False
      Width           =   16
   End
End
#tag EndWindow

#tag WindowCode
#tag EndWindowCode

#tag Events tpConnectionMethod
	#tag Event
		Sub Change()
		  if me.Value=1 then
		    efNetworkAddress.SetFocus
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ConnectMethod
	#tag Event
		Sub Open()
		  dim i as integer
		  dim stream as TextInputStream
		  
		  for i=0 to (System.SerialPortCount-1)
		    me.AddRow "Serial: "+System.SerialPort(i).Name
		    me.RowTag(i)=i
		  next
		  
		  me.ListIndex=0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbConnect
	#tag Event
		Sub Action()
		  Dim i,ii,index,wc, AsciiCode as Integer
		  dim TickCount as integer
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  dim SerialPortName as String
		  dim DesiredSerialPortName as string
		  dim tcpIPConnectionEstablished as Boolean
		  Dim SerialPortNameString as string
		  
		  ConnectionProgress.Visible=true
		  
		  select case tpConnectionMethod.Value
		  case 0
		    if ReceiveTimes.sgTimeSources.Rows>0 then
		      for i=0 to ReceiveTimes.sgTimeSources.Rows
		        if ReceiveTimes.sgTimeSources.CellText(13,i)="Serial" then
		          index=index+1
		        end if
		      next
		    else
		      index=0
		    end if
		    
		    for i=0 to (System.SerialPortCount-1)
		      DesiredSerialPortName=ConnectMethod.Text
		      SerialPortName=System.SerialPort(i).name
		      if InStr(ConnectMethod.text,System.SerialPort(i).name) > 0 then
		        
		        if TargetWin32 then
		          SerialPortNameString=""
		          for ii=1 to len(System.SerialPort(i).name)
		            AsciiCode= asc(mid(System.SerialPort(i).name,ii,1))
		            if(AsciiCode>=48 and AsciiCode<=57) then
		              SerialPortNameString=SerialPortNameString+mid(System.SerialPort(i).name,ii,1)
		            end if
		          next
		          ReceiveTimes.SerialPortConnection(Index).Port=val(SerialPortNameString)-1
		        else
		          ReceiveTimes.SerialPortConnection(Index).Port=val(ConnectMethod.RowTag(i))
		        end if
		        
		        if pmConnectionSpeed.Text="9600 baud" then
		          ReceiveTimes.SerialPortConnection(Index).Baud=8
		        else
		          ReceiveTimes.SerialPortConnection(Index).Baud=4
		        end if
		        
		      end if
		    next
		    
		    If ReceiveTimes.SerialPortConnection(Index).Open then
		      ReceiveTimes.SerialPortConnection(Index).flush
		      ConnectionProgress.Visible=false
		      self.Close
		    Else
		      ConnectionProgress.Visible=false
		      d.icon=1   //display warning icon
		      d.ActionButton.Caption="OK"
		      d.Message="The Serial Port could not be opened"
		      d.Explanation="Error: "+str(ReceiveTimes.SerialPortConnection(Index).LastErrorCode)
		      b=d.ShowModal
		    End if
		    
		    
		  case 1
		    
		    if ReceiveTimes.sgTimeSources.Rows>0 then
		      for i=0 to ReceiveTimes.sgTimeSources.Rows
		        if ReceiveTimes.sgTimeSources.CellText(13,i)="TCP" then
		          index=index+1
		        end if
		      next
		    else
		      index=0
		    end if
		    
		    ReceiveTimes.TCPSocketConnection(index).Address=efNetworkAddress.text
		    ReceiveTimes.TCPSocketConnection(index).Port=5100 ' Try an AMB connection first
		    
		    TickCount=Ticks
		    ReceiveTimes.TCPSocketConnection(index).Connect
		    
		    tcpIPConnectionEstablished=false
		    Do
		      app.DoEvents
		      if ReceiveTimes.TCPSocketConnection(index).IsConnected then
		        TCPIPConnectionEstablished=true
		        App.ReaderType="AMB"
		      end if
		    Loop Until TCPIPConnectionEstablished or Ticks >= TickCount+680
		    if TCPIPConnectionEstablished then
		      ReceiveTimes.CountSinceCommand=2
		      ConnectionProgress.Visible=false
		      self.Close
		    else
		      ReceiveTimes.TCPSocketConnection(index).Port=10200'try iPico
		      
		      TickCount=Ticks
		      ReceiveTimes.TCPSocketConnection(index).Connect
		      
		      tcpIPConnectionEstablished=false
		      Do
		        app.DoEvents
		        if ReceiveTimes.TCPSocketConnection(index).IsConnected then
		          TCPIPConnectionEstablished=true
		          App.ReaderType="iPico"
		        end if
		      Loop Until TCPIPConnectionEstablished or Ticks >= TickCount+680
		      
		      if TCPIPConnectionEstablished then
		        ReceiveTimes.CountSinceCommand=2
		        ConnectionProgress.Visible=false
		        self.Close
		      else
		        ConnectionProgress.Visible=false
		        MsgBox "Unable to establish a network connection to "+efNetworkAddress.text
		      end if
		    end if
		    
		  case 2
		    
		    if ReceiveTimes.sgTimeSources.Rows>0 then
		      for i=0 to ReceiveTimes.sgTimeSources.Rows
		        if ReceiveTimes.sgTimeSources.CellText(13,i)="TCP" then
		          index=index+1
		        end if
		      next
		    else
		      index=0
		    end if
		    
		    TickCount=Ticks
		    ReceiveTimes.TCPSocketConnection(index).Port=val(efToolKitPort.Text)
		    ReceiveTimes.TCPSocketConnection(index).Listen
		    tcpIPConnectionEstablished=false
		    Do
		      app.DoEvents
		      if ReceiveTimes.TCPSocketConnection(index).IsConnected then
		        TCPIPConnectionEstablished=true
		        App.ReaderType="ToolKit"
		        ReceiveTimes.SendData(index,"TCP","@Ping@$")
		      end if
		    Loop Until TCPIPConnectionEstablished or Ticks >= TickCount+680
		    
		    if TCPIPConnectionEstablished then
		      ReceiveTimes.CountSinceCommand=2
		      ConnectionProgress.Visible=false
		      self.Close
		    else
		      ConnectionProgress.Visible=false
		      MsgBox "Unable to establish a ToolKit connection to "+efToolKitPort.text
		    end if
		    
		  end select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbCancel
	#tag Event
		Sub Action()
		  self.close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
