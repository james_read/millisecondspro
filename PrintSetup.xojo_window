#tag Window
Begin Window PrintSetup
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   1
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   599
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   1000
   MaximizeButton  =   True
   MaxWidth        =   696
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   468
   MinimizeButton  =   True
   MinWidth        =   696
   Placement       =   0
   Resizeable      =   True
   Title           =   "Print"
   Visible         =   True
   Width           =   696
   Begin TabPanel ReportsTabPanel
      AutoDeactivate  =   True
      Bold            =   False
      Enabled         =   True
      Height          =   522
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   22
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Panels          =   ""
      Scope           =   "0"
      SmallTabs       =   False
      TabDefinition   =   "Results\rOther Reports"
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   25
      Underline       =   False
      Value           =   0
      Visible         =   True
      Width           =   654
      Begin GroupBox FormatGroupBox
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Format"
         Enabled         =   True
         Height          =   250
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         Italic          =   False
         Left            =   353
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   1
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   83
         Underline       =   False
         Visible         =   True
         Width           =   305
         Begin PopupMenu puFormatType
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "FormatGroupBox"
            InitialValue    =   ""
            Italic          =   False
            Left            =   365
            ListIndex       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            TabIndex        =   2
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   119
            Underline       =   False
            Visible         =   True
            Width           =   278
         End
         Begin RadioButton rbFastestToSlowest
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Fastest to Slowest"
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "FormatGroupBox"
            Italic          =   False
            Left            =   377
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            TabIndex        =   3
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   270
            Underline       =   False
            Value           =   True
            Visible         =   True
            Width           =   166
         End
         Begin RadioButton rbSlowestToFastest
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Slowest to Fastest"
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "FormatGroupBox"
            Italic          =   False
            Left            =   377
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            TabIndex        =   4
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   294
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   166
         End
         Begin CheckBox cbRound
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Round to Nearest Second"
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "FormatGroupBox"
            Italic          =   False
            Left            =   377
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            State           =   0
            TabIndex        =   5
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   238
            Underline       =   False
            Value           =   False
            Visible         =   False
            Width           =   176
         End
         Begin CheckBox cbFinalResults
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Final Results"
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "FormatGroupBox"
            Italic          =   False
            Left            =   377
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            State           =   0
            TabIndex        =   6
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   214
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   103
         End
         Begin TextField MaxPlaces
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "FormatGroupBox"
            Italic          =   False
            Left            =   603
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   "0"
            TabIndex        =   7
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "All"
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   178
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   False
            Width           =   40
         End
         Begin Label MaxLabel
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   21
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "FormatGroupBox"
            Italic          =   False
            Left            =   528
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   "0"
            Selectable      =   False
            TabIndex        =   8
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Maximum:"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   178
            Transparent     =   False
            Underline       =   False
            Visible         =   False
            Width           =   73
         End
         Begin TextField MinPlaces
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "FormatGroupBox"
            Italic          =   False
            Left            =   603
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   "0"
            TabIndex        =   9
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "All"
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   150
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   40
         End
         Begin CheckBox cbPenaltyWorksheet
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Print Penalty Worksheet"
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "FormatGroupBox"
            Italic          =   False
            Left            =   377
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            State           =   1
            TabIndex        =   11
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   151
            Underline       =   False
            Value           =   True
            Visible         =   False
            Width           =   160
         End
         Begin Label MinLabel
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   21
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "FormatGroupBox"
            Italic          =   False
            Left            =   433
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   "0"
            Selectable      =   False
            TabIndex        =   10
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Minimum Scoring Members:"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   150
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   167
         End
      End
      Begin Label StaticText2
         AutoDeactivate  =   True
         Bold            =   True
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         Italic          =   False
         Left            =   41
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Multiline       =   False
         Scope           =   "0"
         Selectable      =   False
         TabIndex        =   4
         TabPanelIndex   =   1
         TabStop         =   True
         Text            =   "Print Results..."
         TextAlign       =   0
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   18.0
         TextUnit        =   0
         Top             =   60
         Transparent     =   False
         Underline       =   False
         Visible         =   True
         Width           =   154
      End
      Begin BevelButton BevelButton2
         AcceptFocus     =   False
         AutoDeactivate  =   True
         BackColor       =   &c00000000
         Bevel           =   0
         Bold            =   False
         ButtonType      =   0
         Caption         =   "Deselect All"
         CaptionAlign    =   3
         CaptionDelta    =   0
         CaptionPlacement=   1
         Enabled         =   True
         HasBackColor    =   False
         HasMenu         =   0
         Height          =   22
         HelpTag         =   ""
         Icon            =   0
         IconAlign       =   0
         IconDX          =   0
         IconDY          =   0
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         Italic          =   False
         Left            =   360
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         MenuValue       =   0
         Scope           =   "0"
         TabIndex        =   5
         TabPanelIndex   =   1
         TabStop         =   True
         TextColor       =   &c00000000
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   463
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
      Begin PopupMenu puReport
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         InitialValue    =   "Select a Report...\rRegistration List"
         Italic          =   False
         Left            =   65
         ListIndex       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   6
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   100
         Underline       =   False
         Visible         =   True
         Width           =   178
      End
      Begin GroupBox gbSortOrder
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Sort Order"
         Enabled         =   True
         Height          =   216
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         Italic          =   False
         Left            =   268
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   9
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   80
         Underline       =   False
         Visible         =   True
         Width           =   375
         Begin GroupBox GroupBox1
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   ""
            Enabled         =   True
            Height          =   55
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "gbSortOrder"
            Italic          =   False
            Left            =   278
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            TabIndex        =   0
            TabPanelIndex   =   2
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   190
            Underline       =   False
            Visible         =   True
            Width           =   357
            Begin CheckBox cbPageBreak
               AutoDeactivate  =   True
               Bold            =   False
               Caption         =   "Page Breaks on Representing"
               DataField       =   ""
               DataSource      =   ""
               Enabled         =   True
               Height          =   40
               HelpTag         =   ""
               Index           =   -2147483648
               InitialParent   =   "GroupBox1"
               Italic          =   False
               Left            =   484
               LockBottom      =   False
               LockedInPosition=   False
               LockLeft        =   True
               LockRight       =   False
               LockTop         =   True
               Scope           =   "0"
               State           =   0
               TabIndex        =   0
               TabPanelIndex   =   2
               TabStop         =   True
               TextFont        =   "System"
               TextSize        =   0.0
               TextUnit        =   0
               Top             =   198
               Underline       =   False
               Value           =   False
               Visible         =   True
               Width           =   129
            End
         End
      End
      Begin RadioButton rbNameSort
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Name"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         Italic          =   False
         Left            =   290
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   7
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   110
         Underline       =   False
         Value           =   True
         Visible         =   True
         Width           =   100
      End
      Begin RadioButton rbRacerNumberSort
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Number"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         Italic          =   False
         Left            =   290
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   8
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   137
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
      Begin RadioButton rbStartTime
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Start Time"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         Italic          =   False
         Left            =   290
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   10
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   249
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
      Begin CheckBox cbAutoPrint
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Auto Print Full Pages"
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         Italic          =   False
         Left            =   98
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         State           =   0
         TabIndex        =   11
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   345
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   171
      End
      Begin RadioButton rbDivisionSort
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Division"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         Italic          =   False
         Left            =   290
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   11
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   165
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   100
      End
      Begin RadioButton rbDivisionRepresenting
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Division, Representing"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         Italic          =   False
         Left            =   290
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   12
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   193
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   183
      End
      Begin RadioButton rbRepresentingDivision
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Representing, Division"
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         Italic          =   False
         Left            =   290
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   13
         TabPanelIndex   =   2
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   221
         Underline       =   False
         Value           =   False
         Visible         =   True
         Width           =   174
      End
      Begin PopupMenu puFormat
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         InitialValue    =   ""
         Italic          =   False
         Left            =   360
         ListIndex       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   12
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   377
         Underline       =   False
         Visible         =   True
         Width           =   213
      End
      Begin GroupBox IncludeGroupBox
         AutoDeactivate  =   True
         Bold            =   False
         Caption         =   "Include"
         Enabled         =   True
         Height          =   250
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         Italic          =   False
         Left            =   41
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   0
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   83
         Underline       =   False
         Visible         =   True
         Width           =   300
         Begin CheckBox cbNonPrinted
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Non-Printed Racers Only"
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   59
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            State           =   0
            TabIndex        =   2
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   264
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   171
         End
         Begin CheckBox cbDNF
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "DNF/DNS/DQ"
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   59
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            State           =   0
            TabIndex        =   3
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   108
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   117
         End
         Begin CheckBox cbAdjustments
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Adjustment Reasons"
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   59
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            State           =   1
            TabIndex        =   5
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   160
            Underline       =   False
            Value           =   True
            Visible         =   True
            Width           =   155
         End
         Begin TextField efLapsFrom
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   176
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   "0"
            TabIndex        =   6
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   184
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   32
         End
         Begin TextField efLapsTo
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   238
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   "0"
            TabIndex        =   7
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   ""
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   0.0
            TextUnit        =   0
            Top             =   184
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   32
         End
         Begin Label stLaps1
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   130
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   "0"
            Selectable      =   False
            TabIndex        =   8
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "From"
            TextAlign       =   2
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   186
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   34
         End
         Begin Label stLaps2
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   210
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   "0"
            Selectable      =   False
            TabIndex        =   9
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "to"
            TextAlign       =   1
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   186
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   24
         End
         Begin CheckBox cbIntervals
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Intervals"
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   59
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            State           =   0
            TabIndex        =   11
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   186
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   76
         End
         Begin CheckBox cbNetTime
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Net Time"
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   59
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            State           =   0
            TabIndex        =   12
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   212
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   103
         End
         Begin CheckBox cbIncludeDivPlace
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   "Include Division Place With Name"
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   59
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            State           =   0
            TabIndex        =   13
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   238
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   232
         End
         Begin CheckBox cbAge
            AutoDeactivate  =   True
            Bold            =   False
            Caption         =   " Particpant Age"
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   20
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   59
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Scope           =   "0"
            State           =   0
            TabIndex        =   14
            TabPanelIndex   =   1
            TabStop         =   True
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   134
            Underline       =   False
            Value           =   False
            Visible         =   True
            Width           =   135
         End
         Begin Label PlacesLabel
            AutoDeactivate  =   True
            Bold            =   False
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Height          =   21
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   59
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Multiline       =   False
            Scope           =   "0"
            Selectable      =   False
            TabIndex        =   16
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "Maximum Number of Racers:"
            TextAlign       =   0
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   295
            Transparent     =   False
            Underline       =   False
            Visible         =   True
            Width           =   167
         End
         Begin TextField Places
            AcceptTabs      =   False
            Alignment       =   0
            AutoDeactivate  =   True
            AutomaticallyCheckSpelling=   False
            BackColor       =   &cFFFFFF00
            Bold            =   False
            Border          =   True
            CueText         =   ""
            DataField       =   ""
            DataSource      =   ""
            Enabled         =   True
            Format          =   ""
            Height          =   22
            HelpTag         =   ""
            Index           =   -2147483648
            InitialParent   =   "IncludeGroupBox"
            Italic          =   False
            Left            =   229
            LimitText       =   0
            LockBottom      =   False
            LockedInPosition=   False
            LockLeft        =   True
            LockRight       =   False
            LockTop         =   True
            Mask            =   ""
            Password        =   False
            ReadOnly        =   False
            Scope           =   "0"
            TabIndex        =   15
            TabPanelIndex   =   1
            TabStop         =   True
            Text            =   "All"
            TextColor       =   &c00000000
            TextFont        =   "System"
            TextSize        =   12.0
            TextUnit        =   0
            Top             =   295
            Underline       =   False
            UseFocusRing    =   True
            Visible         =   True
            Width           =   40
         End
      End
      Begin ListBox DataList
         AutoDeactivate  =   True
         AutoHideScrollbars=   True
         Bold            =   False
         Border          =   True
         ColumnCount     =   4
         ColumnsResizable=   False
         ColumnWidths    =   "0,20,220,0"
         DataField       =   ""
         DataSource      =   ""
         DefaultRowHeight=   -1
         Enabled         =   True
         EnableDrag      =   False
         EnableDragReorder=   False
         GridLinesHorizontal=   0
         GridLinesVertical=   0
         HasHeading      =   False
         HeadingIndex    =   -1
         Height          =   146
         HelpTag         =   ""
         Hierarchical    =   False
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         InitialValue    =   ""
         Italic          =   False
         Left            =   79
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         RequiresSelection=   False
         Scope           =   "0"
         ScrollbarHorizontal=   False
         ScrollBarVertical=   True
         SelectionType   =   0
         TabIndex        =   13
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   377
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   250
         _ScrollOffset   =   0
         _ScrollWidth    =   -1
      End
      Begin PopupMenu puDistanceSelect
         AutoDeactivate  =   True
         Bold            =   False
         DataField       =   ""
         DataSource      =   ""
         Enabled         =   True
         Height          =   20
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ReportsTabPanel"
         InitialValue    =   ""
         Italic          =   False
         Left            =   360
         ListIndex       =   0
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         Scope           =   "0"
         TabIndex        =   14
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   419
         Underline       =   False
         Visible         =   False
         Width           =   213
      End
   End
   Begin PushButton pbOK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "OK"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   607
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   559
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PushButton pbCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   509
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   559
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PushButton pbHTML
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "Create HTML"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   374
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      Scope           =   "0"
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   559
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  DoubleQuoteMark=Chr(34)
		  
		  if wnd_List.ResultType.Text="Cross Country Running" then
		    cbAge.Value=true
		  else
		    cbAge.Value=False
		  end if
		  
		  if app.RacersPerStart>=1 and app.RacersPerStart<=4 then 'TT
		    puReport.AddRow "Start List - Time, No, Name, Rep, Div"
		    puReport.AddRow "Start List - No, Name, Rep, Div, Time"
		    cbRound.Visible=True
		  end if
		  
		  if (app.RacersPerStart>=1 and app.RacersPerStart<=4) or app.RacersPerStart>=9998  then 'cycling event
		    puReport.AddRow "Sign-In Sheets"
		  end if
		  
		  puReport.AddRow "Missing Transponders List"
		  
		  puReport.AddRow "Prize Drawing List"
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Sub ExecuteQuery(selection as integer)
		  dim i as integer
		  dim SelectStatement as String
		  dim recCount as integer
		  dim res as boolean
		  dim rsQueryResults as RecordSet
		  dim genders() as string=array("Male","Female","Mixed")
		  
		  DataList.DeleteAllRows
		  
		  if selection=0 or (Selection>4 and Selection < (5+NumDivisions)) then ' Race Distance
		    SelectStatement="SELECT RaceDistance_Name, rowid FROM racedistances WHERE RaceID =1"
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		    while not rsQueryResults.EOF
		      DataList.AddRow ""
		      DataList.Cell(DataList.lastindex,0)=rsQueryResults.Field("rowid").stringValue
		      DataList.CellCheck(DataList.lastindex,1)=True
		      DataList.Cell(DataList.lastindex,2)=rsQueryResults.Field("RaceDistance_Name").stringValue
		      rsQueryResults.moveNext
		    wend
		    rsQueryResults.Close
		    
		  elseif Selection = 1 or ((Selection>4+NumDivisions) and Selection < (6+(NumDivisions*2))) then 'Gender
		    SelectStatement="SELECT RaceDistance_Name, rowid FROM racedistances WHERE RaceID =1"
		    for i=0 to UBound(genders)
		      rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		      while not rsQueryResults.EOF
		        DataList.AddRow ""
		        DataList.Cell(DataList.lastindex,0)=rsQueryResults.Field("rowid").stringValue
		        DataList.CellCheck(DataList.lastindex,1)=True
		        DataList.Cell(DataList.lastindex,2)=genders(i)+" "+rsQueryResults.Field("RaceDistance_Name").stringValue
		        rsQueryResults.moveNext
		      wend
		    next
		    rsQueryResults.Close
		    
		     'Division and Award
		  elseif Selection=2 or Selection=3 or Selection > (6+(NumDivisions*2)) then
		    SelectStatement="SELECT Division_Name, Stage_Cutoff, rowid FROM divisions WHERE RaceID =1 ORDER BY List_Order ASC"
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		    while not rsQueryResults.EOF
		      DataList.AddRow ""
		      DataList.Cell(DataList.lastindex,0)=rsQueryResults.Field("rowid").stringValue
		      DataList.CellCheck(DataList.lastindex,1)=True
		      DataList.Cell(DataList.lastindex,2)=rsQueryResults.Field("Division_Name").stringValue
		      DataList.Cell(DataList.lastindex,3)=rsQueryResults.Field("Stage_Cutoff").stringValue
		      rsQueryResults.moveNext
		    wend
		    rsQueryResults.Close
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ExportDyeStatCCTeamStandings(SelectionID() as string, Selection() as boolean, SelectionText() as String, DisplayFormat as string, FinalResults as boolean, MinMembers as integer, MaxMembers as integer) As boolean
		  dim i,j,SelectionIndex,FirstTeamPlace,Place,PlaceIncrement,RIS,ScorerCount,SecondTeamPlace,SOA as Integer
		  dim TimeLength,Ties, ScorerCount_al(), TeamMemberCount, PixelCount, PreviousTeamPlace, CurrentTeamPlace as integer
		  dim ClassName, Delimiter, FileName, OutputData, PaceType, AdditionalSQL, SQLStatement, EligibleParticipantsSQL, RemainingSQL, OldTotalTime, OldTeamPoints, OldTeamName as String
		  dim TeamNames_as(), Name_as(), Age_as(), TeamPlaces_as(), TeamPlace_as() as string
		  dim TotalTime_as(), Place_as(), TeamTime_as(), ParticipantID_as() as string
		  dim Pace_as(), UniqueTeamNames(), SortArray() as String
		  dim TotalTeamPoints_as(), TeamPoints_as(), TotalTeamTimes_as(), Scorer_as() as String
		  dim rsTeams, rsTeamMembers, rsEligibleRacers, rsRemainingRacers, rsRacer as RecordSet
		  Dim file As FolderItem
		  Dim fileStream As TextOutputStream
		  dim AtLeastOneFullTeam as boolean
		  
		  AtLeastOneFullTeam = false
		  ClassName=""
		  PageCount=0
		  
		  if TargetMacOS then
		    Delimiter=chr(9)
		    FileName="DyeStat CC Team Export.txt"
		  else
		    Delimiter=","
		    FileName="DyeStat CC Team Export.csv"
		  end if
		  
		  file= GetSaveFolderItem("application/text",FileName)
		  if file<>nil then
		    
		    fileStream=file.CreateTextFile
		    
		    for SelectionIndex=0 to UBound(Selection)-1
		      
		      redim TeamNames_as(-1)
		      redim Name_as(-1)
		      redim ParticipantID_as(-1)
		      redim Age_as(-1)
		      redim TeamPlaces_as(-1)
		      redim TeamPlace_as(-1)
		      redim TotalTime_as(-1)
		      redim Place_as(-1)
		      redim TeamTime_as(-1)
		      redim Pace_as(-1)
		      redim UniqueTeamNames(-1)
		      redim SortArray(-1)
		      redim TotalTeamPoints_as(-1)
		      redim TeamPoints_as(-1)
		      redim TotalTeamTimes_as(-1)
		      redim ScorerCount_al(-1)
		      redim Scorer_as(-1)
		      
		      if Selection(SelectionIndex) then
		        
		        select case DisplayFormat
		        case "Overall By Distance"
		          AdditionalSQL=" AND RaceDistanceID = "+SelectionID(SelectionIndex)+" "
		          
		        case "Overall By Gender"
		          if (InStr(SelectionText(SelectionIndex),"Female")>0) then 'Gots to check for Female first because the male is part of Female and Male
		            AdditionalSQL=" AND RaceDistanceID = "+SelectionID(SelectionIndex)+" AND Gender='F' "
		            
		          elseif (InStr(SelectionText(SelectionIndex),"Male")>0) then
		            AdditionalSQL=" AND RaceDistanceID = "+SelectionID(SelectionIndex)+" AND Gender='M' "
		            
		          else
		            AdditionalSQL=" AND RaceDistanceID = "+SelectionID(SelectionIndex)+" AND Gender='X' "
		            
		          end if
		          
		        else
		          AdditionalSQL=" AND DivisionID = "+SelectionID(SelectionIndex)+" "
		          
		        end select
		        AdditionalSQL=AdditionalSQL+"AND DNF='N' AND DNS='N' AND DQ='N' "
		        
		        
		        'get the unique team names
		        SQLStatement="SELECT DISTINCT Representing FROM participants WHERE RaceID=1 "+AdditionalSQL
		        rsTeams=app.theDB.DBSQLSelect(SQLStatement)
		        
		        'find all those teams with teams having between minimum and maximum members to score
		        EligibleParticipantsSQL="SELECT rowid, Participant_Name, First_Name, Representing, Age, Total_Time, DQ, DNF, DNS, Pace FROM participants WHERE ("
		        For i=1 to rsTeams.RecordCount
		          SQLStatement="SELECT rowid FROM participants WHERE Representing='"+rsTeams.Field("Representing").StringValue+"'"+AdditionalSQL+" ORDER BY Total_Time LIMIT 0, "+Str(MaxMembers)
		          rsTeamMembers=app.theDB.DBSQLSelect(SQLStatement)
		          
		          If (rsTeamMembers.RecordCount>=MinMembers) then
		            AtLeastOneFullTeam=true
		            UniqueTeamNames.Append rsTeams.Field("Representing").StringValue
		            For j=1 to rsTeamMembers.RecordCount
		              EligibleParticipantsSQL=EligibleParticipantsSQL+" rowid="+rsTeamMembers.Field("rowid").StringValue+" or "
		              rsTeamMembers.MoveNext
		            Next
		          End if
		          rsTeams.MoveNext
		          rsTeamMembers.Close
		        Next
		        if AtLeastOneFullTeam then
		          EligibleParticipantsSQL=Left(EligibleParticipantsSQL,len(EligibleParticipantsSQL)-4)+") "+AdditionalSQL
		          EligibleParticipantsSQL=EligibleParticipantsSQL+" ORDER BY Total_Time ASC"
		          rsEligibleRacers=app.theDB.DBSQLSelect(EligibleParticipantsSQL)
		          
		          'place overall among eligible scorers
		          Ties=0
		          Place=0
		          OldTotalTime="00:00:00.000"
		          For i=1 to rsEligibleRacers.RecordCount
		            If OldTotalTime<>rsEligibleRacers.Field("Total_Time").StringValue then
		              PlaceIncrement=1+Ties
		              Ties=0
		            Else
		              PlaceIncrement=0
		              Ties=Ties+1
		            End if
		            
		            Place=Place+PlaceIncrement
		            TeamNames_as.Append rsEligibleRacers.Field("Representing").StringValue
		            ParticipantID_as.Append  rsEligibleRacers.Field("rowid").StringValue
		            Name_as.Append rsEligibleRacers.Field("First_Name").StringValue+Delimiter+rsEligibleRacers.Field("Participant_Name").StringValue
		            Age_as.Append rsEligibleRacers.Field("Age").StringValue
		            TeamPlace_as.Append ""
		            TeamPoints_as.Append ""
		            TeamTime_as.Append ""
		            Scorer_as.Append ""
		            
		            if rsEligibleRacers.Field("DQ").StringValue="Y" then
		              Place_as.Append "10000"
		              TotalTime_as.Append "DSQ"
		              Pace_as.Append ""
		              
		            elseif rsEligibleRacers.Field("DNS").StringValue="Y" then
		              Place_as.Append "10000"
		              TotalTime_as.Append "DNS"
		              Pace_as.Append ""
		              
		            elseif rsEligibleRacers.Field("DNF").StringValue="Y"then
		              Place_as.Append "10000"
		              TotalTime_as.Append "DNF"
		              Pace_as.Append ""
		              
		            Else
		              Pace_as.Append rsEligibleRacers.Field("Pace").StringValue
		              Place_as.Append format(Place,"00000")
		              TotalTime_as.Append rsEligibleRacers.Field("Total_Time").StringValue
		            end if
		            
		            OldTotalTime=rsEligibleRacers.Field("Total_Time").StringValue
		            rsEligibleRacers.MoveNext
		          next
		          
		          'derive points and total time
		          For i=0 to UBound(UniqueTeamNames)
		            ScorerCount_al.Append 0
		            TotalTeamPoints_as.Append "0"
		            TotalTeamTimes_as.Append "00:00:00.000"
		            SortArray.Append ""
		            For j=0 to UBound(TeamNames_as)
		              If ScorerCount_al(i)<MinMembers and UniqueTeamNames(i)=TeamNames_as(j) Then
		                ScorerCount_al(i)=ScorerCount_al(i)+1
		                Scorer_as(j)="*"
		                TotalTeamPoints_as(i)=format(val(TotalTeamPoints_as(i))+val(Place_as(j)),"00000")
		                TotalTeamTimes_as(i)=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(TotalTeamTimes_as(i))+app.ConvertTimeToSeconds (TotalTime_as(j)),false)
		                SortArray(i)=TotalTeamPoints_as(i)+TotalTeamTimes_as(i)+UniqueTeamNames(i)
		              End if
		            next
		          next
		          
		          'Check for Ties
		          
		          SortArray.SortWith(TotalTeamPoints_as,TotalTeamTimes_as,UniqueTeamNames)
		          'delete any teams that didn't have enough scorers
		          
		          for i=1 to ubound(SortArray)
		            if SortArray(0)="" then
		              SortArray.Remove(0)
		              TotalTeamPoints_as.Remove(0)
		              TotalTeamTimes_as.Remove(0)
		              UniqueTeamNames.Remove(0)
		            end if
		          next
		          
		          OldTeamPoints=""
		          Ties=0
		          Place=0
		          PreviousTeamPlace=0
		          CurrentTeamPlace=0
		          For i=0 to UBound(SortArray)
		            TeamPlaces_as.Append ""
		            If OldTeamPoints<>TotalTeamPoints_as(i) then
		              Place=Place+1
		              TeamPlaces_as(i)=format(Place,"00000")
		            Else
		              Place=Place+1
		              'Find the previous team's next runner
		              ScorerCount=0
		              For j=0 to UBound(TeamNames_as)
		                If (UniqueTeamNames(i-1)=TeamNames_as(j)) then
		                  ScorerCount=ScorerCount+1
		                  If (ScorerCount=(MinMembers+1)) then
		                    PreviousTeamPlace=val(Place_as(j))
		                  End if
		                End if
		              Next
		              
		              'Find the current team's next runner
		              ScorerCount=0
		              For j=0 to UBound(TeamNames_as)
		                If (UniqueTeamNames(i)=TeamNames_as(j)) then
		                  ScorerCount=ScorerCount+1
		                  If (ScorerCount=(MinMembers+1)) then
		                    CurrentTeamPlace=val(Place_as(j))
		                  End if
		                End if
		              Next
		              
		              If (i>1) then
		                if PreviousTeamPlace=0 and CurrentTeamPlace=0 then 'True tie
		                  TeamPlaces_as(i)=TeamPlaces_as(i-1)
		                  
		                elseif PreviousTeamPlace=0 and CurrentTeamPlace>0 then 'First is zero - no additional team members so current team wins
		                  TeamPlaces_as(i)=TeamPlaces_as(i-1)
		                  TeamPlaces_as(i-1)=format(Place,"00000")
		                  
		                elseif PreviousTeamPlace>0 and CurrentTeamPlace=0 then 'Second is zero - no additional team members so previous team wins
		                  TeamPlaces_as(i)=format(Place,"00000")
		                  
		                elseif PreviousTeamPlace<CurrentTeamPlace then 'Previous is lower previous wins
		                  TeamPlaces_as(i)=format(Place,"00000")
		                  
		                elseif PreviousTeamPlace>CurrentTeamPlace then 'Current is lower current wins
		                  TeamPlaces_as(i)=TeamPlaces_as(i-1)
		                  TeamPlaces_as(i-1)=format(Place,"00000")
		                  
		                End if
		              End if
		            End if
		            
		            OldTeamPoints=TotalTeamPoints_as(i)
		          Next
		          
		          'update total points for the team
		          For i=0 to UBound(UniqueTeamNames)
		            For j=0 to UBound(TeamNames_as)
		              If (UniqueTeamNames(i)=TeamNames_as(j)) then
		                TeamPlace_as(j)=TeamPlaces_as(i)
		                TeamPoints_as(j)=TotalTeamPoints_as(i)
		                TeamTime_as(j)=TotalTeamTimes_as(i)
		              End if
		            next
		          next
		          
		          'Now need to sort the arrays
		          Redim SortArray(Ubound(TeamNames_as))
		          For i=0 to UBound(TeamNames_as)
		            SortArray(i)=TeamPlace_as(i)+TeamNames_as(i)+Place_as(i)
		          next
		          SortArray.SortWith(TeamPlace_as,TeamNames_as,TeamPoints_as,TeamTime_as,Place_as,Name_as,ParticipantID_as,Age_as,Pace_as,TotalTime_as,Scorer_as)
		          
		          OutputData="Place"+Delimiter
		          OutputData=OutputData+"Score"+Delimiter
		          OutputData=OutputData+"Total Time"+Delimiter
		          OutputData=OutputData+"First Name"+Delimiter
		          OutputData=OutputData+"Last Name"+Delimiter
		          OutputData=OutputData+"Grade"+Delimiter
		          OutputData=OutputData+"Representing"
		          fileStream.WriteLine OutputData
		          
		          OldTeamName=""
		          For i=0 to UBound(TeamNames_as)
		            'If New Team, Print Team Line
		            if OldTeamName <> TeamNames_as(i) then
		              'Print any remaining team members
		              RemainingSQL="SELECT Participant_Name, First_Name, Age, Total_Time, Division_Place, Representing FROM participants WHERE Representing = '"+OldTeamName+"'"+AdditionalSQL
		              RemainingSQL=RemainingSQL+"ORDER BY Total_Time LIMIT "+str(MaxMembers)+", 9999"
		              rsRemainingRacers=app.theDB.DBSQLSelect(RemainingSQL)
		              if rsRemainingRacers.RecordCount>0 then
		                
		                for j=1 to rsRemainingRacers.RecordCount
		                  OutputData=rsRemainingRacers.Field("Division_Place").StringValue+Delimiter
		                  OutputData=OutputData+""+Delimiter
		                  OutputData=OutputData+rsRemainingRacers.Field("Total_Time").StringValue+Delimiter
		                  OutputData=OutputData+rsRemainingRacers.Field("First_Name").StringValue
		                  OutputData=OutputData+rsRemainingRacers.Field("Participant_Name").StringValue+Delimiter
		                  OutputData=OutputData+rsRemainingRacers.Field("Age").StringValue+Delimiter
		                  OutputData=OutputData+rsRemainingRacers.Field("Representing").StringValue
		                  fileStream.WriteLine OutputData
		                  
		                  rsRemainingRacers.MoveNext
		                next
		              end if
		              
		            End if
		            
		            'if not New Team Print Runner information 
		            SQLStatement="SELECT Division_Place FROM participants WHERE rowid = '"+ParticipantID_as(i)+"'"
		            rsRacer=app.theDB.DBSQLSelect(SQLStatement)
		            OutputData=rsRacer.Field("Division_Place").StringValue+Delimiter
		            OutputData=OutputData+app.StripLeadingZeros(Place_as(i))+Delimiter
		            OutputData=OutputData+app.StripLeadingZeros(TotalTime_as(i))+Delimiter
		            OutputData=OutputData+Name_as(i)+Delimiter
		            OutputData=OutputData+rsRemainingRacers.Field("Division_Place").StringValue+Delimiter
		            OutputData=OutputData+Age_as(i)+Delimiter
		            OutputData=OutputData+TeamNames_as(i)
		            fileStream.WriteLine OutputData
		            
		            OldTeamName=TeamNames_as(i)
		          next
		        end if
		      end if
		    next
		    fileStream.Close
		    return true
		  else
		    return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function HTMLPad(IncomingString as string, Justifcation as String, Length as Integer) As String
		  dim NewString as String
		  dim i, IncomingStringLength, Pad, PadBefore, PadAfter as Integer
		  
		  IncomingStringLength=len(IncomingString)
		  if IncomingStringLength<Length then
		    IncomingString=" "+IncomingString+" "
		    
		    Select Case Justifcation
		    case "R"
		      NewString=""
		      for i = 1 to Length-IncomingStringLength
		        NewString=NewString+"&nbsp"
		      next
		      NewString=NewString+IncomingString
		      
		      
		    case "L"
		      NewString=IncomingString
		      
		      for i = 1 to Length-IncomingStringLength
		        NewString=NewString+"&nbsp"
		      next
		      
		      if IncomingStringLength=0 then
		        NewString=NewString+"&nbsp"
		      end if
		      
		    case "C"
		      NewString=""
		      
		      Pad=Length-IncomingStringLength
		      
		      if (Pad mod 2) = 0 then
		        PadBefore=Floor(Pad/2)
		        PadAfter=PadBefore-1
		      else
		        PadBefore=Pad/2
		        PadAfter=PadBefore
		      end if
		      
		      for i = 1 to PadBefore
		        NewString=NewString+"&nbsp"
		      next
		      
		      NewString=NewString+IncomingString
		      
		      for i = 1 to PadAfter
		        NewString=NewString+"&nbsp"
		      next
		      
		      
		    end Select
		    
		  else
		    NewString=" "+left(IncomingString,Length)+"&nbsp"
		    
		  end if
		  
		  Return NewString
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function HTMLResults() As boolean
		  dim i, Old_i, PageLength, ParticipantCount, PixelCount, PlacesToPrint as integer
		  dim NetTime, Back, Pace, ParticipantName, ResultsType, SelectStatement, TotalTime as String
		  dim rsQueryResults, rsParticipant as RecordSet
		  
		  Dim dlg as New SaveAsDialog
		  Dim f as FolderItem
		  Dim OutputStream as TextOutputStream
		  
		  Old_i=-1
		  
		  dlg.InitialDirectory= Volume(0).Child("Documents")
		  dlg.promptText="Save Results HTML Files"
		  dlg.SuggestedFileName=puFormat.Text+".htm"
		  dlg.Title="Save Results HTML Files"
		  f=dlg.ShowModal()
		  
		  if f<>nil then
		    OutputStream=f.CreateTextFile
		    if Places.text="All" then
		      PlacesToPrint=999999
		    else
		      PlacesToPrint=val(Places.text)
		    end if
		    
		    LineCount=0
		    
		    if not(cbFinalResults.Value) then
		      HTMLResults_FirstPageHeader(OutputStream,"Preliminary Results")
		    else
		      HTMLResults_FirstPageHeader(OutputStream,"Final Results")
		    end if
		    
		    for i=0 to DataList.ListCount-1
		      if DataList.CellCheck(i,1) then
		        
		        if Left(app.ResultsDisplayType,6)="Normal" then
		          
		          if not(cbIntervals.Value) then
		            SelectStatement="SELECT A.Racer_Number, A.Overall_Place, A.Gender_Place, A.Division_Place, "
		            SelectStatement=SelectStatement+"A.Participant_Name, A.First_Name, B.Division_Name, A.DivisionID, A.Representing, "
		            SelectStatement=SelectStatement+"A.Net_Time, A.Total_Time, A.Overall_Back, A.Gender_Back, "
		            SelectStatement=SelectStatement+"A.Division_Back, A.Pace, A.DNF, A.DNS, A.DQ "
		            SelectStatement=SelectStatement+"FROM participants AS A INNER JOIN divisions AS B ON A.DivisionID = B.rowid "
		            SelectStatement=SelectStatement+"WHERE "
		          else
		            
		            SelectStatement="SELECT A.Racer_Number, A.Overall_Place, A.Gender_Place, A.Division_Place, "
		            SelectStatement=SelectStatement+"A.Participant_Name, A.First_Name, B.Division_Name, A.DivisionID, A.Representing, "
		            SelectStatement=SelectStatement+"A.Net_Time, A.Total_Time, A.Overall_Back, A.Gender_Back, "
		            SelectStatement=SelectStatement+"A.Division_Back, A.Pace, A.DNF, A.DNS, A.DQ, C.Interval_Time AS IntOneTime, "
		            SelectStatement=SelectStatement+"D.Interval_Time AS IntTwoTime, E.Interval_Time AS IntThreeTime "
		            SelectStatement=SelectStatement+"FROM participants AS A INNER JOIN divisions AS B ON A.DivisionID = B.rowid "
		            SelectStatement=SelectStatement+"LEFT JOIN times AS C ON A.rowid = C.ParticipantID AND C.Interval_Number = '1'  AND C.Use_This_Passing='Y' "
		            SelectStatement=SelectStatement+"LEFT JOIN times AS D ON A.rowid = D.ParticipantID AND D.Interval_Number = '2'  AND D.Use_This_Passing='Y' "
		            SelectStatement=SelectStatement+"LEFT JOIN times AS E ON A.rowid = E.ParticipantID AND E.Interval_Number = '3'  AND E.Use_This_Passing='Y' "
		            SelectStatement=SelectStatement+"WHERE "
		          end if
		          
		        elseif app.ResultsDisplayType="Triathlon" then
		          SelectStatement="SELECT A.rowid AS ParticipantID, A.Racer_Number, A.Overall_Place, A.Gender_Place, A.Division_Place, "
		          SelectStatement=SelectStatement+"A.Participant_Name, A.First_Name, B.Division_Name, A.Representing, "
		          SelectStatement=SelectStatement+"A.Net_Time, A.Total_Time, A.Overall_Back, A.Gender_Back, B.rowid AS DivisionID,  "
		          SelectStatement=SelectStatement+"A.Division_Back, A.Pace, A.DNF, A.DNS, A.DQ "
		          SelectStatement=SelectStatement+"FROM participants AS A INNER JOIN divisions AS B ON A.DivisionID = B.rowid "
		          SelectStatement=SelectStatement+"WHERE "
		          
		        end if
		        
		        if not(cbDNF.Value) then
		          SelectStatement=SelectStatement+"A.DNF = 'N' AND "
		          SelectStatement=SelectStatement+"A.Overall_Place >0 AND "
		        end if
		        
		        select case puFormat.Text
		        case "Overall By Distance"
		          SelectStatement=SelectStatement+"A.RaceDistanceID = "+DataList.Cell(i,0)
		          
		        case "Overall By Gender"
		          if (InStr(DataList.Cell(i,2),"Female")>0) then 'Gots to check for Female first because the male is part of Female and Male
		            SelectStatement=SelectStatement+"A.RaceDistanceID = "+DataList.Cell(i,0)+" AND A.Gender='F' "
		            
		          elseif (InStr(DataList.Cell(i,2),"Male")>0) then
		            SelectStatement=SelectStatement+"A.RaceDistanceID = "+DataList.Cell(i,0)+" AND A.Gender='M' "
		            
		          else
		            SelectStatement=SelectStatement+"A.RaceDistanceID = "+DataList.Cell(i,0)+" AND A.Gender='X' "
		            
		          end if
		          
		        else
		          SelectStatement=SelectStatement+"A.DivisionID = "+DataList.Cell(i,0)
		          
		        end select
		        
		        SelectStatement=SelectStatement+" ORDER BY A.DQ, A.DNF ASC, A.Overall_Place ASC, A.Participant_Name ASC, A.First_Name ASC"
		        rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		        
		        if not (rsQueryResults.EOF) then
		          ParticipantCount=0
		          
		          while (not (rsQueryResults.EOF) and (ParticipantCount<PlacesToPrint))
		            ParticipantCount=ParticipantCount+1
		            'print header
		            
		            if Old_i <> i then
		              if not(cbIntervals.Value) then
		                if Old_i>=0 then
		                  HTMLResults_Footer(OutputStream)
		                end if
		                HTMLResults_Header(OutputStream,puFormat.Text+": "+DataList.Cell(i,2), rsQueryResults.Field("DivisionID").IntegerValue)
		              else
		                'HTMLIntResults_Header(OutputStream,puFormat.Text+": "+DataList.Cell(i,2))
		              end if
		              Old_i=i
		            end if
		            
		            'print detail lines
		            if rsQueryResults.Field("First_Name").stringValue <> "" then
		              ParticipantName=rsQueryResults.Field("Participant_Name").stringValue+", "+rsQueryResults.Field("First_Name").stringValue
		            else
		              ParticipantName=rsQueryResults.Field("Participant_Name").stringValue
		            end if
		            
		            If rsQueryResults.Field("DNF").stringValue = "N" and rsQueryResults.Field("DNS").stringValue = "N" and rsQueryResults.Field("DQ").stringValue = "N" then
		              NetTime=app.StripLeadingZeros(rsQueryResults.Field("Net_Time").stringValue)
		              TotalTime=app.StripLeadingZeros(rsQueryResults.Field("Total_Time").stringValue)
		              select case puFormat.Text
		              case "Overall By Distance"
		                Back=app.StripLeadingZeros(rsQueryResults.Field("Overall_Back").stringValue)
		              case "Overall By Gender"
		                Back=app.StripLeadingZeros(rsQueryResults.Field("Gender_Back").stringValue)
		              else
		                Back=app.StripLeadingZeros(rsQueryResults.Field("Division_Back").stringValue)
		              end select
		              Pace=rsQueryResults.Field("Pace").stringValue
		              
		            elseif rsQueryResults.Field("DQ").stringValue = "Y" then
		              NetTime=""
		              TotalTime="DQ"
		              Back=""
		              Pace=""
		              
		            elseif rsQueryResults.Field("DNS").stringValue = "Y" then
		              NetTime=""
		              TotalTime="DNS"
		              Back=""
		              Pace=""
		              
		            else
		              NetTime=""
		              TotalTime="DNF"
		              Back=""
		              Pace=""
		            end if
		            
		            if not(cbIntervals.Value) and app.ResultsDisplayType<>"Triathlon" then
		              HTMLResults_Detail(OutputStream, rsQueryResults.Field("Overall_Place").stringValue, rsQueryResults.Field("Gender_Place").stringValue, _
		              rsQueryResults.Field("Division_Place").stringValue, rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, _
		              rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("Division_Name").stringValue, _
		              NetTime, TotalTime, Back, Pace)
		              
		            elseif app.ResultsDisplayType="Triathlon" then
		              SelectStatement="SELECT A.Racer_Number, "
		              SelectStatement=SelectStatement+"C.Interval_Time AS IntOneTime, "
		              SelectStatement=SelectStatement+"D.Interval_Time AS IntTwoTime, E.Interval_Time AS IntThreeTime, "
		              SelectStatement=SelectStatement+"F.Interval_Time AS IntFourTime, G.Interval_Time AS IntFiveTime "
		              SelectStatement=SelectStatement+"FROM participants AS A "
		              SelectStatement=SelectStatement+"LEFT JOIN times AS C ON A.rowid = C.ParticipantID AND C.Interval_Number = 1  AND C.Use_This_Passing='Y' "
		              SelectStatement=SelectStatement+"LEFT JOIN times AS D ON A.rowid = D.ParticipantID AND D.Interval_Number = 2  AND D.Use_This_Passing='Y' "
		              SelectStatement=SelectStatement+"LEFT JOIN times AS E ON A.rowid = E.ParticipantID AND E.Interval_Number = 3  AND E.Use_This_Passing='Y' "
		              SelectStatement=SelectStatement+"LEFT JOIN times AS F ON A.rowid = F.ParticipantID AND F.Interval_Number = 4  AND F.Use_This_Passing='Y' "
		              SelectStatement=SelectStatement+"LEFT JOIN times AS G ON A.rowid = G.ParticipantID AND G.Interval_Number = 5  AND G.Use_This_Passing='Y' "
		              SelectStatement=SelectStatement+"WHERE A.rowid="+rsQueryResults.Field("ParticipantID").StringValue
		              rsParticipant=app.theDB.DBSQLSelect(SelectStatement)
		              
		              HTMLResults_TriDetail(OutputStream, rsQueryResults.Field("Overall_Place").stringValue, rsQueryResults.Field("Gender_Place").stringValue, _
		              rsQueryResults.Field("Division_Place").stringValue, rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, _
		              rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("Division_Name").stringValue, TotalTime, Back, _
		              app.StripLeadingZeros(rsParticipant.Field("IntOneTime")), rsParticipant.Field("IntOneTime"), _
		              app.StripLeadingZeros(rsParticipant.Field("IntTwoTime")), _
		              app.StripLeadingZeros(rsParticipant.Field("IntThreeTime")), rsParticipant.Field("IntThreeTime"), _
		              app.StripLeadingZeros(rsParticipant.Field("IntFourTime")), _
		              app.StripLeadingZeros(rsParticipant.Field("IntFiveTime")), rsParticipant.Field("IntFiveTime"))
		              
		              
		              'if Left(TotalTime,1)<>"D" then
		              'HTMLIntResults_Detail(OutputStream, rsQueryResults.Field("Overall_Place").stringValue, rsQueryResults.Field("Gender_Place").stringValue, _
		              'rsQueryResults.Field("Division_Place").stringValue, rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, _
		              'rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("Division_Name").stringValue, _
		              'NetTime, TotalTime, Back, Pace, rsQueryResults.Field("IntOneTime"), rsQueryResults.Field("IntTwoTime"), rsQueryResults.Field("IntThreeTime"))
		              'else
		              'HTMLIntResults_Detail(ResultsPage, PixelCount+LineCount*10, rsQueryResults.Field("Overall_Place").stringValue, rsQueryResults.Field("Gender_Place").stringValue, _
		              'rsQueryResults.Field("Division_Place").stringValue, rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, _
		              'rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("Division_Name").stringValue, _
		              'NetTime, TotalTime, Back, Pace, "", "", "")
		              'end if
		            end if
		            rsQueryResults.moveNext
		            
		            LineCount=LineCount+1
		          wend
		          
		          rsQueryResults.Close
		          
		          
		        end if 'if not (rsQueryResults.EOF) then
		      end if 'if DataList.CellCheck(i,1) then
		    next
		    
		    'print footer
		    if LineCount>0 then
		      HTMLResults_Footer(OutputStream)
		      OutputStream.WriteLine "<FONT SIZE="+DoubleQuoteMark+"-3"+DoubleQuoteMark+">Results prepared by "+app.RegUserCompany+"."
		      if app.RegUserCompany="Milliseconds Sports Timing" then
		        OutputStream.WriteLine" Information and searchable results at <a href="+DoubleQuoteMark+"www.milliseconds.com"+DoubleQuoteMark+">www.milliseconds.com</a>"
		      end if
		      OutputStream.WriteLine "</FONT>"
		      OutputStream.WriteLine "</BODY>"
		      OutputStream.WriteLine "</HTML>"
		    end if  ' if LineCount>0 then
		    OutputStream.Close
		    return true
		  else
		    return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub HTMLResults_Detail(ByRef OutputStream as TextOutputStream, RacerOverallPlace as string, RacerGenderPlace as string, RacerDivisionPlace as string, RacerNumber as string, RacerName as String, RacerRepresenting as string, RacerDivision as string, RacerNetTime as string, RacerTotalTime as string, RacerBackTime as string, RacerPace as string)
		  'if LineCount mod 2 = 1 then
		  'ResultsPage.ForeColor=&cDDDDDD
		  'ResultsPage.FillRect 10, PageLine-7, ResultsPage.Width-15, 9
		  'ResultsPage.ForeColor=&c000000
		  'end if
		  
		  if  ((app.Registered) or (not(app.Registered) and LineCount mod 3 = 0)) then
		    OutputStream.WriteLine "<TR>"
		    
		    Select  Case puFormat.Text
		    case "Overall By Distance"
		      OutputStream.WriteLine "<TD>"+RacerOverallPlace+"</TD>"
		      
		    case "Overall By Gender"
		      OutputStream.WriteLine "<TD>"+RacerGenderPlace+"</TD>"
		      
		    else
		      OutputStream.WriteLine "<TD>"+RacerDivisionPlace+"</TD>"
		    end select
		    
		    OutputStream.WriteLine "<TD>"+RacerNumber+"</TD>"
		    OutputStream.WriteLine "<TD>"+Titlecase(RacerName)+"</TD>"
		    OutputStream.WriteLine "<TD>"+Titlecase(RacerRepresenting)+"</TD>"
		    
		    if puFormat.Text= "Overall By Distance" OR puFormat.Text="Overall By Gender" then
		      OutputStream.WriteLine "<TD>"+RacerDivision+"</TD>"
		    end if
		    OutputStream.WriteLine "<TD>"+RacerTotalTime+"</TD>"
		    if  ((instr(wnd_list.puStartType.Text,"Criterium")>0 or instr(wnd_List.puStartType.Text,"Circuit")>0) or ( app.RacersPerStart>=1 and app.RacersPerStart<=4)) Then 'for cycling events, the total time and net time are the same
		      OutputStream.WriteLine "<TD>"+RacerTotalTime+"</TD>"
		    else
		      OutputStream.WriteLine "<TD>"+RacerNetTime+"</TD>"
		    end if
		    OutputStream.WriteLine "<TD>"+RacerBackTime+"</TD>"
		    OutputStream.WriteLine "<TD>"+RacerPace+"</TD>"
		    OutputStream.WriteLine "</TR>"
		    
		  else
		    OutputStream.WriteLine "<TR>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    if puFormat.Text= "Overall By Distance" OR puFormat.Text="Overall By Gender" then
		      OutputStream.WriteLine "<TD></TD>"
		    end if
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "</TR>"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub HTMLResults_FirstPageHeader(ByRef OutputStream as TextOutputStream, ResultsType as string)
		  dim CurrentDate as new date
		  dim DateTime as string
		  
		  DateTime=CurrentDate.LongDate+" "+CurrentDate.LongTime
		  
		  OutputStream.WriteLine "<!DOCTYPE HTML PUBLIC -"+DoubleQuoteMark+"//W3C//DTD HTML 4.01 Transitional//EN"+DoubleQuoteMark+">"
		  OutputStream.WriteLine "<HTML>"
		  OutputStream.WriteLine "<HEAD><TITLE>"+wnd_List.RaceName.Text+" Results</TITLE></HEAD>"
		  OutputStream.WriteLine "<BODY>"
		  OutputStream.WriteLine "<font face="+DoubleQuoteMark+"Arial, Helvetica, sans-serif"+DoubleQuoteMark+">"
		  
		  'Populate Race Name
		  OutputStream.WriteLine "<H1 ALIGN="+DoubleQuoteMark+"CENTER"+DoubleQuoteMark+">"+wnd_List.RaceName.Text+"</H1>"
		  OutputStream.WriteLine "<P ALIGN="+DoubleQuoteMark+"CENTER"+DoubleQuoteMark+"><font size="+DoubleQuoteMark+"-1"+DoubleQuoteMark+">Race Date: "+wnd_List.RaceDateInput.Text+"</FONT><BR>"
		  
		  'Populate Results Type
		  OutputStream.WriteLine "<B>"+ResultsType+"</B><BR>"
		  
		  if InStr(0,ResultsType,"Preliminary")>0 then
		    'Populate Preliminary Message
		    OutputStream.WriteLine "<font size="+DoubleQuoteMark+"-1"+DoubleQuoteMark+">Subject to Change</FONT><BR>"
		  end if
		  
		  'Populate Date and Time
		  OutputStream.WriteLine "<FONT SIZE="+DoubleQuoteMark+"-3"+DoubleQuoteMark+">Results Posted: "+DateTime+"</FONT><BR>"
		  
		  if app.RegUserCompany="Milliseconds Sports Timing" then
		    OutputStream.WriteLine "<P ALIGN="+DoubleQuoteMark+"CENTER"+DoubleQuoteMark+"><FONT COLOR="+DoubleQuoteMark+"#FF0000"+DoubleQuoteMark+" SIZE="+DoubleQuoteMark+"-3"+DoubleQuoteMark+">If you still have a timing transponder, please mail it in a padded envelope to: Milliseconds Sports Timing, 463 W 3600 S, Salt Lake City, UT, 84115 </FONT></P></P>"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub HTMLResults_Footer(ByRef OutputStream as TextOutputStream)
		  OutputStream.WriteLine "</TABLE>"
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub HTMLResults_Header(ByRef OutputStream as TextOutputStream, ResultsName as string, DivisionID as integer)
		  dim SelectStatement as string
		  dim rsIntervals as RecordSet
		  dim i as integer
		  
		  if app.ResultsDisplayType="Triathlon" then
		    SelectStatement=SelectStatement+"SELECT Interval_Name, Actual_Distance, Number, Pace_Type FROM intervals WHERE DivisionID="+str(DivisionID)+" ORDER BY Number ASC"
		    rsIntervals=app.theDB.DBSQLSelect(SelectStatement)
		    Redim IntervalName(rsIntervals.RecordCount)
		    Redim IntActualDistance(rsIntervals.RecordCount)
		    Redim IntNumber(rsIntervals.RecordCount)
		    Redim IntPaceType(rsIntervals.RecordCount)
		    for i = 1 to rsIntervals.RecordCount
		      IntervalName(i-1)=rsIntervals.Field("Interval_Name").StringValue
		      IntActualDistance(i-1)=rsIntervals.Field("Actual_Distance").DoubleValue
		      IntNumber(i-1)=rsIntervals.Field("Number").StringValue
		      IntPaceType(i-1)=rsIntervals.Field("Pace_Type").StringValue
		      rsIntervals.MoveNext
		    next
		  end if
		  
		  OutputStream.WriteLine "<H3>"+ResultsName+"</H3>"
		  
		  OutputStream.WriteLine "<FONT FACE="+DoubleQuoteMark+"Arial, Helvetica, sans-serif"+DoubleQuoteMark+">"
		  OutputStream.WriteLine "<TABLE BORDER="+DoubleQuoteMark+"0"+DoubleQuoteMark+" CELLSPACING="+DoubleQuoteMark+"10"+DoubleQuoteMark+">"
		  OutputStream.WriteLine "<TR>"
		  OutputStream.WriteLine "<TD><B>Place</B></TD>"
		  OutputStream.WriteLine "<TD><B>Number</B></TD>"
		  OutputStream.WriteLine "<TD><B>Name</B></TD>"
		  OutputStream.WriteLine "<TD><B>Representing</B></TD>"
		  if puFormat.Text= "Overall By Distance" OR puFormat.Text="Overall By Gender" then
		    OutputStream.WriteLine "<TD><B>Division</B></TD>"
		  end if
		  If app.ResultsDisplayType="Triathlon" then
		    OutputStream.WriteLine "<TD><B>"+ IntervalName(0)+"</B></TD>"
		    OutputStream.WriteLine "<TD><B>"+IntPaceType(0)+"</B></TD>"
		    OutputStream.WriteLine "<TD><B>"+IntervalName(1)+"</B></TD>"
		    OutputStream.WriteLine "<TD><B>"+IntervalName(2)+"</B></TD>"
		    OutputStream.WriteLine "<TD><B>"+IntPaceType(2)+"</B></TD>"
		    OutputStream.WriteLine "<TD><B>"+IntervalName(3)+"</B></TD>"
		    OutputStream.WriteLine "<TD><B>"+IntervalName(4)+"</B></TD>"
		    OutputStream.WriteLine "<TD><B>"+IntPaceType(4)+"</B></TD>"
		  end if
		  OutputStream.WriteLine "<TD><B>Total Time</B></TD>"
		  if app.ResultsDisplayType<>"Triathlon" then
		    OutputStream.WriteLine "<TD><B>Net Time</B></TD>"
		  end if
		  
		  OutputStream.WriteLine "<TD><B>Back</B></TD>"
		  if app.ResultsDisplayType<>"Triathlon" then
		    OutputStream.WriteLine "<TD><B>Pace</B></TD>"
		  end if
		  OutputStream.WriteLine "</TR>"
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub HTMLResults_TriDetail(ByRef OutputStream as TextOutputStream, RacerOverallPlace as string, RacerGenderPlace as string, RacerDivisionPlace as string, RacerNumber as string, RacerName as String, RacerRepresenting as string, RacerDivision as string, RacerTotalTime as string, RacerBackTime as string, IntervalOneTime as string, CompleteIntervalOneTime as string, IntervalTwoTime as string, IntervalThreeTime as string, CompleteIntervalThreeTime as string, IntervalFourTime as string, IntervalFiveTime as string, CompleteIntervalFiveTime as string)
		  dim Pace as string
		  
		  'if LineCount mod 2 = 1 then
		  'ResultsPage.ForeColor=&cDDDDDD
		  'ResultsPage.FillRect 10, PageLine-7, ResultsPage.Width-15, 9
		  'ResultsPage.ForeColor=&c000000
		  'end if
		  
		  if  ((app.Registered) or (not(app.Registered) and LineCount mod 3 = 0)) then
		    OutputStream.WriteLine "<TR>"
		    
		    Select  Case puFormat.Text
		    case "Overall By Distance"
		      OutputStream.WriteLine "<TD>"+RacerOverallPlace+"</TD>"
		      
		    case "Overall By Gender"
		      OutputStream.WriteLine "<TD>"+RacerGenderPlace+"</TD>"
		      
		    else
		      OutputStream.WriteLine "<TD>"+RacerDivisionPlace+"</TD>"
		    end select
		    
		    OutputStream.WriteLine "<TD>"+RacerNumber+"</TD>"
		    OutputStream.WriteLine "<TD>"+RacerName+"</TD>"
		    OutputStream.WriteLine "<TD>"+RacerRepresenting+"</TD>"
		    
		    if puFormat.Text= "Overall By Distance" OR puFormat.Text="Overall By Gender" then
		      OutputStream.WriteLine "<TD>"+RacerDivision+"</TD>"
		    end if
		    
		    
		    OutputStream.WriteLine "<TD>"+IntervalOneTime+"</TD>"
		    Pace=app.CalculateIntPace(IntPaceType(0),IntActualDistance(0),CompleteIntervalOneTime)
		    OutputStream.WriteLine "<TD>"+Pace+"</TD>"
		    OutputStream.WriteLine "<TD>"+IntervalTwoTime+"</TD>"
		    OutputStream.WriteLine "<TD>"+IntervalThreeTime+"</TD>"
		    Pace=app.CalculateIntPace(IntPaceType(2),IntActualDistance(2),CompleteIntervalThreeTime)
		    OutputStream.WriteLine "<TD>"+Pace+"</TD>"
		    OutputStream.WriteLine "<TD>"+IntervalFourTime+"</TD>"
		    OutputStream.WriteLine "<TD>"+IntervalFiveTime+"</TD>"
		    Pace=app.CalculateIntPace(IntPaceType(4),IntActualDistance(4),CompleteIntervalFiveTime)
		    OutputStream.WriteLine "<TD>"+Pace+"</TD>"
		    
		    OutputStream.WriteLine "<TD>"+RacerTotalTime+"</TD>"
		    
		    OutputStream.WriteLine "<TD>"+RacerBackTime+"</TD>"
		    OutputStream.WriteLine "</TR>"
		    
		  else
		    OutputStream.WriteLine "<TR>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    if puFormat.Text= "Overall By Distance" OR puFormat.Text="Overall By Gender" then
		      OutputStream.WriteLine "<TD></TD>"
		    end if
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "<TD></TD>"
		    OutputStream.WriteLine "</TR>"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function PrintCCRunResults(SelectionID() as string, Selection() as boolean, SelectionText() as String, DisplayFormat as string, FinalResults as boolean, MinMembers as integer, MaxMembers as integer) As boolean
		  dim i,j,SelectionIndex,FirstTeamPlace,Place,PlaceIncrement,RIS,ScorerCount,SecondTeamPlace,SOA as Integer
		  dim TimeLength,Ties, ScorerCount_al(), TeamMemberCount, PixelCount, PreviousTeamPlace, CurrentTeamPlace as integer
		  dim ClassName, PaceType, AdditionalSQL, SQLStatement, EligibleParticipantsSQL, RemainingSQL, OldTotalTime, OldTeamPoints, OldTeamName, DivisionID, RaceDistanceID, PlaceType, TotalTime as String
		  dim TeamNames_as(), TeamID_as(), Name_as(), Age_as(), TeamPlaces_as(), TeamPlace_as() as string
		  dim TotalTime_as(), Place_as(), TeamTime_as() as string
		  dim Pace_as(), UniqueTeamIDs(), SortArray() as String
		  dim TotalTeamPoints_as(), TeamPoints_as(), TotalTeamTimes_as(), Scorer_as() as String
		  dim rsTeams, rsTeamMembers, rsEligibleRacers, rsRemainingRacers, rsDistance as RecordSet
		  dim ResultsPage as Graphics
		  Dim PageSetup as PrinterSetup
		  dim AtLeastOneFullTeam as boolean 
		  
		  Dim HTMLFile As FolderItem
		  Dim HTMLFileStream As TextOutputStream
		  
		  
		  AtLeastOneFullTeam = false
		  ClassName=""
		  RaceDistanceID="0"
		  DivisionID="0"
		  PageCount=0 
		  
		  if not(HTMLResults) then
		    if app.PrinterSettings <> "" then
		      PageSetup= New PrinterSetup
		      PageSetup.SetupString=app.PrinterSettings
		    end if
		    ResultsPage = OpenPrinterDialog(PageSetup)
		    HTMLFile=Nil
		  else
		    ResultsPage=Nil
		    HTMLFile= GetSaveFolderItem("application/text","")
		  end if
		  
		  if (ResultsPage<>nil and not(HTMLResults)) or (HTMLFile<>nil and HTMLResults)  then
		    
		    if HTMLResults then
		      HTMLFileStream=HTMLFile.CreateTextFile
		      HTMLFileStream.WriteLine "<html>"
		      HTMLFileStream.WriteLine "<body style='font-family: monospace'>"
		      PixelCount=Print_FirstPageHeader(ResultsPage, HTMLFileStream, "Team Standings")+10
		    end if
		    
		    for SelectionIndex=0 to UBound(Selection)-1
		      
		      redim TeamNames_as(-1)
		      redim TeamID_as(-1)
		      redim Name_as(-1)
		      redim Age_as(-1)
		      redim TeamPlaces_as(-1)
		      redim TeamPlace_as(-1)
		      redim TotalTime_as(-1)
		      redim Place_as(-1)
		      redim TeamTime_as(-1)
		      redim Pace_as(-1)
		      redim UniqueTeamIDs(-1)
		      redim SortArray(-1) 
		      redim TotalTeamPoints_as(-1)
		      redim TeamPoints_as(-1)
		      redim TotalTeamTimes_as(-1)
		      redim ScorerCount_al(-1)
		      redim Scorer_as(-1)
		      
		      if Selection(SelectionIndex) then
		        
		        select case DisplayFormat
		        case "Overall By Distance"
		          AdditionalSQL=" AND divisions.RaceDistanceID = "+SelectionID(SelectionIndex)+" "
		          PlaceType="Overall"
		          RaceDistanceID=SelectionID(SelectionIndex)
		          
		        case "Overall By Gender"
		          if (InStr(SelectionText(SelectionIndex),"Female")>0) then 'Gots to check for Female first because the male is part of Female and Male
		            AdditionalSQL=" AND RaceDistanceID = "+SelectionID(SelectionIndex)+" AND Gender='F' "
		            
		          elseif (InStr(SelectionText(SelectionIndex),"Male")>0) then
		            AdditionalSQL=" AND RaceDistanceID = "+SelectionID(SelectionIndex)+" AND Gender='M' "
		            
		          else
		            AdditionalSQL=" AND RaceDistanceID = "+SelectionID(SelectionIndex)+" AND Gender='X' "
		            
		          end if
		          PlaceType="Gender"
		          RaceDistanceID=SelectionID(i)
		        else
		          AdditionalSQL=" AND DivisionID = "+SelectionID(SelectionIndex)+" "
		          DivisionID=SelectionID(SelectionIndex)
		          rsDistance=app.theDB.DBSQLSelect("SELECT racedistances.rowid FROM divisions INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid WHERE divisions.rowid="+SelectionID(SelectionIndex))
		          RaceDistanceID=rsDistance.Field("rowid").StringValue
		          rsDistance.Close
		          
		        end select
		        AdditionalSQL=AdditionalSQL+"AND DNF='N' AND DNS='N' AND DQ='N' "
		        
		        
		        'get the unique team names
		        SQLStatement="SELECT DISTINCT TeamID FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid WHERE TeamID>0"+AdditionalSQL
		        rsTeams=app.theDB.DBSQLSelect(SQLStatement)
		        
		        'find all those teams with teams having between minimum and maximum members to score
		        EligibleParticipantsSQL="SELECT TeamID, Racer_Number, Participant_Name, First_Name, Team_Name, Age, participants.Gender, Total_Time, DQ, DNF, DNS FROM participants "
		        EligibleParticipantsSQL=EligibleParticipantsSQL+"INNER JOIN divisions ON participants.DivisionID = divisions.rowid INNER JOIN CCTeams ON participants.TeamID = CCTeams.rowid WHERE ("
		        For i=1 to rsTeams.RecordCount
		          SQLStatement="SELECT participants.rowid FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid WHERE TeamID='"+rsTeams.Field("TeamID").StringValue+"'"+AdditionalSQL+" AND DNF='N' ORDER BY Total_Time LIMIT 0, "+Str(MaxMembers)
		          rsTeamMembers=app.theDB.DBSQLSelect(SQLStatement)
		          
		          If (rsTeamMembers.RecordCount>=MinMembers) then
		            AtLeastOneFullTeam=true
		            UniqueTeamIDs.Append format(val(rsTeams.Field("TeamID").StringValue), "00000000")
		            For j=1 to rsTeamMembers.RecordCount
		              EligibleParticipantsSQL=EligibleParticipantsSQL+" participants.rowid="+rsTeamMembers.Field("rowid").StringValue+" or "
		              rsTeamMembers.MoveNext
		            Next
		          End if
		          rsTeams.MoveNext
		          rsTeamMembers.Close
		        Next
		        if AtLeastOneFullTeam then
		          EligibleParticipantsSQL=Left(EligibleParticipantsSQL,len(EligibleParticipantsSQL)-4)+") "+AdditionalSQL
		          EligibleParticipantsSQL=EligibleParticipantsSQL+" ORDER BY Total_Time ASC"
		          rsEligibleRacers=app.theDB.DBSQLSelect(EligibleParticipantsSQL)
		          
		          'place overall among eligible scorers
		          Ties=0
		          Place=0
		          OldTotalTime="00:00:00.000"
		          For i=1 to rsEligibleRacers.RecordCount
		            'If OldTotalTime<>rsEligibleRacers.Field("Total_Time").StringValue then
		            'PlaceIncrement=1+Ties
		            'Ties=0
		            'Else
		            'PlaceIncrement=0
		            'Ties=Ties+1
		            'End if
		            
		            TeamNames_as.Append rsEligibleRacers.Field("Team_Name").StringValue
		            TeamID_as.Append format(val(rsEligibleRacers.Field("TeamID").StringValue),"00000000")
		            Name_as.Append rsEligibleRacers.Field("First_Name").StringValue+" "+rsEligibleRacers.Field("Participant_Name").StringValue
		            Age_as.Append rsEligibleRacers.Field("Age").StringValue
		            TeamPlace_as.Append ""
		            TeamPoints_as.Append ""
		            TeamTime_as.Append ""
		            Scorer_as.Append ""
		            
		            if rsEligibleRacers.Field("DQ").StringValue="Y" then
		              Place_as.Append "10000"
		              TotalTime_as.Append "DSQ"
		              Pace_as.Append ""
		              
		            elseif rsEligibleRacers.Field("DNS").StringValue="Y" then
		              Place_as.Append "10000"
		              TotalTime_as.Append "DNS"
		              Pace_as.Append ""
		              
		            elseif rsEligibleRacers.Field("DNF").StringValue="Y"then
		              Place_as.Append "10000"
		              TotalTime_as.Append "DNF"
		              Pace_as.Append ""
		              
		            Else
		              Pace_as.Append app.CalculatePace(RaceDistanceID,rsEligibleRacers.Field("Total_Time").StringValue)
		              if rsEligibleRacers.Field("Participant_Name").StringValue = "Tonn" then
		                beep
		              end if
		              select case DisplayFormat
		              case "Overall By Distance"
		                Place=val(app.GetPlace("Overall",RaceDistanceID,DivisionID,rsEligibleRacers.field("Gender").StringValue,rsEligibleRacers.Field("Total_Time").StringValue,true))
		              case "Division"
		                Place=val(app.GetPlace("Division",RaceDistanceID,DivisionID,rsEligibleRacers.field("Gender").StringValue,rsEligibleRacers.Field("Total_Time").StringValue,true))
		              case "Gender"
		                Place=val(app.GetPlace("Gender",RaceDistanceID,DivisionID,rsEligibleRacers.field("Gender").StringValue,rsEligibleRacers.Field("Total_Time").StringValue,true))
		              end select
		              Place_as.Append format(Place,"00000")
		              if cbRound.Value then
		                TotalTime_as.Append app.ConvertSecondsToTime(round(app.ConvertTimeToSeconds(rsEligibleRacers.Field("Total_Time").StringValue)),false)
		              else
		                TotalTime_as.Append rsEligibleRacers.Field("Total_Time").StringValue
		              end if
		            end if
		            
		            OldTotalTime=rsEligibleRacers.Field("Total_Time").StringValue
		            rsEligibleRacers.MoveNext
		          next
		          
		          'derive points and total time
		          For i=0 to UBound(UniqueTeamIDs)
		            ScorerCount_al.Append 0
		            TotalTeamPoints_as.Append "0"
		            TotalTeamTimes_as.Append "00:00:00.000"
		            SortArray.Append ""
		            For j=0 to UBound(TeamNames_as)
		              If ScorerCount_al(i)<MinMembers and UniqueTeamIDs(i)=TeamID_as(j) Then
		                ScorerCount_al(i)=ScorerCount_al(i)+1
		                Scorer_as(j)="*"
		                TotalTeamPoints_as(i)=format(val(TotalTeamPoints_as(i))+val(Place_as(j)),"00000")
		                TotalTeamTimes_as(i)=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(TotalTeamTimes_as(i))+app.ConvertTimeToSeconds (TotalTime_as(j)),false)
		                SortArray(i)=TotalTeamPoints_as(i)+TotalTeamTimes_as(i)+UniqueTeamIDs(i)
		              End if
		            next
		          next
		          
		          'Check for Ties
		          
		          SortArray.SortWith(TotalTeamPoints_as,TotalTeamTimes_as,UniqueTeamIDs)
		          'delete any teams that didn't have enough scorers
		          
		          for i=1 to ubound(SortArray)
		            if SortArray(0)="" then
		              SortArray.Remove(0)
		              TotalTeamPoints_as.Remove(0)
		              TotalTeamTimes_as.Remove(0)
		              UniqueTeamIDs.Remove(0)
		            end if
		          next
		          
		          OldTeamPoints=""
		          Ties=0
		          Place=0
		          PreviousTeamPlace=0
		          CurrentTeamPlace=0
		          For i=0 to UBound(SortArray)
		            TeamPlaces_as.Append ""
		            If OldTeamPoints<>TotalTeamPoints_as(i) then
		              Place=Place+1
		              TeamPlaces_as(i)=format(Place,"00000")
		            Else
		              Place=Place+1
		              'Find the previous team's next runner
		              ScorerCount=0
		              For j=0 to UBound(TeamID_as)
		                If UniqueTeamIDs(i-1)=TeamID_as(j) then
		                  ScorerCount=ScorerCount+1
		                  If (ScorerCount=(MinMembers+1)) then
		                    PreviousTeamPlace=val(Place_as(j))
		                  End if
		                End if
		              Next
		              
		              'Find the current team's next runner
		              ScorerCount=0
		              For j=0 to UBound(TeamID_as)
		                If (UniqueTeamIDs(i)=TeamID_as(j)) then
		                  ScorerCount=ScorerCount+1
		                  If (ScorerCount=(MinMembers+1)) then
		                    CurrentTeamPlace=val(Place_as(j))
		                  End if
		                End if
		              Next
		              
		              If (i>=1) then
		                if PreviousTeamPlace=0 and CurrentTeamPlace=0 then 'True tie
		                  TeamPlaces_as(i)=TeamPlaces_as(i-1)
		                  
		                elseif PreviousTeamPlace=0 and CurrentTeamPlace>0 then 'First is zero - no additional team members so current team wins
		                  TeamPlaces_as(i)=TeamPlaces_as(i-1)
		                  TeamPlaces_as(i-1)=format(Place,"00000")
		                  
		                elseif PreviousTeamPlace>0 and CurrentTeamPlace=0 then 'Second is zero - no additional team members so previous team wins
		                  TeamPlaces_as(i)=format(Place,"00000")
		                  
		                elseif PreviousTeamPlace<CurrentTeamPlace then 'Previous is lower previous wins
		                  TeamPlaces_as(i)=format(Place,"00000")
		                  
		                elseif PreviousTeamPlace>CurrentTeamPlace then 'Current is lower current wins
		                  TeamPlaces_as(i)=TeamPlaces_as(i-1)
		                  TeamPlaces_as(i-1)=format(Place,"00000")
		                  
		                End if
		              End if
		            End if
		            
		            OldTeamPoints=TotalTeamPoints_as(i)
		          Next
		          
		          'update total points for the team
		          For i=0 to UBound(UniqueTeamIDs)
		            For j=0 to UBound(TeamID_as)
		              If (UniqueTeamIDs(i)=TeamID_as(j)) then
		                TeamPlace_as(j)=TeamPlaces_as(i)
		                TeamPoints_as(j)=TotalTeamPoints_as(i)
		                TeamTime_as(j)=TotalTeamTimes_as(i)
		              End if
		            next
		          next
		          
		          'Now need to sort the arrays
		          Redim SortArray(Ubound(TeamID_as))
		          For i=0 to UBound(TeamID_as)
		            SortArray(i)=TeamPlace_as(i)+TeamID_as(i)+Place_as(i)
		          next
		          SortArray.SortWith(TeamPlace_as,TeamNames_as,TeamPoints_as,TeamTime_as,Place_as,Name_as,Age_as,Pace_as,TotalTime_as,Scorer_as)
		          
		          'Now Print the arrays
		          if not(HTMLResults) then
		            if PageCount>0 then
		              ResultsPage.NextPage
		            end if
		          end if
		          
		          if not(HTMLResults) then
		            PixelCount=Print_FirstPageHeader(ResultsPage, HTMLFileStream, "Team Standings")+10
		          end if
		          
		          PrintCCRunResults_Header(ResultsPage,HTMLFileStream, PixelCount,SelectionText(SelectionIndex))
		          
		          OldTeamName=""
		          For i=0 to UBound(TeamNames_as)
		            'If New Team, Print Team Line
		            if OldTeamName <> TeamNames_as(i) then
		              if OldTeamName<>"" then
		                'Print any remaining team members
		                RemainingSQL="SELECT Participant_Name, First_Name, Age, Total_Time, DQ, DNF, DNS FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid WHERE Representing = '"+OldTeamName+"'"+AdditionalSQL
		                RemainingSQL=RemainingSQL+"ORDER BY Total_Time LIMIT "+str(MaxMembers)+", 9999"
		                rsRemainingRacers=app.theDB.DBSQLSelect(RemainingSQL)
		                if rsRemainingRacers.RecordCount>0 then
		                  for j=1 to rsRemainingRacers.RecordCount
		                    TeamMemberCount=TeamMemberCount+1
		                    
		                    'TotalTime=app.StripLeadingZeros(app.ConvertSecondsToTime(round(app.ConvertTimeToSeconds(rsRemainingRacers.Field("Total_Time").StringValue)),false))
		                    
		                    if (TeamMemberCount mod 2)<>0 then ' Column 1
		                      'new header if necessary
		                      if not(HTMLResults) then
		                        if PixelCount+75>ResultsPage.Height then
		                          PrintCCRunResults_Footer(ResultsPage, HTMLFileStream)
		                          ResultsPage.NextPage
		                          PrintCCRunResults_Header(ResultsPage,HTMLFileStream,10,SelectionText(SelectionIndex))
		                          PixelCount=15
		                          
		                          PixelCount=PixelCount+20
		                          PrintCCRunResults_TeamLine(ResultsPage,HTMLFileStream,"","",OldTeamName+" Continued...", "",PixelCount)
		                          PixelCount=PixelCount+10
		                          
		                          '  print runner header
		                          PrintCCRunResults_RunnerHeader(ResultsPage,HTMLFileStream,PixelCount)
		                          PixelCount=PixelCount+10
		                        end if
		                      end if
		                      PrintCCRunResults_RunnerDetail(ResultsPage,HTMLFileStream,"",rsRemainingRacers.Field("First_Name").StringValue+" "+rsRemainingRacers.Field("Participant_Name").StringValue,rsRemainingRacers.Field("Age").StringValue, _
		                      app.StripLeadingZeros(app.CalculatePace(RaceDistanceID,TotalTime)),TotalTime,"",0,PixelCount)
		                    else 'Column 2
		                      PrintCCRunResults_RunnerDetail(ResultsPage,HTMLFileStream,"",rsRemainingRacers.Field("First_Name").StringValue+" "+rsRemainingRacers.Field("Participant_Name").StringValue,rsRemainingRacers.Field("Age").StringValue, _
		                      app.StripLeadingZeros(app.CalculatePace(RaceDistanceID,TotalTime)),TotalTime,"",275,PixelCount)
		                      PixelCount=PixelCount+10
		                    end if
		                    rsRemainingRacers.MoveNext
		                  next
		                end if
		              end if
		              
		              'Print header if necessary
		              if not(HTMLResults) then
		                if PixelCount+110>ResultsPage.Height then
		                  PrintCCRunResults_Footer(ResultsPage,HTMLFileStream)
		                  ResultsPage.NextPage
		                  PrintCCRunResults_Header(ResultsPage,HTMLFileStream,10,SelectionText(SelectionIndex))
		                  PixelCount=15
		                end if
		              end if
		              
		              'Print Team Line
		              PixelCount=PixelCount+20
		              PrintCCRunResults_TeamLine(ResultsPage,HTMLFileStream,app.StripLeadingZeros(TeamPlace_as(i)),app.StripLeadingZeros(TeamPoints_as(i)),TeamNames_as(i), _
		              app.StripLeadingZeros(TeamTime_as(i)),PixelCount)
		              PixelCount=PixelCount+10
		              
		              '  print runner header
		              PrintCCRunResults_RunnerHeader(ResultsPage,HTMLFileStream,PixelCount)
		              PixelCount=PixelCount+10
		              TeamMemberCount=0
		              
		            End if
		            
		            'if not New Team Print Runner information two up
		            TeamMemberCount=TeamMemberCount+1
		            if (TeamMemberCount mod 2)<>0 then ' Column 1
		              PrintCCRunResults_RunnerDetail(ResultsPage,HTMLFileStream,app.StripLeadingZeros(Place_as(i)),Name_as(i),Age_as(i),app.StripLeadingZeros(Pace_as(i)),app.StripLeadingZeros(TotalTime_as(i)),Scorer_as(i),0,PixelCount)
		            else 'Column 2
		              PrintCCRunResults_RunnerDetail(ResultsPage,HTMLFileStream,app.StripLeadingZeros(Place_as(i)),Name_as(i),Age_as(i),app.StripLeadingZeros(Pace_as(i)),app.StripLeadingZeros(TotalTime_as(i)),Scorer_as(i),275,PixelCount)
		              PixelCount=PixelCount+10
		            end if
		            
		            OldTeamName=TeamNames_as(i)
		          next
		          if not(HTMLResults) then
		            PrintCCRunResults_Footer(ResultsPage,HTMLFileStream)
		          end if
		        else
		          
		          PixelCount=Print_FirstPageHeader(ResultsPage,HTMLFileStream,"Team Standings")+10
		          PrintCCRunResults_Header(ResultsPage,HTMLFileStream,PixelCount,SelectionText(SelectionIndex))
		          PrintCCRunResults_NoCompleteTeams(ResultsPage,HTMLFileStream,MinMembers)
		          PrintCCRunResults_Footer(ResultsPage,HTMLFileStream)
		        end if
		        
		      end if
		    next
		    if HTMLResults then
		      PrintCCRunResults_Footer(ResultsPage,HTMLFileStream)
		      HTMLFileStream.WriteLine "</body></html>"
		      HTMLFileStream.Close
		    end if
		    return true
		  else
		    return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintCCRunResults_Footer(ByRef ResultsPage as Graphics, HTMLFile as TextOutputStream)
		  dim OutputString as String
		  dim FooterStart as integer
		  
		  if not(HTMLResults) then
		    PageCount=PageCount+1
		    FooterStart=ResultsPage.Height-35
		    
		    'print line
		    resultspage.PenWidth=3
		    ResultsPage.DrawLine 10, FooterStart, ResultsPage.Width, FooterStart
		    
		    'print adjustment notification
		    ResultsPage.Bold=false
		    ResultsPage.DrawString "* indicates runner scored team points", 10,FooterStart+10
		    
		    'print page number
		    ResultsPage.Bold=true
		    OutputString= "Page: "+str(PageCount)
		    ResultsPage.DrawString OutputString,ResultsPage.Width-ResultsPage.StringWidth(OutputString)-10,FooterStart+10
		  end if
		  
		  'print copyright stuff
		  OutputString="2005-2009 Milliseconds Computer Services, LLC"
		  if not(HTMLResults) then
		    ResultsPage.bold=false
		    ResultsPage.TextSize=8
		    ResultsPage.DrawString "© "+OutputString,ResultsPage.Width-ResultsPage.StringWidth(OutputString)-10,FooterStart+20
		  else
		    HTMLFile.WriteLine "<br><br>&copy "+OutputString+"<br>"
		  end if
		  
		  OutputString="801.582.3121/www.milliseconds.com"
		  if not(HTMLResults) then
		    ResultsPage.DrawString OutputString,ResultsPage.Width-ResultsPage.StringWidth(OutputString)-10,FooterStart+30
		  else
		    HTMLFile.WriteLine OutputString
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintCCRunResults_Header(ByRef ResultsPage as Graphics, HTMLFile as TextOutputStream, PixelPosition as integer, ResultsName as string)
		  if not(HTMLResults) then
		    'Populate Results NameResultsPage.Bold=True
		    ResultsPage.TextSize=12
		    ResultsPage.DrawString ResultsName, 10, PixelPosition
		    
		    PixelPosition=PixelPosition+15
		    
		    'Populate Results Name
		    ResultsPage.Bold=True
		    ResultsPage.TextSize=12
		    ResultsPage.DrawString "Place", 10, PixelPosition
		    ResultsPage.DrawString "Total Points", 75, PixelPosition
		    ResultsPage.DrawString "Team Name", 175, PixelPosition
		    ResultsPage.DrawString "Total Time", 500, PixelPosition
		    ResultsPage.Bold=False
		    
		    resultspage.PenWidth=1
		    ResultsPage.DrawLine 10, PixelPosition+2, 580, PixelPosition+2
		  else
		    HTMLFile.WriteLine "<br><br>"+ResultsName+"<br>"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintCCRunResults_NoCompleteTeams(ByRef ResultsPage as Graphics, HTMLFile as TextOutputStream, MinMembers as Integer)
		  if not(HTMLResults) then
		    ResultsPage.DrawString "There are no teams with more than "+str(MinMembers)+" racers.", 40, 200
		  else
		    HTMLFile.WriteLine "There are no teams with more than "+str(MinMembers)+" racers.<br>"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintCCRunResults_RunnerDetail(ByRef ResultsPage as Graphics, HTMLFile as TextOutputStream, Points as String, RacerName as String, Grade as String, Pace as string, TotalTime as string, Scorer as string, ColumnBase as Integer, PixelPosition as integer)
		  if not(HTMLResults) then
		    
		    PixelPosition=PixelPosition+10
		    
		    if cbRound.Value then
		      TotalTime=Replace(Replace(Replace(Replace(TotalTime,".000",""),".00",""),".0",""),".","") 'remove the trailing zeros if rounded
		    end if
		    
		    'Populate Results Name
		    ResultsPage.TextSize=8
		    
		    ResultsPage.DrawString Points, (ColumnBase+30+(15-ResultsPage.StringWidth(Points)/2)), PixelPosition, 25, True
		    ResultsPage.DrawString RacerName, ColumnBase+70, PixelPosition, 100, True
		    ResultsPage.DrawString Grade, (ColumnBase+155+(15-ResultsPage.StringWidth(Grade)/2)), PixelPosition, 25, True
		    ResultsPage.DrawString Pace, ColumnBase+195, PixelPosition, 40, True
		    ResultsPage.DrawString TotalTime, ColumnBase+240, PixelPosition, 65, True
		    ResultsPage.DrawString Scorer, ColumnBase+285, PixelPosition, 10, True
		    
		  else
		    if Scorer<>"" then
		      HTMLFile.Write " "+Points+" "+RacerName+" "+TotalTime+HTMLPad(" ","R",2)
		    else
		      HTMLFile.Write " ("+Points+") "+RacerName+" "+TotalTime+HTMLPad(" ","R",2)
		    end if
		    if MemberCount>=275 then
		      MemberCount=0
		      HTMLFile.Write "<br>"+HTMLPad(" ","R",3)
		    else
		      MemberCount=MemberCount+ColumnBase
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintCCRunResults_RunnerHeader(ByRef ResultsPage as Graphics, HTMLFile as TextOutputStream, PixelPosition as integer)
		  if not(HTMLResults) then
		    PixelPosition=PixelPosition+10
		    
		    'Populate Results Name
		    ResultsPage.TextSize=8
		    
		    'First Column
		    ResultsPage.DrawString "Points", 30, PixelPosition, 50, True
		    ResultsPage.DrawString "Name", 70, PixelPosition, 100, True
		    ResultsPage.DrawString "Pace", 195, PixelPosition, 40, True
		    ResultsPage.DrawString "Time", 240, PixelPosition, 65, True
		    
		    'Second Column
		    ResultsPage.DrawString "Points", 305, PixelPosition, 50, True
		    ResultsPage.DrawString "Name", 345, PixelPosition, 100, True
		    ResultsPage.DrawString "Pace", 470, PixelPosition, 40, True
		    ResultsPage.DrawString "Time", 515, PixelPosition, 70, True
		    ResultsPage.Bold=False
		  else
		    HTMLFile.Write HTMLPad(" ","R",3)
		    MemberCount=0
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintCCRunResults_TeamLine(ResultsPage as graphics, HTMLFile as TextOutputStream,Place as string,TotalPoints as string, RacerName as string, TotalTime as string,PixelPosition As integer)
		  Dim Pixels as integer
		  
		  if cbRound.Value then
		    TotalTime=Replace(Replace(Replace(Replace(TotalTime,".000",""),".00",""),".0",""),".","") 'remove the trailing zeros if rounded
		  end if
		  
		  if not(HTMLResults) then
		    PixelPosition=PixelPosition+10
		    
		    ResultsPage.Bold=True
		    
		    ResultsPage.TextSize=12
		    ResultsPage.DrawString Place, (10+(15-ResultsPage.StringWidth(Place)/2)), PixelPosition
		    ResultsPage.DrawString TotalPoints, (75+(35-ResultsPage.StringWidth(TotalPoints)/2)), PixelPosition
		    ResultsPage.DrawString RacerName, 175, PixelPosition
		    ResultsPage.DrawString TotalTime, 500, PixelPosition
		    
		    ResultsPage.Bold=True
		    
		  else
		    HTMLFile.WriteLine "<br><br>"+Place+" "+HTMLPad(RacerName,"L",30)+" "+HTMLPad(TotalPoints,"R",5)+" "+HTMLPad(TotalTime,"R",12)+"<br>"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function PrintIntervalResults(FinalResults as boolean, SelectionID() as string, Selection() as boolean, SelectionText() as string, ListIntervals as boolean, ListDNF as boolean, DisplayFormat as string, NonPrintedRacers as boolean, FromAutoPrint as boolean) As boolean
		  dim i, Old_i, PageLength, ParticipantCount, PixelCount, PlacesToPrint, PagesToPrint, Adjustment_DivisionID as integer
		  dim ResultsPage as Graphics
		  dim Place, IntervalTime, Back, Pace, ParticipantName, Representing, ResultsType, SelectStatement, TotalTime, AdjustmentText() as String
		  dim rsQueryResults,rsParticipant, rsInterval as RecordSet
		  Dim PageSetup as PrinterSetup
		  dim SortByTotalTime as Boolean
		  
		  
		  Dim d as New MessageDialog //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  d.icon=MessageDialog.GraphicCaution //display warning icon
		  d.ActionButton.Caption="Interval Time"
		  d.CancelButton.Caption="Total Time"
		  d.CancelButton.Visible=True
		  d.Message="How do you want to sort the times?"
		  d.Explanation="Click on the button to select the type of sort."
		  
		  b=d.ShowModal //display the dialog
		  
		  if b=d.ActionButton then
		    SortByTotalTime=false
		  else
		    SortByTotalTime=true
		  end if
		  
		  if app.PrinterSettings <> "" then
		    PageSetup= New PrinterSetup
		    PageSetup.SetupString=app.PrinterSettings
		  end if
		  
		  if not(FromAutoPrint) then
		    ResultsPage = OpenPrinterDialog(PageSetup)
		  else
		    'ResultsPage = OpenPrinterDialog(PageSetup)
		    ResultsPage = OpenPrinter(PageSetup)
		  end if
		  
		  if ResultsPage<>nil then
		    
		    
		    
		    
		    if Places.text="All" then
		      PlacesToPrint=999999
		    else
		      PlacesToPrint=val(Places.text)
		    end if
		    
		    PageLength=ResultsPage.Height
		    Old_i=-1 ' force the first header
		    PageCount=0
		    LineCount=0
		    
		    if not(FinalResults) then
		      PixelCount=PrintIntervalResults_FirstPageHeader(ResultsPage,"Preliminary Results")
		    else
		      PixelCount=PrintIntervalResults_FirstPageHeader(ResultsPage,"Final Results")
		    end if
		    
		    for i=0 to UBound(Selection)
		      if Selection(i) then
		        SelectStatement="SELECT A.rowid AS ParticipantID, A.DivisionID, A.Racer_Number, A.Gender, "
		        SelectStatement=SelectStatement+"A.Participant_Name, A.First_Name, B.Division_Name, B.RaceDistanceID, A.Representing, "
		        SelectStatement=SelectStatement+"B.Division_Name, C.Interval_Time, C.Total_Time, C.Interval_Number, "
		        SelectStatement=SelectStatement+"A.DNF, A.DNS, A.DQ, D.MemberName  "
		        SelectStatement=SelectStatement+"FROM participants AS A INNER JOIN divisions AS B ON A.DivisionID = B.rowid "
		        SelectStatement=SelectStatement+"INNER JOIN times AS C ON C.ParticipantID = A.rowid "
		        SelectStatement=SelectStatement+"LEFT OUTER JOIN teamMembers D ON D.ParticipantID = C.ParticipantID AND D.IntervalNumber = C.Interval_Number "
		        SelectStatement=SelectStatement+"WHERE C.Use_This_Passing='Y' AND C.Interval_Number="+puFormat.RowTag(puFormat.ListIndex)+" AND "
		        
		        
		        if (InStr(puFormat.Text,"(by Distance)")>0) then
		          
		          if not(ListDNF) then
		            SelectStatement=SelectStatement+"A.DNF = 'N' AND "
		            
		            SelectStatement=SelectStatement+"C.Interval_Time <> '00:00:00.00' AND "
		          end if
		          SelectStatement=SelectStatement+"B.RaceDistanceID = "+SelectionID(i)
		          if SortByTotalTime then
		            SelectStatement=SelectStatement+" ORDER BY C.Total_Time ASC, "
		          else
		            SelectStatement=SelectStatement+" ORDER BY C.Interval_Time ASC, "
		          end if
		          SelectStatement=SelectStatement+"A.Participant_Name ASC, A.First_Name ASC"
		          
		        elseif (InStr(puFormat.Text,"(by Gender)")>0) then
		          
		          if not(ListDNF) then
		            SelectStatement=SelectStatement+"A.DNF = 'N' AND "
		            
		            'SelectStatement=SelectStatement+"C.Gender_Place > 0 AND "
		          end if
		          
		          if (InStr(SelectionText(i),"Female")>0) then 'Gots to check for Female first because the male is part of Female and Male
		            SelectStatement=SelectStatement+"B.RaceDistanceID = "+SelectionID(i)+" AND A.Gender='F' "
		            
		          elseif (InStr(SelectionText(i),"Male")>0) then
		            SelectStatement=SelectStatement+"B.RaceDistanceID = "+SelectionID(i)+" AND A.Gender='M' "
		            
		          else
		            SelectStatement=SelectStatement+"B.RaceDistanceID = "+SelectionID(i)+" AND A.Gender='X' "
		            
		          end if
		          if SortByTotalTime then
		            SelectStatement=SelectStatement+" ORDER BY C.Total_Time ASC, "
		          else
		            SelectStatement=SelectStatement+" ORDER BY C.Interval_Time ASC, "
		          end if
		          SelectStatement=SelectStatement+"A.Participant_Name ASC, A.First_Name ASC"
		          
		        else
		          
		          if not(ListDNF) then
		            SelectStatement=SelectStatement+"A.DNF = 'N' AND "
		            
		            'SelectStatement=SelectStatement+"C.Division_Place > 0 AND "
		          end if
		          SelectStatement=SelectStatement+"A.DivisionID = "+SelectionID(i)
		          if SortByTotalTime then
		            SelectStatement=SelectStatement+" ORDER BY C.Total_Time ASC, "
		          else
		            SelectStatement=SelectStatement+" ORDER BY C.Interval_Time ASC, "
		          end if
		          SelectStatement=SelectStatement+"A.Participant_Name ASC, A.First_Name ASC"
		          
		        end if
		        rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		        
		        if not (rsQueryResults.EOF) then
		          ParticipantCount=0
		          
		          'Determine places to print
		          if FromAutoPrint then
		            if wnd_List.AutoPrint_FirstPagePrinted then
		              PagesToPrint=rsQueryResults.RecordCount\64
		              if PagesToPrint > 0 then
		                PlacesToPrint=PagesToPrint*64
		              else
		                PlacesToPrint=0
		              end if
		            else
		              if rsQueryResults.RecordCount>61 and rsQueryResults.RecordCount<125 then
		                if (rsQueryResults.RecordCount\61)>0 then
		                  PlacesToPrint=61
		                end if
		              else
		                PagesToPrint=(rsQueryResults.RecordCount-61)\64
		                if PagesToPrint>0 then
		                  PlacesToPrint=PagesToPrint*64+61
		                else
		                  PlacesToPrint=0
		                end if
		              end if
		              
		            end if
		          end if
		          
		          while (not (rsQueryResults.EOF) and (ParticipantCount<PlacesToPrint))
		            ParticipantCount=ParticipantCount+1
		            'print header
		            if (PixelCount+LineCount*10>PageLength-50 or PixelCount+LineCount*10>PageLength-75) Then
		              PrintIntervalResults_Footer(ResultsPage)
		              ResultsPage.NextPage
		              PixelCount=25
		              LineCount=0
		              PrintIntervalResults_PageHeader(ResultsPage)
		              
		              if Old_i = i then
		                PrintIntervalResults_Header(ResultsPage,rsQueryResults.Field("DivisionID").IntegerValue,DisplayFormat,DisplayFormat+": "+SelectionText(i)+" Continued",PixelCount)
		              else
		                PrintIntervalResults_Header(ResultsPage,rsQueryResults.Field("DivisionID").IntegerValue,DisplayFormat,DisplayFormat+": "+SelectionText(i),PixelCount)
		              end if
		              PixelCount=PixelCount+35
		            else
		              if Old_i <> i then
		                if (PixelCount+LineCount*10>PageLength-95) or (FromAutoPrint and Not(wnd_List.AutoPrint_FirstPagePrinted)) or _
		                  ((PixelCount+LineCount*10>PageLength-115) or (FromAutoPrint and Not(wnd_List.AutoPrint_FirstPagePrinted))) Then 'check to make sure it won't overflow the page, or if auto print need a page break between distances
		                  
		                  PrintIntervalResults_Footer(ResultsPage)
		                  ResultsPage.NextPage
		                  PixelCount=25
		                  LineCount=0
		                  PrintIntervalResults_PageHeader(ResultsPage)
		                end if
		                PrintIntervalResults_Header(ResultsPage,rsQueryResults.Field("DivisionID").IntegerValue,DisplayFormat,DisplayFormat+": "+SelectionText(i),PixelCount+LineCount*10)
		                PixelCount=PixelCount+35
		                Old_i=i
		              end if
		            end if
		            
		            'print detail lines
		            if rsQueryResults.Field("MemberName").StringValue="" then
		              if rsQueryResults.Field("First_Name").stringValue <> "" then
		                ParticipantName=rsQueryResults.Field("Participant_Name").stringValue+", "+rsQueryResults.Field("First_Name").stringValue
		              else
		                ParticipantName=rsQueryResults.Field("Participant_Name").stringValue
		              end if
		              Representing=rsQueryResults.Field("Representing").stringValue
		            else
		              ParticipantName=rsQueryResults.Field("MemberName").stringValue
		              Representing=rsQueryResults.Field("Participant_Name").stringValue
		            end if
		            
		            if cbRound.Value then
		              IntervalTime=left(app.ConvertSecondsToTime(round(app.ConvertTimeToSeconds(rsQueryResults.Field("Interval_Time").stringValue)),false),8)
		              TotalTime=left(app.ConvertSecondsToTime(round(app.ConvertTimeToSeconds(rsQueryResults.Field("Total_Time").stringValue)),false),8)
		            else
		              IntervalTime=rsQueryResults.Field("Interval_Time").stringValue
		              TotalTime=rsQueryResults.Field("Total_Time").stringValue
		            end if
		            
		            IntervalTime=app.TruncateTime(app.StripLeadingZeros(IntervalTime))
		            TotalTime=app.TruncateTime(app.StripLeadingZeros(TotalTime))
		            
		            'if rsQueryResults.Field("Gender_Back").stringValue <> "" then
		            'Back=app.GetIntervalBack("Gender",rsQueryResults.Field("ParticipantID").StringValue,rsQueryResults.Field("DivisionID").StringValue, _
		            'rsQueryResults.Field("Gender").StringValue,rsQueryResults.Field("Interval_Number").StringValue,rsQueryResults.Field("Interval_Time").StringValue)
		            'else
		            'Back=""
		            'end if
		            
		            if (InStr(puFormat.Text,"(by Distance)")>0) then
		              if SortByTotalTime then
		                Place=app.GetIntervalPlace("Overall",rsQueryResults.Field("RaceDistanceID").StringValue,rsQueryResults.Field("DivisionID").StringValue,rsQueryResults.Field("Gender").StringValue, _
		                rsQueryResults.Field("Interval_Number").StringValue,rsQueryResults.Field("Total_Time").StringValue,"Total Time",ListDNF)
		              else
		                Place=app.GetIntervalPlace("Overall",rsQueryResults.Field("RaceDistanceID").StringValue,rsQueryResults.Field("DivisionID").StringValue,rsQueryResults.Field("Gender").StringValue, _
		                rsQueryResults.Field("Interval_Number").StringValue,rsQueryResults.Field("Interval_Time").StringValue,"Interval Time",ListDNF)
		              end if
		              Back=app.GetIntervalBack("Overall",rsQueryResults.Field("RaceDistanceID").StringValue,rsQueryResults.Field("DivisionID").StringValue, _
		              rsQueryResults.Field("Gender").StringValue,rsQueryResults.Field("Interval_Number").StringValue,rsQueryResults.Field("Total_Time").StringValue,true)
		              
		              
		            elseif (InStr(puFormat.Text,"(by Gender)")>0) then
		              if SortByTotalTime then
		                Place=app.GetIntervalPlace("Gender",rsQueryResults.Field("RaceDistanceID").StringValue,rsQueryResults.Field("DivisionID").StringValue,rsQueryResults.Field("Gender").StringValue, _
		                rsQueryResults.Field("Interval_Number").StringValue,rsQueryResults.Field("Total_Time").StringValue,"Total Time",ListDNF)
		              else
		                Place=app.GetIntervalPlace("Gender",rsQueryResults.Field("RaceDistanceID").StringValue,rsQueryResults.Field("DivisionID").StringValue,rsQueryResults.Field("Gender").StringValue, _
		                rsQueryResults.Field("Interval_Number").StringValue,rsQueryResults.Field("Interval_Time").StringValue,"Interval Time",ListDNF)
		              end if
		              Back=app.GetIntervalBack("Gender",rsQueryResults.Field("RaceDistanceID").StringValue,rsQueryResults.Field("DivisionID").StringValue, _
		              rsQueryResults.Field("Gender").StringValue,rsQueryResults.Field("Interval_Number").StringValue,rsQueryResults.Field("Total_Time").StringValue,true)
		              
		            else
		              
		              if SortByTotalTime then
		                Place=app.GetIntervalPlace("Division",rsQueryResults.Field("RaceDistanceID").StringValue,rsQueryResults.Field("DivisionID").StringValue,rsQueryResults.Field("Gender").StringValue, _
		                rsQueryResults.Field("Interval_Number").StringValue,rsQueryResults.Field("Total_Time").StringValue,"Total Time",ListDNF)
		              else
		                Place=app.GetIntervalPlace("Division",rsQueryResults.Field("RaceDistanceID").StringValue,rsQueryResults.Field("DivisionID").StringValue,rsQueryResults.Field("Gender").StringValue, _
		                rsQueryResults.Field("Interval_Number").StringValue,rsQueryResults.Field("Interval_Time").StringValue,"Interval Time",ListDNF)
		              end if
		              Back=app.GetIntervalBack("Division",rsQueryResults.Field("ParticipantID").StringValue,rsQueryResults.Field("DivisionID").StringValue, _
		              rsQueryResults.Field("Gender").StringValue,rsQueryResults.Field("Interval_Number").StringValue,rsQueryResults.Field("Total_Time").StringValue,true)
		              
		            end if
		            
		            SelectStatement="SELECT Pace_Type, Actual_Distance FROM intervals WHERE Number="+rsQueryResults.Field("Interval_Number").StringValue+" AND "+rsQueryResults.Field("DivisionID").StringValue
		            rsInterval=app.theDB.DBSQLSelect(SelectStatement)
		            if rsInterval<>nil then
		              Pace=app.CalculateIntPace(rsInterval.Field("Pace_type").StringValue,rsInterval.Field("Actual_Distance").DoubleValue,rsQueryResults.Field("Interval_Time").StringValue)
		              rsInterval.Close
		            else
		              Pace=""
		            end if
		            PrintIntervalResults_Detail(ResultsPage, PixelCount+LineCount*10, Place, _
		            rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, _
		            Representing, rsQueryResults.Field("Division_Name").stringValue, _
		            IntervalTime, TotalTime, Back, Pace, DataList.Cell(i,3))
		            
		            rsQueryResults.moveNext
		            LineCount=LineCount+1
		            
		          wend
		          rsQueryResults.Close
		          
		          
		        end if 'if not (rsQueryResults.EOF) then
		        
		      end if 'if Selection(i) then
		    next
		    
		    'print footer
		    if LineCount>0 then
		      if app.ResultsDisplayType<>"Sea Otter" then
		        PrintIntervalResults_Footer(ResultsPage)
		      else
		        PrintSeaOtterResults_Footer(ResultsPage)
		      end if
		      
		      if UBound(AdjustmentText)>0 then
		        AdjustmentText.Sort
		        PrintIntervalResults_Footer(ResultsPage)
		        ResultsPage.NextPage
		        PixelCount=25
		        LineCount=0
		        PrintIntervalResults_PageHeader(ResultsPage)
		        
		        PixelCount=PixelCount+35
		        
		        for i = 0 to UBound(AdjustmentText)
		          if PixelCount+LineCount*10>PageLength-50 Then
		            ResultsPage.NextPage
		            PixelCount=25
		            LineCount=0
		            PrintIntervalResults_PageHeader(ResultsPage)
		          end if
		        next
		        PrintIntervalResults_Footer(ResultsPage)
		      end if
		      
		    end if  ' if LineCount>0 then
		    
		    return true
		  else
		    return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintIntervalResults_Detail(ByRef ResultsPage as Graphics, PageLine as integer, RacerPlace as string, RacerNumber as string, RacerName as String, RacerRepresenting as string, RacerDivision as string, RacerIntervalTime as string, RacerTotalTime as string, RacerBackTime as string, RacerPace as string, StageCutoff as string)
		  if LineCount mod 2 = 1 then
		    ResultsPage.ForeColor=&cDDDDDD
		    ResultsPage.FillRect 10, PageLine-7, ResultsPage.Width-15, 9
		    ResultsPage.ForeColor=&c000000
		  end if
		  
		  if  ((app.Registered) or (not(app.Registered) and LineCount mod 3 = 0)) then
		    
		    ResultsPage.DrawString RacerPlace, 10+(12-ResultsPage.StringWidth(RacerPlace)/2), PageLine
		    
		    ResultsPage.DrawString RacerNumber, 40+(12-ResultsPage.StringWidth(RacerNumber)/2), PageLine
		    ResultsPage.DrawString RacerName, 70, PageLine, 170, True
		    ResultsPage.DrawString RacerRepresenting, 200, PageLine, 140, True
		    
		    ResultsPage.DrawString RacerIntervalTime, 380+(25-ResultsPage.StringWidth(RacerTotalTime)/2), PageLine, 47, true
		    ResultsPage.DrawString RacerTotalTime, 440+(25-ResultsPage.StringWidth(RacerTotalTime)/2), PageLine, 47, true
		    ResultsPage.DrawString RacerBackTime, 490+(25-ResultsPage.StringWidth(RacerBackTime)/2), PageLine, 50, true
		    ResultsPage.DrawString RacerPace, 535+(25-ResultsPage.StringWidth(RacerPace)/2), PageLine, 50, true
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function PrintIntervalResults_FirstPageHeader(ByRef ResultsPage as Graphics, ResultsType as string) As Integer
		  dim CurrentDate as new date
		  dim d as Double
		  dim HalfWidth,Position, PixelCount, w, h, newh, neww, newy as integer
		  dim DateTime as string
		  
		  HalfWidth=ResultsPage.Width/2
		  DateTime=CurrentDate.LongDate+" "+CurrentDate.LongTime
		  
		  'Populate Logos
		  
		  if wnd_List.Logo1<>"" or wnd_List.Logo2<>"" or wnd_List.Logo3<>"" or wnd_List.Logo4<>"" then
		    if wnd_List.Logo1<>"" then
		      Print_DrawLogos(ResultsPage, wnd_List.Logo1, 10, 100, 100)
		    end if
		    
		    if wnd_List.Logo2<>"" then
		      Print_DrawLogos(ResultsPage, wnd_List.Logo2, 175, 100, 100)
		    end if
		    
		    if wnd_List.Logo3<>"" then
		      Print_DrawLogos(ResultsPage, wnd_List.Logo3, 320, 100, 100)
		    end if
		    
		    if wnd_List.Logo4<>"" then
		      Print_DrawLogos(ResultsPage, wnd_List.Logo4,450, 100, 100)
		    end if
		    
		    PixelCount=120
		  else
		    
		    PixelCount=10
		  end if
		  
		  'Populate Race Name
		  PixelCount=PixelCount+10
		  ResultsPage.TextSize=18
		  ResultsPage.Bold=True
		  Position=HalfWidth-(ResultsPage.StringWidth(wnd_List.RaceName.Text)/2)
		  ResultsPage.DrawString wnd_List.RaceName.Text,Position, PixelCount
		  
		  'Populate Results Type
		  PixelCount=PixelCount+20
		  ResultsPage.TextSize=18
		  ResultsPage.Bold=True
		  Position=HalfWidth-(ResultsPage.StringWidth(ResultsType)/2)
		  ResultsPage.DrawString ResultsType, Position, PixelCount
		  
		  'Populate Date and Time
		  PixelCount=PixelCount+25
		  ResultsPage.TextSize=8
		  Position=HalfWidth-(ResultsPage.StringWidth(DateTime)/2)
		  ResultsPage.DrawString DateTime, Position, PixelCount
		  
		  
		  PixelCount=PixelCount+5
		  resultspage.PenWidth=3
		  ResultsPage.DrawLine 10, PixelCount, ResultsPage.Width, PixelCount
		  
		  ResultsPage.TextSize=8
		  ResultsPage.Bold=False
		  
		  PixelCount=PixelCount+5
		  
		  Return PixelCount
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintIntervalResults_Footer(ByRef ResultsPage as Graphics)
		  dim OutputString as String
		  dim FooterStart as integer
		  
		  PageCount=PageCount+1
		  FooterStart=ResultsPage.Height-40
		  
		  'print line
		  resultspage.PenWidth=3
		  ResultsPage.DrawLine 10, FooterStart, ResultsPage.Width, FooterStart
		  
		  'print adjustment notification
		  ResultsPage.Bold=false
		  ResultsPage.DrawString "* indicates adjustments applied, see last page for details", 10,FooterStart+10
		  
		  'print page number
		  ResultsPage.Bold=true
		  OutputString= "Page: "+str(PageCount)
		  ResultsPage.DrawString OutputString,ResultsPage.Width-ResultsPage.StringWidth(OutputString)-10,FooterStart+10
		  
		  'print copyright stuff
		  ResultsPage.bold=false
		  ResultsPage.TextSize=8
		  OutputString="© 2005-2008 Milliseconds Computer Services, LLC"
		  ResultsPage.DrawString OutputString,ResultsPage.Width-ResultsPage.StringWidth(OutputString)-10,FooterStart+20
		  OutputString="801.582.3121/www.milliseconds.com"
		  ResultsPage.DrawString OutputString,ResultsPage.Width-ResultsPage.StringWidth(OutputString)-10,FooterStart+30
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintIntervalResults_Header(ByRef ResultsPage as Graphics, DivisionID as integer, DisplayFormat as string, ResultsName as string, PixelPosition as integer)
		  Dim rsDivsions as RecordSet
		  Dim SQLStatement, Totals as string
		  
		  PixelPosition=PixelPosition+10
		  
		  'Populate Results Name
		  ResultsPage.Bold=True
		  ResultsPage.TextSize=12
		  ResultsPage.DrawString ResultsName, 10, PixelPosition
		  
		  if DisplayFormat="Division" then
		    SQLStatement="SELECT rowid FROM participants WHERE DivisionID="+str(DivisionID)
		    rsDivsions=app.theDB.DBSQLSelect(SQLStatement)
		    Totals="Reg: "+Format(rsDivsions.RecordCount,"###,###")
		    
		    SQLStatement="SELECT rowid FROM participants WHERE DivisionID="+str(DivisionID)+" AND DNS='Y' AND DQ='N'"
		    rsDivsions=app.theDB.DBSQLSelect(SQLStatement)
		    Totals=Totals+" DNS: "+Format(rsDivsions.RecordCount,"###,###")
		    
		    SQLStatement="SELECT rowid FROM participants WHERE DivisionID="+str(DivisionID)+" AND DNS='N' AND DNF='Y' AND DQ='N'"
		    rsDivsions=app.theDB.DBSQLSelect(SQLStatement)
		    Totals=Totals+" DNF: "+Format(rsDivsions.RecordCount,"###,###")
		    
		    SQLStatement="SELECT rowid FROM participants WHERE DivisionID="+str(DivisionID)+" AND DQ='Y'"
		    rsDivsions=app.theDB.DBSQLSelect(SQLStatement)
		    Totals=Totals+" DQ: "+Format(rsDivsions.RecordCount,"###,###")
		    
		    ResultsPage.TextSize=8
		    ResultsPage.DrawString Totals, ResultsPage.Width-ResultsPage.StringWidth(Totals)-10, PixelPosition
		    rsDivsions.Close
		  end if
		  
		  PixelPosition=PixelPosition+10
		  
		  ResultsPage.TextSize=8
		  
		  ResultsPage.DrawString "PL", 15, PixelPosition, 25, True
		  ResultsPage.DrawString "No", 45, PixelPosition
		  ResultsPage.DrawString "Name", 70, PixelPosition
		  ResultsPage.DrawString "Representing", 205, PixelPosition
		  ResultsPage.DrawString "Interval Time", 380, PixelPosition
		  ResultsPage.DrawString "Total Time", 440, PixelPosition
		  ResultsPage.DrawString "Back", 510, PixelPosition
		  ResultsPage.DrawString "Pace", 550, PixelPosition
		  ResultsPage.Bold=False
		  
		  resultspage.PenWidth=1
		  ResultsPage.DrawLine 10, PixelPosition+2, 580, PixelPosition+2
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintIntervalResults_PageHeader(ByRef ResultsPage as Graphics)
		  'Populate Race Name
		  ResultsPage.TextSize=18
		  ResultsPage.Bold=True
		  ResultsPage.DrawString wnd_List.RaceName.Text, 10, 20
		  ResultsPage.TextSize=8
		  ResultsPage.Bold=False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintIntResults_Detail(ByRef ResultsPage as Graphics, PageLine as integer, RaceDistanceID as string, DivisionID as string, RacerNumber as string, RacerName as String, RacerAge as string, RacerGender as string, RacerRepresenting as string, RacerDivision as string, RacerNetTime as string, RacerTotalTime as string, RacerBackTime as string, RacerPace as string, ParticipantID as String, LapsCompleted as string)
		  dim i, ColPixelCount as Integer
		  dim rsIntRecord, rsIntervals as RecordSet
		  dim IntervalTime,SQLStatement, LastActualTime, PaceString, Place, SearchTime as string
		  
		  SearchTime=Replace(RacerTotalTime,"*","")
		  
		  if cbRound.Value then
		    RacerTotalTime=left(app.ConvertSecondsToTime(round(app.ConvertTimeToSeconds(RacerTotalTime)),false),8)
		    RacerNetTime=left(app.ConvertSecondsToTime(round(app.ConvertTimeToSeconds(RacerNetTime)),false),8)
		  end if
		  
		  RacerTotalTime=app.StripLeadingZeros(RacerTotalTime)
		  RacerNetTime=app.StripLeadingZeros(RacerNetTime)
		  
		  
		  if LineCount mod 2 = 1 then
		    ResultsPage.ForeColor=&cDDDDDD
		    ResultsPage.FillRect 10, PageLine-7, ResultsPage.Width-15, 9
		    ResultsPage.ForeColor=&c000000
		  end if
		  
		  if  ((app.Registered) or (not(app.Registered) and LineCount mod 3 = 0)) then
		    
		    'if Overall list all Rankings
		    if InStr(puFormat.Text,"Overall")>0 then
		      
		      if puFormat.Text="Overall By Distance" then
		        Place=app.GetPlace("Overall",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		        ResultsPage.DrawString Place, 10+(12-ResultsPage.StringWidth(Place)/2), PageLine
		        ResultsPage.DrawString "/", 35, PageLine
		        
		        Place=app.GetPlace("Gender",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		        ResultsPage.DrawString Place, 40+(12-ResultsPage.StringWidth(Place)/2), PageLine
		        ResultsPage.DrawString "/", 65, PageLine
		        
		        Place=app.GetPlace("Division",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		        ResultsPage.DrawString Place, 70+(12-ResultsPage.StringWidth(Place)/2), PageLine
		      else
		        Place=app.GetPlace("Gender",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		        ResultsPage.DrawString Place, 10+(12-ResultsPage.StringWidth(Place)/2), PageLine
		        ResultsPage.DrawString "/", 35, PageLine
		        
		        Place=app.GetPlace("Overall",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		        ResultsPage.DrawString Place, 40+(12-ResultsPage.StringWidth(Place)/2), PageLine
		        ResultsPage.DrawString "/", 65, PageLine
		        
		        Place=app.GetPlace("Division",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		        ResultsPage.DrawString Place, 70+(12-ResultsPage.StringWidth(Place)/2), PageLine
		      end if
		      
		      ResultsPage.DrawString RacerNumber, 100+(12-ResultsPage.StringWidth(RacerNumber)/2), PageLine
		      ResultsPage.DrawString RacerName, 130, PageLine, 87, True
		      ResultsPage.DrawString RacerRepresenting, 220, PageLine, 65, True
		      ResultsPage.DrawString RacerDivision, 285, PageLine, 50, True
		      
		      
		    else 'else list just the division place
		      Place=app.GetPlace("Division",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		      ResultsPage.DrawString Place, 10+(12-ResultsPage.StringWidth(Place)/2), PageLine
		      
		      ResultsPage.DrawString RacerNumber, 40+(12-ResultsPage.StringWidth(RacerNumber)/2), PageLine
		      ResultsPage.DrawString RacerName, 70, PageLine, 155, True
		      ResultsPage.DrawString RacerRepresenting, 235, PageLine, 100, True
		      
		    end if
		    
		    ColPixelCount=335
		    for i=val(efLapsFrom.Text) to val(efLapsTo.Text)
		      SQLStatement="SELECT Interval_Time, Actual_Time FROM times WHERE Interval_Number="+str(i)+" AND ParticipantID="+ParticipantID+" AND Use_This_Passing='Y'"
		      rsIntRecord=app.theDB.DBSQLSelect(SQLStatement)
		      if rsIntRecord.RecordCount>0 then
		        IntervalTime=app.StripLeadingZeros(rsIntRecord.Field("Interval_Time").StringValue)
		        ResultsPage.DrawString IntervalTime, ColPixelCount+(25-ResultsPage.StringWidth(IntervalTime)/2), PageLine, 47, true
		        ColPixelCount=ColPixelCount+50
		        
		        SQLStatement="SELECT Interval_Name, Pace_Type, Actual_Distance FROM intervals WHERE DivisionID="+DivisionID+" AND Number="+str(i)+" ORDER BY Number ASC"
		        rsIntervals=app.theDB.DBSQLSelect(SQLStatement)
		        if not(rsIntervals.EOF) then
		          PaceString=app.CalculateIntPace(rsIntervals.Field("Pace_Type").StringValue,rsIntervals.Field("Actual_Distance").DoubleValue, rsIntRecord.Field("Interval_Time").StringValue)
		        else
		          SQLStatement="SELECT Interval_Name, Pace_Type, Actual_Distance FROM intervals WHERE DivisionID="+DivisionID+" AND Number=1 ORDER BY Number ASC"
		          rsIntervals=app.theDB.DBSQLSelect(SQLStatement)
		          if not(rsIntervals.EOF) then
		            PaceString=app.CalculateIntPace(rsIntervals.Field("Pace_Type").StringValue,rsIntervals.Field("Actual_Distance").DoubleValue, rsIntRecord.Field("Interval_Time").StringValue)
		          else
		            PaceString=""
		          end if
		        end if
		        
		        ResultsPage.DrawString PaceString, ColPixelCount+(25-ResultsPage.StringWidth(PaceString)/2), PageLine, 47, true
		        ColPixelCount=ColPixelCount+50
		        LastActualTime=rsIntRecord.Field("Actual_Time").StringValue
		      else
		        'if LastActualTime<>"" then
		        'SQLStatement="SELECT Actual_Time, Pace FROM times WHERE Interval_Number=9999 AND ParticipantID="+ParticipantID+" AND Use_This_Passing='Y'"
		        'rsIntRecord=app.theDB.DBSQLSelect(SQLStatement)
		        'if rsIntRecord.RecordCount>0 then
		        'IntervalTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(rsIntRecord.Field("Actual_Time").StringValue)-app.ConvertTimeToSeconds(LastActualTime),false)
		        'IntervalTime=app.StripLeadingZeros(IntervalTime)
		        'ColPixelCount=ColPixelCount+50
		        '
		        'PaceString=app.StripLeadingZeros(rsIntRecord.Field("Pace").StringValue)
		        'ResultsPage.DrawString PaceString, ColPixelCount+(25-ResultsPage.StringWidth(PaceString)/2), PageLine, 47, true
		        'ColPixelCount=ColPixelCount+50
		        'else
		        'IntervalTime=""
		        'end if
		        'else
		        'IntervalTime=""
		        'end if
		        'ResultsPage.DrawString IntervalTime, ColPixelCount+(25-ResultsPage.StringWidth(IntervalTime)/2), PageLine, 47, true
		        'ColPixelCount=ColPixelCount+50
		        'PaceString=app.StripLeadingZeros(rsIntRecord.Field("Pace").StringValue)
		        'ResultsPage.DrawString PaceString, ColPixelCount+(25-ResultsPage.StringWidth(PaceString)/2), PageLine, 47, true
		        'ColPixelCount=ColPixelCount+50
		        ColPixelCount=ColPixelCount+100
		        LastActualTime=""
		      end if
		    next
		    
		    ResultsPage.DrawString RacerTotalTime, ColPixelCount+(25-ResultsPage.StringWidth(RacerTotalTime)/2), PageLine, 47, true
		    ColPixelCount=ColPixelCount+50
		    ResultsPage.DrawString RacerBackTime, ColPixelCount+(25-ResultsPage.StringWidth(RacerBackTime)/2), PageLine, 50, true
		    ColPixelCount=ColPixelCount+35
		    if app.RacersPerStart=9998 then
		      ResultsPage.DrawString LapsCompleted, ColPixelCount+(25-ResultsPage.StringWidth(RacerPace)/2), PageLine, 50, true
		    else
		      ResultsPage.DrawString RacerPace, ColPixelCount+(25-ResultsPage.StringWidth(RacerPace)/2), PageLine, 50, true
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintIntResults_Header(ByRef ResultsPage as Graphics, DivisionID as Integer, DisplayFormat as string, ResultsName as string, PixelPosition as integer)
		  dim rsIntervals as RecordSet
		  dim SelectStatement as string
		  dim IntervalName() as string
		  dim i, NumIntervals, ColPixelCount as integer
		  
		  if app.RacersPerStart<>9998 then
		    
		    SelectStatement=SelectStatement+"SELECT Interval_Name, Actual_Distance, Number, Pace_Type FROM intervals WHERE DivisionID="+str(DivisionID)+" ORDER BY Number ASC"
		    rsIntervals=app.theDB.DBSQLSelect(SelectStatement)
		    
		    Redim IntervalName(rsIntervals.RecordCount)
		    Redim IntActualDistance(rsIntervals.RecordCount)
		    Redim IntNumber(rsIntervals.RecordCount)
		    Redim IntPaceType(rsIntervals.RecordCount)
		    
		    for i=0 to rsIntervals.RecordCount-1
		      IntervalName(i)=""
		      IntActualDistance(i)=0
		      IntNumber(i)="0"
		      IntPaceType(i)=""
		    next
		    
		    for i = 1 to rsIntervals.RecordCount
		      IntervalName(i-1)=rsIntervals.Field("Interval_Name").StringValue
		      IntActualDistance(i-1)=rsIntervals.Field("Actual_Distance").DoubleValue
		      IntNumber(i-1)=rsIntervals.Field("Number").StringValue
		      IntPaceType(i-1)=rsIntervals.Field("Pace_Type").StringValue
		      rsIntervals.MoveNext
		    next
		    NumIntervals=val(efLapsTo.text)-val(efLapsFrom.Text)+1
		  else
		    
		    NumIntervals=val(efLapsTo.text)-val(efLapsFrom.Text)+1
		    if NumIntervals>9 then
		      NumIntervals=9
		    end if
		    
		    Redim IntervalName(NumIntervals)
		    Redim IntActualDistance(NumIntervals)
		    Redim IntNumber(NumIntervals)
		    Redim IntPaceType(NumIntervals)
		    
		    for i = 1 to NumIntervals
		      IntervalName(i-1)="Lap "+Str(val(efLapsFrom.Text)+i-1)
		      IntActualDistance(i-1)=0
		      IntNumber(i-1)=Str(val(efLapsFrom.Text)+i-1)
		      IntPaceType(i-1)="none"
		    next
		  end if
		  
		  PixelPosition=PixelPosition+10
		  
		  'Populate Results Name
		  ResultsPage.Bold=True
		  ResultsPage.TextSize=12
		  ResultsPage.DrawString ResultsName, 10, PixelPosition
		  
		  PixelPosition=PixelPosition+10
		  
		  ResultsPage.TextSize=8
		  select case DisplayFormat
		  case "Overall By Distance"
		    ResultsPage.DrawString "OvrAll", 10, PixelPosition, 25, True
		    ResultsPage.DrawString "/", 35, PixelPosition
		    ResultsPage.DrawString "Gndr", 40, PixelPosition, 25, True
		    ResultsPage.DrawString "/", 65, PixelPosition
		    ResultsPage.DrawString "Div", 73, PixelPosition, 25, True
		    ResultsPage.DrawString "No", 105, PixelPosition
		    ResultsPage.DrawString "Name", 130, PixelPosition, 90, true
		    ResultsPage.DrawString "Representing", 220, PixelPosition, 65, true
		    ResultsPage.DrawString "Division", 285, PixelPosition, 75, true
		    
		  case "Overall By Gender"
		    ResultsPage.DrawString "Gndr", 10, PixelPosition, 25, True
		    ResultsPage.DrawString "/", 35, PixelPosition
		    ResultsPage.DrawString "OvrAll", 40, PixelPosition, 25, True
		    ResultsPage.DrawString "/", 65, PixelPosition
		    ResultsPage.DrawString "Div", 73, PixelPosition, 25, True
		    ResultsPage.DrawString "No", 105, PixelPosition
		    ResultsPage.DrawString "Name", 130, PixelPosition, 90, true
		    ResultsPage.DrawString "Representing", 220, PixelPosition, 65, true
		    ResultsPage.DrawString "Division", 285, PixelPosition, 45, true
		    
		  else
		    ResultsPage.DrawString "PL", 15, PixelPosition, 25, True
		    ResultsPage.DrawString "No", 45, PixelPosition
		    ResultsPage.DrawString "Name", 70, PixelPosition
		    ResultsPage.DrawString "Representing", 235, PixelPosition, 50, true
		    
		  end select
		  
		  ColPixelCount=335
		  for i=0 to NumIntervals-1
		    ResultsPage.DrawString IntervalName(i), ColPixelCount+(25-ResultsPage.StringWidth(IntervalName(i))/2), PixelPosition, 48, true
		    ColPixelCount=ColPixelCount+50
		    ResultsPage.DrawString IntPaceType(i), ColPixelCount+(25-ResultsPage.StringWidth(IntPaceType(i))/2), PixelPosition, 48, true
		    ColPixelCount=ColPixelCount+50
		  next
		  
		  ResultsPage.DrawString "Total Time", ColPixelCount, PixelPosition
		  ColPixelCount=ColPixelCount+65
		  ResultsPage.DrawString "Back", ColPixelCount, PixelPosition
		  ColPixelCount=ColPixelCount+35
		  if app.RacersPerStart=9998 then
		    ResultsPage.DrawString "Laps", ColPixelCount, PixelPosition
		  else
		    ResultsPage.DrawString "Pace", ColPixelCount, PixelPosition
		  end if
		  
		  ResultsPage.Bold=False
		  
		  resultspage.PenWidth=1
		  ResultsPage.DrawLine 10, PixelPosition+2, ResultsPage.Width, PixelPosition+2
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function PrintMissingTxList() As boolean
		  dim PageLength, PixelCount as integer
		  dim RegPage as Graphics
		  dim ParticipantName, SelectStatement as String
		  dim rsQueryResults as RecordSet
		  Dim PageSetup as PrinterSetup
		  
		  if app.PrinterSettings<> "" then
		    PageSetup= New PrinterSetup
		    PageSetup.SetupString=app.PrinterSettings
		  end if
		  
		  RegPage = OpenPrinterDialog()
		  
		  if RegPage<>nil then
		    
		    PageLength=RegPage.Height
		    PixelCount=95
		    PageCount=0
		    LineCount=0
		    ColumnNumber=1
		    
		    PrintMissingTx_FirstPageHeader(RegPage)
		    PrintMissingTx_Header(RegPage,55)
		    
		    
		    SelectStatement="SELECT participants.Racer_Number, participants.Participant_Name, participants.First_Name, transponders.TX_Code, participants.Representing "
		    SelectStatement=SelectStatement+"FROM transponders JOIN participants ON transponders.Racer_Number = participants.Racer_Number " 
		    SelectStatement=SelectStatement+"WHERE transponders.Returned IS NULL OR transponders.Returned='0000-00-00 00:00:00' OR transponders.Returned='0-00-00 00:00:00'"
		    if rbNameSort.value then
		      SelectStatement=SelectStatement+"ORDER BY participants.Participant_Name ASC, participants.First_Name ASC"
		    else
		      SelectStatement=SelectStatement+"ORDER BY participants.Racer_Number ASC, participants.Participant_Name ASC, participants.First_Name ASC"
		    end if
		    
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		    
		    if not (rsQueryResults.EOF) then
		      
		      while not (rsQueryResults.EOF)
		        'print header
		        if ((PixelCount+LineCount*10>PageLength-50) and (ColumnNumber=2)) Then
		          PrintMissingTx_Footer(RegPage)
		          RegPage.NextPage
		          PixelCount=25
		          LineCount=0
		          ColumnNumber=1
		          PrintResults_PageHeader(RegPage, nil) ' this is on purpose... I want to use this header
		          PrintMissingTx_Header(RegPage,PixelCount)
		          PixelCount=PixelCount+35
		        end if
		        
		        'print detail lines
		        if rsQueryResults.Field("First_Name").stringValue <> "" then
		          ParticipantName=rsQueryResults.Field("Participant_Name").stringValue+", "+rsQueryResults.Field("First_Name").stringValue
		        else
		          ParticipantName=rsQueryResults.Field("Participant_Name").stringValue
		        end if
		        
		        if (((LineCount>62) and (ColumnNumber=1) and (PageCount=0))  or ((LineCount>65) and (ColumnNumber=1) and (PageCount>0))) then
		          LineCount=0
		          ColumnNumber=2
		        end if
		        
		        PrintMissingTx_Detail(RegPage, rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, _
		        rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("TX_Code").stringValue, _
		        PixelCount+LineCount*10, ColumnNumber)
		        
		        rsQueryResults.moveNext
		        
		        LineCount=LineCount+1
		      wend
		      rsQueryResults.Close
		      
		      
		    end if 'if not (rsQueryResults.EOF) then
		    
		    'print footer
		    if LineCount>0 then
		      PrintMissingTx_Footer(RegPage)
		    end if  ' if LineCount>0 then
		    
		    return true
		  else
		    return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintMissingTx_Detail(ByRef RegPage as Graphics, RacerNumber as string, RacerName as String, RacerRepresenting as string, RacerTransponder as string, PageLine as integer, ColumnNumber as integer)
		  
		  regpage.TextSize=6
		  if ColumnNumber=1 then
		    if LineCount mod 2 = 1 then
		      RegPage.ForeColor=&cDDDDDD
		      RegPage.FillRect 10, PageLine-7, RegPage.Width/2, 9
		      RegPage.ForeColor=&c000000
		    end if
		    
		    RegPage.DrawString RacerNumber, 10+(12-RegPage.StringWidth(RacerNumber)/2), PageLine
		    RegPage.DrawString RacerName, 40, PageLine, 90, True
		    RegPage.DrawString RacerRepresenting, 135, PageLine, 70, True
		    RegPage.DrawString RacerTransponder, 210, PageLine, 75, True
		  else
		    if LineCount mod 2 = 1 then
		      RegPage.ForeColor=&cDDDDDD
		      RegPage.FillRect  RegPage.Width/2, PageLine-7, RegPage.Width-15, 9
		      RegPage.ForeColor=&c000000
		    end if
		    RegPage.DrawString RacerNumber, 300+(12-RegPage.StringWidth(RacerNumber)/2), PageLine
		    RegPage.DrawString RacerName, 330, PageLine, 90, True
		    RegPage.DrawString RacerRepresenting, 415, PageLine, 70, True
		    RegPage.DrawString RacerTransponder, 510, PageLine, 75, True
		  end if
		  regpage.TextSize=8
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintMissingTX_FirstPageHeader(ByRef RegPage as Graphics)
		  dim CurrentDate as new date
		  dim HalfWidth,Position as integer
		  dim DateTime, SortType as string
		  
		  HalfWidth=RegPage.Width/2
		  DateTime=CurrentDate.LongDate+" "+CurrentDate.LongTime
		  
		  'Populate Race Name
		  RegPage.TextSize=18
		  RegPage.Bold=True
		  RegPage.DrawString wnd_List.RaceName.Text, 10, 20
		  
		  'Populate Report Name
		  RegPage.TextSize=18
		  RegPage.Bold=True
		  Position=HalfWidth-(RegPage.StringWidth("Missing Transponder List")/2)
		  RegPage.DrawString "Missing Transponder List", Position, 40
		  
		  'Populate Sort Type
		  RegPage.TextSize=8
		  RegPage.Bold=False
		  if rbNameSort.Value then
		    SortType="Sorted by Name"
		  else
		    SortType="Sorted by Racer Number"
		  end if
		  Position=HalfWidth-(RegPage.StringWidth(SortType)/2)
		  RegPage.DrawString SortType, Position, 55
		  
		  'Populate Date and Time
		  Position=HalfWidth-(RegPage.StringWidth(DateTime)/2)
		  RegPage.DrawString DateTime, Position, 65
		  
		  RegPage.TextSize=8
		  RegPage.Bold=False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintMissingTx_Footer(ByRef RegPage as Graphics)
		  dim OutputString as String
		  dim FooterStart as integer
		  
		  PageCount=PageCount+1
		  FooterStart=RegPage.Height-40
		  
		  'print line
		  RegPage.PenWidth=3
		  RegPage.DrawLine 10, FooterStart, 580, FooterStart
		  
		  'print page number
		  RegPage.Bold=true
		  OutputString= "Page: "+str(PageCount)
		  RegPage.DrawString OutputString,RegPage.Width-RegPage.StringWidth(OutputString)-10,FooterStart+10
		  
		  'print copyright stuff
		  RegPage.bold=false
		  RegPage.TextSize=8
		  OutputString="© 2008 Milliseconds Computer Services, LLC"
		  RegPage.DrawString OutputString,RegPage.Width-RegPage.StringWidth(OutputString)-10,FooterStart+20
		  OutputString="801.582.3121/www.milliseconds.com"
		  RegPage.DrawString OutputString,RegPage.Width-RegPage.StringWidth(OutputString)-10,FooterStart+30
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintMissingTx_Header(ByRef RegPage as Graphics, PixelPosition as integer)
		  PixelPosition=PixelPosition+10
		  
		  'Populate Results Name
		  RegPage.Bold=True
		  PixelPosition=PixelPosition+10
		  
		  RegPage.TextSize=8
		  RegPage.DrawString "No", 10, PixelPosition, 25, True
		  RegPage.DrawString "Name", 40, PixelPosition, 90, True
		  RegPage.DrawString "Representing", 125, PixelPosition, 90, True
		  RegPage.DrawString "TX Code", 210, PixelPosition,75, true
		  
		  RegPage.DrawString "No", 300, PixelPosition, 25, True
		  RegPage.DrawString "Name", 330, PixelPosition, 90, True
		  RegPage.DrawString "Representing", 415, PixelPosition, 90, True
		  RegPage.DrawString "TX Code", 510, PixelPosition,75, true
		  RegPage.Bold=False
		  
		  RegPage.PenWidth=3
		  RegPage.DrawLine 10, PixelPosition+2, 580, PixelPosition+2
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PrintPenaltyPoints(ByRef ResultsPage as Graphics, RaceDistanceID as string, Gender as String, ReportType as String)
		  dim i, pixelCount, racerPlace() as Integer
		  dim racerCountry(), racerDivisionName(), racerLicenseNumber(), racerNumber(), racerName(), racerRespresenting(), racerTotalTime(), racerUsedThesePoints() as string
		  dim racerPoints(), totalPoints as Double
		  dim rsQueryResults as RecordSet
		  dim OutString, SelectStatement as String
		  dim PageSetup as PrinterSetup
		  
		  FISUSSAPenaltyPoints=0
		  
		  if ReportType=constFISScoring then
		    OutString="FIS Penalty Point Calculations"
		  else
		    OutString="USSA Penalty Point Calculations"
		  end if
		  
		  if RaceDistanceID="" then
		    RaceDistanceID=str(-1)
		  end if
		  
		  if ReportType=constFISScoring then
		    SelectStatement="SELECT Racer_Number, Participant_Name, First_Name, Representing, Country, Total_Time, NGB2_License_Number as NGBLicenseNumber, "
		    SelectStatement=SelectStatement+"NGB1_Points, NGB2_Points, Division_Name "
		    SelectStatement=SelectStatement+"FROM participants JOIN divisions ON DivisionID=divisions.rowid WHERE divisions.RaceDistanceID = "+RaceDistanceID
		    SelectStatement=SelectStatement+" AND NGB2_License_Number <> '' AND NGB2_Points <> 'inact' AND DNF = 'N'"
		    SelectStatement=SelectStatement+" ORDER BY Total_Time, NGB2_Points LIMIT 0,5"
		  else
		    SelectStatement="SELECT Racer_Number, Participant_Name, First_Name, Representing, Country, Total_Time, NGB_License_Number as NGBLicenseNumber, "
		    SelectStatement=SelectStatement+"NGB1_Points, NGB2_Points, Division_Name "
		    SelectStatement=SelectStatement+"FROM participants JOIN divisions ON DivisionID=divisions.rowid WHERE divisions.RaceDistanceID = "+RaceDistanceID
		    SelectStatement=SelectStatement+" AND ((NGB_License_Number <> '' AND NGB1_Points <> 'inact')"
		    SelectStatement=SelectStatement+" OR (NGB_License_Number = '' AND NGB2_License_Number <> '' AND NGB2_Points <> 'inact'))"
		    SelectStatement=SelectStatement+" AND DNF = 'N'"
		    SelectStatement=SelectStatement+" ORDER BY Total_Time, NGB1_Points LIMIT 0,5"
		  end if
		  
		  rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		  
		  if (rsQueryResults.RecordCount = 5) then
		    for i = 1 to 5
		      racerPlace.Append i
		      racerNumber.Append rsQueryResults.Field("Racer_Number").StringValue
		      racerName.Append rsQueryResults.Field("Participant_Name").StringValue+", "+rsQueryResults.Field("First_Name").StringValue
		      racerLicenseNumber.Append rsQueryResults.Field("NGBLicenseNumber").StringValue
		      racerRespresenting.Append rsQueryResults.Field("Representing").StringValue
		      racerDivisionName.Append rsQueryResults.Field("Division_Name").StringValue
		      racerCountry.Append rsQueryResults.Field("Country").StringValue
		      racerTotalTime.Append app.StripLeadingZeros(rsQueryResults.Field("Total_Time").StringValue)
		      if i=1 then
		        FISUSSAFastestTime=app.ConvertTimeToSeconds(rsQueryResults.Field("Total_Time").StringValue)
		      end if
		      
		      if ReportType=constFISScoring then
		        if rsQueryResults.Field("NGB2_Points").StringValue<>"active" then
		          racerPoints.Append rsQueryResults.Field("NGB2_Points").DoubleValue
		        else
		          racerPoints.Append 160
		        end if
		      else
		        'USSA racer
		        if rsQueryResults.Field("NGB1_Points").StringValue<>"active" and rsQueryResults.Field("NGB1_Points").StringValue<>"inact" then
		          racerPoints.Append rsQueryResults.Field("NGB1_Points").DoubleValue
		        else
		          if ((rsQueryResults.Field("Country").StringValue<>"USA") and (rsQueryResults.Field("Country").StringValue<>"")) then 'foreign athlete so use their FIS points if they have any.
		            if (val(rsQueryResults.Field("NGB2_Points").StringValue)>0) then
		              racerPoints.Append rsQueryResults.Field("NGB2_Points").DoubleValue
		            else
		              racerPoints.Append 160 ' if they don't use 160 points
		            end if
		          else
		            if Gender = "M" then 'not a foriegn racer so use the default points
		              racerPoints.Append 200
		            else
		              racerPoints.Append 250
		            end if
		          end if
		        end if
		        
		      end if
		      
		      racerUsedThesePoints.Append "N"
		      
		      rsQueryResults.MoveNext
		    next
		    
		    racerPoints.SortWith(racerPlace,racerNumber,racerName,racerLicenseNumber,racerRespresenting,racerCountry,racerDivisionName,racerTotalTime,racerUsedThesePoints)
		    
		    totalPoints=0
		    for i=0 to 2
		      totalPoints=totalPoints+racerPoints(i)
		      racerUsedThesePoints(i)="Y"
		    next
		    FISUSSAPenaltyPoints=round((totalPoints/3.75)*100)/100
		    
		    if ReportType=constFISScoring then
		      select case wnd_List.pmMinPoints.Text
		      case "WC, Olympics, WC"
		        'nop
		        
		      case "CC, NCAA Champs, AB, SuperTour"
		        if FISUSSAPenaltyPoints<15 then
		          FISUSSAPenaltyPoints=15
		        end if
		        
		      case "U23 WC"
		        if FISUSSAPenaltyPoints>25 then
		          FISUSSAPenaltyPoints=25
		        end if
		        
		      case "World Jr Champs"
		        if FISUSSAPenaltyPoints>35 then
		          FISUSSAPenaltyPoints=35
		        end if
		        
		      case "JOs"
		        'nop
		        
		      end select
		      
		      
		    else
		      select case wnd_List.pmMinPoints.Text
		      case "WC, Olympics, WC"
		        'nop
		        
		      case "CC, NCAA Champs, AB, SuperTour"
		        if FISUSSAPenaltyPoints<15 then
		          FISUSSAPenaltyPoints=15
		        end if
		        
		      case "US Champs"
		        if FISUSSAPenaltyPoints<15 then
		          FISUSSAPenaltyPoints=15
		        elseif FISUSSAPenaltyPoints>50 and Gender="M" then
		          FISUSSAPenaltyPoints=50
		        elseif FISUSSAPenaltyPoints>95 and Gender="F" then
		          FISUSSAPenaltyPoints=95
		        end if
		        
		      case "U23 WC"
		        if FISUSSAPenaltyPoints>25 then
		          FISUSSAPenaltyPoints=25
		        end if
		        
		      case "World Jr Champs"
		        if FISUSSAPenaltyPoints>35 then
		          FISUSSAPenaltyPoints=35
		        end if
		        
		      case "JOs"
		        'nop
		        
		      case "Regional, NCAA Regional"
		        if FISUSSAPenaltyPoints>60 and Gender="M" then
		          FISUSSAPenaltyPoints=60
		        elseif FISUSSAPenaltyPoints>85 and Gender="F" then
		          FISUSSAPenaltyPoints=85
		        end if
		        
		      end select
		    end if
		    
		    if cbPenaltyWorksheet.Value then
		      racerPlace.SortWith(racerPoints,racerNumber,racerName,racerLicenseNumber,racerRespresenting,racerCountry,racerDivisionName,racerTotalTime, racerUsedThesePoints)
		      
		      'now that we have calculated the penalty points, produce a report
		      if ResultsPage= nil then
		        PageSetup= New PrinterSetup
		        ResultsPage = OpenPrinterDialog(PageSetup)
		      end if
		      pixelCount=Print_FirstPageHeader(ResultsPage,nil,OutString)+20
		      
		      ResultsPage.DrawString "PL", 15, pixelCount, 25, True
		      ResultsPage.DrawString "No", 45, pixelCount
		      ResultsPage.DrawString "Name", 70, pixelCount
		      ResultsPage.DrawString "Nat", 170, pixelCount
		      ResultsPage.DrawString "License", 190, pixelCount
		      ResultsPage.DrawString "Representing", 250, pixelCount
		      ResultsPage.DrawString "Class", 400, pixelCount
		      ResultsPage.DrawString "Total Time", 475, pixelCount
		      ResultsPage.DrawString "Points", 545, pixelCount
		      ResultsPage.DrawLine 10, pixelCount+2, 580, pixelCount+2
		      
		      pixelCount=pixelCount+15
		      
		      for i = 0 to 4
		        if racerUsedThesePoints(i)="Y"  then 'high light the line
		          ResultsPage.ForeColor=&cDDDDDD
		          ResultsPage.FillRect 10, pixelCount-7, ResultsPage.Width-15, 9
		          ResultsPage.ForeColor=&c000000
		        end if
		        ResultsPage.DrawString str(racerPlace(i)), 15, pixelCount, 25, True
		        ResultsPage.DrawString racerNumber(i), 45, pixelCount
		        ResultsPage.DrawString racerName(i), 70, pixelCount
		        ResultsPage.DrawString racerCountry(i), 170, pixelCount
		        ResultsPage.DrawString racerLicenseNumber(i), 190, pixelCount
		        ResultsPage.DrawString racerRespresenting(i), 250, pixelCount
		        ResultsPage.DrawString racerDivisionName(i), 400, pixelCount
		        ResultsPage.DrawString racerTotalTime(i), 475, pixelCount
		        ResultsPage.DrawString str(racerPoints(i)), 545, pixelCount
		        
		        pixelCount=pixelCount+10
		      next
		      
		      pixelCount=pixelCount+100
		      
		      if ReportType=constFISScoring then
		        OutString="FIS Penalty Points: "+format(FISUSSAPenaltyPoints,"##0.00")
		      else
		        OutString="USSA Penalty Points: "+format(FISUSSAPenaltyPoints,"##0.00")
		      end if
		      ResultsPage.DrawString OutString , 70, pixelCount
		      
		      PrintResults_Footer(ResultsPage,nil)
		      ResultsPage.NextPage
		     end if
		    
		  else
		    if cbPenaltyWorksheet.value then
		      'not enough participants to score a race
		      if ResultsPage= nil then
		        PageSetup= New PrinterSetup
		        ResultsPage = OpenPrinterDialog(PageSetup)
		      end if
		      pixelCount=Print_FirstPageHeader(ResultsPage,nil,OutString)
		      
		      ResultsPage.DrawString "There are not enough active racers to score the race.", 40, 300
		      
		      PrintResults_Footer(ResultsPage,nil)
		      ResultsPage.NextPage
		      
		    end if
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function PrintPrizeDrawingList() As boolean
		  dim TimeDifference as double
		  dim i, ii, NumIntervals, PageLength, PixelCount, MinRacerNumber, MaxRacerNumber, ParticipantCount, RandomRacerNumbers(), NewRacerNumber as integer
		  dim PrizeDrawingList as Graphics
		  dim ParticipantName, SelectStatement as String
		  dim rsQueryResults as RecordSet
		  Dim PageSetup as PrinterSetup
		  dim TotalTime as string
		  Dim r as New Random
		  
		  if app.PrinterSettings<> "" then
		    PageSetup= New PrinterSetup
		    PageSetup.SetupString=app.PrinterSettings
		  end if
		  
		  PrizeDrawingList = OpenPrinterDialog()
		  
		  if PrizeDrawingList<>nil then
		    
		    PageLength=PrizeDrawingList.Height
		    PixelCount=95
		    PageCount=0
		    LineCount=0
		    ColumnNumber=1
		    
		    
		    
		    SelectStatement="SELECT MIN(Racer_Number), MAX(Racer_Number), COUNT(Racer_Number) FROM participants"
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		    if not (rsQueryResults.EOF) then
		      MinRacerNumber=rsQueryResults.Field("MIN(Racer_Number)").IntegerValue
		      MaxRacerNumber=rsQueryResults.Field("MAX(Racer_Number)").IntegerValue
		      ParticipantCount=rsQueryResults.Field("COUNT(Racer_Number)").IntegerValue
		    end if
		    
		    if ParticipantCount>0 then
		      'print the headers
		      PrintPrizeDrawingList_FirstPageHeader(PrizeDrawingList)
		      PrintPrizeDrawingList_Header(PrizeDrawingList,55)
		      
		      'get a random list of numbers
		      redim RandomRacerNumbers(ParticipantCount)
		      
		      for i = 0 to ParticipantCount-1
		        Do
		          Do
		            NewRacerNumber=r.InRange(MinRacerNumber,MaxRacerNumber)
		          Loop Until (RandomRacerNumbers.IndexOf(NewRacerNumber)<0)
		          SelectStatement="SELECT Racer_Number FROM participants WHERE Racer_Number="+str(NewRacerNumber)
		          rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		        Loop Until (not (rsQueryResults.EOF))
		        RandomRacerNumbers(i)=NewRacerNumber
		      next
		      
		      For Each RandomRacerNumber as Integer in RandomRacerNumbers
		        SelectStatement="SELECT participants.Racer_Number, participants.Participant_Name, participants.First_Name, divisions.Division_Name, participants.Representing, participants.Total_Time,  "
		        SelectStatement=SelectStatement+"participants.DNF, participants.DNS, participants.DQ "
		        SelectStatement=SelectStatement+"FROM participants JOIN divisions ON participants.DivisionID = divisions.rowid WHERE participants.Racer_Number="+Str(RandomRacerNumber)
		        rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		        
		        'print header
		        if (PixelCount+LineCount*10>PageLength-50)  Then
		          PrintPrizeDrawingList_Footer(PrizeDrawingList)
		          PrizeDrawingList.NextPage
		          PixelCount=25
		          LineCount=0
		          ColumnNumber=1
		          PrintResults_PageHeader(PrizeDrawingList, nil) ' this is on purpose... I want to use this header
		          PrintPrizeDrawingList_Header(PrizeDrawingList,PixelCount)
		          PixelCount=PixelCount+35
		        end if
		        
		        'print detail lines
		        if rsQueryResults.Field("First_Name").stringValue <> "" then
		          ParticipantName=rsQueryResults.Field("Participant_Name").stringValue+", "+rsQueryResults.Field("First_Name").stringValue
		        else
		          ParticipantName=rsQueryResults.Field("Participant_Name").stringValue
		        end if
		        
		        If rsQueryResults.Field("DQ").stringValue = "Y" then
		          TotalTime="DQ"
		          
		        elseif rsQueryResults.Field("DNS").stringValue = "Y" then
		          TotalTime="DNS"
		          
		        elseif rsQueryResults.Field("DNF").stringValue = "Y" then
		          TotalTime="DNF"
		          
		        else
		          TotalTime=rsQueryResults.Field("Total_Time").stringValue
		        end if
		        
		        PrintPrizeDrawingList_Detail(PrizeDrawingList, rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, _
		        rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("Division_Name").stringValue, _
		        TotalTime, PixelCount+LineCount*10)
		        
		        LineCount=LineCount+1
		      Next
		      
		      'print footer
		      if LineCount>0 then
		        PrintPrizeDrawingList_Footer(PrizeDrawingList)
		      end if  ' if LineCount>0 then
		      
		      rsQueryResults.Close
		    end if
		    return true
		  else
		    return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintPrizeDrawingList_Detail(ByRef PrizeDrawingList as Graphics, RacerNumber as string, RacerName as String, RacerRepresenting as string, RacerDivision as string, TotalTime as string, PageLine as integer)
		  Dim Pixels as integer
		  
		  if LineCount mod 2 = 1 then
		    PrizeDrawingList.ForeColor=&cDDDDDD
		    PrizeDrawingList.FillRect 10, PageLine-7, PrizeDrawingList.Width, 9
		    PrizeDrawingList.ForeColor=&c000000
		  end if
		  
		  
		  PrizeDrawingList.DrawString RacerNumber, 10+(12-PrizeDrawingList.StringWidth(RacerNumber)/2), PageLine
		  Pixels=50
		  
		  PrizeDrawingList.DrawString RacerName, Pixels, PageLine, 180, True
		  Pixels=Pixels+135
		  
		  PrizeDrawingList.DrawString RacerRepresenting, Pixels, PageLine, 180, True
		  Pixels=Pixels+210
		  
		  PrizeDrawingList.DrawString RacerDivision, Pixels, PageLine, 180, True
		  Pixels=Pixels+125
		  
		  PrizeDrawingList.DrawString TotalTime, Pixels+(12-PrizeDrawingList.StringWidth(RacerNumber)/2), PageLine
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintPrizeDrawingList_FirstPageHeader(ByRef PrizeDrawingList as Graphics)
		  dim CurrentDate as new date
		  dim HalfWidth,Position as integer
		  dim DateTime, SortType as string
		  
		  HalfWidth=PrizeDrawingList.Width/2
		  DateTime=CurrentDate.LongDate+" "+CurrentDate.LongTime
		  
		  'Populate Race Name
		  PrizeDrawingList.TextSize=18
		  PrizeDrawingList.Bold=True
		  PrizeDrawingList.DrawString wnd_List.RaceName.Text, 10, 20
		  
		  'Populate Report Name
		  PrizeDrawingList.TextSize=18
		  PrizeDrawingList.Bold=True
		  Position=HalfWidth-(PrizeDrawingList.StringWidth("Random Prize Drawing List")/2)
		  PrizeDrawingList.DrawString "Random Prize Drawing List", Position, 40
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintPrizeDrawingList_Footer(ByRef PrizeDrawingList as Graphics)
		  dim OutputString as String
		  dim FooterStart as integer
		  
		  PageCount=PageCount+1
		  FooterStart=PrizeDrawingList.Height-40
		  
		  'print line
		  PrizeDrawingList.PenWidth=3
		  PrizeDrawingList.DrawLine 10, FooterStart, 580, FooterStart
		  
		  'print page number
		  PrizeDrawingList.Bold=true
		  OutputString= "Page: "+str(PageCount)
		  PrizeDrawingList.DrawString OutputString,PrizeDrawingList.Width-PrizeDrawingList.StringWidth(OutputString)-10,FooterStart+10
		  
		  'print copyright stuff
		  PrizeDrawingList.bold=false
		  PrizeDrawingList.TextSize=8
		  OutputString="© 2008 Milliseconds Computer Services, LLC"
		  PrizeDrawingList.DrawString OutputString,PrizeDrawingList.Width-PrizeDrawingList.StringWidth(OutputString)-10,FooterStart+20
		  OutputString="801.582.3121/www.milliseconds.com"
		  PrizeDrawingList.DrawString OutputString,PrizeDrawingList.Width-PrizeDrawingList.StringWidth(OutputString)-10,FooterStart+30
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintPrizeDrawingList_Header(ByRef PrizeDrawingList as Graphics, PixelPosition as integer)
		  dim Pixels as Integer
		  
		  PixelPosition=PixelPosition+10
		  
		  'Populate Results Name
		  PrizeDrawingList.Bold=True
		  PixelPosition=PixelPosition+10
		  
		  PrizeDrawingList.TextSize=8
		  
		  
		  PrizeDrawingList.DrawString "No", 15, PixelPosition, 25, True
		  Pixels=50
		  
		  PrizeDrawingList.DrawString "Name", Pixels, PixelPosition, 90, True
		  Pixels=Pixels+135
		  
		  PrizeDrawingList.DrawString "Representing", Pixels, PixelPosition, 90, True
		  Pixels=Pixels+210
		  
		  PrizeDrawingList.DrawString "Division", Pixels, PixelPosition,75, true
		  Pixels=Pixels+125
		  
		  PrizeDrawingList.DrawString "Total Time", Pixels, PixelPosition, 75, True
		  
		  
		  PrizeDrawingList.Bold=False
		  
		  PrizeDrawingList.PenWidth=3
		  PrizeDrawingList.DrawLine 10, PixelPosition+2, 580, PixelPosition+2
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function PrintRegList() As boolean
		  dim PageLength, PixelCount as integer
		  dim RegPage as Graphics
		  dim ParticipantName, SelectStatement, OldDivisionName, OldRepresentingName as String
		  dim rsQueryResults as RecordSet
		  Dim PageSetup as PrinterSetup
		  
		  if app.PrinterSettings<> "" then
		    PageSetup= New PrinterSetup
		    PageSetup.SetupString=app.PrinterSettings
		  end if
		  
		  RegPage = OpenPrinterDialog()
		  
		  if RegPage<>nil then
		    
		    PageLength=RegPage.Height
		    if wnd_List.Logo1<>"" or wnd_List.Logo2<>"" or wnd_List.Logo3<>"" or wnd_List.Logo4<>"" then
		      PixelCount=210
		    else
		      PixelCount=60
		    end if
		    PageCount=0
		    LineCount=0
		    ColumnNumber=1
		    
		    PrintRegList_FirstPageHeader(RegPage)
		    PrintRegList_Header(RegPage,PixelCount)
		    
		    PixelCount=PixelCount+40
		    
		    SelectStatement="SELECT participants.TeamID, participants.Racer_Number, participants.Participant_Name, participants.First_Name, divisions.Division_Name, divisions.List_Order, participants.Representing, participants.Age "
		    SelectStatement=SelectStatement+"FROM participants JOIN divisions ON participants.DivisionID = divisions.rowid ORDER BY "
		    if rbNameSort.value then
		      SelectStatement=SelectStatement+"participants.Participant_Name ASC, participants.First_Name ASC, participants.Representing ASC"
		    elseif rbRacerNumberSort.Value then
		      SelectStatement=SelectStatement+"participants.Racer_Number ASC, participants.Participant_Name ASC, participants.First_Name ASC, participants.Representing ASC"
		    elseif rbDivisionSort.Value then
		      SelectStatement=SelectStatement+"divisions.Division_Name, participants.Participant_Name ASC, participants.First_Name ASC, participants.Representing ASC"
		    elseif rbDivisionRepresenting.Value then
		      SelectStatement=SelectStatement+"divisions.List_Order ASC, participants.Representing ASC, participants.Participant_Name ASC, participants.First_Name ASC, participants.Representing ASC"
		    elseif rbRepresentingDivision.Value then
		      SelectStatement=SelectStatement+"participants.Representing ASC, divisions.List_Order ASC, participants.Participant_Name ASC, participants.First_Name ASC, participants.Representing ASC"
		    elseif rbStartTime.Value then
		      SelectStatement=SelectStatement+"participants.Assigned_Start ASC participants.Racer_Number ASC"
		    end if
		    
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		    
		    OldDivisionName=rsQueryResults.Field("Division_Name").StringValue
		    OldRepresentingName=rsQueryResults.Field("Representing").StringValue
		    
		    if not (rsQueryResults.EOF) then
		      
		      while not (rsQueryResults.EOF)
		        
		        if (OldDivisionName<>rsQueryResults.Field("Division_Name").StringValue and not(rbNameSort.Value) and not(rbRacerNumberSort.Value)) or _
		          (OldRepresentingName<>rsQueryResults.Field("Representing").StringValue and (rbDivisionRepresenting.Value or rbRepresentingDivision.Value)) then
		          
		          if (cbPageBreak.Value=true) then
		            PrintRegList_Footer(RegPage)
		            RegPage.NextPage
		            if wnd_List.Logo1<>"" or wnd_List.Logo2<>"" or wnd_List.Logo3<>"" or wnd_List.Logo4<>"" then
		              PixelCount=210
		            else
		              PixelCount=60
		            end if
		            LineCount=0
		            PrintRegList_FirstPageHeader(RegPage)
		            PrintRegList_Header(RegPage,PixelCount)
		            PixelCount=PixelCount+40
		          else
		            PixelCount=PixelCount+10
		            LineCount=LineCount+1
		          end if
		          
		          OldDivisionName=rsQueryResults.Field("Division_Name").StringValue
		          OldRepresentingName=rsQueryResults.Field("Representing").StringValue
		        end if
		        
		        'print header
		        if (PixelCount+LineCount*10>PageLength-40) and (cbPageBreak.Value=false) Then
		          PrintRegList_Footer(RegPage)
		          RegPage.NextPage
		          PixelCount=60
		          LineCount=0
		          ColumnNumber=1
		          PrintResults_PageHeader(RegPage,nil) ' this is on purpose... I want to use this header
		          PrintRegList_Header(RegPage,PixelCount)
		          PixelCount=PixelCount+35
		        end if
		        
		        'print detail lines
		        if rsQueryResults.Field("First_Name").stringValue <> "" then
		          ParticipantName=rsQueryResults.Field("Participant_Name").stringValue+", "+rsQueryResults.Field("First_Name").stringValue
		        else
		          ParticipantName=rsQueryResults.Field("Participant_Name").stringValue
		        end if
		        
		        
		        PrintRegList_Detail(RegPage, rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, _
		        rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("Division_Name").stringValue, _
		        rsQueryResults.Field("TeamID").StringValue, rsQueryResults.Field("Age").StringValue, PixelCount+LineCount*10, ColumnNumber)
		        
		        rsQueryResults.moveNext
		        
		        LineCount=LineCount+1
		      wend
		      rsQueryResults.Close
		      
		      
		    end if 'if not (rsQueryResults.EOF) then
		    
		    'print footer
		    if LineCount>0 then
		      PrintRegList_Footer(RegPage)
		    end if  ' if LineCount>0 then
		    
		    return true
		  else
		    return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintRegList_Detail(ByRef RegPage as Graphics,  RacerNumber as string, RacerName as String, RacerRepresenting as string, RacerDivision as string, TeamID as string, Age as string, PageLine as integer, ColumnNumber as integer)
		  if wnd_List.ResultType.Text="Cross Country Running" then
		    select case Age
		    case "1", "9", "13"
		      Age="FR"
		      
		    case "2", "10", "14"
		      Age="SO"
		      
		    case "3", "11", "15"
		      Age="JR"
		      
		    case "4", "12", "16"
		      Age="SR"
		      
		    else
		      Age="**"
		      
		    end Select
		  end if
		  
		  regpage.TextSize=8
		  if LineCount mod 2 = 1 then
		    RegPage.ForeColor=&cDDDDDD
		    RegPage.FillRect 10, PageLine-7, 580, 9
		    RegPage.ForeColor=&c000000
		  end if
		  
		  RegPage.DrawString RacerNumber, 10+(12-RegPage.StringWidth(RacerNumber)/2), PageLine
		  RegPage.DrawString RacerName, 40, PageLine, 150, True
		  RegPage.DrawString Age, 180, PageLine, 20, True
		  RegPage.DrawString RacerRepresenting, 200, PageLine, 150, True
		  RegPage.DrawString RacerDivision, 360, PageLine, 150, True
		  if TeamID<>"0" then
		    RegPage.DrawString "Declared", 520, PageLine, 50, True
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintRegList_FirstPageHeader(ByRef RegPage as Graphics)
		  dim CurrentDate as new date
		  dim HalfWidth,Position, PixelCount as integer
		  dim DateTime, SortType as string
		  
		  DateTime=CurrentDate.LongDate+" "+CurrentDate.LongTime
		  
		  
		  HalfWidth=RegPage.Width/2
		  
		  'Populate Logos
		  
		  if wnd_List.Logo1<>"" or wnd_List.Logo2<>"" or wnd_List.Logo3<>"" or wnd_List.Logo4<>"" then
		    if wnd_List.Logo1<>"" then
		      Print_DrawLogos(RegPage, wnd_List.Logo1, 10, 100, 100)
		    end if
		    
		    if wnd_List.Logo2<>"" then
		      Print_DrawLogos(RegPage, wnd_List.Logo2, 175, 100, 100)
		    end if
		    
		    if wnd_List.Logo3<>"" then
		      Print_DrawLogos(RegPage, wnd_List.Logo3, 320, 100, 100)
		    end if
		    
		    if wnd_List.Logo4<>"" then
		      Print_DrawLogos(RegPage, wnd_List.Logo4,450, 100, 100)
		    end if
		    
		    PixelCount=130
		  else
		    
		    PixelCount=20
		  end if
		  
		  'Populate Race Name
		  RegPage.TextSize=18
		  RegPage.Bold=True
		  Position=HalfWidth-(RegPage.StringWidth(wnd_List.RaceName.Text)/2)
		  RegPage.DrawString wnd_List.RaceName.Text, Position,PixelCount
		  
		  'Populate Report Name
		  RegPage.TextSize=18
		  RegPage.Bold=True
		  Position=HalfWidth-(RegPage.StringWidth("Registration List")/2)
		  RegPage.DrawString "Registration List", Position, PixelCount+20
		  
		  'Populate Sort Type
		  RegPage.TextSize=8
		  RegPage.Bold=False
		  if rbNameSort.Value then
		    SortType="Sorted by Name"
		  else
		    SortType="Sorted by Racer Number"
		  end if
		  
		  
		  if rbNameSort.value then
		    SortType="Sorted by Name"
		  elseif rbRacerNumberSort.Value then
		    SortType="Sorted by Racer Number"
		  elseif rbDivisionSort.Value then
		    SortType="Sorted by Age Divsion, Name"
		  elseif rbDivisionRepresenting.Value then
		    SortType="Sorted by Division, Representing, Name"
		  elseif rbRepresentingDivision.Value then
		    SortType="Sorted by Representing, Division, Name"
		  end if
		  
		  
		  Position=HalfWidth-(RegPage.StringWidth(SortType)/2)
		  RegPage.DrawString SortType, Position, PixelCount+35
		  
		  'Populate Date and Time
		  Position=HalfWidth-(RegPage.StringWidth(DateTime)/2)
		  RegPage.DrawString DateTime, Position, PixelCount+45
		  
		  RegPage.TextSize=8
		  RegPage.Bold=False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintRegList_Footer(ByRef RegPage as Graphics)
		  dim OutputString as String
		  dim FooterStart as integer
		  
		  PageCount=PageCount+1
		  FooterStart=RegPage.Height-40
		  
		  'print line
		  RegPage.PenWidth=3
		  RegPage.DrawLine 10, FooterStart, 580, FooterStart
		  
		  'print page number
		  RegPage.Bold=true
		  OutputString= "Page: "+str(PageCount)
		  RegPage.DrawString OutputString,RegPage.Width-RegPage.StringWidth(OutputString)-10,FooterStart+10
		  
		  'print copyright stuff
		  RegPage.bold=false
		  RegPage.TextSize=8
		  OutputString="© 2009 Milliseconds Computer Services, LLC"
		  RegPage.DrawString OutputString,RegPage.Width-RegPage.StringWidth(OutputString)-10,FooterStart+20
		  OutputString="801.582.3121/www.milliseconds.com"
		  RegPage.DrawString OutputString,RegPage.Width-RegPage.StringWidth(OutputString)-10,FooterStart+30
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintRegList_Header(ByRef RegPage as Graphics, PixelPosition as integer)
		  PixelPosition=PixelPosition+10
		  
		  'Populate Results Name
		  RegPage.Bold=True
		  PixelPosition=PixelPosition+10
		  
		  RegPage.TextSize=10
		  RegPage.DrawString "No", 10, PixelPosition, 25, True
		  RegPage.DrawString "Name", 40, PixelPosition, 90, True
		  RegPage.DrawString "Age", 175, PixelPosition, 20, True
		  RegPage.DrawString "Representing", 200, PixelPosition, 90, True
		  RegPage.DrawString "Division", 360, PixelPosition,75, true
		  
		  RegPage.PenWidth=3
		  RegPage.DrawLine 10, PixelPosition+2, 580, PixelPosition+2
		  RegPage.Bold=false
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function PrintResults(FinalResults as boolean, SelectionID() as string, Selection() as boolean, SelectionText() as string, ListIntervals as boolean, ListDNF as boolean, DisplayFormat as string, NonPrintedRacers as boolean, FromAutoPrint as boolean) As boolean
		  dim i, Old_i, PageLength, ParticipantCount, PixelCount, PlacesToPrint, PagesToPrint, Adjustment_DivisionID, AGDistIdx as integer
		  dim BackBase,PenaltyPoints as Double
		  dim ResultsPage as Graphics
		  dim NetTime, Back, Pace, ParticipantName, ResultsType, SelectStatement, TotalTime, AdjustmentText(), RaceDistanceID, FISUSSAScoring as String
		  dim NGB1Points, NGB2Points, OutString, SOCRaceCode as String
		  dim rsQueryResults,rsParticipant,rsDistance,rsBackBase,rsDivision as RecordSet
		  Dim PageSetup as PrinterSetup
		  Dim HTMLFile As FolderItem
		  Dim HTMLFileStream As TextOutputStream
		  
		  if not(HTMLResults) then
		    if app.PrinterSettings <> "" then
		      PageSetup= New PrinterSetup
		      PageSetup.SetupString=app.PrinterSettings
		    end if
		    
		    if not(FromAutoPrint) then
		      ResultsPage = OpenPrinterDialog(PageSetup)
		    else
		      'ResultsPage = OpenPrinterDialog(PageSetup)
		      ResultsPage = OpenPrinter(PageSetup)
		    end if
		    HTMLFile=Nil
		  else
		    ResultsPage=Nil
		    HTMLFile= GetSaveFolderItem("application/text","")
		  end if
		  
		  
		  if (ResultsPage<>nil and not(HTMLResults)) or (HTMLFile<>nil and HTMLResults)  then
		    if HTMLResults then
		      HTMLFileStream=HTMLFile.CreateTextFile
		      HTMLFileStream.WriteLine "<html>"
		      HTMLFileStream.WriteLine "<body style='font-family: monospace'>"
		    end if
		    
		    if Places.text="All" then
		      PlacesToPrint=999999
		    else
		      PlacesToPrint=val(Places.text)
		    end if
		    
		    if ResultsPage<>nil then
		      PageLength=ResultsPage.Height
		    else
		      PageLength=500
		    end if
		    Old_i=-1 ' force the first header
		    PageCount=0
		    LineCount=0
		    
		    if (puFormatType.text=constFISScoring or puFormatType.Text=constUSSAScoring) then
		      for i=0 to UBound(Selection)
		        if Selection(i) then
		          if (InStr(SelectionText(i),"Female")>0) then
		            PrintPenaltyPoints(ResultsPage,SelectionID(i),"F",puFormatType.Text)
		          else
		            PrintPenaltyPoints(ResultsPage,SelectionID(i),"M",puFormatType.Text)
		          end if
		        end if
		      next
		    else
		      PenaltyPoints=0
		    end if
		    
		    if app.ResultsDisplayType<>"Sea Otter" then
		      
		      if puFormatType.text=constFISScoring then
		        OutString="FIS Race Point Calculations "
		      elseif puFormatType.text=constUSSAScoring then
		        OutString="USSA Race Point Calculations "
		      else
		        OutString=""
		      end if
		      
		      if not(FinalResults) then
		        PixelCount=Print_FirstPageHeader(ResultsPage,HTMLFileStream,OutString+"Preliminary Results")
		      else
		        PixelCount=Print_FirstPageHeader(ResultsPage,HTMLFileStream,OutString+"Final Results")
		      end if
		    end if
		    
		    for i=0 to UBound(Selection)
		      if Selection(i) then
		        
		        if app.ResultsDisplayType="Sea Otter" then
		          if DisplayFormat="Division" then
		            SelectStatement = "SELECT Import_Name FROM Divisions WHERE rowid="+SelectionID(i)
		            rsDivision=app.theDB.DBSQLSelect(SelectStatement)
		            SOCRaceCode=rsDivision.Field("Import_Name").StringValue
		          end if
		          PixelCount=PrintSeaOtterResults_FirstPageHeader(ResultsPage,SOCRaceCode,SelectionText(i))
		        end if
		        
		        if ((Left(app.ResultsDisplayType,6)="Normal") or (app.ResultsDisplayType=wnd_List.constFISUSSAResultType) or (app.ResultsDisplayType=wnd_List.constCrossCountryResultType)) then
		          if not(ListIntervals) then
		            SelectStatement="SELECT A.rowid AS ParticipantID, A.TeamID, A.Racer_Number, "
		            SelectStatement=SelectStatement+"A.Participant_Name, A.First_Name, A.Age, A.Gender, A.NGB_License_Number, A.NGB2_License_Number, B.Division_Name, A.Representing, A.Country, "
		            SelectStatement=SelectStatement+"A.Net_Time, A.Total_Time, A.Age_Grade, B.rowid AS DivisionID, B.RaceDistanceID, "
		            SelectStatement=SelectStatement+"A.DNF, A.DNS, A.DQ, A.Laps_Completed, A.Total_Adjustment, A.Adjustment_Reason "
		            SelectStatement=SelectStatement+"FROM participants AS A INNER JOIN divisions AS B ON A.DivisionID = B.rowid "
		            SelectStatement=SelectStatement+"WHERE "
		          else
		            SelectStatement="SELECT A.rowid AS ParticipantID, A.TeamID, A.rowid AS ParticipantID, A.Racer_Number,  "
		            SelectStatement=SelectStatement+"A.Participant_Name, A.First_Name, A.Age, A.Gender, A.NGB_License_Number, A.NGB2_License_Number, B.Division_Name, A.Representing, "
		            SelectStatement=SelectStatement+"A.Net_Time, A.Total_Time, A.Age_Grade, B.rowid AS DivisionID,  B.RaceDistanceID, "
		            SelectStatement=SelectStatement+"A.DNF, A.DNS, A.DQ, A.Laps_Completed, A.Total_Adjustment, A.Adjustment_Reason "
		            SelectStatement=SelectStatement+"FROM participants AS A INNER JOIN divisions AS B ON A.DivisionID = B.rowid "
		            SelectStatement=SelectStatement+"WHERE "
		            
		          end if
		          
		        elseif app.ResultsDisplayType="Triathlon" then
		          SelectStatement="SELECT A.rowid AS ParticipantID, A.TeamID, A.Racer_Number, "
		          SelectStatement=SelectStatement+"A.Participant_Name, A.First_Name, A.Age, A.Gender, B.Division_Name, A.Representing, "
		          SelectStatement=SelectStatement+"A.Net_Time, A.Total_Time, B.rowid AS DivisionID, B.RaceDistanceID, "
		          SelectStatement=SelectStatement+"A.DNF, A.DNS, A.DQ, A.Laps_Completed, A.Total_Adjustment, A.Adjustment_Reason "
		          SelectStatement=SelectStatement+"FROM participants AS A INNER JOIN divisions AS B ON A.DivisionID = B.rowid "
		          SelectStatement=SelectStatement+"WHERE "
		          
		        elseif app.ResultsDisplayType="Sea Otter"then
		          SelectStatement="SELECT A.rowid AS ParticipantID, A.Racer_Number, A.Age, A.Gender, "
		          SelectStatement=SelectStatement+"A.Participant_Name, A.First_Name, A.Age, A.Gender, B.Division_Name, A.Representing, A.City, A.State, A.Country, A.NGB_License_Number, A.Laps_Completed, "
		          SelectStatement=SelectStatement+"A.Total_Time, A.Net_Time, B.rowid AS DivisionID, B.RaceDistanceID, A.DNF, A.DNS, A.DQ, A.Total_Adjustment, A.Adjustment_Reason "
		          SelectStatement=SelectStatement+"FROM participants AS A INNER JOIN divisions AS B ON A.DivisionID = B.rowid "
		          SelectStatement=SelectStatement+"WHERE "
		          
		        end if
		        
		        if not(ListDNF) then
		          SelectStatement=SelectStatement+"A.DNF = 'N' AND "
		        end if
		        
		        select case DisplayFormat
		        case "Overall By Distance"
		          SelectStatement=SelectStatement+"B.RaceDistanceID = "+SelectionID(i)
		          RaceDistanceID=SelectionID(i)
		          
		        case "Overall By Gender"
		          if (InStr(SelectionText(i),"Female")>0) then 'Gots to check for Female first because the male is part of Female and Male
		            SelectStatement=SelectStatement+"B.RaceDistanceID = "+SelectionID(i)+" AND A.Gender='F' "
		            
		          elseif (InStr(SelectionText(i),"Male")>0) then
		            SelectStatement=SelectStatement+"B.RaceDistanceID = "+SelectionID(i)+" AND A.Gender='M' "
		            
		          else
		            SelectStatement=SelectStatement+"B.RaceDistanceID = "+SelectionID(i)+" AND A.Gender='X' "
		            
		          end if
		          
		          if puFormatType.Text=constFISScoring then
		            SelectStatement=SelectStatement+" AND NGB2_License_Number <> '' AND NGB2_Points <> 'inact' "
		            FISUSSAScoring="FIS"
		          elseif puFormatType.Text=constUSSAScoring then
		            SelectStatement=SelectStatement+" AND ((NGB_License_Number <> '' AND NGB1_Points <> 'inact')"
		            SelectStatement=SelectStatement+" OR (NGB_License_Number = '' AND NGB2_License_Number <> '' AND NGB2_Points <> 'inact')) "
		            FISUSSAScoring="USSA"
		          end if
		          
		          
		          RaceDistanceID=SelectionID(i)
		          
		        else
		          SelectStatement=SelectStatement+"A.DivisionID = "+SelectionID(i)
		          rsDistance=app.theDB.DBSQLSelect("SELECT racedistances.rowid FROM divisions INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid WHERE divisions.rowid="+SelectionID(i))
		          RaceDistanceID=rsDistance.Field("rowid").StringValue
		          rsDistance.Close
		          
		        end select
		        
		        if NonPrintedRacers then
		          SelectStatement=SelectStatement+" AND A.Results_Printed IS NULL"
		        end if
		        
		        SelectStatement=SelectStatement+" ORDER BY A.DNS ASC, A.DNF ASC, A.DQ ASC, "
		        
		        
		        if app.RacersPerStart=9998 then
		          if not(rbSlowestToFastest.Value) then
		            SelectStatement=SelectStatement+" A.Laps_Completed DESC, "
		          else
		            SelectStatement=SelectStatement+" A.Laps_Completed ASC, "
		          end if
		        end if
		        
		        if PrintSetup.puFormatType.Text<>constAgeGraded then
		          if not(rbSlowestToFastest.Value) then
		            SelectStatement=SelectStatement+" A.Total_Time ASC, "
		          else
		            SelectStatement=SelectStatement+" A.Total_Time DESC, "
		          end if
		        else
		          if not(rbSlowestToFastest.Value) then
		            SelectStatement=SelectStatement+" A.Age_Grade DESC, "
		          else
		            SelectStatement=SelectStatement+" A.Age_Grade ASC, "
		          end if
		        end if
		        SelectStatement=SelectStatement+"A.Participant_Name ASC, A.First_Name ASC"
		        rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		        
		        if not (rsQueryResults.EOF) then
		          ParticipantCount=0
		          
		          'Determine places to print
		          if FromAutoPrint then
		            if wnd_List.AutoPrint_FirstPagePrinted then
		              PagesToPrint=rsQueryResults.RecordCount\64
		              if PagesToPrint > 0 then
		                PlacesToPrint=PagesToPrint*64
		              else
		                PlacesToPrint=0
		              end if
		            else
		              if rsQueryResults.RecordCount>61 and rsQueryResults.RecordCount<125 then
		                if (rsQueryResults.RecordCount\61)>0 then
		                  PlacesToPrint=61
		                end if
		              else
		                PagesToPrint=(rsQueryResults.RecordCount-61)\64
		                if PagesToPrint>0 then
		                  PlacesToPrint=PagesToPrint*64+61
		                else
		                  PlacesToPrint=0
		                end if
		              end if
		              
		            end if
		          end if
		          
		          while (not (rsQueryResults.EOF) and (ParticipantCount<PlacesToPrint))
		            Adjustment_DivisionID=rsQueryResults.Field("DivisionID").IntegerValue 'used when printing adjustments
		            if ParticipantCount=0 then
		              
		              
		              SelectStatement="SELECT Total_Time FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid WHERE "
		              
		              select case DisplayFormat
		              case "Overall By Distance"
		                SelectStatement=SelectStatement+"divisions.RaceDistanceID = "+rsQueryResults.Field("RaceDistanceID").StringValue
		                RaceDistanceID=SelectionID(i)
		                
		              case "Overall By Gender"
		                if (InStr(SelectionText(i),"Female")>0) then 'Gots to check for Female first because the male is part of Female and Male
		                  SelectStatement=SelectStatement+"divisions.RaceDistanceID = "+rsQueryResults.Field("RaceDistanceID").StringValue+" AND divisions.Gender='F' "
		                  
		                elseif (InStr(SelectionText(i),"Male")>0) then
		                  SelectStatement=SelectStatement+"divisions.RaceDistanceID = "+rsQueryResults.Field("RaceDistanceID").StringValue+" AND divisions.Gender='M' "
		                  
		                else
		                  SelectStatement=SelectStatement+"divisions.RaceDistanceID = "+rsQueryResults.Field("RaceDistanceID").StringValue+" AND divisions.Gender='X' "
		                  
		                end if
		                RaceDistanceID=SelectionID(i)
		                
		              else
		                SelectStatement=SelectStatement+"participants.DivisionID = "+rsQueryResults.Field("DivisionID").StringValue
		              end select
		              
		              SelectStatement=SelectStatement+" ORDER BY participants.DNS ASC, participants.DNF ASC, participants.DQ ASC, "
		              
		              if app.RacersPerStart=9998 then
		                SelectStatement=SelectStatement+" Laps_Completed DESC, "
		              end if
		              
		              if PrintSetup.puFormatType.Text<>constAgeGraded then
		                SelectStatement=SelectStatement+" Total_Time ASC "
		              else
		                SelectStatement=SelectStatement+" Age_Grade DESC "
		              end if
		              SelectStatement=SelectStatement+" LIMIT 0,1"
		              rsBackBase=app.theDB.DBSQLSelect(SelectStatement)
		              BackBase=app.ConvertTimeToSeconds(rsBackBase.Field("Total_Time").StringValue)
		              rsBackBase.Close
		              
		            end if
		            ParticipantCount=ParticipantCount+1
		            
		            'print header
		            if ((app.ResultsDisplayType<>"Sea Otter" and PixelCount+LineCount*10>PageLength-50) or _
		              (app.ResultsDisplayType="Sea Otter" and PixelCount+LineCount*10>PageLength-75)) Then
		              if app.ResultsDisplayType<>"Sea Otter" then
		                if not(HTMLResults) then
		                  PrintResults_Footer(ResultsPage,HTMLFileStream)
		                  ResultsPage.NextPage
		                end if
		                PixelCount=25
		                LineCount=0
		                if not(HTMLResults) then
		                  PrintResults_PageHeader(ResultsPage,HTMLFileStream)
		                end if
		              else
		                PrintSeaOtterResults_Footer(ResultsPage)
		                ResultsPage.NextPage
		                PixelCount=PrintSeaOtterResults_FirstPageHeader(ResultsPage,SOCRaceCode,SelectionText(i))
		                LineCount=0
		              end if
		              if Old_i = i then
		                if not(HTMLResults) then
		                  if ((Left(app.ResultsDisplayType,6)="Normal") or (app.ResultsDisplayType=wnd_List.constFISUSSAResultType) or (app.ResultsDisplayType=wnd_List.constCrossCountryResultType)) then
		                    if not(ListIntervals) then
		                      PrintResults_Header(ResultsPage,HTMLFileStream,rsQueryResults.Field("DivisionID").IntegerValue,DisplayFormat,DisplayFormat+": "+SelectionText(i)+" Continued",PixelCount)
		                    else
		                      PrintIntResults_Header(ResultsPage,rsQueryResults.Field("DivisionID").IntegerValue,DisplayFormat,DisplayFormat+": "+SelectionText(i)+" Continued",PixelCount)
		                    end if
		                  elseif app.ResultsDisplayType="Triathlon" then
		                    PrintTriResults_Header(ResultsPage,rsQueryResults.Field("DivisionID").IntegerValue,DisplayFormat,DisplayFormat+": "+SelectionText(i)+" Continued",PixelCount)
		                  elseif app.ResultsDisplayType="Sea Otter" then
		                    
		                    PrintSeaOtterResults_Header(ResultsPage,SelectionText(i)+" Continued",PixelCount)
		                  end if
		                end if
		              else
		                if ((Left(app.ResultsDisplayType,6)="Normal") or (app.ResultsDisplayType=wnd_List.constFISUSSAResultType) or (app.ResultsDisplayType=wnd_List.constCrossCountryResultType)) then
		                  if not(ListIntervals) then
		                    PrintResults_Header(ResultsPage,HTMLFileStream,rsQueryResults.Field("DivisionID").IntegerValue,DisplayFormat,DisplayFormat+": "+SelectionText(i),PixelCount)
		                  else
		                    PrintIntResults_Header(ResultsPage,rsQueryResults.Field("DivisionID").IntegerValue,DisplayFormat,DisplayFormat+": "+SelectionText(i),PixelCount)
		                  end if
		                elseif app.ResultsDisplayType="Triathlon" then
		                  PrintTriResults_Header(ResultsPage,rsQueryResults.Field("DivisionID").IntegerValue,DisplayFormat,DisplayFormat+": "+SelectionText(i),PixelCount)
		                elseif app.ResultsDisplayType="Sea Otter" then
		                  PrintSeaOtterResults_Header(ResultsPage,SelectionText(i),PixelCount)
		                end if
		              end if
		              PixelCount=PixelCount+35
		            else
		              if Old_i <> i then
		                if (app.ResultsDisplayType<>"Sea Otter" and (PixelCount+LineCount*10>PageLength-95) or (FromAutoPrint and Not(wnd_List.AutoPrint_FirstPagePrinted))) or _
		                  (app.ResultsDisplayType="Sea Otter" and (PixelCount+LineCount*10>PageLength-115) or (FromAutoPrint and Not(wnd_List.AutoPrint_FirstPagePrinted))) Then 'check to make sure it won't overflow the page, or if auto print need a page break between distances
		                  if app.ResultsDisplayType<>"Sea Otter" then
		                    PrintResults_Footer(ResultsPage,HTMLFileStream)
		                    if not(HTMLResults) then
		                      ResultsPage.NextPage
		                    end if
		                    PixelCount=25
		                    LineCount=0
		                    PrintResults_PageHeader(ResultsPage,HTMLFileStream)
		                  else
		                    PrintSeaOtterResults_Footer(ResultsPage)
		                    ResultsPage.NextPage
		                    PixelCount=PrintSeaOtterResults_FirstPageHeader(ResultsPage,SOCRaceCode,SelectionText(i))
		                    LineCount=0
		                  end if
		                end if
		                if ((Left(app.ResultsDisplayType,6)="Normal") or (app.ResultsDisplayType=wnd_List.constFISUSSAResultType) or (app.ResultsDisplayType=wnd_List.constCrossCountryResultType)) then
		                  if not(ListIntervals) then
		                    PrintResults_Header(ResultsPage,HTMLFileStream,rsQueryResults.Field("DivisionID").IntegerValue,DisplayFormat,DisplayFormat+": "+SelectionText(i),PixelCount+LineCount*10)
		                  else
		                    PrintIntResults_Header(ResultsPage,rsQueryResults.Field("DivisionID").IntegerValue,DisplayFormat,DisplayFormat+": "+SelectionText(i),PixelCount+LineCount*10)
		                  end if
		                elseif app.ResultsDisplayType="Triathlon" then
		                  PrintTriResults_Header(ResultsPage,rsQueryResults.Field("DivisionID").IntegerValue,DisplayFormat,DisplayFormat+": "+SelectionText(i),PixelCount+LineCount*10)
		                elseif app.ResultsDisplayType="Sea Otter" then
		                  PrintSeaOtterResults_Header(ResultsPage,SelectionText(i),PixelCount+LineCount*10)
		                end if
		                PixelCount=PixelCount+35
		                Old_i=i
		              end if
		            end if
		            
		            'print detail lines
		            if rsQueryResults.Field("First_Name").stringValue <> "" then
		              ParticipantName=rsQueryResults.Field("Participant_Name").stringValue+", "+rsQueryResults.Field("First_Name").stringValue
		            else
		              ParticipantName=rsQueryResults.Field("Participant_Name").stringValue
		            end if
		            
		            If rsQueryResults.Field("DNF").stringValue = "N" and rsQueryResults.Field("DNS").stringValue = "N" and (rsQueryResults.Field("DQ").stringValue = "N" or rsQueryResults.Field("DQ").stringValue = "") then
		              if PrintSetup.puFormatType.Text<>constAgeGraded then
		                NetTime=rsQueryResults.Field("Net_Time").stringValue
		              else
		                NetTime=rsQueryResults.Field("Age_Grade").stringValue
		              end if
		              if rsQueryResults.Field("Total_Adjustment").stringValue="+00:00:00.000" then
		                TotalTime=rsQueryResults.Field("Total_Time").stringValue
		              else
		                TotalTime=rsQueryResults.Field("Total_Time").stringValue+"*"
		                AdjustmentText.Append rsQueryResults.Field("Racer_Number").stringValue+" "+ParticipantName+"  "+rsQueryResults.Field("Total_Adjustment").stringValue+"  "+rsQueryResults.Field("Adjustment_Reason").stringValue
		              end if
		              
		              if cbRound.Value then
		                Back=app.StripLeadingZeros(left(app.ConvertSecondsToTime(round(app.ConvertTimeToSeconds(rsQueryResults.Field("Total_Time").stringValue)-BackBase),false),8))
		              else
		                Back=app.ConvertSecondsToTime((app.ConvertTimeToSeconds(rsQueryResults.Field("Total_Time").stringValue)-BackBase),false)
		                Select case app.PlacesToTruncate
		                case 0
		                  Back=left(Back,8)
		                case 1
		                  Back=left(Back,10)
		                case 2
		                  Back=left(Back,11)
		                end Select
		                Back="+"+app.StripLeadingZeros(Back)
		              end if
		              
		              Pace=app.CalculatePace(rsQueryResults.Field("RaceDistanceID").StringValue,rsQueryResults.Field("Total_Time").StringValue)
		              
		            elseif rsQueryResults.Field("DQ").stringValue = "Y" then
		              NetTime=""
		              TotalTime="DQ"
		              Back=""
		              Pace=""
		              AdjustmentText.Append rsQueryResults.Field("Racer_Number").stringValue+" "+ParticipantName+"  Disqualified:  "+rsQueryResults.Field("Adjustment_Reason").stringValue
		              
		            elseif rsQueryResults.Field("DNS").stringValue = "Y" then
		              NetTime=""
		              TotalTime="DNS"
		              Back=""
		              Pace=""
		              
		            else
		              NetTime=""
		              TotalTime="DNF"
		              Back=""
		              Pace=""
		            end if
		            
		            if ((Left(app.ResultsDisplayType,6)="Normal") or (app.ResultsDisplayType=wnd_List.constFISUSSAResultType) or (app.ResultsDisplayType=wnd_List.constCrossCountryResultType)) then
		              if not(ListIntervals)  then
		                PrintResults_Detail(ResultsPage,HTMLFileStream, PixelCount+LineCount*10, RaceDistanceID, rsQueryResults.Field("DivisionID").StringValue, rsQueryResults.Field("TeamID").StringValue, _
		                rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, rsQueryResults.Field("Age").stringValue, rsQueryResults.Field("Gender").stringValue, _
		                rsQueryResults.Field("NGB_License_Number").stringValue, NGB1Points, rsQueryResults.Field("NGB2_License_Number").stringValue, NGB2Points, _
		                rsQueryResults.Field("Country").stringValue, rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("Division_Name").stringValue, _
		                NetTime, TotalTime, Back, Pace, DataList.Cell(i,3), rsQueryResults.Field("Laps_Completed"), FISUSSAScoring)
		              else
		                if Left(TotalTime,1)<>"D" then
		                  PrintIntResults_Detail(ResultsPage, PixelCount+LineCount*10, RaceDistanceID, rsQueryResults.Field("DivisionID").stringValue, _
		                  rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, rsQueryResults.Field("Age").stringValue, rsQueryResults.Field("Gender").stringValue, _
		                  rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("Division_Name").stringValue, _
		                  NetTime, TotalTime, Back, Pace,rsQueryResults.Field("ParticipantID").stringValue, rsQueryResults.Field("Laps_Completed"))
		                else
		                  PrintIntResults_Detail(ResultsPage, PixelCount+LineCount*10, RaceDistanceID, rsQueryResults.Field("DivisionID").stringValue, _
		                  rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, rsQueryResults.Field("Age").stringValue, rsQueryResults.Field("Gender").stringValue, _
		                  rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("Division_Name").stringValue, _
		                  NetTime, TotalTime, Back, Pace, rsQueryResults.Field("ParticipantID"), rsQueryResults.Field("Laps_Completed"))
		                end if
		              end if
		            elseif app.ResultsDisplayType="Triathlon" then
		              
		              SelectStatement="SELECT A.Racer_Number, "
		              SelectStatement=SelectStatement+"C.Interval_Time AS IntOneTime, "
		              SelectStatement=SelectStatement+"D.Interval_Time AS IntTwoTime, E.Interval_Time AS IntThreeTime, "
		              SelectStatement=SelectStatement+"F.Interval_Time AS IntFourTime, G.Interval_Time AS IntFiveTime "
		              
		              SelectStatement=SelectStatement+"FROM participants AS A "
		              SelectStatement=SelectStatement+"LEFT JOIN times AS C ON A.rowid = C.ParticipantID AND C.Interval_Number = '"+str(IntNumber(0))+"'  AND C.Use_This_Passing='Y' "
		              SelectStatement=SelectStatement+"LEFT JOIN times AS D ON A.rowid = D.ParticipantID AND D.Interval_Number = '"+str(IntNumber(1))+"'  AND D.Use_This_Passing='Y' "
		              SelectStatement=SelectStatement+"LEFT JOIN times AS E ON A.rowid = E.ParticipantID AND E.Interval_Number = '"+str(IntNumber(2))+"'  AND E.Use_This_Passing='Y' "
		              SelectStatement=SelectStatement+"LEFT JOIN times AS F ON A.rowid = F.ParticipantID AND F.Interval_Number = '"+str(IntNumber(3))+"'  AND F.Use_This_Passing='Y' "
		              SelectStatement=SelectStatement+"LEFT JOIN times AS G ON A.rowid = G.ParticipantID AND G.Interval_Number = '"+str(IntNumber(4))+"'  AND G.Use_This_Passing='Y' "
		              SelectStatement=SelectStatement+"WHERE A.rowid="+rsQueryResults.Field("ParticipantID").StringValue
		              rsParticipant=app.theDB.DBSQLSelect(SelectStatement)
		              
		              PrintTriResults_Detail(ResultsPage, PixelCount+LineCount*10, RaceDistanceID, rsQueryResults.Field("DivisionID").stringValue, _
		              rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, rsQueryResults.Field("Age").stringValue, rsQueryResults.Field("Gender").stringValue,_
		              rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("Division_Name").stringValue, _
		              TotalTime, Back, Pace, app.StripLeadingZeros(rsParticipant.Field("IntOneTime")), rsParticipant.Field("IntOneTime"), _
		              app.StripLeadingZeros(rsParticipant.Field("IntTwoTime")), _ 
		              app.StripLeadingZeros(rsParticipant.Field("IntThreeTime")),  rsParticipant.Field("IntThreeTime"), _
		              app.StripLeadingZeros(rsParticipant.Field("IntFourTime")), _
		              app.StripLeadingZeros(rsParticipant.Field("IntFiveTime")), rsParticipant.Field("IntFiveTime"), _
		              app.StripLeadingZeros(rsQueryResults.Field("Total_Adjustment").stringValue))
		              
		            elseif app.ResultsDisplayType="Sea Otter" then
		              PrintSeaOtterResults_Detail(ResultsPage, PixelCount+LineCount*10, rsQueryResults.Field("DivisionID").StringValue, rsQueryResults.Field("Racer_Number").stringValue, rsQueryResults.Field("Participant_Name").stringValue, rsQueryResults.Field("First_Name").stringValue,_
		              rsQueryResults.Field("Gender").stringValue, rsQueryResults.Field("Age").stringValue, _
		              rsQueryResults.Field("City").stringValue+" "+rsQueryResults.Field("State").stringValue+" "+rsQueryResults.Field("Country").stringValue, _
		              rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("NGB_License_Number").stringValue, TotalTime, DataList.Cell(i,3) ,rsQueryResults.Field("Laps_Completed").stringValue )
		            end if
		            
		            'Update Participant Print Flag
		            SelectStatement="UPDATE participants SET Results_Printed='Y' WHERE rowid="+rsQueryResults.Field("ParticipantID").StringValue
		            App.theDB.DBSQLExecute(SelectStatement)
		            app.theDB.DBCommit
		            
		            rsQueryResults.moveNext
		            LineCount=LineCount+1
		            
		          wend
		          rsQueryResults.Close
		          
		          
		        end if 'if not (rsQueryResults.EOF) then
		        
		      end if 'if Selection(i) then
		    next
		    
		    'print footer
		    if LineCount>0 then
		      if not(HTMLResults) then
		        if app.ResultsDisplayType<>"Sea Otter" then
		          PrintResults_Footer(ResultsPage,HTMLFileStream)
		        else
		          PrintSeaOtterResults_Footer(ResultsPage)
		        end if
		      end if
		      
		      if UBound(AdjustmentText)>=0 then
		        AdjustmentText.Sort
		        if not(HTMLResults) then
		          if app.ResultsDisplayType<>"Sea Otter" then
		            PrintResults_Footer(ResultsPage,HTMLFileStream)
		            if not(HTMLResults) then
		              ResultsPage.NextPage
		            end if
		            PixelCount=25
		            LineCount=0
		            PrintResults_PageHeader(ResultsPage,HTMLFileStream)
		          else
		            PrintSeaOtterResults_Footer(ResultsPage)
		            ResultsPage.NextPage
		            PixelCount=PrintSeaOtterResults_FirstPageHeader(ResultsPage,SOCRaceCode,SelectionText(i))
		            LineCount=0
		          end if
		        end if
		        if ((Left(app.ResultsDisplayType,6)="Normal") or (app.ResultsDisplayType=wnd_List.constFISUSSAResultType) or (app.ResultsDisplayType=wnd_List.constCrossCountryResultType)) then
		          if not(ListIntervals) then
		            PrintResults_Header(ResultsPage,HTMLFileStream,Adjustment_DivisionID,DisplayFormat,DisplayFormat+": Adjustments",PixelCount)
		          else
		            PrintIntResults_Header(ResultsPage,Adjustment_DivisionID,DisplayFormat,DisplayFormat+": Adjustments",PixelCount)
		          end if
		        elseif app.ResultsDisplayType="Triathlon" then
		          PrintTriResults_Header(ResultsPage,Adjustment_DivisionID,DisplayFormat,DisplayFormat+": Adjustments",PixelCount)
		        elseif app.ResultsDisplayType="Sea Otter" then
		          PrintSeaOtterResults_Header(ResultsPage,DisplayFormat+": Adjustments",PixelCount)
		        end if
		        
		        PixelCount=PixelCount+35
		        
		        for i = 0 to UBound(AdjustmentText)
		          if PixelCount+LineCount*10>PageLength-50 Then
		            if not(HTMLResults) then
		              if app.ResultsDisplayType<>"Sea Otter" then
		                PrintResults_Footer(ResultsPage,HTMLFileStream)
		                if not(HTMLResults) then
		                  ResultsPage.NextPage
		                end if
		                PixelCount=25
		                LineCount=0
		                PrintResults_PageHeader(ResultsPage,HTMLFileStream)
		              else
		                PrintSeaOtterResults_Footer(ResultsPage)
		                ResultsPage.NextPage
		                PixelCount=PrintSeaOtterResults_FirstPageHeader(ResultsPage,SOCRaceCode,SelectionText(i))
		                LineCount=0
		              end if
		            end if
		            if ((Left(app.ResultsDisplayType,6)="Normal") or (app.ResultsDisplayType=wnd_List.constFISUSSAResultType) or (app.ResultsDisplayType=wnd_List.constCrossCountryResultType)) then
		              if not(ListIntervals) then
		                PrintResults_Header(ResultsPage,HTMLFileStream,Adjustment_DivisionID,DisplayFormat,DisplayFormat+": Adjustments",PixelCount)
		              else
		                PrintIntResults_Header(ResultsPage,Adjustment_DivisionID,DisplayFormat,DisplayFormat+": Adjustments",PixelCount)
		              end if
		            elseif app.ResultsDisplayType="Triathlon" then
		              PrintTriResults_Header(ResultsPage,Adjustment_DivisionID,DisplayFormat,DisplayFormat+": Adjustments",PixelCount)
		            elseif app.ResultsDisplayType="Sea Otter" then
		              PrintSeaOtterResults_Header(ResultsPage,DisplayFormat+": Adjustments",PixelCount)
		            end if
		            
		          else
		            PrintResults_Adjustments(ResultsPage,HTMLFileStream,PixelCount+LineCount*10,AdjustmentText(i))
		            LineCount=LineCount+1
		          end if
		        next
		        if app.ResultsDisplayType<>"Sea Otter" then
		          PrintResults_Footer(ResultsPage,HTMLFileStream)
		        else
		          PrintSeaOtterResults_Footer(ResultsPage)
		        end if
		      end if
		      
		    end if  ' if LineCount>0 then
		    
		    if HTMLResults then
		      PrintResults_Footer(ResultsPage,HTMLFileStream)
		      HTMLFileStream.WriteLine "</body></html>"
		      HTMLFileStream.Close
		    end if
		    
		    return true
		  else
		    return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintResults_Adjustments(ByRef ResultsPage as Graphics, HTMLFile as TextOutputStream, PageLine as integer, AdjustmentReason as string)
		  
		  if not(HTMLResults) then
		    if LineCount mod 2 = 1 then
		      ResultsPage.ForeColor=&cDDDDDD
		      ResultsPage.FillRect 10, PageLine-7, ResultsPage.Width-15, 9
		      ResultsPage.ForeColor=&c000000
		    end if
		  end if
		  
		  if  ((app.Registered) or (not(app.Registered) and LineCount mod 3 = 0)) then
		    if not(HTMLResults) then
		      ResultsPage.DrawString AdjustmentReason, 10, PageLine
		    else
		      HTMLFile.WriteLine AdjustmentReason
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintResults_Detail(ByRef ResultsPage as Graphics, HTMLFile as TextOutputStream, PageLine as integer, RaceDistanceID as string, DivisionID as string, TeamID as string, RacerNumber as string, RacerName as String, RacerAge as string, RacerGender as string, RacerNGB1License as string, RacerNGB1Points as string, RacerNGB2License as string, RacerNGB2Points as string, RacerCountry as string, RacerRepresenting as string, RacerDivision as string, RacerNetTime as string, RacerTotalTime as string, RacerBackTime as string, RacerPace as string, StageCutoff as string, LapsCompleted as string="0", FISUSSAScoring as string = "")
		  dim i, teamMembersRacerNumbers() as Integer
		  dim racePoints,totalTimeInSeconds as double
		  dim DivisionPlace, Place, SearchTime, SQLStatement, OutputString as string
		  dim rs, rsRaceDistance as RecordSet
		  dim NGBLicense as String
		  
		  if wnd_List.ResultType.Text="Cross Country Running" then
		    select case RacerAge
		    case "1", "9", "13"
		      RacerAge="FR"
		      
		    case "2", "10", "14"
		      RacerAge="SO"
		      
		    case "3", "11", "15"
		      RacerAge="JR"
		      
		    case "4", "12", "16"
		      RacerAge="SR"
		      
		    else
		      RacerAge="**"
		      
		    end Select
		  end if
		  
		  if PrintSetup.puFormatType.Text<>constAgeGraded then
		    SearchTime=Replace(RacerTotalTime,"*","")
		  else
		    if left(RacerTotalTime,1)<>"D" then
		      SearchTime=RacerNetTime 'Net time contains a percentage for age graded when doing an age graded report
		    else
		      SearchTime=RacerTotalTime 'want to send the DNS, DNF or DQ so the place returned in null
		    end if
		  end if
		  
		  if puFormatType.Text=constFISScoring or puFormatType.Text=constUSSAScoring then 'calculate race points.
		    totalTimeInSeconds=app.ConvertTimeToSeconds(RacerTotalTime)
		    if totalTimeInSeconds<>FISUSSAFastestTime then
		      racePoints=((totalTimeInSeconds-FISUSSAFastestTime)/FISUSSAFastestTime)*val(wnd_List.pmFValue.RowTag(wnd_List.pmFValue.ListIndex))
		      racePoints=Round(racePoints*100)/100
		      racePoints=racePoints
		    else
		      racePoints=0
		    end if
		  end if
		  
		  
		  if cbRound.Value and left(RacerTotalTime,1)<>"D" then
		    RacerTotalTime=left(app.ConvertSecondsToTime(round(app.ConvertTimeToSeconds(RacerTotalTime)),false),8)
		    if PrintSetup.puFormatType.Text<>constAgeGraded then
		      RacerNetTime=left(app.ConvertSecondsToTime(round(app.ConvertTimeToSeconds(RacerNetTime)),false),8)
		    end if
		  end if
		  
		  RacerTotalTime=app.StripLeadingZeros(RacerTotalTime)
		  RacerNetTime=app.StripLeadingZeros(RacerNetTime)
		  
		  if not(HTMLResults) then
		    if LineCount mod 2 = 1 then
		      ResultsPage.ForeColor=&cDDDDDD
		      ResultsPage.FillRect 10, PageLine-7, ResultsPage.Width-15, 9
		      ResultsPage.ForeColor=&c000000
		    end if
		  end if
		  
		  if  ((app.Registered) or (not(app.Registered) and LineCount mod 3 = 0)) then
		    
		    'if Overall list all Rankings
		    if InStr(puFormat.Text,"Overall")>0 and PrintSetup.puFormatType.Text<>constAgeGraded then
		      
		      if puFormat.Text="Overall By Distance"  then
		        if wnd_List.ResultType.Text=wnd_List.constCrossCountryResultType or wnd_List.ResultType.Text=wnd_List.constFISUSSAResultType  then
		          Place=app.GetPlace("Overall",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		          if TeamID<>"0" and wnd_List.ResultType.Text<>wnd_List.constFISUSSAResultType  then
		            rsRaceDistance=app.theDB.DBSQLSelect("SELECT Min_CC_Scorers FROM raceDistances WHERE rowid="+RaceDistanceID)
		            SQLStatement="SELECT participants.rowid FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		            SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		            SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND participants.DNS='N' AND participants.DNF='N' AND participants.DQ='N' AND "
		            SQLStatement=SQLStatement+"TeamID="+TeamID
		            rs=app.theDB.DBSQLSelect(SQLStatement)
		            if rs.RecordCount >= rsRaceDistance.Field("Min_CC_Scorers").IntegerValue then
		              Place=Place+" ("+app.GetPlace("Overall",RaceDistanceID,DivisionID,RacerGender,SearchTime,true)+")" 'get team scoring place
		            end if
		            rs.Close
		          end if
		          if not(HTMLResults) then
		            ResultsPage.DrawString Place, 10+(12-ResultsPage.StringWidth(Place)/2), PageLine
		            ResultsPage.DrawString RacerNumber, 40+(12-ResultsPage.StringWidth(RacerNumber)/2), PageLine
		            ResultsPage.DrawString RacerName, 70, PageLine, 100, True
		            if wnd_List.ResultType.Text<>wnd_List.constFISUSSAResultType  then
		              if PrintSetup.cbAge.Value then
		                ResultsPage.DrawString RacerAge, 175+(5-ResultsPage.StringWidth(RacerAge)/2), PageLine, 20, True
		              end if
		              ResultsPage.DrawString RacerRepresenting, 190, PageLine, 120, True
		            else
		              ResultsPage.DrawString RacerCountry, 170, PageLine, 30, True
		              if puFormatType.Text=constUSSAScoring then
		                NGBLicense=RacerNGB1License
		                ResultsPage.DrawString NGBLicense, 215, PageLine, 65, True
		                ResultsPage.DrawString format(racePoints,"##0.00"), 285+(20-ResultsPage.StringWidth(format(racePoints,"##0.00"))), PageLine, 65, True
		              elseif puFormatType.Text=constFISScoring then
		                NGBLicense=RacerNGB2License
		                ResultsPage.DrawString NGBLicense, 215, PageLine, 65, True
		                ResultsPage.DrawString format(racePoints,"##0.00"), 285+(20-ResultsPage.StringWidth(format(racePoints,"##0.00"))), PageLine, 65, True
		              else
		                ResultsPage.DrawString RacerNGB1License, 205, PageLine, 65, True
		                ResultsPage.DrawString RacerNGB2License, 270, PageLine, 65, True
		              end if
		              ResultsPage.DrawString RacerRepresenting, 325, PageLine, 95, True
		            end if
		          else
		            
		            OutputString=HTMLPad(Place,"R",10)
		            'OutputString=OutputString+HTMLPad(RacerNumber,"R",5)
		            OutputString=OutputString+HTMLPad(Titlecase(RacerName),"L",25)
		            OutputString=OutputString+HTMLPad(RacerAge,"C",6)
		            OutputString=OutputString+HTMLPad(Titlecase(RacerRepresenting),"L",25)
		          end if
		        else
		          
		          DivisionPlace=app.GetPlace("Division",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		          if val(DivisionPlace)<=3 and not(HTMLResults) then
		            ResultsPage.Bold=true
		          end if
		          
		          Place=app.GetPlace("Overall",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		          if not(HTMLResults) then
		            ResultsPage.DrawString Place, 10+(12-ResultsPage.StringWidth(Place)/2), PageLine
		            ResultsPage.DrawString "/", 35, PageLine
		          else
		            OutputString=HTMLPad(Place,"R",10)
		          end if
		          
		          Place=app.GetPlace("Gender",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		          if not(HTMLResults) then
		            ResultsPage.DrawString Place, 40+(12-ResultsPage.StringWidth(Place)/2), PageLine
		            ResultsPage.DrawString "/", 65, PageLine
		          end if
		          
		          if not(HTMLResults) then
		            ResultsPage.DrawString DivisionPlace, 70+(12-ResultsPage.StringWidth(Place)/2), PageLine
		            ResultsPage.DrawString RacerNumber, 100+(12-ResultsPage.StringWidth(RacerNumber)/2), PageLine
		            ResultsPage.DrawString RacerName, 130, PageLine, 83, True
		            if PrintSetup.cbAge.Value then
		              ResultsPage.DrawString RacerAge, 215+(5-ResultsPage.StringWidth(RacerAge)/2), PageLine, 20, True
		            end if
		            ResultsPage.DrawString RacerRepresenting, 230, PageLine, 90, True
		          else
		            'OutputString=OutputString+HTMLPad(RacerNumber,"R",5)
		            OutputString=OutputString+HTMLPad(Titlecase(RacerName),"L",25)
		            OutputString=OutputString+HTMLPad(RacerAge,"C",6)
		            OutputString=OutputString+HTMLPad(Titlecase(RacerRepresenting),"L",25)
		          end if
		        end if
		        
		      else
		        if wnd_List.ResultType.Text="Cross Country Running" or wnd_List.ResultType.Text=wnd_List.constFISUSSAResultType  then
		          Place=app.GetPlace("Gender",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted,FISUSSAScoring)
		          if not(HTMLResults) then
		            ResultsPage.DrawString Place, 10+(12-ResultsPage.StringWidth(Place)/2), PageLine
		            ResultsPage.DrawString RacerNumber, 40+(20-ResultsPage.StringWidth(RacerNumber)/2), PageLine
		            ResultsPage.DrawString RacerName, 70, PageLine, 100, True
		            if wnd_List.ResultType.Text<>wnd_List.constFISUSSAResultType  then
		              if PrintSetup.cbAge.Value then
		                ResultsPage.DrawString RacerAge, 175+(5-ResultsPage.StringWidth(RacerAge)/2), PageLine, 20, True
		              end if
		              ResultsPage.DrawString RacerRepresenting, 190, PageLine, 120, True
		            else
		              ResultsPage.DrawString RacerCountry, 170, PageLine, 30, True
		              if puFormatType.Text=constUSSAScoring then
		                NGBLicense=RacerNGB1License
		                ResultsPage.DrawString NGBLicense, 215, PageLine, 65, True
		                ResultsPage.DrawString format(racePoints,"##0.00"), 285+(20-ResultsPage.StringWidth(format(racePoints,"##0.00"))), PageLine, 65, True
		              elseif puFormatType.Text=constFISScoring then
		                NGBLicense=RacerNGB2License
		                ResultsPage.DrawString NGBLicense, 215, PageLine, 65, True
		                ResultsPage.DrawString format(racePoints,"##0.00"), 285+(20-ResultsPage.StringWidth(format(racePoints,"##0.00"))), PageLine, 65, True
		              else
		                ResultsPage.DrawString RacerNGB1License, 205, PageLine, 65, True
		                ResultsPage.DrawString RacerNGB2License, 270, PageLine, 65, True
		              end if
		              ResultsPage.DrawString RacerRepresenting, 325, PageLine, 95, True
		            end if
		          end if
		        else
		          DivisionPlace=app.GetPlace("Division",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		          Place=app.GetPlace("Gender",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		          if not(HTMLResults) then
		            ResultsPage.DrawString Place, 10+(12-ResultsPage.StringWidth(Place)/2), PageLine
		            ResultsPage.DrawString "/", 35, PageLine
		          else
		            OutputString=HTMLPad(Place,"R",10)
		          end if
		          
		          Place=app.GetPlace("Overall",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		          if not(HTMLResults) then
		            ResultsPage.DrawString Place, 40+(12-ResultsPage.StringWidth(Place)/2), PageLine
		            ResultsPage.DrawString "/", 65, PageLine
		          end if
		          
		          Place=app.GetPlace("Division",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		          if not(HTMLResults) then
		            ResultsPage.DrawString Place, 70+(12-ResultsPage.StringWidth(Place)/2), PageLine
		            ResultsPage.DrawString RacerNumber, 100+(12-ResultsPage.StringWidth(RacerNumber)/2), PageLine
		            ResultsPage.DrawString RacerName, 130, PageLine, 83, True
		            if PrintSetup.cbAge.Value then
		              ResultsPage.DrawString RacerAge, 215+(5-ResultsPage.StringWidth(RacerAge)/2), PageLine, 20, True
		            end if
		            ResultsPage.DrawString RacerRepresenting, 230, PageLine, 90, True
		          else
		            OutputString=OutputString+DivisionPlace
		            'OutputString=OutputString+HTMLPad(RacerNumber,"R",5)
		            OutputString=OutputString+HTMLPad(Titlecase(RacerName),"L",25)
		            OutputString=OutputString+HTMLPad(RacerAge,"C",6)
		            OutputString=OutputString+HTMLPad(Titlecase(RacerRepresenting),"L",25)
		          end if
		        end if
		      end if
		      
		      if not(HTMLResults) then
		        
		        if wnd_List.ResultType.Text<>wnd_List.constFISUSSAResultType then
		          if not(cbIncludeDivPlace.Value) then
		            ResultsPage.DrawString RacerDivision, 325, PageLine, 60, True
		          else
		            ResultsPage.DrawString RacerDivision+" ("+DivisionPlace+")", 325, PageLine, 60, True
		          end  if
		          if cbNetTime.Value or PrintSetup.puFormatType.Text=constAgeGraded then
		            ResultsPage.DrawString RacerNetTime, 387+(25-ResultsPage.StringWidth(RacerNetTime)/2), PageLine, 47, true
		          end if
		          
		          if wnd_List.puStartType.Text <> "24 hr, Criterium or Cyclocross" then
		            ResultsPage.DrawString RacerTotalTime, 440+(25-ResultsPage.StringWidth(RacerTotalTime)/2), PageLine, 47, true
		            ResultsPage.DrawString RacerBackTime, 490+(25-ResultsPage.StringWidth(RacerBackTime)/2), PageLine, 50, true
		            ResultsPage.DrawString RacerPace, 535+(25-ResultsPage.StringWidth(RacerPace)/2), PageLine, 50, true
		          else
		            ResultsPage.DrawString RacerTotalTime, 500+(25-ResultsPage.StringWidth(RacerTotalTime)/2), PageLine, 47, true
		          end if
		        else
		          if wnd_List.puStartType.Text <> "24 hr, Criterium or Cyclocross" then
		            ResultsPage.DrawString RacerDivision, 425, PageLine, 45, True
		            ResultsPage.DrawString RacerTotalTime, 475+(25-ResultsPage.StringWidth(RacerTotalTime)/2), PageLine, 47, true
		            ResultsPage.DrawString RacerBackTime, 525+(25-ResultsPage.StringWidth(RacerBackTime)/2), PageLine, 50, true
		          else
		            ResultsPage.DrawString RacerTotalTime, 500+(25-ResultsPage.StringWidth(RacerTotalTime)/2), PageLine, 47, true
		          end if
		        end if
		        
		        
		        ResultsPage.Bold=false
		      else
		        'OutputString=OutputString+HTMLPad(RacerNumber,"R",5)
		        'OutputString=OutputString+HTMLPad(RacerName,"L",30)
		        'OutputString=OutputString+HTMLPad(RacerAge,"R",3)
		        'OutputString=OutputString+HTMLPad(RacerRepresenting,"L",30)
		        
		        'if not(cbIncludeDivPlace.Value) then
		        'OutputString=OutputString+HTMLPad(RacerDivision,"L",30)
		        'else
		        'OutputString=OutputString+HTMLPad(RacerDivision+" ("+DivisionPlace+")","L",30)
		        'end if
		        
		        if cbNetTime.Value or PrintSetup.puFormatType.Text=constAgeGraded then
		          OutputString=OutputString+HTMLPad(RacerNetTime,"R",12)
		        end if
		        if app.RacersPerStart=9998 or wnd_List.cbJam.Value then
		          OutputString=OutputString+HTMLPad(LapsCompleted,"R",3)
		        end if
		        OutputString=OutputString+HTMLPad(RacerTotalTime,"R",12)
		        'OutputString=OutputString+HTMLPad(RacerBackTime,"R",12)
		        'OutputString=OutputString+HTMLPad(RacerPace,"R",10)
		        HTMLFile.WriteLine OutputString+"<br>"
		      end if
		      
		    else 'else list just the division place
		      if PrintSetup.puFormatType.Text<>constAgeGraded then 
		        Place=app.GetPlace("Division",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		      else
		        Place=app.GetPlace("Overall",RaceDistanceID,DivisionID,RacerGender,SearchTime,false,LapsCompleted)
		      end if
		      
		      if TeamID<>"0" and (wnd_List.ResultType.Text="Cross Country Running") then
		        rsRaceDistance=app.theDB.DBSQLSelect("SELECT Min_CC_Scorers FROM raceDistances WHERE rowid="+RaceDistanceID)
		        SQLStatement="SELECT participants.Racer_Number FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		        SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		        SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND participants.DNS='N' AND participants.DNF='N' AND participants.DQ='N' AND "
		        SQLStatement=SQLStatement+"TeamID="+TeamID+" ORDER BY participants.Total_Time LIMIT 0,"+MaxPlaces.Text
		        rs=app.theDB.DBSQLSelect(SQLStatement)
		        
		        if rs.RecordCount >= rsRaceDistance.Field("Min_CC_Scorers").IntegerValue then
		          ReDim teamMembersRacerNumbers(0)
		          for i  =  1 to rs.RecordCount
		            teamMembersRacerNumbers.Append rs.Field("Racer_Number").IntegerValue
		            rs.MoveNext
		          next
		          if (teamMembersRacerNumbers.IndexOf(val(RacerNumber))>=0) then
		            Place=Place+" ("+app.GetPlace("Division",RaceDistanceID,DivisionID,RacerGender,SearchTime,true)+")" 'get team scoring place
		          end if
		        end if
		        rs.Close
		      end if
		      
		      
		      if not(HTMLResults) then
		        if Place=StageCutoff then
		          ResultsPage.DrawLine(10,PageLine-8,580,PageLine-8)
		        end if
		        
		        ResultsPage.DrawString Place, 10+(12-ResultsPage.StringWidth(Place)/2), PageLine
		        
		        ResultsPage.DrawString RacerNumber, 40+(12-ResultsPage.StringWidth(RacerNumber)/2), PageLine
		        ResultsPage.DrawString RacerName, 70, PageLine, 170, True
		        if PrintSetup.cbAge.Value then
		          ResultsPage.DrawString RacerAge, 230+(5-ResultsPage.StringWidth(RacerAge)/2), PageLine, 20, True
		        end if
		        ResultsPage.DrawString RacerRepresenting, 245, PageLine, 140, True
		        
		        if cbNetTime.Value or PrintSetup.puFormatType.Text=constAgeGraded then
		          ResultsPage.DrawString RacerNetTime, 385+(25-ResultsPage.StringWidth(RacerNetTime)/2), PageLine, 47, true
		        end if
		        
		        if app.RacersPerStart=9998 or wnd_List.cbJam.Value then
		          ResultsPage.DrawString LapsCompleted, 400+(25-ResultsPage.StringWidth(LapsCompleted)/2), PageLine, 47, true
		        end if
		        
		        if wnd_List.puStartType.Text <> "24 hr, Criterium or Cyclocross" then
		          ResultsPage.DrawString RacerTotalTime, 440+(25-ResultsPage.StringWidth(RacerTotalTime)/2), PageLine, 47, true
		          ResultsPage.DrawString RacerBackTime, 490+(25-ResultsPage.StringWidth(RacerBackTime)/2), PageLine, 40, true
		        else
		          ResultsPage.DrawString RacerTotalTime, 500+(25-ResultsPage.StringWidth(RacerTotalTime)/2), PageLine, 47, true
		        end if
		        ResultsPage.DrawString RacerPace, 530+(25-ResultsPage.StringWidth(RacerPace)/2), PageLine, 40, true
		        
		      else
		        OutputString=HTMLPad(Place,"R",10)
		        'OutputString=OutputString+HTMLPad(RacerNumber,"R",5)
		        OutputString=OutputString+HTMLPad(Titlecase(RacerName),"L",25)
		        OutputString=OutputString+HTMLPad(RacerAge,"C",6)
		        OutputString=OutputString+HTMLPad(Titlecase(RacerRepresenting),"L",25)
		        if cbNetTime.Value or PrintSetup.puFormatType.Text=constAgeGraded then
		          OutputString=OutputString+HTMLPad(RacerNetTime,"R",12)
		        end if
		        if app.RacersPerStart=9998 then
		          OutputString=OutputString+HTMLPad(LapsCompleted,"R",3)
		        end if
		        OutputString=OutputString+HTMLPad(RacerTotalTime,"R",12)
		        'OutputString=OutputString+HTMLPad(RacerBackTime,"R",12)
		        'OutputString=OutputString+HTMLPad(RacerPace,"R",10)
		        HTMLFile.WriteLine OutputString+"<br>"
		      end if
		      
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintResults_Footer(ByRef ResultsPage as Graphics, HTMLFile as TextOutputStream)
		  dim OutputString as String
		  dim FooterStart as integer
		  
		  PageCount=PageCount+1
		  
		  'print line
		  if not(HTMLResults) then
		    FooterStart=ResultsPage.Height-40
		    resultspage.PenWidth=3
		    ResultsPage.DrawLine 10, FooterStart, ResultsPage.Width, FooterStart
		  else
		    HTMLFile.WriteLine "<br>"
		  end if
		  
		  'print adjustment notification
		  OutputString= "* indicates adjustments applied, see last page for details"
		  if not(HTMLResults) then
		    ResultsPage.Bold=false
		    ResultsPage.DrawString OutputString, 10,FooterStart+10
		  end if
		  
		  'print page number
		  OutputString= "Page: "+str(PageCount)
		  if not(HTMLResults) then
		    ResultsPage.Bold=true
		    ResultsPage.DrawString OutputString,ResultsPage.Width-ResultsPage.StringWidth(OutputString)-10,FooterStart+10
		  end if
		  
		  'print copyright stuff
		  OutputString="2005-2009 Milliseconds Computer Services, LLC"
		  if not(HTMLResults) then
		    ResultsPage.bold=false
		    ResultsPage.TextSize=8
		    ResultsPage.DrawString "© "+OutputString,ResultsPage.Width-ResultsPage.StringWidth(OutputString)-10,FooterStart+20
		  else
		    HTMLFile.WriteLine "<br><br>&copy "+OutputString+"<br>"
		  end if
		  
		  OutputString="801.582.3121/www.milliseconds.com"
		  if not(HTMLResults) then
		    ResultsPage.DrawString OutputString,ResultsPage.Width-ResultsPage.StringWidth(OutputString)-10,FooterStart+30
		  else
		    HTMLFile.WriteLine OutputString
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintResults_Header(ByRef ResultsPage as Graphics, HTMLFile as TextOutputStream, DivisionID as integer, DisplayFormat as string, ResultsName as string, PixelPosition as integer)
		  Dim rs,rsDivsions as RecordSet
		  Dim SQLStatement, Totals, OutputString as string
		  
		  PixelPosition=PixelPosition+10
		  
		  'Populate Results Name
		  if not(HTMLResults) then
		    ResultsPage.Bold=True
		    ResultsPage.TextSize=12
		    ResultsPage.DrawString ResultsName, 10, PixelPosition
		  else
		    HTMLFile.WriteLine "<br>"+"<br>"+ResultsName+"<br>"
		  end if
		  
		  if InStr(ResultsName,": Adjustments")=0 then
		    
		    if DisplayFormat="Division" then
		      SQLStatement="SELECT rowid FROM participants WHERE DivisionID="+str(DivisionID)
		      rsDivsions=app.theDB.DBSQLSelect(SQLStatement)
		      Totals="Reg: "+Format(rsDivsions.RecordCount,"###,###")
		      
		      SQLStatement="SELECT rowid FROM participants WHERE DivisionID="+str(DivisionID)+" AND DNS='Y' AND DQ='N'"
		      rsDivsions=app.theDB.DBSQLSelect(SQLStatement)
		      Totals=Totals+" DNS: "+Format(rsDivsions.RecordCount,"###,###")
		      
		      SQLStatement="SELECT rowid FROM participants WHERE DivisionID="+str(DivisionID)+" AND DNS='N' AND DNF='Y' AND DQ='N'"
		      rsDivsions=app.theDB.DBSQLSelect(SQLStatement)
		      Totals=Totals+" DNF: "+Format(rsDivsions.RecordCount,"###,###")
		      
		      SQLStatement="SELECT rowid FROM participants WHERE DivisionID="+str(DivisionID)+" AND DQ='Y'"
		      rsDivsions=app.theDB.DBSQLSelect(SQLStatement)
		      Totals=Totals+" DQ: "+Format(rsDivsions.RecordCount,"###,###")
		      
		      if not(HTMLResults) then 
		        ResultsPage.TextSize=8
		        ResultsPage.DrawString Totals, ResultsPage.Width-ResultsPage.StringWidth(Totals)-10, PixelPosition
		      else
		        HTMLFile.WriteLine Totals+"<br>"
		      end if
		      rsDivsions.Close
		    end if
		    
		    if not(HTMLResults) then
		      PixelPosition=PixelPosition+10
		      
		      ResultsPage.TextSize=8
		    end if
		    
		    select case DisplayFormat
		    case "Overall By Distance"
		      
		      if wnd_List.ResultType.Text="Cross Country Running" then
		        if not(HTMLResults) then
		          ResultsPage.DrawString "PL", 15, PixelPosition, 25, True
		          ResultsPage.DrawString "No", 45, PixelPosition
		          ResultsPage.DrawString "Name", 70, PixelPosition
		          if PrintSetup.cbAge.Value then
		            ResultsPage.DrawString "Age", 185, PixelPosition
		          end if
		          ResultsPage.DrawString "Representing", 210, PixelPosition
		          ResultsPage.DrawString "Division", 325, PixelPosition
		          if cbNetTime.Value then
		            ResultsPage.DrawString "Net Time", 390, PixelPosition
		          end if
		          if PrintSetup.puFormatType.Text=constAgeGraded then
		            ResultsPage.DrawString "Age Grade", 390, PixelPosition
		          end if
		          ResultsPage.DrawString "Total Time", 440, PixelPosition
		          ResultsPage.DrawString "Back", 510, PixelPosition
		          ResultsPage.DrawString "Pace", 550, PixelPosition
		          
		        else
		          OutPutString=OutPutString+HTMLPad("PL","R",10)
		          'OutPutString=OutPutString+HTMLPad("No","R",5)
		          OutPutString=OutPutString+HTMLPad("Name","L",25)
		          OutPutString=OutPutString+HTMLPad("Age","C",6)
		          OutPutString=OutPutString+HTMLPad("Representing","L",25)
		          if cbNetTime.Value then
		            OutPutString=OutPutString+HTMLPad("Net Time","R",12)
		          end if
		          if PrintSetup.puFormatType.Text=constAgeGraded then
		            OutPutString=OutPutString+HTMLPad("Age Grade","R",9)
		          end if
		          OutPutString=OutPutString+HTMLPad("Total Time","R",12)
		          'OutPutString=OutPutString+HTMLPad("Back","R",12)'
		          'OutPutString=OutPutString+HTMLPad("Pace","R",10)
		          HTMLFile.WriteLine OutputString+"<br>"
		        end if
		        
		      else
		        
		        if not(HTMLResults) then
		          if wnd_List.ResultType.Text<>wnd_List.constFISUSSAResultType  then
		            ResultsPage.DrawString "OvrAll", 10, PixelPosition, 25, True
		            ResultsPage.DrawString "/", 35, PixelPosition
		            ResultsPage.DrawString "Gndr", 40, PixelPosition, 25, True
		            ResultsPage.DrawString "/", 65, PixelPosition
		            ResultsPage.DrawString "Div", 73, PixelPosition, 25, True
		            ResultsPage.DrawString "No", 105, PixelPosition
		            ResultsPage.DrawString "Name", 130, PixelPosition
		            if PrintSetup.cbAge.Value then
		              ResultsPage.DrawString "Age", 212, PixelPosition
		            end if
		            ResultsPage.DrawString "Representing", 235, PixelPosition
		            ResultsPage.DrawString "Division", 325, PixelPosition
		            if cbNetTime.Value then
		              ResultsPage.DrawString "Net Time", 390, PixelPosition
		            end if
		            if PrintSetup.puFormatType.Text=constAgeGraded then
		              ResultsPage.DrawString "Age Grade", 390, PixelPosition
		            end if
		            if wnd_List.puStartType.Text <> "24 hr, Criterium or Cyclocross" then
		              ResultsPage.DrawString "Total Time", 440, PixelPosition
		              ResultsPage.DrawString "Back", 510, PixelPosition
		              ResultsPage.DrawString "Pace", 550, PixelPosition
		            else
		              ResultsPage.DrawString "Total Time", 500, PixelPosition
		            end if
		          else
		            ResultsPage.DrawString "PL", 15, PixelPosition, 25, True
		            ResultsPage.DrawString "No", 45, PixelPosition
		            ResultsPage.DrawString "Name", 70, PixelPosition
		            ResultsPage.DrawString "Nat", 170, PixelPosition
		            if puFormatType.Text=constUSSAScoring then
		              ResultsPage.DrawString "USSA License", 215, PixelPosition
		              ResultsPage.DrawString "Points", 280, PixelPosition
		            elseif puFormatType.Text=constFISScoring then
		              ResultsPage.DrawString "FIS License", 215, PixelPosition
		              ResultsPage.DrawString "Points", 280, PixelPosition
		            else
		              ResultsPage.DrawString "USSA License", 205, PixelPosition
		              ResultsPage.DrawString "FIS License", 270, PixelPosition
		            end if
		            ResultsPage.DrawString "Representing", 325, PixelPosition
		            ResultsPage.DrawString "Class", 425, PixelPosition
		            
		            if wnd_List.puStartType.Text <> "24 hr, Criterium or Cyclocross" then
		              ResultsPage.DrawString "Total Time", 475, PixelPosition
		              ResultsPage.DrawString "Back", 545, PixelPosition
		              ResultsPage.DrawString "Pace", 550, PixelPosition
		            else
		              ResultsPage.DrawString "Total Time", 500, PixelPosition
		            end if
		            
		          end if
		        else
		          OutPutString=HTMLPad("PL","R",10)
		          'OutPutString=OutPutString+HTMLPad("No","R",5)
		          OutPutString=OutPutString+HTMLPad("Name","L",30)
		          OutPutString=OutPutString+HTMLPad("Age","C",6)
		          OutPutString=OutPutString+HTMLPad("Representing","L",30)
		          OutPutString=OutPutString+HTMLPad("Division","L",30)
		          if cbNetTime.Value then
		            OutPutString=OutPutString+HTMLPad("Net Time","R",12)
		          end if
		          if PrintSetup.puFormatType.Text=constAgeGraded then
		            OutPutString=OutPutString+HTMLPad("Age Grade","R",9)
		          end if
		          if wnd_List.puStartType.Text <> "24 hr, Criterium or Cyclocross" then
		            OutPutString=OutPutString+HTMLPad("Total Time","R",12)
		            OutPutString=OutPutString+HTMLPad("Back","R",12)
		            OutPutString=OutPutString+HTMLPad("Pace","R",10)
		          else
		            OutPutString=OutPutString+HTMLPad("Total Time","R",12)
		          end if
		          HTMLFile.WriteLine OutputString+"<br>"
		        end if
		      end if
		    case "Overall By Gender"
		      if wnd_List.ResultType.Text=wnd_List.constFISUSSAResultType then
		        if not(HTMLResults) then
		          ResultsPage.DrawString "PL", 15, PixelPosition, 25, True
		          ResultsPage.DrawString "No", 45, PixelPosition
		          ResultsPage.DrawString "Name", 70, PixelPosition
		          if wnd_List.ResultType.Text<>wnd_List.constFISUSSAResultType  then
		            if PrintSetup.cbAge.Value then
		              ResultsPage.DrawString "Age", 185, PixelPosition
		            end if
		            ResultsPage.DrawString "Representing", 210, PixelPosition
		            ResultsPage.DrawString "Division", 325, PixelPosition
		            if cbNetTime.Value then
		              ResultsPage.DrawString "Net Time", 390, PixelPosition
		            end if
		            if PrintSetup.puFormatType.Text=constAgeGraded then
		              ResultsPage.DrawString "Age Grade", 390, PixelPosition
		            end if
		            
		            if wnd_List.puStartType.Text <> "24 hr, Criterium or Cyclocross" then
		              ResultsPage.DrawString "Total Time", 440, PixelPosition
		              ResultsPage.DrawString "Back", 510, PixelPosition
		              ResultsPage.DrawString "Pace", 550, PixelPosition
		            else
		              ResultsPage.DrawString "Total Time", 500, PixelPosition
		            end if
		            
		          else
		            ResultsPage.DrawString "Nat", 170, PixelPosition
		            if puFormatType.Text=constUSSAScoring then
		              ResultsPage.DrawString "USSA License", 215, PixelPosition
		              ResultsPage.DrawString "Points", 280, PixelPosition
		            elseif puFormatType.Text=constFISScoring then
		              ResultsPage.DrawString "FIS License", 215, PixelPosition
		              ResultsPage.DrawString "Points", 280, PixelPosition
		            else
		              ResultsPage.DrawString "USSA License", 205, PixelPosition
		              ResultsPage.DrawString "FIS License", 270, PixelPosition
		            end if
		            ResultsPage.DrawString "Representing", 325, PixelPosition
		            ResultsPage.DrawString "Class", 425, PixelPosition
		            
		            ResultsPage.DrawString "Total Time", 475, PixelPosition
		            ResultsPage.DrawString "Back", 545, PixelPosition
		          end if
		        else
		          OutPutString=OutPutString+HTMLPad("PL","R",10)
		          'OutPutString=OutPutString+HTMLPad("No","R",5)
		          OutPutString=OutPutString+HTMLPad("Name","L",25)
		          OutPutString=OutPutString+HTMLPad("Age","C",6)
		          OutPutString=OutPutString+HTMLPad("Representing","L",25)
		          if cbNetTime.Value then
		            OutPutString=OutPutString+HTMLPad("Net Time","R",12)
		          end if
		          if PrintSetup.puFormatType.Text=constAgeGraded then
		            OutPutString=OutPutString+HTMLPad("Age Grade","R",9)
		          end if
		          OutPutString=OutPutString+HTMLPad("Total Time","R",12)
		          'OutPutString=OutPutString+HTMLPad("Back","R",12)'
		          'OutPutString=OutPutString+HTMLPad("Pace","R",10)
		          HTMLFile.WriteLine OutputString+"<br>"
		        end if
		      else
		        
		        if not(HTMLResults) then
		          ResultsPage.DrawString "Gndr", 10, PixelPosition, 25, True
		          ResultsPage.DrawString "/", 35, PixelPosition
		          ResultsPage.DrawString "OvrAll", 40, PixelPosition, 25, True
		          ResultsPage.DrawString "/", 65, PixelPosition
		          ResultsPage.DrawString "Div", 73, PixelPosition, 25, True
		          ResultsPage.DrawString "No", 105, PixelPosition
		          ResultsPage.DrawString "Name", 130, PixelPosition
		          if PrintSetup.cbAge.Value then
		            ResultsPage.DrawString "Age", 212, PixelPosition
		          end if
		          ResultsPage.DrawString "Representing", 235, PixelPosition
		          ResultsPage.DrawString "Division", 325, PixelPosition
		          if cbNetTime.Value then
		            ResultsPage.DrawString "Net Time", 390, PixelPosition
		          end if
		          if PrintSetup.puFormatType.Text=constAgeGraded then
		            ResultsPage.DrawString "Age Grade", 390, PixelPosition
		          end if
		          
		          if wnd_List.puStartType.Text <> "24 hr, Criterium or Cyclocross" then
		            ResultsPage.DrawString "Total Time", 440, PixelPosition
		            ResultsPage.DrawString "Back", 510, PixelPosition
		            ResultsPage.DrawString "Pace", 550, PixelPosition
		          else
		            ResultsPage.DrawString "Total Time", 500, PixelPosition
		          end if
		        else
		          OutPutString=HTMLPad("PL","R",10)
		          'OutPutString=OutPutString+HTMLPad("No","R",5)
		          OutPutString=OutPutString+HTMLPad("Name","L",30)
		          OutPutString=OutPutString+HTMLPad("Age","L",3)
		          OutPutString=OutPutString+HTMLPad("Representing","L",30)
		          OutPutString=OutPutString+HTMLPad("Division","L",30)
		          if cbNetTime.Value then
		            OutPutString=OutPutString+HTMLPad("Net Time","R",12)
		          end if
		          if PrintSetup.puFormatType.Text=constAgeGraded then
		            OutPutString=OutPutString+HTMLPad("Age Grade","R",9)
		          end if
		          if wnd_List.puStartType.Text <> "24 hr, Criterium or Cyclocross" then
		            OutPutString=OutPutString+HTMLPad("Total Time","R",12)
		            OutPutString=OutPutString+HTMLPad("Back","R",12)
		            OutPutString=OutPutString+HTMLPad("Pace","R",10)
		          else
		            OutPutString=OutPutString+HTMLPad("Total Time","R",12)
		          end if
		          HTMLFile.WriteLine OutputString+"<br>"
		        end if
		      end if
		    else
		      if not(HTMLResults) then
		        ResultsPage.DrawString "PL", 15, PixelPosition, 25, True
		        ResultsPage.DrawString "No", 45, PixelPosition
		        ResultsPage.DrawString "Name", 70, PixelPosition
		        if PrintSetup.cbAge.Value then
		          ResultsPage.DrawString "Age", 226, PixelPosition
		        end if
		        ResultsPage.DrawString "Representing", 250, PixelPosition
		        if cbNetTime.Value then
		          ResultsPage.DrawString "Net Time", 390, PixelPosition
		        end if
		        if PrintSetup.puFormatType.Text=constAgeGraded then
		          ResultsPage.DrawString "Age Grade", 390, PixelPosition
		        end if
		        if app.RacersPerStart=9998 or wnd_List.cbJam.Value  then
		          ResultsPage.DrawString "Int", 419, PixelPosition
		        end if
		        if wnd_List.puStartType.Text <> "24 hr, Criterium or Cyclocross" then
		          ResultsPage.DrawString "Total Time", 440, PixelPosition
		          ResultsPage.DrawString "Back", 510, PixelPosition
		          ResultsPage.DrawString "Pace", 550, PixelPosition
		        else
		          ResultsPage.DrawString "Total Time", 500, PixelPosition
		        end if
		      else
		        OutPutString=OutPutString+HTMLPad("PL","R",10)
		        'OutPutString=OutPutString+HTMLPad("No","R",5)
		        OutPutString=OutPutString+HTMLPad("Name","L",25)
		        OutPutString=OutPutString+HTMLPad("Age","C",6)
		        OutPutString=OutPutString+HTMLPad("Representing","L",25)
		        if cbNetTime.Value then
		          OutPutString=OutPutString+HTMLPad("Net Time","R",12)
		        end if
		        if PrintSetup.puFormatType.Text=constAgeGraded then
		          OutPutString=OutPutString+HTMLPad("Age Grade","R",9)
		        end if
		        OutPutString=OutPutString+HTMLPad("Total Time","R",12)
		        'OutPutString=OutPutString+HTMLPad("Back","R",12)'
		        'OutPutString=OutPutString+HTMLPad("Pace","R",10)
		        HTMLFile.WriteLine OutputString+"<br>"
		      end if
		      
		    end select
		  end if
		  
		  if not(HTMLResults) then
		    ResultsPage.TextSize=8
		    ResultsPage.Bold=False
		    
		    resultspage.PenWidth=1
		    ResultsPage.DrawLine 10, PixelPosition+2, 580, PixelPosition+2
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintResults_PageHeader(ByRef ResultsPage as Graphics, HTMLFile as TextOutputStream)
		  'Populate Race Name
		  if not(HTMLResults) then
		    ResultsPage.TextSize=18
		    ResultsPage.Bold=True
		    ResultsPage.DrawString wnd_List.RaceName.Text, 10, 20
		    ResultsPage.TextSize=8
		    ResultsPage.Bold=False
		  else
		    HTMLFile.WriteLine wnd_List.RaceName.Text+"<br>"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintSeaOtterResults_Adjustments(ByRef ResultsPage as Graphics, PageLine as integer, AdjustmentReason as string)
		  
		  if LineCount mod 2 = 1 then
		    ResultsPage.ForeColor=&cDDDDDD
		    ResultsPage.FillRect 10, PageLine-7, ResultsPage.Width-15, 9
		    ResultsPage.ForeColor=&c000000
		  end if
		  
		  if  ((app.Registered) or (not(app.Registered) and LineCount mod 3 = 0)) then
		    ResultsPage.DrawString AdjustmentReason, 10, PageLine
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintSeaOtterResults_Detail(ByRef ResultsPage as Graphics, PageLine as integer, DivisionID as string, RacerNumber as string, LastName as String, FirstName as String, RacerGender as string, RacerAge as string, RacerHomeTown as string, RacerRepresenting as string, RacerNGBLicense as string, RacerTotalTime as string, StageCutoff as string, LapsCompleted as string)
		  dim Place as string
		  
		  RacerNumber=mid(RacerNumber,2)
		  
		  if LineCount mod 2 = 1 then
		    ResultsPage.ForeColor=&cDDDDDD
		    ResultsPage.FillRect 10, PageLine-7, ResultsPage.Width-15, 9
		    ResultsPage.ForeColor=&c000000
		  end if
		  
		  if  ((app.Registered) or (not(app.Registered) and LineCount mod 3 = 0)) then
		    
		    'if Overall list all Rankings
		    
		    Place=app.GetPlace("Division","0",DivisionID,RacerGender,RacerTotalTime,false,LapsCompleted)
		    
		    if Place=StageCutoff then
		      ResultsPage.DrawLine(10,PageLine-8,580,PageLine-8)
		    end if
		    
		    ResultsPage.DrawString Place, 10+(12-ResultsPage.StringWidth(Place)/2), PageLine
		    
		    ResultsPage.DrawString RacerNumber, 40+(12-ResultsPage.StringWidth(RacerNumber)/2), PageLine   'don't want the first character
		    ResultsPage.DrawString LastName, 70, PageLine, 78, True
		    ResultsPage.DrawString FirstName, 150, PageLine, 55, True
		    ResultsPage.DrawString RacerAge, 210+(12-ResultsPage.StringWidth(RacerAge)/2), PageLine, 140, True
		    ResultsPage.DrawString RacerHomeTown, 240, PageLine, 140, True
		    ResultsPage.DrawString RacerTotalTime, 375+(25-ResultsPage.StringWidth(RacerTotalTime)/2), PageLine, 47, True
		    ResultsPage.DrawString RacerNGBLicense, 430, PageLine, 40, True
		    ResultsPage.DrawString RacerRepresenting, 470, PageLine, 100, True
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function PrintSeaOtterResults_FirstPageHeader(ByRef ResultsPage as Graphics, RaceCode as string, ResultsName as string) As Integer
		  dim CurrentDate as new date
		  dim d as Double
		  dim HalfWidth, Position, PixelCount, w, h, newh, neww, newy as integer
		  dim OutputString as string
		  
		  HalfWidth=ResultsPage.Width/2
		  
		  'Populate Logos
		  ResultsPage.DrawPicture SeaOtterHeader, HalfWidth-130, 10, 350, 116, 0, 0, 350, 116
		  PixelCount=PixelCount+150
		  
		  'Populate Date and Location
		  ResultsPage.TextSize=12
		  ResultsPage.Bold=True
		  OutputString = "April 19 - 21, 2013"
		  Position=HalfWidth-(ResultsPage.StringWidth(OutputString)/2)
		  ResultsPage.DrawString OutputString, Position, PixelCount
		  
		  PixelCount=PixelCount+15
		  ResultsPage.TextSize=12
		  ResultsPage.Bold=True
		  OutputString = "Monterey, California USA"
		  Position=HalfWidth-(ResultsPage.StringWidth(OutputString)/2)
		  ResultsPage.DrawString OutputString, Position, PixelCount
		  
		  PixelCount=PixelCount+15
		  ResultsPage.TextSize=12
		  ResultsPage.Bold=True
		  OutputString = ResultsName
		  Position=HalfWidth-(ResultsPage.StringWidth(OutputString)/2)
		  ResultsPage.DrawString OutputString, Position, PixelCount
		  
		  PixelCount=PixelCount+15
		  ResultsPage.TextSize=12
		  ResultsPage.Bold=True
		  OutputString = "Race Date: "+app.RaceDate.LongDate
		  Position=HalfWidth-(ResultsPage.StringWidth(OutputString)/2)
		  ResultsPage.DrawString OutputString, Position, PixelCount
		  
		  PixelCount=PixelCount+15
		  
		  Return PixelCount
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintSeaOtterResults_Footer(ByRef ResultsPage as Graphics)
		  dim OutputString as String
		  dim FooterStart as integer
		  
		  PageCount=PageCount+1
		  FooterStart=ResultsPage.Height-75
		  
		  'print line
		  resultspage.PenWidth=3
		  ResultsPage.DrawLine 10, FooterStart, ResultsPage.Width, FooterStart
		  
		  'print adjustment notification
		  ResultsPage.Bold=false
		  ResultsPage.DrawString "* indicates adjustments applied, see last page for details", 10,FooterStart+10
		  
		  'print page number
		  ResultsPage.Bold=true
		  OutputString= "Page: "+str(PageCount)
		  ResultsPage.DrawString OutputString,ResultsPage.Width-ResultsPage.StringWidth(OutputString)-10,FooterStart+10
		  
		  
		  'print logo
		  ResultsPage.DrawPicture SeaOtterFooter, Resultspage.Width/2-134, FooterStart+15, 267, 55, 0, 0, 267, 55
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintSeaOtterResults_Header(ByRef ResultsPage as Graphics, ResultsName as string, PixelPosition as integer)
		  Dim rsDivsions as RecordSet
		  Dim SQLStatement, Totals as string
		  Dim HalfWidth, Position as Integer
		  
		  PixelPosition=PixelPosition+10
		  HalfWidth=ResultsPage.Width/2
		  
		  'Populate Results Name
		  'ResultsPage.Bold=True
		  'ResultsPage.TextSize=12
		  'Position=HalfWidth-(ResultsPage.StringWidth(ResultsName)/2)
		  'ResultsPage.DrawString ResultsName, Position, PixelPosition
		  'ResultsPage.Bold=False
		  
		  PixelPosition=PixelPosition+10
		  
		  ResultsPage.TextSize=8
		  ResultsPage.DrawString "Place", 15, PixelPosition, 25, True
		  ResultsPage.DrawString "Bib", 45, PixelPosition
		  ResultsPage.DrawString "Last Name", 70, PixelPosition
		  ResultsPage.DrawString "First Name", 150, PixelPosition
		  ResultsPage.DrawString "Age", 210, PixelPosition
		  ResultsPage.DrawString "City/State/Country", 240, PixelPosition
		  ResultsPage.DrawString "Finish Time", 375, PixelPosition
		  ResultsPage.DrawString "License", 430, PixelPosition
		  ResultsPage.DrawString "Team/Sponsor", 470, PixelPosition
		  
		  resultspage.PenWidth=1
		  ResultsPage.DrawLine 10, PixelPosition+2, ResultsPage.Width, PixelPosition+2
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintSeaOtterResults_PageHeader(ByRef ResultsPage as Graphics)
		  'Populate Race Name
		  ResultsPage.TextSize=18
		  ResultsPage.Bold=True
		  ResultsPage.DrawString wnd_List.RaceName.Text, 10, 20
		  ResultsPage.TextSize=8
		  ResultsPage.Bold=False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function PrintSignInSheet() As boolean
		  dim TimeDifference as double
		  dim i, ii, NumIntervals, PageLength, PixelCount as integer
		  dim SignInSheet as Graphics
		  dim ParticipantName, SelectStatement as String
		  dim rsQueryResults as RecordSet
		  Dim PageSetup as PrinterSetup
		  dim LastDivision as string
		  
		  if app.PrinterSettings<> "" then
		    PageSetup= New PrinterSetup
		    PageSetup.SetupString=app.PrinterSettings
		  end if
		  
		  SignInSheet = OpenPrinterDialog()
		  
		  if SignInSheet<>nil then
		    
		    PageLength=SignInSheet.Height
		    
		    SelectStatement="SELECT participants.Racer_Number, participants.Participant_Name, participants.First_Name, divisions.Division_Name, participants.Representing "
		    SelectStatement=SelectStatement+"FROM participants JOIN divisions ON participants.DivisionID = divisions.rowid ORDER BY "
		    SelectStatement=SelectStatement+"divisions.Division_Name ASC, participants.Participant_Name ASC, participants.First_Name ASC, participants.Representing ASC"
		    
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		    
		    if not (rsQueryResults.EOF) then
		      
		      PixelCount=95
		      PageCount=0
		      LineCount=0
		      PrintSignInSheet_FirstPageHeader(SignInSheet)
		      PrintSignInSheet_Header(SignInSheet,PixelCount,rsQueryResults.Field("Division_Name").stringValue)
		      PixelCount=PixelCount+45
		      LastDivision=rsQueryResults.Field("Division_Name").stringValue
		      
		      while not (rsQueryResults.EOF)
		        'print header
		        if (PixelCount+LineCount*20>PageLength-50) or (LastDivision<>rsQueryResults.Field("Division_Name").stringValue) Then
		          PrintSignInSheet_Footer(SignInSheet)
		          SignInSheet.NextPage
		          LineCount=0
		          if (LastDivision<>rsQueryResults.Field("Division_Name").stringValue) then
		            PixelCount=95
		            PageCount=0
		            PrintSignInSheet_FirstPageHeader(SignInSheet)
		            PrintSignInSheet_Header(SignInSheet,PixelCount,rsQueryResults.Field("Division_Name").stringValue)
		            PixelCount=PixelCount+45
		            LastDivision=rsQueryResults.Field("Division_Name").stringValue
		          else
		            PixelCount=25
		            PrintResults_PageHeader(SignInSheet, nil) ' this is on purpose... I want to use this header
		            PixelCount=PixelCount+25
		            PrintSignInSheet_Header(SignInSheet,PixelCount,rsQueryResults.Field("Division_Name").stringValue)
		            PixelCount=PixelCount+45
		          end if
		        end if
		        
		        'print detail lines
		        if rsQueryResults.Field("First_Name").stringValue <> "" then
		          ParticipantName=rsQueryResults.Field("Participant_Name").stringValue+", "+rsQueryResults.Field("First_Name").stringValue
		        else
		          ParticipantName=rsQueryResults.Field("Participant_Name").stringValue
		        end if
		        
		        PrintSignInSheet_Detail(SignInSheet, rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, _
		        rsQueryResults.Field("Representing").stringValue, PixelCount+LineCount*20)
		        
		        
		        rsQueryResults.moveNext
		        
		        LineCount=LineCount+1
		      wend
		      rsQueryResults.Close
		      
		      
		    end if 'if not (rsQueryResults.EOF) then
		    
		    'print footer
		    if LineCount>0 then
		      PrintSignInSheet_Footer(SignInSheet)
		    end if  ' if LineCount>0 then
		    
		    return true
		  else
		    return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintSignInSheet_Detail(ByRef SignInSheet as Graphics, RacerNumber as string, RacerName as String, RacerRepresenting as string, PageLine as integer)
		  Dim Pixels as integer
		  
		  if LineCount mod 2 = 1 then
		    SignInSheet.ForeColor=&cDDDDDD
		    SignInSheet.FillRect 10, PageLine-14, 580, 20
		    SignInSheet.ForeColor=&c000000
		  end if
		  
		  SignInSheet.DrawString RacerNumber, 10+(12-SignInSheet.StringWidth(RacerNumber)/2), PageLine
		  Pixels=50
		  
		  SignInSheet.DrawString RacerName, Pixels, PageLine, 180, True
		  Pixels=Pixels+135
		  
		  SignInSheet.DrawString RacerRepresenting, Pixels, PageLine, 180, True
		  Pixels=Pixels+210
		  
		  SignInSheet.DrawLine Pixels, PageLine, 580, PageLine
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintSignInSheet_FirstPageHeader(ByRef SignInSheet as Graphics)
		  dim CurrentDate as new date
		  dim HalfWidth,Position as integer
		  dim DateTime, SortType as string
		  
		  HalfWidth=SignInSheet.Width/2
		  DateTime=CurrentDate.LongDate+" "+CurrentDate.LongTime
		  
		  'Populate Race Name
		  SignInSheet.TextSize=18
		  SignInSheet.Bold=True
		  SignInSheet.DrawString wnd_List.RaceName.Text, 10, 20
		  
		  'Populate Report Name
		  SignInSheet.TextSize=18
		  SignInSheet.Bold=True
		  Position=HalfWidth-(SignInSheet.StringWidth("Sign-In Sheet")/2)
		  SignInSheet.DrawString "Sign-In Sheet", Position, 40
		  
		  'Populate Date and Time
		  SignInSheet.TextSize=8
		  Position=HalfWidth-(SignInSheet.StringWidth(DateTime)/2)
		  SignInSheet.DrawString DateTime, Position, 65
		  
		  SignInSheet.TextSize=8
		  SignInSheet.Bold=False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintSignInSheet_Footer(ByRef SignInSheet as Graphics)
		  dim OutputString as String
		  dim FooterStart as integer
		  
		  PageCount=PageCount+1
		  FooterStart=SignInSheet.Height-40
		  
		  'print line
		  SignInSheet.PenWidth=3
		  SignInSheet.DrawLine 10, FooterStart, 580, FooterStart
		  
		  'print page number
		  SignInSheet.Bold=true
		  OutputString= "Page: "+str(PageCount)
		  SignInSheet.DrawString OutputString,SignInSheet.Width-SignInSheet.StringWidth(OutputString)-10,FooterStart+10
		  
		  'print copyright stuff
		  SignInSheet.bold=false
		  SignInSheet.TextSize=8
		  OutputString="© 2008 Milliseconds Computer Services, LLC"
		  SignInSheet.DrawString OutputString,SignInSheet.Width-SignInSheet.StringWidth(OutputString)-10,FooterStart+20
		  OutputString="801.582.3121/www.milliseconds.com"
		  SignInSheet.DrawString OutputString,SignInSheet.Width-SignInSheet.StringWidth(OutputString)-10,FooterStart+30
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintSignInSheet_Header(ByRef SignInSheet as Graphics, PixelPosition as integer, DivisionName as string)
		  dim Pixels as Integer
		  
		  SignInSheet.Bold=True
		  SignInSheet.TextSize=18
		  SignInSheet.DrawString DivisionName, 10, PixelPosition
		  
		  PixelPosition=PixelPosition+20
		  
		  SignInSheet.TextSize=8
		  
		  SignInSheet.DrawString "No", 15, PixelPosition, 25, True
		  Pixels=50
		  
		  SignInSheet.DrawString "Name", Pixels, PixelPosition, 90, True
		  Pixels=Pixels+135
		  
		  SignInSheet.DrawString "Representing", Pixels, PixelPosition, 90, True
		  Pixels=Pixels+280
		  
		  
		  SignInSheet.DrawString "Signature", Pixels, PixelPosition,75, true
		  
		  SignInSheet.Bold=False
		  
		  SignInSheet.PenWidth=1
		  SignInSheet.DrawLine 10, PixelPosition+2, 580, PixelPosition+2
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function PrintStartList(ListType as string) As boolean
		  dim TimeDifference as double
		  dim i, ii, NumIntervals, PageLength, PixelCount as integer
		  dim StartList as Graphics
		  dim ParticipantName, SelectStatement as String
		  dim rsQueryResults as RecordSet
		  Dim PageSetup as PrinterSetup
		  dim CurrentTime, LastTime, NextTime as string
		  
		  LastTime=Mid(wnd_List.RaceTime.Text,12,8)
		  
		  if app.PrinterSettings<> "" then
		    PageSetup= New PrinterSetup
		    PageSetup.SetupString=app.PrinterSettings
		  end if
		  
		  StartList = OpenPrinterDialog()
		  
		  if StartList<>nil then
		    
		    PageLength=StartList.Height
		    
		    PageCount=0
		    LineCount=0
		    ColumnNumber=1
		    
		    PixelCount=Print_FirstPageHeader(StartList,nil,"Start List")
		    PrintStartList_Header(StartList,PixelCount,ListType)
		    PixelCount=PixelCount+35
		    
		    SelectStatement="SELECT participants.Racer_Number, participants.Participant_Name, participants.First_Name, divisions.Division_Name, participants.Representing, participants.Assigned_Start,  "
		    SelectStatement=SelectStatement+"participants.NGB_License_Number, participants.NGB1_Points, participants.NGB2_License_Number, participants.NGB2_Points "
		    SelectStatement=SelectStatement+"FROM participants JOIN divisions ON participants.DivisionID = divisions.rowid ORDER BY "
		    if rbNameSort.value then
		      SelectStatement=SelectStatement+"participants.Participant_Name ASC, participants.First_Name ASC, participants.Representing ASC"
		    elseif rbRacerNumberSort.value then
		      SelectStatement=SelectStatement+"participants.Racer_Number ASC, participants.Participant_Name ASC, participants.First_Name ASC, participants.Representing ASC"
		    elseif rbRepresentingDivision.Value then
		      SelectStatement=SelectStatement+"participants.Representing ASC, participants.Participant_Name ASC, participants.First_Name ASC"
		    else
		      SelectStatement=SelectStatement+"participants.Assigned_Start ASC, participants.Participant_Name ASC, participants.First_Name ASC, participants.Representing ASC"
		    end if
		    
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		    
		    if not (rsQueryResults.EOF) then
		      
		      while not (rsQueryResults.EOF)
		        'print header
		        if (PixelCount+LineCount*10>PageLength-50)  Then
		          PrintStartList_Footer(StartList)
		          StartList.NextPage
		          PixelCount=25
		          LineCount=0
		          ColumnNumber=1
		          PrintResults_PageHeader(StartList, nil) ' this is on purpose... I want to use this header
		          PrintStartList_Header(StartList,PixelCount,ListType)
		          PixelCount=PixelCount+35
		        end if
		        
		        'print detail lines
		        if rsQueryResults.Field("First_Name").stringValue <> "" then
		          ParticipantName=rsQueryResults.Field("Participant_Name").stringValue+", "+rsQueryResults.Field("First_Name").stringValue
		        else
		          ParticipantName=rsQueryResults.Field("Participant_Name").stringValue
		        end if
		        
		        CurrentTime=mid(rsQueryResults.Field("Assigned_Start").StringValue,12,8)
		        TimeDifference=app.ConvertTimeToSeconds(CurrentTime) - app.ConvertTimeToSeconds(LastTime)
		        if (TimeDifference > app.StartIntervalSec) and rbStartTime.Value then
		          NextTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(CurrentTime)-TimeDifference+app.StartIntervalSec,false)
		          NumIntervals=TimeDifference/app.StartIntervalSec/app.RacersPerStart
		          for i = 1 to NumIntervals-1
		            for ii= 1 to app.RacersPerStart
		              if (PixelCount+LineCount*10>PageLength-50)  Then
		                PrintStartList_Footer(StartList)
		                StartList.NextPage
		                PixelCount=25
		                LineCount=0
		                ColumnNumber=1
		                PrintResults_PageHeader(StartList, nil) ' this is on purpose... I want to use this header
		                PrintStartList_Header(StartList,PixelCount,ListType)
		                PixelCount=PixelCount+35
		              end if
		              
		              PrintStartList_Detail(StartList, "", "-", "", "","", "","", "", left(NextTime,8), PixelCount+LineCount*10, ListType)
		              LineCount=LineCount+1
		            next
		            NextTime=app.ConvertSecondsToTime((app.ConvertTimeToSeconds(NextTime)+app.StartIntervalSec),false)
		          next
		        end if
		        
		        PrintStartList_Detail(StartList, rsQueryResults.Field("Racer_Number").stringValue, ParticipantName, _
		        rsQueryResults.Field("NGB_License_Number").stringValue, rsQueryResults.Field("NGB1_Points").stringValue, _
		        rsQueryResults.Field("NGB2_License_Number").stringValue, rsQueryResults.Field("NGB2_Points").stringValue, _
		        rsQueryResults.Field("Representing").stringValue, rsQueryResults.Field("Division_Name").stringValue, _
		        CurrentTime, PixelCount+LineCount*10, ListType)
		        LastTime=CurrentTime
		        
		        rsQueryResults.moveNext
		        
		        LineCount=LineCount+1
		      wend
		      rsQueryResults.Close
		      
		      
		    end if 'if not (rsQueryResults.EOF) then
		    
		    'print footer
		    if LineCount>0 then
		      PrintStartList_Footer(StartList)
		    end if  ' if LineCount>0 then
		    
		    return true
		  else
		    return false
		  end if
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintStartList_Detail(ByRef StartList as Graphics,  RacerNumber as string, RacerName as String, NGB1License as string, NGB1Points as string, NGB2License as string, NGB2Points as string, RacerRepresenting as string, RacerDivision as string, StartTime as string, PageLine as integer, ListType as string)
		  Dim Pixels as integer
		  Dim PrintString as String
		  
		  if LineCount mod 2 = 1 then
		    StartList.ForeColor=&cDDDDDD
		    StartList.FillRect 10, PageLine-7, StartList.Width, 9
		    StartList.ForeColor=&c000000
		  end if
		  
		  if ListType="Start List - Time, No, Name, Rep, Div" then
		    StartList.DrawString StartTime, 15, PageLine
		    StartList.DrawString RacerNumber, 60+(12-StartList.StringWidth(RacerNumber)/2), PageLine
		    Pixels=100
		  else
		    StartList.DrawString RacerNumber, 10+(12-StartList.StringWidth(RacerNumber)/2), PageLine
		    Pixels=50
		  end if
		  
		  StartList.DrawString RacerName, Pixels, PageLine, 90, True
		  Pixels=Pixels+95
		  
		  if NGB1License <>"" then
		    if NGB1Points="nul" then
		      PrintString=NGB1License
		    else
		      PrintString = NGB1License+"/"+NGB1Points
		    end if
		  else
		    PrintString=""
		  end if
		  StartList.DrawString PrintString, Pixels, PageLine, 65, true
		  Pixels=Pixels+70
		  
		  if NGB2License <> "" then
		    if NGB2Points="nul" then
		      PrintString=NGB2License
		    else
		      PrintString = NGB2License+"/"+NGB2Points
		    end if
		  else
		    PrintString=""
		  end if
		  StartList.DrawString PrintString, Pixels, PageLine, 65, true
		  Pixels=Pixels+70
		  
		  StartList.DrawString RacerRepresenting, Pixels, PageLine, 145, True
		  Pixels=Pixels+150
		  
		  StartList.DrawString RacerDivision, Pixels, PageLine, 95, True
		  Pixels=Pixels+100
		  
		  if ListType="Start List - No, Name, Rep, Div, Time" then
		    StartList.DrawString StartTime, Pixels+(12-StartList.StringWidth("Start Time")/2), PageLine
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintStartList_FirstPageHeader(ByRef StartList as Graphics)
		  dim CurrentDate as new date
		  dim HalfWidth,Position as integer
		  dim DateTime, SortType as string
		  
		  HalfWidth=StartList.Width/2
		  DateTime=CurrentDate.LongDate+" "+CurrentDate.LongTime
		  
		  'Populate Race Name
		  StartList.TextSize=18
		  StartList.Bold=True
		  StartList.DrawString wnd_List.RaceName.Text, 10, 20
		  
		  'Populate Report Name
		  StartList.TextSize=18
		  StartList.Bold=True
		  Position=HalfWidth-(StartList.StringWidth("Start List")/2)
		  StartList.DrawString "Start List", Position, 40
		  
		  'Populate Sort Type
		  StartList.TextSize=8
		  StartList.Bold=False
		  if rbNameSort.Value then
		    SortType="Sorted by Name"
		  elseif rbRacerNumberSort.value then
		    SortType="Sorted by Racer Number"
		  else
		    SortType="Sorted by Start Time"
		  end if
		  Position=HalfWidth-(StartList.StringWidth(SortType)/2)
		  StartList.DrawString SortType, Position, 55
		  
		  'Populate Date and Time
		  Position=HalfWidth-(StartList.StringWidth(DateTime)/2)
		  StartList.DrawString DateTime, Position, 65
		  
		  StartList.TextSize=8
		  StartList.Bold=False
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintStartList_Footer(ByRef StartList as Graphics)
		  dim OutputString as String
		  dim FooterStart as integer
		  
		  PageCount=PageCount+1
		  FooterStart=StartList.Height-40
		  
		  'print line
		  StartList.PenWidth=3
		  StartList.DrawLine 10, FooterStart, 580, FooterStart
		  
		  'print page number
		  StartList.Bold=true
		  OutputString= "Page: "+str(PageCount)
		  StartList.DrawString OutputString,StartList.Width-StartList.StringWidth(OutputString)-10,FooterStart+10
		  
		  'print copyright stuff
		  StartList.bold=false
		  StartList.TextSize=8
		  OutputString="© 2008 Milliseconds Computer Services, LLC"
		  StartList.DrawString OutputString,StartList.Width-StartList.StringWidth(OutputString)-10,FooterStart+20
		  OutputString="801.582.3121/www.milliseconds.com"
		  StartList.DrawString OutputString,StartList.Width-StartList.StringWidth(OutputString)-10,FooterStart+30
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintStartList_Header(ByRef StartList as Graphics, PixelPosition as integer, ListType as string)
		  dim Pixels as Integer
		  
		  PixelPosition=PixelPosition+10
		  
		  'Populate Results Name
		  StartList.Bold=True
		  PixelPosition=PixelPosition+10
		  
		  StartList.TextSize=8
		  
		  if ListType="Start List - Time, No, Name, Rep, Div" then
		    StartList.DrawString "Start Time", 10, PixelPosition, 75, True
		    StartList.DrawString "No", 65, PixelPosition, 25, True
		    Pixels=100
		  else
		    StartList.DrawString "No", 15, PixelPosition, 25, True
		    Pixels=50
		  end if
		  
		  StartList.DrawString "Name", Pixels, PixelPosition, 90, True
		  Pixels=Pixels+95
		  
		  StartList.DrawString wnd_List.tfNGB1.text+" Lic/Pts", Pixels, PixelPosition, 90, True
		  Pixels=Pixels+70
		  
		  StartList.DrawString wnd_List.tfNGB2.text+" Lic/Pts", Pixels, PixelPosition, 90, True
		  Pixels=Pixels+70
		  
		  StartList.DrawString "Representing", Pixels, PixelPosition, 90, True
		  Pixels=Pixels+150
		  
		  StartList.DrawString "Division", Pixels, PixelPosition,75, true
		  Pixels=Pixels+90
		  
		  if ListType="Start List - No, Name, Rep, Div, Time" then
		    StartList.DrawString "Start Time", Pixels, PixelPosition, 75, True
		  end if
		  
		  StartList.Bold=False
		  
		  StartList.PenWidth=3
		  StartList.DrawLine 10, PixelPosition+2, 580, PixelPosition+2
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintTheResults()
		  Dim Success, Selection() as Boolean
		  Dim SelectionText(), SelectionID() as string
		  Dim i as Integer
		  
		  Select case ReportsTabPanel.Value
		  case 0  'we are on the Results Panel
		    redim Selection(0)
		    Redim SelectionText(0)
		    for i=0 to DataList.ListCount-1
		      SelectionID.Append ""
		      Selection.Append false
		      SelectionText.Append ""
		      SelectionID(i)=DataList.Cell(i,0)
		      Selection(i)=DataList.CellCheck(i,1)
		      SelectionText(i)=DataList.Cell(i,2)
		    next
		    if not(cbAutoPrint.Value) and not(PrintSetup.puFormatType.Text=constCCRun) and puFormat.ListIndex> 3 then
		      Success=PrintIntervalResults(cbFinalResults.Value,SelectionID,Selection,SelectionText,cbIntervals.Value,cbDNF.Value,puFormat.Text,cbNonPrinted.value,false)
		    elseif not(cbAutoPrint.Value) and not(PrintSetup.puFormatType.Text=constCCRun) then
		      Success=PrintResults(cbFinalResults.Value,SelectionID,Selection,SelectionText,cbIntervals.Value,cbDNF.Value,puFormat.Text,cbNonPrinted.value,false)
		    elseif not(cbAutoPrint.Value) and PrintSetup.puFormatType.Text=constCCRun then
		      Success=PrintCCRunResults(SelectionID,Selection,SelectionText,puFormat.Text,cbFinalResults.Value,val(MinPlaces.text),val(MaxPlaces.Text))
		    else
		      wnd_List.AutoPrint_FinalResults=cbFinalResults.Value
		      wnd_List.AutoPrint_SelectionID=SelectionID
		      wnd_List.AutoPrint_Selection=Selection
		      wnd_List.AutoPrint_SelectionText=SelectionText
		      wnd_List.AutoPrint_ListIntervals=cbIntervals.Value
		      wnd_List.AutoPrint_ListDNF=cbDNF.Value
		      wnd_List.AutoPrint_DisplayFormat=puFormat.Text
		      wnd_List.AutoPrint_PrintResultsTimer.Mode=Timer.ModeMultiple
		      wnd_List.AutoPrint_Stop.Visible=true
		      Success=true
		    end if
		    
		  case 1 ' We are on the other reports Panel
		    select case puReport.Text
		    case "Registration List"
		      Success=PrintRegList
		      
		    case "Missing Transponders List"
		      Success=PrintMissingTxList
		      
		    case "Start List - Time, No, Name, Rep, Div" , "Start List - No, Name, Rep, Div, Time"
		      Success=PrintStartList(puReport.text)
		      
		    case "Sign-In Sheets"
		      Success=PrintSignInSheet
		      
		    case "Prize Drawing List"
		      Success=PrintPrizeDrawingList
		      
		    end Select
		    
		  end Select
		  
		  if Success then
		    self.Close
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintTriResults_Detail(ByRef ResultsPage as Graphics, PageLine as integer,  RaceDistanceID as string, DivisionID as string, RacerNumber as string, RacerName as String, RacerAge as string, RacerGender as string, RacerRepresenting as string, RacerDivision as string, RacerTotalTime as string, RacerBackTime as string, RacerPace as string, IntervalOneTime as string, CompleteIntervalOneTime as string, IntervalTwoTime as string, IntervalThreeTime as string, CompleteIntervalThreeTime as string, IntervalFourTime as string, IntervalFiveTime as string, CompleteIntervalFiveTime as string, Penality as string)
		  dim Pace, Place, PlaceType, SearchTime as string
		  
		  SearchTime=Replace(RacerTotalTime,"*","")
		  
		  if cbRound.Value then
		    RacerTotalTime=left(app.ConvertSecondsToTime(round(app.ConvertTimeToSeconds(RacerTotalTime)),false),8)
		  end if
		  
		  RacerTotalTime=app.StripLeadingZeros(RacerTotalTime)
		  
		  if LineCount mod 2 = 1 then
		    ResultsPage.ForeColor=&cDDDDDD
		    ResultsPage.FillRect 10, PageLine-7, ResultsPage.Width-15, 9
		    ResultsPage.ForeColor=&c000000
		  end if
		  
		  if  ((app.Registered) or (not(app.Registered) and LineCount mod 3 = 0)) then
		    
		    'if Overall list all Rankings
		    if InStr(puFormat.Text,"Overall")>0 then
		      
		      if puFormat.Text="Overall By Distance" then
		        PlaceType="Overall"
		        Place=app.GetPlace(PlaceType,RaceDistanceID,DivisionID,RacerGender,SearchTime,false)
		        ResultsPage.DrawString Place, 10+(12-ResultsPage.StringWidth(Place)/2), PageLine
		      else
		        PlaceType="Gender"
		        Place=app.GetPlace(PlaceType,RaceDistanceID,DivisionID,RacerGender,SearchTime,false)
		        ResultsPage.DrawString Place, 10+(12-ResultsPage.StringWidth(Place)/2), PageLine
		      end if
		      
		      ResultsPage.DrawString RacerNumber, 30+(12-ResultsPage.StringWidth(RacerNumber)/2), PageLine
		      ResultsPage.DrawString RacerName, 65, PageLine, 80, True
		      ResultsPage.DrawString RacerRepresenting, 150, PageLine, 90, True
		      ResultsPage.DrawString RacerDivision, 240, PageLine, 82, True
		      
		      
		    else 'else list just the division place
		      PlaceType="Division"
		      Place=app.GetPlace(PlaceType,RaceDistanceID,DivisionID,RacerGender,SearchTime,false)
		      ResultsPage.DrawString Place, 10+(12-ResultsPage.StringWidth(Place)/2), PageLine
		      
		      ResultsPage.DrawString RacerNumber, 30+(12-ResultsPage.StringWidth(RacerNumber)/2), PageLine
		      ResultsPage.DrawString RacerName, 65, PageLine, 170, True
		      ResultsPage.DrawString RacerRepresenting, 200, PageLine, 122, True
		      
		    end if
		    
		    ResultsPage.DrawString IntervalOneTime, 325+(25-ResultsPage.StringWidth(IntervalOneTime)/2), PageLine, 47, true
		    Pace=app.CalculateIntPace(IntPaceType(0),IntActualDistance(0),CompleteIntervalOneTime)
		    ResultsPage.DrawString Pace, 375+(25-ResultsPage.StringWidth(Pace)/2), PageLine, 47, true
		    
		    Place=app.GetIntervalPlace(PlaceType,RaceDistanceID,DivisionID,RacerGender,"1",CompleteIntervalOneTime,"Interval Time",false)
		    ResultsPage.DrawString Place, 425+(12-ResultsPage.StringWidth(Place)/2), PageLine, 47, true
		    
		    ResultsPage.DrawString IntervalTwoTime, 450+(25-ResultsPage.StringWidth(IntervalTwoTime)/2), PageLine, 47, true 'T1
		    
		    ResultsPage.DrawString IntervalThreeTime, 500+(25-ResultsPage.StringWidth(IntervalThreeTime)/2), PageLine, 47, true
		    Pace=app.CalculateIntPace(IntPaceType(2),IntActualDistance(2),CompleteIntervalThreeTime)
		    ResultsPage.DrawString Pace, 550+(25-ResultsPage.StringWidth(Pace)/2), PageLine, 47, true
		    
		    Place=app.GetIntervalPlace(PlaceType,RaceDistanceID,DivisionID,RacerGender,"3",CompleteIntervalThreeTime,"Interval Time",false)
		    ResultsPage.DrawString Place, 600+(12-ResultsPage.StringWidth(Place)/2), PageLine, 47, true
		    
		    ResultsPage.DrawString IntervalFourTime, 625+(25-ResultsPage.StringWidth(IntervalFourTime)/2), PageLine, 47, true 'T2
		    
		    ResultsPage.DrawString IntervalFiveTime, 675+(25-ResultsPage.StringWidth(IntervalFiveTime)/2), PageLine, 47, true
		    Pace=app.CalculateIntPace(IntPaceType(4),IntActualDistance(4),CompleteIntervalFiveTime)
		    ResultsPage.DrawString Pace, 725+(25-ResultsPage.StringWidth(Pace)/2), PageLine, 47, true
		    
		    Place=app.GetIntervalPlace(PlaceType,RaceDistanceID,DivisionID,RacerGender,"5",CompleteIntervalFiveTime,"Interval Time",false)
		    ResultsPage.DrawString Place, 775+(12-ResultsPage.StringWidth(Place)/2), PageLine, 47, true
		    
		    ResultsPage.DrawString Penality, 800+(25-ResultsPage.StringWidth(RacerTotalTime)/2), PageLine, 47, true
		    ResultsPage.DrawString RacerTotalTime, 850+(25-ResultsPage.StringWidth(RacerTotalTime)/2), PageLine, 47, true
		    ResultsPage.DrawString RacerBackTime, 900+(25-ResultsPage.StringWidth(RacerBackTime)/2), PageLine, 50, true
		    
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub PrintTriResults_Header(ByRef ResultsPage as Graphics, DivisionID as Integer, DisplayFormat as string, ResultsName as string, PixelPosition as integer)
		  dim rsIntervals as RecordSet
		  dim SelectStatement as string
		  dim IntervalName() as string
		  dim i as integer
		  
		  SelectStatement=SelectStatement+"SELECT Interval_Name, Actual_Distance, Number, Pace_Type FROM intervals WHERE DivisionID="+str(DivisionID)+" ORDER BY Number ASC"
		  rsIntervals=app.theDB.DBSQLSelect(SelectStatement)
		  Redim IntervalName(rsIntervals.RecordCount)
		  Redim IntActualDistance(rsIntervals.RecordCount)
		  Redim IntNumber(rsIntervals.RecordCount)
		  Redim IntPaceType(rsIntervals.RecordCount)
		  for i = 1 to rsIntervals.RecordCount
		    IntervalName(i-1)=rsIntervals.Field("Interval_Name").StringValue
		    IntActualDistance(i-1)=rsIntervals.Field("Actual_Distance").DoubleValue
		    IntNumber(i-1)=rsIntervals.Field("Number").StringValue
		    IntPaceType(i-1)=rsIntervals.Field("Pace_Type").StringValue
		    rsIntervals.MoveNext
		  next
		  
		  PixelPosition=PixelPosition+10
		  
		  'Populate Results Name
		  ResultsPage.Bold=True
		  ResultsPage.TextSize=12
		  ResultsPage.DrawString ResultsName, 10, PixelPosition
		  
		  PixelPosition=PixelPosition+10
		  
		  ResultsPage.TextSize=8
		  select case DisplayFormat
		  case "Overall By Distance"
		    ResultsPage.DrawString "Pl", 20, PixelPosition, 25, True
		    ResultsPage.DrawString "No", 40, PixelPosition
		    ResultsPage.DrawString "Name", 65, PixelPosition
		    ResultsPage.DrawString "Representing", 150, PixelPosition
		    ResultsPage.DrawString "Division", 240, PixelPosition
		    
		  case "Overall By Gender"
		    ResultsPage.DrawString "Pl", 10, PixelPosition, 25, True
		    ResultsPage.DrawString "No", 40, PixelPosition
		    ResultsPage.DrawString "Name", 65, PixelPosition
		    ResultsPage.DrawString "Representing", 150, PixelPosition
		    ResultsPage.DrawString "Division", 240, PixelPosition
		    
		  else
		    ResultsPage.DrawString "PL", 15, PixelPosition, 25, True
		    ResultsPage.DrawString "No", 40, PixelPosition
		    ResultsPage.DrawString "Name", 65, PixelPosition
		    ResultsPage.DrawString "Representing", 200, PixelPosition
		    
		  end select
		  
		  ResultsPage.DrawString IntervalName(0), 325+(25-ResultsPage.StringWidth(IntervalName(0))/2), PixelPosition, 48, true  'Interval One
		  ResultsPage.DrawString IntPaceType(0), 375+(25-ResultsPage.StringWidth(IntPaceType(0))/2), PixelPosition, 48, true  'Interval One Pace
		  ResultsPage.DrawString "Pl", 433, PixelPosition, 48, true  'Interval One Pace
		  
		  ResultsPage.DrawString IntervalName(1), 450+(25-ResultsPage.StringWidth(IntervalName(1))/2), PixelPosition, 48, true  'T1
		  
		  ResultsPage.DrawString IntervalName(2), 500+(25-ResultsPage.StringWidth(IntervalName(2))/2), PixelPosition, 48, true  'Interval Two
		  ResultsPage.DrawString IntPaceType(2), 550+(25-ResultsPage.StringWidth(IntPaceType(2))/2), PixelPosition, 48, true  'Interval Two Pace
		  ResultsPage.DrawString "Pl", 608, PixelPosition, 48, true  'Interval Two Place
		  
		  ResultsPage.DrawString IntervalName(3), 625+(25-ResultsPage.StringWidth(IntervalName(3))/2), PixelPosition, 48, true  'T2
		  
		  ResultsPage.DrawString IntervalName(4), 675+(25-ResultsPage.StringWidth(IntervalName(4))/2), PixelPosition, 48, true  'Interval Three
		  ResultsPage.DrawString IntPaceType(4), 725+(25-ResultsPage.StringWidth(IntPaceType(4))/2), PixelPosition, 48, true  'Interval Three Pace
		  ResultsPage.DrawString "Pl", 783, PixelPosition, 48, true  'Interval Three Place
		  
		  ResultsPage.DrawString "Penalty", 800, PixelPosition
		  
		  ResultsPage.DrawString "Total Time", 850, PixelPosition
		  ResultsPage.DrawString "Back", 900+(25-ResultsPage.StringWidth("Back")/2), PixelPosition
		  
		  ResultsPage.Bold=False
		  
		  resultspage.PenWidth=1
		  ResultsPage.DrawLine 10, PixelPosition+2, ResultsPage.Width, PixelPosition+2
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub Print_DrawLogos(g as graphics, LogoPath as string, x as integer, scalew as integer, scaleh as integer)
		  dim d as Double
		  dim w, h, neww, newh, newy as integer 
		  dim f as FolderItem
		  dim p as Picture
		  
		  f=GetFolderItem(LogoPath,FolderItem.PathTypeAbsolute)
		  p=f.OpenAsPicture
		  w=p.width
		  h=p.height
		  d=min(scalew/w,scaleh/h) // Calculate the factor with which to scale
		  d=0.4
		  neww=w*d // the new width of the picture
		  newh=h*d // the new height of the picture
		  newy=10+((100-newh)/2) //starting y position
		  g.DrawPicture p, x, newy, neww, newh, 0, 0, w, h
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function Print_FirstPageHeader(ByRef ResultsPage as Graphics, HTMLFile as TextOutputStream, ResultsType as string) As Integer
		  dim CurrentDate as new date
		  dim StartDate as new date
		  dim d as Double
		  dim HalfWidth,Position, PixelCount, w, h, newh, neww, newy as integer
		  dim DateTime as string
		  
		  if not(HTMLResults) then
		    HalfWidth=ResultsPage.Width/2
		  end if
		  
		  'Populate Logos
		  
		  if wnd_List.Logo1<>"" or wnd_List.Logo2<>"" or wnd_List.Logo3<>"" or wnd_List.Logo4<>"" then
		    if wnd_List.Logo1<>"" then
		      Print_DrawLogos(ResultsPage, wnd_List.Logo1, 10, 100, 100)
		    end if
		    
		    if wnd_List.Logo2<>"" then
		      Print_DrawLogos(ResultsPage, wnd_List.Logo2, 175, 100, 100)
		    end if
		    
		    if wnd_List.Logo3<>"" then
		      Print_DrawLogos(ResultsPage, wnd_List.Logo3, 320, 100, 100)
		    end if
		    
		    if wnd_List.Logo4<>"" then
		      Print_DrawLogos(ResultsPage, wnd_List.Logo4,450, 100, 100)
		    end if
		    
		    PixelCount=150
		  else
		    
		    PixelCount=10
		  end if
		  
		  'Populate Race Name
		  if not(HTMLResults) then
		    PixelCount=PixelCount+10
		    ResultsPage.TextSize=18
		    ResultsPage.Bold=True
		    Position=HalfWidth-(ResultsPage.StringWidth(wnd_List.RaceName.Text)/2)
		    ResultsPage.DrawString wnd_List.RaceName.Text,Position, PixelCount
		  else
		    HTMLFile.WriteLine wnd_List.RaceName.Text+"<br>"
		  end if
		  
		  'Populate Results Type
		  if not(HTMLResults) then
		    PixelCount=PixelCount+20
		    ResultsPage.TextSize=18
		    ResultsPage.Bold=True
		    Position=HalfWidth-(ResultsPage.StringWidth(ResultsType)/2)
		    ResultsPage.DrawString ResultsType, Position, PixelCount
		    
		    ResultsPage.Bold=false
		    PixelCount=PixelCount+20
		    ResultsPage.TextSize=10
		    StartDate.SQLDateTime=left(wnd_List.RaceTime.Text,19)
		    DateTime="Start Time: "+StartDate.LongDate+" "+StartDate.LongTime
		    Position=HalfWidth-(ResultsPage.StringWidth(DateTime)/2)
		    ResultsPage.DrawString DateTime, Position, PixelCount
		  else
		    HTMLFile.WriteLine ResultsType+"<br>"
		  end if
		  
		  if wnd_List.ResultType.Text=wnd_List.constFISUSSAResultType then 'print the jury and course information
		    PixelCount=PixelCount+20
		    
		    resultspage.PenWidth=1
		    ResultsPage.DrawRoundRect(10,PixelCount-10,ResultsPage.Width-20,75,10,10)
		    ResultsPage.DrawLine HalfWidth, PixelCount-10, HalfWidth, PixelCount+64
		    
		    ResultsPage.TextSize=8
		    ResultsPage.Bold=true
		    ResultsPage.DrawString "Jury Information", 20, PixelCount
		    ResultsPage.DrawString "Course Information ", HalfWidth+20, PixelCount
		    PixelCount=PixelCount+5
		    ResultsPage.DrawLine 10, PixelCount, ResultsPage.Width-11, PixelCount
		    PixelCount=PixelCount+10
		    
		    ResultsPage.Bold=true
		    ResultsPage.DrawString "Technical Delegate: ", 20, PixelCount
		    ResultsPage.Bold=false
		    ResultsPage.DrawString wnd_List.tfTDName.Text+"  ("+wnd_List.tfTDDiv.Text+")", 30+ResultsPage.StringWidth("Technical Delegate: "), PixelCount
		    ResultsPage.Bold=true
		    ResultsPage.DrawString "Height Difference (HD): ",HalfWidth+20, PixelCount
		    ResultsPage.Bold=false
		    ResultsPage.DrawString wnd_List.tfHD.Text, HalfWidth+30+ResultsPage.StringWidth("Height Difference (HD): "), PixelCount
		    PixelCount=PixelCount+10
		    
		    ResultsPage.Bold=true
		    ResultsPage.DrawString "Assistant Technical Delegate: ", 20, PixelCount
		    ResultsPage.Bold=false
		    ResultsPage.DrawString wnd_List.tfATDName.text+" ("+wnd_List.tfATDDiv.Text+")", 30+ResultsPage.StringWidth("Assistant Technical Delegate: "), PixelCount
		    ResultsPage.Bold=true
		    ResultsPage.DrawString "Maximum Climb (MC): ", HalfWidth+20, PixelCount
		    ResultsPage.Bold=false
		    ResultsPage.DrawString wnd_List.tfMC.Text, HalfWidth+30+ResultsPage.StringWidth("Height Difference (HD): "), PixelCount
		    PixelCount=PixelCount+10
		    
		    ResultsPage.Bold=true
		    ResultsPage.DrawString "Chief of Course: ", 20, PixelCount
		    ResultsPage.Bold=false
		    ResultsPage.DrawString wnd_List.tfChiefOfCourse.text+" ("+wnd_List.tfChiefOfCourseDiv.Text+")", 30+ResultsPage.StringWidth("Chief of Course: "), PixelCount
		    ResultsPage.Bold=true
		    ResultsPage.DrawString "Total Climb (TC): ", HalfWidth+20, PixelCount
		    ResultsPage.Bold=false
		    ResultsPage.DrawString wnd_List.tfTC.Text, HalfWidth+30+ResultsPage.StringWidth("Total Climb (TC): "), PixelCount
		    PixelCount=PixelCount+10
		    
		    ResultsPage.Bold=true
		    ResultsPage.DrawString "Jury Member 4: ", 20, PixelCount
		    ResultsPage.Bold=false
		    ResultsPage.DrawString wnd_List.tfJuryMember4.text+" ("+wnd_List.tfJuryMember4Div.Text+")", 30+ResultsPage.StringWidth("Jury Member 4: "), PixelCount
		    ResultsPage.Bold=true
		    ResultsPage.DrawString "Lap Length: ", HalfWidth+20, PixelCount
		    ResultsPage.Bold=false
		    ResultsPage.DrawString wnd_List.tfLapLength.Text+"m", HalfWidth+30+ResultsPage.StringWidth("Lap Length: "), PixelCount
		    PixelCount=PixelCount+10
		    
		    ResultsPage.Bold=true
		    ResultsPage.DrawString "Jury Member 5: ", 20, PixelCount
		    ResultsPage.Bold=false
		    ResultsPage.DrawString wnd_List.tfJuryMember5.text+" ("+wnd_List.tfJuryMember5Div.Text+")", 30+ResultsPage.StringWidth("Jury Member 5: "), PixelCount
		    ResultsPage.Bold=true
		    ResultsPage.DrawString "Weather: ", HalfWidth+20, PixelCount
		    ResultsPage.Bold=false
		    ResultsPage.DrawString wnd_List.tfWX.Text, HalfWidth+30+ResultsPage.StringWidth("Weather: "), PixelCount
		    PixelCount=PixelCount+10
		    
		  end if
		  
		  'Populate Date and Time
		  PixelCount=PixelCount+25
		  DateTime=CurrentDate.LongDate+" "+CurrentDate.LongTime
		  if not(HTMLResults) then
		    ResultsPage.TextSize=8
		    Position=HalfWidth-(ResultsPage.StringWidth(DateTime)/2)
		    ResultsPage.DrawString DateTime, Position, PixelCount
		  else
		    HTMLFile.WriteLine DateTime+"<br>"
		  end if
		  
		  
		  
		  
		  if not(HTMLResults) then
		    PixelCount=PixelCount+5
		    resultspage.PenWidth=3
		    ResultsPage.DrawLine 10, PixelCount, ResultsPage.Width, PixelCount
		    ResultsPage.TextSize=8
		    ResultsPage.Bold=False
		    
		    PixelCount=PixelCount+5
		  else
		    HTMLFile.WriteLine "<br>"
		  end if
		  
		  
		  Return PixelCount
		End Function
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected ColumnNumber As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected DoubleQuoteMark As string
	#tag EndProperty

	#tag Property, Flags = &h0
		FISUSSAFastestTime As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		FISUSSAPenaltyPoints As Double
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected HTMLResults As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected IntActualDistance() As Double
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected IntervalName() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected IntNumber() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected IntPaceType() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected LineCount As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected MaxLaps As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected MemberCount As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h21
		Private NumDivisions As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected PageCount As Integer
	#tag EndProperty


	#tag Constant, Name = constAgeGraded, Type = String, Dynamic = False, Default = \"Age Graded\r", Scope = Public
	#tag EndConstant

	#tag Constant, Name = constCCRun, Type = String, Dynamic = False, Default = \"Cross Country Running Team Standing", Scope = Public
	#tag EndConstant

	#tag Constant, Name = constFISScoring, Type = String, Dynamic = False, Default = \"FIS Point Scoring", Scope = Public
	#tag EndConstant

	#tag Constant, Name = constNormal, Type = String, Dynamic = False, Default = \"Normal", Scope = Public
	#tag EndConstant

	#tag Constant, Name = constUSSAScoring, Type = String, Dynamic = False, Default = \"USSA Point Scoring", Scope = Public
	#tag EndConstant


#tag EndWindowCode

#tag Events puFormatType
	#tag Event
		Sub Change()
		  if me.Text=constFISScoring or me.Text=constUSSAScoring then
		    cbPenaltyWorksheet.Visible=true
		    puFormat.ListIndex=1
		    puFormat.Enabled=false
		  else
		    cbPenaltyWorksheet.Visible=false
		    puFormat.Enabled=true
		  end if
		  
		  
		  if wnd_List.ResultType.text+" Team Standing" =constCCRun then
		    MinLabel.Visible=True
		    MinPlaces.Text="5"
		    MinPlaces.Visible=True
		    
		    MaxLabel.Visible=True
		    MaxPlaces.Text="7"
		    MaxPlaces.Visible=True
		    
		  else
		    MinLabel.Visible=False
		    MinPlaces.Visible=False
		    
		    MaxLabel.Visible=False
		    MaxPlaces.Visible=False
		    
		  end if
		End Sub
	#tag EndEvent
	#tag Event
		Sub Open()
		  me.AddRow constNormal
		  me.AddRow constAgeGraded
		  me.AddRow constCCRun
		  if wnd_List.ResultType.Text = wnd_List.constFISUSSAResultType then
		    me.AddRow constFISScoring
		    me.AddRow constUSSAScoring
		  end if
		  me.ListIndex=0
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbRound
	#tag Event
		Sub Open()
		  if  ((instr(wnd_list.puStartType.Text,"Criterium")>0 or instr(wnd_List.puStartType.Text,"Circuit")>0 or wnd_List.ResultType.Text="Cross Country Running") or ( app.RacersPerStart>=1 and app.RacersPerStart<=4)) then
		    me.Visible=True
		  else
		    me.Value=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events BevelButton2
	#tag Event
		Sub Action()
		  dim i as integer
		  
		  for i=0 to DataList.ListCount-1
		    DataList.CellCheck(i,1)=false
		  next
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events puReport
	#tag Event
		Sub Change()
		  if me.text="Sign-In Sheets" then
		    rbNameSort.Enabled=false
		    rbRacerNumberSort.Enabled=false
		    rbDivisionRepresenting.Enabled=false
		    rbDivisionSort.Enabled=false
		    rbRepresentingDivision.Enabled=false
		  elseif (instr(me.text,"Start")>0) then
		    rbNameSort.Enabled=true
		    rbRacerNumberSort.Enabled=true
		    rbDivisionRepresenting.Enabled=true
		    rbDivisionSort.Enabled=true
		    rbRepresentingDivision.Enabled=true
		  else
		    rbNameSort.Enabled=true
		    rbRacerNumberSort.Enabled=true
		    rbDivisionRepresenting.Enabled=true
		    rbDivisionSort.Enabled=true
		    rbRepresentingDivision.Enabled=true
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events puFormat
	#tag Event
		Sub Open()
		  dim SQLStatement as string
		  dim rsDistinctIntervals as RecordSet
		  dim test as string
		  
		  
		  me.AddRow "Overall by Distance"
		  me.AddRow "Overall By Gender"
		  me.AddRow "Division"
		  me.AddRow "Awards"
		  me.ListIndex=0
		  
		  me.AddRow "-"
		  
		  SQLStatement="SELECT DISTINCT Interval_Name, Number FROM intervals WHERE RaceID =1 ORDER BY Number ASC"
		  rsDistinctIntervals=app.theDB.DBSQLSelect(SQLStatement)
		  
		  if rsDistinctIntervals.RecordCount>0 then
		    while not rsDistinctIntervals.EOF
		      me.AddRow "Interval Standings for: "+rsDistinctIntervals.Field("Interval_Name").StringValue+" (by Distance)"
		      me.RowTag(me.ListCount-1)=rsDistinctIntervals.Field("Number").IntegerValue
		      rsDistinctIntervals.MoveNext
		    Wend
		    
		    me.AddRow "-"
		    rsDistinctIntervals.close
		    SQLStatement="SELECT DISTINCT Interval_Name, Number FROM intervals WHERE RaceID =1 ORDER BY Number ASC"
		    rsDistinctIntervals=app.theDB.DBSQLSelect(SQLStatement)
		    
		    while not rsDistinctIntervals.EOF
		      NumDivisions=NumDivisions+1
		      test=rsDistinctIntervals.Field("Interval_Name").StringValue
		      me.AddRow "Interval Standings for: "+rsDistinctIntervals.Field("Interval_Name").StringValue+" (by Gender)"
		      me.RowTag(me.ListCount-1)=rsDistinctIntervals.Field("Number").IntegerValue
		      rsDistinctIntervals.MoveNext
		    Wend
		    
		    me.AddRow "-"
		    rsDistinctIntervals.close
		    SQLStatement="SELECT DISTINCT Interval_Name, Number FROM intervals WHERE RaceID =1 ORDER BY Number ASC"
		    rsDistinctIntervals=app.theDB.DBSQLSelect(SQLStatement)
		    
		    while not rsDistinctIntervals.EOF
		      me.AddRow "Interval Standings for: "+rsDistinctIntervals.Field("Interval_Name").StringValue+" (by Division)"
		      me.RowTag(me.ListCount-1)=rsDistinctIntervals.Field("Number").IntegerValue
		      rsDistinctIntervals.MoveNext
		    Wend
		  end if
		  
		  ExecuteQuery(Me.ListIndex)
		End Sub
	#tag EndEvent
	#tag Event
		Sub Change()
		  dim i as integer
		  dim currentChoice as string
		  
		  ExecuteQuery(me.ListIndex)
		  
		  if me.ListIndex=0 then
		    cbAutoPrint.Visible=true
		  else
		    cbAutoPrint.Visible=false
		  end if
		  cbAutoPrint.Value=false
		  
		  if me.ListIndex=2 or  me.ListIndex=3 then
		    puDistanceSelect.Visible=True
		  else
		    puDistanceSelect.Visible=False
		  end if
		  
		  if me.ListIndex=3 or me.Text=constCCRun then
		    PrintSetup.Places.text="5"
		  else
		    PrintSetup.Places.text="all"
		  end if
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events efLapsFrom
	#tag Event
		Sub Open()
		  if app.RacersPerStart=9998 then
		    me.Visible=True
		  else
		    me.Visible=False
		  end if
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events efLapsTo
	#tag Event
		Sub Open()
		  dim rsMax as RecordSet
		  dim SQLStatement as String
		  
		  if app.RacersPerStart=9998 then
		    me.Visible=True
		    SQLStatement="SELECT MAX(Laps_Completed) FROM participants"
		    rsMax=App.theDB.DBSQLSelect(SQLStatement)
		    if rsMax<>nil then
		      MaxLaps=rsMax.Field("MAX(Laps_Completed)").IntegerValue
		      me.Text=str(MaxLaps)
		      if MaxLaps<=9 then
		        efLapsFrom.Text="1"
		      else
		        efLapsFrom.Text=str(MaxLaps-9+1)
		      end if
		    end if
		  else
		    me.Visible=False
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events stLaps1
	#tag Event
		Sub Open()
		  if app.RacersPerStart=9998 then
		    me.Visible=True
		  else
		    me.Visible=False
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events stLaps2
	#tag Event
		Sub Open()
		  if app.RacersPerStart=9998 then
		    me.Visible=True
		  else
		    me.Visible=False
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbIntervals
	#tag Event
		Sub Action()
		  if me.Value then
		    efLapsTo.Visible=True
		    efLapsFrom.Visible=True
		    stLaps1.Visible=True
		    stLaps2.Visible=True
		  else
		    efLapsTo.Visible=False
		    efLapsFrom.Visible=False
		    stLaps1.Visible=False
		    stLaps2.Visible=False
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events cbNetTime
	#tag Event
		Sub Open()
		  if  ((instr(wnd_list.puStartType.Text,"Criterium")>0 or instr(wnd_List.puStartType.Text,"Circuit")>0) or ( app.RacersPerStart>=1 and app.RacersPerStart<=4)) then
		    cbNetTime.Value=false
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DataList
	#tag Event
		Sub Open()
		  dim SelectStatement as string
		  dim res as Boolean
		  dim rsQueryResults as RecordSet
		  dim recCount as Integer
		  DataList.ColumnType(1)=2 'Make column one an CheckBox
		  'SelectStatement="SELECT Division_Name, Stage_Cutoff, rowid FROM divisions WHERE RaceID =1 ORDER BY List_Order ASC"
		  '
		  'rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		  'while not rsQueryResults.EOF
		  'me.AddRow ""
		  'me.Cell(DataList.lastindex,0)=rsQueryResults.Field("rowid").stringValue
		  'me.CellCheck(DataList.lastindex,1)=True
		  'me.Cell(DataList.lastindex,2)=rsQueryResults.Field("Division_Name").stringValue
		  'me.Cell(DataList.lastindex,3)=rsQueryResults.Field("Stage_Cutoff").stringValue
		  'rsQueryResults.moveNext
		  'wend
		  'rsQueryResults.Close
		End Sub
	#tag EndEvent
	#tag Event
		Function CellClick(row as Integer, column as Integer, x as Integer, y as Integer) As Boolean
		  if column=2 then
		    if DataList.CellCheck(row,1) then
		      DataList.CellCheck(row,1)=false
		    else
		      DataList.CellCheck(row,1)=true
		    end if
		  end if
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events puDistanceSelect
	#tag Event
		Sub Open()
		  
		  dim SelectStatement as String
		  
		  dim rsQueryResults as RecordSet
		  
		  me.AddRow "Select All Distances"
		  me.RowTag(0) = -1
		  SelectStatement="SELECT RaceDistance_Name, rowid FROM racedistances WHERE RaceID =1"
		  rsQueryResults=app.theDB.DBSQLSelect(SelectStatement)
		  while not rsQueryResults.EOF
		    me.AddRow rsQueryResults.Field("RaceDistance_Name").StringValue
		    me.RowTag(me.ListCount-1) = rsQueryResults.Field("rowid").IntegerValue
		    rsQueryResults.MoveNext
		  wend
		  rsQueryResults.Close
		  me.ListIndex=0
		End Sub
	#tag EndEvent
	#tag Event
		Sub Change()
		  dim i, ii as integer
		  dim SQL as String
		  dim rs as RecordSet
		  
		  if me.ListIndex=0 then
		    
		    for i=0 to DataList.ListCount-1
		      DataList.CellCheck(i,1)=True
		    next
		    
		  else
		    
		    for i=0 to DataList.ListCount-1
		      
		      SQL="SELECT RaceDistanceID FROM divisions WHERE RaceDistanceID="+Str(me.RowTag(me.ListIndex))+" AND rowid="+DataList.Cell(i,0)
		      
		      rs=app.theDB.DBSQLSelect(SQL)
		      
		      if rs.RecordCount>0 then
		        DataList.CellCheck(i,1)=True
		      else
		        DataList.CellCheck(i,1)=False
		      end if
		    next
		    
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbOK
	#tag Event
		Sub Action()
		  HTMLResults=false
		  
		  PrintTheResults
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbCancel
	#tag Event
		Sub Action()
		  self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbHTML
	#tag Event
		Sub Action()
		  HTMLResults=True
		  
		  PrintTheResults
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FISUSSAFastestTime"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FISUSSAPenaltyPoints"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
