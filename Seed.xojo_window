#tag Window
Begin Window Seed
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   703
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   697
   MaximizeButton  =   True
   MaxWidth        =   977
   MenuBar         =   -1116165316
   MenuBarVisible  =   True
   MinHeight       =   697
   MinimizeButton  =   True
   MinWidth        =   977
   Placement       =   0
   Resizeable      =   True
   Title           =   "Seeding..."
   Visible         =   True
   Width           =   977
   Begin ListBox DataList
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   2
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   False
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   False
      HeadingIndex    =   -1
      Height          =   350
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   37
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   24.0
      TextUnit        =   0
      Top             =   291
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   906
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin Label RacerName
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   60
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   52
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "RACER, Name"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   48.0
      TextUnit        =   0
      Top             =   19
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   891
   End
   Begin Label RacerNumber
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   60
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   52
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "9999"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   48.0
      TextUnit        =   0
      Top             =   164
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   157
   End
   Begin Label StartTime
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   55
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "00:00:00.000"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   48.0
      TextUnit        =   0
      Top             =   166
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   335
   End
   Begin Label Representing
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   60
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   52
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Representing"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   36.0
      TextUnit        =   0
      Top             =   92
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   891
   End
   Begin Timer SeedTimer
      Height          =   32
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   8
      LockedInPosition=   False
      Mode            =   2
      Period          =   3000
      Scope           =   0
      TabPanelIndex   =   "0"
      Top             =   7
      Width           =   32
   End
   Begin PushButton OK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "OK"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   868
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   657
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PushButton Cancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   769
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   657
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin Label Processing
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   226
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Participant"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   259
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   159
   End
   Begin ProgressBar ProgressBar1
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   12
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   410
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Maximum         =   100
      Scope           =   0
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   263
      Value           =   0
      Visible         =   True
      Width           =   273
   End
   Begin Label ParticipantTotal
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   690
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "999"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   259
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   28
   End
   Begin Label Processed
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   391
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "1"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   259
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   13
   End
   Begin Label GroupProgress
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   226
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Group"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   238
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   159
   End
   Begin ProgressBar ProgressBar2
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   12
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   410
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Maximum         =   100
      Scope           =   0
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   242
      Value           =   0
      Visible         =   True
      Width           =   273
   End
   Begin Label GroupTotal
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   690
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   14
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "999"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   238
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   28
   End
   Begin Label Processed1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   391
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   15
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "1"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   238
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   13
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  SeedTimer.Enabled=false
		  SeedTimer.Period=val(SeedSettings.AssignmentDelay.text)*1000
		  SeedSettingIndex=0
		  SelectParticipantID=0
		  GroupCount=0
		  
		  GroupTotal.text=str(SeedSettings.GroupCount)
		  ProgressBar2.Maximum=SeedSettings.GroupCount
		  
		  DataList.columncount=6
		  DataList.columnwidths="0,0,10%,30%,40%;20%"  
		  
		  DataList.ColumnAlignment(2)=ListBox.AlignCenter
		  DataList.ColumnAlignment(3)=ListBox.AlignLeft
		  DataList.ColumnAlignment(4)=ListBox.AlignLeft
		  DataList.ColumnAlignment(5)=ListBox.AlignCenter
		  
		  
		  RacerName.text="Standby..."
		  Representing.text=""
		  RacerNumber.text=""
		  StartTime.text=""
		  
		  SelectNewGroup
		  SeedTimer.Enabled=true
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Sub AddToDataList(ParticipantID as Integer, RacerNumber as integer, RacerName as string, Representing as string, StartTime as string)
		  dim i As Integer
		  dim AlreadyExists as Boolean
		  dim FormattedRacerNumber as string
		  
		  AlreadyExists=false
		  
		  FormattedRacerNumber=format(RacerNumber,"0000")
		  
		  for i=0 to DataList.ListCount-1
		    if DataList.Cell(i,1)=FormattedRacerNumber then
		      AlreadyExists=true
		      exit
		    end if
		  next
		  
		  if (not(AlreadyExists)) then 'when switching between groups the row gets added again
		    DataList.AddRow ""
		    DataList.Cell(DataList.LastIndex,0)=format(ParticipantID,"##########")
		    DataList.Cell(DataList.LastIndex,1)=FormattedRacerNumber
		    DataList.Cell(DataList.LastIndex,2)=format(RacerNumber,"##########")
		    DataList.Cell(DataList.LastIndex,3)=RacerName
		    DataList.Cell(DataList.LastIndex,4)=Representing
		    DataList.Cell(DataList.LastIndex,5)=StartTime
		    
		    DataList.SortedColumn=1
		    DataList.ColumnSortDirection(1)=1
		    DataList.Sort
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SelectName()
		  dim r as New Random
		  dim NotSeededIndex as Integer
		  dim NotSeededFound as Boolean
		  
		  NotSeededFound=false
		  
		  if SelectParticipantID >0 then
		    AddToDataList(SelectParticipantID,val(RacerNumber.text),RacerName.text,Representing.text,StartTime.text)
		    
		    ProcessedCount=ProcessedCount+1
		    ProgressBar1.value=ProcessedCount
		    ProgressBar1.refresh
		    
		  end if
		  
		  if SeedGroupSeededNumber.IndexOf(0) > -1 then
		    Do 
		      SelectParticipantID=r.InRange(LowID,HighID)
		      NotSeededIndex=SeedGroupParticipantID.IndexOf(SelectParticipantID)
		      if NotSeededIndex>=0 then
		        if SeedGroupSeededNumber(NotSeededIndex)=0 then
		          NotSeededFound=true
		        end if
		      end if
		    Loop Until NotSeededFound
		    
		    RacerName.text=SeedGroupFirstName(NotSeededIndex)+" "+SeedGroupName(NotSeededIndex)
		    Representing.text=SeedGroupRepresenting(NotSeededIndex)
		    RacerNumber.text=""
		    StartTime.text=""
		    SelNameOrNumber="Number"
		  else
		    SelectNewGroup
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SelectNewGroup()
		  dim rsQueryResults as RecordSet
		  dim i, ii, recCount as Integer
		  dim res as Boolean
		  dim SelectStatement as string
		  dim Ghosts as Integer
		  
		  recCount=0
		  
		  'SelectParticipantID=0
		  if SeedSettingIndex<SeedSettings.GroupCount then
		    SelectStatement="SELECT rowid, Participant_Name, First_Name, Representing FROM participants"
		    SelectStatement=SelectStatement+" WHERE RaceID = 1 AND Seed_Group='"+ SeedSettings.SeedGroupSettings.Cell(SeedSettingIndex,1)+"'"
		    SelectStatement=SelectStatement+" ORDER BY rowid"
		    rsQueryResults=App.theDB.DBSQLSelect(SelectStatement)  
		    
		    redim SeedGroupParticipantID(-1)
		    redim SeedGroupName(-1)
		    redim SeedGroupFirstName(-1)
		    redim SeedGroupRepresenting(-1)
		    Redim SeedGroupSeededNumber(-1)
		    
		    while not rsQueryResults.EOF
		      recCount=recCount+1
		      SeedGroupParticipantID.append rsQueryResults.Field("rowid").IntegerValue
		      SeedGroupName.append rsQueryResults.Field("Participant_Name").StringValue
		      SeedGroupFirstName.append rsQueryResults.Field("First_Name").StringValue
		      SeedGroupRepresenting.append rsQueryResults.Field("Representing").StringValue
		      SeedGroupSeededNumber.append(0) 
		      rsQueryResults.MoveNext
		    wend
		    rsQueryResults.Close
		    
		    GroupProgress.text="Group "+SeedSettings.SeedGroupSettings.Cell(SeedSettingIndex,1)+":"
		    GroupCount=GroupCount+1
		    ProgressBar2.value=GroupCount  
		    ProgressBar2.refresh
		    
		    ProcessedCount=0
		    ParticipantTotal.text=str(recCount)
		    ProgressBar1.value=ProcessedCount
		    Progressbar1.Maximum=recCount    
		    ProgressBar1.refresh
		    
		    LowID=SeedGroupParticipantID(0)
		    HighID=SeedGroupParticipantID(recCount-1)
		    
		    Ghosts=val(SeedSettings.SeedGroupSettings.Cell(SeedSettingIndex,2))
		    if Ghosts>0 then
		      // Ghosts before
		      if SeedSettings.SeedGroupSettings.CellCheck(SeedSettingIndex,3) then
		        LowRacerNumber=val(SeedSettings.SeedGroupSettings.Cell(SeedSettingIndex,5))+Ghosts
		        for ii = val(SeedSettings.SeedGroupSettings.Cell(SeedSettingIndex,5)) to LowRacerNumber-1
		          AddToDataList(0,ii,"-","",app.CalculateStartTime(ii,"00:00:00.000",false))
		        next
		      else
		        LowRacerNumber=val(SeedSettings.SeedGroupSettings.Cell(SeedSettingIndex,5))
		      end if
		      
		      //Ghosts after
		      if SeedSettings.SeedGroupSettings.CellCheck(SeedSettingIndex,4) then
		        HighRacerNumber=val(SeedSettings.SeedGroupSettings.Cell(SeedSettingIndex,6))-Ghosts
		        for ii = HighRacerNumber+1 to val(SeedSettings.SeedGroupSettings.Cell(SeedSettingIndex,6))
		          AddToDataList(0,ii,"-","",app.CalculateStartTime(ii,"00:00:00.000",false))
		        next
		      else
		        HighRacerNumber=val(SeedSettings.SeedGroupSettings.Cell(SeedSettingIndex,6))
		      end if
		      
		    else
		      LowRacerNumber=val(SeedSettings.SeedGroupSettings.Cell(SeedSettingIndex,5))
		      HighRacerNumber=val(SeedSettings.SeedGroupSettings.Cell(SeedSettingIndex,6))
		    end if
		    
		    
		    
		    SelNameOrNumber="Name"
		    SeedSettingIndex=SeedSettingIndex+1
		  else
		    SelNameOrNumber="Done"
		    SeedTimer.Enabled=false
		    
		    RacerName.text=""
		    Representing.text=""
		    RacerNumber.text=""
		    StartTime.text=""
		    
		    RacerName.Visible=false
		    Representing.Visible=false
		    RacerNumber.Visible=false
		    StartTime.Visible=false
		    Processing.Visible=false
		    Processed.Visible=false
		    Processed1.Visible=false
		    ProgressBar1.Visible=False
		    ProgressBar2.Visible=False
		    GroupTotal.Visible=false
		    ParticipantTotal.Visible=false
		    GroupProgress.Visible=false
		    Processing.Visible=false
		    
		    
		    DataList.top=20
		    DataList.Height=625
		    
		    Seed.Title="Seed Complete"
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SelectNumber()
		  dim r as New Random
		  dim NumberNotFound as Boolean
		  dim NewRacerNumber as Integer
		  dim index as Integer
		  
		  NumberNotFound=false
		  
		  Do 
		    NewRacerNumber=r.InRange(LowRacerNumber,HighRacerNumber)
		    
		    if SeedGroupSeededNumber.IndexOf(NewRacerNumber)=-1 then
		      NumberNotFound=True
		    else
		    end if
		  Loop Until NumberNotFound
		  
		  SeedGroupSeededNumber(SeedGroupParticipantID.IndexOf(SelectParticipantID))=NewRacerNumber
		  RacerNumber.text=str(NewRacerNumber)
		  StartTime.text=app.CalculateStartTime(NewRacerNumber,"00:00:00.000",false)
		  
		  SelNameOrNumber="Name"
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected GroupCount As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected HighID As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected HighRacerNumber As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected LowID As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected LowRacerNumber As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ProcessedCount As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected SeedGroupFirstName() As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected SeedGroupName() As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected SeedGroupParticipantID() As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected SeedGroupRepresenting() As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected SeedGroupSeededNumber() As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected SeedSettingIndex As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected SelectParticipantID As integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected SelNameOrNumber As string
	#tag EndProperty


#tag EndWindowCode

#tag Events SeedTimer
	#tag Event
		Sub Action()
		  
		  select case SelNameOrNumber
		  case "Name"
		    SelectName
		  case "Number"
		    SelectNumber
		  end select
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OK
	#tag Event
		Sub Action()
		  dim i as Integer
		  dim SQLStatement as String
		  
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  d.icon=1   //display warning icon
		  
		  d.Message="Update Participant Records?"
		  d.Explanation="This will update all the Racer Numbers and Assigned Start Times for the listed participants. There is no undo."
		  d.ActionButton.Caption="Proceed"
		  d.CancelButton.Visible=true
		  b=d.ShowModal  
		  
		  if b=d.ActionButton then
		    Progress.Show
		    Progress.Initialize("Updating","Updating Participant Records...",DataList.ListCount,-1)
		    for i=0 to DataList.ListCount-1
		      if DataList.Cell(i,0)<>"0" then
		        SQLStatement="UPDATE participants SET Racer_Number="+str(val(DataList.Cell(i,1)))
		        SQLStatement=SQLStatement+", Assigned_Start='"+app.RaceDate.SQLDate+" "+DataList.Cell(i,5)+"'"
		        SQLStatement=SQLStatement+" WHERE RaceID=1 AND rowid="+DataList.Cell(i,0)
		        App.theDB.DBSQLExecute(SQLStatement)
		      end if
		      Progress.UpdateProg1(i+1)
		    next
		    app.theDB.DBCommit
		    
		    wnd_List.ExecuteQuery("Participants","","")
		    
		    Progress.Close
		    Seed.Close
		    SeedSettings.Close
		  end if
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Cancel
	#tag Event
		Sub Action()
		  Seed.Close
		  SeedSettings.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
