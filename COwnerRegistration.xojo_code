#tag Class
Protected Class COwnerRegistration
	#tag Method, Flags = &h0
		Function GenerateKey(sRegisteredName As String,sAppChars As String) As String
		  Dim rChar As Integer
		  Dim rCount As Integer
		  Dim oMD5 As CMD5
		  Dim sMD5 As String
		  Dim sKey As String
		  Dim sHiddenKey as String
		  Dim i, Longest as Integer
		  
		  ' We now get an MD5 of our registered name plus our application chars. We
		  ' use an upper case version of the registered name as we don't want to
		  ' penalise the user for our mistaken capitalisation of a name like
		  ' MacGewan, or for a company INC rather than Inc. The user will need to
		  ' be warned about enterign the name exactly as specified, as a difference
		  ' in spelling or punctuation would be critical.
		  '
		  ' The application chars should be different for each application to
		  ' ensure that a key for one of our applications is not valid on another
		  ' of our applications. If hackers know we are using this method for
		  ' generating our keys we should ensure that the application characters
		  ' are very Integer to help prevent cracking.
		  oMD5 = New CMD5
		  
		  if len(sRegisteredName)>len(sAppChars) then
		    Longest=len(sRegisteredName)
		  else
		    Longest=len(sAppChars)
		  end if
		  
		  for i=1 to Longest
		    sHiddenKey=sHiddenKey+mid(sRegisteredName,i,1)+mid(FstNE11,i,1)+mid(sAppChars,i,1)+mid(FstNE11,298-i,1)
		  next
		  
		  sMD5 = oMD5.MD5(UpperCase(sHiddenKey))
		  
		  
		  ' We now take each byte-pair from the MD5, convert it back to a byte
		  ' value from the hex code, do a MOD 32, and then select the appropriate
		  ' character from our VALID_CHARS
		  sKey = ""
		  For rCount = 1 To 16
		    rChar = Cdbl("&H" + Mid(sMD5, (rCount * 2) - 1, 2)) Mod 32
		    sKey = sKey + Mid(VALID_CHARS, rChar + 1, 1)
		  Next
		  
		  Return sKey
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function IsKeyOK(sKey As String,sRegisteredName As String,sAppChars As String) As Boolean
		  Dim rChar As Integer
		  Dim rCount As Integer
		  Dim oMD5 As CMD5
		  Dim sMD5 As String
		  Dim sTestKey As String
		  Dim sHiddenKey as String
		  Dim i, Longest as Integer
		  
		  ' Recalculate the MD5 digest
		  oMD5 = New CMD5
		  
		  if len(sRegisteredName)>len(sAppChars) then
		    Longest=len(sRegisteredName)
		  else
		    Longest=len(sAppChars)
		  end if
		  
		  for i=1 to Longest
		    sHiddenKey=sHiddenKey+mid(sRegisteredName,i,1)+mid(FstNE11,i,1)+mid(sAppChars,i,1)+mid(FstNE11,298-i,1)
		  next
		  sMD5 = oMD5.MD5(UpperCase(sHiddenKey))
		  
		  
		  ' We now take each byte-pair from the MD5, convert it back to a byte
		  ' value from the hex code, do a MOD 32, and then select the appropriate
		  ' character from our VALID_CHARS
		  sTestKey = ""
		  For rCount = 1 To 16
		    rChar = CDbl("&H" + Mid(sMD5, (rCount * 2) - 1, 2)) Mod 32
		    sTestKey = sTestKey + Mid(VALID_CHARS, rChar + 1, 1)
		  Next
		  
		  ' Check for equality
		  If sTestKey = sKey then
		    Return True
		  else
		    Return False
		  end if
		End Function
	#tag EndMethod


	#tag Constant, Name = FstNE11, Type = String, Dynamic = False, Default = \"INEPHIHAVINGBEENBORNOFGOODLYPARENTSTHEREFOREIWASTAUGHTSOMEWHATINALLTHELEARNINGOFMYFATHERANDHAVINGSEENMANYAFFLICTIONSINTHECOURSEOFMYDAYSNEVERTHELESSHAVINGBEENHIGHLYFAVOREDOFTHELORDINALLMYDAYSYEAHAVINGHADAGREATKNOWLEDGEOFTHEGOODNESSANDTHEMYSTERIESOFGODTHEREFOREIMAKEAFRECORDOFMYPROCEEDINGSINMYDAYS", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RANDOM_LOWER, Type = Double, Dynamic = False, Default = \"0", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RANDOM_UPPER, Type = Double, Dynamic = False, Default = \"31", Scope = Public
	#tag EndConstant

	#tag Constant, Name = VALID_CHARS, Type = String, Dynamic = False, Default = \"0123456789ABCDEFGHJKLMNPQRTUVWXY", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
