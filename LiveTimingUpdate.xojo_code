#tag Class
Protected Class LiveTimingUpdate
Inherits Thread
	#tag Event
		Sub Run()
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  d.icon=3   //display questiom icon
		  d.ActionButton.Caption="Upload"
		  d.CancelButton.Visible=True     //show the Cancel button
		  d.CancelButton.Caption="Cancel"
		  d.Message="Do you wish to update the live timing web site?"
		  d.Explanation="This will update the live timing web site."
		  
		  b=d.ShowModal     //display the dialog
		  Select Case b //determine which button was pressed.
		  Case d.ActionButton
		    do  Until Done
		      UpdateData
		    loop
		  Case d.CancelButton
		    //NOP
		  End select
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub SendToRemoteServer(TXCode as string, Time as String)
		  Dim socket1 as New HTTPSocket
		  Dim response, Message as string
		  Dim data as New Dictionary
		  
		  Message="{"+Chr(34)+"chip"+Chr(34)+":"+Chr(34)+TXCode+Chr(34)+","+Chr(34)+"time"+Chr(34)+":"+Chr(34)+Time+Chr(34)+"}"
		  
		  'socket1.SetRequestHeader("Authorization","Basic " + EncodeBase64("ap:racedayblues")) 'Development Server Creds
		  socket1.SetRequestHeader("Authorization","Basic " + EncodeBase64("baee816df28d74bbe3cae51f2e45eb3efe712b4b:ea8cee22a3f3a548dd1b198a5621ecf22c9d61bf")) 'Production Server Creds
		  socket1.SetRequestContent(Message,"application/json")
		  
		  'response = socket1.Post("http://api-dev.athletepath.com/v1/occurrence/"+wnd_List.APRaceID.Text+"/result",10) 'development server
		  response = socket1.Post("http://api.athletepath.com/v1/occurrence/"+wnd_List.APRaceID.Text+"/result",10) ' production server.
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateData()
		  dim SQL, tables(), output as string
		  dim i, ii as  integer
		  dim rsTimes, rsParticipants, rsccTeamIntervals as RecordSet
		  dim xml as XmlDocument
		  dim table, tablerows, row, column as XmlNode
		  
		  if App.PushIntervalAndFinalTimes then
		    SQL = "SELECT rowid, Total_Time, TX_Code FROM times WHERE Use_This_Passing  = 'Y' AND UpdateFlag = 'Y' AND Interval_Number > 0 ORDER BY TX_Code, Interval_Number"
		  else
		    SQL = "SELECT rowid, Total_Time, TX_Code FROM times WHERE Use_This_Passing  = 'Y' AND UpdateFlag = 'Y' AND Interval_Number 99 ORDER BY TX_Code, Interval_Number"
		  end if
		  rsTimes = app.theDB.DBSQLSelect(SQL)
		  
		  if (rsTimes.RecordCount>0) then
		    
		    for i = 1 to rsTimes.RecordCount
		      
		      SendToRemoteServer(rsTimes.Field("TX_Code").StringValue,rsTimes.Field("Total_Time").StringValue)
		      
		      'update the local participant record so we don't process it again
		      SQL = "UPDATE times SET UpdateFlag='N' WHERE rowid = "+rsTimes.Field("rowid").StringValue
		      app.theDB.DBSQLExecute(SQL)
		      rsTimes.MoveNext
		    next
		    
		    rsTimes.Close
		  end if
		  
		  self.Sleep(100)
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Done As Boolean = false
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Done"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Priority"
			Visible=true
			Group="Behavior"
			InitialValue="5"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StackSize"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
