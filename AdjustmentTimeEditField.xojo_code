#tag Class
Protected Class AdjustmentTimeEditField
Inherits TextField
	#tag Event
		Sub GotFocus()
		  me.SelStart=0
		  me.SelLength=1
		End Sub
	#tag EndEvent

	#tag Event
		Function KeyDown(Key As String) As Boolean
		  dim code as integer
		  
		  OldValue=me.SelText
		  
		  code=asc(Key)
		  select case asc(Key)
		  case 8
		    DeletePosition=me.SelStart
		    if DeletePosition=0 then
		      DeletePosition=1
		    end if
		    BackSpace=true
		    
		  case 127
		    DeletePosition=me.SelStart
		    DeleteKey=true
		    
		  case 28
		    me.SelStart=me.SelStart-1
		    me.SelLength=1
		    select case me.SelText
		    case "-"
		      if me.SelStart>0 then
		        me.SelStart=me.SelStart-1
		        me.SelLength=1
		      end if
		    case ":"
		      me.SelStart=me.SelStart-1
		      me.SelLength=1
		    case " "
		      me.SelStart=me.SelStart-1
		      me.SelLength=1
		    case "."
		      me.SelStart=me.SelStart-1
		      me.SelLength=1
		    end Select
		  end Select
		End Function
	#tag EndEvent

	#tag Event
		Sub SelChange()
		  me.SelLength=1
		  select case me.SelText
		  case "-"
		    if me.SelStart>0 then
		      me.SelStart=me.SelStart+1
		      me.SelLength=1
		    end if
		  case ":"
		    me.SelStart=me.SelStart+1
		    me.SelLength=1
		  case " "
		    me.SelStart=me.SelStart+1
		    me.SelLength=1
		  case "."
		    me.SelStart=me.SelStart+1
		    me.SelLength=1
		  end Select
		End Sub
	#tag EndEvent

	#tag Event
		Sub TextChange()
		  dim SelectedDigitValue as Integer
		  dim DataEntryError as Boolean
		  dim Position as integer
		  
		  if len(me.Text)<=MaxLength then
		    
		    DataEntryError=false
		    
		    if DeletePosition>0 then
		      me.text=left(me.text,DeletePosition)+"0"+mid(me.text,DeletePosition+1)
		      if BackSpace then
		        me.SelStart=DeletePosition-1
		        BackSpace=false
		      elseif DeleteKey then
		        me.SelStart=DeletePosition+2
		        DeleteKey=false
		      end if
		      me.SelLength=1
		      select case me.SelText
		      case "-"
		        me.SelStart=me.SelStart-1
		        me.SelLength=1
		      case ":"
		        me.SelStart=me.SelStart-1
		        me.SelLength=1
		      case " "
		        me.SelStart=me.SelStart-1
		        me.SelLength=1
		      case "."
		        me.SelStart=me.SelStart-1
		        me.SelLength=1
		      end Select
		      DeletePosition=0
		    end if
		    
		    SelectedDigitValue=val(mid(me.text,me.SelStart,1))
		    select case me.SelStart
		    case 1
		      if (left(me.Text,1)<>"+") and (left(me.Text,1)<>"-") then
		        DataEntryError=true
		      end if
		      
		    case 5
		      if SelectedDigitValue>5 then
		        DataEntryError=true
		      end if
		      
		    case 6
		      If not(ValidMinutes) then
		        DataEntryError=true
		      end if
		      
		    case 8
		      if SelectedDigitValue>5 then
		        DataEntryError=true
		      end if
		      
		    case 9
		      if not(ValidSeconds) then
		        DataEntryError=true
		      end if
		      
		    end Select
		    
		    if DataEntryError then
		      beep    
		      Position=me.SelStart
		      me.text=left(me.text,Position-1)+OldValue+mid(me.text,Position+1)
		      me.SelStart=Position-1
		      me.SelLength=1
		    end if
		    
		    
		  else
		    beep
		    me.text=left(me.text,MaxLength)
		    me.SelStart=MaxLength+1
		    me.SelLength=1
		  end if
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Function ValidMinutes() As Boolean
		  if val(mid(me.text,5,2)) > 59 then
		    return false
		  else
		    Return true
		  end if
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ValidSeconds() As Boolean
		  if val(mid(me.text,8,2)) > 59 then
		    return false
		  else
		    Return true
		  end if
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private BackSpace As boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private DeleteKey As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private DeletePosition As integer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private OldValue As string
	#tag EndProperty


	#tag Constant, Name = MaxLength, Type = Double, Dynamic = False, Default = \"13", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AcceptTabs"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Alignment"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Default"
				"1 - Left"
				"2 - Center"
				"3 - Right"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutoDeactivate"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AutomaticallyCheckSpelling"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="BackColor"
			Visible=true
			Group="Appearance"
			InitialValue="&hFFFFFF"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Bold"
			Visible=true
			Group="Font"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Border"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CueText"
			Visible=true
			Group="Initial State"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DataField"
			Visible=true
			Group="Database Binding"
			Type="String"
			EditorType="DataField"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DataSource"
			Visible=true
			Group="Database Binding"
			Type="String"
			EditorType="DataSource"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Format"
			Visible=true
			Group="Appearance"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Position"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Appearance"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Italic"
			Visible=true
			Group="Font"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LimitText"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Position"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Mask"
			Visible=true
			Group="Behavior"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Password"
			Visible=true
			Group="Appearance"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ReadOnly"
			Visible=true
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabIndex"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabPanelIndex"
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabStop"
			Visible=true
			Group="Position"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Text"
			Visible=true
			Group="Initial State"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextColor"
			Visible=true
			Group="Appearance"
			InitialValue="&h000000"
			Type="Color"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextFont"
			Visible=true
			Group="Font"
			InitialValue="System"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextSize"
			Visible=true
			Group="Font"
			InitialValue="0"
			Type="Single"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TextUnit"
			Visible=true
			Group="Font"
			InitialValue="0"
			Type="FontUnits"
			EditorType="Enum"
			#tag EnumValues
				"0 - Default"
				"1 - Pixel"
				"2 - Point"
				"3 - Inch"
				"4 - Millimeter"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Underline"
			Visible=true
			Group="Font"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UseFocusRing"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Appearance"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Position"
			InitialValue="80"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
