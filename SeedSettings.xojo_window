#tag Window
Begin Window SeedSettings
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   326
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   613
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   326
   MinimizeButton  =   True
   MinWidth        =   613
   Placement       =   0
   Resizeable      =   True
   Title           =   "Seed Settings"
   Visible         =   True
   Width           =   613
   Begin PopupMenu AssignmentDelay
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "0.01\r0.25\r0.5\r1\r2\r3\r4\r5\r10\r15\r20\r30\r60"
      Italic          =   False
      Left            =   303
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   26
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   134
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Assignment Delay:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   26
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   163
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   395
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Seconds"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   28
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin TextField FirstRacerNumber
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   303
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "1"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   53
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   80
   End
   Begin Label StaticText3
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   143
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "First Racer Number:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   54
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   154
   End
   Begin ListBox SeedGroupSettings
      AutoDeactivate  =   True
      AutoHideScrollbars=   True
      Bold            =   False
      Border          =   True
      ColumnCount     =   2
      ColumnsResizable=   False
      ColumnWidths    =   ""
      DataField       =   ""
      DataSource      =   ""
      DefaultRowHeight=   -1
      Enabled         =   True
      EnableDrag      =   False
      EnableDragReorder=   True
      GridLinesHorizontal=   0
      GridLinesVertical=   0
      HasHeading      =   True
      HeadingIndex    =   -1
      Height          =   95
      HelpTag         =   ""
      Hierarchical    =   False
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      RequiresSelection=   False
      Scope           =   0
      ScrollbarHorizontal=   False
      ScrollBarVertical=   True
      SelectionType   =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   168
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   551
      _ScrollOffset   =   0
      _ScrollWidth    =   -1
   End
   Begin Label NumberRacers
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   192
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Number of Racers: 999"
      TextAlign       =   1
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   138
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   217
   End
   Begin Label StaticText11
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   176
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Start Interval:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   82
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   122
   End
   Begin ComboBox Interval
      AutoComplete    =   False
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialValue    =   ""
      Italic          =   False
      Left            =   303
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   82
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   160
   End
   Begin PushButton OK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "OK"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   479
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   False
      Scope           =   0
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   277
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PushButton Cancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   380
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   False
      Scope           =   0
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   277
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin TextField RacersPerStart
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   303
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "1"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   110
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   80
   End
   Begin Label StaticText4
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   143
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Racers Per Start:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   14.0
      TextUnit        =   0
      Top             =   111
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   154
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  dim i as Integer
		  
		  NumberRacers.text="Number Racers: "+wnd_List.ParticipantsFound.Text
		  Interval.addrow "Select..."
		  Interval.addrow "5 Sec"
		  Interval.addrow "10 Sec"
		  Interval.addrow "15 Sec"
		  Interval.addrow "20 Sec"
		  Interval.addrow "30 Sec"
		  Interval.addrow "60 Sec"
		  Interval.addrow "90 Sec"
		  Interval.addrow "120 Sec"
		  
		  for i=0 to Interval.ListCount-1
		    Interval.ListIndex=i
		    if Interval.text = str(app.StartIntervalSec)+" Sec" then
		      exit
		    end if
		  next
		  if i=Interval.ListCount then
		    Interval.ListIndex=0
		    Interval.text = str(app.StartIntervalSec)+" Sec" 
		  end if
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub CalculateRacerNumberRange()
		  dim i,startingNumber,endingNumber As Integer
		  
		  startingNumber=val(FirstRacerNumber.text)-1
		  
		  For  i=0 to SeedGroupSettings.ListCount-1
		    SeedGroupSettings.Cell(i,5)=str(startingNumber+1)
		    
		    endingNumber=startingNumber+val(SeedGroupSettings.Cell(i,0))
		    
		    if (SeedGroupSettings.CellCheck(i,3)) then
		      endingNumber=endingNumber+val(SeedGroupSettings.Cell(i,2))
		    end if
		    if (SeedGroupSettings.CellCheck(i,4)) then
		      endingNumber=endingNumber+val(SeedGroupSettings.Cell(i,2))
		    end if
		    
		    SeedGroupSettings.Cell(i,6)=str(endingNumber)
		    startingNumber=endingNumber
		  next
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		GroupCount As integer
	#tag EndProperty


#tag EndWindowCode

#tag Events FirstRacerNumber
	#tag Event
		Sub Open()
		  FirstRacerNumber.text=str(app.StartingRacerNumber)
		End Sub
	#tag EndEvent
	#tag Event
		Sub LostFocus()
		  dim SQLStatement as String
		  
		  CalculateRacerNumberRange
		  
		  SQLStatement = "UPDATE races SET Starting_Racer_Number='"+FirstRacerNumber.text+"' WHERE RaceID=1"
		  App.theDB.DBSQLExecute(SQLStatement)
		  
		  app.StartingRacerNumber=val(FirstRacerNumber.Text)
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events SeedGroupSettings
	#tag Event
		Sub Open()
		  dim rsDistinctSeedGroup, rsQueryResults as RecordSet
		  dim res as Boolean
		  dim SelectStatement as string
		  
		  Me.columncount=7  
		  Me.columnwidths="0,16%,16%,16%,16%,16%"  
		  me.ColumnType(2)=listbox.TypeEditable
		  me.ColumnType(3)=listbox.TypeCheckbox
		  me.ColumnType(4)=listbox.TypeCheckbox
		  
		  Me.ColumnAlignment(1)=ListBox.AlignCenter
		  Me.ColumnAlignment(2)=ListBox.AlignCenter
		  Me.ColumnAlignment(3)=ListBox.AlignLeft
		  Me.ColumnAlignment(4)=ListBox.AlignLeft
		  Me.ColumnAlignment(5)=ListBox.AlignCenter
		  Me.ColumnAlignment(6)=ListBox.AlignCenter
		  
		  Me.heading(1)="Group"
		  Me.heading(2)="Num Ghosts"
		  Me.heading(3)=" "
		  Me.heading(4)=" "
		  Me.heading(5)="Start"
		  Me.heading(6)="End"
		  
		  SelectStatement="SELECT DISTINCT seed_group FROM participants WHERE RaceID =1"
		  rsDistinctSeedGroup=App.theDB.DBSQLSelect(SelectStatement) 
		  
		  GroupCount=0
		  while not rsDistinctSeedGroup.eof
		    GroupCount=GroupCount+1
		    SelectStatement="SELECT Racer_Number FROM participants WHERE RaceID =1 AND Seed_Group='"+rsDistinctSeedGroup.field("seed_group").stringValue+"'"
		    rsQueryResults=app.theDB.DBSQLSelect(SelectStatement) 
		    me.AddRow ""
		    me.Cell(me.lastindex,0)=str(rsQueryResults.RecordCount)
		    me.Cell(me.lastindex,1)=rsDistinctSeedGroup.field("seed_group").StringValue
		    me.Cell(me.LastIndex,2)="0"
		    me.Cell(me.lastindex,3)="Before"
		    me.Cell(me.LastIndex,4)="After"
		    me.CellCheck(me.LastIndex,3)=false
		    me.CellCheck(me.LastIndex,4)=false
		    rsDistinctSeedGroup.MoveNext
		  Wend
		  
		  CalculateRacerNumberRange
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub Change()
		  CalculateRacerNumberRange
		End Sub
	#tag EndEvent
	#tag Event
		Sub CellAction(row As Integer, column As Integer)
		  if Column=3 or Column=4 then
		    CalculateRacerNumberRange
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Interval
	#tag Event
		Sub LostFocus()
		  dim SQLStatement as String
		  
		  SQLStatement = "UPDATE races SET Start_Interval='"+Interval.text+"' WHERE RaceID=1"
		  App.theDB.DBSQLExecute(SQLStatement)
		  
		  app.StartIntervalSec=val(Interval.text)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OK
	#tag Event
		Sub Action()
		  Seed.Show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Cancel
	#tag Event
		Sub Action()
		  SeedSettings.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events RacersPerStart
	#tag Event
		Sub Open()
		  RacersPerStart.text=str(app.RacersPerStart)
		End Sub
	#tag EndEvent
	#tag Event
		Sub LostFocus()
		  dim SQLStatement as String
		  
		  CalculateRacerNumberRange
		  
		  SQLStatement = "UPDATE races SET Racers_Per_Start='"+RacersPerStart.text+"' WHERE RaceID=1"
		  App.theDB.DBSQLExecute(SQLStatement)
		  
		  app.RacersPerStart=val(RacersPerStart.Text)
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="GroupCount"
		Group="Behavior"
		InitialValue="0"
		Type="integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
