#tag Class
Protected Class ProcessTimingData
Inherits Thread
	#tag Event
		Sub Run()
		  dim TimeSource as integer
		  dim TimeTime as string
		  dim TimeTXCode as String
		  dim TimeType as String
		  dim TimeTimingPointID as Integer
		  Dim t as Integer
		  
		  Done = False
		  if app.WebServer<>"" then
		    'SMTPSocket1=new SMTPSocket
		    'SMTPSocket1.Address=app.MailServer(0)
		    'SMTPSocket1.Port=val(app.MailServer(1))
		  end if
		  
		  do  Until Done
		    if Ubound(App.TimeTXCode)>=0 then
		      
		      if App.TimeTXCode(0)<>"" then 'this is the last item to be set so if if is not, the rest of the arrays are not ready to process
		        TimeSource=App.TimeSource(0)
		        TimeTime=App.TimeTime(0)
		        TimeType=App.TimeType(0)
		        TimeTXCode=App.TimeTXCode(0)
		        TimeTimingPointID=App.TimeTimingPointID(0)
		        App.TimeSource.Remove 0
		        App.TimeTime.Remove 0
		        App.TimeType.Remove 0
		        App.TimeTXCode.Remove 0
		        App.TimeTimingPointID.Remove 0
		        
		        select case TimeSource
		        case 0 'Start Time
		          AcceptStartTime(TimeTXCode,TimeTime,TimeType,TimeTimingPointID)
		          
		        case 9998 'Multiple Intervals at the same point
		          AcceptMultIntervalTime(TimeTXCode,TimeTime,TimeTimingPointID)
		          
		        case 9999 'Stop Time
		          AcceptStopTime(TimeTXCode,TimeTime,TimeType,TimeTimingPointID)
		          
		        else 'Named intervals
		          AcceptNamedIntTime(TimeTXCode,TimeTime,TimeSource,TimeTimingPointID)
		          
		        end Select    
		      end if
		      
		    else
		      
		      self.Sleep(100)
		      
		    end if
		    
		    App.OKToProcessArrays=true
		    
		  loop
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub AcceptMultIntervalTime(TXCode as string, Time as string, TimingPointNumber as integer)
		  dim rsParticipant,rsTime,rsInterval as RecordSet
		  dim PassingFlag, SQLStatement, aTimeID(), TotalTime, OldTime, IntervalTime, Pace, FastestTime, PlaceStg as String
		  dim i, n, ParticipantID, SOA as Integer
		  dim res, TimeNotFound as Boolean
		  dim TimeInSeconds,aTimes() as Double
		  
		  TimeInSeconds=app.ConvertTimeToSeconds(Time)
		  
		  'find all related interval times for the Participant/TXCode and Order by acutal time
		  '  But first locate the Participant record
		  SQLStatement="SELECT divisions.RaceDistanceID, DivisionID, TeamID, participants.rowid, participants.Gender, Racer_Number, Assigned_Start, Actual_Start, First_Name, Participant_Name, SMS_Address FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		  SQLStatement = SQLStatement + "WHERE Racer_Number ="+app.GetRacerNumber(TXCode)+" "
		  rsParticipant=App.theDB.DBSQLSelect(SQLStatement)
		  ParticipantID=rsParticipant.Field("rowid").IntegerValue
		  
		  if ((ParticipantID>0) and ((Time > rsParticipant.Field("Assigned_Start").StringValue and app.CalculateTimesFrom="Assigned") _
		    or (Time > rsParticipant.Field("Actual_Start").StringValue and app.CalculateTimesFrom="Actual"))) then
		    
		    if not rsParticipant.EOF then
		      if app.CalculateTimesFrom="Assigned" then
		        TotalTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(Time)-App.ConvertTimeToSeconds(rsParticipant.Field("Assigned_Start").StringValue),false)
		      else
		        TotalTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(Time)-App.ConvertTimeToSeconds(rsParticipant.Field("Actual_Start").StringValue),false)
		      end if
		    end If
		    
		    if app.PlacesToTruncate=0 then
		      TotalTime=Left(TotalTime,len(TotalTime)-4)
		    else
		      TotalTime=Left(TotalTime,len(TotalTime)-(3-app.PlacesToTruncate))
		    end if
		    
		    SQLStatement="SELECT rowid, Actual_Time FROM times "
		    SQLStatement = SQLStatement+"WHERE RaceID=1"
		    if not(rsParticipant.EOF) then
		      SQLStatement = SQLStatement+" AND ParticipantID="+str(ParticipantID)
		    else
		      SQLStatement = SQLStatement+" AND TX_Code='"+TXCode+"'"
		    end if
		    SQLStatement = SQLStatement+" AND Use_This_Passing='Y' AND Interval_Number>0 AND Interval_Number<9990"
		    SQLStatement = SQLStatement+" ORDER BY Actual_Time"
		    rsTime=app.theDB.DBSQLSelect(SQLStatement)
		    
		    'Load found records into arrays
		    while not rsTime.EOF
		      aTimeID.append rsTime.Field("rowid").StringValue
		      aTimes.append app.ConvertTimeToSeconds(rsTime.Field("Actual_Time").StringValue)
		      rsTime.MoveNext
		    wend
		    
		    'find where the time should go in the list of found record while accounting for the interval delay
		    SOA=UBound(aTimeID)
		    if SOA>=0 then
		      TimeNotFound=True
		      for i=0 to SOA
		        select case i        
		        case SOA
		          if (TimeInSeconds>(aTimes(i)+app.MinimumLapTime)) then
		            aTimeID.append "0"
		            aTimes.append TimeInSeconds
		            TimeNotFound=false
		          end if
		          
		        case 0
		          if (TimeInSeconds<(aTimes(i)-app.MinimumLapTime)) then
		            aTimeID.Insert(i,"0")
		            aTimes.Insert(i,TimeInSeconds)
		            TimeNotFound=false
		          end if
		          
		        case 1 to SOA-1
		          if ((TimeInSeconds>(aTimes(i-1)+app.MinimumLapTime)) and (TimeInSeconds<(aTimes(i)-app.MinimumLapTime))) then
		            aTimeID.Insert(i,"0")
		            aTimes.Insert(i,TimeInSeconds)
		            TimeNotFound=false
		          end if
		          
		        end select
		      next
		      
		      if TimeNotFound then
		        AddTimeRecord(ParticipantID,TXCode,9990,TimingPointNumber,Time,"00:00:00.000",TotalTime,"N")
		      end if
		    else
		      aTimeID.append "0"
		      aTimes.append TimeInSeconds
		    end if
		    
		    'save the time record updating the interval numbers appropriatly
		    FastestTime="99:00:00.000"
		    OldTime=app.ConvertSecondsToTime(aTimes(0),false)
		    for i=0 to UBound(aTimeID)
		      if i = 0 then
		        if app.CalculateTimesFrom="Assigned" then
		          IntervalTime=app.ConvertSecondsToTime(aTimes(i)-App.ConvertTimeToSeconds(rsParticipant.Field("Assigned_Start").StringValue),false)
		        else
		          IntervalTime=app.ConvertSecondsToTime(aTimes(i)-App.ConvertTimeToSeconds(rsParticipant.Field("Actual_Start").StringValue),false)
		        end if
		      else
		        IntervalTime=app.ConvertSecondsToTime(aTimes(i)-App.ConvertTimeToSeconds(OldTime),false)
		      end if
		      OldTime=app.ConvertSecondsToTime(aTimes(i),false)
		      if app.PlacesToTruncate=0 then
		        IntervalTime=Left(IntervalTime,len(IntervalTime)-4)
		      else
		        IntervalTime=Left(IntervalTime,len(IntervalTime)-(3-app.PlacesToTruncate))
		      end if
		      
		      if IntervalTime<FastestTime then
		        FastestTime=IntervalTime
		      end if
		      
		      if aTimeID(i)<>"0" then  'if ID>0 then update the time record with the lap number
		        SQLStatement="UPDATE times SET Interval_Number="+str(i+1)+", Interval_Time='"+IntervalTime+"' WHERE RaceID=1 AND ID="+aTimeID(i)
		        App.theDB.DBSQLExecute(SQLStatement)
		      else 'else add the time record
		        PassingFlag="Y"
		        AddTimeRecord(ParticipantID,TXCode,i+1,TimingPointNumber,Time,IntervalTime,TotalTime,PassingFlag)
		        
		        if app.SendTweet and PassingFlag="Y" then
		          SQLStatement="SELECT rowid, Interval_Name, Actual_Distance FROM intervals "
		          SQLStatement = SQLStatement + "WHERE RaceID=1 AND Number ="+str(i+1)+" "
		          SQLStatement = SQLStatement + "AND DivisionID="+rsParticipant.Field("DivisionID").StringValue
		          rsInterval=app.theDB.DBSQLSelect(SQLStatement)
		          if app.SendTweet and app.TweetOverall then
		            PlaceStg=app.GetIntervalPlace("Overall",rsParticipant.Field("RaceDistanceID").StringValue,rsParticipant.Field("DivisionID").StringValue,rsParticipant.Field("Gender").StringValue,str(i+1),TotalTime,"Total Time",true)
		          else
		            PlaceStg=app.GetIntervalPlace("Division",rsParticipant.Field("RaceDistanceID").StringValue,rsParticipant.Field("DivisionID").StringValue,rsParticipant.Field("Gender").StringValue,str(i+1),TotalTime,"Total Time",true)
		          end if
		          if  val(PlaceStg) <= app.PlacesToTweet then
		            SendTweet(rsParticipant.Field("Racer_Number").StringValue, rsParticipant.Field("First_Name").StringValue,rsParticipant.Field("Participant_Name").StringValue,Time,TotalTime,"0",rsInterval.Field("Interval_Name").StringValue,PlaceStg)
		          end if
		        end if
		        
		      end if
		      
		      if ((wnd_List.ResultType.Text="Cross Country Running") and (rsParticipant.Field("TeamID").StringValue<>"0")) then
		        UpdateCrossCountryTeamScore(rsParticipant.Field("TeamID").StringValue,str(i+1),rsParticipant.Field("RaceDistanceID").StringValue)
		      end if
		      
		    Next
		    
		    if app.JamSession="Y" then
		      SQLStatement="UPDATE participants SET DNF='N', DNS='N', Total_Time='"+FastestTime+"' WHERE rowid="+Format(ParticipantID,"###########")
		      App.theDB.DBSQLExecute(SQLStatement)
		      wnd_List.UpdateParticipantListTime(rsParticipant.Field("rowid").StringValue,9999,TotalTime,"TT")
		      wnd_List.DataList_Participants.Refresh
		    else
		      SQLStatement="UPDATE participants SET DNS='N', Laps_Completed="+str(UBound(aTimeID)+1)+" WHERE rowid="+Format(ParticipantID,"###########")
		      App.theDB.DBSQLExecute(SQLStatement)
		      wnd_List.UpdateParticipantListTime(rsParticipant.Field("rowid").StringValue,0,"DNF","TT")
		      wnd_List.DataList_Participants.Refresh
		    end if
		  else
		    'Time occured before the racer's assigned and/or actual start, so need to ignore it.
		    AddTimeRecord(ParticipantID,TXCode,9990,TimingPointNumber,Time,"00:00:00.000","00:00:00.000","N")
		  end if
		  rsParticipant.Close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub AcceptNamedIntTime(TXCode as string, Time as string, IntervalNumber as integer, TimingPointNumber as integer)
		  dim rsParticipant,rsInterval,rsTime,rsLaps as RecordSet
		  dim SQLStatement, ParticipantSQLStatement, PlaceStg,OverallBack, NetTime, TotalTime, IntTime, UseThisPassing, tempActTime, tempAssTime, IntervalName, LapsCompleted as String
		  dim DivisionID, n, ParticipantID, Place as Integer
		  dim res, PlaceRecord as Boolean
		  
		  Place=0
		  OverallBack="+00:00:00.000"
		  TotalTime="00:00:00.000"
		  UseThisPassing="Y"
		  IntTime="00:00:00.000"
		  
		  'get participant's division ID
		  SQLStatement="SELECT divisions.RaceDistanceID, DivisionID, TeamID, participants.rowid, Racer_Number, Assigned_Start, Actual_Start, First_Name, Participant_Name, SMS_Address, participants.Gender FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		  SQLStatement = SQLStatement + "WHERE participants.RaceID=1 AND Racer_Number ="+app.GetRacerNumber(TXCode)+" "
		  rsParticipant=app.theDB.DBSQLSelect(SQLStatement)
		  ParticipantID=rsParticipant.Field("rowid").IntegerValue
		  DivisionID=rsParticipant.Field("DivisionID").IntegerValue
		  if not rsParticipant.EOF then
		    if ((Time>rsParticipant.Field("Assigned_Start").StringValue) and (Time>rsParticipant.Field("Actual_Start").StringValue)) then
		      if app.CalculateTimesFrom="Assigned" then
		        TotalTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(Time)-App.ConvertTimeToSeconds(rsParticipant.Field("Assigned_Start").StringValue),false)
		      else
		        TotalTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(Time)-App.ConvertTimeToSeconds(rsParticipant.Field("Actual_Start").StringValue),false)
		      end if
		    else
		      TotalTime="00:00:00.000"
		    end if
		  else
		    TotalTime="00:00:00.000"
		  end if
		  
		  if app.PlacesToTruncate=0 then
		    TotalTime=Left(TotalTime,len(TotalTime)-4)
		  else
		    TotalTime=Left(TotalTime,len(TotalTime)-(3-app.PlacesToTruncate))
		  end if
		  
		  tempAssTime=rsParticipant.Field("Assigned_Start").StringValue
		  tempActTime=rsParticipant.Field("Actual_Start").StringValue
		  
		  if ((Time>rsParticipant.Field("Assigned_Start").StringValue) and (Time>rsParticipant.Field("Actual_Start").StringValue)) then
		    SQLStatement="SELECT rowid, Actual_Time FROM times "
		    SQLStatement = SQLStatement+"WHERE RaceID=1"
		    if not(rsParticipant.EOF) and wnd_List.ResultType.text="Cross Country Running" then
		      SQLStatement = SQLStatement+" AND ParticipantID="+str(ParticipantID)+" AND "
		    else
		      SQLStatement = SQLStatement+" AND TX_Code='"+TXCode+"' AND "
		    end if
		    SQLStatement = SQLStatement+"Interval_Number="+str(IntervalNumber)+" AND "
		    SQLStatement = SQLStatement+"Use_This_Passing='Y'"
		    rsTime=app.theDB.DBSQLSelect(SQLStatement)
		    
		    'found an existing record... 
		    if (not(rsTime.EOF)) then
		      'if incoming time is less than existing time
		      if (Time<rsTime.Field("Actual_Time").StringValue) then
		        '  mark record as UseThisPassing='N'
		        SQLStatement="UPDATE times SET Use_This_Passing='N' WHERE RaceID=1 AND rowid="+rsTime.Field("rowid").StringValue
		        App.theDB.DBSQLExecute(SQLStatement)
		        '  place the record
		        '  save it for positerity with UseThisPassing="Y"
		        PlaceRecord=true
		        UseThisPassing="Y"
		      else
		        PlaceRecord=false
		        'if incoming time is greater than existing time, save the record for posterity
		        UseThisPassing="N"
		      end if
		    else
		      'no incoming record
		      '  place the record 
		      '  save it for posterity with UseThisPassing="Y"
		      PlaceRecord=True
		      UseThisPassing="Y"
		    end if
		    rsTime.Close
		  else
		    PlaceRecord=false
		    UseThisPassing="N"
		  end if
		  
		  if rsParticipant.eof then  'can't place a time if you don't where to place it
		    PlaceRecord=false
		  end if
		  
		  if(PlaceRecord) then
		    SQLStatement="SELECT rowid, Interval_Name, Actual_Distance FROM intervals "
		    SQLStatement = SQLStatement + "WHERE RaceID=1 AND Number ="+str(IntervalNumber)+" "
		    SQLStatement = SQLStatement + "AND DivisionID="+str(DivisionID)
		    rsInterval=app.theDB.DBSQLSelect(SQLStatement)
		    
		    if IntervalNumber>1 then
		      
		      SQLStatement="SELECT rowid, Actual_Time FROM times "
		      SQLStatement = SQLStatement+"WHERE RaceID=1"
		      if not(rsParticipant.EOF) then
		        SQLStatement = SQLStatement+" AND ParticipantID="+str(ParticipantID)+" AND "
		      else
		        SQLStatement = SQLStatement+" AND TX_Code='"+TXCode+"' AND "
		      end if
		      SQLStatement = SQLStatement+"Interval_Number="+str(IntervalNumber-1)
		      rsTime=app.theDB.DBSQLSelect(SQLStatement)
		      if not(rsTime.EOF) then
		        IntTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(Time)-App.ConvertTimeToSeconds(rsTime.Field("Actual_Time").StringValue),false)
		      end if
		    else
		      IntTime=TotalTime
		    end if
		    if app.SendTweet and app.TweetOverall then
		      PlaceStg=app.GetIntervalPlace("Overall",rsParticipant.Field("RaceDistanceID").StringValue,str(DivisionID),rsParticipant.Field("Gender").StringValue,str(IntervalNumber),IntTime,"Total Time",true)
		    else
		      PlaceStg=app.GetIntervalPlace("Division",rsParticipant.Field("RaceDistanceID").StringValue,str(DivisionID),rsParticipant.Field("Gender").StringValue,str(IntervalNumber),IntTime,"Total Time",true)
		    end if
		    IntervalName=rsInterval.Field("Interval_Name").StringValue
		    rsInterval.Close
		  end if
		  
		  if UseThisPassing="Y" then
		    SQLStatement="UPDATE participants SET Laps_Completed=Laps_Completed+1, DNS='N' WHERE rowid="+format(ParticipantID,"##########")
		    wnd_List.UpdateParticipantListTime(format(ParticipantID,"##########"),0,"DNF","TX")
		    wnd_List.DataList_Participants.Refresh
		    App.theDB.DBSQLExecute(SQLStatement)
		    SQLStatement="SELECT Laps_Completed FROM participants WHERE rowid="+format(ParticipantID,"##########")
		    rsLaps=app.theDB.DBSQLSelect(SQLStatement)
		    LapsCompleted=rsLaps.Field("Laps_Completed").StringValue
		    rsLaps.Close
		    wnd_List.RecalcTimes_UpdateIntervals(format(ParticipantID,"##########"),rsParticipant.Field("Assigned_Start").StringValue,rsParticipant.Field("Actual_Start").StringValue)
		  else
		    LapsCompleted="0"
		  end if
		  
		  'Send SMS Message Here
		  if app.SendSMS and rsParticipant.Field("SMS_Address").StringValue<>"" and UseThisPassing="Y" then
		    SendSMSMessage(rsParticipant.Field("Racer_Number").StringValue, rsParticipant.Field("First_Name").StringValue,rsParticipant.Field("Participant_Name").StringValue,Time,TotalTime,LapsCompleted,IntervalName,PlaceStg,rsParticipant.Field("SMS_Address").StringValue)
		  end if
		  
		  if app.SendTweet and val(PlaceStg) <= app.PlacesToTweet and UseThisPassing="Y" then
		    SendTweet(rsParticipant.Field("Racer_Number").StringValue, rsParticipant.Field("First_Name").StringValue,rsParticipant.Field("Participant_Name").StringValue,Time,TotalTime,LapsCompleted,IntervalName,PlaceStg)
		  end if
		  
		  AddTimeRecord(ParticipantID,TXCode,IntervalNumber,TimingPointNumber,Time,IntTime,TotalTime,UseThisPassing)
		  
		  if ((wnd_List.ResultType.Text="Cross Country Running") and (UseThisPassing="Y") and (ParticipantID<>0) and (rsParticipant.Field("TeamID").StringValue<>"0")) then
		    UpdateCrossCountryTeamScore(rsParticipant.Field("TeamID").StringValue,str(IntervalNumber),rsParticipant.Field("RaceDistanceID").StringValue)
		  end if
		  
		  rsParticipant.Close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub AcceptStartTime(TXCode as string, Time as string, Type as string, TimingPointNumber as integer)
		  dim SQLStatement as string
		  dim res as Boolean
		  dim rsParticipant as RecordSet
		  dim ParticipantID as String
		  dim n as Integer
		  
		  SQLStatement="Select participants.rowid, participants.Actual_Start, participants.Assigned_Start FROM transponders LEFT OUTER JOIN participants ON transponders.Racer_Number = participants.Racer_Number "
		  SQLStatement = SQLStatement+"WHERE transponders.RaceID=1 AND transponders.TX_Code ='"+TXCode+"'"
		  rsParticipant=app.theDB.DBSQLSelect(SQLStatement)
		  
		  
		  if not(rsParticipant.eof) Then
		    ParticipantID=rsParticipant.Field("rowid").StringValue
		    
		    if  Time>=rsParticipant.Field("Assigned_Start").StringValue or app.AllowEarlyStart then
		      if Time>rsParticipant.Field("Actual_Start").StringValue then
		        SQLStatement = "UPDATE times SET Use_This_Passing='N' WHERE Interval_Number=0 AND ParticipantID="+ParticipantID
		        App.theDB.DBSQLExecute(SQLStatement)
		      end if
		      AddTimeRecord(val(ParticipantID),TXCode,0,TimingPointNumber,Time,"00:00:00.000","00:00:00.000","Y")
		      SQLStatement = "UPDATE participants SET Actual_Start='"+Time+"'"
		    else
		      AddTimeRecord(val(ParticipantID),TXCode,0,TimingPointNumber,Time,"00:00:00.000","00:00:00.000","N")
		      SQLStatement = "UPDATE participants SET"
		    end if
		    
		    
		    if InStr(SQLStatement,"Actual")>0 then
		      SQLStatement = SQLStatement + ","
		    end if
		    SQLStatement = SQLStatement + " DNS='N',"
		    SQLStatement = SQLStatement + " UpdateFlag='Y'"
		    SQLStatement = SQLStatement+" WHERE RaceID=1"
		    SQLStatement = SQLStatement+" AND rowid ="+ParticipantID
		    App.theDB.DBSQLExecute(SQLStatement)
		    
		    wnd_List.UpdateParticipantListTime(rsParticipant.Field("rowid").StringValue,0,"DNF","TT")
		    
		    wnd_List.DataList_Participants.Refresh
		  else
		    AddTimeRecord(0,TXCode,0,TimingPointNumber,Time,"00:00:00.000","00:00:00.000","N")
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub AcceptStopTime(TXCode as string, Time as string, Type as string, TimingPointNumber as integer)
		  dim SQLStatement,ParticipantSQLStatement as string
		  dim res as Boolean
		  dim rsParticipant,rsDistance,rsDivision,rsTX,rsTimes, rsIntervals as RecordSet
		  dim Pace,ParticipantID as String
		  dim n, OverallPlace, GenderPlace, DivisionPlace as Integer
		  dim OverallBack, GenderBack, DivisionBack, PlaceStg as string
		  dim NetTime,TotalTime, ActualStop, UseThisTime, OldTotalTimePlusLap, LapsCompleted, ActualStartTimePlusLap, AgeGrade as string
		  dim AssignedStartTimePlusLap, IntervalPace, IntervalTime, FirstActual, SecondActual, TimeSrc, OrbitsData as string
		  
		  OverallPlace=0
		  GenderPlace=0
		  DivisionPlace=0
		  OverallBack="+00:00:00.000"
		  GenderBack="+00:00:00.000"
		  DivisionBack="+00:00:00.000"
		  UseThisTime="N"
		  
		  SQLStatement="SELECT Racer_Number FROM transponders WHERE TX_Code ='"+TXCode+"'"
		  rsTX=app.theDB.DBSQLSelect(SQLStatement)
		  
		  if rsTX.RecordCount>0 then
		    SQLStatement="SELECT participants.rowid,participants.Racer_Number,"
		    SQLStatement = SQLStatement+" divisions.RaceDistanceID, DivisionID, TeamID,"
		    SQLStatement = SQLStatement+" Assigned_Start,"
		    SQLStatement = SQLStatement+" Actual_Start,"
		    SQLStatement = SQLStatement+" Actual_Stop,"
		    SQLStatement = SQLStatement+" Total_Adjustment,"
		    SQLStatement = SQLStatement+" Net_Time,"
		    SQLStatement = SQLStatement+" Time_Source,"
		    SQLStatement = SQLStatement+" Total_Time,"
		    SQLStatement = SQLStatement+" Laps_Completed, participants.Gender, participants.Age,"
		    SQLStatement = SQLStatement+" First_Name, Participant_Name,"
		    SQLStatement = SQLStatement+" SMS_Address"
		    SQLStatement = SQLStatement+" FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid"
		    SQLStatement = SQLStatement+" WHERE Racer_Number = "+rsTX.Field("Racer_Number").StringValue
		    rsParticipant=app.theDB.DBSQLSelect(SQLStatement)
		    
		    if not(rsParticipant.eof) then
		      
		      ParticipantID=rsParticipant.Field("rowid").StringValue
		      ParticipantSQLStatement = "UPDATE participants SET"
		      'select case Type
		      'case "Both"
		      ActualStop=rsParticipant.Field("Actual_Stop").StringValue
		      ActualStartTimePlusLap=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(rsParticipant.Field("Actual_Start").StringValue)+app.MinimumLapTime-app.AMBOffset,true)
		      AssignedStartTimePlusLap=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(rsParticipant.Field("Assigned_Start").StringValue)+app.MinimumLapTime-app.AMBOffset,true)
		      
		      if (((Time>ActualStartTimePlusLap) and (Time>AssignedStartTimePlusLap) and rsParticipant.Field("Time_Source").StringValue="TT") or app.AllowEarlyStart) then 'the minimum lap time has to occur before a stop time can be accepted
		        
		        TotalTime=app.CalculateTotalTime(rsParticipant.Field("Assigned_Start").stringValue,rsParticipant.Field("Actual_Start").stringValue,Time,rsParticipant.Field("Total_Adjustment").stringValue,NetTime)
		        
		        OldTotalTimePlusLap=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(ActualStop)+app.MinimumLapTime-app.AMBOffset,true)
		        if (((Time<ActualStop) and (TimingPointNumber=99)) or (ActualStop=app.RaceDate.SQLDate+" 00:00:00.000") or (app.RacersPerStart=9998 and Time>=OldTotalTimePlusLap) or (TotalTime < rsParticipant.Field("Total_Time").StringValue and wnd_List.cbJam.Value)) then
		          
		          If app.RacersPerStart = 9998 or wnd_List.cbJam.Value then 'Crit 
		            LapsCompleted=str(rsParticipant.Field("Laps_Completed").IntegerValue+1)
		          else
		            LapsCompleted="1"
		          end if
		          
		          ParticipantSQLStatement = ParticipantSQLStatement + " Actual_Stop='"+Time+"'"
		          ParticipantSQLStatement = ParticipantSQLStatement + ", Total_Time='"+TotalTime+"'"
		          ParticipantSQLStatement = ParticipantSQLStatement + ", Net_Time='"+NetTime+"'"
		          
		          If app.RacersPerStart <> 9998 and not(wnd_List.cbJam.Value) then 'not a Crit 
		            SQLStatement="SELECT Actual_Time FROM times WHERE Interval_Number=9999 AND ParticipantID="+ParticipantID+" AND Actual_Time<='"+Time+"'"
		            rsTimes=app.theDB.DBSQLSelect(SQLStatement)
		            if rsTimes.RecordCount=app.nthTime-1 then
		              UseThisTime="Y"
		            else
		              UseThisTime="N"
		            end if
		            rsTimes.Close
		          else
		            UseThisTime="Y"
		          end if
		          
		        else
		          If app.RacersPerStart = 9998 or wnd_List.cbJam.Value then 'Crit
		            LapsCompleted=str(rsParticipant.Field("Laps_Completed").IntegerValue+1)
		            ParticipantSQLStatement = ParticipantSQLStatement +  " Laps_Completed='"+LapsCompleted+"'"
		            ParticipantSQLStatement = ParticipantSQLStatement+" WHERE RaceID=1"
		            ParticipantSQLStatement = ParticipantSQLStatement+" AND rowid ="+rsParticipant.Field("rowid").StringValue
		            App.theDB.DBSQLExecute(ParticipantSQLStatement)
		            wnd_List.UpdateParticipantListTime(rsParticipant.Field("rowid").StringValue,9999,"","")
		          else
		            LapsCompleted="1"
		          end if
		        end if
		        
		        SQLStatement="SELECT rowid, Pace_Type, Actual_Distance, Age_Grade_Distance FROM racedistances "
		        SQLStatement = SQLStatement + "WHERE RaceID=1 AND rowid ="+rsParticipant.Field("RaceDistanceID").StringValue
		        rsDistance=app.theDB.DBSQLSelect(SQLStatement)
		        
		        AgeGrade=app.CalculateAgeGrade(rsParticipant.Field("Gender").StringValue,rsParticipant.Field("Age").IntegerValue,rsDistance.Field("Age_Grade_Distance").StringValue,TotalTime)
		        
		        if InStr(ParticipantSQLStatement,"Actual")>0 then
		          ParticipantSQLStatement = ParticipantSQLStatement + ", Age_Grade='"+AgeGrade+"'"
		          ParticipantSQLStatement = ParticipantSQLStatement + ", DNF='N', DNS='N'"
		          ParticipantSQLStatement = ParticipantSQLStatement +  ", Laps_Completed='"+LapsCompleted+"'"
		          ParticipantSQLStatement = ParticipantSQLStatement +  ", UpdateFlag='Y'"
		          ParticipantSQLStatement = ParticipantSQLStatement+" WHERE RaceID=1"
		          ParticipantSQLStatement = ParticipantSQLStatement+" AND rowid ="+rsParticipant.Field("rowid").StringValue
		          
		          
		          if UseThisTime = "Y" then
		            App.theDB.DBSQLExecute(ParticipantSQLStatement)
		            
		            TimeSrc=rsParticipant.Field("Time_Source").StringValue.Left(1)+"T"
		            
		            wnd_List.UpdateParticipantListTime(rsParticipant.Field("rowid").StringValue,9999,TotalTime,TimeSrc)
		            
		            'Send SMS Message Here
		            if app.SendSMS and rsParticipant.Field("SMS_Address").StringValue<>""  and UseThisTime="Y" then
		              PlaceStg=app.GetPlace("Division",rsParticipant.Field("RaceDistanceID").StringValue,rsParticipant.Field("DivisionID").StringValue,rsParticipant.Field("Gender").StringValue,TotalTime,false,LapsCompleted)
		              SendSMSMessage(rsTX.Field("Racer_Number").StringValue,rsParticipant.Field("First_Name").StringValue,rsParticipant.Field("Participant_Name").StringValue,Time,TotalTime,LapsCompleted,"Finish Line",PlaceStg,rsParticipant.Field("SMS_Address").StringValue)
		            end if
		            
		            if app.SendTweet and UseThisTime="Y" then
		              if app.SendTweet and app.TweetOverall then
		                PlaceStg=app.GetPlace("Overall",rsParticipant.Field("RaceDistanceID").StringValue,rsParticipant.Field("DivisionID").StringValue,rsParticipant.Field("Gender").StringValue,TotalTime,false,LapsCompleted)
		              else
		                PlaceStg=app.GetPlace("Division",rsParticipant.Field("RaceDistanceID").StringValue,rsParticipant.Field("DivisionID").StringValue,rsParticipant.Field("Gender").StringValue,TotalTime,false,LapsCompleted)
		              end if
		              if  val(PlaceStg) <= app.PlacesToTweet then
		                SendTweet(rsTX.Field("Racer_Number").StringValue, rsParticipant.Field("First_Name").StringValue,rsParticipant.Field("Participant_Name").StringValue,Time,TotalTime,LapsCompleted,"Finish Line",PlaceStg)
		              end if
		            end if
		            
		            if wnd_List.OrbitsSocket.IsConnected then
		              OrbitsData=rsParticipant.Field("First_Name").StringValue+" "+rsParticipant.Field("Participant_Name").StringValue
		              if len(OrbitsData)>30 then
		                OrbitsData=left(rsParticipant.Field("First_Name").StringValue,1)+" "+rsParticipant.Field("Participant_Name").StringValue
		              end if
		              wnd_List.OrbitsSocket.Write rsTX.Field("Racer_Number").StringValue+"   "+trim(OrbitsData)+"   "+TotalTime
		            end if
		            
		            'Send update to Web Server here
		            'if app.StreamResults then
		            'UpdateWebServer(Time,TotalTime,NetTime,LapsCompleted,str(OverallPlace),OverallBack,str(GenderPlace),GenderBack,str(DivisionPlace),DivisionBack,Pace,wnd_List.ResultsRaceID,ParticipantID)
		            'end if
		            
		          end if
		        end if
		      end if
		    else
		      'don't do anything, but we need to keep the time
		    end if
		    if ((((Time>ActualStartTimePlusLap) and (Time>AssignedStartTimePlusLap) and ((TimingPointNumber=99) or (ActualStop=app.RaceDate.SQLDate+" 00:00:00.000"))) or app.AllowEarlyStart) and ParticipantID<>"")  then 'the minimum lap time has to occur before a stop time can be accepted
		      SQLStatement="SELECT Actual_Time FROM times WHERE Interval_Number="+str(val(LapsCompleted)-1)+" AND ParticipantID="+ParticipantID+" AND Use_This_Passing='Y'"
		      rsTimes=app.theDB.DBSQLSelect(SQLStatement)
		      if rsTimes.RecordCount>0 then
		        FirstActual=rsTimes.Field("Actual_Time").StringValue
		      else
		        if app.CalculateTimesFrom="Actual" then
		          FirstActual=rsParticipant.Field("Actual_Start").StringValue
		        else
		          FirstActual=rsParticipant.Field("Assigned_Start").StringValue
		        end if
		      End if
		      if Time>FirstActual then 'Should be 2nd and subsequent laps
		        IntervalTime=app.ConvertSecondsToTime(app.ConvertTimeToSeconds(Time)-app.ConvertTimeToSeconds(FirstActual),false)
		      else
		        IntervalTime=TotalTime  'Should be the first lap
		      end if
		      
		      if app.RacersPerStart=9998 then
		        SQLStatement="DELETE FROM times WHERE participantID="+ParticipantID+" AND Interval_Number=9999 AND Use_This_Passing='Y'" 'Need to delete the old stop time
		        App.theDB.DBSQLExecute(SQLStatement)
		      end if
		      
		      'add the interval time
		      if ParticipantID<>"0" then
		        SQLStatement="SELECT Pace_Type, Actual_Distance, Number FROM intervals WHERE RaceID=1 AND DivisionID ="+rsParticipant.Field("DivisionID").StringValue+" ORDER BY Number DESC" 'Just want to know if there are any intervals
		        rsIntervals=app.theDB.DBSQLSelect(SQLStatement)
		        if (not(rsIntervals.EOF)) then 'if there are intervals assigned to the race we want to create a lap time
		          if app.RacersPerStart=9998 then
		            AddTimeRecord(val(ParticipantID),TXCode,val(LapsCompleted),TimingPointNumber,Time,IntervalTime,TotalTime,UseThisTime)
		          else
		            AddTimeRecord(val(ParticipantID),TXCode,rsIntervals.Field("Number").IntegerValue,TimingPointNumber,Time,IntervalTime,TotalTime,UseThisTime)
		            if ((wnd_List.ResultType.Text="Cross Country Running") and (UseThisTime="Y") and (ParticipantID<>"0") and (rsParticipant.Field("TeamID").StringValue<>"0")) then
		              UpdateCrossCountryTeamScore(rsParticipant.Field("TeamID").StringValue,rsIntervals.Field("Number").StringValue,rsParticipant.Field("RaceDistanceID").StringValue)
		            end if
		          end if
		        end if
		        rsIntervals.Close
		      end if
		      
		      'add the stop time
		      AddTimeRecord(val(ParticipantID),TXCode,9999,TimingPointNumber,Time,IntervalTime,TotalTime,UseThisTime)
		      rsTimes.Close
		      wnd_List.RecalcTimes_UpdateIntervals(ParticipantID,rsParticipant.Field("Assigned_Start").StringValue,rsParticipant.Field("Actual_Start").StringValue)
		    else
		      AddTimeRecord(0,TXCode,9999,TimingPointNumber,Time,"00:00:00.000","00:00:00.000","N")
		    end if
		    
		    rsParticipant.Close
		  else
		    AddTimeRecord(0,TXCode,9999,TimingPointNumber,Time,"00:00:00.000","00:00:00.000","N")
		  end if
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub AddTimeRecord(ParticipantID as integer, TXCode as string, IntervalNumber as integer, TimingPointNmber as integer, ActualTime as string, IntervalTime as string, TotalTime as String,UseThisPassing as string)
		  dim SQLStatement, TeamMemberID as string
		  dim rsTimes, rsTX, rsTeamMembers as RecordSet
		  
		  'This check will help prevent duplicate times from being added to the race.
		  
		  SQLStatement="Select rowid FROM times WHERE (Interval_Number=9990 OR Interval_Number="+str(IntervalNumber)
		  SQLStatement=SQLStatement+") AND Actual_Time='"+ActualTime+"' AND ParticipantID="+format(ParticipantID,"##########")
		  SQLStatement=SQLStatement+" AND TX_Code='"+TXCode+"'"
		  rsTimes=app.theDB.DBSQLSelect(SQLStatement)
		  
		  'if there is a teammember we need to get the ID into the record
		  'check the transponders first, just in case they ran out of order
		  SQLStatement="SELECT TeamMemberID FROM transponders WHERE TX_Code='"+TXCode+"'"
		  rsTX=app.theDB.DBSQLSelect(SQLStatement)
		  if rsTX.Field("TeamMemberID").StringValue>"0" then
		    TeamMemberID=rsTX.Field("TeamMemberID").StringValue
		  else
		    SQLStatement="SELECT rowid FROM teamMembers WHERE ParticipantID="+format(ParticipantID,"##########")+" AND IntervalNumber="+str(IntervalNumber)
		    rsTeamMembers=app.theDB.DBSQLSelect(SQLStatement)
		    if rsTeamMembers<>nil then 'most of the time we won't have a team member
		      
		      if rsTeamMembers.EOF then 'most of the time we won't have a team member
		        TeamMemberID="0"
		      else
		        TeamMemberID=rsTeamMembers.Field("rowid").StringValue 'but if we do
		      end if
		      rsTeamMembers.Close
		    else
		      TeamMemberID="0"
		    end if
		  end if
		  rsTX.close
		  
		  if rsTimes.RecordCount=0 then
		    SQLStatement="INSERT INTO times (RaceID,ParticipantID,TeamMemberID,TX_Code,Use_This_Passing,Interval_Number,Timing_Point_No,"
		    SQLStatement=SQLStatement+"Actual_Time,Interval_Time,Total_Time,UpdateFlag)"
		    SQLStatement=SQLStatement+" VALUES (1,"
		    SQLStatement=SQLStatement+"'"+format(ParticipantID,"##########")+"',"
		    SQLStatement=SQLStatement+"'"+TeamMemberID+"',"
		    SQLStatement=SQLStatement+"'"+TXCode+"',"
		    SQLStatement=SQLStatement+"'"+UseThisPassing+"',"
		    SQLStatement=SQLStatement+"'"+str(IntervalNumber)+"',"
		    SQLStatement=SQLStatement+"'"+str(TimingPointNmber)+"',"
		    SQLStatement=SQLStatement+"'"+ActualTime+"',"
		    SQLStatement=SQLStatement+"'"+IntervalTime+"',"
		    SQLStatement=SQLStatement+"'"+TotalTime+"',"
		    if UseThisPassing="Y" then
		      SQLStatement=SQLStatement+"'Y')"
		    else
		      SQLStatement=SQLStatement+"'N')"
		    end if
		    App.theDB.DBSQLExecute(SQLStatement)
		    App.theDB.DBCommit
		  end if
		  
		  rsTimes.Close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SendSMSMessage(RacerNumber as string, RacerFirstName as string, RacerLastName as string, ActualTime as string, TotalTime as string, LapsCompleted as string, TimingPoint as string, PlaceStg as string, MessageAddresses as string)
		  Dim i,ii,SpacePosition,PhoneNumberCount as Integer
		  Dim DoubleQuote, EZTextMessage(), Message, Subject,  PhoneNumbers(), PostContent, Response, OutputPlace, ParticipantName, PhoneNumbersString, command as String
		  Dim form as Dictionary
		  Dim socket1 as New HTTPSecureSocket
		  Dim socket2 as New HTTPSocket
		  DoubleQuote=chr(34)
		  Dim ActualDateTime as new Date
		  
		  Dim SMTPSocket1 as new SMTPSecureSocket
		  Dim mail as EmailMessage
		  Dim file as EmailAttachment
		  
		  dim s as new Shell
		  
		  s.TimeOut=300000 ' want it to go for 5 min before it times out.
		  
		  ActualTime=Left(ActualTime,19)
		  
		  ActualDateTime.SQLDateTime=ActualTime
		  
		  TotalTime=app.StripLeadingZeros(TotalTime)
		  
		  TimingPoint=left(TimingPoint,20)
		  
		  if RacerFirstName<>"" then
		    RacerFirstName=left(RacerFirstName,1)+"."
		    ParticipantName=Left((RacerFirstName+" "+RacerLastName),20) 'don't want the message to be over 120 characters
		  else
		    ParticipantName=left(RacerLastName,20)  'don't want the message to be over 120 characters
		  end if
		  
		  Select case right(PlaceStg,1)
		  case "1"
		    if right(PlaceStg,2) = "11" then
		      OutputPlace=PlaceStg+"th"
		    else
		      OutputPlace=PlaceStg+"st"
		    end if
		    
		  case "2"
		    if right(PlaceStg,2) = "12" then
		      OutputPlace=PlaceStg+"th"
		    else
		      OutputPlace=PlaceStg+"nd"
		    end if
		    
		  case "3"
		    if right(PlaceStg,2) = "13" then
		      OutputPlace=PlaceStg+"th" 
		    else
		      OutputPlace=PlaceStg+"rd" 
		    end if
		    
		  case "0", "4", "5", "6", "7", "8", "9"
		    OutputPlace=PlaceStg+"th" 
		    
		  end select
		  
		  PhoneNumbers=Split(MessageAddresses,"|")
		  
		  Subject=wnd_List.RaceName.Text+" Update"
		  if app.RacersPerStart<>9998 then
		    Message="#"+RacerNumber+" "+ParticipantName+" has crossed the "+TimingPoint+" at "+ActualDateTime.ShortTime+" with a time of "+TotalTime
		  else
		    Message="#"+RacerNumber+" "+ParticipantName+" completed "+LapsCompleted
		    if LapsCompleted > "1" then
		      Message=Message+" laps at "
		    else
		      Message=Message+" lap at "
		    end if
		    Message=Message+ActualDateTime.ShortTime
		  end if
		  
		  if PlaceStg<>"0" then
		    Message=Message+" in "+OutputPlace+" place."
		  end if
		  
		  if app.SMSServer(3)<>"" then
		    Message=Message+" "+app.SMSServer(3)
		  end if
		  
		  if app.SMSServer(0)="EZTexting" then 'EZTexting Support
		    redim EZTextMessage(0)
		    Message=Subject+": "+Message
		    
		    if len(Message)<130 then
		      EZTextMessage(0)=Message
		    else
		      i=0
		      do 
		        'find the begining of the word
		        SpacePosition=130
		        While ((mid(Message,SpacePosition,1)<>" ") and (SpacePosition>0) and (len(Message)>130))
		          SpacePosition=SpacePosition-1
		        wend
		        EZTextMessage(i) = left(Message,SpacePosition)
		        Message=Mid(Message,SpacePosition+1)
		        if (len(Message)>0) then
		          EZTextMessage.Append " "
		          i=i+1
		        end if
		      loop until len(Message)=0
		    end if
		    
		    PhoneNumbersString=""
		    PhoneNumberCount=0
		    for i=0 to UBound(PhoneNumbers)
		      if instr(PhoneNumbers(i),"@")>0 then
		        for ii=UBound(EZTextMessage) downto 0 
		          mail = New EmailMessage
		          mail.fromAddress="info@milliseconds.com"
		          mail.Subject=""
		          mail.bodyPlainText = EZTextMessage(ii)
		          mail.headers.appendHeader "X-Mailer","Milliseconds Pro"
		          mail.addRecipient Trim(PhoneNumbers(i))
		          
		          //send the mail
		          SMTPSocket1.Address="smtp.gmail.com"
		          SMTPSocket1.Username="mac@milliseconds.com"
		          SMTPSocket1.Password="kokopelli08"
		          SMTPSocket1.Port=465
		          SMTPSocket1.ConnectionType=3
		          SMTPSocket1.Secure=true
		          SMTPSocket1.messages.append mail //add email to list of messages
		          SMTPSocket1.SendMail //send message
		          app.theDB.DBSQLExecute("UPDATE races SET SMSCount=SMSCount+1")
		        next
		        
		      else
		        PhoneNumbersString=PhoneNumbersString+"&PhoneNumbers[]="+PhoneNumbers(i)
		        PhoneNumberCount=PhoneNumberCount+1
		      end if
		    next
		    if PhoneNumbersString<>"" then
		      for ii=UBound(EZTextMessage) downto 0 
		        // create and populate the form object
		        'form = New Dictionary
		        'form.value("user") = app.SMSServer(1)
		        'form.value("pass") = app.SMSServer(2)
		        'form.value("phonenumber") = PhoneNumbers(i)
		        'form.value("subject") = ""
		        'form.value("message") = EZTextMessage(ii)
		        'form.value("express") = "1"
		        
		        // setup the socket to POST the form
		        'socket1.setFormData form
		        'response = socket1.post ("https://app.eztexting.com/api/sending",30)
		        
		        command="curl -d 'User="+app.SMSServer(1)+"&Password="+app.SMSServer(2)+PhoneNumbersString+"&Message="+left(EZTextMessage(ii),130)+"&MessageTypeID=2' https://app.eztexting.com/sending/messages?format=xml"
		        
		        s.Execute(command)
		        
		        if InStr(s.Result,"<Status>Success</Status>")>0 then
		          app.theDB.DBSQLExecute("UPDATE races SET SMSCount=SMSCount+"+Str(PhoneNumberCount))
		        end if
		        
		      next
		    end if
		    
		  else
		    'networkText
		    
		    for i=0 to UBound(PhoneNumbers)
		      // create and populate the PostContent
		      PostContent="<?xml version="+DoubleQuote+"1.0"+DoubleQuote+"?>"
		      PostContent=PostContent+"<methodCall>"
		      PostContent=PostContent+"<methodName>send</methodName>"
		      PostContent=PostContent+"<params>"
		      PostContent=PostContent+"<param>"
		      PostContent=PostContent+"<struct>"
		      PostContent=PostContent+"<member>"
		      PostContent=PostContent+"<name>Id</name>"
		      PostContent=PostContent+"<value>"+app.SMSServer(1)+"</value>"
		      PostContent=PostContent+"</member>"
		      PostContent=PostContent+"<member>"
		      PostContent=PostContent+"<name>Recipient</name>"
		      PostContent=PostContent+"<value>"+PhoneNumbers(i)+"</value>"
		      PostContent=PostContent+"</member>"
		      PostContent=PostContent+"<member>"
		      PostContent=PostContent+"<name>Message</name>"
		      PostContent=PostContent+"<value>"+Subject+": "+Message+"</value>"
		      PostContent=PostContent+"</member>"
		      PostContent=PostContent+"</struct>"
		      PostContent=PostContent+"</param>"
		      PostContent=PostContent+"</params>"
		      PostContent=PostContent+"</methodCall>"
		      
		      // setup the socket to POST the message
		      socket2.SetPostContent(PostContent,"text/xml")
		      response = socket2.post ("http://gateway.networktext.com/gateway",30)
		    next
		    
		    
		    
		  end if
		  
		  Socket1.Close
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SendTweet(RacerNumber as string, RacerFirstName as string, RacerLastName as string, ActualTime as string, TotalTime as string, LapsCompleted as string, TimingPoint as string, PlaceStg as string)
		  Dim i,ii,SpacePosition as Integer
		  Dim DoubleQuote, EZTextMessage(), Message, Subject,  PhoneNumbers(), PostContent, Response, OutputPlace, ParticipantName as String
		  Dim form as Dictionary
		  
		  DoubleQuote=chr(34)
		  Dim ActualDateTime as new Date
		  
		  ActualTime=Left(ActualTime,19)
		  
		  ActualDateTime.SQLDateTime=ActualTime
		  
		  TotalTime=app.StripLeadingZeros(TotalTime)
		  
		  TimingPoint=left(TimingPoint,20)
		  
		  if RacerFirstName<>"" then
		    ParticipantName=RacerFirstName+" "+RacerLastName
		  else
		    ParticipantName=RacerLastName
		  end if
		  
		  Select case right(PlaceStg,1)
		  case "1"
		    if right(PlaceStg,2) = "11" then
		      OutputPlace=PlaceStg+"th"
		    else
		      OutputPlace=PlaceStg+"st"
		    end if
		    
		  case "2"
		    if right(PlaceStg,2) = "12" then
		      OutputPlace=PlaceStg+"th"
		    else
		      OutputPlace=PlaceStg+"nd"
		    end if
		    
		  case "3"
		    if right(PlaceStg,2) = "13" then
		      OutputPlace=PlaceStg+"th" 
		    else
		      OutputPlace=PlaceStg+"rd" 
		    end if
		    
		  case "0", "4", "5", "6", "7", "8", "9"
		    OutputPlace=PlaceStg+"th" 
		    
		  end select
		  
		  if app.RacersPerStart<>9998 then
		    Message="#"+RacerNumber+" "+ParticipantName+" has crossed the "+TimingPoint+" with a time of "+TotalTime
		  else
		    Message="#"+RacerNumber+" "+ParticipantName+" completed "+LapsCompleted
		    if LapsCompleted > "1" then
		      Message=Message+" laps at "
		    else
		      Message=Message+" lap at "
		    end if
		    Message=Message+ActualDateTime.ShortTime
		  end if
		  
		  if PlaceStg<>"0" then
		    Message=Message+" in "+OutputPlace+" place."
		  end if
		  
		  if app.TweetRace then
		    wnd_List.praemusCore.SendTweet(Message)
		  end if
		  
		  if app.TweetMST then
		    wnd_List.praemusCore.SendTweet(Message,True)
		  end if
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub UpdateCrossCountryTeamScore(TeamID As string, IntervalNumber as string, RaceDistanceID as string)
		  dim rsParticipant,rsRaceDistance as RecordSet
		  dim SQLStatement as String
		  dim i, TeamScore, Scorers as Integer
		  
		  'Get the RaceDistance Min_Scorers
		  SQLStatement="SELECT Min_CC_Scorers FROM racedistances WHERE rowid ="+RaceDistanceID
		  rsRaceDistance=App.theDB.DBSQLSelect(SQLStatement)
		  Scorers=rsRaceDistance.Field("Min_CC_Scorers").IntegerValue 'we'll worry about ties with the final results
		  
		  'Get all the participants on the team
		  'Total Up the places for first n runners (only breaking ties with the final results)
		  
		  
		  SQLStatement="SELECT participants.DivisionID, participants.Gender, times.Total_Time FROM participants INNER JOIN times ON participants.rowid = times.ParticipantID"
		  SQLStatement=SQLStatement+" WHERE TeamID ="+TeamID+" AND times.Use_This_Passing='Y' AND times.Interval_Number="+IntervalNumber
		  SQLStatement=SQLStatement+" ORDER BY times.Total_Time LIMIT 0,"+str(Scorers)
		  
		  rsParticipant=App.theDB.DBSQLSelect(SQLStatement)
		  
		  TeamScore=0
		  for i=1 to rsParticipant.RecordCount
		    TeamScore=TeamScore+val(app.GetIntervalPlace("Division", RaceDistanceID, rsParticipant.Field("DivisionID").StringValue, rsParticipant.Field("Gender").StringValue, _
		    IntervalNumber, rsParticipant.Field("Total_Time").StringValue,"Team",true))
		    rsParticipant.MoveNext
		  next
		  
		  'Update the Team_Interval record
		  SQLStatement="UPDATE ccTeamIntervals SET"
		  SQLStatement=SQLStatement+" Number_Scorers="+str(rsParticipant.RecordCount)+","
		  SQLStatement=SQLStatement+" Total_Points="+str(TeamScore)+","
		  SQLStatement=SQLStatement+" UpdateFlag='Y'"
		  SQLStatement=SQLStatement+" WHERE TeamID="+TeamID+" AND Interval_Number="+IntervalNumber
		  App.theDB.DBSQLExecute(SQLStatement)
		  App.theDB.DBCommit
		  
		  rsRaceDistance.Close
		  rsParticipant.Close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub UpdateWebServer(StopTime as string, TotalTime as string, NetTime as string, IntervalNumber as string, OverallPlace as string, OverallBack as string, GenderPlace as string, GenderBack as string, ClassPlace as string, ClassBack as string, Pace as string, RaceID as string, ParticipantID as string)
		  dim SQLStatement as string
		  
		  SQLStatement="UPDATE participants SET "
		  SQLStatement=SQLStatement+"StopTime='"+StopTime+"', "
		  SQLStatement=SQLStatement+"TotalTime='"+TotalTime+"', "
		  SQLStatement=SQLStatement+"NetTime='"+NetTime+"', "
		  SQLStatement=SQLStatement+"participants.Interval="+IntervalNumber+", "
		  SQLStatement=SQLStatement+"OverallPlace="+OverallPlace+", "
		  SQLStatement=SQLStatement+" OverallBack='"+OverallBack+"', "
		  SQLStatement=SQLStatement+"GenderPlace="+GenderPlace+", "
		  SQLStatement=SQLStatement+"GenderBack='"+GenderBack+"', "
		  SQLStatement=SQLStatement+"ClassPlace="+ClassPlace+", "
		  SQLStatement=SQLStatement+"ClassBack='"+ClassBack+"', "
		  SQLStatement=SQLStatement+"Pace='"+Pace+"' "
		  SQLStatement=SQLStatement+"WHERE RaceID="+RaceID+" AND ID="+ParticipantID
		  
		  wnd_List.theResultsDb.SQLExecute(SQLStatement)
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Done As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private SMTPSocket1 As SMTPSocket
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Done"
			Group="Behavior"
			InitialValue="0"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Priority"
			Visible=true
			Group="Behavior"
			InitialValue="5"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StackSize"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
