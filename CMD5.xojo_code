#tag Class
Protected Class CMD5
	#tag Method, Flags = &h1
		Protected Function AddUnsigned(rX As Integer, ry As Integer) As Integer
		  Dim rX4 As Integer
		  Dim rY4 As Integer
		  Dim rX8 As Integer
		  Dim rY8 As Integer
		  Dim rResult As Integer
		  
		  rX8 = Bitwise.BitAnd(rX, &H80000000)
		  rY8 = Bitwise.BitAnd(rY, &H80000000)
		  rX4 = Bitwise.BitAnd(rX,  &H40000000)
		  rY4 = Bitwise.BitAnd(rY, &H40000000)
		  
		  rResult = BitWise.BitAnd(rX, &H3FFFFFFF) + BitWise.BitAnd(rY, &H3FFFFFFF)
		  
		  If BitWise.BitAnd(rX4, rY4) <> 0 Then
		    rResult = Bitwise.BitXor(rResult, &H80000000)
		    rResult = BitWise.BitXor(rResult, rX8)
		    rResult = BitWise.BitXor(rResult, rY8)
		    
		  ElseIf BitWise.BitOr(rX4, rY4) <> 0 Then
		    If BitWise.BitAnd(rResult, &H40000000) <> 0 Then
		      rResult = BitWise.BitXor(rResult, &HC0000000)
		      rResult = BitWise.BitXor(rResult, rX8)
		      rResult = BitWise.BitXor(rResult, rY8)
		    Else
		      rResult = BitWise.BitXor(rResult, &H40000000)
		      rResult = BitWise.BitXor(rResult, rX8)
		      rResult = BitWise.BitXor(rResult, rY8)
		    End If
		  Else
		    rResult = BitWise.BitXor(rResult, rX8)
		    rResult = BitWise.BitXor(rResult, rY8)
		  End If
		  
		  return rResult
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  m_OnBits(0) = 1
		  m_OnBits(1) = 3
		  m_OnBits(2) = 7
		  m_OnBits(3) = 15
		  m_OnBits(4) = 31
		  m_OnBits(5) = 63
		  m_OnBits(6) = 127
		  m_OnBits(7) = 255
		  m_OnBits(8) = 511
		  m_OnBits(9) = 1023 
		  m_OnBits(10) = 2047 
		  m_OnBits(11) = 4095 
		  m_OnBits(12) = 8191 
		  m_OnBits(13) = 16383
		  m_OnBits(14) = 32767
		  m_OnBits(15) = 65535
		  m_OnBits(16) = 131071
		  m_OnBits(17) = 262143
		  m_OnBits(18) = 524287
		  m_OnBits(19) = 1048575
		  m_OnBits(20) = 2097151
		  m_OnBits(21) = 4194303
		  m_OnBits(22) = 8388607
		  m_OnBits(23) = 16777215
		  m_OnBits(24) = 33554431
		  m_OnBits(25) = 67108863
		  m_OnBits(26) = 134217727
		  m_OnBits(27) = 268435455
		  m_OnBits(28) = 536870911
		  m_OnBits(29) = 1073741823 
		  m_OnBits(30) = 2147483647 
		  
		  m_Power(0) = 1
		  m_Power(1) = 2
		  m_Power(2) = 4
		  m_Power(3) = 8
		  m_Power(4) = 16
		  m_Power(5) = 32
		  m_Power(6) = 64
		  m_Power(7) = 128
		  m_Power(8) = 256
		  m_Power(9) = 512
		  m_Power(10) = 1024 
		  m_Power(11) = 2048 
		  m_Power(12) = 4096 
		  m_Power(13) = 8192 
		  m_Power(14) = 16384
		  m_Power(15) = 32768
		  m_Power(16) = 65536
		  m_Power(17) = 131072
		  m_Power(18) = 262144
		  m_Power(19) = 524288
		  m_Power(20) = 1048576
		  m_Power(21) = 2097152
		  m_Power(22) = 4194304
		  m_Power(23) = 8388608
		  m_Power(24) = 16777216
		  m_Power(25) = 33554432
		  m_Power(26) = 67108864
		  m_Power(27) = 134217728 
		  m_Power(28) = 268435456
		  m_Power(29) = 536870912
		  m_Power(30) = 1073741824 
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function ConvertToWordArray(sMessage as String) As Integer()
		  Dim rMessageLength As Integer
		  Dim rNumberOfWords As Integer
		  Dim rWordArray() As Integer
		  Dim rBytePosition As Integer
		  Dim rByteCount As Integer
		  Dim rWordCount As Integer
		  Dim rChar As Integer
		  Dim Modulus_Bits As Integer
		  Dim Congruent_bits As Integer
		  Modulus_Bits = 512
		  Congruent_bits = 448
		  rMessageLength = Len(sMessage)
		  rNumberOfWords = (((rMessageLength + _
		  ((MODULUS_BITS - CONGRUENT_BITS) \ BITS_TO_A_BYTE)) \ _
		  (MODULUS_BITS \ BITS_TO_A_BYTE)) + 1) * _
		  (MODULUS_BITS \ BITS_TO_A_WORD)
		  ReDim rWordArray(rNumberOfWords - 1)
		  
		  rBytePosition = 0
		  rByteCount = 0
		  Do Until rByteCount = rMessageLength
		    ' Each word is 4 bytes
		    rWordCount = rByteCount \ BYTES_TO_A_WORD
		    rBytePosition = (rByteCount Mod BYTES_TO_A_WORD) * BITS_TO_A_BYTE
		    rChar = AscB(Mid(sMessage, rByteCount + 1, 1))
		    rWordArray(rWordCount) = BitWise.BitOr(rWordArray(rWordCount), BitWise.ShiftLeft(rChar, rBytePosition))
		    rByteCount = rByteCount + 1
		  Loop
		  
		  rWordCount = rByteCount \ BYTES_TO_A_WORD
		  rBytePosition = (rByteCount Mod BYTES_TO_A_WORD) * BITS_TO_A_BYTE
		  
		  rWordArray(rWordCount) = BitWise.BitOr(rWordArray(rWordCount), BitWise.ShiftLeft(&H80, rBytePosition))
		  
		  rWordArray(rNumberOfWords - 2) = BitWise.ShiftLeft(rMessageLength, 3)
		  rWordArray(rNumberOfWords - 1) = Bitwise.ShiftRight(rMessageLength, 29)
		  
		  return rWordArray
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function F(x as Integer, y as Integer, z as Integer) As Integer
		  Return Bitwise.BitOr(Bitwise.BitAnd(x,  y), (Bitwise.BitAnd(BitWise.OnesComplement(x),z)))
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub FF(ByRef a as Integer, b as Integer, c as Integer, d as integer, x as integer, s As integer, ac as integer)
		  a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac))
		  a = RotateLeft(a, s)
		  a = AddUnsigned(a, b)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function G(x as Integer, y as Integer, z As Integer) As Integer
		  Return Bitwise.BitOr(bitwise.BitAnd(x, y), (Bitwise.BitAnd(y, Bitwise.OnesComplement(z))))
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub GG(ByRef a As Integer, b As Integer, c as Integer, d As Integer, x as integer, s As Integer, ac as integer)
		  a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac))
		  a = RotateLeft(a, s)
		  a = AddUnsigned(a, b)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function H(x as Integer, y as Integer, z as Integer) As Integer
		  x = BitWise.BitXor(x, y)
		  x = Bitwise.BitXor(x, z)
		  return x
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub HH(ByRef a As Integer, b As Integer, c as Integer, d As Integer, x as integer, s As Integer, ac as integer)
		  a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac))
		  a = RotateLeft(a, s)
		  a = AddUnsigned(a, b)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function I(x as integer,y as integer, z as integer) As integer
		  Return Bitwise.BitXor(y, (Bitwise.Bitor(x,bitwise.OnesComplement(z))))
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub II(ByRef a As Integer, b As Integer, c as Integer, d As Integer, x as integer, s As Integer, ac as integer)
		  a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac))
		  a = RotateLeft(a, s)
		  a = AddUnsigned(a, b)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MD5(sMessage As String) As String
		  Dim x() As Integer
		  Dim k As Integer
		  Dim AA As Integer
		  Dim BB As Integer
		  Dim CC As Integer
		  Dim DD As Integer
		  Dim a As Integer
		  Dim b As Integer
		  Dim c As Integer
		  Dim d As Integer
		  
		  Const S11  = 7
		  Const S12  = 12
		  Const S13 = 17
		  Const S14 = 22
		  Const S21 = 5
		  Const S22  = 9
		  Const S23 = 14
		  Const S24 = 20
		  Const S31 = 4
		  Const S32 = 11
		  Const S33 = 16
		  Const S34 = 23
		  Const S41 = 6
		  Const S42 = 10
		  Const S43 = 15
		  Const S44 = 21
		  
		  x = ConvertToWordArray(sMessage)
		  a = &H67452301
		  b = &HEFCDAB89
		  c = &H98BADCFE
		  d = &H10325476
		  
		  For k = 0 To UBound(x) Step 16
		    AA = a
		    BB = b
		    CC = c
		    DD = d
		    a = AddUnsigned(a, AA)
		    b = AddUnsigned(b, BB)
		    c = AddUnsigned(c, CC)
		    d = AddUnsigned(d, DD)
		    
		    FF a, b, c, d, x(k + 0), S11, &HD76AA478
		    FF d, a, b, c, x(k + 1), S12, &HE8C7B756
		    FF c, d, a, b, x(k + 2), S13, &H242070DB
		    FF b, c, d, a, x(k + 3), S14, &HC1BDCEEE
		    FF a, b, c, d, x(k + 4), S11, &HF57C0FAF
		    FF d, a, b, c, x(k + 5), S12, &H4787C62A
		    FF c, d, a, b, x(k + 6), S13, &HA8304613
		    FF b, c, d, a, x(k + 7), S14, &HFD469501
		    FF a, b, c, d, x(k + 8), S11, &H698098D8
		    FF d, a, b, c, x(k + 9), S12, &H8B44F7AF
		    FF c, d, a, b, x(k + 10), S13, &HFFFF5BB1
		    FF b, c, d, a, x(k + 11), S14, &H895CD7BE
		    FF a, b, c, d, x(k + 12), S11, &H6B901122
		    FF d, a, b, c, x(k + 13), S12, &HFD987193
		    FF c, d, a, b, x(k + 14), S13, &HA679438E
		    FF b, c, d, a, x(k + 15), S14, &H49B40821
		    
		    GG a, b, c, d, x(k + 1), S21, &HF61E2562
		    GG d, a, b, c, x(k + 6), S22, &HC040B340
		    GG c, d, a, b, x(k + 11), S23, &H265E5A51
		    GG b, c, d, a, x(k + 0), S24, &HE9B6C7AA
		    GG a, b, c, d, x(k + 5), S21, &HD62F105D
		    GG d, a, b, c, x(k + 10), S22, &H2441453
		    GG c, d, a, b, x(k + 15), S23, &HD8A1E681
		    GG b, c, d, a, x(k + 4), S24, &HE7D3FBC8
		    GG a, b, c, d, x(k + 9), S21, &H21E1CDE6
		    GG d, a, b, c, x(k + 14), S22, &HC33707D6
		    GG c, d, a, b, x(k + 3), S23, &HF4D50D87
		    GG b, c, d, a, x(k + 8), S24, &H455A14ED
		    GG a, b, c, d, x(k + 13), S21, &HA9E3E905
		    GG d, a, b, c, x(k + 2), S22, &HFCEFA3F8
		    GG c, d, a, b, x(k + 7), S23, &H676F02D9
		    GG b, c, d, a, x(k + 12), S24, &H8D2A4C8A
		    
		    HH a, b, c, d, x(k + 5), S31, &HFFFA3942
		    HH d, a, b, c, x(k + 8), S32, &H8771F681
		    HH c, d, a, b, x(k + 11), S33, &H6D9D6122
		    HH b, c, d, a, x(k + 14), S34, &HFDE5380C
		    HH a, b, c, d, x(k + 1), S31, &HA4BEEA44
		    HH d, a, b, c, x(k + 4), S32, &H4BDECFA9
		    HH c, d, a, b, x(k + 7), S33, &HF6BB4B60
		    HH b, c, d, a, x(k + 10), S34, &HBEBFBC70
		    HH a, b, c, d, x(k + 13), S31, &H289B7EC6
		    HH d, a, b, c, x(k + 0), S32, &HEAA127FA
		    HH c, d, a, b, x(k + 3), S33, &HD4EF3085
		    HH b, c, d, a, x(k + 6), S34, &H4881D05
		    HH a, b, c, d, x(k + 9), S31, &HD9D4D039
		    HH d, a, b, c, x(k + 12), S32, &HE6DB99E5
		    HH c, d, a, b, x(k + 15), S33, &H1FA27CF8
		    HH b, c, d, a, x(k + 2), S34, &HC4AC5665
		    
		    II a, b, c, d, x(k + 0), S41, &HF4292244
		    II d, a, b, c, x(k + 7), S42, &H432AFF97
		    II c, d, a, b, x(k + 14), S43, &HAB9423A7
		    II b, c, d, a, x(k + 5), S44, &HFC93A039
		    II a, b, c, d, x(k + 12), S41, &H655B59C3
		    II d, a, b, c, x(k + 3), S42, &H8F0CCC92
		    II c, d, a, b, x(k + 10), S43, &HFFEFF47D
		    II b, c, d, a, x(k + 1), S44, &H85845DD1
		    II a, b, c, d, x(k + 8), S41, &H6FA87E4F
		    II d, a, b, c, x(k + 15), S42, &HFE2CE6E0
		    II c, d, a, b, x(k + 6), S43, &HA3014314
		    II b, c, d, a, x(k + 13), S44, &H4E0811A1
		    II a, b, c, d, x(k + 4), S41, &HF7537E82
		    II d, a, b, c, x(k + 11), S42, &HBD3AF235
		    II c, d, a, b, x(k + 2), S43, &H2AD7D2BB
		    II b, c, d, a, x(k + 9), S44, &HEB86D391
		  Next
		  Return LowerCase(WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d))
		  
		  Return "whatever"
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function RotateLeft(rValue As Integer, iShiftBits As Integer) As Integer
		  Return BitWise.BitOr(BitWise.ShiftLeft(rValue, iShiftBits), BitWise.ShiftRight(rValue, (32 - iShiftBits)))
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function WordToHex(rValue as Integer) As String
		  Dim rByte As Integer
		  Dim rCount As Integer
		  Dim TempString As String
		  
		  For rCount = 0 To 3
		    rByte = Bitwise.BitAnd(BitWise.ShiftRight(rValue, rCount * BITS_TO_A_BYTE), m_OnBits(BITS_TO_A_BYTE - 1))
		    TempString = TempString + Right("0" + Hex(rByte), 2)
		    
		  Next
		  
		  Return TempSTring
		End Function
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected m_OnBits(30) As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected m_Power(30) As Integer
	#tag EndProperty


	#tag Constant, Name = Bits_To_A_Byte, Type = Integer, Dynamic = False, Default = \"8", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = BITS_TO_A_WORD, Type = Integer, Dynamic = False, Default = \"32", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = BYTES_TO_A_WORD, Type = Integer, Dynamic = False, Default = \"4", Scope = Protected
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
