#tag Window
Begin Window SwitchDistances
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   309
   ImplicitInstance=   True
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   False
   Title           =   "Reassign Distances"
   Visible         =   True
   Width           =   594
   Begin PopupMenu pmOldDistance
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   259
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   15
      Underline       =   False
      Visible         =   True
      Width           =   245
   End
   Begin PopupMenu pmTimingPoint
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   132
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   62
      Underline       =   False
      Visible         =   True
      Width           =   256
   End
   Begin PopupMenu pmNewDistance
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   False
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   143
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   116
      Underline       =   False
      Visible         =   True
      Width           =   245
   End
   Begin Label stRacerName
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Racer Name"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   184
      Transparent     =   False
      Underline       =   False
      Visible         =   False
      Width           =   278
   End
   Begin Label stTotalTime
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "00:00:00.000"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   240
      Transparent     =   False
      Underline       =   False
      Visible         =   False
      Width           =   100
   End
   Begin Label stCurrentDivision
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Age Division"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   269
      Transparent     =   False
      Underline       =   False
      Visible         =   False
      Width           =   278
   End
   Begin PopupMenu pmNewDivision
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   False
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   310
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   211
      Underline       =   False
      Visible         =   True
      Width           =   250
   End
   Begin Label stCurrentDistance
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Find all those who registered for the"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   16
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   235
   End
   Begin Label stCurrentDistance1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   519
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "distance"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   16
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   68
   End
   Begin Label stCurrentDistance11
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   400
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "timing point."
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   63
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   89
   End
   Begin ProgressBar AssignmentProgress
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   95
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Maximum         =   100
      Scope           =   "0"
      TabIndex        =   "10"
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   150
      Value           =   0
      Visible         =   True
      Width           =   395
   End
   Begin PushButton pbFind
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Find"
      Default         =   False
      Enabled         =   False
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   494
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   62
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   26
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Reassign to the"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   117
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   105
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   400
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "distance."
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   117
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   73
   End
   Begin Label stCurrentRecord
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   26
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "1"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   149
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   64
   End
   Begin Label stFound
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   500
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   14
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "0"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   149
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   74
   End
   Begin PushButton pbUpdate
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Update"
      Default         =   True
      Enabled         =   False
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   494
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   15
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   269
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin Line Line1
      BorderWidth     =   1
      Height          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   85
      LineColor       =   &c00000000
      LockedInPosition=   False
      Scope           =   "0"
      TabIndex        =   "17"
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   95
      Visible         =   True
      Width           =   426
      X1              =   85
      X2              =   511
      Y1              =   95
      Y2              =   95
   End
   Begin Label stAge
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   17
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "99"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   212
      Transparent     =   False
      Underline       =   False
      Visible         =   False
      Width           =   39
   End
   Begin Label stGender
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   71
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   18
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "M"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   212
      Transparent     =   False
      Underline       =   False
      Visible         =   False
      Width           =   39
   End
   Begin PushButton pbNext
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Next"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   402
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   19
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   269
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin Label stCurrentDistance12
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   26
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   20
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "and crossed the"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   63
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   111
   End
   Begin PushButton pbPrevious
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Previous"
      Default         =   False
      Enabled         =   False
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   310
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   21
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   269
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h1
		Protected Sub PopulateStaticText(Index as Integer)
		  dim rsDivisions as recordSet
		  dim SQL as string
		  
		  redim DivisionStartTime(0)
		  
		  Index=Index-1 'need to account for the zero based arrays
		  
		  stRacerName.text=RacerName(Index)
		  stAge.text=RacerAge(Index)
		  stGender.text=RacerGender(Index)
		  stTotalTime.text=RacerTotalTime(Index)
		  stCurrentDivision.Text=OldRacerDivision(Index)
		  
		  if pmNewDistance.ListIndex>0 then
		    pmNewDivision.deleteAllRows
		    
		    SQL="SELECT Division_Name, rowid FROM divisions "
		    SQL=SQL+"WHERE RaceDistanceID="+pmNewDistance.RowTag(pmNewDistance.ListIndex)
		    SQL=SQL+" AND Low_Age<="+stAge.text+" AND High_Age>="+stAge.text+" AND Gender='"+stGender.text+"' "
		    SQL=SQL+"ORDER BY List_Order"
		    
		    rsDivisions=App.theDB.DBSQLSelect(SQL)
		    while not rsDivisions.EOF
		      pmNewDivision.addRow rsDivisions.Field("Division_Name").stringValue
		      pmNewDivision.rowTag(pmNewDivision.ListCount-1)=rsDivisions.Field("rowid").integerValue
		      rsDivisions.moveNext
		    Wend
		    
		    pmNewDivision.ListIndex=0
		    
		    rsDivisions.Close
		  end if
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected DivisionStartTime() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected NewDivisionID() As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected NewDivisionName() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected OldDivisionID() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected OldRacerDivision() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected RacerAge() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected RacerGender() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected RacerName() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		RacerNumber() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected RacerParticipantID() As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected RacerTotalTime() As String
	#tag EndProperty


#tag EndWindowCode

#tag Events pmOldDistance
	#tag Event
		Sub Open()
		  //Load the Old Distance popupmenu
		  dim recCount as integer
		  dim rsDistances as recordSet
		  dim res as boolean
		  
		  
		  me.addRow "Select a Distance..."
		  rsDistances=App.theDB.DBSQLSelect("SELECT rowid, RaceDistance_Name FROM racedistances ORDER BY RaceDistance_Name")
		  while not rsDistances.EOF
		    me.AddRow rsDistances.Field("RaceDistance_Name").stringValue
		    me.RowTag(me.ListCount-1)=rsDistances.Field("rowid").StringValue
		    rsDistances.moveNext
		  Wend
		  
		  me.ListIndex=0
		  
		  rsDistances.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmTimingPoint
	#tag Event
		Sub Open()
		  //Load the Timing Point popupmenu
		  dim recCount as integer
		  dim rsTimingPointIDs as recordSet
		  dim res as boolean
		  
		  
		  me.addRow "Select a Timing Point Number..."
		  rsTimingPointIDs=App.theDB.DBSQLSelect("SELECT DISTINCT(Timing_Point_No) FROM times ORDER BY Timing_Point_No")
		  while not rsTimingPointIDs.EOF
		    me.AddRow rsTimingPointIDs.Field("Timing_Point_No").stringValue
		    rsTimingPointIDs.moveNext
		  Wend
		  
		  me.ListIndex=0
		  
		  rsTimingPointIDs.Close
		End Sub
	#tag EndEvent
	#tag Event
		Sub Change()
		  if me.ListIndex>0 and pmOldDistance.ListIndex>0 then
		    pbFind.Enabled=True
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmNewDistance
	#tag Event
		Sub Open()
		  //Load the New Distance popupmenu
		  dim recCount as integer
		  dim rsDistances as recordSet
		  dim res as boolean
		  
		  
		  me.addRow "Select a New Distance..."
		  rsDistances=App.theDB.DBSQLSelect("SELECT rowid, RaceDistance_Name FROM racedistances ORDER BY RaceDistance_Name")
		  while not rsDistances.EOF
		    me.AddRow rsDistances.Field("RaceDistance_Name").stringValue
		    me.RowTag(me.ListCount-1)=rsDistances.Field("rowid").StringValue
		    rsDistances.moveNext
		  Wend
		  
		  me.ListIndex=0
		  
		  rsDistances.Close
		End Sub
	#tag EndEvent
	#tag Event
		Sub Change()
		  if me.ListIndex>0 then
		    PopulateStaticText(val(stCurrentRecord.Text))
		    pmNewDivision.Enabled=true
		    pbUpdate.Enabled=true
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbFind
	#tag Event
		Sub Action()
		  'find the participants who went the wrong distance
		  
		  dim SQL as String
		  dim rsParticipants as recordSet
		  
		  redim RacerParticipantID(-1)
		  redim RacerName(-1)
		  redim OldRacerDivision(-1)
		  redim OldDivisionID(-1)
		  redim RacerAge(-1)
		  redim RacerGender(-1)
		  redim RacerTotalTime(-1)
		  
		  if (pmOldDistance.ListIndex>0) and (pmTimingPoint.Listindex>0) then
		    SQL="SELECT divisions.Division_Name, "
		    SQL=SQL+"divisions.rowid as DivisionID, "
		    SQL=SQL+"participants.rowid as ParticipantID, "
		    SQL=SQL+"participants.Racer_Number, "
		    SQL=SQL+"participants.Participant_Name, "
		    SQL=SQL+"participants.First_Name, "
		    SQL=SQL+"participants.Gender, "
		    SQL=SQL+"participants.Age, "
		    SQL=SQL+"participants.Total_Time "
		    SQL=SQL+"FROM times INNER JOIN participants ON times.ParticipantID = participants.rowid "
		    SQL=SQL+"INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		    SQL=SQL+"WHERE divisions.RaceDistanceID="+pmOldDistance.RowTag(pmOldDistance.ListIndex)+" AND times.Timing_Point_No="+pmTimingPoint.Text+" "
		    SQL=SQL+"AND times.Use_This_Passing='Y' ORDER BY participants.Total_Time"
		    
		    rsParticipants=App.theDB.DBSQLSelect(SQL)
		    while not rsParticipants.EOF
		      RacerParticipantID.Append rsParticipants.Field("ParticipantID").StringValue
		      RacerName.Append rsParticipants.Field("Racer_Number").StringValue+" "+rsParticipants.Field("First_Name").StringValue+" "+rsParticipants.Field("Participant_Name").StringValue
		      OldRacerDivision.Append rsParticipants.Field("Division_Name").StringValue
		      OldDivisionID.Append rsParticipants.Field("DivisionID").StringValue
		      RacerAge.Append rsParticipants.Field("Age").StringValue
		      RacerGender.Append rsParticipants.Field("Gender").StringValue
		      RacerTotalTime.Append rsParticipants.Field("Total_Time").StringValue
		      rsParticipants.moveNext
		    Wend
		    
		    stCurrentRecord.Text="1"
		    stFound.Text=Format(rsParticipants.RecordCount,"###,###")
		    AssignmentProgress.Maximum=rsParticipants.RecordCount
		    AssignmentProgress.Value=1
		    
		    rsParticipants.Close
		    pmNewDistance.Enabled=true
		    
		    stRacerName.Visible=true
		    stAge.Visible=true
		    stGender.Visible=true
		    stTotalTime.Visible=true
		    stCurrentDivision.Visible=true
		    
		    PopulateStaticText(1)
		  else
		    beep
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbUpdate
	#tag Event
		Sub Action()
		  dim SQL, NetTime, TotalTime as String
		  dim rsDivisions, rsParticipants as RecordSet
		  
		  'need to recalculate times for the participant and update the participant record with the new Assigned Start time aand the new net and total times.
		  
		  'get the new assigned start time from the division record.
		  SQL="SELECT Actual_Start_Time FROM divisions "
		  SQL=SQL+"WHERE rowid="+pmNewDivision.RowTag(pmNewDivision.ListIndex)
		  rsDivisions=App.theDB.DBSQLSelect(SQL)
		  
		  'calculate new total times
		  SQL="SELECT Actual_Start, Actual_Stop, Total_Adjustment FROM participants WHERE rowid="+RacerParticipantID(val(stCurrentRecord.text)-1)
		  rsParticipants=App.theDB.DBSQLSelect(SQL)
		  
		  TotalTime=app.CalculateTotalTime(rsDivisions.Field("Actual_Start_Time").StringValue,rsParticipants.Field("Actual_Start").StringValue,rsParticipants.Field("Actual_Stop").StringValue, _
		  rsParticipants.Field("Total_Adjustment").StringValue,NetTime)
		  
		  wnd_List.RecalcTimes_UpdateIntervals(RacerParticipantID(val(stCurrentRecord.text)-1),rsDivisions.Field("Actual_Start_Time").StringValue,rsParticipants.Field("Actual_Start").StringValue) 'update the interval times
		  
		  'update the participant record
		  
		  SQL="UPDATE participants SET DivisionID="+pmNewDivision.RowTag(pmNewDivision.ListIndex)+", "
		  SQL=SQL+"Actual_Start='"+rsDivisions.Field("Actual_Start_Time").StringValue+"', "
		  SQL=SQL+"Total_Time='"+TotalTime+"', "
		  SQL=SQL+"Net_Time='"+NetTime+"' "
		  SQL=SQL+"WHERE rowid="+RacerParticipantID(val(stCurrentRecord.text)-1)
		  App.theDB.DBSQLExecute(SQL)
		  App.theDB.DBCommit
		  
		  stCurrentRecord.Text=format(val(stCurrentRecord.Text)+1,"###,###")
		  pbPrevious.Enabled=true
		  if val(stCurrentRecord.Text)=val(stFound.Text) then
		    pbNext.Enabled=False
		    me.Enabled=false
		  end if
		  AssignmentProgress.Value=val(stCurrentRecord.Text)
		  PopulateStaticText(val(stCurrentRecord.Text))
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbNext
	#tag Event
		Sub Action()
		  stCurrentRecord.Text=format(val(stCurrentRecord.Text)+1,"###,###")
		  pbPrevious.Enabled=true
		  if val(stCurrentRecord.Text)=val(stFound.Text) then
		    me.Enabled=False
		    me.Enabled=False
		  end if
		  AssignmentProgress.Value=val(stCurrentRecord.Text)
		  PopulateStaticText(val(stCurrentRecord.Text))
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbPrevious
	#tag Event
		Sub Action()
		  stCurrentRecord.Text=format(val(stCurrentRecord.Text)-1,"###,###")
		  pbNext.Enabled=true
		  if val(stCurrentRecord.Text)=1 then
		    me.Enabled=False
		  end if
		  
		  AssignmentProgress.Value=val(stCurrentRecord.Text)
		  PopulateStaticText(val(stCurrentRecord.Text))
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
