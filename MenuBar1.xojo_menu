#tag Menu
Begin Menu MenuBar1
   Begin MenuItem UntitledMenu7
      SpecialMenu = 1
      Text = ""
      Index = -2147483648
      AutoEnable = True
      Visible = True
   End
   Begin MenuItem FileMenu
      SpecialMenu = 0
      Text = "&File"
      Index = -2147483648
      AutoEnable = True
      Visible = True
      Begin MenuItem FileNewRace
         SpecialMenu = 0
         Text = "New Race..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem FileOpenRace
         SpecialMenu = 0
         Text = "Open Race..."
         Index = -2147483648
         ShortcutKey = "O"
         Shortcut = "Cmd+O"
         MenuModifier = True
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem FileOpenRecent
         SpecialMenu = 0
         Text = "Open Recent"
         Index = -2147483648
         AutoEnable = True
         SubMenu = True
         Visible = True
         Begin MenuItem 
            SpecialMenu = 2
            Text = ""
            Index = -2147483648
            AutoEnable = True
            Visible = True
         End
      End
      Begin MenuItem UntitledMenu6
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem FileConnecttomySQLServer
         SpecialMenu = 0
         Text = "Connect to MySQL Server..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledSeparator
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem FileImport
         SpecialMenu = 0
         Text = "Import Data..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledMenu5
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem FilePageSetup
         SpecialMenu = 0
         Text = "Page Setup..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem FilePrintFeedbackResults
         SpecialMenu = 0
         Text = "Print Feedback Results..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem FilePrint
         SpecialMenu = 0
         Text = "Print"
         Index = -2147483648
         ShortcutKey = "P"
         Shortcut = "Cmd+P"
         MenuModifier = True
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledMenu4
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin QuitMenuItem FileQuit
         SpecialMenu = 0
         Text = "Quit"
         Index = -2147483648
         ShortcutKey = "Q"
         Shortcut = "Cmd+Q"
         MenuModifier = True
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem 
         SpecialMenu = 2
         Text = ""
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
   End
   Begin MenuItem EditMenu
      SpecialMenu = 0
      Text = "&Edit"
      Index = -2147483648
      AutoEnable = True
      Visible = True
      Begin MenuItem EditUndo
         SpecialMenu = 0
         Text = "&Undo"
         Index = -2147483648
         ShortcutKey = "Z"
         Shortcut = "Cmd+Z"
         MenuModifier = True
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledMenu3
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem EditCut
         SpecialMenu = 0
         Text = "Cu&t"
         Index = -2147483648
         ShortcutKey = "X"
         Shortcut = "Cmd+X"
         MenuModifier = True
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem EditCopy
         SpecialMenu = 0
         Text = "&Copy"
         Index = -2147483648
         ShortcutKey = "C"
         Shortcut = "Cmd+C"
         MenuModifier = True
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem EditPaste
         SpecialMenu = 0
         Text = "&Paste"
         Index = -2147483648
         ShortcutKey = "V"
         Shortcut = "Cmd+V"
         MenuModifier = True
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem EditClear
         SpecialMenu = 0
         Text = "Clear"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem 
         SpecialMenu = 2
         Text = ""
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
   End
   Begin MenuItem SpecialMenu
      SpecialMenu = 0
      Text = "Special"
      Index = -2147483648
      AutoEnable = True
      Visible = True
      Begin MenuItem SpecialTransponderAssignment
         SpecialMenu = 0
         Text = "Transponder Assignment..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem SpecialTransponderCheckin
         SpecialMenu = 0
         Text = "Transponder Check-In..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledMenu2
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem SpecialUpdateStartTimes
         SpecialMenu = 0
         Text = "Update Start Times..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem SpecialAcceptTimes
         SpecialMenu = 0
         Text = "Accept Times..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledMenu0
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem SpecialResultsPreview
         SpecialMenu = 0
         Text = "Results Preview..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledSeparator0
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem SpecialCorrectSwitchTransponders
         SpecialMenu = 0
         Text = "Correct Switch Transponders..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem SpecialReassignWrongDistances
         SpecialMenu = 0
         Text = "Reassign Wrong Distances..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem SpecialRecalculateTimes
         SpecialMenu = 0
         Text = "Recalculate Times..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem SpecialResetRace
         SpecialMenu = 0
         Text = "Reset Race..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem UntitledItem1
         SpecialMenu = 0
         Text = "-"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin MenuItem 
         SpecialMenu = 2
         Text = ""
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin PrefsMenuItem SpecialPreferences
         SpecialMenu = 0
         Text = "Preferences..."
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin AppleMenuItem AppleAboutMillisecondsProMMV
         SpecialMenu = 0
         Text = "About Milliseconds Pro"
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
      Begin AppleMenuItem 
         SpecialMenu = 2
         Text = ""
         Index = -2147483648
         AutoEnable = True
         Visible = True
      End
   End
   Begin MenuItem 
      SpecialMenu = 2
      Text = ""
      Index = -2147483648
      AutoEnable = True
      Visible = True
   End
End
#tag EndMenu
