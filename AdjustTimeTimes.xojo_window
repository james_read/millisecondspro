#tag Window
Begin Window AdjustTimeTimes
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   8
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   167
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   False
   Title           =   "Untitled"
   Visible         =   True
   Width           =   389
   Begin TimeEditField tefAdjustmentTime
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   128
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   73
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   100
   End
   Begin Label stOPDesc
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   230
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "to"
      TextAlign       =   1
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   73
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   47
   End
   Begin PopupMenu pmOperation
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "Add\rSubtract"
      Italic          =   False
      Left            =   20
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   73
      Underline       =   False
      Visible         =   True
      Width           =   96
   End
   Begin Label stAdjustMessage
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   26
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   29
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   True
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Adjust Times"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   20.0
      TextUnit        =   0
      Top             =   23
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   248
   End
   Begin PushButton pbOK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "OK"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   289
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   127
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton pbCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   196
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   127
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin CheckBox cbActual
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Actual"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   269
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      State           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   50
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   100
   End
   Begin CheckBox cbTotal
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Total"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   269
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      State           =   0
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   72
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   100
   End
   Begin CheckBox cbInterval
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Interval"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   269
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      State           =   0
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   95
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   100
   End
End
#tag EndWindow

#tag WindowCode
	#tag Method, Flags = &h0
		Function CalcAdjustedTime(TimeToBeAdjustedStg as string, AdjustmentTime as string, AddOrSubtract as string, DateIncluded as boolean) As string
		  Dim HasSign as Boolean
		  Dim AdjustedTime, Adjustment, TimeToBeAdjusted as Double
		  Dim FirstChar as String
		  
		  FirstChar=left(TimeToBeAdjustedStg,1)
		  if FirstChar="+" or FirstChar="-" then
		    TimeToBeAdjustedStg=Right(TimeToBeAdjustedStg,len(TimeToBeAdjustedStg)-1)
		    if FirstChar="-" then
		      TimeToBeAdjusted=app.ConvertTimeToSeconds(TimeToBeAdjustedStg)*(-1)
		    else
		      TimeToBeAdjusted=app.ConvertTimeToSeconds(TimeToBeAdjustedStg)
		    end if
		  else
		    FirstChar=""
		    TimeToBeAdjusted=app.ConvertTimeToSeconds(TimeToBeAdjustedStg)
		  end if
		  
		  Adjustment=app.ConvertTimeToSeconds(tefAdjustmentTime.text)
		  
		  if DateIncluded then
		    TimeToBeAdjusted=TimeToBeAdjusted-app.AMBOffset
		  end if
		  
		  if AddOrSubtract="Add" then
		    AdjustedTime=round((TimeToBeAdjusted+Adjustment)*1000)/1000 'make sure we get the proper fractions
		  else
		    AdjustedTime=round((TimeToBeAdjusted-Adjustment)*1000)/1000 'make sure we get the proper fractions
		  end if
		  
		  Return FirstChar+app.ConvertSecondsToTime(AdjustedTime,DateIncluded)
		End Function
	#tag EndMethod


#tag EndWindowCode

#tag Events tefAdjustmentTime
	#tag Event
		Sub Open()
		  tefAdjustmentTime.text="00:00:00.000"
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pmOperation
	#tag Event
		Sub Change()
		  if me.text="Add" then
		    stOPDesc.text="to"
		  else
		    stOPDesc.text="from"
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events stAdjustMessage
	#tag Event
		Sub Open()
		  me.text="Adjust times for "+wnd_List.TimesFound.text+" records"
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbOK
	#tag Event
		Sub Action()
		  Dim DataEntryError as boolean
		  Dim row as integer
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  Dim recsRS as RecordSet
		  Dim AdjustedTime, CR, SQLStatement as string
		  
		  CR=chr(13)
		  
		  'Error Checking
		  If tefAdjustmentTime.text="00:00:00.000" then
		    app.DisplayDataEntryError("Adjustment time misssing.","Please enter an adjustment time.",tefAdjustmentTime)
		    DataEntryError=true
		    
		  elseif not(cbTotal.value) and not(cbInterval.value)  and not(cbActual.value) then
		    d.icon=2   
		    cbActual.setFocus
		    d.Message="Time type to update is required"
		    d.Explanation="Please select a time type to update."
		    b=d.ShowModal
		    DataEntryError=true
		  else
		    DataEntryError=false
		  end if
		  
		  if not(DataEntryError) then
		    'Confirm
		    d.icon=1   //display warning icon
		    d.ActionButton.Caption="OK"
		    d.CancelButton.Visible=True     //show the Cancel button
		    d.CancelButton.Caption="Cancel"
		    d.Message="Really adjust times for "+wnd_List.TimesFound.Text+" records. There is no UNDO for this action."
		    d.Explanation=pmOperation.text+" "+stOPDesc.text+" the following times:"
		    
		    if cbActual.Value then
		      d.Explanation=d.Explanation+CR+"    Actual Times"
		    end if
		    
		    if cbTotal.Value then
		      d.Explanation=d.Explanation+CR+"    Total Times"
		    end if
		    
		    if cbInterval.Value then
		      d.Explanation=d.Explanation+CR+"    Interval Times"
		    End If
		    
		    b=d.ShowModal     //display the dialog
		    Select Case b //determine which button was pressed.
		    Case d.ActionButton
		      DataEntryError=false
		    Case d.CancelButton
		      DataEntryError=true
		    End select
		    
		    if not(DataEntryError) then
		      'Process the changes
		      Progress.Show
		      Progress.Initialize("Adjusting Times","",wnd_List.DataList_Times.Rows,-1)
		      for row=1 to wnd_List.DataList_Times.Rows
		        Progress.UpdateProg1(row)
		        
		        SQLStatement="Select Actual_Time, Interval_Time, Total_Time FROM times WHERE rowid="+ _
		        str(wnd_List.DataList_Times.Row(row).ItemData)
		        recsRS=app.theDB.DBSQLSelect(SQLStatement)
		        
		        SQLStatement="UPDATE times SET "
		        if cbTotal.Value then
		          AdjustedTime=CalcAdjustedTime(recsRS.Field("Total_Time").stringValue,tefAdjustmentTime.text,pmOperation.text,true)
		          SQLStatement=SQLStatement+"Total_Time='"+AdjustedTime+"', "
		        end if
		        
		        if cbInterval.Value then
		          AdjustedTime=CalcAdjustedTime(recsRS.Field("Interval_Time").stringValue,tefAdjustmentTime.text,pmOperation.text,true)
		          SQLStatement=SQLStatement+"Interval_Time='"+AdjustedTime+"', "
		        end if
		        
		        if cbActual.Value then
		          AdjustedTime=CalcAdjustedTime(recsRS.Field("Actual_Time").stringValue,tefAdjustmentTime.text,pmOperation.text,true)
		          SQLStatement=SQLStatement+"Actual_Time='"+AdjustedTime+"', "
		        End If
		        
		        SQLStatement=SQLStatement+"RaceID=1 WHERE rowid="+str(wnd_List.DataList_Times.Row(row).ItemData) 'RaceID assignment there so I don't have to strip the trailing comma 
		        App.theDB.DBSQLExecute(SQLStatement)
		      next
		      app.theDB.DBCommit
		      
		      wnd_List.ExecuteQuery("Participants",wnd_List.ExtraQuery_Participants,wnd_List.Participant_Sort)
		      
		      
		      Progress.Close
		      
		      beep
		      d.Message="Update complete."
		      d.Explanation="Don't forget to recalculate times and/or place racers, if necessary."
		      d.CancelButton.Visible=false
		      b=d.ShowModal
		      
		      self.Close
		      
		    end if
		    
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbCancel
	#tag Event
		Sub Action()
		  self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
