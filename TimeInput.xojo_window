#tag Window
Begin Window TimeInput
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   8
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   347
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Time Input"
   Visible         =   True
   Width           =   465
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   37
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Racer Number:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   20
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Use This Passing:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   84
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   117
   End
   Begin Label StaticText3
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   37
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Timing Point No:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   179
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label StaticText4
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   37
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Time of Day:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   211
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label StaticText5
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   37
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Total Time:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   243
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label StaticText6
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   37
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Interval Time:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   275
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin TextField RacerNumber
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   149
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "######"
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   19
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   80
   End
   Begin Label lblRacerName
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   241
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   20
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   204
   End
   Begin TextField IntervalNumber
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   149
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "#####"
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   145
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   80
   End
   Begin TextField TimingPointNumber
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   149
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "####"
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   178
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   80
   End
   Begin DateTimeEditField TOD
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   149
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   209
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   175
   End
   Begin TimeEditField TotalTime
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   149
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   242
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   110
   End
   Begin TimeEditField IntervalTime
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   149
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   "0"
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   274
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   110
   End
   Begin PushButton pbOK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "OK"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   376
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   308
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PushButton pbCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   287
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   14
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   307
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PopupMenu puUseThisPassing
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "Yes\rNo"
      Italic          =   False
      Left            =   149
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   15
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   85
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin Label StaticText7
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   37
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   16
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Interval No:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   146
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin Label lblIntervalName
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   241
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   17
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   146
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   250
   End
   Begin Label StaticText8
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   37
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   18
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Type:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   113
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin PopupMenu puTimeType
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "Both\rPrimary\rBackup"
      Italic          =   False
      Left            =   149
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   19
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   113
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PopupMenu cbParticipantName
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   False
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   149
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   "0"
      TabIndex        =   20
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   53
      Underline       =   False
      Visible         =   True
      Width           =   239
   End
   Begin Label stName
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   37
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   "0"
      Selectable      =   False
      TabIndex        =   21
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Team Member:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   53
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  dim recsRS, rsParticipants, rsInterval, rsTeamMembers as RecordSet
		  dim i,TeamMemberIdx as integer
		  dim SQLStatement as String
		  TimeID=wnd_List.TimeID
		  
		  SQLStatement="Select ParticipantID, TeamMemberID, Use_This_Passing, Type, Interval_Number, Timing_Point_No, Actual_Time, Interval_Time, Total_Time FROM times WHERE rowid="+TimeID
		  
		  recsRS=app.theDB.DBSQLSelect(SQLStatement)
		  if not recsRS.EOF then  'not a new record
		    newTime=false
		    ParticipantID=recsRS.Field("ParticipantID").StringValue
		    
		    if(recsRS.Field("Use_This_Passing").StringValue="Y") then
		      puUseThisPassing.ListIndex=0
		    else
		      puUseThisPassing.ListIndex=1
		    end if
		    
		    select case recsRS.Field("Type").StringValue
		    case "Both"
		      puTimeType.ListIndex=0
		      
		    case "Primary"
		      puTimeType.ListIndex=1
		      
		    case "Backup"
		      puTimeType.ListIndex=2
		      
		    else
		      puTimeType.ListIndex=0
		      
		    end Select
		    
		    
		    rsParticipants=app.theDB.DBSQLSelect("SELECT rowid, Participant_Name, Racer_Number, First_Name, DivisionID, Actual_Start, Assigned_Start FROM participants WHERE rowid="+recsRS.Field("ParticipantID").StringValue)
		    if not(rsParticipants.EOF) then
		      RacerNumber.text=rsParticipants.Field("Racer_Number").StringValue
		      lblRacerName.Text=rsParticipants.Field("First_Name").StringValue+" "+rsParticipants.field("Participant_Name").StringValue
		      DivisionID=rsParticipants.Field("DivisionID").StringValue
		      Participant_AssignedStart=rsParticipants.Field("Actual_Start").StringValue
		      Participant_ActualStart=rsParticipants.Field("Assigned_Start").StringValue
		      
		      cbParticipantName.AddRow "Select a Team Member..."
		      cbParticipantName.RowTag(0) = 0
		      TeamMemberIdx=0
		      rsTeamMembers=app.theDB.DBSQLSelect("SELECT rowid, MemberName FROM teamMembers WHERE ParticipantID ="+rsParticipants.Field("rowid").StringValue+" ORDER BY IntervalNumber")
		      if not rsTeamMembers.EOF then
		        stName.Visible=true
		        cbParticipantName.Visible=true
		        for i=1 to rsTeamMembers.RecordCount
		          cbParticipantName.AddRow rsTeamMembers.Field("MemberName").StringValue
		          cbParticipantName.RowTag(i)=rsTeamMembers.Field("rowid").IntegerValue
		          if recsRS.Field("TeamMemberID").IntegerValue = rsTeamMembers.Field("rowid").IntegerValue then
		            TeamMemberIdx=i
		          end if
		          rsTeamMembers.MoveNext
		        next
		        cbParticipantName.Enabled=true
		      end if
		      rsTeamMembers.Close
		      cbParticipantName.ListIndex=TeamMemberIdx
		      
		    else
		      RacerNumber.text="0"
		      lblRacerName.Text=""
		      DivisionID="0"
		      Participant_AssignedStart="0000-00-00 00:00:00.000"
		      Participant_ActualStart="0000-00-00 00:00:00.000"
		      
		      cbParticipantName.AddRow "Select a Team Member..."
		      cbParticipantName.RowTag(0) = 0
		      cbParticipantName.ListIndex = 0
		      
		    end if
		    
		    rsParticipants.Close
		    
		    IntervalNumber.Text=recsRS.Field("Interval_Number").StringValue
		    
		    select case IntervalNumber.text
		    case "0"
		      lblIntervalName.Text="Start"
		      
		    case "9999"
		      lblIntervalName.Text="Stop"
		      
		    else
		      rsInterval=app.theDB.DBSQLSelect("Select Interval_Name FROM intervals WHERE Number ="+IntervalNumber.text+" AND DivisionID="+DivisionID)
		      if not rsInterval.EOF then
		        lblIntervalName.Text=rsInterval.Field("Interval_Name").StringValue
		      else
		        lblIntervalName.Text=""
		      end if
		      rsInterval.Close
		    end Select
		    
		    TimingPointNumber.text=recsRS.Field("Timing_Point_No").StringValue
		    TOD.Text=recsRS.Field("Actual_Time").StringValue
		    TotalTime.Text=recsRS.Field("Total_Time").StringValue
		    IntervalTime.text=recsRS.Field("Interval_Time").StringValue
		    
		  else
		    newTime=true
		    RacerNumber.text="0"
		    puUseThisPassing.ListIndex=0
		    lblRacerName.Text=""
		    IntervalNumber.Text="0"
		    lblIntervalName.Text="Start"
		    TimingPointNumber.text="0"
		    TOD.Text=app.RaceDate.SQLDate+" 00:00:00.000"
		    TotalTime.Text="00:00:00.000"
		    IntervalTime.text="00:00:00.000"
		    cbParticipantName.AddRow "Select a Team Member..."
		    cbParticipantName.RowTag(0) = 0
		    cbParticipantName.ListIndex = 0
		  end if
		  RacerNumber.SelStart=0
		  RacerNumber.SelLength=len(RacerNumber.Text)
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Sub InsertTime()
		  dim RowCount as Integer
		  dim SQLStatement as string
		  dim TXID as string
		  dim rsIntervals, rs as RecordSet
		  dim CalculatedTotalTime, CalculatedNetTime as string
		  
		  if puUseThisPassing.text="Yes" then
		    ResetPassingFlags(IntervalNumber.text)
		  end if
		  
		  
		  'add the interval time
		  if IntervalNumber.Text="9999" then
		    if ParticipantID<>"0" then
		      SQLStatement="SELECT Interval_Name, Number FROM intervals WHERE RaceID=1 AND DivisionID ="+DivisionID+" ORDER BY Number DESC" 'Just want to know if there are any intervals
		      rsIntervals=app.theDB.DBSQLSelect(SQLStatement)
		      if (not(rsIntervals.EOF)) then 'if there are intervals assigned to the race we want to create a lap time
		        if (MsgBox ("Add an interval time associated with this Stop Time?" ,4+32,"Interval")=6) then
		          SQLStatement="INSERT INTO times (RaceID, ParticipantID, TeamMemberID, Use_This_Passing, Interval_Number, Timing_Point_No,"
		          SQLStatement=SQLStatement+"Type, Actual_Time, Interval_Time, Total_Time, TX_Code) VALUES ("
		          SQLStatement=SQLStatement+"1, " 'RaceID
		          SQLStatement=SQLStatement+ParticipantID+", " 'ParticipantID
		          SQLStatement=SQLStatement+cbParticipantName.RowTag(cbParticipantName.ListIndex)+", " 'teamMemberID
		          SQLStatement=SQLStatement+"'"+left(puUseThisPassing.text,1)+"', " 'Use_This_Passing
		          SQLStatement=SQLStatement+rsIntervals.Field("Number").StringValue+", " 'Interval_Number
		          SQLStatement=SQLStatement+TimingPointNumber.text+", " 'Timing_Point_Number
		          SQLStatement = SQLStatement + "'"+puTimeType.text+"'," 'Type
		          SQLStatement=SQLStatement+"'"+TOD.Text+"', " 'Actual_Time
		          SQLStatement=SQLStatement+"'"+IntervalTime.Text+"', " 'Interval_Time
		          SQLStatement=SQLStatement+"'"+TotalTime.Text+"', " 'Interval_Time
		          SQLStatement=SQLStatement+"'"+TXCode+"') " 'Transponder code
		          App.theDB.DBSQLExecute(SQLStatement)
		          
		          'Update the wndlist
		          wnd_List.DataList_Times.LockDrawing=true
		          wnd_List.DataList_Times.AppendRow(RacerNumber.text)
		          RowCount=wnd_List.DataList_Times.Rows
		          wnd_List.DataList_Times.CellText(2,RowCount)=lblRacerName.Text
		          if cbParticipantName.RowTag(cbParticipantName.ListIndex)>0 then
		            wnd_List.DataList_Times.CellText(3,RowCount)=cbParticipantName.Text
		          else
		            wnd_List.DataList_Times.CellText(3,RowCount)=""
		          end if
		          wnd_List.DataList_Times.CellText(4,RowCount)=rsIntervals.Field("Interval_Name").StringValue
		          wnd_List.DataList_Times.CellText(5,RowCount)=IntervalTime.text
		          wnd_List.DataList_Times.CellText(6,RowCount)=TotalTime.Text
		          wnd_List.DataList_Times.CellText(7,RowCount)=TOD.Text
		          wnd_List.DataList_Times.CellText(8,RowCount)=puUseThisPassing.Text
		          wnd_List.DataList_Times.Row(RowCount).ItemData=str(app.theDB.DBLastRowID)
		          
		          wnd_List.DataList_Times.VScrollValue=RowCount
		          wnd_List.DataList_Times.LockDrawing=false
		        end if
		      end if
		      rsIntervals.Close
		    end if
		  end if
		  
		  
		  
		  'Build SQL Statement
		  SQLStatement="INSERT INTO times (RaceID, ParticipantID, TeamMemberID, Use_This_Passing, Interval_Number, Timing_Point_No,"
		  SQLStatement=SQLStatement+"Type, Actual_Time, Interval_Time, Total_Time, TX_Code) VALUES ("
		  SQLStatement=SQLStatement+"1, " 'RaceID
		  SQLStatement=SQLStatement+ParticipantID+", " 'ParticipantID
		  SQLStatement=SQLStatement+cbParticipantName.RowTag(cbParticipantName.ListIndex)+", " 'teamMemberID
		  SQLStatement=SQLStatement+"'"+left(puUseThisPassing.text,1)+"', " 'Use_This_Passing
		  SQLStatement=SQLStatement+IntervalNumber.text+", " 'Interval_Number
		  SQLStatement=SQLStatement+TimingPointNumber.text+", " 'Timing_Point_Number
		  SQLStatement = SQLStatement + "'"+puTimeType.text+"'," 'Type
		  SQLStatement=SQLStatement+"'"+TOD.Text+"', " 'Actual_Time
		  SQLStatement=SQLStatement+"'"+IntervalTime.Text+"', " 'Interval_Time
		  SQLStatement=SQLStatement+"'"+TotalTime.Text+"', " 'Total_Time
		  SQLStatement=SQLStatement+"'"+TXCode+"') " 'Transponder code
		  
		  'Execute the SQL
		  App.theDB.DBSQLExecute(SQLStatement)
		  App.theDB.DBCommit
		  
		  'Update the wndlist
		  wnd_List.DataList_Times.LockDrawing=true
		  wnd_List.DataList_Times.AppendRow(RacerNumber.text)
		  RowCount=wnd_List.DataList_Times.Rows
		  wnd_List.DataList_Times.CellText(2,RowCount)=lblRacerName.Text
		  if cbParticipantName.RowTag(cbParticipantName.ListIndex)>0 then
		    wnd_List.DataList_Times.CellText(3,RowCount)=cbParticipantName.Text
		  else
		    wnd_List.DataList_Times.CellText(3,RowCount)=""
		  end if
		  if lblIntervalName.text <> "" then
		    wnd_List.DataList_Times.CellText(4,RowCount)=lblIntervalName.text
		  else
		    wnd_List.DataList_Times.CellText(4,RowCount)=IntervalNumber.text
		  end if
		  wnd_List.DataList_Times.CellText(5,RowCount)=IntervalTime.text
		  wnd_List.DataList_Times.CellText(6,RowCount)=TotalTime.Text
		  wnd_List.DataList_Times.CellText(7,RowCount)=TOD.Text
		  wnd_List.DataList_Times.CellText(8,RowCount)=puUseThisPassing.Text
		  wnd_List.DataList_Times.Row(RowCount).ItemData=str(app.theDB.DBLastRowID)
		  
		  wnd_List.DataList_Times.VScrollValue=RowCount
		  wnd_List.DataList_Times.LockDrawing=false
		  
		  if IntervalNumber.Text="9999" then
		    rs=app.theDB.DBSQLSelect("Select Assigned_Start, Actual_Start, Total_Adjustment, Actual_Stop, Laps_Completed FROM participants WHERE rowid ="+ParticipantID)
		    if not rs.EOF then
		      if rs.field("Actual_Stop").StringValue=app.RaceDate.SQLDate+" 00:00:00.000" then
		        CalculatedNetTime=rs.field("Total_Adjustment").StringValue
		        CalculatedTotalTime=App.CalculateTotalTime(rs.field("Assigned_Start").StringValue,rs.field("Actual_Start").StringValue,TOD.Text,rs.field("Total_Adjustment").StringValue,CalculatedNetTime)
		        SQLStatement = "UPDATE participants SET Actual_Stop='"+TOD.Text+"'"
		        SQLStatement = SQLStatement + ", Total_Time='"+CalculatedTotalTime+"'"
		        SQLStatement = SQLStatement + ", Net_Time='"+CalculatedNetTime+"'"
		        SQLStatement = SQLStatement + ", Laps_Completed="+str(rs.field("Laps_Completed").IntegerValue+1)
		        SQLStatement = SQLStatement + ", DNS='N'"
		        SQLStatement = SQLStatement + ", DNF='N'"
		        SQLStatement = SQLStatement + " WHERE rowid="+ParticipantID
		        
		        App.theDB.DBSQLExecute(SQLStatement)
		        App.theDB.DBCommit
		        
		      end if
		    end if
		    rs.Close
		  end if
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub ResetPassingFlags(IntervalNumber as string)
		  dim SQLStatement as String
		  
		  SQLStatement = "UPDATE times SET Use_This_Passing='N'"
		  SQLStatement = SQLStatement + " WHERE ParticipantID='"+ParticipantID+" AND Interval_Number="+IntervalNumber
		  
		  App.theDB.DBSQLExecute(SQLStatement)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub UpdateTime()
		  dim SQLStatement as String
		  dim SelectedRow as Integer
		  
		  if puUseThisPassing.text="Yes" then
		    ResetPassingFlags(IntervalNumber.text)
		  end if
		  
		  
		  SQLStatement = "UPDATE times SET ParticipantID='"+ParticipantID+"'"
		  SQLStatement = SQLStatement + ", TeamMemberID='"+cbParticipantName.RowTag(cbParticipantName.ListIndex)+"'"
		  SQLStatement = SQLStatement + ", Use_This_Passing='"+left(puUseThisPassing.text,1)+"'"
		  SQLStatement = SQLStatement + ", Type='"+puTimeType.text+"'"
		  SQLStatement = SQLStatement + ", Interval_Number='"+IntervalNumber.text+"'"
		  SQLStatement = SQLStatement + ", Timing_Point_No='"+TimingPointNumber.text+"'"
		  SQLStatement = SQLStatement + ", Actual_Time='"+TOD.text+"'"
		  SQLStatement = SQLStatement + ", Interval_Time='"+IntervalTime.text+"'"
		  SQLStatement = SQLStatement + ", Total_Time='"+TotalTime.text+"'"
		  'SQLStatement = SQLStatement + ", TX_Code='"+TXCode+"'"
		  SQLStatement = SQLStatement + " WHERE rowid="+TimeID
		  
		  App.theDB.DBSQLExecute(SQLStatement)
		  
		  SelectedRow=wnd_List.DataList_Times.VScrollValue
		  
		  wnd_List.DataList_Times.CellText(1,wnd_List.RowSelected)=RacerNumber.Text
		  wnd_List.DataList_Times.CellText(2,wnd_List.RowSelected)=lblRacerName.Text
		  if cbParticipantName.RowTag(cbParticipantName.ListIndex)>0 then
		    wnd_List.DataList_Times.CellText(3,wnd_List.RowSelected)=cbParticipantName.Text
		  else
		    wnd_List.DataList_Times.CellText(3,wnd_List.RowSelected)=""
		  end if
		  if lblIntervalName.Text<>"" then
		    wnd_List.DataList_Times.CellText(4,wnd_List.RowSelected)=lblIntervalName.Text
		  else
		    wnd_List.DataList_Times.CellText(4,wnd_List.RowSelected)=IntervalNumber.text
		  end if
		  wnd_List.DataList_Times.CellText(5,wnd_List.RowSelected)=IntervalTime.Text
		  wnd_List.DataList_Times.CellText(6,wnd_List.RowSelected)=TotalTime.Text
		  wnd_List.DataList_Times.CellText(7,wnd_List.RowSelected)=TOD.Text
		  wnd_List.DataList_Times.CellText(8,wnd_List.RowSelected)=puUseThisPassing.Text
		  'wnd_List.DataList_Times.CellText(9,wnd_List.RowSelected)=TXCode
		  
		  wnd_List.DataList_Times.Refresh
		  wnd_List.DataList_Times.Selection.Clear
		  wnd_List.DataList_Times.VScrollValue=SelectedRow
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected DivisionID As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected newTime As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		Pace As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected ParticipantID As string
	#tag EndProperty

	#tag Property, Flags = &h0
		Participant_ActualStart As string
	#tag EndProperty

	#tag Property, Flags = &h0
		Participant_AssignedStart As string
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected TimeID As String
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected TXCode As String
	#tag EndProperty


#tag EndWindowCode

#tag Events RacerNumber
	#tag Event
		Sub LostFocus()
		  dim rs, rsTeamMembers, rsTransponder as RecordSet
		  dim i as integer
		  
		  rs=app.theDB.DBSQLSelect("Select Participant_Name, First_Name, DivisionID, Assigned_Start, Actual_Start, rowid FROM participants WHERE Racer_Number ="+RacerNumber.text)
		  if not rs.EOF then
		    ParticipantID=rs.Field("rowid").StringValue
		    DivisionID=rs.Field("DivisionID").StringValue
		    lblRacerName.Text=rs.field("First_Name").StringValue+" "+rs.field("Participant_Name").StringValue
		    Participant_AssignedStart=rs.Field("Assigned_Start").StringValue
		    Participant_ActualStart=rs.Field("Actual_Start").StringValue
		    
		    cbParticipantName.DeleteAllRows
		    cbParticipantName.AddRow "Select a Team Member..."
		    cbParticipantName.RowTag(0) = 0
		    
		    rsTeamMembers=app.theDB.DBSQLSelect("SELECT rowid, MemberName FROM teamMembers WHERE ParticipantID ="+rs.Field("rowid").StringValue+" ORDER BY IntervalNumber")
		    if not rsTeamMembers.EOF then
		      cbParticipantName.Enabled=true
		      for i=1 to rsTeamMembers.RecordCount
		        cbParticipantName.AddRow rsTeamMembers.Field("MemberName").StringValue
		        cbParticipantName.RowTag(i)= rsTeamMembers.Field("rowid").IntegerValue
		        rsTeamMembers.MoveNext
		      next
		    end if
		    rsTeamMembers.Close
		    cbParticipantName.ListIndex=0
		    
		    
		    rsTransponder=app.theDB.DBSQLSelect("SELECT TX_Code FROM Transponders WHERE Racer_Number='"+RacerNumber.Text+"'")
		    TXCode=rsTransponder.Field("TX_Code").StringValue
		    
		  else
		    ParticipantID="0"
		    DivisionID="0"
		    lblRacerName.Text=""
		    Participant_AssignedStart=""
		    Participant_ActualStart=""
		    TXCode=""
		    
		  end if
		  
		  rs.Close
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IntervalNumber
	#tag Event
		Sub LostFocus()
		  dim rs as RecordSet
		  
		  select case IntervalNumber.text
		  case "0"
		    lblIntervalName.Text="Start"
		    
		  case "9999"
		    lblIntervalName.Text="Stop"
		    
		  else
		    rs=app.theDB.DBSQLSelect("Select Interval_Name FROM intervals WHERE Number ="+IntervalNumber.text+" AND DivisionID="+DivisionID)
		    if not rs.EOF then
		      lblIntervalName.Text=rs.Field("Interval_Name").StringValue
		    else
		      lblIntervalName.Text=""
		    end if
		    rs.Close
		    
		  end Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TOD
	#tag Event
		Sub LostFocus()
		  dim NetTimeStg, SQLStatement as string
		  dim rsIntervals as RecordSet
		  NetTimeStg="00:00:00.000"
		  TotalTime.text=app.CalculateTotalTime(Participant_AssignedStart,Participant_ActualStart,TOD.Text,"+00:00:00.000",NetTimeStg)
		  
		  SQLStatement="Select Actual_Time FROM times WHERE ParticipantID="+ParticipantID+" AND Interval_Number < "+IntervalNumber.Text
		  SQLStatement=SQLStatement+" AND Use_This_Passing='Y' ORDER BY Interval_Number DESC"
		  rsIntervals=App.theDB.DBSQLSelect(SQLStatement)
		  
		  if rsIntervals.RecordCount>0 then
		    IntervalTime.Text=app.ConvertSecondsToTime((app.ConvertTimeToSeconds(TOD.text)-app.ConvertTimeToSeconds(rsIntervals.Field("Actual_TIme").StringValue)),false)
		  else
		    IntervalTime.Text=TotalTime.Text
		  end if
		  
		  SQLStatement="SELECT Actual_Distance, Pace_Type FROM intervals "
		  SQLStatement = SQLStatement + "WHERE RaceID=1 AND Number ="+IntervalNumber.text+" "
		  SQLStatement = SQLStatement + "AND DivisionID="+DivisionID
		  rsIntervals=app.theDB.DBSQLSelect(SQLStatement)
		  Pace=app.CalculateIntPace(rsIntervals.Field("Pace_Type").StringValue,rsIntervals.Field("Actual_Distance").DoubleValue,IntervalTime.Text)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbOK
	#tag Event
		Sub Action()
		  dim DataEntryError as boolean
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if (val(RacerNumber.text)<1) then
		    app.DisplayDataEntryError("A racer number is required","Please enter a racer number",RacerNumber)
		    DataEntryError=true
		    
		  elseif (IntervalNumber.text="") then
		    app.DisplayDataEntryError("An interval number is required","Please enter an interval number",IntervalNumber)
		    DataEntryError=true
		    
		  elseif TotalTime.text="" then
		    app.DisplayDataEntryError("A total time is required","Please enter enter a total time",TotalTime)
		    DataEntryError=true
		    
		  else
		    DataEntryError=false
		  end if
		  
		  if not(DataEntryError) then
		    
		    if newTime then
		      InsertTime
		    else
		      UpdateTime
		    end if
		    TimeInput.Close
		    
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbCancel
	#tag Event
		Sub Action()
		  TimeInput.close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Pace"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Participant_ActualStart"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Participant_AssignedStart"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
