#tag Module
Protected Module Globals
	#tag Method, Flags = &h0
		Sub RemoveWindow(index as integer)
		  
		  dim i,j as integer
		  
		  j = Ubound(OpenWindows)
		  
		  for i = index to j-1
		    OpenWindows(i) = OpenWindows(i+1)
		    OpenWindows(i).myIndex = i
		  next
		  
		  if j >= 0 then
		    OpenWindows.remove j
		  end if
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		OpenWindows(0) As ReceiveTimes
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
