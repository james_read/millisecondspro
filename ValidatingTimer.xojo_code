#tag Class
Protected Class ValidatingTimer
Inherits Timer
	#tag Event
		Sub Action()
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  d.icon=2   //display stop icon
		  d.ActionButton.Caption="OK"
		  
		  If fValidatingEditField <> nil Then
		    fValidatingEditField.SetFocus()
		    select case fValidatingEditField.Name
		    case "Age"
		      d.Message="Invalid Age"
		      d.Explanation="Please enter an age between 1 and 299"
		    case "Gender"
		      d.Message="Invalid Gender"
		      d.Explanation="Please use: "+EndOfLine+EndOfLine+"'M' for Male"+EndOfLine+"'F for Female"+EndOfLine+"'X' for Mixed Team"
		    end select
		    b=d.ShowModal     //display the dialog
		  End If
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub EditFieldSocket(Assigns o as EditField)
		  fValidatingEditField = o
		  Me.Mode = 1
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h1
		Protected fValidatingEditField As EditField
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Mode"
			Visible=true
			Group="Behavior"
			InitialValue="2"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Off"
				"1 - Single"
				"2 - Multiple"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Period"
			Visible=true
			Group="Behavior"
			InitialValue="1000"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
