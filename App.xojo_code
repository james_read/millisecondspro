#tag Class
Protected Class App
Inherits Application
	#tag Event
		Sub Close()
		  PreferencesWrite
		  theDB.DBClose
		  ProcessTimingDataThread.Done=true
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  dim AMBInitialDate as new date
		  dim res as Boolean
		  
		  ReDim SMSServer(3)
		  theDB = new DBSupport
		  
		  if TargetWin32 then
		    MDIWindow.MinHeight=700
		    MDIWindow.MinWidth=1100
		    MDIWindow.Maximize
		  end if
		  
		  AMBInitialDate.SQLDateTime="1970-01-01 00:00:00"
		  AMBOffset=AMBInitialDate.TotalSeconds
		  'AMBOffset=0
		  
		  SecondsInYear=86400*365.2422
		  
		  RaceDate = new date
		  RaceAgeDate = new date
		  OKToProcessArrays=true
		  InitCRCTable
		  
		  redim TwitterParts(3)
		  
		  PreferencesRead
		  
		  app.MenuBar=MenuBar1
		  
		  ProcessTimingDataThread = new ProcessTimingData
		  ProcessTimingDataThread.Run
		  
		  LoadAgeStandards
		  
		End Sub
	#tag EndEvent


	#tag MenuHandler
		Function AppleAboutMillisecondsProMMV() As Boolean Handles AppleAboutMillisecondsProMMV.Action
			wnd_about.show
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileConnecttomySQLServer() As Boolean Handles FileConnecttomySQLServer.Action
			theDB.MySQL_Connect
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileNewRace() As Boolean Handles FileNewRace.Action
			theDB.RealSQLDatabase_New
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileOpenRace() As Boolean Handles FileOpenRace.Action
			theDB.RealSQLDatabase_Open("")
		End Function
	#tag EndMenuHandler


	#tag Method, Flags = &h0
		Function CalculateAge(BirthDate as date, AsOfDate as date) As Integer
		  dim SecondsLived as Double
		  dim AgeYears, AgeMonths, AgeDays as Integer
		  
		  AgeYears = AsOfDate.year - BirthDate.year
		  AgeMonths = AsOfDate.month - BirthDate.month
		  AgeDays = AsOfDate.day - BirthDate.day
		  
		  
		  If AgeMonths < 0 then
		    AgeYears = AgeYears-1
		  elseif AgeDays < 0 and AgeMonths = 0 then
		    AgeYears = AgeYears-1
		  end if
		  
		  return AgeYears
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CalculateAgeGrade(gender as string, age as integer, distance as string, totalTime as string) As string
		  dim distanceIdx as integer
		  dim ageGrade as double
		  
		  distanceIdx = AgeGrade_Distances.IndexOf(distance)
		  
		  if distanceIdx>=0 then
		    if gender="M" then
		      ageGrade = AgeStandard_Male(age,distanceIdx)/ConvertTimeToSeconds(totalTime)
		    elseif Gender="F" then
		      ageGrade = AgeStandard_Female(age,distanceIdx)/ConvertTimeToSeconds(totalTime)
		    else
		      ageGrade=0
		    end if
		  else
		    ageGrade=0
		  end if
		  
		  ageGrade=Round((ageGrade*10000))/10000 ' round to the nearest 1/100
		  
		  return Format(ageGrade, "00.00%")
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CalculateIntPace(PaceType as string, Distance as Double, Time as string) As String
		  dim Intermediate,Seconds,Hours as double
		  dim Pace as String
		  dim PaceInSeconds as Double
		  
		  
		  select case PaceType
		  case "MPH", "KPH"
		    Hours=ConvertTimeToSeconds(Time)/60/60
		    Intermediate=round((Distance/Hours)*100)/100
		    Pace=format(Intermediate,"#,##0.0")
		    
		  case "min/mi", "min/km"
		    PaceInSeconds=round((ConvertTimeToSeconds(Time)/Distance)*10)/10
		    Pace=left(ConvertSecondsToTime(PaceInSeconds,false),10)
		    
		  case "min/100yds", "min/100m"
		    PaceInSeconds=round((ConvertTimeToSeconds(Time)/(Distance/100))*10)/10
		    Pace=left(ConvertSecondsToTime(PaceInSeconds,false),10)
		    
		  end select
		  
		  if Pace="inf.0" or Pace = "00:00:00.0"  or Pace = "00:00:00.00" then
		    Pace=""
		  end if
		  
		  Pace=StripLeadingZeros(Pace)
		  
		  Return Pace
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CalculatePace(RaceDistanceID as string, Time as string) As String
		  dim Distance,Intermediate,Seconds,Hours as double
		  dim Pace as String
		  dim PaceInSeconds as Double
		  dim rs as RecordSet
		  
		  rs=app.theDB.DBSQLSelect("SELECT Pace_Type, Actual_Distance FROM raceDistances WHERE rowid="+RaceDistanceID)
		  if rs<>nil and rs.RecordCount>0 then
		    Distance=rs.Field("Actual_Distance").DoubleValue
		    select case rs.Field("Pace_Type").StringValue
		    case "MPH", "KPH"
		      Hours=ConvertTimeToSeconds(Time)/60/60
		      Intermediate=round((Distance/Hours)*100)/100
		      Pace=format(Intermediate,"#,##0.0")
		      
		    case "min/mi", "min/km"
		      PaceInSeconds=round((ConvertTimeToSeconds(Time)/Distance)*10)/10
		      Pace=left(ConvertSecondsToTime(PaceInSeconds,false),10)
		      
		    case "min/100yds", "min/100m"
		      PaceInSeconds=round((ConvertTimeToSeconds(Time)/(Distance/100))*10)/10
		      Pace=left(ConvertSecondsToTime(PaceInSeconds,false),10)
		      
		    end select
		    
		    if Pace<>"inf.0" and Pace <> "00:00:00.0"  and Pace <> "00:00:00.00" then
		      Pace=StripLeadingZeros(Pace)
		    else
		      Pace=""
		    end if
		  else
		    Pace=""
		  end if
		  
		  Return Pace
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CalculateStartTime(RacerNumber as integer, DivisionStartTime as string, DateIncluded as boolean) As String
		  dim StartTime as Double
		  dim Remainder as Boolean
		  
		  if ((RacerNumber mod RacersPerStart)>0) then
		    StartTime=app.ConvertTimeToSeconds(DivisionStartTime)+(((RacerNumber\RacersPerStart)+1)*StartIntervalSec)
		  else
		    StartTime=app.ConvertTimeToSeconds(DivisionStartTime)+((RacerNumber\RacersPerStart)*StartIntervalSec)
		  end if
		  
		  if DateIncluded then
		    Return ConvertSecondsToTime(StartTime-AMBOffset,True)
		  else
		    Return ConvertSecondsToTime(StartTime,False)
		  end if
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function CalculateTotalTime(AssignedStartStg as string,ActualStartStg as String,ActualStopStg as string,AdjustmentStg as string, byref NetTimeStg as string) As String
		  dim AssignedStart as double
		  dim ActualStart as double
		  dim ActualStop as double
		  dim Adjustment as double
		  dim TotalTime as double
		  dim TotalTimeStg as string
		  dim NetTime as double
		  
		  AssignedStart=ConvertTimeToSeconds(AssignedStartStg)
		  ActualStart=ConvertTimeToSeconds(ActualStartStg)
		  ActualStop=ConvertTimeToSeconds(ActualStopStg)
		  
		  Adjustment=ConvertTimeToSeconds(AdjustmentStg)
		  
		  select case app.CalculateTimesFrom
		  case "Assigned"
		    if ActualStop > AssignedStart then
		      TotalTime=round(((ActualStop-AssignedStart)+Adjustment)*1000)/1000
		      if ((ActualStart>=AssignedStart) or (AllowEarlyStart)) then
		        NetTime=round(((ActualStop-ActualStart)+Adjustment)*1000)/1000
		      else
		        NetTime=round(((ActualStop-AssignedStart)+Adjustment)*1000)/1000
		      end if
		    end if
		    
		  case "Actual"
		    if ActualStop > ActualStart then
		      if ((ActualStart>=AssignedStart) or (AllowEarlyStart)) then
		        TotalTime=round(((ActualStop-ActualStart)+Adjustment)*1000)/1000
		      else
		        TotalTime=round(((ActualStop-AssignedStart)+Adjustment)*1000)/1000
		      end if
		      NetTime=TotalTime
		    end if
		    
		  end select
		  
		  NetTimeStg=TruncateTime(ConvertSecondsToTime(NetTime,false))
		  TotalTimeStg=TruncateTime(ConvertSecondsToTime(TotalTime,false))
		  
		  Return TotalTimeStg
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ChangeRace()
		  dim recsRS as recordSet
		  dim res, DataVersionFieldFound as boolean
		  dim SQLStatement, Version as string
		  dim i as integer
		  
		  recsRS=App.theDB.DBSQLSelect("Select * FROM races WHERE RaceID=1")
		  if not recsRS.eof then 'not a new record
		    
		    RaceDate=new Date
		    
		    if recsRS.IdxField(1).Name<>"DataVersion"  then
		      i=0
		      do
		        i=i+1
		        if recsRS.IdxField(i).Name="DataVersion"  then
		          DataVersionFieldFound=true
		        end if
		      loop until ((DataVersionFieldFound) or (i >= recsRS.FieldCount))
		      
		      if (not(DataVersionFieldFound))  then
		        app.theDB.DBSQLExecute("ALTER TABLE races ADD DataVersion VARCHAR")
		        app.theDB.DBSQLExecute("UPDATE races SET DataVersion='0.0.0'")
		        app.theDB.DBCommit
		        recsRS=App.theDB.DBSQLSelect("Select * FROM races WHERE RaceID=1")
		      end if
		    end if
		    Version=recsRS.Field("DataVersion").StringValue
		    if recsRS.Field("DataVersion").StringValue < app.ShortVersion then
		      'upgrade version
		      if recsRS.Field("DataVersion").StringValue < "1.0.0b22" then
		        app.theDB.DBSQLExecute("ALTER TABLE participants ADD Street VARCHAR")
		        app.theDB.DBSQLExecute("ALTER TABLE participants ADD City VARCHAR")
		        app.theDB.DBSQLExecute("ALTER TABLE participants ADD State VARCHAR")
		        app.theDB.DBSQLExecute("ALTER TABLE participants ADD Postal_Code VARCHAR")
		        app.theDB.DBSQLExecute("ALTER TABLE participants ADD Country VARCHAR")
		        app.theDB.DBSQLExecute("ALTER TABLE participants ADD EMail VARCHAR")
		        app.theDB.DBSQLExecute("ALTER TABLE participants ADD Phone VARCHAR")
		        app.theDB.DBSQLExecute("ALTER TABLE participants ADD Birth_Date DATE")
		        app.theDB.DBCommit
		      end if
		      if recsRS.Field("DataVersion").StringValue < "1.0.0b25" then
		        app.theDB.DBSQLExecute("ALTER TABLE races ADD Jam_Session VARCHAR")
		        app.theDB.DBCommit
		        app.theDB.DBSQLExecute("UPDATE races SET Jam_Session='N'")
		        app.theDB.DBCommit
		      end if
		      
		      if recsRS.Field("DataVersion").StringValue < "1.0.0b28" then
		        app.theDB.DBSQLExecute("ALTER TABLE participants ADD SMS_Address VARCHAR")
		        app.theDB.DBCommit
		      end if
		      
		      if recsRS.Field("DataVersion").StringValue < "1.0.0b29" then
		        app.theDB.DBSQLExecute("ALTER TABLE divisions ADD Has_Team_Members VARCHAR")
		        app.theDB.DBCommit
		        app.theDB.DBSQLExecute("UPDATE divisions SET Has_Team_Members='N'")
		        app.theDB.DBCommit
		      end if
		    end if
		    
		    if recsRS.Field("DataVersion").StringValue < "1.0.37" then
		      app.theDB.DBSQLExecute(DBSupport.RBSchema_TeamMembers)
		      app.theDB.DBCommit
		    end if
		    
		    if recsRS.Field("DataVersion").StringValue < "1.1.0b01" then
		      app.theDB.DBSQLExecute("ALTER TABLE participants ADD Query_Flag VARCHAR")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("UPDATE participants SET Query_Flag='N'")
		      app.theDB.DBCommit
		    end if
		    
		    if recsRS.Field("DataVersion").StringValue < "1.1.0b03" then
		      app.theDB.DBSQLExecute("ALTER TABLE participants ADD Query_Flag VARCHAR")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("UPDATE participants SET Query_Flag='N'")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("ALTER TABLE participants ADD TeamID INTEGER")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("ALTER TABLE race_distances ADD Min_CC_Scorers INTEGER")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("ALTER TABLE race_distances ADD Max_CC_Scorers INTEGER")
		      app.theDB.DBCommit
		      if app.theDB.DatabaseType="REALSQLDatabase" then
		        app.theDB.DBSQLExecute(app.theDB.RBSchema_CCTeams)
		        app.theDB.DBCommit
		        app.theDB.DBSQLExecute(app.theDB.RBSchema_CCTeamIntervals)
		        app.theDB.DBCommit
		      else
		        app.theDB.DBSQLExecute(app.theDB.mySQLSchema_CCTeams)
		        app.theDB.DBCommit
		        app.theDB.DBSQLExecute(app.theDB.mySQLSchema_CCTeamIntervals)
		        app.theDB.DBCommit
		      end if
		    end if
		    
		    if recsRS.Field("DataVersion").StringValue < "1.1.0b04" then
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD Logo1 VARCHAR")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD Logo2 VARCHAR")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD Logo3 VARCHAR")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD Logo4 VARCHAR")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("ALTER TABLE CCTeams ADD Gender VARCHAR")
		      app.theDB.DBCommit
		    end if
		    
		    if (recsRS.Field("DataVersion").StringValue < "2.1.0b05") or (instr(recsRS.Field("DataVersion").StringValue,"2.0.1")>0) then
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD nthTime INTEGER")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("ALTER TABLE participants ADD UpdateFlag VARCHAR")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("ALTER TABLE times ADD UpdateFlag VARCHAR")
		      app.theDB.DBCommit
		    end if
		    
		    if (recsRS.Field("DataVersion").StringValue < "2.1.0b06") or (instr(recsRS.Field("DataVersion").StringValue,"2.0.1")>0) then
		      app.theDB.DBSQLExecute("ALTER TABLE racedistances ADD Age_Grade_Distance DOUBLE")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("ALTER TABLE participants ADD Age_Grade VARCHAR")
		      app.theDB.DBCommit
		    end if
		    
		    if (recsRS.Field("DataVersion").StringValue < "2.0.1b12") then 'or (instr(recsRS.Field("DataVersion").StringValue,"2.0.1")>0) then
		      
		      SQLStatement="ALTER TABLE participants RENAME TO __temp__participants"
		      app.theDB.DBSQLExecute(SQLStatement)
		      
		      SQLStatement="CREATE TABLE participants "
		      SQLStatement=SQLStatement+"    (RaceID INTEGER, "
		      SQLStatement=SQLStatement+"    TeamID INTEGER, "
		      SQLStatement=SQLStatement+"    RaceDistanceID INTEGER, "
		      SQLStatement=SQLStatement+"    DivisionID INTEGER, "
		      SQLStatement=SQLStatement+"    Racer_Number INTEGER, "
		      SQLStatement=SQLStatement+"    Participant_Name VARCHAR, "
		      SQLStatement=SQLStatement+"    First_Name VARCHAR, "
		      SQLStatement=SQLStatement+"    Street VARCHAR, "
		      SQLStatement=SQLStatement+"    City VARCHAR, "
		      SQLStatement=SQLStatement+"    State VARCHAR, "
		      SQLStatement=SQLStatement+"    Postal_Code VARCHAR, "
		      SQLStatement=SQLStatement+"    Country VARCHAR, "
		      SQLStatement=SQLStatement+"    Gender VARCHAR, "
		      SQLStatement=SQLStatement+"    Phone VARCHAR, "
		      SQLStatement=SQLStatement+"    EMail VARCHAR, "
		      SQLStatement=SQLStatement+"    SMS_Address VARCHAR, "
		      SQLStatement=SQLStatement+"    Birth_Date DATE, "
		      SQLStatement=SQLStatement+"    Age INTEGER, "
		      SQLStatement=SQLStatement+"    Representing VARCHAR, "
		      SQLStatement=SQLStatement+"    Assigned_Start VARCHAR, "
		      SQLStatement=SQLStatement+"    Assigned_StartB VARCHAR, "
		      SQLStatement=SQLStatement+"    Actual_Start VARCHAR, "
		      SQLStatement=SQLStatement+"    Actual_StartB VARCHAR, "
		      SQLStatement=SQLStatement+"    Actual_Stop VARCHAR, "
		      SQLStatement=SQLStatement+"    Actual_StopB VARCHAR, "
		      SQLStatement=SQLStatement+"    Total_Adjustment VARCHAR, "
		      SQLStatement=SQLStatement+"    Total_AdjustmentB VARCHAR, "
		      SQLStatement=SQLStatement+"    Total_Time VARCHAR, "
		      SQLStatement=SQLStatement+"    Total_TimeB VARCHAR, "
		      SQLStatement=SQLStatement+"    Net_Time VARCHAR, "
		      SQLStatement=SQLStatement+"    Net_TimeB VARCHAR, "
		      SQLStatement=SQLStatement+"    Age_Grade VARCHAR, "
		      SQLStatement=SQLStatement+"    DNS VARCHAR, "
		      SQLStatement=SQLStatement+"    DNF VARCHAR, "
		      SQLStatement=SQLStatement+"    DQ VARCHAR, "
		      SQLStatement=SQLStatement+"    Prevent_External_Updates VARCHAR, "
		      SQLStatement=SQLStatement+"    Laps_Completed INTEGER, "
		      SQLStatement=SQLStatement+"    Overall_Place INTEGER, "
		      SQLStatement=SQLStatement+"    Overall_Back VARCHAR, "
		      SQLStatement=SQLStatement+"    Gender_Place INTEGER, "
		      SQLStatement=SQLStatement+"    Gender_Back VARCHAR, "
		      SQLStatement=SQLStatement+"    Division_Place INTEGER, "
		      SQLStatement=SQLStatement+"    Division_Back VARCHAR, "
		      SQLStatement=SQLStatement+"    Pace VARCHAR, "
		      SQLStatement=SQLStatement+"    Adjustment_Applied VARCHAR, "
		      SQLStatement=SQLStatement+"    Adjustment_Reason VARCHAR, "
		      SQLStatement=SQLStatement+"    NGB_License_Number VARCHAR, "
		      SQLStatement=SQLStatement+"    NGB1_Points DOUBLE, "
		      SQLStatement=SQLStatement+"    NGB2_Points DOUBLE, "
		      SQLStatement=SQLStatement+"    Seed_Group VARCHAR, "
		      SQLStatement=SQLStatement+"    Results_Printed VARCHAR, "
		      SQLStatement=SQLStatement+"    Query_Flag VARCHAR, "
		      SQLStatement=SQLStatement+"    UpdateFlag VARCHAR DEFAULT N ) "
		      app.theDB.DBSQLExecute(SQLStatement)
		      
		      SQLStatement="INSERT INTO participants SELECT * FROM __temp__participants "
		      app.theDB.DBSQLExecute(SQLStatement)
		      
		      SQLStatement="DROP TABLE __temp__participants "
		      app.theDB.DBSQLExecute(SQLStatement)
		      
		      SQLStatement="ALTER TABLE participants RENAME TO __temp__participants "
		      app.theDB.DBSQLExecute(SQLStatement)
		      
		      SQLStatement="CREATE TABLE participants  "
		      SQLStatement=SQLStatement+"    (RaceID INTEGER, "
		      SQLStatement=SQLStatement+"    TeamID INTEGER, "
		      SQLStatement=SQLStatement+"    RaceDistanceID INTEGER, "
		      SQLStatement=SQLStatement+"    DivisionID INTEGER, "
		      SQLStatement=SQLStatement+"    Racer_Number INTEGER, "
		      SQLStatement=SQLStatement+"    Participant_Name VARCHAR, "
		      SQLStatement=SQLStatement+"    First_Name VARCHAR, "
		      SQLStatement=SQLStatement+"    Street VARCHAR, "
		      SQLStatement=SQLStatement+"    City VARCHAR, "
		      SQLStatement=SQLStatement+"    State VARCHAR, "
		      SQLStatement=SQLStatement+"    Postal_Code VARCHAR, "
		      SQLStatement=SQLStatement+"    Country VARCHAR, "
		      SQLStatement=SQLStatement+"    Gender VARCHAR, "
		      SQLStatement=SQLStatement+"    Phone VARCHAR, "
		      SQLStatement=SQLStatement+"    EMail VARCHAR, "
		      SQLStatement=SQLStatement+"    SMS_Address VARCHAR, "
		      SQLStatement=SQLStatement+"    Birth_Date DATE, "
		      SQLStatement=SQLStatement+"    Age INTEGER, "
		      SQLStatement=SQLStatement+"    Representing VARCHAR, "
		      SQLStatement=SQLStatement+"    Assigned_Start VARCHAR, "
		      SQLStatement=SQLStatement+"    Actual_Start VARCHAR, "
		      SQLStatement=SQLStatement+"    Actual_Stop VARCHAR, "
		      SQLStatement=SQLStatement+"    Total_Adjustment VARCHAR, "
		      SQLStatement=SQLStatement+"    Total_Time VARCHAR, "
		      SQLStatement=SQLStatement+"    Net_Time VARCHAR, "
		      SQLStatement=SQLStatement+"    Age_Grade VARCHAR, "
		      SQLStatement=SQLStatement+"    DNS VARCHAR, "
		      SQLStatement=SQLStatement+"    DNF VARCHAR, "
		      SQLStatement=SQLStatement+"    DQ VARCHAR, "
		      SQLStatement=SQLStatement+"    Prevent_External_Updates VARCHAR, "
		      SQLStatement=SQLStatement+"    Laps_Completed INTEGER, "
		      SQLStatement=SQLStatement+"    Adjustment_Applied VARCHAR, "
		      SQLStatement=SQLStatement+"    Adjustment_Reason VARCHAR, "
		      SQLStatement=SQLStatement+"    NGB_License_Number VARCHAR, "
		      SQLStatement=SQLStatement+"    NGB1_Points DOUBLE, "
		      SQLStatement=SQLStatement+"    NGB2_Points DOUBLE, "
		      SQLStatement=SQLStatement+"    Seed_Group VARCHAR, "
		      SQLStatement=SQLStatement+"    Results_Printed VARCHAR, "
		      SQLStatement=SQLStatement+"    Query_Flag VARCHAR, "
		      SQLStatement=SQLStatement+"    UpdateFlag VARCHAR DEFAULT N ) "
		      app.theDB.DBSQLExecute(SQLStatement)
		      
		      SQLStatement="INSERT INTO participants SELECT "
		      SQLStatement=SQLStatement+"RaceID,TeamID,RaceDistanceID,DivisionID,Racer_Number,Participant_Name,First_Name,Street,City,State,Postal_Code,Country,Gender,Phone,EMail,SMS_Address,Birth_Date,Age,Representing,Assigned_Start,Actual_Start,Actual_Stop,Total_Adjustment,Total_Time,Net_Time,Age_Grade,DNS,DNF,DQ,Prevent_External_Updates,Laps_Completed,Adjustment_Applied,Adjustment_Reason,NGB_License_Number,NGB1_Points,NGB2_Points,Seed_Group,Results_Printed,Query_Flag,UpdateFlag FROM __temp__participants "
		      app.theDB.DBSQLExecute(SQLStatement)
		      
		      SQLStatement="DROP TABLE __temp__participants"
		      app.theDB.DBSQLExecute(SQLStatement)
		      
		      app.theDB.DBSQLExecute("ALTER TABLE participants ADD Time_Source VARCHAR DEFAULT TX ")
		      
		      app.theDB.DBCommit
		    end if
		    
		    if (recsRS.Field("DataVersion").StringValue < "2.0.1b15") then
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD UpdateFlag VARCHAR DEFAULT N")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD SMSCount INTEGER")
		      app.theDB.DBCommit
		    end if
		    
		    if (recsRS.Field("DataVersion").StringValue < "2.0.1b17") then
		      app.theDB.DBSQLExecute("ALTER TABLE times ADD TeamMemberID INTEGER DEFAULT 0")
		      app.theDB.DBCommit
		      app.theDB.DBSQLExecute("ALTER TABLE transponders ADD TeamMemberID INTEGER DEFAULT 0")
		      app.theDB.DBCommit
		    end if
		    
		    if (recsRS.Field("DataVersion").StringValue < "2.0.1b35") then
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD TD_Number VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD TD_Name VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD TD_Div VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD ATD_Number VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD ATD_Name VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD ATD_Div VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD CoC_Name VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD CoC_Div VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD Jury_Member_4_Name VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD Jury_Member_4_Div VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD Jury_Member_5_Name VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD Jury_Member_5_Div VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD Lap_Length INTEGER")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD HD INTEGER")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD MC INTEGER")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD TC INTEGER")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD WX VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD FValue VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD MinPointsEventType VARCHAR")
		      
		      app.theDB.DBSQLExecute("ALTER TABLE participants ADD NGB2_License_Number VARCHAR")
		      
		    end if
		    
		    if (recsRS.Field("DataVersion").StringValue < "2.1.24") then
		      app.theDB.DBSQLExecute("ALTER TABLE participants ADD DateTimeInserted DateTimeStamp")
		      app.theDB.DBSQLExecute("ALTER TABLE participants ADD Note VARCHAR")
		      app.theDB.DBSQLExecute("ALTER TABLE races ADD AthletePathRaceID VARCHAR")
		    end if
		    
		    if (recsRS.Field("DataVersion").StringValue < "2.1.28") then
		      app.theDB.DBSQLExecute("ALTER TABLE participants ADD Wave VARCHAR")
		    end if
		    
		    app.theDB.DBSQLExecute("UPDATE races SET DataVersion='"+app.ShortVersion+"'")
		    app.theDB.DBCommit
		    
		    CalculateTimesFrom=recsRS.Field("Calculate_Time_From").stringValue
		    NGB1=recsRS.Field("NGB1").StringValue
		    NGB2=recsRS.Field("NGB2").StringValue
		    RacersPerStart=recsRS.Field("Racers_Per_Start").IntegerValue
		    StartIntervalSec=recsRS.Field("Start_Interval").IntegerValue
		    RaceStartTime=recsRS.Field("Start_Time").StringValue
		    RaceStartTimeSec=ConvertTimeToSeconds(RaceStartTime)
		    RaceDate.SQLDate=recsRS.Field("Race_Date").StringValue
		    RaceAgeDate.SQLDate=recsRS.Field("Race_Age_Date").StringValue
		    StartingRacerNumber=recsRS.Field("Starting_Racer_Number").IntegerValue
		    ResultsDisplayType=recsRS.Field("Display_Type").StringValue
		    PlacesToTruncate=recsRS.Field("Places_To_Truncate").IntegerValue
		    MinimumLapTime=ConvertTimeToSeconds(recsRS.Field("Minimum_Time").StringValue)
		    nthTime=recsRS.Field("nthTime").IntegerValue
		    if theDB.DatabaseType="MySQLDatabase" then
		      wnd_List.Title="Remote: "+recsRS.Field("Race_Name").StringValue
		    else
		      wnd_List.Title=recsRS.Field("Race_Name").StringValue
		    end if
		    JamSession=recsRS.Field("Jam_Session").StringValue
		  end if
		  recsRS.Close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ConvertSecondsToTime(Time as double, IncludeDate as boolean) As string
		  dim Fraction as double
		  dim WholeTime as new date
		  dim Hours, Minutes, Seconds, TimeString as string
		  
		  
		  
		  if IncludeDate then
		    Fraction=val(format(Time-floor(Time),".000"))
		    WholeTime.TotalSeconds=AMBOffset+Floor(Time)
		    TimeString=WholeTime.SQLDateTime+format(Fraction,".000")
		  else
		    Hours=format(Time\3600,"00")
		    Time=Time-val(Hours)*3600
		    Minutes=format(Time\60,"00")
		    Time=Time-val(Minutes)*60
		    Seconds=format(Time,"00.000")
		    TimeString=Hours+":"+Minutes+":"+Seconds
		  end if
		  
		  return TimeString
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ConvertTimeToSeconds(TimeString as string) As Double
		  dim DateTime as new date
		  dim HoursInSeconds, MinutesInSeconds, Seconds as double
		  dim Fraction, TimeParts() as string
		  
		  if (TimeString<>"") and (TimeString<>"DNS") and (TimeString<>"DNF") and (TimeString<>"DQ") then
		    if ((InStr(1,TimeString,"-")=5) and (InStr(6,TimeString,"-")=8)) then 'has the date included with the time "YYYY-MM-DD HH:MM:SS.THT"
		      if (InStr(TimeString,".")>0) then
		        Fraction=mid(TimeString,InStr(TimeString,"."))
		        DateTime.SQLDateTime=Left(TimeString,19)
		        Seconds=DateTime.TotalSeconds+(round(val(Fraction)*1000)/1000)
		      else
		        DateTime.SQLDateTime=Left(TimeString,19)
		        Seconds=DateTime.TotalSeconds
		      end if
		    else
		      TimeParts=Split(TimeString,":")
		      
		      HoursInSeconds=(val(TimeParts(0))*3600)
		      MinutesInSeconds=(val(TimeParts(1))*60)
		      Seconds=round((val(TimeParts(2)))*1000)/1000
		      Seconds=HoursInSeconds+MinutesInSeconds+Seconds
		      
		      if (left(TimeString,1)="-") and (Seconds>0) then
		        Seconds=Seconds*(-1)
		      end if
		      
		    end if
		  else
		    Seconds=0
		  end if
		  
		  return Seconds
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DisplayDataEntryError(MessageStg as string, DetailsStg as string, DataEntryObject as TextField)
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  d.icon=2   //display stop icon
		  
		  DataEntryObject.selStart=0
		  DataEntryObject.selLength=len(DataEntryObject.Text)+1
		  DataEntryObject.setFocus
		  
		  d.Message=MessageStg
		  d.Explanation=DetailsStg
		  b=d.ShowModal
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetBack(BackType as string, RaceDistanceID as string, DivisionID as string, Gender as string, TotalTime as string) As string
		  dim rs as RecordSet
		  dim Back, SQLStatement as string
		  
		  if left(TotalTime,1)<>"D" then
		    
		    Select Case BackType
		    case "Overall"
		      SQLStatement="SELECT Total_Time "
		      SQLStatement=SQLStatement+"FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		      SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		      SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND participants.DNS='N' AND participants.DNF='N' AND participants.DQ='N' ORDER BY Total_Time LIMIT 0,1"
		      
		      
		    case "Gender"
		      SQLStatement="SELECT Total_Time "
		      SQLStatement=SQLStatement+"FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		      SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		      SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND participants.DNS='N' AND participants.DNF='N' AND participants.DQ='N' AND "
		      
		      SQLStatement=SQLStatement+"participants.Gender='"+Gender+"' ORDER BY Total_Time LIMIT 0,1"
		      
		    case "Division"
		      SQLStatement="SELECT Total_Time "
		      SQLStatement=SQLStatement+"FROM participants WHERE DivisionID="+DivisionID+" AND participants.DNS='N' AND participants.DNF='N' AND participants.DQ='N' ORDER BY Total_Time LIMIT 0,1 "
		      
		    end select
		    
		    rs=app.theDB.DBSQLSelect(SQLStatement)
		    
		    if rs<>nil and rs.RecordCount>0 then
		      Back = app.ConvertSecondsToTime(app.ConvertTimeToSeconds(TotalTime)-app.ConvertTimeToSeconds(rs.Field("Total_Time").StringValue),false)
		    else
		      Back=""
		    end if
		    
		    
		  else
		    Back=""
		  end if
		  
		  return Back
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIntervalBack(PlaceType as string, RaceDistanceID as string, DivisionID as string, Gender as string, Interval as string, IntervalTime as String, TeamBack as Boolean) As string
		  dim rs as RecordSet
		  dim Back, SQLStatement as string
		  
		  Select Case PlaceType
		  case "Overall"
		    SQLStatement="SELECT Interval_Time, times.Total_Time as TotalTime FROM times INNER JOIN participants ON times.ParticipantID = participants.rowid "
		    SQLStatement=SQLStatement+"INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		    SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		    SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND "
		    SQLStatement=SQLStatement+"participants.DNS='N' AND participants.DNF='N' AND participants.DQ='N' AND "
		    if TeamBack then
		      SQLStatement=SQLStatement+"participants.TeamID>0 AND "
		      SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Total_Time>'00:00:00.000' AND times.Total_Time<'"+IntervalTime+"'"  'Team Standings are based on total time, not the interval time
		    else
		      SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Interval_Time>'00:00:00.000' AND times.Interval_Time<'"+IntervalTime+"'"
		    end if
		    
		  case "Gender"
		    SQLStatement="SELECT Interval_Time, times.Total_Time as TotalTime FROM times INNER JOIN participants ON times.ParticipantID = participants.rowid "
		    SQLStatement=SQLStatement+"INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		    SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		    SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND "
		    SQLStatement=SQLStatement+"participants.DNS='N' AND participants.DNF='N' AND participants.DQ='N' AND "
		    if TeamBack then
		      SQLStatement=SQLStatement+"participants.TeamID>0 AND "
		      SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Total_Time>'00:00:00.000' AND times.Total_Time<'"+IntervalTime+"'"  'Team Standings are based on total time, not the interval time
		    else
		      SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Interval_Time>'00:00:00.000' AND times.Interval_Time<'"+IntervalTime+"'"
		    end if
		    
		  case "Division"
		    SQLStatement="SELECT Interval_Time, times.Total_Time as TotalTime FROM times INNER JOIN participants ON times.ParticipantID = participants.rowid "
		    SQLStatement=SQLStatement+"INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		    SQLStatement=SQLStatement+"WHERE DivisionID="+DivisionID+" AND "
		    SQLStatement=SQLStatement+"participants.DNS='N' AND participants.DNF='N' AND participants.DQ='N' AND "
		    if TeamBack then
		      SQLStatement=SQLStatement+"participants.TeamID>0 AND "
		      SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Total_Time>'00:00:00.000' AND times.Total_Time<'"+IntervalTime+"'"  'Team Standings are based on total time, not the interval time
		    else
		      SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Interval_Time>'00:00:00.000' AND times.Interval_Time<'"+IntervalTime+"'"
		    end if
		    
		  end select
		  
		  rs=app.theDB.DBSQLSelect(SQLStatement)
		  if rs<>nil and rs.RecordCount>0 then
		    if TeamBack then
		      Back = app.ConvertSecondsToTime(app.ConvertTimeToSeconds(IntervalTime)-app.ConvertTimeToSeconds(rs.Field("TotalTime").StringValue),false)
		    else
		      Back = app.ConvertSecondsToTime(app.ConvertTimeToSeconds(IntervalTime)-app.ConvertTimeToSeconds(rs.Field("Interval_Time").StringValue),false)
		    end if
		    Back = TruncateTime(Back)
		    Back = StripLeadingZeros(Back)
		    Back = "+"+Back
		  elseif rs.RecordCount=0 then
		    Back="+0:00"
		  else
		    Back=""
		  end if
		  rs.Close
		  
		  return Back
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetIntervalPlace(PlaceType as string, RaceDistanceID as string, DivisionID as string, Gender as string, Interval as string, PlacingTime as string, PlacingType as String, ListDNSDNFDQ as Boolean) As string
		  dim rs as RecordSet
		  dim Place, SQLStatement as string
		  
		  if PlacingTime<>"00:00:00.000" then
		    
		    Select Case PlaceType
		    case "Overall"
		      SQLStatement="SELECT COUNT() AS Place FROM times INNER JOIN participants ON times.ParticipantID = participants.rowid "
		      SQLStatement=SQLStatement+"INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		      SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		      SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND "
		      if not(ListDNSDNFDQ) then
		        SQLStatement=SQLStatement+"participants.DNS='N' AND participants.DNF='N' AND participants.DQ='N' AND "
		      end if
		      select case PlacingType 
		      case "Team"
		        SQLStatement=SQLStatement+"participants.TeamID>0 AND "
		        SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Total_Time>'00:00:00.000' AND times.Total_Time<'"+PlacingTime+"'"  'Team Standings are based on total time, not the interval time
		      case "Total Time"
		        SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Total_Time>'00:00:00.000' AND times.Total_Time<'"+PlacingTime+"'" 
		      else
		        SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Interval_Time>'00:00:00.000' AND times.Interval_Time<'"+PlacingTime+"'" 
		      end select
		      
		    case "Gender"
		      SQLStatement="SELECT COUNT() AS Place FROM times INNER JOIN participants ON times.ParticipantID = participants.rowid "
		      SQLStatement=SQLStatement+"INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		      SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		      SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND "
		      SQLStatement=SQLStatement+"participants.Gender='"+Gender+"' AND "
		      if not(ListDNSDNFDQ) then
		        SQLStatement=SQLStatement+"participants.DNS='N' AND participants.DNF='N' AND participants.DQ='N' AND "
		      end if
		      select case PlacingType 
		      case "Team"
		        SQLStatement=SQLStatement+"participants.TeamID>0 AND "
		        SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Total_Time>'00:00:00.000' AND times.Total_Time<'"+PlacingTime+"'"  'Team Standings are based on total time, not the interval time
		      case "Total Time"
		        SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Total_Time>'00:00:00.000' AND times.Total_Time<'"+PlacingTime+"'"
		      else
		        SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Interval_Time>'00:00:00.000' AND times.Interval_Time<'"+PlacingTime+"'"
		      end select
		      
		    case "Division"
		      SQLStatement="SELECT COUNT() as Place FROM times INNER JOIN participants ON times.ParticipantID = participants.rowid "
		      SQLStatement=SQLStatement+"INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		      SQLStatement=SQLStatement+"WHERE DivisionID="+DivisionID+" AND "
		      if not(ListDNSDNFDQ) then
		        SQLStatement=SQLStatement+"participants.DNS='N' AND participants.DNF='N' AND participants.DQ='N' AND "
		      end if
		      select case PlacingType 
		      case "Team"
		        SQLStatement=SQLStatement+"participants.TeamID>0 AND "
		        SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Total_Time>'00:00:00.000' AND times.Total_Time<'"+PlacingTime+"'"  'Team Standings are based on total time, not the interval time
		      case "Total Time"
		        SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Total_Time>'00:00:00.000' AND times.Total_Time<'"+PlacingTime+"'"
		      else
		        SQLStatement=SQLStatement+"times.Use_This_Passing='Y' AND times.Interval_Number="+Interval+" AND times.Interval_Time>'00:00:00.000' AND times.Interval_Time<'"+PlacingTime+"'"
		      end select
		      
		    end select
		    
		    rs=app.theDB.DBSQLSelect(SQLStatement)
		    Place=str(rs.Field("Place").IntegerValue+1)
		    rs.Close
		    
		  else
		    
		    Place="0"
		    
		  end if
		  
		  return Place
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetPlace(PlaceType as string, RaceDistanceID as string, DivisionID as string, Gender as string, TotalTime as string, TeamPlace as boolean, LapsCompleted as string = "0", FISUSSAScoring as string = "", Points as string ="") As string
		  dim TotalTimeSearch as Boolean
		  dim i, ii, Count, RacersAheadOnLaps, teamsChecked() as integer
		  dim rs, rsTeamMembers, rsRaceDistance as RecordSet
		  dim Place, SQLStatement, SQLTemp as string
		  
		  if left(TotalTime,1)<>"D" then
		    if instr(TotalTime,"%")=0 then
		      TotalTimeSearch=true
		    else
		      TotalTimeSearch=false
		    end if
		    
		    Select Case PlaceType
		    case "Overall"
		      if not(TeamPlace) then
		        SQLStatement="SELECT COUNT() AS Place "
		      else
		        SQLStatement="SELECT TeamID "
		      end if
		      SQLStatement=SQLStatement+"FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		      SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		      SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND participants.DNS='N' AND participants.DNF='N' AND (participants.DQ='N' OR participants.DQ is null) AND "
		      if TeamPlace then
		        SQLStatement=SQLStatement+"participants.TeamID>0 AND "
		      end if
		      
		      if FISUSSAScoring="FIS" then
		        SQLStatement=SQLStatement+" NGB2_License_Number <> '' AND NGB2_Points <> 'inact' AND "
		      elseif FISUSSAScoring="USSA" then
		        SQLStatement=SQLStatement+" ((NGB_License_Number <> '' AND NGB1_Points <> 'inact')"
		        SQLStatement=SQLStatement+" OR (NGB_License_Number = '' AND NGB2_License_Number <> '' AND NGB2_Points <> 'inact')) AND "
		      end if
		      
		      if app.RacersPerStart<>9998 then
		        if TotalTimeSearch then
		          SQLStatement=SQLStatement+"participants.Total_Time<'"+TotalTime+"'"
		        else
		          SQLStatement=SQLStatement+"participants.Age_Grade>'"+TotalTime+"'"
		        end if
		        RacersAheadOnLaps=0
		      else
		        SQLTemp=SQLStatement
		        SQLStatement=SQLStatement+"participants.Laps_Completed>"+LapsCompleted
		        rs=app.theDB.DBSQLSelect(SQLStatement)
		        RacersAheadOnLaps=rs.Field("Place").IntegerValue
		        
		        if TotalTimeSearch then
		          SQLStatement=SQLTemp+"participants.Laps_Completed="+LapsCompleted+" AND participants.Total_Time<'"+TotalTime+"'"
		        else
		          SQLStatement=SQLTemp+"participants.Laps_Completed="+LapsCompleted+" AND participants.Age_Grade>'"+TotalTime+"'"
		        end if
		      end if
		      
		      'if (app.ResultsDisplayType=wnd_List.constFISUSSAResultType)  then
		      'SQLStatement=SQLStatement+" AND participants.NGB2_Points<'"+Points+"'"
		      'end if
		      
		      if not(TeamPlace) then
		        rs=app.theDB.DBSQLSelect(SQLStatement)
		        Place=str(rs.Field("Place").IntegerValue+1+RacersAheadOnLaps)
		        rs.Close
		        
		      else
		        Count=0
		        redim teamsChecked(0)
		        rsRaceDistance=app.theDB.DBSQLSelect("SELECT Min_CC_Scorers, Max_CC_Scorers FROM raceDistances WHERE rowid="+RaceDistanceID)
		        rs=app.theDB.DBSQLSelect(SQLStatement)
		        'go through each of the TeamID to see if there are enough finishers to Count as a team
		        
		        for i=1 to rs.RecordCount
		          if teamsChecked.IndexOf(rs.Field("TeamID").IntegerValue) < 0 then
		            SQLStatement="SELECT participants.Racer_Number, participants.Total_Time FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		            SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		            SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND participants.DNS='N' AND participants.DNF='N' AND (participants.DQ='N' OR participants.DQ is null) AND "
		            SQLStatement=SQLStatement+"TeamID="+rs.Field("TeamID").StringValue+" ORDER BY participants.Total_Time LIMIT 0,"+ rsRaceDistance.Field("Max_CC_Scorers").StringValue
		            rsTeamMembers=app.theDB.DBSQLSelect(SQLStatement)
		            if rsTeamMembers.RecordCount >= rsRaceDistance.Field("Min_CC_Scorers").IntegerValue then
		              for ii = 1 to rsTeamMembers.RecordCount
		                if rsTeamMembers.Field("Total_Time").StringValue < TotalTime then
		                  Count=Count+1
		                end if
		                rsTeamMembers.MoveNext
		              next
		            end if
		            rsTeamMembers.Close
		            teamsChecked.Append rs.Field("TeamID").IntegerValue
		          end if
		          rs.MoveNext
		        next
		        rs.Close
		        rsRaceDistance.Close
		        Place=str(Count+1+RacersAheadOnLaps)
		      end if
		      
		    case "Gender"
		      if not(TeamPlace) then
		        SQLStatement="SELECT COUNT() AS Place "
		      else
		        SQLStatement="SELECT TeamID "
		      end if
		      SQLStatement=SQLStatement+"FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		      SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		      SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND participants.DNS='N' AND participants.DNF='N' AND (participants.DQ='N' OR participants.DQ is null) AND "
		      if TeamPlace then
		        SQLStatement=SQLStatement+"participants.TeamID>0 AND "
		      end if
		      SQLStatement=SQLStatement+"participants.Gender='"+Gender+"' AND "
		      
		      if app.RacersPerStart<>9998 then
		        if TotalTimeSearch then
		          SQLStatement=SQLStatement+"participants.Total_Time<'"+TotalTime+"'"
		        else
		          SQLStatement=SQLStatement+"participants.Age_Grade>'"+TotalTime+"'"
		        end if
		        RacersAheadOnLaps=0
		      else
		        SQLTemp=SQLStatement
		        SQLStatement=SQLStatement+"participants.Laps_Completed>"+LapsCompleted
		        rs=app.theDB.DBSQLSelect(SQLStatement)
		        RacersAheadOnLaps=rs.Field("Place").IntegerValue
		        
		        if TotalTimeSearch then
		          SQLStatement=SQLTemp+"participants.Laps_Completed="+LapsCompleted+" AND participants.Total_Time<'"+TotalTime+"'"
		        else
		          SQLStatement=SQLTemp+"participants.Laps_Completed="+LapsCompleted+" AND participants.Age_Grade>'"+TotalTime+"'"
		        end if
		        
		      end if
		      
		      if not(TeamPlace) then
		        rs=app.theDB.DBSQLSelect(SQLStatement)
		        Place=str(rs.Field("Place").IntegerValue+1+RacersAheadOnLaps)
		        rs.Close
		        
		      else
		        Count=0
		        redim teamsChecked(0)
		        rsRaceDistance=app.theDB.DBSQLSelect("SELECT Min_CC_Scorers, Max_CC_Scorers FROM raceDistances WHERE rowid="+RaceDistanceID)
		        rs=app.theDB.DBSQLSelect(SQLStatement)
		        'go through each of the TeamID to see if there are enough finishers to Count as a team
		        
		        for i=1 to rs.RecordCount
		          if teamsChecked.IndexOf(rs.Field("TeamID").IntegerValue) < 0 then
		            SQLStatement="SELECT participants.Racer_Number, participants.Total_Time FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		            SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		            SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND participants.DNS='N' AND participants.DNF='N' AND (participants.DQ='N' OR participants.DQ is null) AND "
		            SQLStatement=SQLStatement+"TeamID="+rs.Field("TeamID").StringValue+" ORDER BY participants.Total_Time LIMIT 0,"+ rsRaceDistance.Field("Max_CC_Scorers").StringValue
		            rsTeamMembers=app.theDB.DBSQLSelect(SQLStatement)
		            if rsTeamMembers.RecordCount >= rsRaceDistance.Field("Min_CC_Scorers").IntegerValue then
		              for ii = 1 to rsTeamMembers.RecordCount
		                if rsTeamMembers.Field("Total_Time").StringValue < TotalTime then
		                  Count=Count+1
		                end if
		                rsTeamMembers.MoveNext
		              next
		            end if
		            rsTeamMembers.Close
		            teamsChecked.Append rs.Field("TeamID").IntegerValue
		          end if
		          rs.MoveNext
		        next
		        rs.Close
		        rsRaceDistance.Close
		        Place=str(Count+1+RacersAheadOnLaps)
		      end if
		      
		    case "Division"
		      if not(TeamPlace) then
		        SQLStatement="SELECT Count() as Place "
		      else
		        SQLStatement="SELECT TeamID "
		      end if
		      SQLStatement=SQLStatement+"FROM participants WHERE DivisionID="+DivisionID+" AND participants.DNS='N' AND participants.DNF='N' AND (participants.DQ='N' OR participants.DQ is null) AND "
		      if TeamPlace then
		        SQLStatement=SQLStatement+"participants.TeamID>0 AND "
		      end if
		      
		      if app.RacersPerStart<>9998 then
		        if TotalTimeSearch then
		          SQLStatement=SQLStatement+"participants.Total_Time<'"+TotalTime+"'"
		        else
		          SQLStatement=SQLStatement+"participants.Age_Grade>'"+TotalTime+"'"
		        end if
		        RacersAheadOnLaps=0
		      else
		        SQLTemp=SQLStatement
		        SQLStatement=SQLStatement+"participants.Laps_Completed>"+LapsCompleted
		        rs=app.theDB.DBSQLSelect(SQLStatement)
		        RacersAheadOnLaps=rs.Field("Place").IntegerValue
		        
		        if TotalTimeSearch then
		          SQLStatement=SQLTemp+"participants.Laps_Completed="+LapsCompleted+" AND participants.Total_Time<'"+TotalTime+"'"
		        else
		          SQLStatement=SQLTemp+"participants.Laps_Completed="+LapsCompleted+" AND participants.Age_Grade>'"+TotalTime+"'"
		        end if
		        
		      end if
		      
		      if not(TeamPlace) then
		        rs=app.theDB.DBSQLSelect(SQLStatement)
		        Place=str(rs.Field("Place").IntegerValue+1+RacersAheadOnLaps)
		        rs.Close
		        
		      else
		        Count=0
		        redim teamsChecked(0)
		        rsRaceDistance=app.theDB.DBSQLSelect("SELECT Min_CC_Scorers, Max_CC_Scorers FROM raceDistances WHERE rowid="+RaceDistanceID)
		        rs=app.theDB.DBSQLSelect(SQLStatement)
		        'go through each of the TeamID to see if there are enough finishers to Count as a team
		        
		        for i=1 to rs.RecordCount
		          if teamsChecked.IndexOf(rs.Field("TeamID").IntegerValue) < 0 then
		            SQLStatement="SELECT participants.Racer_Number, participants.Total_Time FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		            SQLStatement=SQLStatement+"INNER JOIN racedistances ON divisions.RaceDistanceID = racedistances.rowid "
		            SQLStatement=SQLStatement+"WHERE racedistances.rowid="+RaceDistanceID+" AND participants.DNS='N' AND participants.DNF='N' AND (participants.DQ='N' OR participants.DQ is null) AND "
		            SQLStatement=SQLStatement+"TeamID="+rs.Field("TeamID").StringValue+" ORDER BY participants.Total_Time LIMIT 0,"+ rsRaceDistance.Field("Max_CC_Scorers").StringValue
		            rsTeamMembers=app.theDB.DBSQLSelect(SQLStatement)
		            if rsTeamMembers.RecordCount >= rsRaceDistance.Field("Min_CC_Scorers").IntegerValue then
		              for ii = 1 to rsTeamMembers.RecordCount
		                if rsTeamMembers.Field("Total_Time").StringValue < TotalTime then
		                  Count=Count+1
		                end if
		                rsTeamMembers.MoveNext
		              next
		            end if
		            rsTeamMembers.Close
		            teamsChecked.Append rs.Field("TeamID").IntegerValue
		          end if
		          rs.MoveNext
		        next
		        rs.Close
		        rsRaceDistance.Close
		        Place=str(Count+1+RacersAheadOnLaps)
		      end if
		      
		    end select
		    
		    
		    
		  else
		    Place=""
		  end if
		  
		  return Place
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetRacerNumber(TXCode as string) As String
		  dim SQLStatement as string
		  dim res as Boolean
		  dim rs as RecordSet
		  dim RacerNumber as string
		  dim recCount as Integer
		  
		  SQLStatement="Select Racer_Number FROM Transponders WHERE RaceID=1 AND TX_Code='"+TXCode+"'"
		  rs=app.theDB.DBSQLSelect(SQLStatement)
		  if rs.EOF then
		    RacerNumber="0"
		  else
		    RacerNumber=rs.Field("Racer_Number").StringValue
		  end if
		  Return RacerNumber
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub InitCRCTable()
		  Dim crc as MemoryBlock
		  dim i as integer
		  dim j as integer
		  
		  crc=NewMemoryBlock(4)
		  
		  For  i = 0  to 255
		    crc.UShort(0) = Bitwise.ShiftLeft(i,8)
		    For j = 0 to 7
		      if Bitwise.BitAnd(crc.UShort(0),val("&h8000")) >  0 then
		        crc.UShort(0)= bitwise.BitXor(Bitwise.ShiftLeft(crc.UShort(0),1),val("&h1021"))
		      else
		        crc.UShort(0)= bitwise.BitXor(Bitwise.ShiftLeft(crc.UShort(0),1),0)
		      end if
		    Next  
		    
		    CRC16Table(i)=NewMemoryBlock(4)
		    CRC16Table(i).UShort(0) = crc.UShort(0)
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub LoadAgeStandards()
		  dim FemaleStandardString, aFemaleStandardString(), MaleStandardString, aMaleStandardString() , AgeGradeDistances as string
		  dim i, ii, idx, Standard as integer
		  
		  AgeGradeDistances="none,5 km,6 km,4 Mile,8 km,5 Mile,10 km,12 km,15 km,10 Mile,20 km,Half Mar,25 km,30 km,Marathon,50 km,50 Mile,100 km,150 km,100 Mile,200 km"
		  AgeGrade_Distances=Split(AgeGradeDistances,",")
		  
		  FemaleStandardString="5,1225,1480,1592,1993,2003,2510,3034,3823,4112,5156,5448,6499,7857,11207,13545,24497,32539,54759,60000,79448,6,1172,1416,1523,1907,1916,2405,2927,3785,4142,5336,5690,6915,8518,12388,14972,27077,35967,60528,66321,87818,7,1126,1361,1463,1832,1841,2311,2811,3623,3957,5081,5412,6562,8063,11697,14138,25569,33963,57155,62626,82925,8,1087,1313,1412,1768,1777,2230,2710,3484,3798,4863,5174,6261,7678,11113,13432,24292,32268,54302,59499,78785,9,1053,1272,1368,1713,1722,2160,2624,3364,3661,4675,4970,6003,7348,10615,12830,23204,30822,51868,56833,75255,10,1024,1237,1330,1666,1674,2100,2549,3261,3543,4514,4795,5782,7064,10188,12313,22270,29581,49781,54545,72226,11,998,1206,1298,1625,1633,2048,2484,3172,3441,4374,4643,5590,6820,9820,11869,21465,28512,47982,52574,69616,12,977,1180,1269,1589,1597,2004,2429,3095,3354,4254,4512,5425,6609,9502,11484,20770,27589,46427,50871,67361,13,958,1158,1245,1559,1567,1965,2382,3029,3278,4150,4399,5282,6426,9227,11151,20168,26790,45083,49398,65410,14,943,1139,1225,1534,1542,1933,2342,2973,3213,4061,4302,5159,6268,8989,10864,19648,26099,43921,48125,63724,15,930,1124,1208,1513,1520,1907,2308,2926,3159,3985,4219,5053,6133,8784,10616,19200,25504,42919,47027,62270,16,918,1110,1193,1494,1502,1883,2278,2883,3109,3916,4144,4957,6010,8598,10392,18794,24964,42011,46032,60952,17,907,1096,1179,1476,1483,1859,2249,2842,3061,3849,4071,4865,5892,8420,10176,18404,24447,41140,45078,59689,18,898,1085,1166,1461,1468,1840,2225,2807,3021,3794,4011,4789,5795,8273,9999,18084,24021,40424,44293,58650,19,891,1077,1159,1451,1458,1827,2209,2785,2995,3758,3972,4739,5731,8177,9883,17874,23743,39956,43780,57971,20,888,1073,1154,1446,1453,1821,2201,2773,2982,3740,3952,4715,5700,8131,9827,17772,23608,39728,43530,57640,21,888,1073,1154,1445,1452,1820,2200,2772,2981,3738,3950,4712,5696,8125,9820,17760,23591,39700,43500,57600,22,888,1073,1154,1445,1452,1820,2200,2772,2981,3738,3950,4712,5696,8125,9820,17760,23591,39700,43500,57600,23,888,1073,1154,1445,1452,1820,2200,2772,2981,3738,3950,4712,5696,8125,9820,17760,23591,39700,43500,57600,24,888,1073,1154,1445,1452,1820,2200,2772,2981,3738,3950,4712,5696,8125,9820,17760,23591,39700,43500,57600,25,888,1073,1154,1445,1452,1820,2200,2772,2981,3738,3950,4712,5696,8125,9820,17760,23591,39700,43500,57600,26,888,1073,1154,1445,1452,1820,2200,2772,2981,3738,3950,4712,5696,8125,9820,17760,23591,39700,43500,57600,27,888,1073,1154,1445,1452,1820,2200,2772,2981,3738,3950,4712,5696,8125,9820,17760,23591,39700,43500,57600,28,888,1073,1154,1445,1452,1820,2200,2772,2981,3738,3950,4712,5696,8125,9820,17760,23591,39700,43500,57600,29,888,1073,1154,1445,1452,1820,2200,2772,2981,3738,3950,4712,5696,8125,9820,17760,23591,39700,43500,57600,30,888,1073,1154,1445,1452,1821,2201,2773,2982,3739,3951,4713,5698,8128,9824,17767,23600,39716,43517,57623,31,889,1074,1155,1447,1454,1822,2202,2775,2984,3742,3954,4718,5703,8139,9837,17790,23631,39768,43574,57698,32,890,1076,1157,1448,1455,1824,2205,2779,2988,3747,3960,4724,5713,8156,9857,17828,23681,39851,43666,57820,33,892,1078,1159,1451,1458,1828,2210,2784,2994,3754,3967,4734,5726,8180,9886,17880,23750,39968,43793,57989,34,894,1080,1162,1455,1462,1832,2215,2790,3001,3763,3976,4747,5743,8211,9924,17948,23841,40121,43962,58211,35,897,1083,1165,1459,1466,1838,2221,2799,3010,3774,3988,4762,5764,8250,9971,18032,23953,40309,44167,58483,36,900,1087,1169,1464,1471,1844,2229,2809,3020,3787,4002,4780,5789,8295,10026,18132,24085,40531,44410,58806,37,903,1092,1174,1470,1477,1851,2238,2820,3033,3803,4018,4802,5818,8349,10090,18249,24241,40793,44698,59186,38,908,1097,1179,1477,1484,1860,2248,2833,3046,3820,4037,4826,5850,8411,10166,18385,24421,41097,45031,59627,39,912,1102,1186,1484,1492,1870,2260,2848,3062,3840,4058,4854,5889,8480,10249,18537,24623,41436,45402,60119,40,918,1109,1192,1493,1500,1881,2273,2864,3080,3862,4081,4884,5930,8559,10344,18709,24851,41820,45823,60676,41,923,1116,1200,1503,1510,1892,2288,2882,3100,3887,4107,4919,5977,8647,10451,18902,25107,42252,46296,61303,42,930,1123,1208,1513,1520,1906,2303,2902,3121,3914,4136,4957,6029,8744,10568,19113,25389,42725,46814,61989,43,937,1132,1217,1524,1532,1920,2321,2924,3145,3943,4167,4998,6085,8848,10694,19340,25690,43232,47370,62725,44,944,1141,1227,1537,1544,1936,2340,2948,3171,3976,4201,5043,6148,8954,10822,19572,25998,43751,47939,63478,45,953,1151,1238,1551,1558,1953,2361,2975,3199,4011,4239,5093,6216,9063,10954,19810,26315,44283,48522,64250,46,962,1162,1250,1565,1573,1971,2383,3003,3229,4049,4279,5146,6289,9175,11089,20054,26638,44828,49119,65041,47,972,1174,1263,1581,1589,1991,2407,3033,3262,4090,4322,5204,6366,9289,11227,20304,26970,45387,49731,65851,48,982,1187,1277,1598,1606,2013,2434,3066,3298,4135,4369,5267,6444,9406,11368,20560,27311,45960,50359,66682,49,994,1201,1291,1617,1625,2036,2462,3102,3336,4183,4420,5332,6525,9526,11514,20823,27660,46547,51002,67534,50,1006,1215,1307,1637,1645,2062,2492,3140,3377,4234,4474,5399,6607,9650,11663,21093,28018,47150,51663,68409,51,1018,1231,1324,1657,1665,2087,2523,3179,3419,4287,4530,5467,6692,9776,11816,21369,28385,47768,52340,69306,52,1031,1246,1340,1678,1686,2114,2555,3220,3462,4341,4588,5537,6779,9906,11973,21653,28762,48403,53036,70227,53,1045,1262,1357,1700,1708,2141,2588,3261,3507,4397,4647,5609,6868,10040,12134,21945,29150,49055,53750,71173,54,1058,1279,1375,1722,1730,2169,2622,3303,3552,4454,4707,5683,6959,10177,12300,22244,29548,49724,54484,72144,55,1072,1295,1393,1745,1753,2197,2656,3347,3599,4513,4769,5758,7053,10317,12470,22552,29957,50413,55238,73143,56,1086,1313,1412,1768,1776,2227,2691,3391,3647,4573,4832,5836,7149,10462,12645,22869,30377,51120,56013,74169,57,1101,1330,1431,1792,1800,2257,2728,3437,3696,4635,4898,5916,7249,10611,12825,23194,30810,51848,56811,75225,58,1116,1349,1450,1816,1825,2288,2765,3484,3747,4698,4965,5998,7351,10764,13010,23529,31255,52597,57631,76312,59,1132,1367,1471,1841,1850,2319,2804,3533,3799,4764,5034,6082,7455,10922,13201,23874,31713,53367,58476,77430,60,1148,1387,1491,1867,1876,2352,2843,3582,3852,4831,5105,6169,7563,11085,13397,24229,32184,54161,59345,78581,61,1164,1406,1513,1894,1903,2386,2884,3634,3907,4900,5178,6258,7674,11252,13599,24595,32670,54979,60241,79767,62,1181,1427,1535,1922,1931,2420,2926,3686,3964,4971,5253,6350,7789,11424,13808,24972,33171,55821,61164,80990,63,1198,1448,1557,1950,1959,2456,2969,3740,4022,5044,5330,6445,7907,11602,14023,25361,33687,56690,62116,82250,64,1216,1469,1580,1979,1988,2492,3013,3796,4082,5119,5409,6543,8028,11786,14244,25762,34220,57586,63098,83551,65,1235,1492,1604,2009,2019,2530,3059,3854,4144,5197,5491,6643,8153,11975,14473,26175,34769,58511,64112,84893,66,1254,1515,1629,2040,2050,2569,3106,3913,4208,5277,5576,6747,8283,12170,14709,26603,35337,59467,65159,86279,67,1273,1538,1654,2072,2082,2609,3154,3974,4274,5359,5663,6854,8416,12372,14954,27044,35924,60454,66240,87711,68,1293,1563,1681,2105,2115,2651,3204,4037,4342,5444,5753,6964,8554,12581,15206,27501,36530,61474,67358,89192,69,1314,1588,1708,2139,2149,2694,3256,4102,4412,5532,5846,7078,8696,12797,15467,27973,37157,62530,68515,90723,70,1336,1614,1736,2174,2184,2738,3309,4170,4484,5623,5942,7196,8843,13021,15737,28462,37806,63622,69712,92308,71,1358,1641,1765,2210,2221,2783,3364,4239,4559,5716,6041,7318,8996,13252,16017,28968,38478,64753,70951,93949,72,1381,1669,1795,2247,2258,2830,3421,4311,4636,5813,6143,7444,9153,13492,16307,29492,39175,65925,72235,95649,73,1405,1698,1826,2286,2297,2879,3480,4385,4716,5914,6249,7574,9316,13741,16607,30036,39897,67140,73567,97412,74,1429,1727,1858,2326,2337,2930,3542,4462,4799,6017,6359,7709,9485,14001,16922,30605,40653,68413,74961,99259,75,1455,1758,1891,2368,2379,2982,3605,4542,4884,6125,6472,7849,9661,14289,17270,31235,41490,69821,76504,101301,76,1481,1790,1925,2411,2422,3036,3670,4625,4973,6236,6590,7995,9843,14611,17659,31937,42422,71390,78223,103578,77,1509,1823,1961,2455,2467,3093,3738,4710,5065,6352,6712,8145,10037,14966,18088,32713,43454,73126,80125,106097,78,1537,1858,1998,2502,2514,3151,3809,4799,5161,6472,6839,8303,10258,15362,18567,33579,44604,75061,82246,108905,79,1567,1893,2036,2550,2562,3212,3882,4891,5260,6596,6970,8479,10507,15804,19101,34546,45888,77222,84614,112040,80,1599,1933,2079,2603,2615,3278,3963,4993,5369,6733,7115,8681,10788,16299,19699,35627,47324,79639,87262,115547,81,1637,1978,2127,2664,2676,3355,4055,5110,5495,6890,7281,8912,11108,16853,20369,36839,48934,82348,90230,119477,82,1680,2030,2183,2734,2747,3443,4162,5244,5639,7072,7473,9174,11468,17473,21118,38194,50733,85376,93548,123871,83,1729,2090,2247,2814,2828,3544,4284,5398,5805,7279,7692,9473,11877,18173,21964,39723,52764,88794,97294,128830,84,1786,2158,2321,2906,2920,3660,4425,5575,5996,7518,7944,9815,12342,18966,22923,41457,55068,92670,101541,134454,85,1851,2237,2406,3012,3027,3794,4586,5779,6214,7792,8234,10208,12872,19870,24016,43434,57694,97090,106383,140866,86,1926,2328,2503,3134,3150,3948,4772,6013,6466,8108,8568,10658,13485,20908,25270,45703,60708,102162,111940,148224,87,2013,2433,2616,3276,3292,4126,4988,6284,6758,8474,8955,11179,14190,22103,26714,48313,64176,107998,118335,156692,88,2114,2555,2748,3440,3457,4333,5238,6600,7098,8900,9405,11786,15017,23496,28398,51359,68222,114806,125795,166570,89,2233,2698,2902,3633,3651,4576,5532,6970,7496,9399,9932,12499,15987,25139,30384,54950,72992,122834,134592,178218,90,2373,2867,3084,3862,3880,4864,5879,7408,7966,9989,10556,13345,17141,27101,32755,59239,78689,132422,145097,192128,91,2541,3070,3302,4134,4155,5207,6295,7931,8529,10695,11302,14361,18536,29481,35631,64441,85599,144049,157837,208999,92,2744,3316,3566,4465,4487,5624,6799,8566,9212,11551,12206,15608,20249,32409,39170,70842,94101,158357,173514,229757,93,2995,3619,3892,4874,4897,6138,7420,9349,10054,12607,13322,17153,22390,36111,43644,78933,104849,176444,193333,256000,94,3311,4001,4303,5388,5414,6786,8203,10336,11115,13937,14728,19123,25159,40932,49471,89471,118846,200000,219144,290176,95,3720,4495,4835,6054,6083,7625,9217,11613,12488,15660,16548,21714,28841,47459,57360,103738,137798,231893,254089,336449,96,4269,5159,5548,6947,6981,8750,10577,13327,14332,17971,18990,25265,33986,56778,68623,124109,164857,277428,303983,402516,97,5043,6093,6553,8206,8245,10335,12493,15741,16928,21227,22430,30420,41668,71085,85914,155381,206395,347332,380577,503937,98,6210,7503,8070,10105,10154,12727,15385,19385,20846,26140,27622,38560,54351,95927,115939,209681,278524,468713,513577,680047,99,8169,9871,10616,13293,13358,16743,20239,25501,27424,34388,36339,53243,79221,149632,180847,327072,434457,731123,801105,1060773,100,12131,14658,15765,19740,19836,24863,30055,37869,40724,51066,53962,87910,149895,351732,425108,768831,1021255,1718615,1883117,2493506,,,,,,,,,,,,,,,,,,,,,,"
		  aFemaleStandardString=split(FemaleStandardString,",")
		  
		  for idx = 0 to UBound(aFemaleStandardString)
		    Standard=val(aFemaleStandardString(idx))
		    if Standard <=100 then
		      i=Standard
		      ii=1
		    else
		      AgeStandard_Female(i,ii)=Standard
		      ii=ii+1
		    end if
		  next
		  
		  MaleStandardString="5,1186,1440,1549,1949,1961,2469,2990,3793,4100,5247,5579,6771,8296,12067,14619,25890,34391,58445,64160,85010,6,1122,1363,1465,1844,1855,2335,2828,3587,3876,4952,5262,6378,7807,11349,13749,24349,32344,54967,60342,79952,7,1068,1297,1394,1754,1766,2222,2691,3412,3687,4703,4995,6046,7395,10746,13018,23054,30624,52043,57133,75699,8,1021,1240,1334,1678,1689,2126,2574,3263,3525,4491,4768,5765,7046,10233,12398,21955,29164,49563,54410,72092,9,981,1192,1282,1613,1623,2043,2474,3136,3387,4310,4573,5524,6748,9796,11868,21017,27918,47445,52085,69011,10,947,1150,1237,1557,1567,1972,2388,3026,3267,4153,4406,5317,6492,9421,11413,20211,26848,45626,50088,66365,11,918,1115,1199,1508,1518,1910,2313,2931,3165,4019,4262,5139,6271,9097,11021,19517,25925,44059,48368,64085,12,892,1084,1165,1466,1476,1857,2249,2849,3076,3902,4137,4985,6080,8818,10682,18918,25129,42706,46882,62118,13,870,1057,1137,1430,1439,1811,2194,2779,2999,3802,4030,4852,5915,8576,10390,18400,24442,41538,45600,60419,14,851,1034,1112,1399,1408,1772,2146,2718,2933,3716,3937,4737,5774,8369,10138,17954,23850,40531,44495,58955,15,835,1014,1091,1373,1381,1739,2106,2666,2877,3642,3858,4639,5652,8190,9922,17572,23342,39668,43547,57699,16,822,998,1073,1350,1359,1710,2071,2623,2830,3580,3791,4556,5549,8038,9738,17246,22909,38932,42739,56628,17,810,984,1059,1332,1340,1687,2043,2586,2791,3527,3735,4486,5461,7910,9583,16971,22544,38311,42058,55726,18,800,972,1046,1315,1324,1666,2018,2554,2755,3481,3685,4423,5383,7795,9444,16724,22215,37754,41446,54914,19,791,960,1033,1299,1307,1646,1993,2522,2721,3435,3636,4362,5307,7683,9308,16484,21896,37212,40851,54126,20,782,950,1022,1286,1294,1628,1972,2496,2692,3397,3595,4311,5243,7590,9195,16284,21630,36759,40354,53468,21,777,944,1015,1277,1285,1617,1959,2479,2673,3372,3568,4277,5202,7529,9121,16153,21457,36464,40030,53039,22,774,940,1011,1273,1281,1612,1952,2470,2664,3360,3555,4261,5182,7499,9085,16088,21371,36318,39870,52826,23,774,940,1011,1272,1280,1611,1951,2469,2663,3358,3553,4259,5179,7495,9080,16080,21360,36300,39850,52800,24,774,940,1011,1272,1280,1611,1951,2469,2663,3358,3553,4259,5179,7495,9080,16080,21360,36300,39850,52800,25,774,940,1011,1272,1280,1611,1951,2469,2663,3358,3553,4259,5179,7495,9080,16080,21360,36300,39850,52800,26,774,940,1011,1272,1280,1611,1951,2469,2663,3358,3553,4259,5179,7495,9080,16080,21360,36300,39850,52800,27,774,940,1011,1272,1280,1611,1951,2469,2663,3358,3553,4259,5179,7495,9080,16080,21360,36300,39850,52800,28,774,940,1011,1272,1280,1611,1951,2469,2663,3358,3553,4259,5179,7495,9080,16080,21360,36300,39850,52800,29,775,941,1012,1273,1281,1612,1951,2469,2663,3358,3553,4259,5179,7495,9080,16080,21360,36300,39850,52800,30,776,942,1014,1275,1283,1615,1953,2469,2663,3358,3553,4259,5179,7495,9080,16080,21360,36300,39850,52800,31,778,945,1016,1278,1286,1619,1956,2471,2665,3358,3553,4259,5179,7495,9080,16080,21360,36300,39850,52800,32,780,947,1019,1282,1290,1624,1961,2476,2669,3360,3554,4259,5179,7495,9080,16080,21360,36300,39850,52800,33,783,951,1023,1287,1295,1630,1968,2483,2675,3365,3559,4261,5179,7495,9080,16080,21360,36300,39850,52800,34,787,955,1027,1293,1301,1637,1976,2492,2684,3374,3567,4268,5182,7495,9080,16080,21360,36300,39850,52800,35,791,960,1033,1300,1308,1646,1986,2503,2696,3387,3580,4280,5191,7495,9080,16080,21360,36300,39850,52800,36,796,966,1039,1307,1316,1656,1998,2517,2711,3403,3597,4298,5209,7503,9089,16096,21381,36336,39890,52853,37,801,973,1046,1316,1325,1667,2012,2533,2728,3424,3618,4321,5234,7525,9116,16145,21446,36446,40010,53012,38,807,980,1054,1326,1334,1680,2026,2552,2748,3448,3644,4351,5267,7563,9162,16226,21554,36630,40212,53280,39,813,987,1062,1336,1344,1692,2042,2572,2769,3475,3672,4385,5309,7617,9228,16341,21707,36890,40498,53659,40,819,995,1070,1346,1354,1705,2057,2591,2791,3502,3701,4420,5351,7680,9304,16477,21887,37196,40834,54104,41,825,1002,1078,1356,1365,1717,2073,2611,2812,3530,3731,4455,5395,7744,9381,16613,22068,37504,41172,54551,42,831,1010,1086,1366,1375,1730,2089,2632,2834,3558,3761,4491,5439,7808,9459,16752,22252,37816,41515,55006,43,838,1017,1094,1377,1385,1744,2105,2652,2857,3587,3791,4528,5484,7874,9539,16893,22439,38134,41864,55468,44,844,1025,1103,1387,1396,1757,2121,2674,2880,3616,3822,4565,5530,7940,9620,17036,22630,38457,42218,55938,45,851,1033,1111,1398,1407,1771,2138,2695,2903,3645,3853,4603,5575,8009,9703,17183,22825,38790,42584,56422,46,857,1041,1120,1409,1418,1784,2155,2716,2926,3675,3885,4641,5623,8078,9787,17331,23022,39125,42951,56909,47,864,1049,1129,1420,1429,1798,2172,2738,2950,3706,3917,4680,5671,8149,9872,17482,23222,39465,43325,57404,48,871,1058,1137,1431,1440,1813,2189,2761,2974,3737,3950,4720,5719,8220,9958,17635,23426,39811,43705,57907,49,878,1066,1147,1443,1452,1827,2207,2784,2999,3768,3983,4760,5769,8293,10046,17792,23634,40164,44092,58420,50,885,1075,1156,1454,1463,1842,2225,2807,3024,3800,4017,4802,5820,8368,10137,17952,23847,40527,44490,58948,51,892,1083,1165,1466,1475,1857,2243,2830,3050,3833,4052,4844,5871,8443,10229,18114,24062,40892,44891,59480,52,899,1092,1175,1478,1487,1872,2262,2854,3075,3866,4087,4886,5924,8520,10322,18279,24281,41264,45300,60020,53,907,1101,1184,1490,1500,1887,2281,2879,3102,3900,4123,4929,5976,8598,10416,18447,24504,41643,45715,60571,54,914,1110,1194,1503,1512,1903,2300,2903,3129,3934,4159,4974,6031,8678,10513,18618,24731,42028,46139,61132,55,922,1120,1204,1515,1525,1919,2320,2928,3156,3969,4196,5018,6086,8760,10612,18794,24965,42426,46576,61711,56,930,1129,1215,1528,1538,1935,2340,2954,3184,4004,4234,5064,6142,8843,10713,18971,25201,42827,47015,62294,57,938,1139,1225,1541,1551,1952,2360,2980,3212,4040,4272,5111,6199,8927,10815,19152,25441,43235,47463,62887,58,946,1149,1235,1554,1564,1968,2381,3007,3241,4078,4311,5158,6258,9013,10919,19336,25685,43651,47920,63492,59,954,1159,1246,1568,1578,1986,2402,3034,3270,4115,4352,5207,6317,9100,11025,19524,25935,44075,48385,64109,60,962,1169,1257,1581,1591,2003,2423,3061,3300,4153,4392,5255,6378,9191,11134,19718,26193,44513,48866,64746,61,971,1179,1268,1596,1606,2021,2445,3089,3330,4192,4433,5306,6440,9282,11245,19913,26452,44954,49350,65387,62,979,1190,1279,1610,1620,2039,2467,3118,3362,4232,4475,5357,6502,9375,11357,20113,26717,45403,49844,66041,63,988,1200,1291,1624,1634,2057,2489,3147,3393,4272,4519,5409,6567,9469,11472,20316,26987,45862,50347,66709,64,997,1211,1303,1639,1649,2076,2513,3177,3425,4313,4562,5462,6632,9566,11589,20523,27262,46331,50862,67390,65,1006,1222,1315,1654,1664,2095,2536,3207,3458,4355,4607,5517,6699,9666,11710,20738,27547,46815,51393,68094,66,1016,1234,1327,1669,1680,2114,2560,3238,3491,4399,4652,5572,6767,9767,11832,20954,27834,47303,51929,68804,67,1025,1245,1339,1685,1695,2134,2584,3269,3525,4442,4699,5628,6837,9870,11957,21175,28127,47801,52476,69529,68,1035,1257,1352,1701,1711,2154,2609,3301,3560,4487,4746,5686,6908,9975,12084,21400,28427,48310,53034,70269,69,1046,1270,1366,1718,1729,2176,2635,3334,3595,4532,4795,5745,6981,10082,12214,21630,28733,48830,53605,71025,70,1058,1284,1381,1738,1749,2201,2665,3370,3634,4579,4845,5805,7054,10193,12349,21869,29049,49368,54196,71807,71,1071,1300,1398,1759,1770,2228,2697,3410,3676,4630,4899,5869,7132,10307,12486,22112,29373,49917,54799,72607,72,1085,1318,1417,1783,1794,2258,2732,3454,3722,4687,4958,5940,7217,10431,12637,22380,29729,50522,55463,73486,73,1101,1337,1438,1809,1821,2291,2772,3502,3774,4750,5025,6019,7313,10570,12805,22677,30123,51192,56198,74461,74,1118,1358,1460,1837,1849,2327,2814,3556,3831,4820,5098,6106,7418,10722,12990,23004,30558,51931,57010,75536,75,1137,1381,1485,1868,1880,2366,2862,3614,3893,4896,5179,6202,7534,10891,13194,23365,31037,52746,57905,76722,76,1157,1406,1512,1902,1914,2409,2913,3678,3962,4981,5268,6309,7661,11077,13420,23766,31570,53651,58897,78037,77,1180,1433,1541,1939,1952,2456,2969,3748,4037,5073,5365,6425,7802,11281,13666,24202,32149,54636,59979,79470,78,1205,1463,1574,1980,1992,2507,3030,3824,4118,5175,5473,6552,7955,11504,13937,24682,32786,55718,61167,81044,79,1232,1496,1609,2024,2037,2563,3097,3909,4208,5286,5590,6692,8125,11749,14234,25208,33485,56905,62471,82772,80,1261,1531,1647,2072,2085,2625,3171,4000,4307,5408,5720,6846,8312,12019,14561,25786,34253,58210,63903,84670,81,1293,1571,1689,2125,2139,2692,3252,4101,4415,5542,5861,7015,8515,12317,14922,26426,35103,59655,65489,86771,82,1329,1614,1736,2184,2197,2766,3340,4213,4534,5691,6017,7202,8741,12643,15317,27126,36032,61235,67223,89069,83,1367,1661,1786,2247,2261,2846,3438,4335,4665,5853,6190,7407,8990,13003,15753,27897,37058,62977,69136,91603,84,1410,1713,1842,2318,2332,2935,3545,4470,4809,6033,6380,7634,9265,13401,16235,28750,38191,64903,71250,94404,85,1458,1771,1904,2396,2411,3034,3665,4618,4969,6232,6591,7886,9569,13841,16768,29695,39446,67036,73592,97507,86,1511,1835,1973,2482,2498,3144,3796,4784,5147,6454,6825,8165,9908,14334,17365,30752,40849,69421,76210,100975,87,1569,1906,2049,2579,2595,3266,3943,4969,5345,6701,7086,8477,10286,14880,18027,31924,42406,72067,79115,104824,88,1635,1985,2135,2686,2703,3402,4107,5175,5568,6978,7379,8827,10709,15492,18768,33237,44150,75031,82369,109136,89,1708,2075,2231,2807,2825,3556,4293,5407,5817,7290,7709,9221,11186,16181,19603,34715,46114,78368,86032,113990,90,1791,2175,2340,2944,2962,3728,4502,5671,6099,7644,8082,9666,11725,16961,20548,36388,48337,82145,90179,119484,91,1886,2290,2463,3099,3119,3925,4739,5970,6420,8045,8506,10174,12340,17854,21629,38304,50881,86470,94926,125774,92,1994,2422,2605,3278,3298,4151,5012,6313,6790,8508,8995,10758,13049,18874,22866,40494,53790,91413,100353,132964,93,2119,2574,2768,3483,3505,4411,5326,6709,7217,9041,9559,11430,13866,20056,24298,43029,57158,97137,106636,141290,94,2266,2752,2960,3724,3747,4716,5695,7173,7717,9666,10222,12221,14823,21439,25973,45995,61098,103833,113987,151030,95,2439,2962,3185,4008,4033,5076,6131,7723,8306,10409,11003,13157,15960,23076,27956,49507,65764,111761,122691,162562,96,2645,3213,3455,4347,4375,5506,6652,8381,9015,11299,11947,14282,17321,25050,30348,53743,71390,121324,133189,176471,97,2898,3519,3785,4762,4792,6031,7291,9185,9881,12387,13096,15658,18992,27454,33260,58901,78242,132967,145971,193407,98,3213,3902,4197,5280,5313,6687,8085,10186,10963,13745,14532,17377,21070,30455,36896,65339,86794,147501,161926,214547,99,3613,4388,4720,5938,5976,7521,9100,11468,12346,15489,16381,19582,23746,34302,41556,73593,97757,166133,182380,241648,100,4143,5032,5412,6809,6852,8624,10444,13168,14180,17805,18829,22511,27287,39406,47739,84543,112303,190852,209516,277603,"
		  aMaleStandardString=split(MaleStandardString,",")
		  
		  for idx = 0 to UBound(aMaleStandardString)
		    Standard=val(aMaleStandardString(idx))
		    if Standard <=100 then
		      i=Standard
		      ii=1
		    else
		      AgeStandard_Male(i,ii)=Standard
		      ii=ii+1
		    end if
		  next
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function PadString(IncomingString as string, Justifcation as String, Length as Integer) As String
		  dim NewString as String
		  dim i, IncomingStringLength, Pad, PadBefore, PadAfter as Integer
		  
		  IncomingStringLength=len(IncomingString)
		  if IncomingStringLength<Length then
		    IncomingString=" "+IncomingString+" "
		    
		    Select Case Justifcation
		    case "R"
		      NewString=""
		      for i = 1 to Length-IncomingStringLength
		        NewString=NewString+" "
		      next
		      NewString=NewString+IncomingString
		      
		      
		    case "L"
		      
		      NewString=IncomingString
		      for i = 1 to Length-IncomingStringLength
		        NewString=NewString+" "
		      next
		      
		    case "C"
		      NewString=""
		      
		      Pad=Length-IncomingStringLength
		      
		      if (Pad mod 2) = 0 then
		        PadBefore=Floor(Pad/2)
		        PadAfter=PadBefore-1
		      else
		        PadBefore=Pad/2
		        PadAfter=PadBefore
		      end if
		      
		      for i = 1 to PadBefore
		        NewString=NewString+" "
		      next
		      
		      NewString=NewString+IncomingString
		      
		      for i = 1 to PadAfter
		        NewString=NewString+" "
		      next
		      
		      
		    end Select
		    
		  else
		    NewString=" "+left(IncomingString,Length)+" "
		    
		  end if
		  
		  Return NewString
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function PictureToString(p as picture) As string
		  // Convert a picture to a string by saving the picture
		  // out to a file on disk and then reading in the binary
		  // data from that file.
		  dim f as folderItem
		  dim bs as binaryStream
		  dim data as string
		  
		  // save picture to a temporary file
		  f = getTemporaryFolderItem()
		  f.saveAsPicture p
		  
		  // read the data in from the temp file
		  bs = f.openAsBinaryFile( false )
		  data = bs.read( bs.length )
		  bs.close
		  
		  data = EncodeBase64(data)
		  
		  // remove temporary file
		  f.delete
		  
		  // return the binary data
		  return data
		  
		exception// an exception occurred preventing the conversion
		  return ""
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PopulateRaceSettings(RaceID as integer)
		  dim Result as boolean
		  dim rsRace as recordSet
		  dim recordCount as integer
		  
		  rsRace=app.theDB.DBSQLSelect("SELECT * FROM races WHERE RaceID="+format(RaceID,"0000000000"))
		  
		  ResultsDisplayType=rsRace.field("Display_Type").value
		  
		  if rsRace.field("Allow_Early_Start").value = "Y" then
		    AllowEarlyStart=true
		  else
		    AllowEarlyStart=false
		  end if
		  
		  CalculateTimesFrom=rsRace.field("Calclulate_Time_From").value
		  RaceAgeDate=rsRace.field("Race_Age_Date").value
		  RaceDate=rsRace.field("Race_Date").value
		  
		  if rsRace.field("Show_Back_Time").value="Y" then
		    ShowBackTime=true
		  else
		    ShowBackTime=False
		  end if
		  
		  if rsRace.field("Show_Net_Time").value="Y" then
		    ShowNetTime=true
		  else
		    ShowNetTime=false
		  end if
		  
		  if rsRace.field("Show_Adjustment").value="Y" then
		    ShowAdjustment=true
		  else
		    ShowAdjustment=false
		  end if
		  
		  MinimumLapTime=ConvertTimeToSeconds(rsRace.field("Minimum_Time").value+".000")
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PreferencesRead()
		  dim f as FolderItem
		  dim i, DayNumber as Integer
		  dim ImportStream as TextInputStream
		  dim IncomingLine(), PathParts(), IncomingData, Temp, WebServerParts() as string
		  dim m as OpenRecentMenuItem
		  Dim oReg As COwnerRegistration
		  
		  Dim ExpirationDate As New Date
		  Dim CurrentDate as New Date
		  
		  f=SpecialFolder.Preferences.Child("Milliseconds Pro.prefs")
		  if f.exists then
		    ImportStream=f.OpenAsTextFile
		    IncomingData=ImportStream.ReadAll
		    IncomingData=ReplaceAll(IncomingData,Chr(10),"")
		    if IncomingData<>"" then
		      IncomingLine=IncomingData.Split(chr(13))
		      ImportStream.Close
		      
		      'First three rows are registration information
		      RegUserName=IncomingLine(0)
		      RegUserCompany=IncomingLine(1)
		      RegLicenseNumber=IncomingLine(2)
		      
		      oReg = New COwnerRegistration
		      If oReg.IsKeyOK(left(RegLicenseNumber,16), RegUserCompany, RegUserName) Then
		        
		        if len(RegLicenseNumber)=21 then ' early purchaser with no expiration date
		          RegLicenseNumber=RegLicenseNumber+"QRYQSY"
		        end if
		        'Handle expiration date
		        'The string is in the following format: D2 M2 Y1 M1 D1 Y2 the # represents the order in a Standard mm/dd/yy format
		        Temp=mid(RegLicenseNumber,26,1)
		        DayNumber=Asc(mid(RegLicenseNumber,26,1))-80
		        Temp=mid(RegLicenseNumber,22,1)
		        DayNumber=Asc(mid(RegLicenseNumber,22,1))-80
		        ExpirationDate.Day=val(str(Asc(mid(RegLicenseNumber,26,1))-80)+str(Asc(mid(RegLicenseNumber,22,1))-80))
		        ExpirationDate.Month=val(str(Asc(mid(RegLicenseNumber,25,1))-80)+str(Asc(mid(RegLicenseNumber,23,1))-80))
		        ExpirationDate.Year=val("20"+str(Asc(mid(RegLicenseNumber,24,1))-80)+str(Asc(mid(RegLicenseNumber,27,1))-80))
		        
		        if ExpirationDate>CurrentDate then
		          Registered=true
		        else
		          Registered=false
		          NotRegistered.ShowModal
		        end if
		      Else
		        Registered=false
		        NotRegistered.ShowModal
		      End If
		      
		      'Handle the recent races 
		      RecentRacePaths=IncomingLine(3).Split("|")
		      
		      'for i=UBound(RecentRacePaths)-1 DownTo 0 Step 1
		      'Temp=RecentRacePaths(i)
		      'if (GetFolderItem(RecentRacePaths(i)).Exists) then
		      ''NOP
		      'Else
		      'RecentRacePaths.Remove i
		      'end if
		      'next
		      
		      for i=0 to UBound(RecentRacePaths)-1
		        m = new OpenRecentMenuItem
		        #if TargetMacOS
		          PathParts=RecentRacePaths(i).Split(":")
		        #else
		          PathParts=RecentRacePaths(i).Split("/")
		        #endif
		        m.Text = PathParts(UBound(PathParts))
		        FileOpenRecent.Append( m )
		      next
		      
		      'Handle Modules
		      
		      if mid(RegLicenseNumber,17,1)="I" and Registered then 'Can Use Internet Access Features
		        if IncomingLine(4)<>"" then
		          WebServerParts=IncomingLine(4).Split(chr(11))
		          WebServer=WebServerParts(0)
		          if UBound(WebServerParts)>=3 then
		            TimerCompany=WebServerParts(1)
		            TimerName=WebServerParts(2)
		            TimerEmail=WebServerParts(3)
		          end if
		        else
		          WebServer="www.milliseconds.com"
		          TimerCompany=""
		          TimerName=""
		          TimerEmail=""
		        end if
		        
		        if IncomingLine(5)<>"" then
		          SMSServer=split(IncomingLine(5),chr(11))
		          if (SMSServer(0)<>"networkText") and (SMSServer(0)<>"EZTexting") and (SMSServer(0)<>"Select SMS Gateway...") then
		            redim SMSServer(4)
		            SMSServer(0)=""
		            SMSServer(1)=""
		            SMSServer(2)=""
		          end if
		        else
		          redim SMSServer(4)
		        end if
		        
		        if IncomingLine(6)<>"" then
		          TwitterParts=split(IncomingLine(6),chr(11))
		        else
		          redim TwitterParts(4)
		        end if
		        
		        if mid(RegLicenseNumber,18,1)="N" or not(Registered) then 'Can Use Triathlon Features
		          TriathlonAvailable=true
		        else
		          TriathlonAvailable=false
		        end if
		        
		        if mid(RegLicenseNumber,19,1)="H" or not(Registered) then 'Can Use Circuit Race Features
		          CircuitRaceAvailable=true
		        else
		          CircuitRaceAvailable=false
		        end if
		        
		        if mid(RegLicenseNumber,20,1)="B" or not(Registered) then 'Can Use Criterium Features
		          CriteriumAvailable=true
		        else
		          CriteriumAvailable=false
		        end if
		        
		        if mid(RegLicenseNumber,21,1)="O" or not(Registered) then 'Can Use Time Trial Features
		          TimeTrialAvailable=true
		        else
		          TimeTrialAvailable=false
		        end if
		        
		      end if
		    else
		      Registered=false
		      TriathlonAvailable=true
		      CircuitRaceAvailable=true
		      CriteriumAvailable=true
		      TimeTrialAvailable=true
		      NotRegistered.ShowModal
		    end if
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PreferencesWrite()
		  dim Path as string
		  dim i, limit as integer 
		  dim f as FolderItem
		  dim InputStream as TextInputStream
		  dim OutputStream as TextOutputStream
		  dim IncomingLine(), OutgoingLine(), PathParts(), IncomingData as string
		  redim OutgoingLine(6)
		  OutgoingLine(0)=RegUserName
		  OutgoingLine(1)=RegUserCompany
		  OutgoingLine(2)=RegLicenseNumber
		  OutgoingLine(4)=WebServer+chr(11)+TimerCompany+chr(11)+TimerName+chr(11)+TimerEmail
		  OutgoingLine(5)=Join(SMSServer,chr(11))
		  OutgoingLine(6)=TwitterParts(0)+chr(11)+TwitterParts(1)+chr(11)+str(TwitterParts(2))+chr(11)+TwitterParts(3)
		  
		  if theDB.DatabaseType="REALSQLDatabase" then
		    if theDB.RealSQLDB <>nil then
		      if theDB.RealSQLDB.DatabaseFile<>nil then
		        Path=theDB.RealSQLDB.DatabaseFile.AbsolutePath
		        for i= 0 to UBound(app.RecentRacePaths)-1
		          if instr(app.RecentRacePaths(i),Path) > 0 then
		            app.RecentRacePaths.Remove i
		            exit
		          end if
		        next
		        
		        app.RecentRacePaths.Insert 0, Path
		        
		        if UBound(app.RecentRacePaths) > 5 then
		          limit = 4
		        else
		          limit = UBound(app.RecentRacePaths)-1
		          if limit<0 then
		            limit=0
		          end if
		        end if
		        
		        for i=0 to limit
		          if i = 0 then
		            if UBound(OutgoingLine)<=0 then
		              OutgoingLine.append app.RecentRacePaths(i)+"|"
		            else
		              OutgoingLine(3)=app.RecentRacePaths(i)+"|"
		            end if
		          else
		            OutgoingLine(3)=OutgoingLine(3)+app.RecentRacePaths(i)+"|"
		          end if
		        next
		      end if
		    end if
		  end if
		  
		  f=SpecialFolder.Preferences.Child("Milliseconds Pro.prefs")
		  OutputStream=f.CreateTextFile
		  for i=0 to UBound(OutgoingLine)
		    OutputStream.WriteLine(OutgoingLine(i))
		  next
		  
		  OutputStream.Close
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub PrintCheckInReceipt(RacerNumber as string)
		  dim CheckInReceipt as Graphics
		  Dim PageSetup as PrinterSetup
		  dim i, LinePixelCount as Integer
		  dim rs, rsTimes, rsIntervals, rsTX as RecordSet
		  dim SQL, Footer, IntervalName as string
		  
		  LinePixelCount=0
		  
		  
		  if FirstFeedBackResult then
		    CheckInReceipt = OpenPrinterDialog()
		    PageSetup= New PrinterSetup
		    If PageSetup.PageSetupDialog Then
		      
		    End If
		    FirstFeedBackResult=false
		  else
		    CheckInReceipt = OpenPrinter()
		  end if
		  
		  if CheckInReceipt<>nil then
		    
		    
		    CheckInReceipt.DrawPicture NewMillisecondsBW, 10, LinePixelCount
		    LinePixelCount=88
		    
		    CheckInReceipt.Bold=true
		    
		    'Race Name
		    CheckInReceipt.TextSize=18
		    LinePixelCount=LinePixelCount+10
		    CheckInReceipt.DrawString wnd_List.RaceName.Text, 10, LinePixelCount, 125
		    CheckInReceipt.TextSize=12
		    
		    LinePixelCount=LinePixelCount+70
		    CheckInReceipt.DrawString "Preliminary Results", 10, LinePixelCount
		    
		    CheckInReceipt.TextSize=10
		    'Racer name
		    SQL="SELECT participants.rowid, DivisionID, Racer_Number, First_Name, Participant_Name, Representing, Total_Time, Division_Name "
		    SQL=SQL+"FROM participants INNER JOIN divisions ON participants.DivisionID = divisions.rowid "
		    SQL=SQL+"WHERE Racer_Number ='"+RacerNumber+"'"
		    rs=app.theDB.DBSQLSelect(SQL)
		    
		    LinePixelCount=LinePixelCount+40
		    CheckInReceipt.DrawString rs.Field("Racer_Number").StringValue+" "+rs.Field("First_Name").StringValue+" "+ rs.Field("Participant_Name").StringValue, 10, LinePixelCount
		    
		    CheckInReceipt.Bold=False
		    
		    'Representing
		    LinePixelCount=LinePixelCount+15
		    CheckInReceipt.DrawString rs.Field("Representing").StringValue, 10, LinePixelCount
		    
		    'Division Name
		    LinePixelCount=LinePixelCount+30
		    CheckInReceipt.DrawString rs.Field("Division_Name").StringValue, 10, LinePixelCount
		    
		    'Total Time
		    CheckInReceipt.Bold=true
		    LinePixelCount=LinePixelCount+30
		    CheckInReceipt.DrawString "Total Time: "+rs.Field("Total_Time").StringValue, 10, LinePixelCount
		    CheckInReceipt.Bold=false
		    
		    'Division Place and Division Back
		    LinePixelCount=LinePixelCount+30
		    CheckInReceipt.DrawString "Division Place: "+rs.Field("Division_Place").StringValue, 10, LinePixelCount
		    LinePixelCount=LinePixelCount+15
		    CheckInReceipt.DrawString rs.Field("Division_Back").StringValue+" behind 1st", 10, LinePixelCount
		    
		    'Gender Place and Gender Back
		    LinePixelCount=LinePixelCount+20
		    CheckInReceipt.DrawString "Gender Place: "+rs.Field("Gender_Place").StringValue, 10, LinePixelCount
		    LinePixelCount=LinePixelCount+15
		    CheckInReceipt.DrawString rs.Field("Gender_Back").StringValue+" behind 1st", 10, LinePixelCount
		    
		    'Overall Place and Overall Back
		    LinePixelCount=LinePixelCount+20
		    CheckInReceipt.DrawString "Overall Place: "+rs.Field("Overall_Place").StringValue, 10, LinePixelCount
		    LinePixelCount=LinePixelCount+15
		    CheckInReceipt.DrawString rs.Field("Overall_Back").StringValue+" behind 1st", 10, LinePixelCount
		    
		    'Interval Name and Time
		    LinePixelCount=LinePixelCount+12
		    rsTimes=app.theDB.DBSQLSelect("SELECT Interval_Number, Interval_Time FROM Times WHERE ParticipantID ='"+rs.Field("rowid").StringValue+"' AND Use_This_Passing='Y' AND Interval_Number>0 and Interval_Number< 9900 ORDER BY Interval_Number")
		    for i=1 to rsTimes.RecordCount
		      rsIntervals=app.theDB.DBSQLSelect("SELECT Interval_Name FROM Intervals WHERE DivisionID="+rs.Field("DivisionID").StringValue+" AND Number="+rsTimes.Field("Interval_Number").StringValue)
		      LinePixelCount=LinePixelCount+15
		      if rsIntervals.Field("Interval_Name").StringValue <> "" then
		        IntervalName=rsIntervals.Field("Interval_Name").StringValue
		      else
		        IntervalName=Str(i)
		      end if
		      CheckInReceipt.DrawString IntervalName+": "+app.StripLeadingZeros(rsTimes.Field("Interval_Time").StringValue), 10, LinePixelCount
		      rsTimes.MoveNext
		      rsIntervals.Close
		    next
		    
		    CheckInReceipt.TextSize=6
		    'Returned Transponders
		    rsTX=app.theDB.DBSQLSelect("SELECT TX_Code, Returned FROM Transponders WHERE Returned <>'0000-00-00 00:00:00' AND Racer_Number="+RacerNumber)
		    if rsTX.RecordCount>0 then
		      LinePixelCount=LinePixelCount+20
		      CheckInReceipt.DrawString "Transponder(s) Returned:", 10, LinePixelCount, 125
		      for i=1 to rsTX.RecordCount
		        LinePixelCount=LinePixelCount+10
		        CheckInReceipt.DrawString rsTX.Field("TX_Code").StringValue+": "+ rsTX.Field("Returned").StringValue, 30, LinePixelCount, 125
		        rsTimes.MoveNext
		      next
		    end if
		    
		    CheckInReceipt.Bold=true
		    CheckInReceipt.TextSize=8
		    LinePixelCount=LinePixelCount+40
		    CheckInReceipt.DrawString "Complete results at www.milliseconds.com", 20, LinePixelCount, 125
		    CheckInReceipt.Bold=false
		    
		    CheckInReceipt.TextSize=6
		    LinePixelCount=LinePixelCount+20
		    Footer="Results prepared by: "+app.RegUserCompany+"."
		    Footer=Footer+" These results are preliminary and are subject to change. Copyright 2007, Milliseconds Computer Services, LLC."
		    CheckInReceipt.DrawString Footer, 10, LinePixelCount, 125
		    rs.Close
		    rsTimes.Close
		  end if
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SQLDateToDate(SQLDate as String) As Date
		  Dim TheDate as Date
		  Dim Success as boolean
		  
		  Success = ParseDate(right(SQLDate,5)+"-"+Left(SQLDate,4),TheDate)
		  Return TheDate
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function StringToPicture(data as string) As picture
		  // Convert a string to a picture by saving the binary data
		  // to a file on disk and then reading the file as a picture.
		  dim f as folderItem
		  dim bs as binaryStream
		  dim p as picture
		  
		  data=DecodeBase64(data)
		  
		  // write binary data to temporary file
		  f = getTemporaryFolderItem()
		  bs = f.createBinaryFile( "any" )
		  bs.write data
		  bs.close
		  
		  // open the file as a picture
		  p = f.openAsPicture()
		  
		  // remove temporary file
		  f.delete
		  
		  // return the binary data
		  return p
		  
		exception// an exception occurred preventing the conversion
		  return NIL
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function StripLeadingZeros(IncomingTime as string) As string
		  dim i as integer
		  dim CleanString as string
		  
		  if left(IncomingTime,1)="+" then
		    CleanString="+"
		    IncomingTime=Replace(IncomingTime,"+","")
		  end if
		  
		  if left(IncomingTime,5)="00:00" then
		    CleanString=Replace(IncomingTime,"00:00:","0:")
		  else
		    i=1
		    while (((mid(IncomingTime,i,1)="0") or (mid(IncomingTime,i,1)=":")) and (i<8))
		      i=i+1
		    wend
		    CleanString=CleanString+mid(IncomingTime,i)
		  end if
		  return CleanString
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function TruncateTime(TimeToTruncate as String) As String
		  if PlacesToTruncate=0 then
		    TimeToTruncate=Left(TimeToTruncate,len(TimeToTruncate)-4)
		  else
		    TimeToTruncate=Left(TimeToTruncate,len(TimeToTruncate)-(3-PlacesToTruncate))
		  end if
		  
		  return TimeToTruncate
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		AdditionalMessage As String
	#tag EndProperty

	#tag Property, Flags = &h0
		AgeGrade_Distances(20) As String
	#tag EndProperty

	#tag Property, Flags = &h0
		AgeStandard_Female(100,20) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		AgeStandard_Male(100,20) As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		AllowEarlyStart As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		AMBOffset As double
	#tag EndProperty

	#tag Property, Flags = &h0
		CalculateTimesFrom As string
	#tag EndProperty

	#tag Property, Flags = &h0
		CircuitRaceAvailable As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		CRC16Table(255) As MemoryBlock
	#tag EndProperty

	#tag Property, Flags = &h0
		CriteriumAvailable As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		CrossCountry As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		CrossCountryMinScorers As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		CrossCountryRunAvailable As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		DataRecord() As string
	#tag EndProperty

	#tag Property, Flags = &h0
		DisplayLoopupList As boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private FirstFeedBackResult As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		IAmServer As Boolean = false
	#tag EndProperty

	#tag Property, Flags = &h0
		IPAddressFirstThree As String
	#tag EndProperty

	#tag Property, Flags = &h0
		JamSession As String
	#tag EndProperty

	#tag Property, Flags = &h0
		LiveTimingUpdateThread As LiveTimingUpdate
	#tag EndProperty

	#tag Property, Flags = &h0
		MinimumLapTime As integer
	#tag EndProperty

	#tag Property, Flags = &h0
		NGB1 As string
	#tag EndProperty

	#tag Property, Flags = &h0
		NGB2 As string
	#tag EndProperty

	#tag Property, Flags = &h0
		nthTime As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		OKToProcessArrays As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		PlacesToTruncate As integer
	#tag EndProperty

	#tag Property, Flags = &h0
		PlacesToTweet As Integer = 10
	#tag EndProperty

	#tag Property, Flags = &h0
		PrinterSettings As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ProcessTimingDataThread As ProcessTimingData
	#tag EndProperty

	#tag Property, Flags = &h0
		Shared PushIntervalAndFinalTimes As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		RaceAgeDate As Date
	#tag EndProperty

	#tag Property, Flags = &h0
		RaceDate As date
	#tag EndProperty

	#tag Property, Flags = &h0
		RaceID As Integer = 1
	#tag EndProperty

	#tag Property, Flags = &h0
		RacersPerStart As integer
	#tag EndProperty

	#tag Property, Flags = &h0
		RaceStartTime As String
	#tag EndProperty

	#tag Property, Flags = &h0
		RaceStartTimeSec As double
	#tag EndProperty

	#tag Property, Flags = &h0
		ReaderType As String = "iPico"
	#tag EndProperty

	#tag Property, Flags = &h0
		RecentRacePaths() As string
	#tag EndProperty

	#tag Property, Flags = &h0
		Registered As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		RegLicenseNumber As String
	#tag EndProperty

	#tag Property, Flags = &h0
		RegUserCompany As String
	#tag EndProperty

	#tag Property, Flags = &h0
		RegUserName As String
	#tag EndProperty

	#tag Property, Flags = &h0
		ResultsDisplayType As string
	#tag EndProperty

	#tag Property, Flags = &h0
		SecondsInYear As double
	#tag EndProperty

	#tag Property, Flags = &h0
		SendSMS As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		SendTweet As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		ServerAutoDiscovery As AutoDiscovery
	#tag EndProperty

	#tag Property, Flags = &h0
		ShowAdjustment As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		ShowBackTime As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		ShowNetTime As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		SMSServer() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		StartingRacerNumber As integer
	#tag EndProperty

	#tag Property, Flags = &h0
		StartIntervalSec As double
	#tag EndProperty

	#tag Property, Flags = &h0
		StreamResults As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		theDB As DBSupport
	#tag EndProperty

	#tag Property, Flags = &h0
		TimerCompany As String
	#tag EndProperty

	#tag Property, Flags = &h0
		TimerEmail As String
	#tag EndProperty

	#tag Property, Flags = &h0
		timerID As String = "1"
	#tag EndProperty

	#tag Property, Flags = &h0
		TimerName As String
	#tag EndProperty

	#tag Property, Flags = &h0
		TimeSource() As integer
	#tag EndProperty

	#tag Property, Flags = &h0
		TimeTime() As string
	#tag EndProperty

	#tag Property, Flags = &h0
		TimeTimingPointID() As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		TimeTrialAvailable As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		TimeTXCode() As string
	#tag EndProperty

	#tag Property, Flags = &h0
		TimeType() As string
	#tag EndProperty

	#tag Property, Flags = &h0
		TriathlonAvailable As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		TweetMST As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		TweetOverall As Boolean = True
	#tag EndProperty

	#tag Property, Flags = &h0
		TweetRace As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		TwitterParts() As String
	#tag EndProperty

	#tag Property, Flags = &h0
		UTCOffset As integer
	#tag EndProperty

	#tag Property, Flags = &h0
		WebServer As String
	#tag EndProperty


	#tag Constant, Name = AgeGradeDistances, Type = String, Dynamic = False, Default = \"Select Age Grade Distance...\x2Cnone\x2C5 km\x2C6 km\x2C4 Mile\x2C8 km\x2C5 Mile\x2C10 km\x2C12 km\x2C15 km\x2C10 Mile\x2C20 km\x2CHalf Mar\x2C25 km\x2C30 km\x2CMarathon\x2C50 km\x2C50 Mile\x2C100 km\x2C150 km\x2C100 Mile\x2C200 km", Scope = Public
	#tag EndConstant

	#tag Constant, Name = DontGetRecordCount, Type = Boolean, Dynamic = False, Default = \"false", Scope = Public
	#tag EndConstant

	#tag Constant, Name = GetRecordCount, Type = Boolean, Dynamic = False, Default = \"true", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PaceTypes, Type = String, Dynamic = False, Default = \"Select Pace Type...\x2Cnone\x2Cmin/mi\x2CMPH\x2Cmin/100yds\x2Cmin/km\x2CKPH\x2Cmin/100m", Scope = Public
	#tag EndConstant

	#tag Constant, Name = RaceID, Type = Integer, Dynamic = False, Default = \"1", Scope = Public
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="AdditionalMessage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AllowEarlyStart"
			Group="Behavior"
			InitialValue="0"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="AMBOffset"
			Group="Behavior"
			InitialValue="0"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CalculateTimesFrom"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CircuitRaceAvailable"
			Group="Behavior"
			InitialValue="0"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CriteriumAvailable"
			Group="Behavior"
			InitialValue="0"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CrossCountry"
			Group="Behavior"
			InitialValue="false"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CrossCountryMinScorers"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="CrossCountryRunAvailable"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="DisplayLoopupList"
			Group="Behavior"
			InitialValue="0"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IAmServer"
			Group="Behavior"
			InitialValue="false"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IPAddressFirstThree"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="JamSession"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="MinimumLapTime"
			Group="Behavior"
			InitialValue="0"
			Type="integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="NGB1"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="NGB2"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="nthTime"
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="OKToProcessArrays"
			Group="Behavior"
			InitialValue="0"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PlacesToTruncate"
			Group="Behavior"
			InitialValue="0"
			Type="integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PlacesToTweet"
			Group="Behavior"
			InitialValue="10"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PrinterSettings"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RaceID"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RacersPerStart"
			Group="Behavior"
			InitialValue="0"
			Type="integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RaceStartTime"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RaceStartTimeSec"
			Group="Behavior"
			InitialValue="0"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ReaderType"
			Group="Behavior"
			InitialValue="iPico"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Registered"
			Group="Behavior"
			InitialValue="0"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RegLicenseNumber"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RegUserCompany"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RegUserName"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ResultsDisplayType"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SecondsInYear"
			Group="Behavior"
			InitialValue="0"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SendSMS"
			Group="Behavior"
			InitialValue="0"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="SendTweet"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowAdjustment"
			Group="Behavior"
			InitialValue="0"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowBackTime"
			Group="Behavior"
			InitialValue="0"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ShowNetTime"
			Group="Behavior"
			InitialValue="0"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StartingRacerNumber"
			Group="Behavior"
			InitialValue="0"
			Type="integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StartIntervalSec"
			Group="Behavior"
			InitialValue="0"
			Type="double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StreamResults"
			Group="Behavior"
			InitialValue="0"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TimerCompany"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TimerEmail"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timerID"
			Group="Behavior"
			InitialValue="1"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TimerName"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TimeTrialAvailable"
			Group="Behavior"
			InitialValue="0"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TriathlonAvailable"
			Group="Behavior"
			InitialValue="0"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TweetMST"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TweetOverall"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TweetRace"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UTCOffset"
			Group="Behavior"
			InitialValue="0"
			Type="integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="WebServer"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
