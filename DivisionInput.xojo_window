#tag Window
Begin Window DivisionInput
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   8
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   375
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Divisions"
   Visible         =   True
   Width           =   595
   Begin TextField DivisionName
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   "Enter the name of the race"
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   96
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   22
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   453
   End
   Begin TextField DivisionLowAge
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   21
      HelpTag         =   "Enter the date the race is scheduled for."
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   170
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "###"
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   51
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   55
   End
   Begin TextField DivisionHighAge
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   "Enter the scheduled start time for the race."
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   413
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "###"
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   51
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   55
   End
   Begin DateTimeEditField DivisionStartTime
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   170
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   78
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   166
   End
   Begin TextField DivisionListOrder
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   "Enter the scheduled start time for the race."
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   413
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "####"
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   78
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   55
   End
   Begin PopupMenu DivisionGender
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   "Select the gender for the division"
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   166
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   107
      Underline       =   False
      Visible         =   True
      Width           =   100
   End
   Begin PopupMenu divisionDistance
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   "Select the format of the results."
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   413
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   108
      Underline       =   False
      Visible         =   True
      Width           =   132
   End
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Name:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   23
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   68
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   91
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Low Age:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   51
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin Label StaticText3
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   90
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Gender:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   107
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin Label StaticText4
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   346
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "High Age:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   52
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   60
   End
   Begin GroupBox GroupBox3
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Intervals:"
      Enabled         =   True
      Height          =   134
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   42
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   11
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   179
      Underline       =   False
      Visible         =   True
      Width           =   439
      Begin ListBox IntervalList
         AutoDeactivate  =   True
         AutoHideScrollbars=   True
         Bold            =   False
         Border          =   True
         ColumnCount     =   2
         ColumnsResizable=   False
         ColumnWidths    =   ""
         DataField       =   ""
         DataSource      =   ""
         DefaultRowHeight=   -1
         Enabled         =   True
         EnableDrag      =   False
         EnableDragReorder=   False
         GridLinesHorizontal=   0
         GridLinesVertical=   0
         HasHeading      =   True
         HeadingIndex    =   -1
         Height          =   99
         HelpTag         =   ""
         Hierarchical    =   False
         Index           =   -2147483648
         InitialParent   =   "GroupBox3"
         InitialValue    =   ""
         Italic          =   False
         Left            =   48
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         RequiresSelection=   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollBarVertical=   True
         SelectionType   =   0
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   204
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   428
         _ScrollOffset   =   0
         _ScrollWidth    =   -1
      End
   End
   Begin PushButton OK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "OK"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   480
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   12
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   330
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PushButton Cancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   394
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   13
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   330
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin BevelButton NewRaceDistance
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "New"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   493
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   14
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   204
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   60
   End
   Begin BevelButton DeleteRaceDistance
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Delete"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   493
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   15
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   245
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   60
   End
   Begin Label StaticText6
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   337
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   16
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Distance:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   108
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   68
   End
   Begin Label StaticText5
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   94
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   17
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Start Time:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   81
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   66
   End
   Begin Label StaticText7
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   362
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   18
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Order:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   79
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   43
   End
   Begin TextField DivisionImportName
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   "Enter the name of the category for the division. Used along with age and gender to uniquely identify the age division during participant import."
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   96
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   19
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   139
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   453
   End
   Begin Label StaticText8
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   7
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   20
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Category:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   140
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   81
   End
   Begin CheckBox cbHasTeamMembers
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Has Team Members"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   30
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   478
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      State           =   0
      TabIndex        =   21
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   62
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   100
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  dim rsRaceDistances as recordSet
		  dim recsRS as recordSet
		  dim recCount as integer
		  dim i as integer
		  dim SelectStatement as string
		  dim res as boolean
		  
		  DivisionIDStg=Format(wnd_List.DivisionID,"##########")
		  
		  DivisionGender.addRow "Select Gender..."
		  DivisionGender.addRow "Male"
		  DivisionGender.addRow "Female"
		  DivisionGender.addRow "Mixed"
		  DivisionGender.listindex = 0
		  
		  //Load the DivisionDistance popupmenu
		  divisionDistance.addRow "Select Distance..."
		  rsRaceDistances=app.theDB.DBSQLSelect("Select RaceDistance_Name,rowid FROM racedistances WHERE RaceID=1")
		  While not rsRaceDistances.EOF
		    divisionDistance.addRow rsRaceDistances.Field("RaceDistance_Name").stringValue
		    divisionDistance.rowTag(divisionDistance.ListCount-1)=rsRaceDistances.Field("rowid").integerValue
		    rsRaceDistances.moveNext
		  wend
		  divisionDistance.ListIndex=0
		  rsRaceDistances.Close
		  
		  recsRS=app.theDB.DBSQLSelect("Select * FROM divisions WHERE RaceID=1 AND rowid="+DivisionIDStg)
		  if not recsRS.EOF then  'not a new record
		    NewDivision=false
		    DivisionName.text=recsRS.Field("Division_Name").stringValue
		    DivisionLowAge.text=recsRS.Field("Low_Age").stringValue
		    DivisionHighAge.text=recsRS.Field("High_Age").stringValue
		    DivisionStartTime.text=recsRS.Field("Start_Time").stringValue
		    DivisionListOrder.text=recsRS.Field("List_Order").stringValue
		    DivisionImportName.text=recsRS.Field("Import_Name").stringValue
		    
		    Select case recsRS.Field("Gender").stringValue
		    case "M"
		      DivisionGender.listindex=1
		    case "F"
		      DivisionGender.listindex=2
		    case "X"
		      DivisionGender.listindex=3
		    else
		      DivisionGender.listindex=0
		    end select
		    
		    for i=0 to divisionDistance.ListCount-1
		      divisionDistance.ListIndex=i
		      if divisionDistance.rowTag(i) = recsRS.Field("RaceDistanceID").integerValue then
		        exit
		      end if
		    next
		    if i=divisionDistance.ListCount then
		      divisionDistance.ListIndex=0
		    end if
		    
		    if recsRS.Field("Has_Team_Members").StringValue="Y" then
		      cbHasTeamMembers.Value=true
		    else
		      cbHasTeamMembers.Value=false
		    end if
		    
		  else
		    NewDivision=true
		    recsRS=app.theDB.DBSQLSelect("Select rowid FROM divisions WHERE RaceID=1 ORDER BY rowid DESC")
		    DivisionIDStg=Format(recsRS.Field("rowid").IntegerValue+1,"##########")
		    DivisionStartTime.text=app.RaceStartTime
		  end if
		  
		  IntervalList.columncount=5
		  IntervalList.columnwidths="0,15%,35%,25%,25%"
		  
		  IntervalList.heading(1)="Number"
		  IntervalList.heading(2)="Name"
		  IntervalList.heading(3)="Actual Distance"
		  IntervalList.heading(4)="Pace Type"
		  IntervalList.ColumnAlignment(1)=ListBox.AlignCenter
		  IntervalList.ColumnAlignment(2)=ListBox.AlignLeft
		  IntervalList.ColumnAlignment(3)=ListBox.AlignCenter
		  IntervalList.ColumnAlignment(4)=ListBox.AlignCenter
		  
		  LoadIntervals
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub LoadIntervals()
		  Dim recsRS as RecordSet
		  
		  IntervalList.DeleteAllRows
		  
		  recsRS=App.theDB.DBSQLSelect("SELECT Number,Interval_Name,Actual_Distance,Pace_Type,rowid FROM intervals WHERE RaceID = 1 AND DivisionID = "+DivisionIDStg+" ORDER BY Number")
		  while not recsRS.EOF
		    IntervalList.AddRow ""
		    IntervalList.Cell(IntervalList.lastindex,0)=recsRS.Field("rowid").stringValue
		    IntervalList.Cell(IntervalList.lastindex,1)=recsRS.Field("Number").stringValue
		    IntervalList.Cell(IntervalList.lastindex,2)=recsRS.Field("Interval_Name").stringValue
		    IntervalList.Cell(IntervalList.lastindex,3)=str(recsRS.Field("Actual_Distance").DoubleValue)
		    IntervalList.Cell(IntervalList.lastindex,4)=recsRS.Field("Pace_Type").stringValue
		    recsRS.moveNext
		  wend
		  recsRS.close
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		DivisionIDStg As string
	#tag EndProperty

	#tag Property, Flags = &h0
		DivisionIntervalRow As integer
	#tag EndProperty

	#tag Property, Flags = &h0
		IntervalID As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected NewDivision As boolean
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected RaceDistanceIDStg As string
	#tag EndProperty


#tag EndWindowCode

#tag Events divisionDistance
	#tag Event
		Sub Change()
		  RaceDistanceIDStg=str(divisionDistance.RowTag(divisionDistance.ListIndex))
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IntervalList
	#tag Event
		Sub DoubleClick()
		  DivisionIntervalRow=IntervalList.ListIndex
		  IntervalID=val(IntervalList.Cell(DivisionIntervalRow,0))
		  DivisionIntervalInput.newInterval=False
		  DivisionIntervalInput.show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OK
	#tag Event
		Sub Action()
		  dim SQLStatement as string
		  dim res as boolean
		  dim DataEntryError as boolean
		  dim SelectedRow, RowCount as integer
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if (DivisionName.text="") then
		    app.DisplayDataEntryError("An division name is required","Please enter a division name",DivisionName)
		    DataEntryError=true
		    
		  elseif ((val(DivisionLowAge.text)<1) or (val(DivisionLowAge.text)>300)) then
		    app.DisplayDataEntryError("Invalid low age","Please enter a low ge between 1 and 99",DivisionLowAge)
		    DataEntryError=true
		    
		  elseif ((val(DivisionHighAge.text)<1) or (val(DivisionHighAge.text)>300)) then
		    app.DisplayDataEntryError("Invalid high age","Please enter a high age between 1 and 299",DivisionHighAge)
		    DataEntryError=true
		    
		  elseif (val(DivisionLowAge.text)>val(DivisionHighAge.text)) then
		    app.DisplayDataEntryError("Invalid high age","Please enter a high age greater than or equal to the low age",DivisionHighAge)
		    DataEntryError=true
		    
		  elseif DivisionGender.ListIndex=0 then
		    d.icon=2   //display stop icon
		    DivisionGender.setFocus
		    d.Message="Gender is required"
		    d.Explanation="Please select a gender"
		    b=d.ShowModal
		    DataEntryError=true
		    
		  elseif divisionDistance.ListIndex=0 then
		    d.icon=2   //display stop icon
		    divisionDistance.setFocus
		    d.Message="Distance is required"
		    d.Explanation="Please select a distance"
		    b=d.ShowModal
		    DataEntryError=true
		    
		  else
		    DataEntryError=false
		  end if
		  
		  if not(DataEntryError) then
		    
		    
		    if NewDivision then
		      'Build the SQL
		      SQLStatement="INSERT INTO divisions (RaceID, Division_Name,RaceDistanceID,Import_Name,Low_Age,High_Age,Gender,Start_Time,Actual_Start_Time,List_Order,Has_Team_Members,rowid) "
		      SQLStatement=SQLStatement+" VALUES (1,"
		      SQLStatement=SQLStatement+"'"+DivisionName.text+"',"
		      SQLStatement=SQLStatement+RaceDistanceIDStg+"," 
		      SQLStatement=SQLStatement+"'"+DivisionImportName.text+"',"
		      SQLStatement=SQLStatement+DivisionLowAge.text+","
		      SQLStatement=SQLStatement+DivisionHighAge.text+","
		      select case DivisionGender.text
		      case "Male"
		        SQLStatement=SQLStatement+"'M',"
		      case "Female"
		        SQLStatement=SQLStatement+"'F',"
		      case "Mixed"
		        SQLStatement=SQLStatement+"'X',"
		      end select
		      SQLStatement=SQLStatement+"'"+DivisionStartTime.text+"'," 'assigned start time
		      SQLStatement=SQLStatement+"'"+DivisionStartTime.text+"'," 'actual start time
		      SQLStatement=SQLStatement+DivisionListOrder.text+"," ' List order
		      if cbHasTeamMembers.Value then
		        SQLStatement = SQLStatement + "'Y',"
		      else
		        SQLStatement = SQLStatement + "'N',"
		      end if
		      SQLStatement=SQLStatement+DivisionIDStg+")"
		      app.theDB.DBSQLExecute(SQLStatement)
		      
		      wnd_List.DataList_Divisions.LockDrawing=true
		      wnd_List.DataList_Divisions.AppendRow(DivisionName.Text)
		      RowCount=wnd_List.DataList_Divisions.Rows
		      wnd_List.DataList_Divisions.CellText(2,RowCount)=DivisionLowAge.Text
		      wnd_List.DataList_Divisions.CellText(3,RowCount)=DivisionHighAge.Text
		      select case DivisionGender.text
		      case "Male"
		        wnd_List.DataList_Divisions.CellText(4,RowCount)="M"
		      case "Female"
		        wnd_List.DataList_Divisions.CellText(4,RowCount)="F"
		      case "Mixed"
		        wnd_List.DataList_Divisions.CellText(4,RowCount)="X"
		      end select
		      wnd_List.DataList_Divisions.CellText(5,RowCount)=DivisionDistance.Text
		      wnd_List.DataList_Divisions.CellText(6,RowCount)=DivisionImportName.Text
		      wnd_List.DataList_Divisions.CellText(7,RowCount)=DivisionStartTime.Text
		      wnd_List.DataList_Divisions.CellText(8,RowCount)=DivisionListOrder.Text
		      
		      wnd_List.DataList_Divisions.Row(RowCount).ItemData=str(app.theDB.DBLastRowID)
		      wnd_List.DataList_Divisions.LockDrawing=false
		      
		    else
		      'Build the SQL
		      SQLStatement = "UPDATE divisions SET Division_Name='"+DivisionName.text+"'"
		      SQLStatement = SQLStatement + ", RaceDistanceID="+RaceDistanceIDStg
		      SQLStatement = SQLStatement + ", Import_Name='"+DivisionImportName.text+"'"
		      SQLStatement = SQLStatement + ", Low_Age="+DivisionLowAge.text
		      SQLStatement = SQLStatement + ", High_Age="+DivisionHighAge.text
		      select case DivisionGender.text
		      case "Male"
		        SQLStatement = SQLStatement + ", Gender='M'"
		      case "Female"
		        SQLStatement = SQLStatement + ", Gender='F'"
		      case "Mixed"
		        SQLStatement = SQLStatement + ", Gender='X'"
		      end select
		      SQLStatement = SQLStatement + ", Start_Time='"+DivisionStartTime.text+"'"
		      SQLStatement = SQLStatement + ", Actual_Start_Time='"+DivisionStartTime.text+"'"
		      SQLStatement = SQLStatement + ", List_Order="+DivisionListOrder.text
		      if cbHasTeamMembers.Value then
		        SQLStatement = SQLStatement + ", Has_Team_Members='Y'"
		      else
		        SQLStatement = SQLStatement + ", Has_Team_Members='N'"
		      end if
		      SQLStatement = SQLStatement + " WHERE RaceID=1 AND rowid="+DivisionIDStg
		      
		      app.theDB.DBSQLExecute(SQLStatement)
		      app.theDB.DBCommit
		      
		      'update the display
		      SelectedRow=wnd_List.DataList_Divisions.VScrollValue
		      
		      wnd_List.DataList_Divisions.CellText(1,wnd_List.RowSelected)=DivisionName.Text
		      wnd_List.DataList_Divisions.CellText(2,wnd_List.RowSelected)=DivisionLowAge.Text
		      wnd_List.DataList_Divisions.CellText(3,wnd_List.RowSelected)=DivisionHighAge.Text
		      select case DivisionGender.text
		      case "Male"
		        wnd_List.DataList_Divisions.CellText(4,wnd_List.RowSelected)="M"
		      case "Female"
		        wnd_List.DataList_Divisions.CellText(4,wnd_List.RowSelected)="F"
		      case "Mixed"
		        wnd_List.DataList_Divisions.CellText(4,wnd_List.RowSelected)="X"
		      end select
		      wnd_List.DataList_Divisions.CellText(5,wnd_List.RowSelected)=DivisionDistance.Text
		      wnd_List.DataList_Divisions.CellText(6,wnd_List.RowSelected)=DivisionImportName.Text
		      wnd_List.DataList_Divisions.CellText(7,wnd_List.RowSelected)=DivisionStartTime.Text
		      wnd_List.DataList_Divisions.CellText(8,wnd_List.RowSelected)=DivisionListOrder.Text
		      
		      wnd_List.DataList_Divisions.Selection.Clear
		      wnd_List.DataList_Divisions.VScrollValue=SelectedRow
		    end if
		    
		    
		    DivisionInput.close
		    
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Cancel
	#tag Event
		Sub Action()
		  DivisionInput.close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events NewRaceDistance
	#tag Event
		Sub Action()
		  IntervalID=0
		  DivisionIntervalInput.show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DeleteRaceDistance
	#tag Event
		Sub Action()
		  Dim SQLStatement as string
		  dim res as boolean
		  Dim Result as integer
		  dim row as integer
		  
		  Result=MsgBox("Are you sure you want to delete this Interval?",1+48)
		  if Result = 1 then
		    row=IntervalList.ListIndex
		    SQLStatement="DELETE FROM intervals WHERE RaceID=1 AND DivisionID="+DivisionInput.DivisionIDStg+" AND rowid="+IntervalList.Cell(row,0)
		    App.theDB.DBSQLExecute(SQLStatement)
		    IntervalList.RemoveRow row
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="DivisionIDStg"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="DivisionIntervalRow"
		Group="Behavior"
		InitialValue="0"
		Type="integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="IntervalID"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
