#tag Window
Begin Window TeamInput
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   8
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   364
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   True
   Title           =   "Divisions"
   Visible         =   True
   Width           =   573
   Begin TextField TeamName
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   "Enter the name of the race"
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   96
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   ""
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   ""
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   22
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   453
   End
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Name:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   23
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   68
   End
   Begin GroupBox GroupBox3
      AutoDeactivate  =   True
      Bold            =   False
      Caption         =   "Intervals:"
      Enabled         =   True
      Height          =   134
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   42
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   147
      Underline       =   False
      Visible         =   True
      Width           =   439
      Begin ListBox IntervalList
         AutoDeactivate  =   True
         AutoHideScrollbars=   True
         Bold            =   False
         Border          =   True
         ColumnCount     =   2
         ColumnsResizable=   False
         ColumnWidths    =   ""
         DataField       =   ""
         DataSource      =   ""
         DefaultRowHeight=   -1
         Enabled         =   True
         EnableDrag      =   False
         EnableDragReorder=   False
         GridLinesHorizontal=   0
         GridLinesVertical=   0
         HasHeading      =   True
         HeadingIndex    =   -1
         Height          =   99
         HelpTag         =   ""
         Hierarchical    =   False
         Index           =   -2147483648
         InitialParent   =   "GroupBox3"
         InitialValue    =   ""
         Italic          =   False
         Left            =   48
         LockBottom      =   False
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   False
         LockTop         =   True
         RequiresSelection=   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollBarVertical=   True
         SelectionType   =   0
         TabIndex        =   0
         TabPanelIndex   =   0
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   12.0
         TextUnit        =   0
         Top             =   172
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   True
         Width           =   428
         _ScrollOffset   =   0
         _ScrollWidth    =   -1
      End
   End
   Begin PushButton OK
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "OK"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   480
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   298
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PushButton Cancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "Cancel"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   394
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   298
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin BevelButton NewRaceDistance
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "New"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   493
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   172
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   60
   End
   Begin BevelButton DeleteRaceDistance
      AcceptFocus     =   False
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Delete"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   22
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   493
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   213
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   60
   End
   Begin PopupMenu TeamGender
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   "Select the gender for the division"
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   ""
      Italic          =   False
      Left            =   96
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   58
      Underline       =   False
      Visible         =   True
      Width           =   169
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   8
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Gender:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   58
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
   Begin PopupMenu TeamDivision
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   False
      Height          =   20
      HelpTag         =   "Select the division"
      Index           =   -2147483648
      InitialParent   =   ""
      InitialValue    =   "Select a gender first..."
      Italic          =   False
      Left            =   96
      ListIndex       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Scope           =   0
      TabIndex        =   9
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   94
      Underline       =   False
      Visible         =   True
      Width           =   332
   End
   Begin Label StaticText21
      AutoDeactivate  =   True
      Bold            =   False
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   21
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Scope           =   0
      Selectable      =   False
      TabIndex        =   10
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Division:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   12.0
      TextUnit        =   0
      Top             =   94
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   69
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  dim rsRaceDistances as recordSet
		  dim recsRS as recordSet
		  dim recCount as integer
		  dim i as integer
		  dim SelectStatement as string
		  dim res as boolean
		  
		  TeamGender.addRow "Select Gender..."
		  TeamGender.addRow "Male"
		  TeamGender.addRow "Female"
		  TeamGender.addRow "Mixed"
		  TeamGender.listindex = 0
		  
		  TeamIDStg=Format(wnd_List.TeamID,"##########")
		  
		  recsRS=app.theDB.DBSQLSelect("Select * FROM CCTeams WHERE rowid="+TeamIDStg)
		  if not recsRS.EOF then  'not a new record
		    NewTeam=false
		    TeamName.text=recsRS.Field("Team_Name").stringValue
		    
		    Select case recsRS.Field("Gender").stringValue
		    case "M"
		      TeamGender.listindex=1
		    case "F"
		      TeamGender.listindex=2
		    case "X"
		      TeamGender.listindex=3
		    else
		      TeamGender.listindex=0
		    end select
		    
		  else
		    NewTeam=true
		    recsRS=app.theDB.DBSQLSelect("Select rowid FROM CCteams ORDER BY rowid DESC")
		    TeamIDStg=Format(recsRS.Field("rowid").IntegerValue+1,"##########")
		  end if
		  
		  if TeamGender.ListIndex>0 Then
		    LoadDivisions
		    i=0
		    do
		      i=i+1
		    loop until(TeamDivision.RowTag(i)=recsRS.Field("DivisionID").IntegerValue)
		    TeamDivision.ListIndex=i
		  else
		    TeamDivision.Enabled=False
		  end if
		  
		  IntervalList.columncount=5
		  IntervalList.columnwidths="0,33%,33%,34%"
		  
		  IntervalList.heading(1)="Interval Number"
		  IntervalList.heading(2)="Total Points"
		  IntervalList.heading(3)="Number Scorers"
		  IntervalList.ColumnAlignment(1)=ListBox.AlignLeft
		  IntervalList.ColumnAlignment(2)=ListBox.AlignCenter
		  IntervalList.ColumnAlignment(3)=ListBox.AlignCenter
		  
		  LoadIntervals
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h1
		Protected Sub LoadDivisions()
		  //Load the DivisionDistance popupmenu
		  dim recCount as integer
		  dim rsDivisions as recordSet
		  dim res as boolean
		  dim Gender as String
		  
		  select case TeamGender.Text
		  case"Male"
		    Gender="M"
		  case"Female"
		    Gender="F"
		  case"Mixed"
		    Gender="X"
		  end select
		  
		  TeamDivision.deleteAllRows
		  
		  TeamDivision.addRow "Select Division..."
		  rsDivisions=App.theDB.DBSQLSelect("Select Division_Name, rowid FROM divisions WHERE RaceID=1 AND Gender='"+ _
		  Gender+"' ORDER BY List_Order")
		  while not rsDivisions.EOF
		    TeamDivision.addRow rsDivisions.Field("Division_Name").stringValue
		    TeamDivision.rowTag(TeamDivision.ListCount-1)=rsDivisions.Field("rowid").integerValue
		    rsDivisions.moveNext
		  Wend
		  
		  if rsDivisions.RecordCount=1 then
		    TeamDivision.ListIndex=1
		  else
		    TeamDivision.ListIndex=0
		  end if
		  
		  rsDivisions.Close
		  TeamDivision.Enabled=True
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadIntervals()
		  Dim recsRS as RecordSet
		  
		  IntervalList.DeleteAllRows
		  
		  recsRS=App.theDB.DBSQLSelect("SELECT Interval_Number,Total_Points,Number_Scorers,rowid FROM ccTeamIntervals WHERE TeamID = "+TeamIDStg+" ORDER BY Interval_Number")
		  while not recsRS.EOF
		    IntervalList.AddRow ""
		    IntervalList.Cell(IntervalList.lastindex,0)=recsRS.Field("rowid").stringValue
		    IntervalList.Cell(IntervalList.lastindex,1)=recsRS.Field("Interval_Number").stringValue
		    IntervalList.Cell(IntervalList.lastindex,2)=recsRS.Field("Total_Points").stringValue
		    IntervalList.Cell(IntervalList.lastindex,3)=recsRS.Field("Number_Scorers").stringValue
		    recsRS.moveNext
		  wend
		  recsRS.close
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		IntervalID As Integer
	#tag EndProperty

	#tag Property, Flags = &h1
		Protected NewTeam As boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		TeamIDStg As string
	#tag EndProperty

	#tag Property, Flags = &h0
		TeamIntervalRow As integer
	#tag EndProperty


#tag EndWindowCode

#tag Events IntervalList
	#tag Event
		Sub DoubleClick()
		  TeamIntervalRow=IntervalList.ListIndex
		  IntervalID=val(IntervalList.Cell(TeamIntervalRow,0))
		  TeamIntervalInput.newInterval=False
		  TeamIntervalInput.show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OK
	#tag Event
		Sub Action()
		  dim SQLStatement as string
		  dim res as boolean
		  dim DataEntryError as boolean
		  dim SelectedRow, RowCount as integer
		  Dim d as New MessageDialog  //declare the MessageDialog object
		  Dim b as MessageDialogButton //for handling the result
		  
		  if (TeamName.text="") then
		    app.DisplayDataEntryError("An team name is required","Please enter a team name",TeamName)
		    DataEntryError=true
		  else
		    DataEntryError=false
		  end if
		  
		  if not(DataEntryError) then
		    
		    
		    if NewTeam then
		      'Build the SQL
		      SQLStatement="INSERT INTO CCTeams (Team_Name, Gender, DivisionID, rowid) "
		      SQLStatement=SQLStatement+" VALUES ("
		      
		      SQLStatement=SQLStatement+"'"+TeamName.Text+"', "
		      select case TeamGender.text
		      case "Male"
		        SQLStatement=SQLStatement+"'M',"
		      case "Female"
		        SQLStatement=SQLStatement+"'F',"
		      case "Mixed"
		        SQLStatement=SQLStatement+"'X',"
		      end select
		      SQLStatement=SQLStatement+"'"+str(TeamDivision.RowTag(TeamDivision.ListIndex))+"',"
		      
		      SQLStatement=SQLStatement+TeamIDStg+")"
		      app.theDB.DBSQLExecute(SQLStatement)
		      
		      wnd_List.DataList_Teams.LockDrawing=true
		      wnd_List.DataList_Teams.AppendRow(TeamName.Text)
		      RowCount=wnd_List.DataList_Teams.Rows
		      wnd_List.DataList_Teams.CellText(2,RowCount)=TeamGender.Text
		      wnd_List.DataList_Teams.CellText(3,RowCount)=TeamDivision.Text
		      wnd_List.DataList_Teams.Row(RowCount).ItemData=str(app.theDB.DBLastRowID)
		      wnd_List.DataList_Teams.LockDrawing=false
		      
		    else
		      'Build the SQL
		      SQLStatement = "UPDATE CCTeams SET Team_Name='"+TeamName.text+"'"
		      
		      select case TeamGender.text
		      case "Male"
		        SQLStatement = SQLStatement + ", Gender='M'"
		      case "Female"
		        SQLStatement = SQLStatement + ", Gender='F'"
		      case "Mixed"
		        SQLStatement = SQLStatement + ", Gender='X'"
		      end select
		      
		      SQLStatement=SQLStatement+", DivisionID='"+str(TeamDivision.RowTag(TeamDivision.ListIndex))+"' "
		      
		      SQLStatement = SQLStatement + " WHERE rowid="+TeamIDStg
		      
		      app.theDB.DBSQLExecute(SQLStatement)
		      
		      'update the display
		      SelectedRow=wnd_List.DataList_Divisions.VScrollValue
		      
		      wnd_List.DataList_Teams.CellText(1,wnd_List.RowSelected)=TeamName.Text
		      wnd_List.DataList_Teams.CellText(2,wnd_List.RowSelected)=TeamGender.Text
		      wnd_List.DataList_Teams.CellText(3,wnd_List.RowSelected)=TeamDivision.Text
		      wnd_List.DataList_Teams.Selection.Clear
		      wnd_List.DataList_Teams.VScrollValue=SelectedRow
		    end if
		    
		    
		    TeamInput.close
		    
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events Cancel
	#tag Event
		Sub Action()
		  TeamInput.close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events NewRaceDistance
	#tag Event
		Sub Action()
		  IntervalID=0
		  TeamIntervalInput.show
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events DeleteRaceDistance
	#tag Event
		Sub Action()
		  Dim SQLStatement as string
		  dim res as boolean
		  Dim Result as integer
		  dim row as integer
		  
		  Result=MsgBox("Are you sure you want to delete this Interval?",1+48)
		  if Result = 1 then
		    row=IntervalList.ListIndex
		    SQLStatement="DELETE FROM intervals WHERE RaceID=1 AND DivisionID="+DivisionInput.DivisionIDStg+" AND rowid="+IntervalList.Cell(row,0)
		    App.theDB.DBSQLExecute(SQLStatement)
		    IntervalList.RemoveRow row
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events TeamGender
	#tag Event
		Sub Change()
		  LoadDivisions
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="IntervalID"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TeamIDStg"
		Group="Behavior"
		Type="string"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TeamIntervalRow"
		Group="Behavior"
		InitialValue="0"
		Type="integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
