#tag Window
Begin Window CorrectSwitchTransponders
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   BalloonHelp     =   ""
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   8
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   118
   ImplicitInstance=   True
   LiveResize      =   False
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   False
   MaxWidth        =   32000
   MenuBar         =   0
   MenuBarVisible  =   True
   MinHeight       =   64
   MinimizeButton  =   True
   MinWidth        =   64
   Placement       =   0
   Resizeable      =   False
   Title           =   "Correct Switch Transponders"
   Visible         =   True
   Width           =   487
   Begin Label StaticText1
      AutoDeactivate  =   True
      Bold            =   False
      ControlOrder    =   "0"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   19
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Selectable      =   False
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Racer No 1:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   14
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin Label StaticText2
      AutoDeactivate  =   True
      Bold            =   False
      ControlOrder    =   "1"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   19
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Selectable      =   False
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Racer No 2:"
      TextAlign       =   2
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   46
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin TextField efRacerOne
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      ControlOrder    =   "2"
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   111
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "#####"
      Multiline       =   ""
      Password        =   False
      ReadOnly        =   False
      ScrollbarHorizontal=   ""
      ScrollbarVertical=   "True"
      Styled          =   ""
      TabIndex        =   2
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "0"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   13
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   51
   End
   Begin TextField efRacerTwo
      AcceptTabs      =   False
      Alignment       =   0
      AutoDeactivate  =   True
      AutomaticallyCheckSpelling=   False
      BackColor       =   &cFFFFFF00
      Bold            =   False
      Border          =   True
      ControlOrder    =   "3"
      CueText         =   ""
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Format          =   ""
      Height          =   22
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   111
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Mask            =   "#####"
      Multiline       =   ""
      Password        =   False
      ReadOnly        =   False
      ScrollbarHorizontal=   ""
      ScrollbarVertical=   "True"
      Styled          =   ""
      TabIndex        =   3
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "0"
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   44
      Underline       =   False
      UseFocusRing    =   True
      Visible         =   True
      Width           =   51
   End
   Begin Label stRacerOne
      AutoDeactivate  =   True
      Bold            =   False
      ControlOrder    =   "4"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   174
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Selectable      =   False
      TabIndex        =   4
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Racer One"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   14
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   293
   End
   Begin Label stRacerTwo
      AutoDeactivate  =   True
      Bold            =   False
      ControlOrder    =   "5"
      DataField       =   ""
      DataSource      =   ""
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   174
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      Multiline       =   False
      Selectable      =   False
      TabIndex        =   5
      TabPanelIndex   =   0
      TabStop         =   True
      Text            =   "Racer Two"
      TextAlign       =   0
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   44
      Transparent     =   False
      Underline       =   False
      Visible         =   True
      Width           =   293
   End
   Begin PushButton pbCorrect
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   False
      Caption         =   "Correct"
      ControlOrder    =   "6"
      Default         =   True
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   387
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      TabIndex        =   6
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   78
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
   Begin PushButton phCancel
      AutoDeactivate  =   True
      Bold            =   False
      ButtonStyle     =   "0"
      Cancel          =   True
      Caption         =   "Cancel"
      ControlOrder    =   "7"
      Default         =   False
      Enabled         =   True
      Height          =   20
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   295
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      TabIndex        =   7
      TabPanelIndex   =   0
      TabStop         =   True
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   78
      Underline       =   False
      Visible         =   True
      Width           =   80
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  stRacerOne.Text=""
		  stRacerTwo.Text=""
		  efRacerOne.SelStart=0
		  efRacerOne.SelLength=1
		  
		End Sub
	#tag EndEvent


#tag EndWindowCode

#tag Events efRacerOne
	#tag Event
		Sub LostFocus()
		  dim rs as RecordSet
		  
		  rs=app.theDB.DBSQLSelect("Select Participant_Name, First_Name FROM participants WHERE RaceID=1 AND Racer_Number ="+efRacerOne.text)
		  if not rs.EOF then
		    stRacerOne.Text=rs.Field("First_Name").StringValue+" "+rs.field("Participant_Name").StringValue
		  end if
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events efRacerTwo
	#tag Event
		Sub LostFocus()
		  dim rs as RecordSet
		  
		  rs=app.theDB.DBSQLSelect("Select Participant_Name, First_Name FROM participants WHERE RaceID=1 AND Racer_Number ="+efRacerTwo.text)
		  if not rs.EOF then
		    stRacerTwo.Text=rs.Field("First_Name").StringValue+" "+rs.field("Participant_Name").StringValue
		  end if
		  efRacerOne.SetFocus
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events pbCorrect
	#tag Event
		Sub Action()
		  dim RacerOneParticipantID, RacerTwoParticipantID as Integer
		  dim rsRacerOne, rsRacerTwo as RecordSet
		  dim SQLStatement,TotalTime,NetTime as string
		  
		  if val(efRacerOne.Text)=0 Then
		    app.DisplayDataEntryError("Racer 1 Missing racer number","Please enter a racer number for Racer No 1.",efRacerOne)
		  elseif val(efRacerTwo.Text)=0 Then
		    app.DisplayDataEntryError("Racer 2 Missing racer number","Please enter a racer number for Racer No 2.",efRacerOne)
		  else
		    'need to switch times on the Participant Record
		    '  get the data
		    SQLStatement="SELECT Assigned_Start, Actual_Start, Actual_Stop, Total_Adjustment, Net_Time, rowid FROM participants WHERE RaceID=1 AND Racer_Number ="+efRacerOne.text
		    rsRacerOne=app.theDB.DBSQLSelect(SQLStatement)
		    RacerOneParticipantID=rsRacerOne.Field("rowid").IntegerValue
		    SQLStatement="SELECT Assigned_Start, Actual_Start, Actual_Stop, Total_Adjustment, Net_Time, rowid FROM participants WHERE RaceID=1 AND Racer_Number ="+efRacerTwo.text
		    rsRacerTwo=app.theDB.DBSQLSelect(SQLStatement)
		    RacerTwoParticipantID=rsRacerTwo.Field("rowid").IntegerValue
		    
		    '  update Racer One's data first
		    NetTime=rsRacerTwo.field("Net_Time").StringValue
		    TotalTime=app.CalculateTotalTime(rsRacerOne.field("Assigned_Start").StringValue, rsRacerTwo.field("Actual_Start").StringValue, _
		    rsRacerTwo.field("Actual_Stop").StringValue,rsRacerTwo.field("Total_Adjustment").StringValue,NetTime)
		    SQLStatement="UPDATE participants SET"
		    SQLStatement=SQLStatement+" Actual_Start='"+rsRacerTwo.Field("Actual_Start").StringValue+"',"
		    SQLStatement=SQLStatement+" Actual_Stop='"+rsRacerTwo.Field("Actual_Stop").StringValue+"',"
		    SQLStatement=SQLStatement+" Total_Time='"+TotalTime+"',"
		    SQLStatement=SQLStatement+" Net_Time='"+NetTime+"'"
		    SQLStatement=SQLStatement+" WHERE RaceID=1 AND Racer_Number="+efRacerOne.text
		    App.theDB.DBSQLExecute(SQLStatement)
		    App.theDB.DBCommit
		    
		    '  update Racer Two's data next
		    NetTime=rsRacerOne.field("Net_Time").StringValue
		    TotalTime=app.CalculateTotalTime(rsRacerTwo.field("Assigned_Start").StringValue,rsRacerOne.field("Actual_Start").StringValue, _
		    rsRacerOne.field("Actual_Stop").StringValue,rsRacerOne.field("Total_Adjustment").StringValue, NetTime)
		    SQLStatement="UPDATE participants SET"
		    SQLStatement=SQLStatement+" Actual_Start='"+rsRacerOne.Field("Actual_Start").StringValue+"',"
		    SQLStatement=SQLStatement+" Actual_Stop='"+rsRacerOne.Field("Actual_Stop").StringValue+"',"
		    SQLStatement=SQLStatement+" Total_Time='"+TotalTime+"',"
		    SQLStatement=SQLStatement+" Net_Time='"+NetTime+"'"
		    SQLStatement=SQLStatement+" WHERE RaceID=1 AND Racer_Number="+efRacerTwo.text
		    App.theDB.DBSQLExecute(SQLStatement)
		    App.theDB.DBCommit
		    
		    'need to switch the Participant IDs on the time records
		    SQLStatement="UPDATE times SET"
		    SQLStatement=SQLStatement+" ParticipantID="+str(1000000+RacerOneParticipantID)
		    SQLStatement=SQLStatement+" WHERE RaceID=1 AND ParticipantID="+str(RacerOneParticipantID)
		    App.theDB.DBSQLExecute(SQLStatement)
		    App.theDB.DBCommit
		    
		    SQLStatement="UPDATE times SET"
		    SQLStatement=SQLStatement+" ParticipantID="+str(RacerOneParticipantID)
		    SQLStatement=SQLStatement+" WHERE RaceID=1 AND ParticipantID="+str(RacerTwoParticipantID)
		    App.theDB.DBSQLExecute(SQLStatement)
		    App.theDB.DBCommit
		    
		    SQLStatement="UPDATE times SET"
		    SQLStatement=SQLStatement+" ParticipantID="+str(RacerTwoParticipantID)
		    SQLStatement=SQLStatement+" WHERE RaceID=1 AND ParticipantID="+str(1000000+RacerOneParticipantID)
		    App.theDB.DBSQLExecute(SQLStatement)
		    App.theDB.DBCommit
		    
		    self.Close
		  end if
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events phCancel
	#tag Event
		Sub Action()
		  self.Close
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Appearance"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Appearance"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"10 - Drawer Window"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Visible=true
		Group="Appearance"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Position"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Appearance"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Position"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Appearance"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Position"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
#tag EndViewBehavior
